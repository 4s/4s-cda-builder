package dk.s4.hl7.cda.model.cpd.v20;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CdaBuilderException;
import dk.s4.hl7.cda.model.CdaDate;
import dk.s4.hl7.cda.model.CdaDateInterval;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument.Status;
import dk.s4.hl7.cda.model.util.ModelUtil;

public class CPDHealthStatusActEntry {
  private ID id;
  private CodedValue code;
  private Status statusCode;

  private CdaDate effectiveTime;
  private CdaDateInterval effectiveTimeInterval;
  private List<Participant> authorList;
  private List<CPDHealthConcernObservation> observationList;

  private CPDHealthStatusActEntry(CPDHealthStatusActEntryBuilder builder) {
    this.id = builder.id;
    this.code = builder.code;
    this.statusCode = builder.statusCode;
    this.effectiveTime = builder.effectiveTime;
    this.effectiveTimeInterval = builder.effectiveTimeInterval;
    this.authorList = builder.authorList;
    this.observationList = builder.observationList;

  }

  /**
   * Builder for constructing PDHealthStatusObservationEntry.
   * 
   * <p> Some of the set methods states, that the value is not used during build/encoding of the XML document. </p>
   * <p> This is because the value is given by the Careplan standard, and that value overrules, whatever the builder is given. </p>
   * <p> Hence there is no check for null on those values. </p>
   */

  public static class CPDHealthStatusActEntryBuilder {

    private ID id; // from super
    private CodedValue code;
    private Status statusCode;
    private CdaDate effectiveTime;
    private CdaDateInterval effectiveTimeInterval;

    private List<Participant> authorList;
    private List<CPDHealthConcernObservation> observationList;

    public CPDHealthStatusActEntryBuilder() {
      this.authorList = new ArrayList<Participant>();
      this.observationList = new ArrayList<CPDHealthConcernObservation>();
    }

    /**
     * @param id the unique id of the concern. The value is mandatory.
     */
    public CPDHealthStatusActEntryBuilder setId(ID id) {

      this.id = id;
      return this;
    }

    /**
     * @param code the type code of the concern
     * 
     * <p>The value is not used during build/encoding of the XML document. The encoder uses it's own value.</p>
     */
    public CPDHealthStatusActEntryBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    /**
     * @param statusCode the status of the concern. The value is mandatory and must be Active or completed
     */
    public CPDHealthStatusActEntryBuilder setStatusCode(Status statusCode) {
      this.statusCode = statusCode;
      return this;
    }

    /**
     * @param effectiveTime the time of the concern (date + time). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthStatusActEntryBuilder setEffectiveTime(Date effectiveTime) {
      this.effectiveTime = effectiveTime == null ? null : new CdaDate(effectiveTime, true);
      return this;
    }

    /**
     * @param effectiveTime the time of the concern (date only). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthStatusActEntryBuilder setEffectiveDate(Date effectiveTime) {
      this.effectiveTime = effectiveTime == null ? null : new CdaDate(effectiveTime, false);
      return this;
    }

    /**
     * @param effectiveTime the time of the concern. 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthStatusActEntryBuilder setEffectiveTime(CdaDate effectiveTime) {
      this.effectiveTime = effectiveTime;
      return this;
    }

    /**
     * @param effectiveStartTime the start time interval of the concern (date + time). 
     * @param effectiveStopTime the end time interval of the concern (date + time).
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthStatusActEntryBuilder setEffectiveTimeInterval(Date effectiveStartTime, Date effectiveStopTime) {
      this.effectiveTimeInterval = new CdaDateInterval(effectiveStartTime, effectiveStopTime, true);
      return this;
    }

    /**
     * @param effectiveStartTime the start time interval of the concern (date only). 
     * @param effectiveStopTime the end time interval of the concern (date only). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthStatusActEntryBuilder setEffectiveDateInterval(Date effectiveStartTime, Date effectiveStopTime) {
      this.effectiveTimeInterval = new CdaDateInterval(effectiveStartTime, effectiveStopTime, false);
      return this;
    }

    /**
     * @param effectiveTimeInterval the time interval of the concern. 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthStatusActEntryBuilder setEffectiveTimeInterval(CdaDateInterval effectiveTimeInterval) {
      this.effectiveTimeInterval = effectiveTimeInterval;
      return this;
    }

    /**
     * @param authorList the list of authors of the concern
     */
    public CPDHealthStatusActEntryBuilder setAuthorList(List<Participant> authorList) {
      this.authorList = authorList;
      return this;
    }

    /**
     * @param author one author of the concern
     */
    public CPDHealthStatusActEntryBuilder addAuthor(Participant author) {
      this.authorList.add(author);
      return this;
    }

    /**
     * @param observationList the list of observation of the concern
     */
    public CPDHealthStatusActEntryBuilder setObservationList(List<CPDHealthConcernObservation> observationList) {
      this.observationList = observationList;
      return this;
    }

    /**
     * @param observation one observation of the concern
     */
    public CPDHealthStatusActEntryBuilder addObservation(CPDHealthConcernObservation observation) {
      this.observationList.add(observation);
      return this;
    }

    /**
     * validate and build CPDHealthStatusActEntry 
     */
    public CPDHealthStatusActEntry build() {
      ModelUtil.checkNull(id, "id value is mandatory");
      ModelUtil.checkNull(statusCode, "statusCode value is mandatory");
      if (effectiveTimeInterval != null) {
        ModelUtil.checkNullAtLeastOne(effectiveTimeInterval.getStartTime(), effectiveTimeInterval.getStopTime(),
            "effectiveStartTime value is mandatory when interval");
      }
      checkValidValue(statusCode);
      return new CPDHealthStatusActEntry(this);
    }

    private void checkValidValue(Status status) {
      if (!status.equals(Status.ACTIVE) && !status.equals(Status.COMPLETED)) {
        throw new CdaBuilderException("Status must be either active or completed");
      }
    }

  }

  /**
   * @return the unique id of the concern
   */
  public ID getId() {
    return id;
  }

  /**
   * @return the type code of the concern
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return the status code of the concern
   */
  public Status getStatusCode() {
    return statusCode;
  }

  /**
   * @return the date and time of the concern. Use effectiveTimeHasTime to check if time part has to be considered
   */
  public Date getEffectiveTime() {
    return (effectiveTime == null) ? null : effectiveTime.getDate();
  }

  /**
   * @return if the time part of the effectiveTime has to be considered 
   */
  public boolean effectiveTimeHasTime() {
    return (effectiveTime == null) ? false : effectiveTime.dateHasTime();
  }

  /**
   * @return the start date and time of the concern as CdaDate format
   */
  public CdaDate getEffectiveTimeCdaDate() {
    return effectiveTime;
  }

  /**
   * @return the start date and time interval of the concern
   */
  public CdaDateInterval getEffectiveTimeInterval() {
    return effectiveTimeInterval;
  }

  /**
   * @return the list of authors of the concern
   */
  public List<Participant> getAuthorList() {
    return authorList;
  }

  /**
   * @return the list of observation
   */
  public List<CPDHealthConcernObservation> getObservationList() {
    return observationList;
  }

}
