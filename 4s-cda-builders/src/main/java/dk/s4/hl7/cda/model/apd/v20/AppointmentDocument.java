package dk.s4.hl7.cda.model.apd.v20;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.core.v20.BaseClinicalDocument;

/**
 * An implementation of SimpleClinicalDocument that only supports the data of
 * the Danish Appointment version 2.0
 * 
 */
public class AppointmentDocument extends BaseClinicalDocument {

  public enum Status {
    ACTIVE
  }

  public enum AppointmentEncounterCode {
    MunicipalityAppointment,
    PractitionerAppointment,
    RegionalAppointment
  }

  public enum AppointmentLocationTypeCode {
    HEALTH_ORGANIZATION,
    HOME,
    NON_SOR
  }

  private Status appointmentStatus;

  private String appointmentText;

  private String appointmentTitle;

  private ID appointmentId;

  private CodedValue indicationCode;

  private Participant appointmentAuthor;

  private Participant appointmentPerformer;

  private AppointmentLocationTypeCode appointmentLocationTypeCode;
  private OrganizationIdentity appointmentLocation;
  private OrganizationIdentity appointmentLocationHomeAddress;

  private boolean isRepeatingDocument;
  private String repeatingDocumentGroupValue;

  private boolean isGuidedInterval;
  private String guidedIntervalText;

  private AppointmentEncounterCode appointmentEncounterCode;

  public AppointmentDocument(ID id) {
    super(HL7.CDA_TYPEID_ROOT, HL7.CDA_TYPEID_EXTENSION, id, Loinc.APD_CODE, Loinc.APD_DISPLAYNAME,
        new String[] { MedCom.DK_APD_ROOT_OID }, HL7.REALM_CODE_DK);
  }

  public String getAppointmentText() {
    return appointmentText;
  }

  public void setAppointmentText(String appointmentText) {
    this.appointmentText = appointmentText;
  }

  public String getAppointmentTitle() {
    return appointmentTitle;
  }

  public void setAppointmentTitle(String appointmentTitle) {
    this.appointmentTitle = appointmentTitle;
  }

  public ID getAppointmentId() {
    return appointmentId;
  }

  public void setAppointmentId(ID id) {
    this.appointmentId = id;
  }

  /**
   * @param extension the id of the appointment. Root and authorityname will be defaulted.
   * <p> Use method setAppointmentId(ID id) instead </p> 
   * 
   */
  @Deprecated
  public void setAppointmentId(String extension) {
    this.appointmentId = new IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension(extension)
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
  }

  public Status getAppointmentStatus() {
    return appointmentStatus;
  }

  public void setAppointmentStatus(Status appointmentStatus) {
    this.appointmentStatus = appointmentStatus;
  }

  public String getIndicationDisplayName() {
    if (indicationCode != null) {
      return indicationCode.getDisplayName();
    }
    return null;
  }

  public void setIndicationDisplayName(String indicationDisplayName) {
    this.indicationCode = new CodedValue(null, indicationDisplayName);
  }

  public CodedValue getIndicationCode() {
    return this.indicationCode;
  }

  public void setIndicationCode(CodedValue codedValue) {
    this.indicationCode = codedValue;
  }

  public Participant getAppointmentAuthor() {
    return appointmentAuthor;
  }

  public void setAppointmentAuthor(Participant appointmentAuthor) {
    this.appointmentAuthor = appointmentAuthor;
  }

  public Participant getAppointmentPerformer() {
    return appointmentPerformer;
  }

  public void setAppointmentPerformer(Participant appointmentPerformer) {
    this.appointmentPerformer = appointmentPerformer;
  }

  public AppointmentLocationTypeCode getAppointmentLocationTypeCode() {
    return appointmentLocationTypeCode;
  }

  public void setAppointmentLocationTypeCode(AppointmentLocationTypeCode appointmentLocationTypeCode) {
    this.appointmentLocationTypeCode = appointmentLocationTypeCode;
  }

  public OrganizationIdentity getAppointmentLocation() {
    return appointmentLocation;
  }

  public void setAppointmentLocation(OrganizationIdentity appointmentLocation) {
    this.appointmentLocation = appointmentLocation;
  }

  public OrganizationIdentity getAppointmentLocationHomeAddress() {
    return appointmentLocationHomeAddress;
  }

  public void setAppointmentLocationHomeAddress(OrganizationIdentity appointmentLocationHomeAddress) {
    this.appointmentLocationHomeAddress = appointmentLocationHomeAddress;
  }

  public boolean isRepeatingDocument() {
    return isRepeatingDocument;
  }

  public void setRepeatingDocument(boolean isRepeatingDocument) {
    this.isRepeatingDocument = isRepeatingDocument;
  }

  public boolean isGuidedInterval() {
    return isGuidedInterval;
  }

  public String getRepeatingDocumentGroupValue() {
    return repeatingDocumentGroupValue;
  }

  public void setRepeatingDocumentGroupValue(String repeatingDocumentGroupValue) {
    this.repeatingDocumentGroupValue = repeatingDocumentGroupValue;
  }

  public void setGuidedInterval(boolean isGuidedInterval) {
    this.isGuidedInterval = isGuidedInterval;
  }

  public String getGuidedIntervalText() {
    return guidedIntervalText;
  }

  public void setGuidedIntervalText(String guidedIntervalText) {
    this.guidedIntervalText = guidedIntervalText;
  }

  public AppointmentEncounterCode getAppointmentEncounterCode() {
    return appointmentEncounterCode;
  }

  public void setAppointmentEncounterCode(AppointmentEncounterCode appointmentEncounterCode) {
    this.appointmentEncounterCode = appointmentEncounterCode;
  }

}
