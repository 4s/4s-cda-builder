package dk.s4.hl7.cda.model.cpd.v20;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.util.ModelUtil;

public class CPDGoalObservationComponent {

  private String title;
  private String text;
  private CodedValue code;
  private List<CPDGoalObservationEntry> entryList;

  private CPDGoalObservationComponent(CPDGoalObservationComponentBuilder builder) {
    this.title = builder.title;
    this.text = builder.text;
    this.code = builder.code;
    this.entryList = builder.entryList;
  }

  public static class CPDGoalObservationComponentBuilder {

    private String title;
    private String text;
    private CodedValue code;
    private List<CPDGoalObservationEntry> entryList;

    public CPDGoalObservationComponentBuilder() {
      this.entryList = new ArrayList<CPDGoalObservationEntry>();
    }

    public CPDGoalObservationComponentBuilder setTitle(String title) {
      this.title = title;
      return this;
    }

    public CPDGoalObservationComponentBuilder setText(String text) {
      this.text = text;
      return this;
    }

    public CPDGoalObservationComponentBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public CPDGoalObservationComponentBuilder setEntryList(List<CPDGoalObservationEntry> entryList) {
      this.entryList = entryList;
      return this;
    }

    public CPDGoalObservationComponentBuilder addEntry(CPDGoalObservationEntry entry) {
      this.entryList.add(entry);
      return this;
    }

    public CPDGoalObservationComponent build() {
      ModelUtil.checkNull(code, "code value is mandatory");
      return new CPDGoalObservationComponent(this);
    }
  }

  public List<CPDGoalObservationEntry> getEntryList() {
    return entryList;
  }

  public String getTitle() {
    return title;
  }

  public String getText() {
    return text;
  }

  public CodedValue getCode() {
    return code;
  }

}
