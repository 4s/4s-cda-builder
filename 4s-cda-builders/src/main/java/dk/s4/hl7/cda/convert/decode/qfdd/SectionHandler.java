package dk.s4.hl7.cda.convert.decode.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class SectionHandler implements CDAXmlHandler<QFDDDocument> {
  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section";

  private String title;
  private String language;
  private List<Section<QFDDOrganizer>> sections;
  private OrganizerHandler organizerHandler;
  private RawTextHandler rawTextHandler;
  private CopyRightHandler copyRightHandler;

  public SectionHandler() {
    organizerHandler = new OrganizerHandler();
    copyRightHandler = new CopyRightHandler();
    sections = new ArrayList<Section<QFDDOrganizer>>();
    rawTextHandler = new RawTextHandler(COMPONENT_SECTION + "/text", "text");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "languageCode", "code")) {
      language = xmlElement.getAttributeValue("code");
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "title")) {
      title = xmlElement.getElementValue();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "section")) {
      if (copyRightHandler.isCopyRightSection()) {
        Section sectionCopyright = new Section(title, rawTextHandler.getRawText(), language);
        sectionCopyright.setCopyrightTexts(copyRightHandler.getCopyRightTexts());
        sections.add(sectionCopyright);
      } else {
        Section<QFDDOrganizer> section = new Section<QFDDOrganizer>(title, rawTextHandler.getRawText(), language);
        for (QFDDOrganizer organizer : organizerHandler.getOrganizers()) {
          section.addOrganizer(organizer);
        }
        sections.add(section);
      }
      clear();
    }
  }

  public void clear() {
    rawTextHandler.clear();
    title = null;
    language = null;
    organizerHandler.clear();
    copyRightHandler.clear();
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    organizerHandler.addHandlerToMap(xmlMapping);
    rawTextHandler.addHandlerToMap(xmlMapping);
    copyRightHandler.addHandlerToMap(xmlMapping);
    xmlMapping.add(COMPONENT_SECTION, this);
    xmlMapping.add(COMPONENT_SECTION + "/title", this);
    xmlMapping.add(COMPONENT_SECTION + "/languageCode", this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    organizerHandler.removeHandlerFromMap(xmlMapping);
    rawTextHandler.removeHandlerFromMap(xmlMapping);
    copyRightHandler.removeHandlerFromMap(xmlMapping);
    xmlMapping.remove(COMPONENT_SECTION);
    xmlMapping.remove(COMPONENT_SECTION + "/title");
    xmlMapping.remove(COMPONENT_SECTION + "/languageCode");
  }

  @Override
  public void addDataToDocument(QFDDDocument clinicalDocument) {
    for (Section<QFDDOrganizer> section : sections) {
      clinicalDocument.addSection(section);
    }
  }
}
