package dk.s4.hl7.cda.convert;

import java.io.Reader;

import dk.s4.hl7.cda.convert.base.AppendableSerializer;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.convert.base.ReaderSerializer;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;

/**
 * The implementation of Codec to encode and decode CPD from objects to XML
 * and XML to objects. After construction the codec is considered thread-safe.
 * 
 * @see <a href="https://www.dartlang.org/articles/libraries/converters-and-codecs">https://www.dartlang.org/articles/libraries/converters-and-codecs</a>
 * 
 */
public class CPDV20XmlCodec
    implements Codec<CPDDocument, String>, AppendableSerializer<CPDDocument>, ReaderSerializer<CPDDocument> {
  private CPDV20XmlConverter cpdXmlConverter;
  private XmlCPDV20Converter xmlCpdConverter;

  public CPDV20XmlCodec() {
    this(new XmlPrettyPrinter());
  }

  /**
   * Create CPDXmlCodec
   * 
   * @param xmlPrettyPrinter May be null
   */
  public CPDV20XmlCodec(XmlPrettyPrinter xmlPrettyPrinter) {
    this.cpdXmlConverter = new CPDV20XmlConverter(xmlPrettyPrinter);
    this.xmlCpdConverter = new XmlCPDV20Converter();
  }

  public String encode(CPDDocument source) {
    return cpdXmlConverter.convert(source);
  }

  public CPDDocument decode(String source) {
    return xmlCpdConverter.convert(source);
  }

  @Override
  public void serialize(CPDDocument source, Appendable appendable) {
    if (appendable == null) {
      throw new NullPointerException("Target appendable is null");
    }
    cpdXmlConverter.serialize(source, appendable);
  }

  @Override
  public CPDDocument deserialize(Reader source) {
    return xmlCpdConverter.deserialize(source);
  }
}
