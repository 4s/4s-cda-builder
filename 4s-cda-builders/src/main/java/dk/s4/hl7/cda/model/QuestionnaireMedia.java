package dk.s4.hl7.cda.model;

public class QuestionnaireMedia {
  private final String mediaType;
  private final String representation;
  private final String value;
  private final String id;

  private QuestionnaireMedia(QuestionnaireMediaBuilder builder) {
    this.mediaType = builder.mediaType;
    this.representation = builder.representation;
    this.value = builder.value;
    this.id = builder.id;
  }

  public static class QuestionnaireMediaBuilder {
    private String mediaType;
    private String representation;
    private String value;
    private String id;

    public QuestionnaireMediaBuilder mediaType(String mediaType1) {
      this.mediaType = mediaType1;
      return this;
    }

    public QuestionnaireMediaBuilder representation(String representation) {
      this.representation = representation;
      return this;
    }

    public QuestionnaireMediaBuilder value(String value) {
      this.value = value;
      return this;
    }

    public QuestionnaireMediaBuilder id(String id) {
      this.id = id;
      return this;
    }

    public QuestionnaireMedia build() {
      return new QuestionnaireMedia(this);
    }
  }

  public String getMediaType() {
    return mediaType;
  }

  public String getRepresentation() {
    return representation;
  }

  public String getValue() {
    return value;
  }

  public String getId() {
    return id;
  }

}
