package dk.s4.hl7.cda.convert;

import dk.s4.hl7.cda.codes.*;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.convert.encode.ClinicalDocumentConverter;
import dk.s4.hl7.cda.convert.encode.narrative.text.NarrativeTextConverter;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Comment;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.Measurement.Status;
import dk.s4.hl7.cda.model.phmr.v20.MeasurementGroup;
import dk.s4.hl7.cda.model.phmr.v20.PHMRDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;
import dk.s4.hl7.util.xml.XmlStreamBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * An implementation of the Converter that can build an XML Document following
 * the Danish PHMR standard.
 */

public class PHMRV20XmlConverter extends ClinicalDocumentConverter<PHMRDocument, Void> {
  private Logger logger;

  public PHMRV20XmlConverter(XmlPrettyPrinter xmlPrettyPrinter) {
    super(xmlPrettyPrinter);
    this.logger = LoggerFactory.getLogger(PHMRV20XmlConverter.class);
  }

  public PHMRV20XmlConverter() {
    this(new XmlPrettyPrinter());
  }

  private void buildVitalSignsSection(List<MeasurementGroup> measurementGroups, String text,
      XmlStreamBuilder xmlBuilder) throws IOException {
    if (!measurementGroups.isEmpty()) {
      xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
      xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");

      //    BuildUtil.buildTemplateIds(xmlBuilder, HL7.PHMR_VITAL_SIGNS_FIRST_ID, HL7.PHMR_VITAL_SIGNS_SECOND_ID,HL7.PHMR_ENTRY_ID); // templates foreløbig fjernet i v 2.0
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.PHMR_VITAL_SIGNS_FIRST_ID);
      BuildUtil.buildCode(Loinc.SECTION_VITAL_SIGNS_CODE, Loinc.OID, Loinc.SECTION_VITAL_SIGNS_DISPLAYNAME,
          Loinc.DISPLAYNAME, xmlBuilder);
      xmlBuilder.element("title").value("Vital Signs").elementEnd();

      if (text != null) {
        // Text may be embedded HTML at this point
        xmlBuilder.element("text").valueNoEscaping(text).elementEnd();
      } else {
        xmlBuilder.element("text").value("Vital Signs").elementEnd();
      }
      for (MeasurementGroup group : measurementGroups) {
        buildMeasurementObservationList(group, xmlBuilder);
      }

      xmlBuilder.elementEnd(); // end section
      xmlBuilder.elementEnd(); // end component
    }
  }

  private void buildResultsSection(List<MeasurementGroup> measurementGroups, String text, XmlStreamBuilder xmlBuilder)
      throws IOException {

    logger.debug("Creating observations from " + measurementGroups.size() + " measurements groups");
    if (!measurementGroups.isEmpty()) {
      xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
      xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");

      //      BuildUtil.buildTemplateIds(xmlBuilder, HL7.PHMR_RESULTS_FIRST_ID, HL7.PHMR_RESULTS_SECOND_ID, HL7.PHMR_RESULTS_THIRD_ID); // templates foreløbig fjernet i v 2.0
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.PHMR_RESULTS_FIRST_ID);
      BuildUtil.buildCode(Loinc.SECTION_RESULTS_CODE, Loinc.OID, Loinc.SECTION_RESULTS_DISPLAYNAME, Loinc.DISPLAYNAME,
          xmlBuilder);
      xmlBuilder.element("title").value("Results").elementEnd();

      if (text != null) {
        // Text may be embedded HTML at this point
        xmlBuilder.element("text").valueNoEscaping(text).elementEnd();
      } else {
        xmlBuilder.element("text").value("Results").elementEnd();
      }

      for (MeasurementGroup group : measurementGroups) {
        buildMeasurementObservationList(group, xmlBuilder);
      }

      xmlBuilder.elementEnd(); // end section
      xmlBuilder.elementEnd(); // end component
    }
  }

  // === Private helper methods

  private void buildMeasurementObservationList(MeasurementGroup measurementGroup, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (!measurementGroup.getMeasurementList().isEmpty()) {

      xmlBuilder.element("entry").attribute("typeCode", "COMP").attribute("contextConductionInd", "true").addElement(
          xmlBuilder.element("organizer").attribute("classCode", "CLUSTER").attribute("moodCode", "EVN"));

      BuildUtil.buildTemplateIds(xmlBuilder, HL7.OBSERVATION_ORGANIZER);
      buildStatusCode(measurementGroup.getStatus(), xmlBuilder);
      buildEffectiveTime(measurementGroup.getTimestamp(), xmlBuilder);

      for (Measurement measurement : measurementGroup.getMeasurementList()) {

        xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
        xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");

        if (measurement.getType() == Measurement.Type.PHYSICAL_QUANTITY) {
          //      BuildUtil.buildTemplateIds(xmlBuilder, HL7.OBSERVATION, HL7.PHMR_NUMERIC_OBSERVATION);  // templates foreløbig fjernet i v 2.0
          BuildUtil.buildTemplateIds(xmlBuilder, HL7.PHMR_NUMERIC_OBSERVATION);
          BuildUtil.buildId(measurement.getId(), xmlBuilder);
          buildMeasurementCode(measurement, xmlBuilder);

          buildValueAsPQ(measurement, xmlBuilder);
          buildDataInputContext(measurement, xmlBuilder);
        }

        if (measurement.hasComment()) {
          buildComment(measurement.getComment(), xmlBuilder);
        }
        xmlBuilder.elementEnd(); // end observation
        xmlBuilder.elementEnd(); // end component
      }
      xmlBuilder.elementEnd(); // end organizer
      xmlBuilder.elementEnd(); // end entry

    }
  }

  private void buildDataInputContext(Measurement measurement, XmlStreamBuilder xmlBuilder) throws IOException {
    if (measurement.getDataInputContext() == null) {
      BuildUtil.buildNullFlavor("methodCode", xmlBuilder);
      return;
    }
    if (measurement.getDataInputContext().getMeasurementPerformerCode() != null) {
      buildMethodCode(getPerformerMethodCode(measurement), MedCom.MESSAGECODE_OID,
          getPerformerMethodDisplayName(measurement), MedCom.MESSAGECODE_DISPLAYNAME, xmlBuilder);
    }
    if (measurement.getDataInputContext().getMeasurementProvisionMethodCode() != null) {
      buildMethodCode(getProvisionMethodCode(measurement), MedCom.MESSAGECODE_OID,
          getProvisionMethodDisplayName(measurement), MedCom.MESSAGECODE_DISPLAYNAME, xmlBuilder);
    }
    if (measurement.getDataInputContext().getOrganizationIdentity() != null
        && measurement.getDataInputContext().getProvisionUserType() != null) {
      xmlBuilder.element("participant").attribute("typeCode", "ENT").attribute("contextControlCode", "OP");
      xmlBuilder.element("participantRole").attribute("classCode",
          measurement.getDataInputContext().getProvisionUserType());
      buildSORId(measurement.getDataInputContext().getOrganizationIdentity(), xmlBuilder);
      xmlBuilder.elementEnd(); // participant
      xmlBuilder.elementEnd(); // participantRole
    }
  }

  private void buildMeasurementCode(Measurement measurement, XmlStreamBuilder xmlBuilder) throws IOException {
    CodedValue code = new CodedValue(measurement.getCode(), measurement.getCodeSystem(), measurement.getDisplayName(),
        measurement.getCodeSystemName());
    BuildUtil.buildCode(code, xmlBuilder);
    // for phmr v2.0 skal koder ikke sendes ud med translation, men alene direkte i code elementet.
  }

  private void buildComment(Comment comment, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("entryRelationship").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("act").attribute("classCode", "ACT").attribute("moodCode", "EVN");

    BuildUtil.buildCode(Loinc.COMMENT_CODEDVALUE, xmlBuilder);
    buildText(comment.getText(), xmlBuilder);
    buildAuthorSection(null, comment.getAuthor(), xmlBuilder);

    xmlBuilder.elementEnd(); // end act
    xmlBuilder.elementEnd(); // end entryRelationship
  }

  private void buildText(String text, XmlStreamBuilder xmlBuilder) throws IOException {
    if (text != null && !text.trim().isEmpty()) {
      xmlBuilder.element("text").value(text).elementEnd();
    }
  }

  private static void buildStatusCode(Status status, XmlStreamBuilder xmlBuilder) throws IOException {
    if (status == Status.COMPLETED) {
      xmlBuilder.element("statusCode").attribute("code", "completed").elementShortEnd();
    } else if (status == Status.NULLIFIED) {
      xmlBuilder.element("statusCode").attribute("code", "nullified").elementShortEnd();
    }
  }

  private static String getProvisionMethodCode(Measurement measurement) {
    switch (measurement.getDataInputContext().getMeasurementProvisionMethodCode()) {
    case Electronically:
      return MedCom.TRANSFERRED_ELECTRONICALLY;
    case TypedByCitizen:
      return MedCom.TYPED_BY_CITIZEN;
    case TypedByCitizenRelative:
      return MedCom.TYPED_BY_CITIZEN_RELATIVE;
    case TypedByHealthcareProfessional:
      return MedCom.TYPED_BY_HEALTHCAREPROFESSIONAL;
    case TypedByCareGiver:
      return MedCom.TYPED_BY_CAREGIVER;
    default:
      throw new IllegalStateException("Invalid Context provision method code"
          + measurement.getDataInputContext().getMeasurementProvisionMethodCode().toString());
    }
  }

  private static String getProvisionMethodDisplayName(Measurement measurement) {
    switch (measurement.getDataInputContext().getMeasurementProvisionMethodCode()) {
    case Electronically:
      return MedCom.TRANSFERRED_ELECTRONICALLY_DISPLAYNAME;
    case TypedByCitizen:
      return MedCom.TYPED_BY_CITIZEN_DISPLAYNAME;
    case TypedByCitizenRelative:
      return MedCom.TYPED_BY_CITIZEN_RELATIVE_DISPLAYNAME;
    case TypedByHealthcareProfessional:
      return MedCom.TYPED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME;
    case TypedByCareGiver:
      return MedCom.TYPED_BY_CAREGIVER_DISPLAYNAME;
    default:
      throw new IllegalStateException("Invalid Context provision method code"
          + measurement.getDataInputContext().getMeasurementProvisionMethodCode().toString());
    }
  }

  private static String getPerformerMethodCode(Measurement measurement) {
    switch (measurement.getDataInputContext().getMeasurementPerformerCode()) {
    case Citizen:
      return MedCom.PERFORMED_BY_CITIZEN;
    case HealthcareProfessional:
      return MedCom.PERFORMED_BY_HEALTHCAREPROFESSIONAL;
    case CareGiver:
      return MedCom.PERFORMED_BY_CAREGIVER;
    default:
      throw new IllegalStateException("Invalid Context performer code"
          + measurement.getDataInputContext().getMeasurementPerformerCode().toString());
    }
  }

  private static String getPerformerMethodDisplayName(Measurement measurement) {
    switch (measurement.getDataInputContext().getMeasurementPerformerCode()) {
    case Citizen:
      return MedCom.PERFORMED_BY_CITIZEN_DISPLAYNAME;
    case HealthcareProfessional:
      return MedCom.PERFORMED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME;
    case CareGiver:
      return MedCom.PERFORMED_BY_CAREGIVER_DISPLAYNAME;
    default:
      throw new IllegalStateException("Invalid Context performer code"
          + measurement.getDataInputContext().getMeasurementPerformerCode().toString());
    }
  }

  public String convert(PHMRDocument source) {
    int nofObservations = 0;
    for (MeasurementGroup measurementGroup : source.getVitalSignGroups()) {
      nofObservations += measurementGroup.getMeasurementList().size();
    }
    for (MeasurementGroup measurementGroup : source.getResultGroups()) {
      nofObservations += measurementGroup.getMeasurementList().size();
    }

    StringBuilder builder = new StringBuilder(8000 + (nofObservations * 1500));
    serialize(source, builder);
    return builder.toString();
  }

  @Override
  protected void buildDocumentationOf(PHMRDocument source, XmlStreamBuilder xmlBuilder) throws IOException {
    Collection<CodedValue> codedValues = getUniqueMeasurementCodes((PHMRDocument) source);

    xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
    xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");
    xmlBuilder.element("effectiveTime");

    if (source.getServiceStartTime() != null) {
      xmlBuilder
          .element("low")
          .attribute("value", dateTimeformatter.format(source.getServiceStartTime()))
          .elementShortEnd();
    }
    if (source.getServiceStopTime() != null) {
      xmlBuilder
          .element("high")
          .attribute("value", dateTimeformatter.format(source.getServiceStopTime()))
          .elementShortEnd();
    } else {
      BuildUtil.buildNullFlavor("high", xmlBuilder);
    }
    xmlBuilder.elementEnd(); // end effectiveTime
    xmlBuilder.elementEnd(); // end serviceEvent
    xmlBuilder.elementEnd(); // end documentationOf

    buildPhmrVersion(source, xmlBuilder);

    for (CodedValue codedValue : codedValues) {
      xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
      xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");
      BuildUtil.buildCode(codedValue.getCode(), codedValue.getCodeSystem(), codedValue.getDisplayName(), xmlBuilder);
      xmlBuilder.elementEnd(); // end serviceEvent
      xmlBuilder.elementEnd(); // end documentationOf
    }
  }

  private void buildPhmrVersion(PHMRDocument source, XmlStreamBuilder xmlBuilder) throws IOException {

    xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
    xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIds(xmlBuilder, MedCom.VERSION_TEMPLATEID_ROOT);

    BuildUtil.buildId(MedCom.VERSION_ID_ROOT, MedCom.ROOT_AUTHORITYNAME, source.getCdaProfileAndVersion(), xmlBuilder);

    xmlBuilder.elementEnd(); // end serviceEvent
    xmlBuilder.elementEnd(); // end documentationOf

  }

  private Collection<CodedValue> getUniqueMeasurementCodes(PHMRDocument source) {
    Set<String> codes = new HashSet<String>();
    List<CodedValue> codedValues = new ArrayList<CodedValue>(10);
    for (MeasurementGroup measurementGroup : source.getVitalSignGroups()) {
      for (Measurement measurement : measurementGroup.getMeasurementList()) {
        addObservationCode(measurement, codes, codedValues);
      }
    }
    for (MeasurementGroup measurementGroup : source.getResultGroups()) {
      for (Measurement measurement : measurementGroup.getMeasurementList()) {
        addObservationCode(measurement, codes, codedValues);
      }
    }
    return codedValues;
  }

  private void addObservationCode(Measurement measurement, Set<String> codes, List<CodedValue> codedValues) {
    if (measurement.getCode() != null && !codes.contains(measurement.getCode())) {
      codes.add(measurement.getCode());
      codedValues.add(new CodedValue(measurement.getCode(), measurement.getCodeSystem(), measurement.getDisplayName(),
          measurement.getCodeSystemName()));
    }
    // for phmr v2.0 skal koders translated værdier ikke sendes med ud.
  }

  @Override
  protected void buildInformationRecipient(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // Not used
  }

  @Override
  protected void buildParticipants(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // Not used
  }

  @Override
  protected void buildSelfAuthorSectionConsiderRepresentedOrganization(Patient patient, Date time,
      OrganizationIdentity organizationIdentity, XmlStreamBuilder xmlBuilder) throws IOException {
    buildSelfAuthorSection(patient, time, organizationIdentity, xmlBuilder);
  }

  @Override
  protected void buildRepresentedOrganization(OrganizationIdentity organizationIdentity, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (organizationIdentity != null && organizationIdentity.getOrgName() != null
        && !organizationIdentity.getOrgName().trim().isEmpty()) {
      xmlBuilder.element("representedOrganization").attribute("classCode", "ORG").attribute("determinerCode",
          "INSTANCE");
      BuildUtil.buildIdOptional(organizationIdentity.getId(), xmlBuilder);
      xmlBuilder.element("name").value(organizationIdentity.getOrgName()).elementEnd();
      buildTelecom(organizationIdentity.getTelecomList(), xmlBuilder);
      buildAddress(organizationIdentity.getAddress(), false, xmlBuilder);
      xmlBuilder.elementEnd(); // end representedOrganization
    }
  }

  @Override
  protected void buildBody(PHMRDocument phmrDocument, XmlStreamBuilder xmlBuilder,
      NarrativeTextConverter<Void> narrativeTextConverter) throws IOException {
    this.buildStructuredBodySectionStart(xmlBuilder);
    this.buildVitalSignsSection(phmrDocument.getVitalSignGroups(), phmrDocument.getVitalSignsText(), xmlBuilder);
    this.buildResultsSection(phmrDocument.getResultGroups(), phmrDocument.getResultsText(), xmlBuilder);
    this.buildStructuredBodySectionEnd(xmlBuilder);
  }
}