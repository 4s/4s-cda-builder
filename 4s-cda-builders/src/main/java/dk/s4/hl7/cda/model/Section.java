package dk.s4.hl7.cda.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Generic Section used by QFDD and QRD documents. Holds a list of Organizers, which holds a list of questions or responses
 */

public class Section<O extends Organizer> {

  private SectionInformation sectionInformation;
  private List<O> organizers;

  private List<String> copyrightTexts;

  public Section(String title, String text) {
    this(title, text, null);
  }

  public Section(String title, String text, String language) {
    this.sectionInformation = new SectionInformation(title, text, language);
    this.organizers = new ArrayList<O>();
    this.copyrightTexts = new ArrayList<>();
  }

  public Section(SectionInformation sectionInformation) {
    this.sectionInformation = sectionInformation;
    this.organizers = new ArrayList<O>();
    this.copyrightTexts = new ArrayList<>();
  }

  public String getTitle() {
    if (sectionInformation != null) {
      return sectionInformation.getTitle();
    }
    return null;
  }

  public String getText() {
    if (sectionInformation != null) {
      return sectionInformation.getText();
    }
    return null;
  }

  public String getLanguage() {
    if (sectionInformation != null) {
      return sectionInformation.getLanguage();
    }
    return null;
  }

  public SectionInformation getSectionInformation() {
    return sectionInformation;
  }

  public void addOrganizer(O organizer) {
    organizers.add(organizer);
  }

  public List<O> getOrganizers() {
    return organizers;
  }

  public void addCopyrightText(String text) {
    this.copyrightTexts.add(text);
  }

  public void setCopyrightTexts(List<String> texts) {
    this.copyrightTexts = texts;
  }

  public List<String> getCopyrightTexts() {
    return this.copyrightTexts;
  }

  public boolean isCopyRightSection() {
    return copyrightTexts != null && copyrightTexts.size() > 0;
  }

  @Override
  public String toString() {
    if (sectionInformation != null) {
      return "Section [title=" + getText() + ", text=" + getText() + ", organizers=" + organizers + ", copyrightTexts="
          + copyrightTexts + "]";
    }
    return "Section [title='', text='', organizers=" + organizers + ", copyrightTexts=" + copyrightTexts + "]";
  }
}
