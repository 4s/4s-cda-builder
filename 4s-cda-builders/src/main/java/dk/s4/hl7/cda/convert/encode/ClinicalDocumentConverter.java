package dk.s4.hl7.cda.convert.encode;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.Format;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.NSI;
import dk.s4.hl7.cda.convert.PHMRXmlConverter;
import dk.s4.hl7.cda.convert.base.AppendableSerializer;
import dk.s4.hl7.cda.convert.base.Converter;
import dk.s4.hl7.cda.convert.encode.narrative.text.NarrativeTextConverter;
import dk.s4.hl7.cda.convert.encode.pattern.ExternalReferencePattern;
import dk.s4.hl7.cda.convert.encode.pattern.QuestionOptionsPattern;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Participant.ROLE_TYPE;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Telecom;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

/**
 * Abstract base class for all CDA converters that writes xml from the GreenCDA
 * model.
 * 
 * Contains overall process of converting and helper methods
 * 
 * @author Frank Jacobsen Systematic A/S
 */
public abstract class ClinicalDocumentConverter<E extends ClinicalDocument, C>
    implements Converter<E, String>, AppendableSerializer<E> {

  protected static final ExternalReferencePattern externalReferencePattern = new ExternalReferencePattern();
  protected static final QuestionOptionsPattern questionOptionsPattern = new QuestionOptionsPattern();

  public static Writer createBufferedFileWriter(File file, boolean append, String charset, int bufferSize)
      throws UnsupportedEncodingException, FileNotFoundException {
    return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, append), charset), bufferSize);
  }

  protected Logger logger;
  protected XmlPrettyPrinter xmlPrettyPrinter;

  /**
   * dateTimeformatter that can translate into the HL7 formatting. Changed to
   * use Apache commons FastDateFormat which ensures thread safety
   **/
  protected static final Format dateTimeformatter = FastDateFormat.getInstance("yyyyMMddHHmmssZ");
  protected static final Format dateformatter = FastDateFormat.getInstance("yyyyMMdd");
  private static final Format birthTimeFormatter = FastDateFormat.getInstance("yyyyMMdd000000+0000");

  public ClinicalDocumentConverter() {
    this(new XmlPrettyPrinter());
  }

  public ClinicalDocumentConverter(XmlPrettyPrinter xmlPrettyPrinter) {
    this.logger = LoggerFactory.getLogger(PHMRXmlConverter.class);
    this.xmlPrettyPrinter = xmlPrettyPrinter;
  }

  protected void buildRootNode(XmlStreamBuilder xmlBuilder) throws IOException {
    // CONF-PHMR-1:
    xmlBuilder.addDefaultPI();
    xmlBuilder.addStyleSheet();
    xmlBuilder
        .element("ClinicalDocument")
        .attribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
        .attribute("xmlns", "urn:hl7-org:v3")
        .attribute("xmlns:sdtc", "urn:hl7-org:sdtc")
        .attribute("classCode", "DOCCLIN")
        .attribute("moodCode", "EVN")
        .attribute("xsi:schemaLocation",
            "urn:hl7-org:v3 http://svn.medcom.dk/svn/releases/Standarder/HL7/Generic/Schema/CDA_SDTC.xsd");
  }

  protected void buildRootNodeEnd(XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.elementEnd();
  }

  protected void buildHeaderInfomation(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // The sequencing is important of the following tags (and not
    // quite what is defined in the PHMR report!), refer to
    // Boone chapter 14.
    xmlBuilder.element("realmCode").attribute("code", document.getRealmCode()).elementShortEnd();
    // CONF-PHMR-? - SECTION 2.6
    xmlBuilder
        .element("typeId")
        .attribute("root", document.getTypeIdRoot())
        .attribute("extension", document.getTypeIdExtension())
        .elementShortEnd();

    // CONF-DK PHMR-2

    buildHeaderTemplateId(document.getTemplateIds(), xmlBuilder);

    if (document.getId() == null) {
      throw new IOException("Header information ID, must be set");
    }

    // CONF-PHMR-DK-19
    BuildUtil.buildIdExtensionOptional(document.getId(), xmlBuilder);

    // CONF-PHMR-3:
    xmlBuilder
        .element("code")
        .attribute("code", document.getCode())
        .attribute("codeSystem", Loinc.OID)
        .attribute("codeSystemName", Loinc.DISPLAYNAME)
        .attribute("displayName", document.getCodeDisplayName())
        .elementShortEnd();
  }

  protected void buildHeaderTemplateId(String[] templateIds, XmlStreamBuilder xmlBuilder) throws IOException {
    BuildUtil.buildTemplateIds(xmlBuilder, templateIds);
  }

  protected void buildContext(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // CONF-PHMR-15
    xmlBuilder.element("title").value(document.getTitle()).elementEnd();
    // CONF-PHMR-16 / CONF-DK PHMR-17
    xmlBuilder
        .element("effectiveTime")
        .attribute("value", dateTimeformatter.format(document.getEffectiveTime()))
        .elementShortEnd();

    xmlBuilder
        .element("confidentialityCode")
        .attribute("code", "N")
        .attribute("codeSystem", HL7.CONFIDENTIALITY_OID)
        .elementShortEnd();

    // CONF-PHMR-17-20
    xmlBuilder.element("languageCode").attribute("code", document.getLanguageCode()).elementShortEnd();

    // CONF-DK PHMR-22
    BuildUtil.buildSetIdExtensionOptional(document.getSetId(), xmlBuilder);
    if (document.getVersionNumber() != null) {
      xmlBuilder.element("versionNumber").attribute("value", document.getVersionNumber().toString()).elementShortEnd();
    }
  }

  /**
   * Generate address based on citizen
   * 
   * @param addressData
   *          used for constructing JAXB objets
   * @param xmlBuilder
   * @return the populated ADExplicit object
   */
  protected void buildAddress(AddressData addressData, boolean includeNullFlavorAdr, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (addressData != null && addressData.getStreet().length > 0) {
      if (AddressData.Use.WorkPlace.equals(addressData.getAddressUse())) {
        xmlBuilder.element("addr").attribute("use", "WP");
      } else if (AddressData.Use.MobileContact.equals(addressData.getAddressUse())) {
        xmlBuilder.element("addr").attribute("use", "MC");
      } else {
        xmlBuilder.element("addr").attribute("use", "H");
      }

      // Street
      for (String street : addressData.getStreet()) {
        xmlBuilder.element("streetAddressLine").value(street).elementEnd();
      }

      // Zip code
      if (addressData.getPostalCode() != null) {
        xmlBuilder.element("postalCode").value(addressData.getPostalCode()).elementEnd();
      } else {
        BuildUtil.buildNullFlavor("postalCode", xmlBuilder);
      }

      // City
      if (addressData.getCity() != null) {
        xmlBuilder.element("city").value(addressData.getCity()).elementEnd();
      } else {
        BuildUtil.buildNullFlavor("city", xmlBuilder);
      }

      // Country
      if (addressData.getCountry() != null) {
        xmlBuilder.element("country").value(addressData.getCountry()).elementEnd();
      }
      xmlBuilder.elementEnd(); // end addr
    } else {
      if (includeNullFlavorAdr) {
        xmlBuilder.element("addr").attribute("use", "WP");
        BuildUtil.buildNullFlavor("streetAddressLine", xmlBuilder);
        xmlBuilder.elementEnd(); // end addr    		
      }
    }
  }

  protected void buildPatientSection(E document, XmlStreamBuilder xmlBuilder) throws IOException {
    Patient patientIdentity = document.getPatient();
    // CONF-PHMR-24-28
    xmlBuilder.element("recordTarget").attribute("typeCode", "RCT").attribute("contextControlCode", "OP");

    xmlBuilder.element("patientRole").attribute("classCode", "PAT");

    // If the person is not defined, add a null flavor
    if (document.getPatient().getNullFlavor() != null) {
      xmlBuilder.element("id").attribute("nullFlavor", "NI").elementShortEnd();
    } else {
      // Decision - we use CPR number as ID under the codeSystem
      // of Danish CPR
      BuildUtil.buildId(patientIdentity.getId(), xmlBuilder);

      // ADDRESS
      // CONF-PHMR-5
      buildAddress(patientIdentity.getAddress(), true, xmlBuilder);
      buildTelecom(patientIdentity.getTelecomList(), xmlBuilder);

      // PATIENT
      // The CDA book p 161
      // CONF-PHMR-4
      xmlBuilder.element("patient").attribute("classCode", "PSN").attribute("determinerCode", "INSTANCE");
      buildName(patientIdentity, xmlBuilder);

      buildGender(patientIdentity, xmlBuilder);
      buildBirthTime(patientIdentity, xmlBuilder);

      xmlBuilder.elementEnd(); // end patient
    }
    buildPatientExtra(document, xmlBuilder);
    xmlBuilder.elementEnd(); // end patientRole
    xmlBuilder.elementEnd(); // end recordTarget

  }

  protected void buildPatientExtra(E document, XmlStreamBuilder xmlBuilder) throws IOException {

  }

  protected void buildBirthTime(Patient patientIdentity, XmlStreamBuilder xmlBuilder) throws IOException {
    if (patientIdentity.getBirthTime() != null) {
      xmlBuilder
          .element("birthTime")
          .attribute("value", birthTimeFormatter.format(patientIdentity.getBirthTime()))
          .elementShortEnd();
    } else {
      BuildUtil.buildNullFlavor("birthTime", xmlBuilder);
    }
  }

  private void buildGender(Patient patientIdentity, XmlStreamBuilder xmlBuilder) throws IOException {
    String gender;
    switch (patientIdentity.getGender()) {
    case Female:
      gender = "F";
      break;
    case Male:
      gender = "M";
      break;
    default: // Undifferentiated
      gender = "UN";
      break;
    }
    xmlBuilder
        .element("administrativeGenderCode")
        .attribute("code", gender)
        .attribute("codeSystem", HL7.GENDER_OID)
        .elementShortEnd();
  }

  protected void buildAuthorSection(Patient patient, Participant author, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (author != null) {
      if (patient != null && patient.getId() != null && author.getId() != null
          && patient.getId().getExtension().equalsIgnoreCase(author.getId().getExtension())) {
        buildSelfAuthorSectionConsiderRepresentedOrganization(patient, author.getTime(),
            author.getOrganizationIdentity(), xmlBuilder);
      } else {
        // Section 2.13.2
        // The CDA book p 151
        xmlBuilder.element("author").attribute("typeCode", "AUT").attribute("contextControlCode", "OP");
        if (author.getTime() != null) {
          xmlBuilder.element("time").attribute("value", dateTimeformatter.format(author.getTime())).elementShortEnd();
        }

        xmlBuilder.element("assignedAuthor").attribute("classCode", "ASSIGNED");
        BuildUtil.buildId(author.getId(), xmlBuilder);
        buildAddress(author.getAddress(), true, xmlBuilder);
        buildTelecom(author.getTelecomList(), xmlBuilder);
        // assignedPerson
        if (author.getPersonIdentity() != null) {
          xmlBuilder.element("assignedPerson").attribute("classCode", "PSN").attribute("determinerCode", "INSTANCE");
          buildName(author.getPersonIdentity(), xmlBuilder);
          xmlBuilder.elementEnd(); // end assignedPerson
        }
        // representedOrganization
        buildRepresentedOrganization(author.getOrganizationIdentity(), xmlBuilder);
        xmlBuilder.elementEnd(); // end assignedAuthor
        xmlBuilder.elementEnd(); // end author
      }
    }
  }

  protected void buildSelfAuthorSectionConsiderRepresentedOrganization(Patient patient, Date time,
      OrganizationIdentity organizationIdentity, XmlStreamBuilder xmlBuilder) throws IOException {
    // organizationIdentity håndteres ikke default, men er det behov for det, overstyres det i det enkelte dokument. Som eks. QRD
    buildSelfAuthorSection(patient, time, null, xmlBuilder);
  }

  protected void buildSelfAuthorSection(Patient patient, Date time, OrganizationIdentity organizationIdentity,
      XmlStreamBuilder xmlBuilder) throws IOException {
    if (patient != null) {
      // Section 2.13.2
      // The CDA book p 151
      xmlBuilder.element("author").attribute("typeCode", "AUT").attribute("contextControlCode", "OP");
      if (time != null) {
        xmlBuilder.element("time").attribute("value", dateTimeformatter.format(time)).elementShortEnd();
      }
      xmlBuilder.element("assignedAuthor").attribute("classCode", "ASSIGNED");
      BuildUtil.buildId(patient.getId(), xmlBuilder);
      xmlBuilder
          .element("code")
          .attribute("code", HL7.SELF_REFERENCE.getCode())
          .attribute("displayName", HL7.SELF_REFERENCE.getDisplayName())
          .attribute("codeSystem", HL7.SELF_REFERENCE.getCodeSystem())
          .attribute("codeSystemName", HL7.SELF_REFERENCE.getCodeSystemName())
          .elementShortEnd();
      buildAddress(patient.getAddress(), true, xmlBuilder);
      buildTelecom(patient.getTelecomList(), xmlBuilder);
      // assignedPerson
      xmlBuilder.element("assignedPerson").attribute("classCode", "PSN").attribute("determinerCode", "INSTANCE");
      buildName(patient, xmlBuilder);
      xmlBuilder.elementEnd(); // end assignedPerson
      // representedOrganization
      if (organizationIdentity != null) {
        buildRepresentedOrganization(organizationIdentity, xmlBuilder);
      }
      xmlBuilder.elementEnd(); // end assignedAuthor
      xmlBuilder.elementEnd(); // end author
    }
  }

  protected void buildSORId(OrganizationIdentity authorOrganizationIdentity, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (authorOrganizationIdentity.getIdValue() != null) {
      xmlBuilder
          .element("id")
          .attribute("extension", authorOrganizationIdentity.getIdValue())
          .attribute("root", NSI.SOR_OID)
          .attribute("assigningAuthorityName", NSI.SOR_AUTHORITYNAME)
          .elementShortEnd();
    }
  }

  protected void buildRepresentedOrganization(OrganizationIdentity organizationIdentity, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (organizationIdentity != null && organizationIdentity.getOrgName() != null
        && !organizationIdentity.getOrgName().trim().isEmpty()) {
      xmlBuilder.element("representedOrganization").attribute("classCode", "ORG").attribute("determinerCode",
          "INSTANCE");
      BuildUtil.buildIdOptional(organizationIdentity.getId(), xmlBuilder);
      xmlBuilder.element("name").value(organizationIdentity.getOrgName()).elementEnd();
      BuildUtil.buildNullFlavor("telecom", xmlBuilder);
      xmlBuilder.element("addr").attribute("use", "WP");
      BuildUtil.buildNullFlavor("streetAddressLine", xmlBuilder);
      xmlBuilder.elementEnd(); // end addr
      xmlBuilder.elementEnd(); // end representedOrganization
    }
  }

  protected void buildCustodianSection(ClinicalDocument document, boolean includeNullFlavorAdr,
      XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("custodian").attribute("typeCode", "CST");
    xmlBuilder.element("assignedCustodian").attribute("classCode", "ASSIGNED");
    xmlBuilder.element("representedCustodianOrganization").attribute("classCode", "ORG").attribute("determinerCode",
        "INSTANCE");
    if (document.getCustodianIdentity() != null) {
      OrganizationIdentity custodianIdentity = document.getCustodianIdentity();
      BuildUtil.buildId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME, custodianIdentity.getIdValue(), xmlBuilder);
      xmlBuilder.element("name").value(custodianIdentity.getOrgName()).elementEnd();
      buildTelecom(custodianIdentity.getTelecomList(), xmlBuilder);
      buildAddress(custodianIdentity.getAddress(), includeNullFlavorAdr, xmlBuilder);
    } else {
      BuildUtil.buildNullFlavor("id", xmlBuilder);
      BuildUtil.buildNullFlavor("name", xmlBuilder);
      if (includeNullFlavorAdr) {
        BuildUtil.buildNullFlavor("telecom", xmlBuilder);
        BuildUtil.buildNullFlavor("addr", xmlBuilder);
      }
    }
    xmlBuilder.elementEnd(); // end representedCustodianOrganization
    xmlBuilder.elementEnd(); // end assignedCustodian
    xmlBuilder.elementEnd(); // end custodian
  }

  protected void buildLegalAuthenticatorSection(ClinicalDocument document, XmlStreamBuilder xmlBuilder)
      throws IOException {
    Participant legalAuthenticator = document.getLegalAuthenticator();
    if (legalAuthenticator != null) {
      xmlBuilder.element("legalAuthenticator").attribute("typeCode", "LA").attribute("contextControlCode", "OP");
      if (legalAuthenticator.getTime() != null) {
        xmlBuilder
            .element("time")
            .attribute("value", dateTimeformatter.format(legalAuthenticator.getTime()))
            .elementShortEnd();
      } else {
        BuildUtil.buildNullFlavor("time", xmlBuilder);
      }
      BuildUtil.buildNullFlavor("signatureCode", xmlBuilder);

      buildAssignedEntity(legalAuthenticator, xmlBuilder);
      xmlBuilder.elementEnd(); // end legalAuthenticator
    }
  }

  protected void buildAssignedEntity(Participant p, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("assignedEntity").attribute("classCode", "ASSIGNED");
    BuildUtil.buildId(p.getId(), xmlBuilder);
    buildAddress(p.getAddress(), true, xmlBuilder);
    buildTelecom(p.getTelecomList(), xmlBuilder);
    if (p.getPersonIdentity() != null) {
      xmlBuilder.element("assignedPerson").attribute("classCode", "PSN").attribute("determinerCode", "INSTANCE");
      buildName(p.getPersonIdentity(), xmlBuilder);
      xmlBuilder.elementEnd(); // end assignedPerson	
    }
    buildRepresentedOrganization(p.getOrganizationIdentity(), xmlBuilder);

    xmlBuilder.elementEnd(); // end assignedEntity
  }

  protected void buildStructuredBodySectionStart(XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("structuredBody").attribute("classCode", "DOCBODY").attribute("moodCode", "EVN");
  }

  protected void buildStructuredBodySectionEnd(XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.elementEnd(); // end structuredBody
    xmlBuilder.elementEnd(); // end component
  }

  /**
   * Create PNExplicit structure (name)
   * 
   * @param personIdentity
   *          The PersonIdentity class to build name structure for
   * @param xmlBuilder
   * @return the PNExplicit element
   */
  protected void buildName(PersonIdentity personIdentity, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("name");
    if (personIdentity.hasPrefix()) {
      xmlBuilder.element("prefix").value(personIdentity.getPrefix()).elementEnd();
    }

    if (personIdentity.hasGivenName()) {
      for (int i = 0; i < personIdentity.getGivenNames().length; i++) {
        xmlBuilder.element("given").value(personIdentity.getGivenNames()[i]).elementEnd();
      }
    } else {
      xmlBuilder.element("given").attribute("nullFlavor", "NI").elementShortEnd();
    }

    if (personIdentity.hasFamilyName()) {
      xmlBuilder.element("family").value(personIdentity.getFamilyName()).elementEnd();
    } else {
      xmlBuilder.element("family").attribute("nullFlavor", "NI").elementShortEnd();
    }
    xmlBuilder.elementEnd();
  }

  /**
   * Generic method for create telecom
   * 
   * @param address
   * @param protocol
   * @param use
   * @return
   */
  protected void buildTelecom(Telecom[] telecoms, XmlStreamBuilder xmlBuilder) throws IOException {
    if (telecoms != null) {
      for (Telecom telecom : telecoms) {
        if (telecom.getValue() != null) {
          xmlBuilder
              .element("telecom")
              .attribute("value", telecom.getProtocol() + ":" + telecom.getValue())
              .attribute("use", translateToHL7String(telecom.getAddressUse()))
              .elementShortEnd();
        } else {
          xmlBuilder.element("telecom").attribute("nullFlavor", "NI").elementShortEnd();
        }
      }
    }
  }

  protected static void buildMethodCode(String code, String codeSystem, String displayName, String codeSystemName,
      XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder
        .element("methodCode")
        .attribute("code", code)
        .attribute("codeSystem", codeSystem)
        .attribute("displayName", displayName)
        .attribute("codeSystemName", codeSystemName)
        .elementShortEnd();

  }

  protected static void buildValueAsPQ(Measurement measurement, XmlStreamBuilder xmlBuilder) throws IOException {
    if (measurement.getValue() != null) {
      if (measurement.getUnit() != null) {
        xmlBuilder
            .element("value")
            .attribute("unit", measurement.getUnit())
            .attribute("value", measurement.getValue())
            .attribute("xsi:type", "PQ")
            .elementShortEnd();
      } else {
        xmlBuilder
            .element("value")
            .attribute("value", measurement.getValue())
            .attribute("xsi:type", "PQ")
            .elementShortEnd();
      }
    } else {
      buildValueAsIntervalPQ(measurement, xmlBuilder);
    }
  }

  protected static void buildValueAsIntervalPQ(Measurement measurement, XmlStreamBuilder xmlBuilder)
      throws IOException {
    xmlBuilder.element("value").attribute("xsi:type", "IVL_PQ").element("low");
    if (measurement.getValueInterval().isLowValueUnknown()) {
      xmlBuilder.attribute("nullFlavor", "UNK");
    } else {
      xmlBuilder
          .attribute("unit", measurement.getUnit())
          .attribute("value", measurement.getValueInterval().getLowValue())
          .attribute("inclusive", measurement.getValueInterval().isLowValueInclusive() ? "true" : "false")
          .attribute("xsi:type", "IVXB_PQ");
    }
    xmlBuilder
        .elementShortEnd() // low
        .element("high");
    if (measurement.getValueInterval().isHighValueUnknown()) {
      xmlBuilder.attribute("nullFlavor", "UNK");
    } else {
      xmlBuilder
          .attribute("unit", measurement.getUnit())
          .attribute("value", measurement.getValueInterval().getHighValue())
          .attribute("inclusive", measurement.getValueInterval().isHighValueInclusive() ? "true" : "false")
          .attribute("xsi:type", "IVXB_PQ");
    }
    xmlBuilder
        .elementShortEnd() // high
        .elementEnd(); // outer value
  }

  protected static boolean isNotNullAndEmpty(String text) {
    return text != null && !text.trim().isEmpty();
  }

  protected static void buildEffectiveTime(Measurement measurement, XmlStreamBuilder xmlBuilder) throws IOException {
    if (measurement.getTimestamp() != null) {
      xmlBuilder
          .element("effectiveTime")
          .attribute("value",
              dateTimeformatter.format((measurement.getTimestamp() != null ? measurement.getTimestamp() : new Date())))
          .elementShortEnd();
    }
  }

  protected static void buildEffectiveTime(Date timestamp, XmlStreamBuilder xmlBuilder) throws IOException {
    if (timestamp != null) {
      xmlBuilder.element("effectiveTime").attribute("value", dateTimeformatter.format((timestamp))).elementShortEnd();
    }
  }

  protected void buildInformationRecipient(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    List<Participant> informationRecipients = document.getInformationRecipients();
    if (informationRecipients != null) {
      for (Participant participant : informationRecipients) {
        xmlBuilder.element("informationRecipient").attribute("typeCode", "PRCP");
        xmlBuilder.element("intendedRecipient").attribute("classCode", "ASSIGNED");
        BuildUtil.buildId(participant.getId(), xmlBuilder);
        buildAddress(participant.getAddress(), true, xmlBuilder);
        buildTelecom(participant.getTelecomList(), xmlBuilder);

        // informationRecipient
        if (participant.getPersonIdentity() != null) {
          xmlBuilder.element("informationRecipient").attribute("classCode", "PSN").attribute("determinerCode",
              "INSTANCE");
          buildName(participant.getPersonIdentity(), xmlBuilder);
          xmlBuilder.elementEnd(); // end assignedPerson
        }

        xmlBuilder.element("receivedOrganization").attribute("classCode", "ORG").attribute("determinerCode",
            "INSTANCE");

        if (participant.getOrganizationIdentity() != null) {
          BuildUtil.buildId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME, participant.getOrganizationIdentity().getIdValue(),
              xmlBuilder);
          xmlBuilder.element("name").value(participant.getOrganizationIdentity().getOrgName()).elementEnd();
          buildTelecom(participant.getOrganizationIdentity().getTelecomList(), xmlBuilder);

          buildAddress(participant.getOrganizationIdentity().getAddress(), true, xmlBuilder);
        } else {
          BuildUtil.buildNullFlavor("id", xmlBuilder);
          BuildUtil.buildNullFlavor("name", xmlBuilder);
          BuildUtil.buildNullFlavor("telecom", xmlBuilder);
          BuildUtil.buildNullFlavor("addr", xmlBuilder);
        }
        xmlBuilder.elementEnd(); // end receivedOrganization
        xmlBuilder.elementEnd(); // end intendedRecipient
        xmlBuilder.elementEnd(); // end informationRecipient
      }
    }
  }

  protected void buildDataEnterer(ClinicalDocument document, boolean includeNullFlavorAdr, XmlStreamBuilder xmlBuilder)
      throws IOException {
    Participant dataEnterer = document.getDataEnterer();
    if (dataEnterer != null) {
      xmlBuilder.element("dataEnterer").attribute("typeCode", "ENT");
      xmlBuilder.element("assignedEntity").attribute("classCode", "ASSIGNED");
      BuildUtil.buildId(dataEnterer.getId(), xmlBuilder);
      buildAddress(dataEnterer.getAddress(), true, xmlBuilder);
      buildTelecom(dataEnterer.getTelecomList(), xmlBuilder);
      PersonIdentity personIdentity = dataEnterer.getPersonIdentity();
      if (personIdentity != null) {
        xmlBuilder.element("assignedPerson").attribute("classCode", "PSN");
        buildName(personIdentity, xmlBuilder);
        xmlBuilder.elementEnd(); // end assignedPerson
      }
      xmlBuilder.element("representedOrganization").attribute("classCode", "ORG").attribute("determinerCode",
          "INSTANCE");

      if (dataEnterer.getOrganizationIdentity() != null) {
        BuildUtil.buildId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME, dataEnterer.getOrganizationIdentity().getIdValue(),
            xmlBuilder);
        xmlBuilder.element("name").value(dataEnterer.getOrganizationIdentity().getOrgName()).elementEnd();

        buildTelecom(dataEnterer.getOrganizationIdentity().getTelecomList(), xmlBuilder);
        buildAddress(dataEnterer.getOrganizationIdentity().getAddress(), includeNullFlavorAdr, xmlBuilder);
      } else {
        BuildUtil.buildNullFlavor("id", xmlBuilder);
        BuildUtil.buildNullFlavor("name", xmlBuilder);
        if (includeNullFlavorAdr) {
          BuildUtil.buildNullFlavor("telecom", xmlBuilder);
          BuildUtil.buildNullFlavor("addr", xmlBuilder);
        }
      }
      xmlBuilder.elementEnd(); // end representedOrganization
      xmlBuilder.elementEnd(); // end AssignedEntity
      xmlBuilder.elementEnd(); // end dataEnterer
    }
  }

  protected void buildParticipants(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    List<Participant> participants = document.getParticipants();
    if (participants != null) {
      for (Participant participant : participants) {
        xmlBuilder.element("participant").attribute("typeCode", "IND");
        if (participant.getRoleType() == null) {
          logger.warn("The participant has no roleType! - defaulting to " + ROLE_TYPE.PRS);
          xmlBuilder.element("associatedEntity").attribute("classCode", ROLE_TYPE.PRS.name());
        } else {
          xmlBuilder.element("associatedEntity").attribute("classCode", participant.getRoleType().name());
        }

        buildAddress(participant.getAddress(), true, xmlBuilder);
        buildTelecom(participant.getTelecomList(), xmlBuilder);

        if (participant.getPersonIdentity() != null) {
          xmlBuilder.element("associatedPerson").attribute("classCode", "PSN");
          buildName(participant.getPersonIdentity(), xmlBuilder);
          xmlBuilder.elementEnd(); // end associatedPerson
        }

        xmlBuilder.element("scopingOrganization").attribute("classCode", "ORG").attribute("determinerCode", "INSTANCE");

        if (participant.getOrganizationIdentity() != null) {
          BuildUtil.buildId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME, participant.getOrganizationIdentity().getIdValue(),
              xmlBuilder);
          xmlBuilder.element("name").value(participant.getOrganizationIdentity().getOrgName()).elementEnd();

          buildTelecom(participant.getOrganizationIdentity().getTelecomList(), xmlBuilder);
          buildAddress(participant.getOrganizationIdentity().getAddress(), true, xmlBuilder);
        } else {
          BuildUtil.buildNullFlavor("id", xmlBuilder);
          BuildUtil.buildNullFlavor("name", xmlBuilder);
          BuildUtil.buildNullFlavor("telecom", xmlBuilder);
          BuildUtil.buildNullFlavor("addr", xmlBuilder);
        }
        xmlBuilder.elementEnd(); // end representedOrganization

        xmlBuilder.elementEnd(); // end assciatedEntity
        xmlBuilder.elementEnd(); // end participant
      }
    }
  }

  private String translateToHL7String(AddressData.Use use) {
    String useString = "ERROR";
    if (use == AddressData.Use.HomeAddress) {
      useString = "H";
    }
    if (use == AddressData.Use.WorkPlace) {
      useString = "WP";
    }
    if (use == AddressData.Use.MobileContact) {
      useString = "MC";
    }
    return useString;
  }

  public void buildHeader(E clinicalDocument, XmlStreamBuilder xmlBuilder) throws IOException {
    buildHeaderInfomation(clinicalDocument, xmlBuilder);
    buildContext(clinicalDocument, xmlBuilder);
    buildPatientSection(clinicalDocument, xmlBuilder);
    buildAuthorSection(clinicalDocument.getPatient(), clinicalDocument.getAuthor(), xmlBuilder);
    buildDataEnterer(clinicalDocument, true, xmlBuilder);
    buildCustodianSection(clinicalDocument, true, xmlBuilder);
    buildInformationRecipient(clinicalDocument, xmlBuilder);
    buildLegalAuthenticatorSection(clinicalDocument, xmlBuilder);
    buildParticipants(clinicalDocument, xmlBuilder);
    buildDocumentationOf(clinicalDocument, xmlBuilder);
  }

  protected abstract void buildBody(E baseClinicalDocument, XmlStreamBuilder xmlBuilder,
      NarrativeTextConverter<C> narrativeTextConverter) throws IOException;

  protected abstract void buildDocumentationOf(E document, XmlStreamBuilder xmlBuilder) throws IOException;

  @Override
  public void serialize(E source, Appendable target) {
    this.serialize(source, target, null);
  }

  public void serialize(E source, Appendable target, NarrativeTextConverter<C> narrativeTextConverter) {
    try {
      XmlStreamBuilder xmlBuilder = new XmlStreamBuilder(target, xmlPrettyPrinter);
      buildRootNode(xmlBuilder);
      buildHeader(source, xmlBuilder);
      buildBody(source, xmlBuilder, narrativeTextConverter);
      buildRootNodeEnd(xmlBuilder);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }
}