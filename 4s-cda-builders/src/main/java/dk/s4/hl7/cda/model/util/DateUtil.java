package dk.s4.hl7.cda.model.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Helper methods for creating dates
 */
public class DateUtil {
  private static Calendar createDanishCalendar() {
    return Calendar.getInstance(new Locale("da-DK"));
  }

  private static Calendar createDanishCalendarWithTimeZone() {
    return Calendar.getInstance(TimeZone.getTimeZone("Europe/Copenhagen"), new Locale("da-DK"));
  }

  public static Calendar createDanishCalendarWithTimeZone(Date date) {
    Calendar danishCalendar = createDanishCalendarWithTimeZone();
    danishCalendar.setTime(date);
    return danishCalendar;

  }

  private static Calendar createUTCCalendar() {
    return Calendar.getInstance(TimeZone.getTimeZone("UTC"));
  }

  /**
   * Create a date based on a Danish Calendar
   * 
   * @param year
   * @param month
   * @param day
   * @param hour
   * @param min
   * @param sec
   * @return
   */
  public static Date makeDanishDateTime(int year, int month, int day, int hour, int min, int sec) {
    Calendar danishCalendar = createDanishCalendar();
    danishCalendar.set(year, month, day, hour, min, sec);
    return danishCalendar.getTime();
  }

  public static Date makeDanishDateTimeWithTimeZone(int year, int month, int day, int hour, int min, int sec) {
    Calendar danishCalendar = createDanishCalendarWithTimeZone();
    danishCalendar.set(year, month, day, hour, min, sec);
    return danishCalendar.getTime();
  }

  /**
   * Create a date based on a UTCCalendar
   * 
   * @param year
   * @param month
   * @param day
   * @return
   */
  public static Date makeUtcDate(int year, int month, int day) {
    Calendar utcCalendar = createUTCCalendar();
    utcCalendar.set(year, month, day, 0, 0, 0);
    return utcCalendar.getTime();
  }
}
