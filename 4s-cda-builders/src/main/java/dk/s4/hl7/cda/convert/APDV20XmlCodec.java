package dk.s4.hl7.cda.convert;

import java.io.Reader;

import dk.s4.hl7.cda.convert.base.AppendableSerializer;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.convert.base.ReaderSerializer;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;

/**
 * The implementation of Codec to encode and decode Appointment from objects to XML
 * and XML to objects. After construction the codec is considered thread-safe.
 * 
 * @see <a href="https://www.dartlang.org/articles/libraries/converters-and-codecs">https://www.dartlang.org/articles/libraries/converters-and-codecs</a>
 * 
 */
public class APDV20XmlCodec implements Codec<AppointmentDocument, String>, AppendableSerializer<AppointmentDocument>,
    ReaderSerializer<AppointmentDocument> {

  private APDV20XmlConverter apdV20XmlConverter;
  private XmlAPDV20Converter xmlApdV20Converter;

  public APDV20XmlCodec() {
    this(new XmlPrettyPrinter());
  }

  public APDV20XmlCodec(XmlPrettyPrinter xmlPrettyPrinter) {
    this.apdV20XmlConverter = new APDV20XmlConverter(xmlPrettyPrinter);
    this.xmlApdV20Converter = new XmlAPDV20Converter();
  }

  public String encode(AppointmentDocument source) {
    return apdV20XmlConverter.convert(source);
  }

  public AppointmentDocument decode(String source) {
    return xmlApdV20Converter.convert(source);
  }

  @Override
  public void serialize(AppointmentDocument source, Appendable appendable) {
    if (appendable == null) {
      throw new NullPointerException("Target appendable is null");
    }
    apdV20XmlConverter.serialize(source, appendable);
  }

  @Override
  public AppointmentDocument deserialize(Reader source) {
    return xmlApdV20Converter.deserialize(source);
  }
}
