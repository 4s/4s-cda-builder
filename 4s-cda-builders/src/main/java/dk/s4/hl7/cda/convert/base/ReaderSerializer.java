package dk.s4.hl7.cda.convert.base;

import java.io.Reader;

/**
 * Interface for deserializers to support parsing from a Reader object
 */
public interface ReaderSerializer<T> {
  T deserialize(Reader source);
}
