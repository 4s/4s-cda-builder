package dk.s4.hl7.cda.convert.decode.qfdd;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

import java.util.ArrayList;
import java.util.List;

public class CopyRightHandler extends BaseXmlHandler {
  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section/entry/observation";

  private boolean isCopyRightSection;
  private List<String> copyRightTexts;

  public CopyRightHandler() {
    addPath(COMPONENT_SECTION + "/templateId");
    addPath(COMPONENT_SECTION + "/value");
    isCopyRightSection = false;
    this.copyRightTexts = new ArrayList<>();
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      isCopyRightSection = HL7.QFDD_COPYRIGHT_PATTERN_TEMPLATEID.equalsIgnoreCase(xmlElement.getAttributeValue("root"));
    } else if (isCopyRightSection
        && ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type")) {
      copyRightTexts.add(xmlElement.getElementValue());
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

  public boolean isCopyRightSection() {
    return isCopyRightSection;
  }

  public List<String> getCopyRightTexts() {
    return copyRightTexts;
  }

  public void clear() {
    this.isCopyRightSection = false;
    this.copyRightTexts = new ArrayList<>();
  }

}
