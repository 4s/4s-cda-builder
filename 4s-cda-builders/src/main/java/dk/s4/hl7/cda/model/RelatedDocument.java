package dk.s4.hl7.cda.model;

public class RelatedDocument {

  protected String id = null;
  protected String setId = null;
  protected Integer versionNumber = null;
  protected boolean isReplaced;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getSetId() {
    return setId;
  }

  public void setSetId(String setId) {
    this.setId = setId;
  }

  public Integer getVersionNumber() {
    return versionNumber;
  }

  public void setVersionNumber(Integer versionNumber) {
    this.versionNumber = versionNumber;
  }

  public boolean isReplaced() {
    return isReplaced;
  }

  public void setReplaced(boolean isReplaced) {
    this.isReplaced = isReplaced;
  }

}
