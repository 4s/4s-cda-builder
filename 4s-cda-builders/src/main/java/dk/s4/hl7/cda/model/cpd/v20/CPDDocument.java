package dk.s4.hl7.cda.model.cpd.v20;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.core.v20.BaseClinicalDocument;

public class CPDDocument extends BaseClinicalDocument {

  public enum Status {
    ACTIVE,
    COMPLETED
  }

  private CPDSection<CPDHealthStatusComponent> healthConcernSection;
  private CPDSection<CPDGoalObservationComponent> goalSection;
  private CPDSection<CPDInterventionActComponent> interventionSection;
  private CPDSection<CPDOutcomeObservationComponent> outcomeSection;

  public CPDDocument(ID id) {
    super(HL7.CDA_TYPEID_ROOT, HL7.CDA_TYPEID_EXTENSION, id, Loinc.CPD_CODE, Loinc.CPD_DISPLAYNAME,
        new String[] { MedCom.DK_CPD_ROOT_OID }, HL7.REALM_CODE_DK);
  }

  public CPDSection<CPDHealthStatusComponent> getHealthConcernSection() {
    return healthConcernSection;
  }

  public void setHealthConcernSection(CPDSection<CPDHealthStatusComponent> healthConcernSection) {
    this.healthConcernSection = healthConcernSection;
  }

  public CPDSection<CPDGoalObservationComponent> getGoalSection() {
    return goalSection;
  }

  public void setGoalSection(CPDSection<CPDGoalObservationComponent> goalSection) {
    this.goalSection = goalSection;
  }

  public CPDSection<CPDInterventionActComponent> getInterventionSection() {
    return interventionSection;
  }

  public void setInterventionSection(CPDSection<CPDInterventionActComponent> interventionSection) {
    this.interventionSection = interventionSection;
  }

  public CPDSection<CPDOutcomeObservationComponent> getOutcomeSection() {
    return outcomeSection;
  }

  public void setOutcomeSection(CPDSection<CPDOutcomeObservationComponent> outcomeSection) {
    this.outcomeSection = outcomeSection;
  }

}
