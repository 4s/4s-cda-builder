package dk.s4.hl7.cda.convert.decode.cdametadata;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.cdametadata.CDAMetadata;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class SectionHandler implements CDAXmlHandler<CDAMetadata> {

  private CodedValue confidentialyCode;

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "confidentialityCode", "code")) {
      confidentialyCode = new CodedValue.CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .build();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    //ignore
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add("/ClinicalDocument/confidentialityCode", this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove("/ClinicalDocument/confidentialityCode");
  }

  @Override
  public void addDataToDocument(CDAMetadata clinicalDocument) {
    clinicalDocument.setConfidentialityCodeCodedValue(confidentialyCode);
  }

}