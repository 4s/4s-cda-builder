package dk.s4.hl7.cda.convert.decode.header.v20;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.RelatedDocument;
import dk.s4.hl7.cda.model.core.v20.ClinicalDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class RelatedDocumentHandler<C extends ClinicalDocument> implements CDAXmlHandler<C> {

  private static Logger logger = LoggerFactory.getLogger(RelatedDocumentHandler.class);
  private String id = null;
  private String setId = null;
  private String versionNumber = null;
  private boolean isReplaced;

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "relatedDocument", "typeCode")) {
      if (xmlElement.getAttributeValue("typeCode").equals("RPLC")) {
        isReplaced = true;
      } else {
        isReplaced = false;
      }
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "root")) {
      id = xmlElement.getAttributeValue("root");

    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "setId", "root")) {
      setId = xmlElement.getAttributeValue("root");

    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "versionNumber", "value")) {
      versionNumber = xmlElement.getAttributeValue("value");
    }

  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {

  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add("/ClinicalDocument/relatedDocument", this);
    xmlMapping.add("/ClinicalDocument/relatedDocument/parentDocument/setId", this);
    xmlMapping.add("/ClinicalDocument/relatedDocument/parentDocument/id", this);
    xmlMapping.add("/ClinicalDocument/relatedDocument/parentDocument/code", this);
    xmlMapping.add("/ClinicalDocument/relatedDocument/parentDocument/versionNumber", this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove("/ClinicalDocument/relatedDocument");
    xmlMapping.remove("/ClinicalDocument/relatedDocument/parentDocument/code");
    xmlMapping.remove("/ClinicalDocument/relatedDocument/parentDocument/setId");
    xmlMapping.remove("/ClinicalDocument/relatedDocument/parentDocument/id");
    xmlMapping.remove("/ClinicalDocument/relatedDocument/parentDocument/versionNumber");
  }

  private Integer toInteger(String versionNumber) {
    if (versionNumber != null) {
      try {
        return Integer.parseInt(versionNumber);
      } catch (Exception ex) {
        logger.warn("Document version is not an integer value: " + versionNumber);
        return null;
      }
    }
    return null;
  }

  @Override
  public void addDataToDocument(C clinicalDocument) {
    if (id != null) {
      RelatedDocument relatedDocument = new RelatedDocument();
      relatedDocument.setReplaced(isReplaced);
      relatedDocument.setId(id);
      relatedDocument.setSetId(setId);
      relatedDocument.setVersionNumber(toInteger(versionNumber));
      clinicalDocument.setRelateDocument(relatedDocument);
    }
  }
}