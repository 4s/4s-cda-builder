package dk.s4.hl7.cda.model.pdc.v30;

import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.core.v20.BaseClinicalDocument;

/**
 * An implementation of BaseClinicalDocument that only supports the data of the
 * Danish Pdc version 3.0
 * 
 */
public class PDCDocument extends BaseClinicalDocument {

  private String text;
  private List<CustodyInformation> custodyInformationList;
  private NameAndAddressInformation nameAndAddressInformation;
  private CoverageGroup coverageGroup;
  private OrganDonorRegistration organDonorRegistration;
  private TreatmentWillRegistration treatmentWillRegistration;
  private LivingWillRegistration livingWillRegistration;
  private NoResuscitationRegistration noResuscitationRegistration;
  private ManuallyEnteredSpokenLanguage manuallyEnteredSpokenLanguage;
  private ManuallyEnteredTemporaryAddress manuallyEnteredTemporaryAddress;
  private ManuallyEnteredDentistInformation manuallyEnteredDentistInformation;
  private ManuallyEnteredContactInformation manuallyEnteredContactInformation;
  private List<ManuallyEnteredInformationAboutRelatives> manuallyEnteredInformationAboutRelativesList;

  public PDCDocument(ID id) {
    super(HL7.CDA_TYPEID_ROOT, HL7.CDA_TYPEID_EXTENSION, id, Loinc.PDC_CODE, Loinc.PDC_DISPLAYNAME,
        new String[] { MedCom.DK_PDC_ROOT_OID }, HL7.REALM_CODE_DK);
  }

  /**
   * @return String Returns a Text element containing the narrative text
   */
  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  /**
   * @return Returns a list of custody information returned from the CPR register.
   */
  public List<CustodyInformation> getCustodyInformationList() {
    return custodyInformationList;
  }

  public void setCustodyInformationList(List<CustodyInformation> custodyInformationList) {
    this.custodyInformationList = custodyInformationList;
  }

  /**
   * @return Returns Citizen’s name and address
   */
  public NameAndAddressInformation getNameAndAddressInformation() {
    return nameAndAddressInformation;
  }

  public void setNameAndAddressInformation(NameAndAddressInformation nameAndAddressInformation) {
    this.nameAndAddressInformation = nameAndAddressInformation;
  }

  /**
   * @return Returns Citizen’s coverage group returned from the CPR register.
   */
  public CoverageGroup getCoverageGroup() {
    return coverageGroup;
  }

  public void setCoverageGroup(CoverageGroup coverageGroup) {
    this.coverageGroup = coverageGroup;
  }

  /**
   * @return Returns whether the Citizen is registered as an organ donor or not.
   */
  public OrganDonorRegistration getOrganDonorRegistration() {
    return organDonorRegistration;
  }

  public void setOrganDonorRegistration(OrganDonorRegistration organDonorRegistration) {
    this.organDonorRegistration = organDonorRegistration;
  }

  /**
   * @return Returns whether the Citizen has registered a treatment will or not.
   */
  public TreatmentWillRegistration getTreatmentWillRegistration() {
    return treatmentWillRegistration;
  }

  public void setTreatmentWillRegistration(TreatmentWillRegistration treatmentWillRegistration) {
    this.treatmentWillRegistration = treatmentWillRegistration;
  }

  /**
   * @return Returns whether the Citizen has registered a living will or not.
   */
  public LivingWillRegistration getLivingWillRegistration() {
    return livingWillRegistration;
  }

  public void setLivingWillRegistration(LivingWillRegistration livingWillRegistration) {
    this.livingWillRegistration = livingWillRegistration;
  }

  /**
   * @return Returns whether the Citizen has registered NoResuscitation.
   */
  public NoResuscitationRegistration getNoResuscitationRegistration() {
    return noResuscitationRegistration;
  }

  public void setNoResuscitationRegistration(NoResuscitationRegistration noResuscitationRegistration) {
    this.noResuscitationRegistration = noResuscitationRegistration;
  }

  /**
   * @return Returns manually entered information about the citizen’s spoken
   *         language.
   */
  public ManuallyEnteredSpokenLanguage getManuallyEnteredSpokenLanguage() {
    return manuallyEnteredSpokenLanguage;
  }

  public void setManuallyEnteredSpokenLanguage(ManuallyEnteredSpokenLanguage manuallyEnteredSpokenLanguage) {
    this.manuallyEnteredSpokenLanguage = manuallyEnteredSpokenLanguage;
  }

  /**
   * @return Returns manually entered information about the citizen’s temporary
   *         address.
   */
  public ManuallyEnteredTemporaryAddress getManuallyEnteredTemporaryAddress() {
    return manuallyEnteredTemporaryAddress;
  }

  public void setManuallyEnteredTemporaryAddress(ManuallyEnteredTemporaryAddress manuallyEnteredTemporaryAddress) {
    this.manuallyEnteredTemporaryAddress = manuallyEnteredTemporaryAddress;
  }

  /**
   * @return Returns manually entered information about the citizen’s dentist.
   */
  public ManuallyEnteredDentistInformation getManuallyEnteredDentistInformation() {
    return manuallyEnteredDentistInformation;
  }

  public void
      setManuallyEnteredDentistInformation(ManuallyEnteredDentistInformation manuallyEnteredDentistInformation) {
    this.manuallyEnteredDentistInformation = manuallyEnteredDentistInformation;
  }

  /**
   * @return Returns manually entered information about the citizen’s phone
   *         numbers
   */
  public ManuallyEnteredContactInformation getManuallyEnteredContactInformation() {
    return manuallyEnteredContactInformation;
  }

  public void
      setManuallyEnteredContactInformation(ManuallyEnteredContactInformation manuallyEnteredContactInformation) {
    this.manuallyEnteredContactInformation = manuallyEnteredContactInformation;
  }

  /**
   * @return Returns a list of manually entered information about the citizen’s
   *         relatives.
   */
  public List<ManuallyEnteredInformationAboutRelatives> getManuallyEnteredInformationAboutRelativesList() {
    return manuallyEnteredInformationAboutRelativesList;
  }

  public void setManuallyEnteredInformationAboutRelativesList(
      List<ManuallyEnteredInformationAboutRelatives> manuallyEnteredInformationAboutRelativesList) {
    this.manuallyEnteredInformationAboutRelativesList = manuallyEnteredInformationAboutRelativesList;
  }

}
