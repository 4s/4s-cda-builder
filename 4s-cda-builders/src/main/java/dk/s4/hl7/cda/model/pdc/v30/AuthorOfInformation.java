package dk.s4.hl7.cda.model.pdc.v30;

import java.util.Date;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.PersonIdentity;

/**
 * Author Of Information
 * <p>
 * The author can be either from a register or manually entered. This is
 * indicated via "sourceOfInformation" with the values FromRegister and
 * ManuallyEntered
 */
public class AuthorOfInformation {
  /**
   * SourceOfInformation indicates how the information is created
   */
  public enum SourceOfInformation {
    /**
     * When the information is returned from a register.
     */
    ManuallyEntered,
    /**
     * When the information is manually entered by a person
     */
    FromRegister,
  }

  private ID id;
  private CodedValue code;
  private Date time;

  private PersonIdentity assignedPerson; // is used when manually entered
  private String representedOrganizationName; // is used when a register author

  private SourceOfInformation sourceOfInformation;

  public static class AuthorOfInformationBuilder {
    private ID id;
    private CodedValue code;
    private Date time;

    private PersonIdentity assignedPerson;
    private String representedOrganizationName;

    private SourceOfInformation sourceOfInformation;

    public AuthorOfInformationBuilder(ID id) {
      this.id = id;
    }

    public AuthorOfInformationBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public AuthorOfInformationBuilder setTime(Date time) {
      this.time = time;
      return this;
    }

    public AuthorOfInformationBuilder setAssignedPerson(PersonIdentity assignedPerson) {
      this.assignedPerson = assignedPerson;
      return this;
    }

    public AuthorOfInformationBuilder setRepresentedOrganizationName(String representedOrganizationName) {
      this.representedOrganizationName = representedOrganizationName;
      return this;
    }

    public AuthorOfInformationBuilder setSourceOfInformation(SourceOfInformation sourceOfInformation) {
      this.sourceOfInformation = sourceOfInformation;
      return this;
    }

    public AuthorOfInformation build() {

      return new AuthorOfInformation(this);
    }

  }

  private AuthorOfInformation(AuthorOfInformationBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.time = builder.time;
    this.assignedPerson = builder.assignedPerson;
    this.representedOrganizationName = builder.representedOrganizationName;
    this.sourceOfInformation = builder.sourceOfInformation;

  }

  /**
   * The id of the author
   * <p>
   * If @sourceOfInformation is FromRegister, @root is the OID of the register
   * <p>
   * If @sourceOfInformation is ManuallyEntered, @extension is the CPR number of
   * the author
   * 
   * @return ID Returns the identification of the author
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return Date Returns time of the information
   */
  public Date getTime() {
    return time;
  }

  /**
   * The name of the author.
   * <p>
   * Only available if @sourceOfInformation is ManuallyEntered
   * 
   * @return PersonIdentity Returns name of the author
   */
  public PersonIdentity getAssignedPerson() {
    return assignedPerson;
  }

  /**
   * The name of the organization.
   * <p>
   * Only available if @sourceOfInformation is FromRegister
   * 
   * @return String Returns the organizations name
   */
  public String getRepresentedOrganizationName() {
    return representedOrganizationName;
  }

  /**
   * @return SourceOfInformation Returns the source of the information.
   *         ManuallyEntered or FromRegister
   */
  public SourceOfInformation getSourceOfInformation() {
    return sourceOfInformation;
  }

}
