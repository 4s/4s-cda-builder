package dk.s4.hl7.cda.convert.decode.header.v20;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.OrganizationHandler;
import dk.s4.hl7.cda.convert.decode.header.v20.PersonHandler;
import dk.s4.hl7.cda.model.core.v20.ClinicalDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class PatientHandler<C extends ClinicalDocument> extends dk.s4.hl7.cda.convert.decode.header.PatientHandler<C>
    implements CDAXmlHandler<C> {

  private static final String PATIENT_PATH = "/ClinicalDocument/recordTarget/patientRole";
  private static final String ORG_PATH = "/providerOrganization";

  private OrganizationHandler providerOrganizationHandler;

  //
  public PatientHandler() {
    super(new PersonHandler(PATIENT_PATH, "patient"));
    this.providerOrganizationHandler = new OrganizationHandler(PATIENT_PATH + ORG_PATH);
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    super.handleElementStart(xmlMapping, xmlElement);
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    providerOrganizationHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void addDataToDocument(C clinicalDocument) {
    super.addDataToDocument(clinicalDocument);
    clinicalDocument.setProviderOrganization(providerOrganizationHandler.getOrganization());
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    providerOrganizationHandler.removeHandlerFromMap(xmlMapping);
  }

}