package dk.s4.hl7.cda.convert.decode.cdametadata;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.codes.FormatCodeMapper;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.cdametadata.CDAMetadata;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class MetaDataServiceEventCodeHandler implements CDAXmlHandler<CDAMetadata> {
  private static final Logger logger = LoggerFactory.getLogger(MetaDataServiceEventCodeHandler.class);

  private List<CodedValue> eventCodeList;

  private String templateRoot;
  private String idExtension;

  public MetaDataServiceEventCodeHandler() {
    this.eventCodeList = new ArrayList<CodedValue>();
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code")) {
      CodedValue eventCode = new CodedValue.CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .build();
      logger.debug("code is found: " + eventCode.getCode() + ", " + eventCode.getCodeSystem() + ", "
          + eventCode.getDisplayName());
      eventCodeList.add(eventCode);

    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      if (xmlElement.getAttributeValue("root") != null
          && xmlElement.getAttributeValue("root").equals(MedCom.APD_VERSION_TEMPLATEID_ROOT)) {
        templateRoot = xmlElement.getAttributeValue("root");
      }

    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "extension")) {
      if (templateRoot != null && idExtension == null) {
        idExtension = xmlElement.getAttributeValue("extension");
      }
    }

  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent/code", this);
    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent/templateId", this);
    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent/id", this);
  }

  @Override
  public void addDataToDocument(CDAMetadata clinicalDocument) {
    clinicalDocument.setEventCodeList(eventCodeList);
    if (templateRoot != null && idExtension != null) {
      CodedValue formatCode = FormatCodeMapper.getFormatCode(idExtension);
      if (formatCode != null) {
        clinicalDocument.setFormatCode(formatCode);
      }
    }
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/code");
    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/templateId");
    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/id");
  }
}