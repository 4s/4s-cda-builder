package dk.s4.hl7.cda.model.pdc.v20;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.TelecomPhone;

/**
 * Manually entered information about the citizen’s relatives.
 */

public class ManuallyEnteredInformationAboutRelatives {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;
  private PersonIdentity personIdentity;
  private TelecomPhone[] telecomPhone;
  private CodedValue relationCode;
  private String note;

  public static class ManuallyEnteredInformationAboutRelativesBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;
    private PersonIdentity personIdentity;
    private List<TelecomPhone> telecomPhoneTemporary;
    private TelecomPhone[] telecomPhone;
    private CodedValue relationCode;
    private String note;

    public ManuallyEnteredInformationAboutRelativesBuilder(ID id) {
      telecomPhoneTemporary = new ArrayList<TelecomPhone>();
      this.id = id;
    }

    public ManuallyEnteredInformationAboutRelativesBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public ManuallyEnteredInformationAboutRelativesBuilder
        setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public ManuallyEnteredInformationAboutRelativesBuilder setPersonIdentity(PersonIdentity personIdentity) {
      this.personIdentity = personIdentity;
      return this;
    }

    public ManuallyEnteredInformationAboutRelativesBuilder addTelecomPhone(TelecomPhone telecom) {
      telecomPhoneTemporary.add(telecom);
      return this;
    }

    public ManuallyEnteredInformationAboutRelativesBuilder setTelecomPhone(TelecomPhone[] telecom) {
      this.telecomPhone = telecom;
      return this;
    }

    public ManuallyEnteredInformationAboutRelativesBuilder setRelationCode(CodedValue relationCode) {
      this.relationCode = relationCode;
      return this;
    }

    public ManuallyEnteredInformationAboutRelativesBuilder setNote(String note) {
      this.note = note;
      return this;
    }

    public ManuallyEnteredInformationAboutRelatives build() {
      for (int i = 0; i < telecomPhone.length; i++) {
        telecomPhoneTemporary.add(telecomPhone[i]);
      }
      if (telecomPhoneTemporary.size() > 0) {
        telecomPhone = new TelecomPhone[telecomPhoneTemporary.size()];
        telecomPhoneTemporary.toArray(telecomPhone);
      } else {
        telecomPhone = new TelecomPhone[0];
      }

      return new ManuallyEnteredInformationAboutRelatives(this);
    }

  }

  private ManuallyEnteredInformationAboutRelatives(ManuallyEnteredInformationAboutRelativesBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.personIdentity = builder.personIdentity;
    this.telecomPhone = builder.telecomPhone;
    this.relationCode = builder.relationCode;
    this.note = builder.note;

  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * @return PersonIdentity Returns the relatives name
   */
  public PersonIdentity getPersonIdentity() {
    return personIdentity;
  }

  /**
   * @return TelecomPhone Returns an array of relatives phonenumbers
   */
  public TelecomPhone[] getTelecomPhone() {
    return telecomPhone;
  }

  /**
   * The value of the relationCode represents the relation of the relative
   * <p>
   * The @code is the relation e.g. "nabo" and the @codeDisplayName the
   * corresponding description e.g. "Nabo"
   * 
   * @return CodedValue Returns the relation Code of the custody.
   */
  public CodedValue getRelationCode() {
    return relationCode;
  }

  /**
   * @return String Returns a note about the relative
   */
  public String getNote() {
    return note;
  }

}
