package dk.s4.hl7.cda.codes;

public enum BasicType {
  INT,
  REAL,
  TS
}
