package dk.s4.hl7.cda.convert.decode.general;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

/**
 * General CodedValue handler. Hence the XPath for which this handler is called
 * is configurable
 */
public class CodedValueHandler extends BaseXmlHandler {

  private static final String CODE_ELEMENT_NAME = "code";
  private CodedValue codedValue;
  private boolean allowCodeNI;

  public CodedValueHandler(String xpathToParentElement, boolean allowCodeNI) {
    addPath(xpathToParentElement + "/" + CODE_ELEMENT_NAME);
    this.allowCodeNI = allowCodeNI;
  }

  public CodedValue getCodedValue() {
    return codedValue;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), CODE_ELEMENT_NAME, "code",
        "codeSystem", "displayName")) {
      codedValue = new CodedValue.CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    } else if (allowCodeNI && ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(),
        CODE_ELEMENT_NAME, "displayName")) {
      codedValue = new CodedValue.CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

  public void clear() {
    codedValue = null;
  }
}
