package dk.s4.hl7.cda.model;

public class CdaMeasurementInterval {

  private CdaMeasurement measurementLow;
  private boolean inclusiveLow;
  private CdaMeasurement measurementHigh;
  private boolean inclusiveHigh;

  public CdaMeasurementInterval() {

  }

  public CdaMeasurementInterval(CdaMeasurement measurementLow, boolean inclusiveLow, CdaMeasurement measurementHigh,
      boolean inclusiveHigh) {
    this.measurementLow = measurementLow;
    this.inclusiveLow = inclusiveLow;
    this.measurementHigh = measurementHigh;
    this.inclusiveHigh = inclusiveHigh;
  }

  public CdaMeasurement getMeasurementLow() {
    return measurementLow;
  }

  public boolean isInclusiveLow() {
    return inclusiveLow;
  }

  public CdaMeasurement getMeasurementHigh() {
    return measurementHigh;
  }

  public boolean isInclusiveHigh() {
    return inclusiveHigh;
  }

  public void setLow(CdaMeasurement measurementLow, boolean inclusiveLow) {
    this.measurementLow = measurementLow;
    this.inclusiveLow = inclusiveLow;
  }

  public void setHigh(CdaMeasurement measurementHigh, boolean inclusiveHigh) {
    this.measurementHigh = measurementHigh;
    this.inclusiveHigh = inclusiveHigh;
  }

}
