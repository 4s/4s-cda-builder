package dk.s4.hl7.cda.model.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Organizer;

/**
 * Organizer used by QFDD documents. Holds a list of Questions
 */
public class QFDDOrganizer extends Organizer {
  private List<ID> ids;
  private CodedValue codedValue;
  private String introduction;
  private List<QFDDQuestion> qfddQuestions;
  private List<QFDDPrecondition> preconditions;

  private QFDDOrganizer(QFDDOrganizerBuilder builder) {
    this.ids = builder.ids;
    this.codedValue = builder.codedValue;
    this.introduction = builder.introduction;
    this.qfddQuestions = builder.qfddQuestions;
    this.preconditions = builder.preconditions;
  }

  /**
   * "Effective Java" Builder for constructing QFDDOrganizer.
   */
  public static class QFDDOrganizerBuilder {

    private List<ID> ids;
    private CodedValue codedValue;
    private String introduction;
    private List<QFDDQuestion> qfddQuestions;
    private List<QFDDPrecondition> preconditions;

    public QFDDOrganizerBuilder() {
      this.ids = new ArrayList<ID>();
      this.qfddQuestions = new ArrayList<QFDDQuestion>();
      this.preconditions = new ArrayList<QFDDPrecondition>();
    }

    public QFDDOrganizerBuilder addId(ID id) {
      this.ids.add(id);
      return this;
    }

    public QFDDOrganizerBuilder setIds(List<ID> ids) {
      this.ids = ids;
      return this;
    }

    public QFDDOrganizerBuilder setCode(CodedValue codedValue) {
      this.codedValue = codedValue;
      return this;
    }

    public QFDDOrganizerBuilder setIntroduction(String introduction) {
      this.introduction = introduction;
      return this;
    }

    public QFDDOrganizerBuilder addPrecondition(QFDDPrecondition precondition) {
      preconditions.add(precondition);
      return this;
    }

    public QFDDOrganizerBuilder addQFDDQuestion(QFDDQuestion qfddQuestion) {
      qfddQuestions.add(qfddQuestion);
      return this;
    }

    public QFDDOrganizer build() {
      return new QFDDOrganizer(this);
    }

  }

  public List<ID> getIds() {
    return ids;
  }

  public CodedValue getCode() {
    return codedValue;
  }

  public String getIntroduction() {
    return introduction;
  }

  public List<QFDDPrecondition> getPreconditions() {
    return preconditions;
  }

  public List<QFDDQuestion> getQFDDQuestions() {
    return qfddQuestions;
  }

  @Override
  public String toString() {
    return "QFDDOrganizer [ids=" + ids + ", codedValue=" + codedValue + ", introduction=" + introduction
        + ", qfddQuestions=" + qfddQuestions + ", preconditions=" + preconditions + "]";
  }

}
