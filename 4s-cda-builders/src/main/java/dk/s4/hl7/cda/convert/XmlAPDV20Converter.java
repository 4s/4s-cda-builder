package dk.s4.hl7.cda.convert;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.v20.ClinicalDocumentXmlConverter;
import dk.s4.hl7.cda.convert.decode.apd.v20.SectionHandler;
import dk.s4.hl7.cda.convert.decode.header.CDAHeaderHandler;
import dk.s4.hl7.cda.convert.decode.header.CustodianHandler;
import dk.s4.hl7.cda.convert.decode.header.v20.PatientHandler;
import dk.s4.hl7.cda.convert.decode.header.v20.DocumentationOfHandler;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;

/**
 * Parse Appointment xml to GreenCDA model
 */
public class XmlAPDV20Converter extends ClinicalDocumentXmlConverter<AppointmentDocument> {

  @Override
  public AppointmentDocument convert(String source) {
    return deserialize(new StringReader(source));
  }

  @Override
  protected List<CDAXmlHandler<AppointmentDocument>> createParticipantHandlers() {
    List<CDAXmlHandler<AppointmentDocument>> list = new ArrayList<CDAXmlHandler<AppointmentDocument>>();
    list.add(new PatientHandler<AppointmentDocument>());
    list.add(createAuthorHandler());
    list.add(new CustodianHandler<AppointmentDocument>());
    list.add(new DocumentationOfHandler<AppointmentDocument>());
    return list;
  }

  @Override
  protected List<CDAXmlHandler<AppointmentDocument>> createSectionHandlers() {
    List<CDAXmlHandler<AppointmentDocument>> list = new ArrayList<CDAXmlHandler<AppointmentDocument>>();
    list.add(new SectionHandler());
    return list;
  }

  @Override
  protected AppointmentDocument createNewDocument(CDAHeaderHandler<AppointmentDocument> headerHandler) {
    return new AppointmentDocument(headerHandler.getDocumentId());
  }

}
