package dk.s4.hl7.cda.model.util;

import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.Telecom;

/**
 * General utilities used in the greenCDA model classes
 */
public class ModelUtil {
  private static final String NULL_FLAVOR_VALUE = "NI";

  public static boolean isTextUseful(String text) {
    if (text != null) {
      text = text.trim();
      return !text.isEmpty() && !text.equalsIgnoreCase("null") && !text.equalsIgnoreCase(NULL_FLAVOR_VALUE);
    }
    return false;
  }

  public static void checkNull(Object text, String message) {
    if (text == null) {
      throw new IllegalArgumentException(message);
    }
  }

  public static void checkNull(String text, String message) {
    if (!isTextUseful(text)) {
      throw new IllegalArgumentException(message);
    }
  }

  public static void checkNullMutualDependent(String text1, String text2, String message) {
    //both must be filled in
    if (!isTextUseful(text1) && isTextUseful(text2)) {
      throw new IllegalArgumentException(message);
    }
    if (isTextUseful(text1) && !isTextUseful(text2)) {
      throw new IllegalArgumentException(message);
    }
  }

  public static void checkNullDependent(String text1, String text2, String message) {
    //if text1 is given, then text 2 must also be given
    if (isTextUseful(text1) && !isTextUseful(text2)) {
      throw new IllegalArgumentException(message);
    }
  }

  public static void checkNullAtLeastOne(Object text1, Object text2, String message) {
    //at least one must be filled in
    if (text1 == null && text2 == null) {
      throw new IllegalArgumentException(message);
    }
  }

  public static String checkDots(String value, IntervalType intervalType) {
    if (intervalType == IntervalType.IVL_REAL) {
      return value.replace(',', '.');
    }
    return value;
  }

  public static void checkPostive(int number, String message) {
    if (number < 1) {
      throw new IllegalArgumentException(message);
    }
  }

  public static boolean addressDataAreEqual(AddressData address1, AddressData address2) {
    if (address1 == null && address2 == null) {
      return true;
    }

    if (address1 == null && address2 != null || address1 != null && address2 == null) {
      return false;
    }

    String adrUse1 = (address1.getAddressUse() != null) ? null : address1.getAddressUse().toString();
    String adrUse2 = (address2.getAddressUse() != null) ? null : address2.getAddressUse().toString();

    if (!stringsAreEqual(adrUse1, adrUse2)) {
      return false;
    }

    if (address1.getStreet() == null && address2.getStreet() != null
        || address1.getStreet() != null && address2.getStreet() == null) {
      return false;
    }
    if (address1.getStreet() != null && address2.getStreet() != null) {
      if (address1.getStreet().length != address2.getStreet().length) {
        return false;
      } else {
        for (int idx = 0; idx < address1.getStreet().length; idx++) {
          if (!stringsAreEqual(address1.getStreet()[idx], address2.getStreet()[idx])) {
            return false;
          }
        }
      }
    }

    if (!stringsAreEqual(address1.getPostalCode(), address2.getPostalCode())) {
      return false;
    }

    if (!stringsAreEqual(address1.getCity(), address2.getCity())) {
      return false;
    }

    if (!stringsAreEqual(address1.getCountry(), address2.getCountry())) {
      return false;
    }

    return true;
  }

  public static boolean telecomAreEqual(Telecom[] telecomList1, Telecom[] telecomList2) {

    if (telecomList1 == null && telecomList2 == null) {
      return false;
    }

    if (telecomList1 == null && telecomList2 != null || telecomList1 != null && telecomList2 == null) {
      return false;
    }

    if (telecomList1 != null && telecomList2 != null) {
      if (telecomList1.length != telecomList2.length) {
        return false;
      } else {
        for (int idx = 0; idx < telecomList1.length; idx++) {
          String use1 = (telecomList1[idx].getUse() != null) ? null : telecomList1[idx].getUse().toString();
          String use2 = (telecomList2[idx].getUse() != null) ? null : telecomList2[idx].getUse().toString();
          if (!stringsAreEqual(use1, use2)) {
            return false;
          }

          if (!stringsAreEqual(telecomList1[idx].getProtocol(), telecomList2[idx].getProtocol())) {
            return false;
          }

          if (!stringsAreEqual(telecomList1[idx].getValue(), telecomList2[idx].getValue())) {
            return false;
          }
        }
      }
    }
    return true;
  }

  private static boolean stringsAreEqual(String string1, String string2) {

    if (string1 == null && string2 == null) {
      return true;
    }

    if (string1 == null && string2 != null || string1 != null && string2 == null) {
      return false;
    }

    return string1.equals(string2);
  }

}
