package dk.s4.hl7.cda.convert.decode.apd.v20;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlHandler;
import dk.s4.hl7.util.xml.XmlMapping;

public class PreconditionHandler implements XmlHandler {
  private static final Logger logger = LoggerFactory.getLogger(PreconditionHandler.class);

  private static final String PRECONDITION_XPATH = "/ClinicalDocument/component/structuredBody/component/section/entry/encounter/precondition";
  private boolean isRepeatingDocument;
  private String repeatingDocumentGroupValue;

  private boolean isGuidedInterval;
  private String guidedIntervalText;

  private String currentPreconditionCode;

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "precondition")) {
      currentPreconditionCode = null;
    }

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code")) {
      currentPreconditionCode = xmlElement.getAttributeValue("code");
      logger.debug("current precondition code is: " + currentPreconditionCode);
      if (currentPreconditionCode != null && currentPreconditionCode.equals(MedCom.APD_CODE_GUIDED_INTERVAL_TYPE)) {
        isGuidedInterval = true;
      } else if (currentPreconditionCode != null
          && currentPreconditionCode.equals(MedCom.APD_CODE_REPEATING_APPOINTMENT_TYPE)) {
        isRepeatingDocument = true;
      }
    } else if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "text")
        && currentPreconditionCode != null && currentPreconditionCode.equals(MedCom.APD_CODE_GUIDED_INTERVAL_TYPE)) {
      if (xmlElement.getElementValue() != null) {
        guidedIntervalText = xmlElement.getElementValue();
      }
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "extension")
        && currentPreconditionCode != null
        && currentPreconditionCode.equals(MedCom.APD_CODE_REPEATING_APPOINTMENT_TYPE)) {
      repeatingDocumentGroupValue = xmlElement.getAttributeValue("extension");
    }

  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
  }

  public void addDataToDocument(AppointmentDocument clinicalDocument) {
    clinicalDocument.setRepeatingDocument(isRepeatingDocument);
    clinicalDocument.setRepeatingDocumentGroupValue(repeatingDocumentGroupValue);
    clinicalDocument.setGuidedInterval(isGuidedInterval);
    clinicalDocument.setGuidedIntervalText(guidedIntervalText);
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add(PRECONDITION_XPATH, this);
    xmlMapping.add(PRECONDITION_XPATH + "/criterion/code", this);
    xmlMapping.add(PRECONDITION_XPATH + "/criterion/text", this);
    xmlMapping.add(PRECONDITION_XPATH + "/criterion/value", this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove(PRECONDITION_XPATH);
    xmlMapping.remove(PRECONDITION_XPATH + "/templateId/criterion/code");
    xmlMapping.remove(PRECONDITION_XPATH + "/templateId/criterion/text");
    xmlMapping.remove(PRECONDITION_XPATH + "/templateId/criterion/value");
  }
}