package dk.s4.hl7.cda.convert.decode.v20;

import dk.s4.hl7.cda.convert.base.Converter;
import dk.s4.hl7.cda.convert.base.ReaderSerializer;
import dk.s4.hl7.cda.convert.decode.header.ParticipantHandler.ParticipantType;
import dk.s4.hl7.cda.convert.decode.header.v20.ParticipantHandler;
import dk.s4.hl7.cda.model.core.v20.ClinicalDocument;

/**
 * Abstract base class for all CDA converters that parses xml to the GreenCDA
 * model
 * 
 * Contains overall process of converting.
 * 
 * @param <E>
 */
public abstract class ClinicalDocumentXmlConverter<E extends ClinicalDocument> extends
    dk.s4.hl7.cda.convert.decode.ClinicalDocumentXmlConverter<E> implements Converter<String, E>, ReaderSerializer<E> {

  @Override
  protected ParticipantHandler<E> createAuthorHandler() {
    return new ParticipantHandler<E>(ParticipantType.AUTHOR, "/ClinicalDocument/author/assignedAuthor",
        "/ClinicalDocument/author/time", "assignedPerson", "/representedOrganization");
  }

}
