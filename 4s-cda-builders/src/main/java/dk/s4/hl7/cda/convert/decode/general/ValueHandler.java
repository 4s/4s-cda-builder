package dk.s4.hl7.cda.convert.decode.general;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.AddressBuilder;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CdaMeasurement;
import dk.s4.hl7.cda.model.CdaMeasurementInterval;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.TelecomPhone;
import dk.s4.hl7.cda.model.pdc.v30.NoResuscitationRegistration.Registered;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class ValueHandler extends BaseXmlHandler {
  private static final String CODE_ELEMENT_NAME = "value";
  private boolean allowNullFlavor;

  // # value variables #
  private String cpr;
  private String prefix;
  private List<String> givenNames;
  private String familyName;
  private List<String> streetAddressLines;
  private String postalCode;
  private String city;
  private String country;
  private Use use;
  private String coverageGroupValue;
  private boolean isRegistered;
  private Registered registered;
  private CodedValue valueCode;
  private ID identification;
  private List<TelecomPhone> telecomList;
  private String string;
  private CdaMeasurement cdaMeasurement;

  private CdaMeasurementInterval cdaMeasurementInterval;

  // Types
  private String valueTypeCurrent;
  static private final String valueTypePN = "PN";
  static private final String valueTypePQ = "PQ";
  static private final String valueTypeIVL_PQ = "IVL_PQ";
  static private final String valueTypeEN = "EN";
  static private final String valueTypeAD = "AD";
  static private final String valueTypeII = "II";
  static private final String valueTypeCD = "CD";
  static private final String valueTypeTEL = "TEL";
  static private final String valueTypeST = "ST";
  static private final String valueTypeTN = "TN";

  public ValueHandler(String xpathToParentElement, boolean allowNullFlavor) {
    String path = xpathToParentElement + "/" + CODE_ELEMENT_NAME;
    this.allowNullFlavor = allowNullFlavor;

    addPath(path);
    addPath(path + "/prefix");
    addPath(path + "/given");
    addPath(path + "/family");
    addPath(path + "/streetAddressLine");
    addPath(path + "/postalCode");
    addPath(path + "/city");
    addPath(path + "/country");
    addPath(path + "/low");
    addPath(path + "/high");

    localClear();
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type") || (allowNullFlavor
        && ConvertXmlUtil.isAttributePresentEvenNullFlavor(xmlElement, xmlElement.getElementName(), "value", "type"))) {
      valueTypeCurrent = xmlElement.getAttributeValue("type");

      //			Handle those attributes present on the value tag itself.
      switch (valueTypeCurrent) {
      case valueTypeII:
        handleElementII(xmlElement);
        break;
      case valueTypeCD:
        handleElementCode(xmlElement);
        break;
      case valueTypeTN:
      case valueTypeST:
        handleElementSt(xmlElement);
        break;
      case valueTypePQ:
        handleElementPq(xmlElement);
        break;
      case valueTypeTEL:
        handleElementTel(xmlElement);
        break;
      case valueTypeAD:
        use = ConvertXmlUtil.getUse(xmlElement, null);
        break;
      }
    } else if (valueTypeCurrent != null) {

      //			otherwise handle sub values within the value tag based on the type
      switch (valueTypeCurrent) {
      case valueTypePN:
      case valueTypeEN:
        handleElementPerson(xmlElement);
        break;
      case valueTypeAD:
        handleElementAddress(xmlElement);
        break;
      case valueTypeIVL_PQ:
        handleElementIvlPq(xmlElement);
        break;
      }
    }

  }

  private void handleElementPerson(XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "given")) {
      givenNames.add(xmlElement.getElementValue());
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "family")) {
      familyName = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "prefix")) {
      prefix = xmlElement.getElementValue();
    }
  }

  private void handleElementAddress(XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "streetAddressLine")) {
      streetAddressLines.add(xmlElement.getElementValue());
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "postalCode")) {
      postalCode = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "city")) {
      city = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "country")) {
      country = xmlElement.getElementValue();
    }
  }

  private void handleElementII(XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type", "root",
        "extension")) {
      String root = xmlElement.getAttributeValue("root");
      if (root.equalsIgnoreCase(MedCom.PDC_COVERAGE_GROUP_VALUE_ROOT)) {
        coverageGroupValue = xmlElement.getAttributeValue("extension");
      } else if (root.equalsIgnoreCase(MedCom.PDC_ORGAN_DONOR_VALUE_ROOT)
          || root.equalsIgnoreCase(MedCom.PDC_TREATMENT_WILL_VALUE_ROOT)
          || root.equalsIgnoreCase(MedCom.PDC_LIVING_WILL_VALUE_ROOT)) {
        isRegistered = Boolean.parseBoolean(xmlElement.getAttributeValue("extension"));
      } else if (root.equalsIgnoreCase(MedCom.PDC_NORESUSCITATION_VALUE_ROOT)) {
        boolean booleanValue = Boolean.parseBoolean(xmlElement.getAttributeValue("extension"));
        if (booleanValue) {
          registered = Registered.True;
        } else {
          String booleanStringValue = xmlElement.getAttributeValue("extension").toUpperCase();
          registered = "FALSE".equals(booleanStringValue) ? Registered.False : Registered.Nav;
        }
      } else if (root.equalsIgnoreCase(MedCom.PDC_DENTIST_VALUE_ROOT)
          || root.equalsIgnoreCase(MedCom.PDC_NAME_AND_ADDRESS_VALUE_ROOT)) {
        if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "root", "extension")) {
          identification = new ID.IDBuilder()
              .setAuthorityName(xmlElement.getAttributeValue("assigningAuthorityName"))
              .setExtension(xmlElement.getAttributeValue("extension"))
              .setRoot(xmlElement.getAttributeValue("root"))
              .build();
        }
      } else if (root.equalsIgnoreCase(MedCom.PDC_CUSTODY_VALUE_ROOT)) {
        cpr = xmlElement.getAttributeValue("extension");
      }
    }
  }

  private void handleElementCode(XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "code", "displayName",
        "codeSystem", "codeSystemName")) {
      valueCode = new CodedValue(xmlElement.getAttributeValue("code"), xmlElement.getAttributeValue("codeSystem"),
          xmlElement.getAttributeValue("displayName"), xmlElement.getAttributeValue("codeSystemName"));
    }
  }

  private void handleElementPq(XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresentEvenNullFlavor(xmlElement, xmlElement.getElementName(), "value")) {
      cdaMeasurement = new CdaMeasurement(xmlElement.getAttributeValue("value"), xmlElement.getAttributeValue("unit"));
    }
  }

  private void handleElementIvlPq(XMLElement xmlElement) {

    if (cdaMeasurementInterval == null) {
      cdaMeasurementInterval = new CdaMeasurementInterval();
    }

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "low", "value")) {
      cdaMeasurementInterval.setLow(
          new CdaMeasurement(xmlElement.getAttributeValue("value"), xmlElement.getAttributeValue("unit")),
          stringToBoolean(xmlElement.getAttributeValue("inclusive")));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "high", "value")) {
      cdaMeasurementInterval.setHigh(
          new CdaMeasurement(xmlElement.getAttributeValue("value"), xmlElement.getAttributeValue("unit")),
          stringToBoolean(xmlElement.getAttributeValue("inclusive")));
    }
  }

  private boolean stringToBoolean(String string) {
    return string != null ? "true".equalsIgnoreCase(string) : true;
  }

  private void handleElementSt(XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "value")) {
      string = xmlElement.getElementValue();
    }
  }

  private void handleElementTel(XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "use", "value")) {
      telecomList.add(ConvertXmlUtil.createTelecomPhone(xmlElement));
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "value")) {
      localClear();
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
  }

  public String getCpr() {
    return cpr;
  }

  public PersonIdentity getPersonIdentity() {

    PersonBuilder personBuilder = new PersonIdentity.PersonBuilder(familyName).setPrefix(prefix);
    for (String givenName : givenNames) {
      personBuilder.addGivenName(givenName);
    }
    return personBuilder.build();
  }

  public CodedValue getCode() {
    return valueCode;
  }

  public AddressData getAddressData(Use useDefault) {
    if (use == null) {
      use = useDefault;
    }
    AddressBuilder addressBuilder = new AddressData.AddressBuilder()
        .setCity(city)
        .setPostalCode(postalCode)
        .setCountry(country)
        .setUse(use);
    for (String address : streetAddressLines) {
      addressBuilder.addAddressLine(address);
    }
    return addressBuilder.build();

  }

  public ID getIdentification() {
    return identification;
  }

  public String getCoverageGroupValue() {
    return coverageGroupValue;
  }

  public boolean isRegistered() {
    return isRegistered;
  }

  public Registered getRegistered() {
    return registered;
  }

  public String getString() {
    return string;
  }

  public CdaMeasurement getCdaMeasurement() {
    return cdaMeasurement;
  }

  public CdaMeasurementInterval getCdaMeasurementInterval() {
    return cdaMeasurementInterval;
  }

  public String getNoteType() {
    return valueTypeST;
  }

  public TelecomPhone[] getTelecoms() {
    TelecomPhone[] telecom = new TelecomPhone[telecomList.size()];
    telecomList.toArray(telecom);
    return telecom;
  }

  public void clear() {

    cpr = null;
    prefix = null;
    givenNames = new ArrayList<String>();
    familyName = null;
    streetAddressLines = new ArrayList<String>();
    postalCode = null;
    city = null;
    country = null;
    use = null;

    coverageGroupValue = null;
    isRegistered = false;
    registered = Registered.Nav;
    valueCode = null;
    identification = null;
    telecomList = new ArrayList<TelecomPhone>();
    string = null;

    cdaMeasurement = null;
    cdaMeasurementInterval = null;

    localClear();
  }

  private void localClear() {
    valueTypeCurrent = null;
  }

}
