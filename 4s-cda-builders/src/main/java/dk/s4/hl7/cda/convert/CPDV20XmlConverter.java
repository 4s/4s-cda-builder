package dk.s4.hl7.cda.convert;

import java.io.IOException;

import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.convert.encode.narrative.text.NarrativeTextConverter;
import dk.s4.hl7.cda.convert.encode.pattern.ValuePattern;
import dk.s4.hl7.cda.convert.encode.v20.ClinicalDocumentConverter;
import dk.s4.hl7.cda.model.CdaDate;
import dk.s4.hl7.cda.model.CdaDateInterval;
import dk.s4.hl7.cda.model.CdaMeasurement;
import dk.s4.hl7.cda.model.CdaMeasurementInterval;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument.Status;
import dk.s4.hl7.cda.model.cpd.v20.CPDGoalObservationComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDGoalObservationEntry;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthConcernObservation;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusActEntry;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDInterventionActComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDInterventionActEntry;
import dk.s4.hl7.cda.model.cpd.v20.CPDOutcomeObservationComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDOutcomeObservationEntry;
import dk.s4.hl7.cda.model.cpd.v20.CPDSection;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

/**
 * An implementation of the Converter that can build an XML Document following
 * the Danish CPD standard.
 * 
 */

public class CPDV20XmlConverter extends ClinicalDocumentConverter<CPDDocument, Void> {

  final private CodedValue codedValueHealthConcern = new CodedValue(Loinc.CPD_HEALTH_CONCERNS_CODE, Loinc.OID,
      Loinc.CPD_HEALTH_CONCERNS_DISPLAYNAME, Loinc.DISPLAYNAME);
  final private CodedValue codedValueGoal = new CodedValue(Loinc.CPD_GOALS_CODE, Loinc.OID, Loinc.CPD_GOALS_DISPLAYNAME,
      Loinc.DISPLAYNAME);
  final private CodedValue codedValueIntervension = new CodedValue(Loinc.CPD_INTERVENSIONS_CODE, Loinc.OID,
      Loinc.CPD_INTERVENSIONS_DISPLAYNAME, Loinc.DISPLAYNAME);
  final private CodedValue codedValueOutcome = new CodedValue(Loinc.CPD_OUTCOME_CODE, Loinc.OID,
      Loinc.CPD_OUTCOME_DISPLAYNAME, Loinc.DISPLAYNAME);

  static final ValuePattern valuePattern = new ValuePattern();

  public CPDV20XmlConverter(XmlPrettyPrinter xmlPrettyPrinter) {
    super(xmlPrettyPrinter);
    this.logger = LoggerFactory.getLogger(CPDV20XmlConverter.class);
  }

  public CPDV20XmlConverter() {
    this(new XmlPrettyPrinter());
  }

  // ################################### HEADER ##########################################################

  @Override
  protected void buildRootNode(XmlStreamBuilder xmlBuilder) throws IOException {

    xmlBuilder.addDefaultPI();
    xmlBuilder.addStyleSheet();
    xmlBuilder
        .element("ClinicalDocument")
        .attribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
        .attribute("xmlns", "urn:hl7-org:v3")
        .attribute("xmlns:sdtc", "urn:hl7-org:sdtc")
        .attribute("xmlns:voc", "urn:hl7-org:v3/voc")
        .attribute("classCode", "DOCCLIN")
        .attribute("moodCode", "EVN")
        .attribute("xsi:schemaLocation",
            "urn:hl7-org:v3 http://svn.medcom.dk/svn/releases/Standarder/HL7/Generic/Schema/CDA_SDTC.xsd");
  }

  @Override
  protected void buildDocumentationOf(CPDDocument source, XmlStreamBuilder xmlBuilder) throws IOException {
    buildEffectiveTimesHdr(source, xmlBuilder);
    buildVersion(source, xmlBuilder);
    if (source.getEpisodeOfCareLabel() != null) {
      buildEpisodeOfCareLabel(source, xmlBuilder);
    }
  }

  private void buildEffectiveTimesHdr(CPDDocument source, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("documentationOf");
    xmlBuilder.element("serviceEvent");
    xmlBuilder.element("effectiveTime");

    if (source.getServiceStartTime() != null) {
      xmlBuilder
          .element("low")
          .attribute("value", dateTimeformatter.format(source.getServiceStartTime()))
          .elementShortEnd();
    }

    if (source.getServiceStopTime() != null) {
      xmlBuilder
          .element("high")
          .attribute("value", dateTimeformatter.format(source.getServiceStopTime()))
          .elementShortEnd();
    } else {
      BuildUtil.buildNullFlavor("high", xmlBuilder);
    }
    xmlBuilder.elementEnd(); // end effectiveTime
    xmlBuilder.elementEnd(); // end serviceEvent
    xmlBuilder.elementEnd(); // end documentationOf

  }

  private void buildVersion(CPDDocument source, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
    xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIds(xmlBuilder, MedCom.CPD_VERSION_TEMPLATEID_ROOT);
    BuildUtil.buildId(MedCom.VERSION_ID_ROOT, MedCom.ROOT_AUTHORITYNAME, source.getCdaProfileAndVersion(), xmlBuilder);

    xmlBuilder.elementEnd(); // end serviceEvent
    xmlBuilder.elementEnd(); // end documentationOf

  }

  private void buildEpisodeOfCareLabel(CPDDocument source, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
    xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIds(xmlBuilder, MedCom.CPD_EPISODE_OF_CARE_LABEL_TEMPLATEID_ROOT);

    for (ID id : source.getEpisodeOfCareIdentifierList()) {
      if (id != null) {
        BuildUtil.buildId(id, xmlBuilder);
      }
    }
    CodedValue episodeOfCareLabelCode;
    if (source.getEpisodeOfCareLabelDisplayName() != null) {
      episodeOfCareLabelCode = new CodedValue(source.getEpisodeOfCareLabel(),
          MedCom.CPD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEM, source.getEpisodeOfCareLabelDisplayName(),
          MedCom.CPD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEMNAME);
    } else {
      episodeOfCareLabelCode = new CodedValue(source.getEpisodeOfCareLabel(),
          MedCom.CPD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEM, MedCom.CPD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEMNAME);
    }

    BuildUtil.buildCode(episodeOfCareLabelCode, xmlBuilder);
    xmlBuilder.elementEnd(); // end serviceEvent
    xmlBuilder.elementEnd(); // end documentationOf

  }

  // ################################### CONTENT ##########################################################

  private void buildHealthConcernSections(CPDSection<CPDHealthStatusComponent> section, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (section != null) {
      this.buildStartSection(section.getTitle(), section.getText(), xmlBuilder, HL7.CPD_SECTION_HEALTH_CONCERN_OID,
          codedValueHealthConcern, true);

      if (section.getComponent() != null) {
        CPDHealthStatusComponent component = section.getComponent();
        this.buildStartSection(component.getTitle(), component.getText(), xmlBuilder,
            HL7.CPD_SECTION_HEALTH_CONCERN_SUB_OID, component.getCode(), false);

        for (CPDHealthStatusActEntry entry : component.getEntryList()) {
          xmlBuilder.element("entry").attribute("typeCode", "DRIV").attribute("contextConductionInd", "true");

          xmlBuilder.element("act").attribute("classCode", "ACT").attribute("moodCode", "EVN");
          buildTemplateId(HL7.CPD_SECTION_HEALTH_CONCERN_ACT_OID, xmlBuilder);
          buildId(entry.getId(), xmlBuilder);
          buildCode(Loinc.CPD_HEALTH_ACT_CODE, Loinc.CPD_HEALTH_ACT_DISPLAYNAME, xmlBuilder);
          buildStatusCode(entry.getStatusCode(), xmlBuilder);
          buildEffectiveTimes(entry.getEffectiveTimeCdaDate(), entry.getEffectiveTimeInterval(), xmlBuilder);

          for (Participant author : entry.getAuthorList()) {
            buildAuthorSection(null, author, xmlBuilder);
          }
          for (CPDHealthConcernObservation cpdHealthConcernObservation : entry.getObservationList()) {
            xmlBuilder.element("entryRelationship").attribute("typeCode", "REFR");
            xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");
            buildId(cpdHealthConcernObservation.getId(), xmlBuilder);
            buildCodeAllowCodeNI(cpdHealthConcernObservation.getCode(), null, xmlBuilder);
            buildEffectiveTimes(cpdHealthConcernObservation.getEffectiveTimeCdaDate(),
                cpdHealthConcernObservation.getEffectiveTimeInterval(), xmlBuilder);
            valuePattern.buildValueCD(cpdHealthConcernObservation.getValueCode(), xmlBuilder);
            this.buildTwoEndSections(xmlBuilder);
          }
          this.buildTwoEndSections(xmlBuilder);
        }
        this.buildTwoEndSections(xmlBuilder);
      }
      this.buildTwoEndSections(xmlBuilder);
    }
  }

  private void buildGoalSections(CPDSection<CPDGoalObservationComponent> section, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (section != null) {
      this.buildStartSection(section.getTitle(), section.getText(), xmlBuilder, HL7.CPD_SECTION_GOAL_OID,
          codedValueGoal, true);

      if (section.getComponent() != null) {
        CPDGoalObservationComponent component = section.getComponent();
        this.buildStartSection(component.getTitle(), component.getText(), xmlBuilder, HL7.CPD_SECTION_GOAL_SUB_OID,
            component.getCode(), false);

        for (CPDGoalObservationEntry entry : component.getEntryList()) {
          xmlBuilder.element("entry").attribute("typeCode", "DRIV").attribute("contextConductionInd", "true");
          xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "GOL");
          buildTemplateId(HL7.CPD_SECTION_GOAL_OBSERVATION_OID, xmlBuilder);
          buildId(entry.getId(), xmlBuilder);
          buildCodeAllowCodeNI(entry.getCode(), "NI", xmlBuilder);
          buildText(entry.getText(), false, xmlBuilder);
          buildStatusCode(Status.ACTIVE, xmlBuilder);
          buildEffectiveTimes(entry.getEffectiveTimeCdaDate(), entry.getEffectiveTimeInterval(), xmlBuilder);
          selectAndBuildValue(entry.getCdaMeasurement(), entry.getCdaMeasurementInterval(), entry.getComment(),
              xmlBuilder);
          for (Participant author : entry.getAuthorList()) {
            buildAuthorSection(null, author, xmlBuilder);
          }
          this.buildTwoEndSections(xmlBuilder);
        }
        this.buildTwoEndSections(xmlBuilder);
      }
      this.buildTwoEndSections(xmlBuilder);
    }
  }

  private void buildIntervensionSections(CPDSection<CPDInterventionActComponent> section, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (section != null) {
      this.buildStartSection(section.getTitle(), section.getText(), xmlBuilder, HL7.CPD_SECTION_INTERVENTION_OID,
          codedValueIntervension, true);

      if (section.getComponent() != null) {
        CPDInterventionActComponent component = section.getComponent();
        this.buildStartSection(component.getTitle(), component.getText(), xmlBuilder,
            HL7.CPD_SECTION_INTERVENTION_SUB_OID, component.getCode(), false);

        for (Participant author : component.getAuthorList()) {
          buildAuthorSection(null, author, xmlBuilder);
        }

        for (CPDInterventionActEntry entry : component.getEntryList()) {
          xmlBuilder.element("entry").attribute("typeCode", "DRIV").attribute("contextConductionInd", "true");

          if (entry.getStatusCode().equals(Status.COMPLETED)) {
            xmlBuilder.element("encounter").attribute("classCode", "ACT").attribute("moodCode", "EVN");
          } else {
            xmlBuilder.element("encounter").attribute("classCode", "ACT").attribute("moodCode", "INT");
          }
          buildTemplateId(HL7.CPD_SECTION_INTERVENTION_ACT_OID, xmlBuilder);
          buildId(entry.getId(), xmlBuilder);
          buildCodeAllowCodeNI(entry.getCode(), "NI", xmlBuilder);
          buildText(entry.getText(), false, xmlBuilder);
          buildStatusCode(entry.getStatusCode(), xmlBuilder);
          buildEffectiveTimes(entry.getEffectiveTimeCdaDate(), entry.getEffectiveTimeInterval(), xmlBuilder);

          for (Participant performer : entry.getPerformerList()) {
            xmlBuilder.element("performer").attribute("typeCode", "PRF");
            buildAssignedEntity(performer, xmlBuilder);
            xmlBuilder.elementEnd(); // end performer
          }
          this.buildTwoEndSections(xmlBuilder);
        }
        this.buildTwoEndSections(xmlBuilder);
      }
      this.buildTwoEndSections(xmlBuilder);
    }
  }

  private void buildOutcomesSections(CPDSection<CPDOutcomeObservationComponent> section, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (section != null) {
      this.buildStartSection(section.getTitle(), section.getText(), xmlBuilder, HL7.CPD_SECTION_OUTCOME_OID,
          codedValueOutcome, true);

      if (section.getComponent() != null) {
        CPDOutcomeObservationComponent component = section.getComponent();
        this.buildStartSection(component.getTitle(), component.getText(), xmlBuilder, HL7.CPD_SECTION_OUTCOME_SUB_OID,
            null, false);

        for (CPDOutcomeObservationEntry entry : component.getEntryList()) {
          xmlBuilder.element("entry").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
          xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");
          buildTemplateId(HL7.CPD_SECTION_OUTCOME_OBSERVATION_OID, xmlBuilder);
          buildId(entry.getId(), xmlBuilder);
          buildCodeAllowCodeNI(entry.getCode(), null, xmlBuilder);
          buildStatusCode(Status.COMPLETED, xmlBuilder);
          buildEffectiveTimes(entry.getEffectiveTimeCdaDate(), entry.getEffectiveTimeInterval(), xmlBuilder);
          selectAndBuildValue(entry.getCdaMeasurement(), entry.getCdaMeasurementInterval(), entry.getComment(),
              xmlBuilder);
          for (Participant author : entry.getAuthorList()) {
            buildAuthorSection(null, author, xmlBuilder);
          }
          this.buildTwoEndSections(xmlBuilder);
        }
        this.buildTwoEndSections(xmlBuilder);
      }
      this.buildTwoEndSections(xmlBuilder);
    }
  }

  private void selectAndBuildValue(CdaMeasurement cdaMeasurement, CdaMeasurementInterval cdaMeasurementInterval,
      String comment, XmlStreamBuilder xmlBuilder) throws IOException {
    if (cdaMeasurementInterval != null) {
      valuePattern.buildValueIVL_PQ(cdaMeasurementInterval, xmlBuilder);
    } else if (cdaMeasurement != null) {
      valuePattern.buildValuePQ(cdaMeasurement, xmlBuilder);
    } else {
      valuePattern.buildValueST(comment, xmlBuilder);
    }
  }

  @Override
  protected void buildBody(CPDDocument cpdDocument, XmlStreamBuilder xmlBuilder,
      NarrativeTextConverter<Void> narrativeTextConverter) throws IOException {
    this.buildStructuredBodySectionStart(xmlBuilder);

    this.buildHealthConcernSections(cpdDocument.getHealthConcernSection(), xmlBuilder);
    this.buildGoalSections(cpdDocument.getGoalSection(), xmlBuilder);
    this.buildIntervensionSections(cpdDocument.getInterventionSection(), xmlBuilder);
    this.buildOutcomesSections(cpdDocument.getOutcomeSection(), xmlBuilder);

    this.buildStructuredBodySectionEnd(xmlBuilder);
  }

  @Override
  public String convert(CPDDocument source) {

    int estiamtedSize = 4 * 1500;

    StringBuilder builder = new StringBuilder(estiamtedSize);
    serialize(source, builder);
    return builder.toString();
  }

  // ################################### CONTENT BUILDING BLOCKS ##########################################################

  private void buildStartSection(String title, String text, XmlStreamBuilder xmlBuilder, String templateId,
      CodedValue code, boolean outerSection) throws IOException {
    xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");

    if (outerSection) {
      BuildUtil.buildTemplateIdsWithExtension(xmlBuilder, HL7.CPD_SECTION_EXTENSION, templateId);
    } else {
      BuildUtil.buildTemplateIdsWithExtension(xmlBuilder, HL7.CPD_SECTION_SUB_EXTENSION, templateId);
    }

    if (code != null) {
      BuildUtil.buildCode(code, xmlBuilder);
    }

    buildTitle(title, xmlBuilder);
    buildText(text, outerSection, xmlBuilder);
  }

  private void buildTwoEndSections(XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.elementEnd(); // End section
    xmlBuilder.elementEnd(); // End component
  }

  private void buildCode(String code, String displayName, XmlStreamBuilder xmlBuilder) throws IOException {
    BuildUtil.buildCode(code, Loinc.OID, displayName, Loinc.DISPLAYNAME, xmlBuilder);
  }

  private void buildCodeAllowCodeNI(CodedValue codedValue, String nullValue, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (codedValue != null && codedValue.getCode() != null && codedValue.getCodeSystem() != null
        && codedValue.getDisplayName() != null && codedValue.getCodeSystemName() != null) {
      BuildUtil.buildCode(codedValue, xmlBuilder);
    } else if (codedValue != null && codedValue.getDisplayName() != null) {
      xmlBuilder
          .element("code")
          .attribute("code", "NI")
          .attribute("displayName", codedValue.getDisplayName())
          .elementShortEnd();
    } else if (nullValue != null) {
      BuildUtil.buildNullFlavor("code", nullValue, xmlBuilder);
    }
  }

  public static void buildCodeCodeSystemOptional(CodedValue codedValue, XmlStreamBuilder xmlBuilder)
      throws IOException {
    xmlBuilder.element("code").attribute("code", codedValue.getCode());
    if (codedValue.getCodeSystem() != null) {
      xmlBuilder.attribute("codeSystem", codedValue.getCodeSystem());
    }
    if (codedValue.getDisplayName() != null) {
      xmlBuilder.attribute("displayName", codedValue.getDisplayName());
    }
    if (codedValue.getCodeSystemName() != null) {
      xmlBuilder.attribute("codeSystemName", codedValue.getCodeSystemName());
    }
    xmlBuilder.elementShortEnd();
  }

  private void buildEffectiveTimes(CdaDateInterval effectiveTimeInterval, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (effectiveTimeInterval != null
        && (effectiveTimeInterval.getStartTime() != null || effectiveTimeInterval.getStopTime() != null)) {
      xmlBuilder.element("effectiveTime");
      if (effectiveTimeInterval.getStartTime() != null) {
        xmlBuilder
            .element("low")
            .attribute("value", effectiveTimeInterval.getStartTimeCdaDate().dateToFormattedString())
            .elementShortEnd();
      } else {
        BuildUtil.buildNullFlavor("low", xmlBuilder);
      }
      if (effectiveTimeInterval.getStopTime() != null) {
        xmlBuilder
            .element("high")
            .attribute("value", effectiveTimeInterval.getStopTimeCdaDate().dateToFormattedString())
            .elementShortEnd();
      } else {
        BuildUtil.buildNullFlavor("high", xmlBuilder);
      }
      xmlBuilder.elementEnd(); // end effectiveTime
    }
  }

  private void buildEffectiveTime(CdaDate time, XmlStreamBuilder xmlBuilder) throws IOException {
    if (time != null) {
      xmlBuilder.element("effectiveTime").attribute("value", time.dateToFormattedString()).elementShortEnd();
    }
  }

  private void buildEffectiveTimes(CdaDate effectiveTime, CdaDateInterval effectiveTimeInterval,
      XmlStreamBuilder xmlBuilder) throws IOException {
    if (effectiveTimeInterval != null) {//TODO0: hvad med null value på nogen datoer?
      buildEffectiveTimes(effectiveTimeInterval, xmlBuilder);
    } else {
      buildEffectiveTime(effectiveTime, xmlBuilder);
    }
  }

  private void buildId(ID id, XmlStreamBuilder xmlBuilder) throws IOException {
    if (id.getExtension() == null) {
      BuildUtil.buildId(id.getRoot(), xmlBuilder);
    } else {
      BuildUtil.buildId(id.getRoot(), id.getExtension(), xmlBuilder);
    }
  }

  private void buildStatusCode(Status statusCode, XmlStreamBuilder xmlBuilder) throws IOException {
    if (statusCode == Status.ACTIVE) {
      xmlBuilder.element("statusCode").attribute("code", "active").elementShortEnd();
    } else if (statusCode == Status.COMPLETED) {
      xmlBuilder.element("statusCode").attribute("code", "completed").elementShortEnd();
    }
  }

  private void buildTemplateId(String id, XmlStreamBuilder xmlBuilder) throws IOException {
    BuildUtil.buildTemplateIdsWithExtension(xmlBuilder, HL7.CPD_SECTION_EXTENSION, id);
  }

  private void buildText(String text, boolean emptyTag, XmlStreamBuilder xmlBuilder) throws IOException {
    if (text != null && !text.isEmpty()) {
      xmlBuilder.element("text").valueNoEscaping(text).elementEnd();
    } else if (emptyTag) {
      xmlBuilder.emptyShortEnd("text");
    }
  }

  private void buildTitle(String title, XmlStreamBuilder xmlBuilder) throws IOException {
    if (title != null) {
      xmlBuilder.element("title").value(title).elementEnd();
    }
  }

}