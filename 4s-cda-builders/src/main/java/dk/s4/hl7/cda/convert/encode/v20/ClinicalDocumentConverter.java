package dk.s4.hl7.cda.convert.encode.v20;

import java.io.IOException;
import java.text.Format;

import org.apache.commons.lang3.time.FastDateFormat;

import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.RelatedDocument;
import dk.s4.hl7.cda.model.core.v20.ClinicalDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

/**
 * Abstract base class for all CDA converters that writes xml from the GreenCDA
 * model.
 * 
 * Contains overall process of converting and helper methods
 * 
 * @author Frank Jacobsen Systematic A/S
 */
public abstract class ClinicalDocumentConverter<E extends ClinicalDocument, C>
    extends dk.s4.hl7.cda.convert.encode.ClinicalDocumentConverter<E, C> {

  private static final Format birthDateFormatter = FastDateFormat.getInstance("yyyyMMdd");
  protected static final Format birthDateAndTimeFormatter = FastDateFormat.getInstance("yyyyMMddHHmmssZ");

  public ClinicalDocumentConverter(XmlPrettyPrinter xmlPrettyPrinter) {
    super(xmlPrettyPrinter);
  }

  @Override
  public void buildHeader(E clinicalDocument, XmlStreamBuilder xmlBuilder) throws IOException {
    buildHeaderInfomation(clinicalDocument, xmlBuilder);
    buildContext(clinicalDocument, xmlBuilder);
    buildPatientSection(clinicalDocument, xmlBuilder);
    buildAuthorSection(clinicalDocument.getPatient(), clinicalDocument.getAuthor(), xmlBuilder);
    buildAuthorReferrerSection(clinicalDocument.getAuthorReferrer(), xmlBuilder);
    buildDataEnterer(clinicalDocument, false, xmlBuilder);
    buildCustodianSection(clinicalDocument, false, xmlBuilder);
    buildInformationRecipient(clinicalDocument, xmlBuilder);
    buildLegalAuthenticatorSection(clinicalDocument, xmlBuilder);
    buildParticipants(clinicalDocument, xmlBuilder);
    buildDocumentationOf(clinicalDocument, xmlBuilder);
    buildRelatedDocument(clinicalDocument.getRelateDocument(), clinicalDocument.getCode(),
        clinicalDocument.getCodeDisplayName(), xmlBuilder);
  }

  @Override
  protected void buildRepresentedOrganization(OrganizationIdentity organizationIdentity, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (organizationIdentity != null && organizationIdentity.getOrgName() != null
        && !organizationIdentity.getOrgName().trim().isEmpty()) {
      xmlBuilder.element("representedOrganization").attribute("classCode", "ORG").attribute("determinerCode",
          "INSTANCE");
      BuildUtil.buildIdOptional(organizationIdentity.getId(), xmlBuilder);
      xmlBuilder.element("name").value(organizationIdentity.getOrgName()).elementEnd();
      buildTelecom(organizationIdentity.getTelecomList(), xmlBuilder);
      buildAddress(organizationIdentity.getAddress(), false, xmlBuilder);
      xmlBuilder.elementEnd(); // end representedOrganization
    }
  }

  @Override
  protected void buildPatientExtra(E document, XmlStreamBuilder xmlBuilder) throws IOException {
    buildProviderOrganization(document.getProviderOrganization(), xmlBuilder);
  }

  @Override
  protected void buildBirthTime(Patient patientIdentity, XmlStreamBuilder xmlBuilder) throws IOException {
    if (patientIdentity.getBirthDate() != null) {
      if (patientIdentity.isBirthTimeOnlyDate()) {
        xmlBuilder
            .element("birthTime")
            .attribute("value", birthDateFormatter.format(patientIdentity.getBirthDate()))
            .elementShortEnd();
      } else {
        xmlBuilder
            .element("birthTime")
            .attribute("value", birthDateAndTimeFormatter.format(patientIdentity.getBirthDateAndTime()))
            .elementShortEnd();
      }

    } else {
      BuildUtil.buildNullFlavor("birthTime", xmlBuilder);
    }

  }

  protected void buildProviderOrganization(OrganizationIdentity organizationIdentity, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (organizationIdentity != null && organizationIdentity.getOrgName() != null
        && !organizationIdentity.getOrgName().trim().isEmpty() && organizationIdentity.getId() != null
        && organizationIdentity.getId().getExtension() != null
        && !organizationIdentity.getId().getExtension().trim().isEmpty()) {
      xmlBuilder.element("providerOrganization");
      BuildUtil.buildId(organizationIdentity.getId(), xmlBuilder);
      xmlBuilder.element("name").value(organizationIdentity.getOrgName()).elementEnd();
      buildTelecom(organizationIdentity.getTelecomList(), xmlBuilder);
      buildAddress(organizationIdentity.getAddress(), true, xmlBuilder);
      xmlBuilder.elementEnd(); // end providerOrganization
    }
  }

  protected void buildAuthorReferrerSection(Participant author, XmlStreamBuilder xmlBuilder) throws IOException {
    if (author != null) {
      xmlBuilder.element("author").attribute("typeCode", "AUT").attribute("contextControlCode", "OP");
      if (author.getTime() != null) {
        xmlBuilder.element("time").attribute("value", dateTimeformatter.format(author.getTime())).elementShortEnd();
      }

      xmlBuilder.element("assignedAuthor").attribute("classCode", "ASSIGNED");
      BuildUtil.buildId(author.getId(), xmlBuilder);

      BuildUtil.buildCode(MedCom.PARTICIPANT_TYPE_CODE_REFB, MedCom.PARTICIPANT_TYPE_CODESYSTEM,
          MedCom.PARTICIPANT_TYPE_DISPLAYNAME_REFB, MedCom.PARTICIPANT_TYPE_CODESYSTEMNAME, xmlBuilder);

      buildAddress(author.getAddress(), true, xmlBuilder);
      buildTelecom(author.getTelecomList(), xmlBuilder);
      // assignedPerson
      if (author.getPersonIdentity() != null) {
        xmlBuilder.element("assignedPerson").attribute("classCode", "PSN");
        buildName(author.getPersonIdentity(), xmlBuilder);
        xmlBuilder.elementEnd(); // end assignedPerson
      }
      xmlBuilder.elementEnd(); // end assignedAuthor
      xmlBuilder.elementEnd(); // end author
    }
  }

  protected void buildRelatedDocument(RelatedDocument relatedDocument, String code, String displayName,
      XmlStreamBuilder xmlBuilder) throws IOException {

    if (relatedDocument != null) {
      if (relatedDocument.isReplaced()) {
        xmlBuilder.element("relatedDocument").attribute("typeCode", "RPLC");
      } else {
        xmlBuilder.element("relatedDocument").attribute("typeCode", "APND");
      }
      xmlBuilder.element("parentDocument");

      xmlBuilder.element("id").attribute("root", relatedDocument.getId()).elementShortEnd();

      xmlBuilder
          .element("code")
          .attribute("code", code)
          .attribute("codeSystem", Loinc.OID)
          .attribute("codeSystemName", Loinc.DISPLAYNAME)
          .attribute("displayName", displayName)
          .elementShortEnd();

      xmlBuilder.element("setId").attribute("root", relatedDocument.getSetId()).elementShortEnd();
      //TODO: skal være ID senere.
      //      if (isNotNullAndEmpty(relatedDocument.getSetId())) {
      //        xmlBuilder
      //            .element("setId")
      //            .attribute("root", MedCom.MESSAGECODE_OID)
      //            .attribute("extension", relatedDocument.getSetId())
      //            .elementShortEnd();
      //      }
      if (relatedDocument.getVersionNumber() != null) {
        xmlBuilder
            .element("versionNumber")
            .attribute("value", relatedDocument.getVersionNumber().toString())
            .elementShortEnd();
      }

      xmlBuilder.elementEnd(); // end parentDocument
      xmlBuilder.elementEnd(); // end relatedDocument

    }
  }

}