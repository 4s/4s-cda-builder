package dk.s4.hl7.cda.convert.decode.qrd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.qfdd.CopyRightHandler;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class SectionHandler implements CDAXmlHandler<QRDDocument> {
  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section";

  private String title;
  private String language;
  private List<Section<QRDOrganizer>> sections;
  private OrganizerHandler organizerHandler;
  private RawTextHandler rawTextHandler;
  private CopyRightHandler copyRightHandler;

  public SectionHandler() {
    title = null;
    language = null;
    organizerHandler = new OrganizerHandler();
    copyRightHandler = new CopyRightHandler();
    sections = new ArrayList<Section<QRDOrganizer>>();
    rawTextHandler = new RawTextHandler(COMPONENT_SECTION + "/text", "text");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "title")) {
      title = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "languageCode", "code")) {
      language = xmlElement.getAttributeValue("code");
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "section")) {
      if (copyRightHandler.isCopyRightSection()) {
        Section sectionCopyright = new Section(title, rawTextHandler.getRawText(), language);
        sectionCopyright.setCopyrightTexts(copyRightHandler.getCopyRightTexts());
        sections.add(sectionCopyright);
      } else {
        Section<QRDOrganizer> section = new Section<QRDOrganizer>(title, rawTextHandler.getRawText(), language);
        for (QRDOrganizer organizer : organizerHandler.getOrganizers()) {
          section.addOrganizer(organizer);
        }
        sections.add(section);
      }
      clear();
    }
  }

  public void clear() {
    rawTextHandler.clear();
    title = null;
    language = null;
    organizerHandler.clear();
    copyRightHandler.clear();
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    organizerHandler.addHandlerToMap(xmlMapping);
    copyRightHandler.addHandlerToMap(xmlMapping);
    xmlMapping.add(COMPONENT_SECTION, this);
    xmlMapping.add(COMPONENT_SECTION + "/title", this);
    xmlMapping.add(COMPONENT_SECTION + "/languageCode", this);
    rawTextHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    organizerHandler.removeHandlerFromMap(xmlMapping);
    copyRightHandler.removeHandlerFromMap(xmlMapping);
    rawTextHandler.removeHandlerFromMap(xmlMapping);
    xmlMapping.remove(COMPONENT_SECTION);
    xmlMapping.remove(COMPONENT_SECTION + "/title");
    xmlMapping.remove(COMPONENT_SECTION + "/languageCode");
  }

  @Override
  public void addDataToDocument(QRDDocument clinicalDocument) {
    for (Section<QRDOrganizer> section : sections) {
      clinicalDocument.addSection(section);
    }
  }
}
