package dk.s4.hl7.cda.convert.decode.phmr;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.ObservationRangePatternHandler;
import dk.s4.hl7.cda.convert.decode.general.ReferenceHandler;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.DataInputContext;
import dk.s4.hl7.cda.model.DataInputContext.PerformerType;
import dk.s4.hl7.cda.model.DataInputContext.ProvisionMethod;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.PhysicalQuantityInterval;
import dk.s4.hl7.cda.model.PhysicalQuantityInterval.PhysicalQuantityIntervalBuilder;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.ReferenceRange;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.Measurement.MeasurementBuilder;
import dk.s4.hl7.cda.model.phmr.Measurement.Status;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlHandler;
import dk.s4.hl7.util.xml.XmlMapping;

public class MeasurementHandler implements XmlHandler {
  private static final String NO_VITAL_SIGNS_TEXT = "No Vital Signs";
  private static final String NO_RESULTS_TEXT = "No Results";

  public static final String COMPONENT_SECTION_TEXT = "/ClinicalDocument/component/structuredBody/component/section/text";
  public static final String COMPONENT_OBSERVATION_ENTRY = "/ClinicalDocument/component/structuredBody/component/section/entry";
  public static final String COMPONENT_OBSERVATION_CODE = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/code";
  public static final String COMPONENT_OBSERVATION_ID = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/id";
  public static final String COMPONENT_OBSERVATION_VALUE_LOW = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/value/low";
  public static final String COMPONENT_OBSERVATION_VALUE_HIGH = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/value/high";

  private CodedValue observationCode;
  private CodedValue observationTranslatedCode;
  private MeasurementBuilder measurementBuilder;
  private PhysicalQuantityIntervalBuilder measurementIntervalBuilder = null;
  private List<Measurement> vitalMeasurements;
  private List<Measurement> resultMeasurements;
  private DataInputContext context;
  private ReferenceHandler referenceHandler;
  private RawTextHandler rawTextHandler;
  private ObservationRangePatternHandler observationRangeHandler;
  private String resultText;
  private String vitalText;

  private String componentSectionCode;

  public MeasurementHandler() {
    this.vitalMeasurements = new ArrayList<Measurement>();
    this.resultMeasurements = new ArrayList<Measurement>();
    this.referenceHandler = new ReferenceHandler();
    this.rawTextHandler = new RawTextHandler(COMPONENT_SECTION_TEXT, "text");
    this.observationRangeHandler = new ObservationRangePatternHandler();
    this.resultText = NO_RESULTS_TEXT;
    this.vitalText = NO_VITAL_SIGNS_TEXT;
  }

  public void setComponentSectionCode(String componentSectionCode) {
    this.componentSectionCode = componentSectionCode;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "entry")) {
      measurementBuilder = new Measurement.MeasurementBuilder();
      context = new DataInputContext();
      measurementBuilder.setContext(context);
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "statusCode", "code")) {
      setStatus(xmlElement.getAttributeValue("code"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "effectiveTime", "value")) {
      measurementBuilder.setDate(ConvertXmlUtil.getDateFromyyyyMMddhhmmss(xmlElement.getAttributeValue("value")));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getXPath(), COMPONENT_OBSERVATION_ID, "root",
        "extension")) {
      ID id = new ID.IDBuilder()
          .setAuthorityName(xmlElement.getAttributeValue("assigningAuthorityName"))
          .setExtension(xmlElement.getAttributeValue("extension"))
          .setRoot(xmlElement.getAttributeValue("root"))
          .build();
      measurementBuilder.setId(id);
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "translation", "code",
        "codeSystem")) {
      observationTranslatedCode = new CodedValue(xmlElement.getAttributeValue("code"),
          xmlElement.getAttributeValue("codeSystem"), xmlElement.getAttributeValue("displayName"),
          xmlElement.getAttributeValue("codeSystemName"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getXPath(), COMPONENT_OBSERVATION_CODE, "code",
        "codeSystem")) {
      observationCode = new CodedValue(xmlElement.getAttributeValue("code"), xmlElement.getAttributeValue("codeSystem"),
          xmlElement.getAttributeValue("displayName"), xmlElement.getAttributeValue("codeSystemName"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "value", "unit")) {
      setPhysicalQuantity(xmlElement);
    } else if (ConvertXmlUtil.isElementPresentOrNullFlavoredUnknown(xmlElement, xmlElement.getXPath(),
        COMPONENT_OBSERVATION_VALUE_LOW)) {
      setPhysicalQuantityInterval(xmlElement, true);
    } else if (ConvertXmlUtil.isElementPresentOrNullFlavoredUnknown(xmlElement, xmlElement.getXPath(),
        COMPONENT_OBSERVATION_VALUE_HIGH)) {
      setPhysicalQuantityInterval(xmlElement, false);
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "value")) {
      setPhysicalQuantity(xmlElement);
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "methodCode", "code")) {
      String code = xmlElement.getAttributeValue("code");
      PerformerType performerType = MeasurementHandlerUtil.getPerformerType(code);
      ProvisionMethod ProvisionCode = MeasurementHandlerUtil.getProvisionCode(code);
      if (ProvisionCode != null) {
        context.setDataProvision(ProvisionCode);
      }
      if (performerType != null) {
        context.setMeasurementActor(performerType);
      }
    }
  }

  private void setStatus(String statusCodeCode) {
    if (Status.COMPLETED.toString().equalsIgnoreCase(statusCodeCode)) {
      measurementBuilder.setStatus(Status.COMPLETED);
    } else if (Status.NULLIFIED.toString().equalsIgnoreCase(statusCodeCode)) {
      measurementBuilder.setStatus(Status.NULLIFIED);
    }
  }

  private void setPhysicalQuantity(XMLElement xmlElement) {
    String unit = xmlElement.getAttributeValue("unit");
    String value = xmlElement.getAttributeValue("value");
    if (observationCode == null) {
      // Set default values
      observationCode = new CodedValue(null, null, null);
    }
    measurementBuilder.setPhysicalQuantity(value, unit, observationCode.getCode(), observationCode.getDisplayName(),
        observationCode.getCodeSystem(), observationCode.getCodeSystemName());
    if (observationTranslatedCode != null) {
      measurementBuilder.setTranlated(observationTranslatedCode.getCode(), observationTranslatedCode.getDisplayName(),
          observationTranslatedCode.getCodeSystem(), observationTranslatedCode.getCodeSystemName());
    }
  }

  private void setPhysicalQuantityInterval(XMLElement xmlElement, boolean isLow) {
    boolean lowAndHighSet = false;
    if (measurementIntervalBuilder == null) {
      measurementIntervalBuilder = new PhysicalQuantityIntervalBuilder();
    } else {
      lowAndHighSet = true;
    }
    if (isLow) {
      if (ConvertXmlUtil.isNullFlavorUnknown(xmlElement)) {
        measurementIntervalBuilder.setLowValueUnknown();
      } else {
        String unit = xmlElement.getAttributeValue("unit");
        String value = xmlElement.getAttributeValue("value");
        String inclusive = xmlElement.getAttributeValue("inclusive");
        measurementIntervalBuilder.setLowValue(value, "true".equalsIgnoreCase(inclusive));
        measurementIntervalBuilder.setUnit(unit);
      }
    } else {
      if (ConvertXmlUtil.isNullFlavorUnknown(xmlElement)) {
        measurementIntervalBuilder.setHighValueUnknown();
      } else {
        String unit = xmlElement.getAttributeValue("unit");
        String value = xmlElement.getAttributeValue("value");
        String inclusive = xmlElement.getAttributeValue("inclusive");
        measurementIntervalBuilder.setHighValue(value, "true".equalsIgnoreCase(inclusive));
        measurementIntervalBuilder.setUnit(unit);
      }
    }

    if (lowAndHighSet) { // Both set means ready to build interval
      if (observationCode == null) {
        // Set default values
        observationCode = new CodedValue(null, null, null);
      }
      PhysicalQuantityInterval interval = measurementIntervalBuilder.build();
      measurementBuilder.setPhysicalQuantity(interval, interval.getUnit(), observationCode);
      if (observationTranslatedCode != null) {
        measurementBuilder.setTranlated(observationTranslatedCode.getCode(), observationTranslatedCode.getDisplayName(),
            observationTranslatedCode.getCodeSystem(), observationTranslatedCode.getCodeSystemName());
      }
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase("entry") && measurementBuilder != null) {
      Measurement measurement = measurementBuilder.build();
      measurementBuilder = null;
      setSectionText();
      for (Reference reference : referenceHandler.getReferences()) {
        measurement.addReference(reference);
      }
      referenceHandler.clear();
      for (ReferenceRange referenceRange : observationRangeHandler.getObservationReferenceRanges()) {
        measurement.addObservationRange(referenceRange);
      }
      observationRangeHandler.clear();
      if (Loinc.SECTION_RESULTS_CODE.equalsIgnoreCase(componentSectionCode)) {
        resultMeasurements.add(measurement);
      } else if (Loinc.SECTION_VITAL_SIGNS_CODE.equalsIgnoreCase(componentSectionCode)) {
        vitalMeasurements.add(measurement);
      }
    }
  }

  private void setSectionText() {
    if (NO_RESULTS_TEXT.equals(resultText) && Loinc.SECTION_RESULTS_CODE.equalsIgnoreCase(componentSectionCode)) {
      resultText = rawTextHandler.getRawText();
    } else if (NO_VITAL_SIGNS_TEXT.equals(vitalText)
        && Loinc.SECTION_VITAL_SIGNS_CODE.equalsIgnoreCase(componentSectionCode)) {
      vitalText = rawTextHandler.getRawText();
    }
  }

  public void addDataToDocument(PHMRDocument phmrDocument) {
    if (!vitalMeasurements.isEmpty()) {
      phmrDocument.setVitalSignsText(vitalText);
      for (Measurement measurement : vitalMeasurements) {
        phmrDocument.addVitalSign(measurement);
      }
    }
    if (resultMeasurements != null) {
      phmrDocument.setResultsText(resultText);
      for (Measurement measurement : resultMeasurements) {
        phmrDocument.addResult(measurement);
      }
    }
  }

  @Override
  public boolean includeChildren() {
    return true;
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add(COMPONENT_OBSERVATION_ENTRY, this);
    referenceHandler.addHandlerToMap(xmlMapping);
    rawTextHandler.addHandlerToMap(xmlMapping);
    observationRangeHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove(COMPONENT_OBSERVATION_ENTRY);
    referenceHandler.removeHandlerFromMap(xmlMapping);
    rawTextHandler.removeHandlerFromMap(xmlMapping);
    observationRangeHandler.removeHandlerFromMap(xmlMapping);
  }
}