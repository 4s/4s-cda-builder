package dk.s4.hl7.cda.convert.decode.pdc.v20;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.IdHandler;
import dk.s4.hl7.cda.convert.decode.general.ValueHandler;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.pdc.v20.CoverageGroup;
import dk.s4.hl7.cda.model.pdc.v20.CustodyInformation;
import dk.s4.hl7.cda.model.pdc.v20.LivingWillRegistration;
import dk.s4.hl7.cda.model.pdc.v20.ManuallyEnteredContactInformation;
import dk.s4.hl7.cda.model.pdc.v20.ManuallyEnteredDentistInformation;
import dk.s4.hl7.cda.model.pdc.v20.ManuallyEnteredInformationAboutRelatives;
import dk.s4.hl7.cda.model.pdc.v20.ManuallyEnteredSpokenLanguage;
import dk.s4.hl7.cda.model.pdc.v20.ManuallyEnteredTemporaryAddress;
import dk.s4.hl7.cda.model.pdc.v20.NameAndAddressInformation;
import dk.s4.hl7.cda.model.pdc.v20.OrganDonorRegistration;
import dk.s4.hl7.cda.model.pdc.v20.TreatmentWillRegistration;
import dk.s4.hl7.cda.model.pdc.v20.CoverageGroup.CoverageGroupBuilder;
import dk.s4.hl7.cda.model.pdc.v20.CustodyInformation.CustodyInformationBuilder;
import dk.s4.hl7.cda.model.pdc.v20.LivingWillRegistration.LivingWillRegistrationBuilder;
import dk.s4.hl7.cda.model.pdc.v20.ManuallyEnteredContactInformation.ManuallyEnteredContactInformationBuilder;
import dk.s4.hl7.cda.model.pdc.v20.ManuallyEnteredDentistInformation.ManuallyEnteredDentistInformationBuilder;
import dk.s4.hl7.cda.model.pdc.v20.ManuallyEnteredInformationAboutRelatives.ManuallyEnteredInformationAboutRelativesBuilder;
import dk.s4.hl7.cda.model.pdc.v20.ManuallyEnteredSpokenLanguage.ManuallyEnteredSpokenLanguageBuilder;
import dk.s4.hl7.cda.model.pdc.v20.ManuallyEnteredTemporaryAddress.ManuallyEnteredTemporaryAddressBuilder;
import dk.s4.hl7.cda.model.pdc.v20.NameAndAddressInformation.NameAndAddressInformationBuilder;
import dk.s4.hl7.cda.model.pdc.v20.OrganDonorRegistration.OrganDonorRegistrationBuilder;
import dk.s4.hl7.cda.model.pdc.v20.TreatmentWillRegistration.TreatmentWillRegistrationBuilder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class ObservationHandler extends BaseXmlHandler {
  public static final String OBSERVATION_SECTION = "/ClinicalDocument/component/structuredBody/component/section/entry/observation";

  private List<CustodyInformation> custodyInformationList;
  private NameAndAddressInformation nameAndAddressInformation;
  private CoverageGroup coverageGroup;
  private OrganDonorRegistration organDonorRegistration;
  private TreatmentWillRegistration treatmentWillRegistration;
  private LivingWillRegistration livingWillRegistration;
  private ManuallyEnteredSpokenLanguage manuallyEnteredSpokenLanguage;
  private ManuallyEnteredTemporaryAddress manuallyEnteredTemporaryAddress;
  private ManuallyEnteredDentistInformation manuallyEnteredDentistInformation;
  private ManuallyEnteredContactInformation manuallyEnteredContactInformation;
  private List<ManuallyEnteredInformationAboutRelatives> manuallyEnteredInformationAboutRelativesList;

  private String template;
  private CodedValue code;
  private IdHandler idHandler;
  private ValueHandler valueHandler;
  private AuthorOfInformationHandler authorHandlerOfInformation;

  private Date low;
  private Date high;

  public ObservationHandler() {
    this.idHandler = new IdHandler(OBSERVATION_SECTION);
    this.authorHandlerOfInformation = new AuthorOfInformationHandler(OBSERVATION_SECTION);
    this.valueHandler = new ValueHandler(OBSERVATION_SECTION, false);

    addPath(OBSERVATION_SECTION + "/templateId");
    addPath(OBSERVATION_SECTION + "/code");

    addPath(OBSERVATION_SECTION);
    addPath(OBSERVATION_SECTION + "/effectiveTime/low");
    addPath(OBSERVATION_SECTION + "/effectiveTime/high");

    localClear();
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      localClear();
      template = xmlElement.getAttributeValue("root");

    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code",
        "codeSystem")) {
      code = new CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "low", "value")) {
      low = ConvertXmlUtil.getDateFromyyyyMMddhhmmss(xmlElement.getAttributeValue("value"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "high", "value")) {
      high = ConvertXmlUtil.getDateFromyyyyMMddhhmmss(xmlElement.getAttributeValue("value"));
    }

  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "observation")) {
      if (template != null) {
        switch (template) {
        case MedCom.PDC_CUSTODY_INFORMATION_ROOT:
          createCustodyInformation();
          break;
        case MedCom.PDC_NAME_AND_ADDRESS_INFORMATION_ROOT:
          createNameAndAddressInformation();
          break;
        case MedCom.PDC_COVERAGE_GROUP_ROOT:
          createCoverageGroup();
          break;
        case MedCom.PDC_ORGAN_DONOR_REGISTRATION_ROOT:
          createOrganDonorRegistration();
          break;
        case MedCom.PDC_TREATMENT_WILL_REGISTRATION_ROOT:
          createTreatmentWillRegistration();
          break;
        case MedCom.PDC_LIVING_WILL_REGISTRATION_ROOT:
          createLivingWillRegistration();
          break;
        case MedCom.PDC_MANUALLY_SPOKEN_LANGUAGE_ROOT:
          createManuallyEnteredSpokenLanguage();
          break;
        case MedCom.PDC_MANUALLY_ENTERED_TEMPORARY_ADDRESS_ROOT:
          createManuallyEnteredTemporaryAddress();
          break;
        case MedCom.PDC_MANUALLY_ENTERED_DENTIST_INFORMATION_ROOT:
          createManuallyEnteredDentistInformation();
          break;
        case MedCom.PDC_MANUALLY_ENTERED_CONTACT_INFORMATION_ROOT:
          createManuallyEnteredContactInformation();
          break;
        case MedCom.PDC_MANUALLY_ENTERED_INFOMATION_ABOUT_RELATIVES_ROOT:
          createManuallyEnteredInformationAboutRelatives();
          break;
        }

      }
    }
  }

  private void createCustodyInformation() {
    if (custodyInformationList == null) {
      custodyInformationList = new ArrayList<CustodyInformation>();
    }

    CustodyInformation custodyInformation = new CustodyInformationBuilder(idHandler.getId())
        .setCode(this.code)
        .setCpr(valueHandler.getCpr())
        .setPersonIdentity(valueHandler.getPersonIdentity())
        .setRelationCode(valueHandler.getCode())
        .setAuthorOfInformation(authorHandlerOfInformation.getAuthorOfInformation())
        .build();

    custodyInformationList.add(custodyInformation);
  }

  private void createNameAndAddressInformation() {
    nameAndAddressInformation = new NameAndAddressInformationBuilder(idHandler.getId())
        .setCode(this.code)
        .setPersonIdentity(valueHandler.getPersonIdentity())
        .setAddressData(valueHandler.getAddressData(Use.HomeAddress))
        .setConfidentialId(valueHandler.getIdentification())
        .setAuthorOfInformation(authorHandlerOfInformation.getAuthorOfInformation())
        .build();

  }

  private void createCoverageGroup() {
    coverageGroup = new CoverageGroupBuilder(idHandler.getId())
        .setCode(this.code)
        .setCoverageGroup(valueHandler.getCoverageGroupValue())
        .setAuthorOfInformation(authorHandlerOfInformation.getAuthorOfInformation())
        .build();
  }

  private void createOrganDonorRegistration() {
    organDonorRegistration = new OrganDonorRegistrationBuilder(idHandler.getId())
        .setCode(this.code)
        .setRegisteret(valueHandler.isRegistered())
        .setAuthorOfInformation(authorHandlerOfInformation.getAuthorOfInformation())
        .build();

  }

  private void createTreatmentWillRegistration() {
    treatmentWillRegistration = new TreatmentWillRegistrationBuilder(idHandler.getId())
        .setCode(this.code)
        .setRegisteret(valueHandler.isRegistered())
        .setAuthorOfInformation(authorHandlerOfInformation.getAuthorOfInformation())
        .build();

  }

  private void createLivingWillRegistration() {
    livingWillRegistration = new LivingWillRegistrationBuilder(idHandler.getId())
        .setCode(this.code)
        .setRegisteret(valueHandler.isRegistered())
        .setAuthorOfInformation(authorHandlerOfInformation.getAuthorOfInformation())
        .build();
  }

  private void createManuallyEnteredSpokenLanguage() {
    manuallyEnteredSpokenLanguage = new ManuallyEnteredSpokenLanguageBuilder(idHandler.getId())
        .setCode(this.code)
        .setLanguageCode(valueHandler.getCode())
        .setAuthorOfInformation(authorHandlerOfInformation.getAuthorOfInformation())
        .build();

  }

  private void createManuallyEnteredTemporaryAddress() {
    manuallyEnteredTemporaryAddress = new ManuallyEnteredTemporaryAddressBuilder(idHandler.getId())
        .setCode(this.code)
        .setEffectiveFrom(low)
        .setEffectiveTo(high)
        .setAddressData(valueHandler.getAddressData(Use.HomeAddress))
        .setAuthorOfInformation(authorHandlerOfInformation.getAuthorOfInformation())
        .build();

  }

  private void createManuallyEnteredDentistInformation() {
    manuallyEnteredDentistInformation = new ManuallyEnteredDentistInformationBuilder(idHandler.getId())
        .setCode(this.code)
        .setIdentification(valueHandler.getIdentification())
        .setPersonIdentity(valueHandler.getPersonIdentity())
        .setAddressData(valueHandler.getAddressData(null))
        .setTelecomPhone(valueHandler.getTelecoms())
        .setAuthorOfInformation(authorHandlerOfInformation.getAuthorOfInformation())
        .build();

  }

  private void createManuallyEnteredContactInformation() {
    manuallyEnteredContactInformation = new ManuallyEnteredContactInformationBuilder(idHandler.getId())
        .setCode(this.code)
        .setTelecomPhone(valueHandler.getTelecoms())
        .setAuthorOfInformation(authorHandlerOfInformation.getAuthorOfInformation())
        .build();
  }

  private void createManuallyEnteredInformationAboutRelatives() {
    if (manuallyEnteredInformationAboutRelativesList == null) {
      manuallyEnteredInformationAboutRelativesList = new ArrayList<ManuallyEnteredInformationAboutRelatives>();
    }

    ManuallyEnteredInformationAboutRelatives manuallyEnteredInformationAboutRelatives = new ManuallyEnteredInformationAboutRelativesBuilder(
        idHandler.getId())
            .setCode(this.code)
            .setPersonIdentity(valueHandler.getPersonIdentity())
            .setTelecomPhone(valueHandler.getTelecoms())
            .setRelationCode(valueHandler.getCode())
            .setNote(valueHandler.getString())
            .setAuthorOfInformation(authorHandlerOfInformation.getAuthorOfInformation())
            .build();

    manuallyEnteredInformationAboutRelativesList.add(manuallyEnteredInformationAboutRelatives);
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    idHandler.addHandlerToMap(xmlMapping);
    valueHandler.addHandlerToMap(xmlMapping);
    authorHandlerOfInformation.addHandlerToMap(xmlMapping);

  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    idHandler.removeHandlerFromMap(xmlMapping);
    valueHandler.removeHandlerFromMap(xmlMapping);
    authorHandlerOfInformation.removeHandlerFromMap(xmlMapping);

  }

  public List<CustodyInformation> getCustodyInformationList() {
    return custodyInformationList;
  }

  public NameAndAddressInformation getNameAndAddressInformation() {
    return nameAndAddressInformation;
  }

  public OrganDonorRegistration getOrganDonorRegistration() {
    return organDonorRegistration;
  }

  public CoverageGroup getCoverageGroup() {
    return coverageGroup;
  }

  public TreatmentWillRegistration getTreatmentWillRegistration() {
    return treatmentWillRegistration;
  }

  public LivingWillRegistration getLivingWillRegistration() {
    return livingWillRegistration;
  }

  public ManuallyEnteredSpokenLanguage getManuallyEnteredSpokenLanguage() {
    return manuallyEnteredSpokenLanguage;
  }

  public ManuallyEnteredTemporaryAddress getManuallyEnteredTemporaryAddress() {
    return manuallyEnteredTemporaryAddress;
  }

  public ManuallyEnteredDentistInformation getManuallyEnteredDentistInformation() {
    return manuallyEnteredDentistInformation;
  }

  public ManuallyEnteredContactInformation getManuallyEnteredContactInformation() {
    return manuallyEnteredContactInformation;
  }

  public List<ManuallyEnteredInformationAboutRelatives> getManuallyEnteredInformationAboutRelativesList() {
    return manuallyEnteredInformationAboutRelativesList;
  }

  public void clear() {
    custodyInformationList = null;
    nameAndAddressInformation = null;
    coverageGroup = null;
    organDonorRegistration = null;
    treatmentWillRegistration = null;
    livingWillRegistration = null;
    manuallyEnteredSpokenLanguage = null;
    manuallyEnteredTemporaryAddress = null;
    manuallyEnteredDentistInformation = null;
    manuallyEnteredContactInformation = null;
    manuallyEnteredInformationAboutRelativesList = null;
    localClear();

  }

  private void localClear() {
    template = null;
    code = null;
    idHandler.clear();
    authorHandlerOfInformation.clear();
    low = null;
    high = null;
    valueHandler.clear();

  }

}
