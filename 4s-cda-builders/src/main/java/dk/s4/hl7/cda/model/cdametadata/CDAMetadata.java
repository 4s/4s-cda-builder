package dk.s4.hl7.cda.model.cdametadata;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.core.BaseClinicalDocument;

/**
 * An implementation of SimpleClinicalDocument handling meta data 
 * 
 */
public class CDAMetadata extends BaseClinicalDocument {

  //The following list of fields are the one available as fields for Meta data in a Danish CDA document.
  //Some of them are not available in the CDA header itself. Those are marked with RDK
  //Some are assigned automatically when uploaded. Those are marked with AUT
  //Some of the fields are already available in the BaseClinicalDcoument. When relevant those have been "encapsulated" for simplicity. Others uses the super super class methods.
  //The rest of the fields are implemented in this CDAMetadata class.
  //Each "field" might cover more sub fields. The relevant methods to fetch the the correct data are listed.

  // author.authorInstitution   						// .getAuthorInstitution().getDisplayName(), .getAuthorInstitution().getCode(), .getAuthorInstitution().getCodeSystem()
  // author.authorperson 	     						// .getAuthorperson().getGivenNames(), .getAuthorperson().getFamilyName()
  // availabilityStatus - RDK
  // classCode - RDK
  private CodedValue confidentialityCodeCodedValue; // .getConfidentialityCodeCodedValue().getCode(), getConfidentialityCodeCodedValue().getCodeSystem(), .getConfidentialityCodeCodedValue().getDisplayName()		
  // contentTypeCode - not included in dk format
  // creationTime - 									// .getCreationTime()
  // entryUUID - AUT 
  private List<CodedValue> eventCodeList; // .getEventCodeList().get(i).getCode(), CdaMetadata.getEventCodeList().get(i).getCodeSystem(), .getEventCodeList().get(i).getDisplayName() 
  // formatCode - RDK									// .getFormatCode //in apd v2.0 this can be retrieved and is no longer RDK, but since Metadata v. 2.0 is not yet implemented, the implementation is put in below instead of core model
  private CodedValue formatCode;
  // hash - AUT
  // healthcareFacilityTypeCode - RDK
  // homeCommunityId - AUT
  // languageCode 										// .getLanguageCode()
  // legalAuthenticator									// .getLegalAuthenticator().getPersonIdentity().getGivenNames(), .getLegalAuthenticator().getPersonIdentity().getFamilyName()
  // mimeType - AUT
  // objectType - RDK
  // patientId;											// .getPatientId().getCode(), .getPatientId().getCodeSystem()
  // practiceSettingCode - RDK
  // referenceIdList - RDK
  // repositoryUniqueId - AUT
  // serviceStartTime									// .getServiceStartTime()
  // serviceStopTime									// .getServiceStopTime()
  // size - AUT
  // sourcePatientId									// .getSourcePatientId().getCode(), .getSourcePatientId().getCodeSystem()
  // sourcePatientInfo									// .getPatient().getFamilyName(), .getPatient().getGivenNames(), .getPatient().getBirthTime(), .getPatient().getGender()
  // submissionTime - RDK
  // title												// .getTitle()
  // typeCode											// .getCodeCodedValue().getCode(), .getCodeCodedValue().getDisplayName(), .getCodeCodedValue().getCodeSystem()
  // uniqueId											// .getId().getExtension(), .getId().getRoot()
  // URI - AUT

  public static final String METADATA_CODE = "metadata";

  public CodedValue getAuthorInstitution() {
    return new CodedValue.CodedValueBuilder()
        .setCode(super.getAuthor().getId().getExtension())
        .setCodeSystem(super.getAuthor().getId().getRoot())
        //		       .setCodeSystemName()
        .setDisplayName(super.getAuthor().getOrganizationIdentity().getOrgName())
        .build();
  }

  public PersonIdentity getAuthorperson() {
    return super.getAuthor().getPersonIdentity();
  }

  public Date getCreationTime() {
    return super.getEffectiveTime();
  }

  public CodedValue getConfidentialityCodeCodedValue() { //CodedValue added to attribute/method name because the name was already used in super class returning a string 
    return confidentialityCodeCodedValue;
  }

  public void setConfidentialityCodeCodedValue(CodedValue confidentialityCodeCodedValue) {
    this.confidentialityCodeCodedValue = confidentialityCodeCodedValue;
  }

  public List<CodedValue> getEventCodeList() {
    return eventCodeList;
  }

  public void setEventCodeList(List<CodedValue> eventCodeList) {
    this.eventCodeList = eventCodeList;
  }

  public void addEventCode(CodedValue eventCode) {
    this.eventCodeList.add(eventCode);
  }

  public CodedValue getPatientId() {
    return new CodedValue.CodedValueBuilder()
        .setCode(super.getPatient().getId().getExtension())
        .setCodeSystem(super.getPatient().getId().getRoot())
        //		       .setCodeSystemName()
        //		       .setDisplayName()
        .build();
  }

  public CodedValue getSourcePatientId() {
    return new CodedValue.CodedValueBuilder()
        .setCode(super.getPatient().getId().getExtension())
        .setCodeSystem(super.getPatient().getId().getRoot())
        //		       .setCodeSystemName()
        //		       .setDisplayName()
        .build();
  }

  public CDAMetadata(ID id) {
    super(HL7.CDA_TYPEID_ROOT, HL7.CDA_TYPEID_EXTENSION, id, METADATA_CODE, METADATA_CODE,
        new String[] { METADATA_CODE }, HL7.REALM_CODE_DK);
    this.eventCodeList = new ArrayList<CodedValue>();
  }

  public CodedValue getFormatCode() {
    return formatCode;
  }

  public void setFormatCode(CodedValue formatCode) {
    this.formatCode = formatCode;
  }

}