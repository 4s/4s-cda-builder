package dk.s4.hl7.cda.model.pdc.v20;

import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.PersonIdentity;

/**
 * Citizen’s name and address
 */
public class NameAndAddressInformation {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;
  private PersonIdentity personIdentity;
  private ID confidentialId;
  private AddressData addressData;

  public static class NameAndAddressInformationBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;

    private PersonIdentity personIdentity;
    private ID confidentialId;
    private AddressData addressData;

    public NameAndAddressInformationBuilder(ID id) {
      this.id = id;
    }

    public NameAndAddressInformationBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public NameAndAddressInformationBuilder setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public NameAndAddressInformationBuilder setPersonIdentity(PersonIdentity personIdentity) {
      this.personIdentity = personIdentity;
      return this;
    }

    public NameAndAddressInformationBuilder setConfidentialId(ID confidentialId) {
      this.confidentialId = confidentialId;
      return this;
    }

    public NameAndAddressInformationBuilder setAddressData(AddressData addressData) {
      this.addressData = addressData;
      return this;
    }

    public NameAndAddressInformation build() {

      return new NameAndAddressInformation(this);
    }

  }

  private NameAndAddressInformation(NameAndAddressInformationBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.personIdentity = builder.personIdentity;
    this.confidentialId = builder.confidentialId;
    this.addressData = builder.addressData;

  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * @return PersonIdentity Returns the citizens name
   */
  public PersonIdentity getPersonIdentity() {
    return personIdentity;
  }

  /**
   * @return boolean Returns if the citizens address is confidential or not
   */
  public boolean isAddressConfidential() {
    return (confidentialId != null && confidentialId.getExtension().equalsIgnoreCase("ConfAddr"));
  }

  /**
   * The confidential id is present when the citizens address is confidential
   * 
   * @return ID Returns the confidentialId
   */
  public ID getConfidentialId() {
    return confidentialId;
  }

  /**
   * @return AddressData Returns the citizens address
   */
  public AddressData getAddressData() {
    return addressData;
  }

}
