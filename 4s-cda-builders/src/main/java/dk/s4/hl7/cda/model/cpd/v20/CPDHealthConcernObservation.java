package dk.s4.hl7.cda.model.cpd.v20;

import java.util.Date;

import dk.s4.hl7.cda.model.CdaDate;
import dk.s4.hl7.cda.model.CdaDateInterval;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.util.ModelUtil;

public class CPDHealthConcernObservation {

  private ID id;
  private CodedValue code;
  private CdaDate effectiveTime;
  private CdaDateInterval effectiveTimeInterval;
  private CodedValue valueCode;

  private CPDHealthConcernObservation(CPDHealthConcernObservationBuilder builder) {
    this.id = builder.id;
    this.code = builder.code;
    this.effectiveTime = builder.effectiveTime;
    this.effectiveTimeInterval = builder.effectiveTimeInterval;
    this.valueCode = builder.valueCode;
  }

  /**
   * Builder for constructing CPDHealthConcernObservation.
   * 
   */

  public static class CPDHealthConcernObservationBuilder {

    private ID id;
    private CodedValue code;
    private CdaDate effectiveTime;
    private CdaDateInterval effectiveTimeInterval;
    private CodedValue valueCode;

    /**
     * @param id the id of the observation
     */
    public CPDHealthConcernObservationBuilder setId(ID id) {
      this.id = id;
      return this;
    }

    /**
     * @param code the code describing the type of observation
     */
    public CPDHealthConcernObservationBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    /**
     * @param effectiveTime the time of the observation (date + time). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthConcernObservationBuilder setEffectiveTime(Date effectiveTime) {
      this.effectiveTime = effectiveTime == null ? null : new CdaDate(effectiveTime, true);
      return this;
    }

    /**
     * @param effectiveTime the time of the observation (date only). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthConcernObservationBuilder setEffectiveDate(Date effectiveTime) {
      this.effectiveTime = effectiveTime == null ? null : new CdaDate(effectiveTime, false);
      return this;
    }

    /**
     * @param effectiveTime the time of the observation. 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthConcernObservationBuilder setEffectiveTime(CdaDate effectiveTime) {
      this.effectiveTime = effectiveTime;
      return this;
    }

    /**
     * @param effectiveStartTime the start time interval of the concern (date + time). 
     * @param effectiveStopTime the end time interval of the concern (date + time). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthConcernObservationBuilder setEffectiveTimeInterval(Date effectiveStartTime,
        Date effectiveStopTime) {
      this.effectiveTimeInterval = new CdaDateInterval(effectiveStartTime, effectiveStopTime, true);
      return this;
    }

    /**
     * @param effectiveStartTime the start time interval of the concern (date only). 
     * @param effectiveStopTime the end time interval of the concern (date only). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthConcernObservationBuilder setEffectiveDateInterval(Date effectiveStartTime,
        Date effectiveStopTime) {
      this.effectiveTimeInterval = new CdaDateInterval(effectiveStartTime, effectiveStopTime, false);
      return this;
    }

    /**
     * @param effectiveTime the time interval of the observation. 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDHealthConcernObservationBuilder setEffectiveTimeInterval(CdaDateInterval effectiveTimeInterval) {
      this.effectiveTimeInterval = effectiveTimeInterval;
      return this;
    }

    /**
     * @param valueCode the value code of the observation
     */
    public CPDHealthConcernObservationBuilder setValueCode(CodedValue valueCode) {
      this.valueCode = valueCode;
      return this;
    }

    public CPDHealthConcernObservation build() {
      ModelUtil.checkNull(id, "id value is mandatory");
      ModelUtil.checkNull(code, "code value is mandatory");
      ModelUtil.checkNullAtLeastOne(effectiveTime, effectiveTimeInterval, "effectiveTime value is mandatory");
      if (effectiveTimeInterval != null) {
        ModelUtil.checkNullAtLeastOne(effectiveTimeInterval.getStartTime(), effectiveTimeInterval.getStopTime(),
            "effectiveStartTime value is mandatory when interval");
      }
      ModelUtil.checkNull(valueCode, "valueCode value is mandatory");

      return new CPDHealthConcernObservation(this);
    }

  }

  public ID getId() {
    return id;
  }

  public CodedValue getCode() {
    return code;
  }

  /**
   * @return the date and time of the observation. Use effectiveTimeHasTime to check if time part has to be considered
   */
  public Date getEffectiveTime() {
    return (effectiveTime == null) ? null : effectiveTime.getDate();
  }

  /**
   * @return if the time part of the effectiveTime has to be considered 
   */
  public boolean effectiveTimeHasTime() {
    return (effectiveTime == null) ? false : effectiveTime.dateHasTime();
  }

  /**
   * @return the start date and time of the observation as CdaDate format
   */
  public CdaDate getEffectiveTimeCdaDate() {
    return effectiveTime;
  }

  /**
   * @return the start date and time interval of the observation
   */
  public CdaDateInterval getEffectiveTimeInterval() {
    return effectiveTimeInterval;
  }

  public CodedValue getValueCode() {
    return valueCode;
  }

}
