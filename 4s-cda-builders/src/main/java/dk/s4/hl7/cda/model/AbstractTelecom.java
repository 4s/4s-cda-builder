package dk.s4.hl7.cda.model;

public abstract class AbstractTelecom<T> {

  private String value;
  private String protocol;
  private T use;

  /**
   * Telecom constructor.
   * 
   * @param use   The address use of the telecom.
   * @param value The telecom value.
   */
  public AbstractTelecom(T use, String value) {
    this.value = value;
    this.use = use;
  }

  /**
   * Telecom constructor.
   * 
   * @param use      The use of the telecom.
   * @param value    The telecom value.
   * @param protocol The protocol for the telecom - ie. mailto, tel etc
   */
  public AbstractTelecom(T use, String protocol, String value) {
    this.value = value;
    this.protocol = protocol;
    this.use = use;
  }

  /**
   * Returns the protocol of the telecom
   * 
   * @return the protocol
   */
  public String getProtocol() {
    return protocol;
  }

  /**
   * Get the telecom value.
   * 
   * @return the telecom value.
   */
  public String getValue() {
    return value;
  }

  /**
   * Get the use.
   * 
   * @return the use 
   */
  public T getUse() {
    return use;
  }

  public String toString() {
    return getUse() + "/" + getProtocol() + ":" + getValue();
  }

}
