package dk.s4.hl7.cda.model.apd;

public enum StatusV10 implements Status {
  /**
   * @since 1.0
   */
  ACTIVE,
  /**
   * @since 1.0
   */
  CANCELLED;
}
