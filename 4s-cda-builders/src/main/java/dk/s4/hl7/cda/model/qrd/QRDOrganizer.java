package dk.s4.hl7.cda.model.qrd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Organizer;

/**
 * Organizer used by QRD documents. Holds a list of Answers
 */
public class QRDOrganizer extends Organizer {
  private CodedValue codeValue;
  private String introduction;
  private List<ID> ids;
  private List<QRDResponse> qrdResponses;

  private QRDOrganizer(QRDOrganizerBuilder builder) {
    this.codeValue = builder.codeValue;
    this.introduction = builder.introduction;
    this.ids = builder.ids;
    this.qrdResponses = builder.qrdResponses;
  }

  /**
   * "Effective Java" Builder for constructing QRDOrganizer.
   */
  public static class QRDOrganizerBuilder {
    private CodedValue codeValue;
    private String introduction;
    private List<ID> ids;
    private List<QRDResponse> qrdResponses;

    public QRDOrganizerBuilder() {
      this.ids = new ArrayList<ID>();
      this.qrdResponses = new ArrayList<QRDResponse>();
    }

    public QRDOrganizerBuilder setCode(CodedValue codeValue) {
      this.codeValue = codeValue;
      return this;
    }

    public QRDOrganizerBuilder setIntroduction(String introduction) {
      this.introduction = introduction;
      return this;
    }

    public QRDOrganizerBuilder addId(ID id) {
      this.ids.add(id);
      return this;
    }

    public QRDOrganizerBuilder setIds(List<ID> ids) {
      this.ids = ids;
      return this;
    }

    public QRDOrganizerBuilder addQRDResponse(QRDResponse qrdResponse) {
      qrdResponses.add(qrdResponse);
      return this;
    }

    public QRDOrganizer build() {
      return new QRDOrganizer(this);
    }

  }

  public String getIntroduction() {
    return introduction;
  }

  public CodedValue getCode() {
    return codeValue;
  }

  public List<ID> getIds() {
    return ids;
  }

  public List<QRDResponse> getQRDResponses() {
    return qrdResponses;
  }

  @Override
  public String toString() {
    return "QRDOrganizer [codeValue=" + codeValue + ", introduction=" + introduction + ", ids=" + ids
        + ", qrdResponses=" + qrdResponses + "]";
  }

}
