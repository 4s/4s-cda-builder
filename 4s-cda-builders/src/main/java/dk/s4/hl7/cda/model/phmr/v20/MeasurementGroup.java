package dk.s4.hl7.cda.model.phmr.v20;

import dk.s4.hl7.cda.model.phmr.Measurement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * MeasurementGroup objects translate into an 'Organizer' section in the PHMR.
 * Each Organizer section can have multiple observations (measurements)
 *
 * example of usages:
 *     Date time = DateUtil.makeDanishDateTimeWithTimeZone(2016, 5, 9, 12, 10, 10);
 *     Measurement sat = NPU.createSaturation("0.97", time1, context, id);
 *     phmr.addVitalSignGroups(new MeasurementGroup(sat, time, Measurement.Status.COMPLETED));
 *
 */
public class MeasurementGroup {
  private List<Measurement> measurementList;
  private Date timestamp;
  private Measurement.Status status;

  /** Use this to create a organizer group for measurements.
   * Timestamp and status is mandatory at organizer level during build so don't forget to add them later.
   */
  public MeasurementGroup() {
    this.measurementList = new ArrayList<Measurement>();
  }

  /** Use this to create a organizer group for measurements.
   * Timestamp and status is mandatory at organizer level during build
   * @param timestamp the timestamp of the group of measurements
   * @param status the status of the group of measurements. Boolean stating whether measurements are completed.
   */
  public MeasurementGroup(Date timestamp, Measurement.Status status) {
    this.measurementList = new ArrayList<Measurement>();
    this.timestamp = timestamp;
    this.status = status;
  }

  /** Use this to create a organizer group for measurements.
   * Timestamp and status is mandatory at organizer level during build
   * @param measurement a measurement to include in the group. More can be added later.
   * @param timestamp the timestamp of the group of measurements
   * @param status the status of the group of measurements.  Boolean stating whether measurements are completed.
   */
  public MeasurementGroup(Measurement measurement, Date timestamp, Measurement.Status status) {
    this(timestamp, status);
    measurementList.add(measurement);
  }

  /** Get the list of measurement at the organizer level.
   * @return The list.
   */
  public List<Measurement> getMeasurementList() {
    return measurementList;
  }

  /** Set a list of measurement at the organizer level.
   * @param measurementList the list of measurements to add to the organizer level
   */
  public void setMeasurementList(List<Measurement> measurementList) {
    this.measurementList = measurementList;
  }

  /** Add one measurement to the organizer level.
   * @param measurement the measurement to add ot the list of measurements at organizer level
   */
  public void addMeasurement(Measurement measurement) {
    this.measurementList.add(measurement);
  }

  /** Get the timestamp at the organizer level.
   * @return The timestamp
   */
  public Date getTimestamp() {
    return timestamp;
  }

  /** Set the timestamp at the organizer level.
   * @param timestamp the timestamp to set at the organizer level
   */
  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  /** Get the status at the organizer level.
   * @return The status
   */
  public Measurement.Status getStatus() {
    return status;
  }

  /** Set the status at the organizer level.
   * @param status the status to set at the organizer level. Boolean stating whether measurements are completed.
   */
  public void setStatus(Measurement.Status status) {
    this.status = status;
  }
}
