package dk.s4.hl7.cda.model.cpd.v20;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.util.ModelUtil;

public class CPDHealthStatusComponent {

  private String title;
  private String text;
  private CodedValue code;
  private List<CPDHealthStatusActEntry> entryList;

  private CPDHealthStatusComponent(CPDHealthStatusComponentBuilder builder) {
    this.entryList = builder.entryList;
    this.title = builder.title;
    this.text = builder.text;
    this.code = builder.code;
  }

  public static class CPDHealthStatusComponentBuilder {

    private String title;
    private String text;
    private CodedValue code;
    private List<CPDHealthStatusActEntry> entryList;

    public CPDHealthStatusComponentBuilder() {
      this.entryList = new ArrayList<CPDHealthStatusActEntry>();
    }

    public CPDHealthStatusComponentBuilder setTitle(String title) {
      this.title = title;
      return this;
    }

    public CPDHealthStatusComponentBuilder setText(String text) {
      this.text = text;
      return this;
    }

    public CPDHealthStatusComponentBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public CPDHealthStatusComponentBuilder setEntryList(List<CPDHealthStatusActEntry> entryList) {
      this.entryList = entryList;
      return this;
    }

    public CPDHealthStatusComponentBuilder addEntry(CPDHealthStatusActEntry entry) {
      this.entryList.add(entry);
      return this;
    }

    public CPDHealthStatusComponent build() {
      ModelUtil.checkNull(code, "code value is mandatory");
      return new CPDHealthStatusComponent(this);
    }
  }

  public List<CPDHealthStatusActEntry> getEntryList() {
    return entryList;
  }

  public String getTitle() {
    return title;
  }

  public String getText() {
    return text;
  }

  public CodedValue getCode() {
    return code;
  }

}
