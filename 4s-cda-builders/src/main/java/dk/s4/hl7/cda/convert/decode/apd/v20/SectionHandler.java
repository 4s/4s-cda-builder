package dk.s4.hl7.cda.convert.decode.apd.v20;

import java.util.List;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.OrganizationHandler;
import dk.s4.hl7.cda.convert.decode.header.ParticipantHandler.ParticipantType;
import dk.s4.hl7.cda.convert.decode.header.v20.ParticipantHandler;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.AppointmentEncounterCode;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.AppointmentLocationTypeCode;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.Status;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlHandler;
import dk.s4.hl7.util.xml.XmlMapping;

public class SectionHandler implements CDAXmlHandler<AppointmentDocument> {

  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section";

  public static final String ENCOUNTER_SECTION = "/ClinicalDocument/component/structuredBody/component/section/entry/encounter";

  private String title;

  private AppointmentEncounterCode appointmentEncounterCode;

  protected Status status;

  private ID id;

  private CodedValue indicationCode;

  private RawTextHandler textHandler;

  private ParticipantHandler<AppointmentDocument> authorHandler;

  private ParticipantHandler<AppointmentDocument> performerHandler;

  private PreconditionHandler preconditionHandler;

  private OrganizationHandler locationHandler;

  private XmlHandler currentHandler;

  private AppointmentLocationTypeCode appointmentLocationTypeCode;

  public SectionHandler() {
    textHandler = new RawTextHandler(COMPONENT_SECTION + "/text", "text");
    authorHandler = new ParticipantHandler<AppointmentDocument>(ParticipantType.PARTICIPANT,
        ENCOUNTER_SECTION + "/author/assignedAuthor", ENCOUNTER_SECTION + "/author/time", "assignedPerson",
        "/representedOrganization");
    performerHandler = new ParticipantHandler<AppointmentDocument>(ParticipantType.PARTICIPANT,
        ENCOUNTER_SECTION + "/performer/assignedEntity", ENCOUNTER_SECTION + "/performer/time", "assignedPerson",
        "/representedOrganization");
    locationHandler = new OrganizationHandler(ENCOUNTER_SECTION + "/participant/participantRole",
        "/playingEntity/name");

    preconditionHandler = new PreconditionHandler();
    currentHandler = null;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "title")) {
      title = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code")) {
      if (!(ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "codeSystem")
          && xmlElement.getAttributeValue("codeSystem").equals(MedCom.MESSAGECODE_OID))) {
        indicationCode = new CodedValue.CodedValueBuilder()
            .setCode(xmlElement.getAttributeValue("code"))
            .setDisplayName(xmlElement.getAttributeValue("displayName"))
            .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
            .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
            .build();
      } else {
        appointmentEncounterCode = AppointmentEncounterCode.valueOf(xmlElement.getAttributeValue("code"));
        //make sure enum AppointmentEncounterCode values and MedCom.APD_CODE_?_ENCOUNTER value are the same for the above line to work
      }
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "statusCode", "code")) {
      status = Status.valueOf(xmlElement.getAttributeValue("code").toUpperCase());
      //      status = StatusImpl.valueOf(xmlElement.getAttributeValue("code").toUpperCase());

    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "root")) {
      id = new ID.IDBuilder()
          .setAuthorityName(xmlElement.getAttributeValue("assigningAuthorityName"))
          .setExtension(xmlElement.getAttributeValue("extension"))
          .setRoot(xmlElement.getAttributeValue("root"))
          .build();
      addHandlerToMap(xmlMapping, preconditionHandler);
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "extension")) { //backward compatibility
      id = new IDBuilder()
          .setRoot(MedCom.ROOT_OID)
          .setExtension(xmlElement.getAttributeValue("extension"))
          .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
          .build();
      addHandlerToMap(xmlMapping, preconditionHandler);

    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "displayName")) {
      indicationCode = new CodedValue.CodedValueBuilder()
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "participant", "typeCode")) {
      if (xmlElement.getAttributeValue("typeCode") != null) {
        String typeCode = xmlElement.getAttributeValue("typeCode");
        if ("LOC".equals(typeCode)) {
          appointmentLocationTypeCode = AppointmentLocationTypeCode.HEALTH_ORGANIZATION;
        } else if ("SBJ".equals(typeCode)) {
          appointmentLocationTypeCode = AppointmentLocationTypeCode.HOME;
        } else if ("DST".equals(typeCode)) {
          appointmentLocationTypeCode = AppointmentLocationTypeCode.NON_SOR;
        }
      }

    } else {

    }

  }

  private void addHandlerToMap(XmlMapping xmlMapping, XmlHandler xmlhandler) {
    xmlhandler.addHandlerToMap(xmlMapping);
    currentHandler = xmlhandler;
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase("encounter")) {
      currentHandler.removeHandlerFromMap(xmlMapping);
      currentHandler = null;
    }

  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    textHandler.addHandlerToMap(xmlMapping);
    authorHandler.addHandlerToMap(xmlMapping);
    performerHandler.addHandlerToMap(xmlMapping);
    locationHandler.addHandlerToMap(xmlMapping);

    xmlMapping.add(COMPONENT_SECTION, this);
    xmlMapping.add(COMPONENT_SECTION + "/title", this);
    xmlMapping.add(COMPONENT_SECTION + "/entry/encounter/code", this);
    xmlMapping.add(COMPONENT_SECTION + "/entry/encounter/statusCode", this);
    xmlMapping.add(COMPONENT_SECTION + "/entry/encounter/id", this);
    xmlMapping.add(COMPONENT_SECTION + "/entry/encounter/entryRelationship/observation/code", this);
    xmlMapping.add(COMPONENT_SECTION + "/entry/encounter/participant", this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    textHandler.removeHandlerFromMap(xmlMapping);
    authorHandler.removeHandlerFromMap(xmlMapping);
    performerHandler.removeHandlerFromMap(xmlMapping);
    locationHandler.removeHandlerFromMap(xmlMapping);
    xmlMapping.remove(COMPONENT_SECTION);
    xmlMapping.remove(COMPONENT_SECTION + "/title");
    xmlMapping.remove(COMPONENT_SECTION + "/entry/encounter/code");
    xmlMapping.remove(COMPONENT_SECTION + "/entry/encounter/statusCode");
    xmlMapping.remove(COMPONENT_SECTION + "/entry/encounter/id");
    xmlMapping.remove(COMPONENT_SECTION + "/entry/encounter/entryRelationship/observation/code");
    xmlMapping.remove(COMPONENT_SECTION + "/entry/encounter/participant");
  }

  @Override
  public void addDataToDocument(AppointmentDocument clinicalDocument) {
    clinicalDocument.setAppointmentTitle(title);
    clinicalDocument.setAppointmentId(id);
    clinicalDocument.setAppointmentStatus(status);
    clinicalDocument.setIndicationCode(indicationCode);

    clinicalDocument.setAppointmentText(textHandler.getRawText());
    clinicalDocument.setAppointmentAuthor(getFirstIfAny(authorHandler.getParticipants())); // Encounter author is optional
    clinicalDocument.setAppointmentPerformer(getFirstIfAny(performerHandler.getParticipants())); // Encounter performer is mandatory - but prefer successful parsing rather than fail here
    clinicalDocument.setAppointmentLocationTypeCode(appointmentLocationTypeCode);
    if (AppointmentLocationTypeCode.HOME.equals(appointmentLocationTypeCode)) {
      clinicalDocument.setAppointmentLocationHomeAddress(locationHandler.getOrganization());
    } else {
      clinicalDocument.setAppointmentLocation(locationHandler.getOrganization());
    }
    clinicalDocument.setAppointmentEncounterCode(appointmentEncounterCode);
    preconditionHandler.addDataToDocument(clinicalDocument);
  }

  /**
   * Pick first participant from list. Return null if no participants are present.
   */
  private static Participant getFirstIfAny(List<Participant> participants) {
    if (participants == null || participants.isEmpty()) {
      return null;
    }
    return participants.get(0);
  }
}
