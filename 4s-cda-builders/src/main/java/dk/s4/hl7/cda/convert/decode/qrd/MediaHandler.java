package dk.s4.hl7.cda.convert.decode.qrd;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.model.QuestionnaireMedia;
import dk.s4.hl7.cda.model.QuestionnaireMedia.QuestionnaireMediaBuilder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class MediaHandler extends BaseXmlHandler {
  public static final String MEDIA_BASE = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/entryRelationship/observationMedia";

  private String mediaType;
  private String representation;
  private String value;
  private String id;

  public MediaHandler() {
    addPath(MEDIA_BASE + "");
    addPath(MEDIA_BASE + "/value");
  }

  public MediaHandler(String mediaBaseXpath) {
    addPath(mediaBaseXpath + "");
    addPath(mediaBaseXpath + "/value");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "observationMedia", "ID")) {
      id = xmlElement.getAttributeValue("ID");
    }
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "representation")) {
      representation = xmlElement.getAttributeValue("representation");
    }
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "mediaType")) {
      mediaType = xmlElement.getAttributeValue("mediaType");
      if (mediaType.substring(0, 6).equals("image/")) {
        value = xmlElement.getElementValue();
      }
    }

  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignored
  }

  public QuestionnaireMedia getQuestionnaireMedia() {
    if (hasValues()) {
      return new QuestionnaireMediaBuilder()
          .representation(representation)
          .mediaType(mediaType)
          .value(value)
          .id(id)
          .build();
    }
    return null;
  }

  private boolean hasValues() {

    return mediaType != null;
  }

  public void clear() {

    mediaType = null;
    representation = null;
    value = null;
    id = null;
  }
}
