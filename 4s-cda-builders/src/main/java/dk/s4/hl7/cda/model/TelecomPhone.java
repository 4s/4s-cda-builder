package dk.s4.hl7.cda.model;

/** Data structure for a single telecom instance which allow for phone number types (Use).
 * 
 */
public final class TelecomPhone extends AbstractTelecom<TelecomPhone.Use> {

  /**
   * TelecomPhone data use enumeration.
   */
  public enum Use {
    /** Home number. */
    Home,
    /** Work number. */
    Work,
    /** Mobile number. */
    Mobile,
  }

  /** TelecomPhone constructor.
   * @param use The use of the telecom.
   * @param value The telecom value.
   * @param protocol The protocol for the telecom - ie. tel
   */
  public TelecomPhone(TelecomPhone.Use use, String protocol, String value) {
    super(use, protocol, value);
  }

}
