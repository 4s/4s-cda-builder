package dk.s4.hl7.cda.convert.encode.narrative.text;

import java.util.List;

public class NarrativeTextCreator {

  public static String createTextFromListBrSeparated(List<String> parts) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < parts.size(); i++) {
      sb.append(parts.get(i));
      if (i < parts.size() - 1) {
        sb.append("<br/>");
      }
    }
    return sb.toString();
  }

}
