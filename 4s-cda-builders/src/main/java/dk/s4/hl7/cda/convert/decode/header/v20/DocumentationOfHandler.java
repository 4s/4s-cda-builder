package dk.s4.hl7.cda.convert.decode.header.v20;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.core.v20.ClinicalDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class DocumentationOfHandler<C extends ClinicalDocument>
    extends dk.s4.hl7.cda.convert.decode.header.DocumentationOfHandler<C> implements CDAXmlHandler<C> {

  private static final Logger logger = LoggerFactory.getLogger(DocumentationOfHandler.class);
  private String version;

  private List<ID> episodeOfCareIdentifierList;
  private String codeEpisodeOfCareLabel;
  private String codeEpisodeOfCareLabelDisplayName;

  private String currentTemplateId;

  public DocumentationOfHandler() {
    this.episodeOfCareIdentifierList = new ArrayList<ID>();
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    super.handleElementStart(xmlMapping, xmlElement);

    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "serviceEvent")) {
      currentTemplateId = null;
    }

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      currentTemplateId = xmlElement.getAttributeValue("root");
      logger.debug("current currentTemplateId is: " + currentTemplateId);
    }

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "root", "extension")) {
      if (currentTemplateId != null && currentTemplateId.equals(MedCom.APD_VERSION_TEMPLATEID_ROOT)) {
        version = xmlElement.getAttributeValue("extension");
      }
      if (currentTemplateId != null && currentTemplateId.equals(MedCom.APD_EPISODE_OF_CARE_LABEL_TEMPLATEID_ROOT)) {
        episodeOfCareIdentifierList.add(new ID.IDBuilder()
            .setAuthorityName(xmlElement.getAttributeValue("assigningAuthorityName"))
            .setExtension(xmlElement.getAttributeValue("extension"))
            .setRoot(xmlElement.getAttributeValue("root"))
            .build());
      }
    }

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code")) {
      if (currentTemplateId != null && currentTemplateId.equals(MedCom.APD_EPISODE_OF_CARE_LABEL_TEMPLATEID_ROOT)) {
        codeEpisodeOfCareLabel = xmlElement.getAttributeValue("code");
      }
    }
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "displayName")) {
      if (currentTemplateId != null && currentTemplateId.equals(MedCom.APD_EPISODE_OF_CARE_LABEL_TEMPLATEID_ROOT)) {
        codeEpisodeOfCareLabelDisplayName = xmlElement.getAttributeValue("displayName");
      }
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);

    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent", this);
    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent/templateId", this);
    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent/id", this);
    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent/code", this);
  }

  @Override
  public void addDataToDocument(C clinicalDocument) {
    super.addDataToDocument(clinicalDocument);

    clinicalDocument.setCdaProfileAndVersion(version);
    clinicalDocument.setEpisodeOfCareIdentifierList(episodeOfCareIdentifierList);
    clinicalDocument.setEpisodeOfCareLabel(codeEpisodeOfCareLabel);
    clinicalDocument.setEpisodeOfCareLabelDisplayName(codeEpisodeOfCareLabelDisplayName);

  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);

    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/");
    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/code");
    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/templateId");
    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/id");
  }

}