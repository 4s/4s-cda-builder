package dk.s4.hl7.cda.model.cpd.v20;

import dk.s4.hl7.cda.model.util.ModelUtil;

/**
 * CPDSection is used by the CPDDocument (Careplan) document, to keep track of the different sections.
 * 
 * <p>When used, indicate the type of component it should handle </p>
 * <p>E.g. CPDSection&lt;CPDGoalObservationComponent&gt; cpdGoalObservationSection</p>
 */
public class CPDSection<C> {

  private String title;
  private String text;
  private C component;

  private CPDSection(CPDSectionBuilder<C> builder) {
    this.title = builder.title;
    this.text = builder.text;
    this.component = builder.component;
  }

  /**
   * Builder for constructing CPDSection.
   * 
   * <p>CPDSection is used by all the four section level templates for Careplan</p>
   * <p>When instantiated, indicate the type of component it should handle </p>
   * <p>E.g. CPDSectionBuilder&lt;CPDHealthStatusComponent&gt; CPDSectionBuilder = cpdHealthSectionBuilder new&lt;CPDHealthStatusComponent&gt;() </p>
   */
  public static class CPDSectionBuilder<C> {

    private String title;
    private String text;
    private C component;

    /**
     * @param title the title of the section. The value is mandatory.
     */
    public CPDSectionBuilder<C> setTitle(String title) {
      this.title = title;
      return this;
    }

    /**
     * @param text a free text description of the section.
     */
    public CPDSectionBuilder<C> setText(String text) {
      this.text = text;
      return this;
    }

    /**
     * @param component  the component of a given type the section should handle
     */
    public CPDSectionBuilder<C> setComponent(C component) {
      this.component = component;
      return this;
    }

    /**
     * validate and build CPDSection 
     */
    public CPDSection<C> build() {
      ModelUtil.checkNull(title, "title value is mandatory");
      // Text is mandatory, but encoded as empty tag when null
      return new CPDSection<C>(this);
    }
  }

  /**
   * @return the the component the section holds
   */
  public C getComponent() {
    return component;
  }

  /**
   * @return the title of the section
   */
  public String getTitle() {
    return title;
  }

  /**
   * @return the text of the section
   */
  public String getText() {
    return text;
  }

}
