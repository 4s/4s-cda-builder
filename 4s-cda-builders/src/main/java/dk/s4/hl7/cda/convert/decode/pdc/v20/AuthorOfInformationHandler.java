package dk.s4.hl7.cda.convert.decode.pdc.v20;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.pdc.v20.AuthorOfInformation;
import dk.s4.hl7.cda.model.pdc.v20.AuthorOfInformation.AuthorOfInformationBuilder;
import dk.s4.hl7.cda.model.pdc.v20.AuthorOfInformation.SourceOfInformation;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class AuthorOfInformationHandler extends BaseXmlHandler {

  private static final String AUTHOR_ELEMENT_NAME = "author";
  private ID id;
  private AuthorOfInformation authorOfInformation;
  private Date time;
  private String representedOrganizationName;
  private String templateId;
  private List<String> givenNames;
  private String familyName;

  private boolean isOrganization;

  public AuthorOfInformationHandler(String xpathToParentElement) {
    addPath(xpathToParentElement + "/" + AUTHOR_ELEMENT_NAME);
    addPath(xpathToParentElement + "/" + AUTHOR_ELEMENT_NAME + "/time");
    addPath(xpathToParentElement + "/" + AUTHOR_ELEMENT_NAME + "/templateId");
    addPath(xpathToParentElement + "/" + AUTHOR_ELEMENT_NAME + "/assignedAuthor/id");
    addPath(xpathToParentElement + "/" + AUTHOR_ELEMENT_NAME + "/assignedAuthor/representedOrganization");
    addPath(xpathToParentElement + "/" + AUTHOR_ELEMENT_NAME + "/assignedAuthor/representedOrganization/name");
    addPath(xpathToParentElement + "/" + AUTHOR_ELEMENT_NAME + "/assignedAuthor/assignedPerson");
    addPath(xpathToParentElement + "/" + AUTHOR_ELEMENT_NAME + "/assignedAuthor/assignedPerson/name/given");
    addPath(xpathToParentElement + "/" + AUTHOR_ELEMENT_NAME + "/assignedAuthor/assignedPerson/name/family");

    clear();
  }

  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "time", "value")) {
      time = ConvertXmlUtil.getDateFromyyyyMMddhhmmss(xmlElement.getAttributeValue("value"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      templateId = xmlElement.getAttributeValue("root");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "root", "extension")) {
      id = new ID.IDBuilder()
          .setAuthorityName(xmlElement.getAttributeValue("assigningAuthorityName"))
          .setExtension(xmlElement.getAttributeValue("extension"))
          .setRoot(xmlElement.getAttributeValue("root"))
          .build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "root")) {
      id = new ID.IDBuilder()
          .setAuthorityName(xmlElement.getAttributeValue("assigningAuthorityName"))
          //            .setExtension(xmlElement.getAttributeValue("extension"))
          .setRoot(xmlElement.getAttributeValue("root"))
          .build();
    } else if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "representedOrganization")) {
      isOrganization = true;
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "name")
        && isOrganization) {
      representedOrganizationName = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "given")) {
      givenNames.add(xmlElement.getElementValue());
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "family")) {
      familyName = xmlElement.getElementValue();
    }

  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "author")) {
      AuthorOfInformationBuilder authorOfInformationBuilder = new AuthorOfInformationBuilder(id)
          .setTime(time)
          .setRepresentedOrganizationName(representedOrganizationName);
      if (templateId.equals(MedCom.PDC_REGISTER_INFORMATION_ROOT)) {
        authorOfInformationBuilder.setSourceOfInformation(SourceOfInformation.FromRegister);
      } else if (templateId.equals(MedCom.PDC_MANUALLY_ENTERED_INFORMATION_ROOT)) {
        authorOfInformationBuilder.setSourceOfInformation(SourceOfInformation.ManuallyEntered);
      }
      if (familyName != null || givenNames.size() > 0) {
        PersonBuilder personBuilder = new PersonIdentity.PersonBuilder(familyName);
        for (String givenName : givenNames) {
          personBuilder.addGivenName(givenName);
        }
        authorOfInformationBuilder.setAssignedPerson(personBuilder.build());
      }

      authorOfInformation = authorOfInformationBuilder.build();

    } else if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "representedOrganization")) {
      isOrganization = false;

    }

  }

  public void clear() {
    id = null;
    time = null;
    representedOrganizationName = null;
    authorOfInformation = null;
    templateId = null;
    givenNames = new ArrayList<String>();
    familyName = null;
    isOrganization = false;

  }

}
