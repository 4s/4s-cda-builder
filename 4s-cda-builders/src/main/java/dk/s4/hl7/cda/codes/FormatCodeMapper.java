package dk.s4.hl7.cda.codes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.model.CodedValue;

public class FormatCodeMapper {

  private static final Logger logger = LoggerFactory.getLogger(FormatCodeMapper.class);

  private static int idIdx = 0;
  private static int codeIdx = 1;
  private static int systemIdx = 2;
  private static int displayNameIdx = 3;

  private static String[][] mapData = {
      { "phmr-v1.3", "urn:ad:dk:medcom:phmr-v1.3:full", "1.2.208.184.100.10", "PHMR DK schema" },
      { "qfdd-v1.1", "urn:ad:dk:medcom:qfdd-v1.1:full", "1.2.208.184.100.10", "QFDD DK schema" },
      { "qfdd-v1.2", "urn:ad:dk:medcom:qfdd-v1.2:full", "1.2.208.184.100.10", "QFDD DK schema" },
      { "qrd-v1.2", "urn:ad:dk:medcom:qrd-v1.2:full", "1.2.208.184.100.10", "QRD DK schema" },
      { "qrd-v1.3", "urn:ad:dk:medcom:qrd-v1.3:full", "1.2.208.184.100.10", "QRD DK schema" },
      { "cpd-v1.0.1", "urn:ad:dk:medcom:cpd-v1.0.1:full", "1.2.208.184.100.10", "CPD DK schema" },
      { "pdc-v2.0", "urn:ad:dk:medcom:pdc-v2.0:full", "1.2.208.184.100.10", "PDC DK schema" },
      { "pdc-v3.0", "urn:ad:dk:medcom:pdc-v3.0:full", "1.2.208.184.100.10", "PDC DK schema" },
      { "apd-v2.0", "urn:ad:dk:medcom:apd-v2.0:full", "1.2.208.184.100.10", "APD DK schema" },
      { "apd-v2.0.1", "urn:ad:dk:medcom:apd-v2.0.1:full", "1.2.208.184.100.10", "APD DK schema" } };

  public static CodedValue getFormatCode(String id) {
    for (int i = 0; i < mapData.length; i++) {
      if (mapData[i][idIdx].equals(id)) {
        CodedValue formatCode = new CodedValue.CodedValueBuilder()
            .setCode(mapData[i][codeIdx])
            .setCodeSystem(mapData[i][systemIdx])
            .setDisplayName(mapData[i][displayNameIdx])
            .build();
        logger.debug("Matching Format Code: " + formatCode.getCode());
        return formatCode;
      }
      logger.debug("No matching Format Code for id: " + id);
    }
    return null;
  }

}
