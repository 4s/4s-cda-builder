package dk.s4.hl7.cda.convert;

import dk.s4.hl7.cda.convert.base.AppendableSerializer;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.convert.base.ReaderSerializer;
import dk.s4.hl7.cda.model.phmr.v20.PHMRDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;

import java.io.Reader;

/**
 * The implementation of Codec to encode and decode PHMR from objects to XML
 * and XML to objects. After construction the codec is considered thread-safe.
 * 
 * @see <a href="https://www.dartlang.org/articles/libraries/converters-and-codecs">https://www.dartlang.org/articles/libraries/converters-and-codecs</a>
 */
public class PHMRV20XmlCodec
    implements Codec<PHMRDocument, String>, AppendableSerializer<PHMRDocument>, ReaderSerializer<PHMRDocument> {
  private PHMRV20XmlConverter phmrXmlConverter;
  private XmlPHMRV20Converter xmlPhmrConverter;

  public PHMRV20XmlCodec() {
    this(new XmlPrettyPrinter());
  }

  /**
   * Create PHMRXmlCodec
   *
   * @param xmlPrettyPrinter May be null
   */
  public PHMRV20XmlCodec(XmlPrettyPrinter xmlPrettyPrinter) {
    this.phmrXmlConverter = new PHMRV20XmlConverter(xmlPrettyPrinter);
    this.xmlPhmrConverter = new XmlPHMRV20Converter();
  }

  public String encode(PHMRDocument source) {
    return phmrXmlConverter.convert(source);
  }

  public PHMRDocument decode(String source) {
    return xmlPhmrConverter.convert(source);
  }

  @Override
  public void serialize(PHMRDocument source, Appendable appendable) {
    if (appendable == null) {
      throw new NullPointerException("Target appendable is null");
    }
    phmrXmlConverter.serialize(source, appendable);
  }

  @Override
  public PHMRDocument deserialize(Reader source) {
    return xmlPhmrConverter.deserialize(source);
  }
}
