package dk.s4.hl7.cda.model.cpd.v20;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.model.CdaDate;
import dk.s4.hl7.cda.model.CdaDateInterval;
import dk.s4.hl7.cda.model.CdaMeasurement;
import dk.s4.hl7.cda.model.CdaMeasurementInterval;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument.Status;
import dk.s4.hl7.cda.model.util.ModelUtil;

public class CPDOutcomeObservationEntry {
  private ID id;
  private CodedValue code;
  private Status statusCode;
  private CdaDate effectiveTime;
  private CdaDateInterval effectiveTimeInterval;
  private CdaMeasurement cdaMeasurement;
  private CdaMeasurementInterval cdaMeasurementInterval;
  private String comment;
  private List<Participant> authorList;

  private CPDOutcomeObservationEntry(CPDOutcomeObservationEntryBuilder builder) {
    this.id = builder.id;
    this.code = builder.code;
    this.statusCode = builder.statusCode;
    this.effectiveTime = builder.effectiveTime;
    this.effectiveTimeInterval = builder.effectiveTimeInterval;
    this.cdaMeasurement = builder.cdaMeasurement;
    this.cdaMeasurementInterval = builder.cdaMeasurementInterval;
    this.comment = builder.comment;
    this.authorList = builder.authorList;

  }

  /**
   * Builder for constructing CPDOutcomeObservationEntry.
   * 
   * <p> Some of the set methods states, that the value is not used during build/encoding of the XML document. </p>
   * <p> This is because the value is given by the Careplan standard, and that value overrules, whatever the builder is given. </p>
   * <p> Hence there is no check for null on those values. </p>
   */
  public static class CPDOutcomeObservationEntryBuilder {
    private ID id;
    private CodedValue code;
    private Status statusCode;
    private CdaDate effectiveTime;
    private CdaDateInterval effectiveTimeInterval;
    private CdaMeasurement cdaMeasurement;
    private CdaMeasurementInterval cdaMeasurementInterval;
    private String comment;
    private List<Participant> authorList;

    public CPDOutcomeObservationEntryBuilder() {
      this.authorList = new ArrayList<Participant>();
    }

    /**
     * @param id the unique id of the observation. The value is mandatory.
     */
    public CPDOutcomeObservationEntryBuilder setId(ID id) {
      this.id = id;
      return this;
    }

    /**
     * @param code the code of the outcome. The value is mandatory
     * 
     */
    public CPDOutcomeObservationEntryBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    /**
     * @param statusCode the status of the observation
     * 
     * <p>The value is not used during build/encoding of the XML document. The encoder uses it's own value which is completed.</p> 
     */
    public CPDOutcomeObservationEntryBuilder setStatusCode(Status statusCode) {
      this.statusCode = statusCode;
      return this;
    }

    /**
     * @param effectiveTime the time of the observation (date + time). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDOutcomeObservationEntryBuilder setEffectiveTime(Date effectiveTime) {
      this.effectiveTime = effectiveTime == null ? null : new CdaDate(effectiveTime, true);
      return this;
    }

    /**
     * @param effectiveTime the time of the observation (date only). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDOutcomeObservationEntryBuilder setEffectiveDate(Date effectiveTime) {
      this.effectiveTime = effectiveTime == null ? null : new CdaDate(effectiveTime, false);
      return this;
    }

    /**
     * @param effectiveTime the time of the observation. 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDOutcomeObservationEntryBuilder setEffectiveTime(CdaDate effectiveTime) {
      this.effectiveTime = effectiveTime;
      return this;
    }

    /**
     * @param effectiveStartTime the start time interval of the concern (date + time). 
     * @param effectiveStopTime the end time interval of the concern (date + time). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDOutcomeObservationEntryBuilder setEffectiveTimeInterval(Date effectiveStartTime, Date effectiveStopTime) {
      this.effectiveTimeInterval = new CdaDateInterval(effectiveStartTime, effectiveStopTime, true);
      return this;
    }

    /**
     * @param effectiveStartTime the start time interval of the concern (date only). 
     * @param effectiveStopTime the end time interval of the concern (date only). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDOutcomeObservationEntryBuilder setEffectiveDateInterval(Date effectiveStartTime, Date effectiveStopTime) {
      this.effectiveTimeInterval = new CdaDateInterval(effectiveStartTime, effectiveStopTime, false);
      return this;
    }

    /**
     * @param effectiveTime the time interval of the observation. 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDOutcomeObservationEntryBuilder setEffectiveTimeInterval(CdaDateInterval effectiveTimeInterval) {
      this.effectiveTimeInterval = effectiveTimeInterval;
      return this;
    }

    /**
     * @param cdaMeasurement the cdaMeasurement of the observation. Value and unit is mutually inclusive. Use either cdaMeasurement or cdaMeasurementInterval, not both.
     */
    public CPDOutcomeObservationEntryBuilder setCdaMeasurement(CdaMeasurement cdaMeasurement) {
      this.cdaMeasurement = cdaMeasurement;
      return this;
    }

    /**
     * @param cdaMeasurementInterval the cdaMeasurementInterval of the observation. Value and unit is mutually inclusive per cdaMeasurement. Use either cdaMeasurement or cdaMeasurementInterval, not both.
     */
    public CPDOutcomeObservationEntryBuilder setCdaMeasurementInterval(CdaMeasurementInterval cdaMeasurementInterval) {
      this.cdaMeasurementInterval = cdaMeasurementInterval;
      return this;
    }

    /**
     * @param comment the comment of the observation. 
     */
    public CPDOutcomeObservationEntryBuilder setComment(String comment) {
      this.comment = comment;
      return this;
    }

    /**
     * @param authorList the list of authors of the observation
     */
    public CPDOutcomeObservationEntryBuilder setAuthorList(List<Participant> authorList) {
      this.authorList = authorList;
      return this;
    }

    /**
     * @param author one author of the observation
     */
    public CPDOutcomeObservationEntryBuilder addAuthor(Participant author) {
      this.authorList.add(author);
      return this;
    }

    /**
     * validate and build CPDOutcomeObservationEntry 
     */
    public CPDOutcomeObservationEntry build() {
      ModelUtil.checkNull(id, "id value is mandatory");
      ModelUtil.checkNull(code, "code value is mandatory");
      ModelUtil.checkNullAtLeastOne(effectiveTime, effectiveTimeInterval, "effectiveTime value is mandatory");
      if (effectiveTimeInterval != null) {
        ModelUtil.checkNullAtLeastOne(effectiveTimeInterval.getStartTime(), effectiveTimeInterval.getStopTime(),
            "effectiveStartTime value is mandatory when interval");
      }
      if (cdaMeasurement != null) {
        ModelUtil.checkNullMutualDependent(cdaMeasurement.getValue(), cdaMeasurement.getUnit(),
            "Value and unit are mutual dependent");
      }
      return new CPDOutcomeObservationEntry(this);
    }

  }

  /**
   * @return the unique id of the observation
   */
  public ID getId() {
    return id;
  }

  /**
   * @return the code of the outcome
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return the status code of the observation
   */
  public Status getStatusCode() {
    return statusCode;
  }

  /**
   * @return the date and time of the observation. Use effectiveTimeHasTime to check if time part has to be considered
   */
  public Date getEffectiveTime() {
    return (effectiveTime == null) ? null : effectiveTime.getDate();
  }

  /**
   * @return if the time part of the effectiveTime has to be considered 
   */
  public boolean effectiveTimeHasTime() {
    return (effectiveTime == null) ? false : effectiveTime.dateHasTime();
  }

  /**
   * @return the start date and time of the observation as CdaDate format
   */
  public CdaDate getEffectiveTimeCdaDate() {
    return effectiveTime;
  }

  /**
   * @return the start date and time interval of the observation
   */
  public CdaDateInterval getEffectiveTimeInterval() {
    return effectiveTimeInterval;
  }

  /**
   * @return the measurement of the observation 
   */
  public CdaMeasurement getCdaMeasurement() {
    return cdaMeasurement;
  }

  /**
   * @return the measurementInterval of the observation 
   */
  public CdaMeasurementInterval getCdaMeasurementInterval() {
    return cdaMeasurementInterval;
  }

  /**
   * @return the comment of the observation 
   */
  public String getComment() {
    return comment;
  }

  public List<Participant> getAuthorList() {
    return authorList;
  }

}
