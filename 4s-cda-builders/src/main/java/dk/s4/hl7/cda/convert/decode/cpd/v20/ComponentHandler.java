package dk.s4.hl7.cda.convert.decode.cpd.v20;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.cpd.v20.SectionHandler.DocumentPart;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.CodedValueHandler;
import dk.s4.hl7.cda.convert.decode.header.ParticipantHandler;
import dk.s4.hl7.cda.convert.decode.header.ParticipantHandler.ParticipantType;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument;
import dk.s4.hl7.cda.model.cpd.v20.CPDGoalObservationComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDGoalObservationComponent.CPDGoalObservationComponentBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusComponent.CPDHealthStatusComponentBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDInterventionActComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDInterventionActComponent.CPDInterventionActComponentBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDOutcomeObservationComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDOutcomeObservationComponent.CPDOutcomeObservationComponentBuilder;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class ComponentHandler extends BaseXmlHandler {
  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section/component/section";

  private String title;
  private RawTextHandler rawTextHandler;
  private CodedValueHandler codedValueHandler;
  private EntryHandler entryHandler;
  private ParticipantHandler<CPDDocument> authorHandler;

  private DocumentPart currentDocumentPart;
  private CPDHealthStatusComponent cpdHealthStatusObservationComponent;
  private CPDGoalObservationComponent cpdGoalObservationComponent;
  private CPDInterventionActComponent cpdInterventionActComponent;
  private CPDOutcomeObservationComponent cpdOutcomeObservationComponent;

  public ComponentHandler() {
    rawTextHandler = new RawTextHandler(COMPONENT_SECTION + "/text", "text");
    codedValueHandler = new CodedValueHandler(COMPONENT_SECTION, false);
    entryHandler = new EntryHandler();
    authorHandler = new ParticipantHandler<CPDDocument>(ParticipantType.PARTICIPANT,
        COMPONENT_SECTION + "/author/assignedAuthor", COMPONENT_SECTION + "/author/time", "assignedPerson",
        "/representedOrganization");
  }

  public void clear() {
    clearLocal();
  }

  private void clearLocal() {
    title = null;
    rawTextHandler.clear();
    codedValueHandler.clear();
    entryHandler.clear();
    authorHandler.clear();
    currentDocumentPart = DocumentPart.None;

  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      this.setDocumentType(xmlElement);
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "title")) {
      title = xmlElement.getElementValue();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase("section")) {
      if (currentDocumentPart != null) {
        if (currentDocumentPart.equals(DocumentPart.HealthStatus)) {
          cpdHealthStatusObservationComponent = new CPDHealthStatusComponentBuilder()
              .setTitle(title)
              .setText(rawTextHandler.getRawText())

              .setCode(codedValueHandler.getCodedValue())
              .setEntryList(entryHandler.getCPDHealthStatusActEntryList())
              .build();
        } else if (currentDocumentPart.equals(DocumentPart.Goal)) {
          cpdGoalObservationComponent = new CPDGoalObservationComponentBuilder()
              .setTitle(title)
              .setText(rawTextHandler.getRawText())
              .setCode(codedValueHandler.getCodedValue())
              .setEntryList(entryHandler.getCPDGoalObservationEntryList())
              .build();
        } else if (currentDocumentPart.equals(DocumentPart.Intervention)) {
          cpdInterventionActComponent = new CPDInterventionActComponentBuilder()
              .setTitle(title)
              .setText(rawTextHandler.getRawText())

              .setCode(codedValueHandler.getCodedValue())
              .setEntryList(entryHandler.getCPDInterventionActEntryList())
              .setAuthorList(authorHandler.getParticipants())
              .build();
        } else if (currentDocumentPart.equals(DocumentPart.Outcome)) {
          cpdOutcomeObservationComponent = new CPDOutcomeObservationComponentBuilder()
              .setTitle(title)
              .setText(rawTextHandler.getRawText())

              .setEntryList(entryHandler.getCPDOutcomeObservationEntryList())
              .build();
        }
      }
      clearLocal();
    }
  }

  private void setDocumentType(XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "templateId")) {
      if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_HEALTH_CONCERN_SUB_OID)) {
        currentDocumentPart = DocumentPart.HealthStatus;
      } else if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_GOAL_SUB_OID)) {
        currentDocumentPart = DocumentPart.Goal;
      } else if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_INTERVENTION_SUB_OID)) {
        currentDocumentPart = DocumentPart.Intervention;
      } else

      if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_OUTCOME_SUB_OID)) {
        currentDocumentPart = DocumentPart.Outcome;
      } else {
        currentDocumentPart = DocumentPart.None;
      }
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add(COMPONENT_SECTION, this);
    xmlMapping.add(COMPONENT_SECTION + "/title", this);
    xmlMapping.add(COMPONENT_SECTION + "/templateId", this);

    rawTextHandler.addHandlerToMap(xmlMapping);
    codedValueHandler.addHandlerToMap(xmlMapping);
    entryHandler.addHandlerToMap(xmlMapping);
    authorHandler.addHandlerToMap(xmlMapping);

  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove(COMPONENT_SECTION);
    xmlMapping.remove(COMPONENT_SECTION + "/title");
    xmlMapping.remove(COMPONENT_SECTION + "/templateId");

    rawTextHandler.removeHandlerFromMap(xmlMapping);
    codedValueHandler.removeHandlerFromMap(xmlMapping);
    entryHandler.addHandlerToMap(xmlMapping);
    authorHandler.removeHandlerFromMap(xmlMapping);
  }

  public CPDHealthStatusComponent getCPDHealthStatusObservationComponent() {
    return cpdHealthStatusObservationComponent;
  }

  public CPDGoalObservationComponent getCPDGoalObservationComponent() {
    return cpdGoalObservationComponent;
  }

  public CPDInterventionActComponent getCPDInterventionActComponent() {
    return cpdInterventionActComponent;
  }

  public CPDOutcomeObservationComponent getCPDOutcomeObservationComponent() {
    return cpdOutcomeObservationComponent;
  }

}
