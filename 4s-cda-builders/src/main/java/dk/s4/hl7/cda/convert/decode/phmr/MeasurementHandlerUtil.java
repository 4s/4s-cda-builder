package dk.s4.hl7.cda.convert.decode.phmr;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.DataInputContext;

public class MeasurementHandlerUtil {

  public static DataInputContext.ProvisionMethod getProvisionCode(String code) {
    DataInputContext.ProvisionMethod measurementProvisionCode = null;
    if (code.equalsIgnoreCase(MedCom.TRANSFERRED_ELECTRONICALLY.toString())) {
      measurementProvisionCode = DataInputContext.ProvisionMethod.Electronically;
    } else if (code.equalsIgnoreCase(MedCom.TYPED_BY_CITIZEN.toString())) {
      measurementProvisionCode = DataInputContext.ProvisionMethod.TypedByCitizen;
    } else if (code.equalsIgnoreCase(MedCom.TYPED_BY_CITIZEN_RELATIVE.toString())) {
      measurementProvisionCode = DataInputContext.ProvisionMethod.TypedByCitizenRelative;
    } else if (code.equalsIgnoreCase(MedCom.TYPED_BY_HEALTHCAREPROFESSIONAL.toString())) {
      measurementProvisionCode = DataInputContext.ProvisionMethod.TypedByHealthcareProfessional;
    } else if (code.equalsIgnoreCase(MedCom.TYPED_BY_CAREGIVER.toString())) {
      measurementProvisionCode = DataInputContext.ProvisionMethod.TypedByCareGiver;
    }
    return measurementProvisionCode;
  }

  public static DataInputContext.PerformerType getPerformerType(String code) {
    // Setup the performerType
    DataInputContext.PerformerType performerType = null;
    if (code.equalsIgnoreCase(MedCom.PERFORMED_BY_CITIZEN.toString())) {
      performerType = DataInputContext.PerformerType.Citizen;
    } else if (code.equalsIgnoreCase(MedCom.PERFORMED_BY_HEALTHCAREPROFESSIONAL.toString())) {
      performerType = DataInputContext.PerformerType.HealthcareProfessional;
    } else if (code.equalsIgnoreCase(MedCom.PERFORMED_BY_CAREGIVER)) {
      performerType = DataInputContext.PerformerType.CareGiver;
    }
    return performerType;
  }

}
