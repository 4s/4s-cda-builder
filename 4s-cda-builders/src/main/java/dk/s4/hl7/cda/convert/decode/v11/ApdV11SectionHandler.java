package dk.s4.hl7.cda.convert.decode.v11;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.apd.SectionHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class ApdV11SectionHandler extends SectionHandler {

  public ApdV11SectionHandler() {
    super();
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "statusCode", "code")) {
      status = StatusV11Impl.valueOf(xmlElement.getAttributeValue("code").toUpperCase());
      return;
    }

    super.handleElementStart(xmlMapping, xmlElement);
  }
}
