package dk.s4.hl7.cda.model.pdc.v20;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;

/**
 * Indicates whether the Citizen is registered as an organ donor or not.
 */

public class OrganDonorRegistration {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;
  private boolean isRegistered;

  public static class OrganDonorRegistrationBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;
    private boolean isRegistered;

    public OrganDonorRegistrationBuilder(ID id) {
      this.id = id;
    }

    public OrganDonorRegistrationBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public OrganDonorRegistrationBuilder setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public OrganDonorRegistrationBuilder setRegisteret(boolean isRegistered) {
      this.isRegistered = isRegistered;
      return this;
    }

    public OrganDonorRegistration build() {

      return new OrganDonorRegistration(this);
    }

  }

  private OrganDonorRegistration(OrganDonorRegistrationBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.isRegistered = builder.isRegistered;

  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * @return boolean Returns if the citizen is organ donor or not
   */
  public boolean isRegistered() {
    return isRegistered;
  }

}
