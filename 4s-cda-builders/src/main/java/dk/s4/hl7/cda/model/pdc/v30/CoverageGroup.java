package dk.s4.hl7.cda.model.pdc.v30;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;

/**
 * Citizen’s coverage group returned from the CPR register.
 */
public class CoverageGroup {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;
  private String coverageGroup;

  public static class CoverageGroupBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;
    private String coverageGroup;

    public CoverageGroupBuilder(ID id) {
      this.id = id;
    }

    public CoverageGroupBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public CoverageGroupBuilder setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public CoverageGroupBuilder setCoverageGroup(String coverageGroup) {
      this.coverageGroup = coverageGroup;
      return this;
    }

    public CoverageGroup build() {

      return new CoverageGroup(this);
    }

  }

  private CoverageGroup(CoverageGroupBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.coverageGroup = builder.coverageGroup;

  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * @return String Returns coverage group as specified by "Sygesikringen"
   */
  public String getCoverageGroup() {
    return coverageGroup;
  }

}
