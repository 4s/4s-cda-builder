package dk.s4.hl7.cda.model.qfdd;

import dk.s4.hl7.cda.model.util.ModelUtil;

/**
 * From specification:
 * The Analog Slider Question Pattern Observation is used to ask a question from
 * the patient in the form of visual analogue scale (VAS). The Analog Slider
 * Question Pattern Observation is used to create an instance that carries the
 * information necessary to construct VAS.
 * 
 * The analog slider is defined by a minimum and maximum range. including the
 * increment.
 */
public class QFDDAnalogSliderQuestion extends QFDDQuestion {
  private String minimum; // head value
  private String maximum; // denominator
  private String increment;

  /**
   * "Effective Java" Builder for constructing QFDDAnalogSliderResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QFDDAnalogSliderQuestionBuilder
      extends QFDDQuestion.BaseQFDDQuestionBuilder<QFDDAnalogSliderQuestion, QFDDAnalogSliderQuestionBuilder> {

    private String minimum; // head value
    private String maximum; // denominator
    private String increment;

    public QFDDAnalogSliderQuestionBuilder setInterval(String minimum, String maximum, String increment) {
      this.minimum = minimum;
      this.maximum = maximum;
      this.increment = increment;
      return this;
    }

    @Override
    public QFDDAnalogSliderQuestion build() {
      ModelUtil.checkNull(minimum, "Minimum value is mandatory");
      ModelUtil.checkNull(maximum, "Maximum value is mandatory");
      ModelUtil.checkNull(increment, "Increment value is mandatory");
      return new QFDDAnalogSliderQuestion(this);
    }

    @Override
    public QFDDAnalogSliderQuestionBuilder getThis() {
      return this;
    }
  }

  private QFDDAnalogSliderQuestion(QFDDAnalogSliderQuestionBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    maximum = builder.maximum;
    increment = builder.increment;
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }

  public String getIncrement() {
    return increment;
  }

}
