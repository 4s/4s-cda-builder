package dk.s4.hl7.cda.codes;

import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.ID.IDBuilder;

// CHECKSTYLE:OFF
public class MedCom {

  public static final String ROOT_AUTHORITYNAME = "MedCom";
  public static final String ROOT_OID = "1.2.208.184";

  public static final String MESSAGECODE_DISPLAYNAME = "MedCom Message Codes";
  public static final String MEDICAL_DISPLAYNAME = "MedCom Instrument Codes";

  public static final String MESSAGECODE_OID = "1.2.208.184.100.1";
  public static final String DEVICE_OID = "1.2.208.184.100.2";
  public static final String MEDICAL_DEVICE_OID = "1.2.208.184.100.3";

  public static final String PHMR_ROOT_POD = "2.16.840.1.113883.10.20.9";
  public static final String DK_PHMR_ROOT_POD = "1.2.208.184.11.1";

  public static final String DK_APD_ROOT_OID = "1.2.208.184.14.1";

  public static final String DK_QRD_ROOT_OID = "1.2.208.184.13.1";
  public static final String DK_QRD_ROOT_OID_LEVEL = "1.2.208.184.13.1.1.1";

  public static final String DK_QFD_ROOT_OID = "1.2.208.184.12.1";
  public static final String DK_QFD_ROOT_OID_LEVEL = "1.2.208.184.12.1.1.1";

  public static final String DK_PDC_ROOT_OID = "1.2.208.184.16.1";

  public static final String DK_CPD_ROOT_OID = "1.2.208.184.15.1";

  // Reference
  public static final String DK_REFERENCE_ROOT_ID = "1.2.208.184.6.1";
  public static final String DK_REFERENCE_EXTERNAL_DOCUMENT = "1.2.208.184.5";
  public static final String DK_REFERENCE_EXTERNAL_URL = "1.2.208.184.5.3";
  // public static final String DK_REFERENCE_EXTERNAL_OBSERVATION =
  // "1.2.208.184.100.2";

  // Observation range (RED, YELLOW)
  public static final String DK_PHMR_OBSERVATION_RANGE_ROOT_OID = "1.2.208.184.11.1.2";
  public static final String DK_OBSERVATION_RANGE_RED_ALERT = "RAL";
  public static final String DK_OBSERVATION_RANGE_RED_ALERT_DISPLAYNAME = "Terapeutiske grænseværdier for RØD alarm";
  public static final String DK_OBSERVATION_RANGE_YELLOW_ALERT = "GAL";
  public static final String DK_OBSERVATION_RANGE_YELLOW_ALERT_DISPLAYNAME = "Terapeutiske grænseværdier for GUL alarm";

  // MedCom HL7 to identify the performer of a measurement
  public static final String PERFORMED_BY_CITIZEN = "POT";
  public static final String PERFORMED_BY_HEALTHCAREPROFESSIONAL = "PNT";
  public static final String PERFORMED_BY_CAREGIVER = "PCG";

  // Related displaynames
  public static final String PERFORMED_BY_CITIZEN_DISPLAYNAME = "Målt af borger";
  public static final String PERFORMED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME = "Målt af aut. sundhedsperson";
  public static final String PERFORMED_BY_CAREGIVER_DISPLAYNAME = "Målt af anden omsorgsperson";

  // MedCom codes to identify how data is provided to the data collection device
  public static final String TRANSFERRED_ELECTRONICALLY = "AUT";
  public static final String TYPED_BY_CITIZEN = "TPD";
  public static final String TYPED_BY_CITIZEN_RELATIVE = "TPR";
  public static final String TYPED_BY_HEALTHCAREPROFESSIONAL = "TPH";
  public static final String TYPED_BY_CAREGIVER = "TPC";

  // Related displaynames
  public static final String TRANSFERRED_ELECTRONICALLY_DISPLAYNAME = "Måling overført automatisk";
  public static final String TYPED_BY_CITIZEN_RELATIVE_DISPLAYNAME = "Indtastet af pårørende";
  public static final String TYPED_BY_CITIZEN_DISPLAYNAME = "Indtastet af borger";
  public static final String TYPED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME = "Indtastet af aut. sundhedsperson";
  public static final String TYPED_BY_CAREGIVER_DISPLAYNAME = "Indtastet af anden omsorgsperson";

  // Prompt table
  public static final String MEDCOM_PROMPT_OID = "1.2.208.184.100.2";
  public static final String MEDCOM_PROMPT_TABLE = "Medcom prompt table";

  public static ID createId(String Id) {
    return new IDBuilder().setRoot(ROOT_OID).setExtension(Id).setAuthorityName(ROOT_AUTHORITYNAME).build();
  }

  public static ID createURLId(String url) {
    return new IDBuilder()
        .setAuthorityName(ROOT_AUTHORITYNAME)
        .setExtension(url)
        .setRoot(DK_REFERENCE_EXTERNAL_URL)
        .build();
  }

  public static final String APD_VERSION_TEMPLATEID_ROOT = "1.2.208.184.10.1.10";
  public static final String VERSION_TEMPLATEID_ROOT = "1.2.208.184.10.1.10";
  //  public static final String APD_VERSION_TEMPLATEID_EXTENSION = "2019-09-10";
  public static final String VERSION_ID_ROOT = "1.2.208.184.100.10";

  public static final String APD_EFFECTIVE_TIME_ROOT = "1.2.208.184.10.1.11";

  public static final String APD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEM = "1.2.208.176.2.4";
  public static final String APD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEMNAME = "Sundhedsvæsenets Klassifikations System";
  public static final String APD_EPISODE_OF_CARE_LABEL_TEMPLATEID_ROOT = "1.2.208.184.10.1.12";

  public static final String APD_REPEATING_APPOINTMENT_ROOT = "1.2.208.184.14.11.4";
  public static final String APD_REPEATING_APPOINTMENT_EXTENSION = "2019-09-10";
  public static final String APD_CODE_REPEATING_APPOINTMENT_TYPE = "RepeatingDocumentType";

  public static final String APD_GUIDED_INTERVAL_ROOT = "1.2.208.184.14.11.5";
  public static final String APD_GUIDED_INTERVAL_EXTENSION = "2019-09-10";
  public static final String APD_CODE_GUIDED_INTERVAL_TYPE = "GuidedIntervalType";

  public static final String APD_CODE_MUNICIPALITY_APPOINTMENT_ENCOUNTER = "MunicipalityAppointment";
  public static final String APD_CODE_REGIONAL_APPOINTMENT_ENCOUNTER = "RegionalAppointment";
  public static final String APD_CODE_PRACTITIONER_APPOINTMENT_ENCOUNTER = "PractitionerAppointment";

  public static final String PARTICIPANT_TYPE_CODESYSTEM = "2.16.840.1.113883.5.90";
  public static final String PARTICIPANT_TYPE_CODESYSTEMNAME = "ParticipationType (HL7) Code System";
  public static final String PARTICIPANT_TYPE_CODE_REFB = "REFB";
  public static final String PARTICIPANT_TYPE_DISPLAYNAME_REFB = "ParticipationReferredBy";

  public static final String PDC_ENTRY_ROOT = "1.2.208.184";

  public static final String PDC_CUSTODY_INFORMATION_ROOT = "1.2.208.184.16.1.10.20.1.23";
  public static final String PDC_CUSTODY_INFORMATION_EXTENSION = "2019-08-14";
  public static final String PDC_NAME_AND_ADDRESS_INFORMATION_ROOT = "1.2.208.184.16.1.10.20.1.26";
  public static final String PDC_NAME_AND_ADDRESS_INFORMATION_EXTENSION = "2019-08-14";
  public static final String PDC_COVERAGE_GROUP_ROOT = "1.2.208.184.16.1.10.20.1.27";
  public static final String PDC_COVERAGE_GROUP_EXTENSION = "2019-08-14";
  public static final String PDC_ORGAN_DONOR_REGISTRATION_ROOT = "1.2.208.184.16.1.10.20.1.28";
  public static final String PDC_ORGAN_DONOR_REGISTRATION_EXTENSION = "2019-08-14";
  public static final String PDC_TREATMENT_WILL_REGISTRATION_ROOT = "1.2.208.184.16.1.10.20.1.29";
  public static final String PDC_TREATMENT_WILL_REGISTRATION_EXTENSION = "2019-08-14";
  public static final String PDC_LIVING_WILL_REGISTRATION_ROOT = "1.2.208.184.16.1.10.20.1.30";
  public static final String PDC_LIVING_WILL_REGISTRATION_EXTENSION = "2019-08-14";
  public static final String PDC_NORESUSCITATION_REGISTRATION_ROOT = "1.2.208.184.16.1.10.20.1.31";
  public static final String PDC_NORESUSCITATION_REGISTRATION_EXTENSION = "2023-07-01";
  public static final String PDC_MANUALLY_SPOKEN_LANGUAGE_ROOT = "1.2.208.184.16.1.10.20.1.20";
  public static final String PDC_MANUALLY_SPOKEN_LANGUAGE_EXTENSION = "2019-08-14";
  public static final String PDC_MANUALLY_ENTERED_TEMPORARY_ADDRESS_ROOT = "1.2.208.184.16.1.10.20.1.21";
  public static final String PDC_MANUALLY_ENTERED_TEMPORARY_ADDRESS_EXTENSION = "2019-08-14";
  public static final String PDC_MANUALLY_ENTERED_DENTIST_INFORMATION_ROOT = "1.2.208.184.16.1.10.20.1.22";
  public static final String PDC_MANUALLY_ENTERED_DENTIST_INFORMATION_EXTENSION = "2019-08-14";
  public static final String PDC_MANUALLY_ENTERED_CONTACT_INFORMATION_ROOT = "1.2.208.184.16.1.10.20.1.24";
  public static final String PDC_MANUALLY_ENTERED_CONTACT_INFORMATION_EXTENSION = "2019-08-14";
  public static final String PDC_MANUALLY_ENTERED_INFOMATION_ABOUT_RELATIVES_ROOT = "1.2.208.184.16.1.10.20.1.25";
  public static final String PDC_MANUALLY_ENTERED_INFOMATION_ABOUT_RELATIVES_EXTENSION = "2019-08-14";

  public static final String PDC_REGISTER_INFORMATION_ROOT = "1.2.208.184.16.1.10.20.31";
  public static final String PDC_MANUALLY_ENTERED_INFORMATION_ROOT = "1.2.208.184.16.1.10.20.30";

  public static final String PDC_NAME_AND_ADDRESS_VALUE_ROOT = "1.2.208.184.100.1";
  public static final String PDC_COVERAGE_GROUP_VALUE_ROOT = "1.2.208.176.2.7";
  public static final String PDC_ORGAN_DONOR_VALUE_ROOT = "1.2.208.176.1.10";
  public static final String PDC_TREATMENT_WILL_VALUE_ROOT = "1.2.208.176.1.9";
  public static final String PDC_LIVING_WILL_VALUE_ROOT = "1.2.208.176.1.8";
  public static final String PDC_NORESUSCITATION_VALUE_ROOT = "1.2.208.176.1.11";
  public static final String PDC_DENTIST_VALUE_ROOT = "1.2.208.176.1.4";
  public static final String PDC_CUSTODY_VALUE_ROOT = "1.2.208.176.1.2";

  public static final String CPD_VERSION_TEMPLATEID_ROOT = "1.2.208.184.10.1.10";
  public static final String CPD_EPISODE_OF_CARE_LABEL_TEMPLATEID_ROOT = "1.2.208.184.10.1.12";
  public static final String CPD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEM = "1.2.208.176.2.4";
  public static final String CPD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEMNAME = "Sundhedsvæsenets Klassifikations System";
}
