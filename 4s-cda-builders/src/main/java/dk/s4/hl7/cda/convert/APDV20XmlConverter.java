package dk.s4.hl7.cda.convert;

import java.io.IOException;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.codes.NSI;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.convert.encode.narrative.text.NarrativeTextConverter;
import dk.s4.hl7.cda.convert.encode.v20.ClinicalDocumentConverter;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.AppointmentEncounterCode;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.AppointmentLocationTypeCode;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.Status;
import dk.s4.hl7.cda.model.util.ModelUtil;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

/**
 * An implementation of the Converter that can build an XML Document object
 * following the Danish Appointment standard.
 */
public class APDV20XmlConverter extends ClinicalDocumentConverter<AppointmentDocument, Void> {

  public APDV20XmlConverter(XmlPrettyPrinter xmlPrettyPrinter) {
    super(xmlPrettyPrinter);
  }

  @Override
  public String convert(AppointmentDocument source) {
    StringBuilder builder = new StringBuilder(10000);
    serialize(source, builder);
    return builder.toString();
  }

  @Override
  protected void buildRootNode(XmlStreamBuilder xmlBuilder) throws IOException {
    // CONF-PHMR-1:
    xmlBuilder.addDefaultPI();
    xmlBuilder.addStyleSheet();
    xmlBuilder
        .element("ClinicalDocument")
        .attribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
        .attribute("xmlns", "urn:hl7-org:v3")
        .attribute("classCode", "DOCCLIN")
        .attribute("moodCode", "EVN")
        .attribute("xsi:schemaLocation",
            "urn:hl7-org:v3 http://svn.medcom.dk/svn/releases/Standarder/HL7/Generic/Schema/CDA_SDTC.xsd");
  }

  @Override
  protected void buildBody(AppointmentDocument appointment, XmlStreamBuilder xmlBuilder,
      NarrativeTextConverter<Void> narrativeTextConverter) throws IOException {
    this.buildStructuredBodySectionStart(xmlBuilder);
    xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIdWithExtension(xmlBuilder, HL7.APD_ID, HL7.APD_ID_EXTENSION_V2);

    BuildUtil.buildCode(Loinc.SECTION_APD_CODE, Loinc.OID, Loinc.SECTION_APD_DISPLAYNAME, Loinc.DISPLAYNAME,
        xmlBuilder);

    xmlBuilder.element("title").value(appointment.getAppointmentTitle()).elementEnd();
    xmlBuilder.element("text").valueNoEscaping(appointment.getAppointmentText()).elementEnd();

    xmlBuilder.element("entry");

    buildEncounter(appointment, xmlBuilder);

    xmlBuilder.elementEnd(); // end entry
    xmlBuilder.elementEnd(); // end section
    xmlBuilder.elementEnd(); // end component
    this.buildStructuredBodySectionEnd(xmlBuilder);
  }

  private void buildEncounter(AppointmentDocument appointment, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("encounter").attribute("moodCode", "APT").attribute("classCode", "ENC");
    BuildUtil.buildTemplateIdWithExtension(xmlBuilder, HL7.APD_ENCOUNTER_ID, HL7.APD_ID_EXTENSION_V2);
    BuildUtil.buildIdExtensionOptional(appointment.getAppointmentId(), xmlBuilder);

    BuildUtil.buildCode(getAppointmentEncounterCode(appointment), MedCom.MESSAGECODE_OID, null,
        MedCom.MESSAGECODE_DISPLAYNAME, xmlBuilder);

    xmlBuilder.element("statusCode").attribute("code", getStatusCode(appointment)).elementShortEnd();

    buildEffectiveTime(appointment, xmlBuilder);

    buildPerformer(appointment.getAppointmentPerformer(), xmlBuilder);
    buildAuthorSection(null, appointment.getAppointmentAuthor(), xmlBuilder);
    if (appointment.getAppointmentLocationHomeAddress() != null) {
      buildLocation(appointment.getAppointmentLocationHomeAddress(), appointment.getPatient(), xmlBuilder,
          AppointmentLocationTypeCode.HOME);
    } else {
      buildLocation(appointment.getAppointmentLocation(), appointment.getPatient(), xmlBuilder,
          appointment.getAppointmentLocationTypeCode());
    }

    xmlBuilder.element("entryRelationship").attribute("typeCode", "RSON");
    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");

    if (appointment.getIndicationCode() != null && appointment.getIndicationCode().getCode() != null
        && appointment.getIndicationCode().getCodeSystem() != null) {
      BuildUtil.buildCode(appointment.getIndicationCode(), xmlBuilder);
    } else {
      xmlBuilder
          .element("code")
          .attribute("code", "NI")
          .attribute("displayName", appointment.getIndicationDisplayName())
          .elementShortEnd();
    }

    xmlBuilder.elementEnd(); // end observation
    xmlBuilder.elementEnd(); // end entryRelationship

    if (appointment.isRepeatingDocument()) {
      buildPreconditionReapeting(appointment.getRepeatingDocumentGroupValue(), xmlBuilder);
    }

    if (appointment.isGuidedInterval()) {
      buildPreconditionGuidedInterval(appointment.getGuidedIntervalText(), xmlBuilder);
    }

    xmlBuilder.elementEnd(); // end encounter
  }

  protected String getStatusCode(AppointmentDocument a) {
    Status appointmentStatus = a.getAppointmentStatus();
    switch (appointmentStatus) {
    case ACTIVE:
      return "active";
    }
    throw new IllegalStateException("Invalid Appointment status" + appointmentStatus);
  }

  private String getAppointmentEncounterCode(AppointmentDocument a) {
    AppointmentEncounterCode appointmentEncounterCode = a.getAppointmentEncounterCode();

    switch (appointmentEncounterCode) {
    case MunicipalityAppointment:
      return MedCom.APD_CODE_MUNICIPALITY_APPOINTMENT_ENCOUNTER;
    case RegionalAppointment:
      return MedCom.APD_CODE_REGIONAL_APPOINTMENT_ENCOUNTER;
    case PractitionerAppointment:
      return MedCom.APD_CODE_PRACTITIONER_APPOINTMENT_ENCOUNTER;
    }
    throw new IllegalStateException("Invalid Appointment encounter code" + appointmentEncounterCode);
  }

  private void buildPerformer(Participant participant, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("performer").attribute("typeCode", "PRF");
    buildAssignedEntity(participant, xmlBuilder);
    xmlBuilder.elementEnd(); // end performer
  }

  private void buildLocation(OrganizationIdentity organizationIdentity, Patient patient, XmlStreamBuilder xmlBuilder,
      AppointmentLocationTypeCode appointmentLocationTypeCode) throws IOException {

    if (AppointmentLocationTypeCode.HOME.equals(appointmentLocationTypeCode)) {
      xmlBuilder.element("participant").attribute("typeCode", "SBJ");
    } else if (AppointmentLocationTypeCode.HEALTH_ORGANIZATION.equals(appointmentLocationTypeCode)) {
      xmlBuilder.element("participant").attribute("typeCode", "LOC");
    } else if (AppointmentLocationTypeCode.NON_SOR.equals(appointmentLocationTypeCode)) {
      xmlBuilder.element("participant").attribute("typeCode", "DST");
    } else {
      String typeCode = determineLocationTypeCode(organizationIdentity, patient);
      xmlBuilder.element("participant").attribute("typeCode", typeCode);
    }

    xmlBuilder.element("participantRole").attribute("classCode", "SDLOC");

    BuildUtil.buildTemplateIdWithExtension(xmlBuilder, HL7.APD_LOCATION_ID, HL7.APD_ID_EXTENSION_V2);
    buildSORId(organizationIdentity, xmlBuilder);
    buildAddress(organizationIdentity.getAddress(), true, xmlBuilder);
    buildTelecom(organizationIdentity.getTelecomList(), xmlBuilder);

    xmlBuilder.element("playingEntity").attribute("classCode", "PLC");

    xmlBuilder.element("name").value(organizationIdentity.getOrgName()).elementEnd();

    xmlBuilder.elementEnd(); // end playingEntity

    xmlBuilder.elementEnd(); // end participantRole
    xmlBuilder.elementEnd(); // end participant
  }

  private String determineLocationTypeCode(OrganizationIdentity organizationIdentity, Patient patient) {

    if (organizationIdentity.getId() != null && organizationIdentity.getId().getAuthorityName() != null
        && organizationIdentity.getId().getRoot() != null
        && organizationIdentity.getId().getAuthorityName().equals(NSI.SOR_AUTHORITYNAME)
        && organizationIdentity.getId().getRoot().equals(NSI.SOR_OID)) {
      return "LOC";
    } else {
      if (organizationIdentity.getAddress() != null
          && ModelUtil.addressDataAreEqual(organizationIdentity.getAddress(), patient.getAddress())
          && ModelUtil.telecomAreEqual(organizationIdentity.getTelecomList(), patient.getTelecomList())) {
        return "SBJ";
      } else {
        return "DST";
      }
    }
  }

  @Override
  protected void buildDocumentationOf(AppointmentDocument source, XmlStreamBuilder xmlBuilder) throws IOException {

    xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
    xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIds(xmlBuilder, MedCom.APD_EFFECTIVE_TIME_ROOT);

    buildEffectiveTime(source, xmlBuilder);

    xmlBuilder.elementEnd(); // end serviceEvent
    xmlBuilder.elementEnd(); // end documentationOf

    buildAppointmentVersion(source, xmlBuilder);

    if (source.getEpisodeOfCareLabel() != null) {
      buildEpisodeOfCareLabel(source, xmlBuilder);
    }

  }

  @Override
  protected void buildHeaderTemplateId(String[] templateIds, XmlStreamBuilder xmlBuilder) throws IOException {
    BuildUtil.buildTemplateIdsWithExtension(xmlBuilder, HL7.APD_ID_EXTENSION_V2, templateIds);
  }

  protected void buildEffectiveTime(AppointmentDocument source, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("effectiveTime");

    if (source.getServiceStartTime() != null) {
      xmlBuilder
          .element("low")
          .attribute("value", dateTimeformatter.format(source.getServiceStartTime()))
          .elementShortEnd();
    }
    if (source.getServiceStopTime() != null) {
      xmlBuilder
          .element("high")
          .attribute("value", dateTimeformatter.format(source.getServiceStopTime()))
          .elementShortEnd();
    } else {
      BuildUtil.buildNullFlavor("high", xmlBuilder);
    }
    xmlBuilder.elementEnd(); // end effectiveTime
  }

  private void buildAppointmentVersion(AppointmentDocument source, XmlStreamBuilder xmlBuilder) throws IOException {

    xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
    xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIds(xmlBuilder, MedCom.APD_VERSION_TEMPLATEID_ROOT);

    BuildUtil.buildId(MedCom.VERSION_ID_ROOT, MedCom.ROOT_AUTHORITYNAME, source.getCdaProfileAndVersion(), xmlBuilder);

    xmlBuilder.elementEnd(); // end serviceEvent
    xmlBuilder.elementEnd(); // end documentationOf

  }

  private void buildEpisodeOfCareLabel(AppointmentDocument source, XmlStreamBuilder xmlBuilder) throws IOException {

    xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
    xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIds(xmlBuilder, MedCom.APD_EPISODE_OF_CARE_LABEL_TEMPLATEID_ROOT);

    for (ID id : source.getEpisodeOfCareIdentifierList()) {
      if (id != null) {
        BuildUtil.buildId(id, xmlBuilder);
      }
    }
    CodedValue episodeOfCareLabelCode;

    if (source.getEpisodeOfCareLabelDisplayName() != null) {
      episodeOfCareLabelCode = new CodedValue(source.getEpisodeOfCareLabel(),
          MedCom.APD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEM, source.getEpisodeOfCareLabelDisplayName(),
          MedCom.APD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEMNAME);
    } else {
      episodeOfCareLabelCode = new CodedValue(source.getEpisodeOfCareLabel(),
          MedCom.APD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEM, MedCom.APD_EPISODE_OF_CARE_LABEL_CODE_CODESYSTEMNAME);
    }

    BuildUtil.buildCode(episodeOfCareLabelCode, xmlBuilder);
    xmlBuilder.elementEnd(); // end serviceEvent
    xmlBuilder.elementEnd(); // end documentationOf

  }

  private void buildPreconditionReapeting(String value, XmlStreamBuilder xmlBuilder) throws IOException {

    xmlBuilder.element("precondition");
    BuildUtil.buildTemplateIdWithExtension(xmlBuilder, MedCom.APD_REPEATING_APPOINTMENT_ROOT,
        MedCom.APD_REPEATING_APPOINTMENT_EXTENSION);
    xmlBuilder.element("criterion");

    BuildUtil.buildCode(MedCom.APD_CODE_REPEATING_APPOINTMENT_TYPE, MedCom.MESSAGECODE_OID, null,
        MedCom.MESSAGECODE_DISPLAYNAME, xmlBuilder);
    buildValue(value, xmlBuilder);

    xmlBuilder.elementEnd(); // end criterion
    xmlBuilder.elementEnd(); // end precondition

  }

  private void buildValue(String value, XmlStreamBuilder xmlBuilder) throws IOException {
    if (value != null) {
      xmlBuilder
          .element("value")
          .attribute("root", MedCom.ROOT_OID)
          .attribute("extension", value)
          .attribute("assigningAuthorityName", MedCom.ROOT_AUTHORITYNAME)
          .attribute("xsi:type", "II")
          .elementShortEnd();
      ;
    }
  }

  private void buildPreconditionGuidedInterval(String text, XmlStreamBuilder xmlBuilder) throws IOException {

    xmlBuilder.element("precondition");
    BuildUtil.buildTemplateIdWithExtension(xmlBuilder, MedCom.APD_GUIDED_INTERVAL_ROOT,
        MedCom.APD_GUIDED_INTERVAL_EXTENSION);
    xmlBuilder.element("criterion");

    BuildUtil.buildCode(MedCom.APD_CODE_GUIDED_INTERVAL_TYPE, MedCom.MESSAGECODE_OID, null,
        MedCom.MESSAGECODE_DISPLAYNAME, xmlBuilder);
    buildText(text, xmlBuilder);

    xmlBuilder.elementEnd(); // end criterion
    xmlBuilder.elementEnd(); // end precondition

  }

  private void buildText(String text, XmlStreamBuilder xmlBuilder) throws IOException {
    if (text != null) {
      xmlBuilder.element("text").value(text).elementEnd();
    }
  }

}
