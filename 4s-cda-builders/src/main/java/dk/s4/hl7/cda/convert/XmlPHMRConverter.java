package dk.s4.hl7.cda.convert;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ClinicalDocumentXmlConverter;
import dk.s4.hl7.cda.convert.decode.header.CDAHeaderHandler;
import dk.s4.hl7.cda.convert.decode.header.CustodianHandler;
import dk.s4.hl7.cda.convert.decode.header.DocumentationOfHandler;
import dk.s4.hl7.cda.convert.decode.header.PatientHandler;
import dk.s4.hl7.cda.convert.decode.phmr.SectionHandler;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;

/**
 * Parse PHMR xml to GreenCDA model
 */
public class XmlPHMRConverter extends ClinicalDocumentXmlConverter<PHMRDocument> {
  public PHMRDocument convert(String source) {
    return deserialize(new StringReader(source));
  }

  @Override
  protected List<CDAXmlHandler<PHMRDocument>> createParticipantHandlers() {
    List<CDAXmlHandler<PHMRDocument>> list = new ArrayList<CDAXmlHandler<PHMRDocument>>();
    list.add(new PatientHandler<PHMRDocument>());
    list.add(createAuthorHandler());
    list.add(createLegalAuthenticatorHandler());
    list.add(new CustodianHandler<PHMRDocument>());
    list.add(new DocumentationOfHandler<PHMRDocument>());
    return list;
  }

  @Override
  protected PHMRDocument createNewDocument(CDAHeaderHandler<PHMRDocument> headerHandler) {
    return new PHMRDocument(headerHandler.getDocumentId());
  }

  @Override
  protected List<CDAXmlHandler<PHMRDocument>> createSectionHandlers() {
    List<CDAXmlHandler<PHMRDocument>> list = new ArrayList<CDAXmlHandler<PHMRDocument>>();
    list.add(new SectionHandler());
    return list;
  }
}
