package dk.s4.hl7.cda.convert;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.v20.ClinicalDocumentXmlConverter;
import dk.s4.hl7.cda.convert.decode.cpd.v20.SectionHandler;
import dk.s4.hl7.cda.convert.decode.header.CDAHeaderHandler;
import dk.s4.hl7.cda.convert.decode.header.CustodianHandler;
import dk.s4.hl7.cda.convert.decode.header.v20.DocumentationOfHandler;
import dk.s4.hl7.cda.convert.decode.header.v20.PatientHandler;
import dk.s4.hl7.cda.convert.decode.header.v20.RelatedDocumentHandler;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument;

/**
 * Parse CPD xml to GreenCDA model
 */
public class XmlCPDV20Converter extends ClinicalDocumentXmlConverter<CPDDocument> {

  @Override
  public CPDDocument convert(String source) {
    return deserialize(new StringReader(source));
  }

  @Override
  protected List<CDAXmlHandler<CPDDocument>> createParticipantHandlers() {
    List<CDAXmlHandler<CPDDocument>> list = new ArrayList<CDAXmlHandler<CPDDocument>>();
    list.add(new PatientHandler<CPDDocument>());
    list.add(createAuthorHandler());
    list.add(new CustodianHandler<CPDDocument>());
    list.add(new DocumentationOfHandler<CPDDocument>());
    list.add(new RelatedDocumentHandler<CPDDocument>());
    return list;
  }

  @Override
  protected List<CDAXmlHandler<CPDDocument>> createSectionHandlers() {
    List<CDAXmlHandler<CPDDocument>> list = new ArrayList<CDAXmlHandler<CPDDocument>>();
    list.add(new SectionHandler());
    return list;
  }

  @Override
  protected CPDDocument createNewDocument(CDAHeaderHandler<CPDDocument> headerHandler) {
    return new CPDDocument(headerHandler.getDocumentId());
  }

}
