package dk.s4.hl7.cda.model.qrd;

import dk.s4.hl7.cda.model.util.ModelUtil;

/**
 * Represents additional text for helping the user answer the question
 */
public class QRDHelpText {
  private String language;
  private final String helpText;

  private QRDHelpText(QRDHelpTextBuilder builder) {
    this.language = builder.language;
    this.helpText = builder.helpText;
  }

  public static class QRDHelpTextBuilder {
    private String language;
    private String helpText;

    public QRDHelpTextBuilder language(String language) {
      this.language = language;
      return this;
    }

    public QRDHelpTextBuilder helpText(String helpText) {
      this.helpText = helpText;
      return this;
    }

    public QRDHelpText build() {
      ModelUtil.checkNull(helpText, "Help text is mandatory");
      return new QRDHelpText(this);
    }
  }

  public String getLanguage() {
    return language;
  }

  public String getHelpText() {
    return helpText;
  }
}
