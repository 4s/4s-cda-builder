package dk.s4.hl7.cda.convert.encode.narrative.text;

/**
 * The NarrativeTextConverter is used, when changes are needed to Narrative Text during xml encoding.
 * 
 * When setting the Codec for the document, include a NarrativeTextConverter. Then "selected" text variables will respond to this during encoding
 * An example is QRDXmlConverter  method buildQRDSections where xmlBuilder.element("text") is set using the Narrative Text converter. 
 * 
 */

public interface NarrativeTextConverter<T> {

  public String createText(T documentPart);

}
