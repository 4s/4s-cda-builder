package dk.s4.hl7.cda.model.core.v20;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.RelatedDocument;

public abstract class BaseClinicalDocument extends dk.s4.hl7.cda.model.core.BaseClinicalDocument
    implements ClinicalDocument {

  // documentationOf (0..n) // 
  protected String cdaProfileAndVersion;

  protected String episodeOfCareLabel;
  protected String episodeOfCareLabelDisplayName;
  protected List<ID> episodeOfCareIdentifierList = null;

  protected OrganizationIdentity providerOrganization = null;

  protected Participant authorReferrer;

  protected RelatedDocument relatedDocument;

  public BaseClinicalDocument(String typeIdRoot, String typeIdExtension, ID id, String codeCode,
      String codeDisplayName) {
    this(typeIdRoot, typeIdExtension, id, codeCode, codeDisplayName, null, null);
  }

  public BaseClinicalDocument(String typeIdRoot, String typeIdExtension, ID id, String codeCode, String codeDisplayName,
      String[] templateIds) {
    this(typeIdRoot, typeIdExtension, id, codeCode, codeDisplayName, templateIds, null);
  }

  public BaseClinicalDocument(String typeIdRoot, String typeIdExtension, ID id, String code, String codeDisplayName,
      String[] templateIds, String realmCode) {
    super(typeIdRoot, typeIdExtension, id, code, codeDisplayName, templateIds, realmCode);
    this.episodeOfCareIdentifierList = new ArrayList<ID>();
  }

  @Override
  public String getCdaProfileAndVersion() {
    return cdaProfileAndVersion;
  }

  @Override
  public void setCdaProfileAndVersion(String cdaProfileAndVersion) {
    this.cdaProfileAndVersion = cdaProfileAndVersion;
  }

  @Override
  public String getEpisodeOfCareLabel() {
    return this.episodeOfCareLabel;
  }

  @Override
  public void setEpisodeOfCareLabel(String episodeOfCareLabel) {
    this.episodeOfCareLabel = episodeOfCareLabel;
  }

  @Override
  public String getEpisodeOfCareLabelDisplayName() {
    return this.episodeOfCareLabelDisplayName;
  }

  @Override
  public void setEpisodeOfCareLabelDisplayName(String episodeOfCareLabelDisplayName) {
    this.episodeOfCareLabelDisplayName = episodeOfCareLabelDisplayName;
  }

  @Override
  public List<ID> getEpisodeOfCareIdentifierList() {
    return episodeOfCareIdentifierList;
  }

  @Override
  public void setEpisodeOfCareIdentifierList(List<ID> episodeOfCareIdentifierList) {
    this.episodeOfCareIdentifierList = episodeOfCareIdentifierList;
  }

  @Override
  public void addEpisodeOfCareIdentifier(ID episodeOfCareIdentifier) {
    this.episodeOfCareIdentifierList.add(episodeOfCareIdentifier);
  }

  @Override
  public void setProviderOrganization(OrganizationIdentity providerOrganization) {
    this.providerOrganization = providerOrganization;
  }

  @Override
  public OrganizationIdentity getProviderOrganization() {
    return providerOrganization;
  }

  @Override
  public void setAuthorReferrer(Participant authorReferrer) {
    this.authorReferrer = authorReferrer;
  }

  @Override
  public Participant getAuthorReferrer() {
    return authorReferrer;
  }

  @Override
  public void setRelateDocument(RelatedDocument relatedDocument) {
    this.relatedDocument = relatedDocument;
  }

  @Override
  public RelatedDocument getRelateDocument() {
    return relatedDocument;
  }

}