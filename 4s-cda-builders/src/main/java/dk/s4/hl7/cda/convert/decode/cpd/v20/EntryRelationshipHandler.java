package dk.s4.hl7.cda.convert.decode.cpd.v20;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.ValueHandler;
import dk.s4.hl7.cda.model.CdaDate;
import dk.s4.hl7.cda.model.CdaDateInterval;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthConcernObservation;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthConcernObservation.CPDHealthConcernObservationBuilder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

/**
 * EntryRelationsship handler. 
 */
public class EntryRelationshipHandler extends BaseXmlHandler {

  private static final String CODE_ELEMENT_NAME = "entryRelationship/observation";
  private List<CPDHealthConcernObservation> cpdHealthConcernObservationList;
  private ID id;
  private CodedValue code;
  private CdaDate effectiveTime;
  private CdaDateInterval effectiveTimeInterval;
  private ValueHandler valueHandlerObservation;

  public EntryRelationshipHandler(String xpathToParentElement) {
    addPath(xpathToParentElement + "/" + CODE_ELEMENT_NAME);
    addPath(xpathToParentElement + "/" + CODE_ELEMENT_NAME + "/id");
    addPath(xpathToParentElement + "/" + CODE_ELEMENT_NAME + "/code");
    addPath(xpathToParentElement + "/" + CODE_ELEMENT_NAME + "/effectiveTime");
    addPath(xpathToParentElement + "/" + CODE_ELEMENT_NAME + "/effectiveTime/low");
    addPath(xpathToParentElement + "/" + CODE_ELEMENT_NAME + "/effectiveTime/high");
    valueHandlerObservation = new ValueHandler(xpathToParentElement + "/" + CODE_ELEMENT_NAME, false);
    this.clear();
  }

  public List<CPDHealthConcernObservation> getObservationList() {
    return this.cpdHealthConcernObservationList;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "root", "extension")) {
      id = new IDBuilder()
          .setRoot(xmlElement.getAttributeValue("root"))
          .setExtension(xmlElement.getAttributeValue("extension"))
          .build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "root")) {
      id = new IDBuilder().setRoot(xmlElement.getAttributeValue("root")).build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code", "codeSystem",
        "displayName", "codeSystemName")) {
      code = new CodedValue.CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "effectiveTime", "value")) {
      this.effectiveTime = new CdaDate(xmlElement.getAttributeValue("value"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "low", "value")) {
      checkResetEffectiveTimeInterval();
      effectiveTimeInterval.setStartTime(new CdaDate(xmlElement.getAttributeValue("value")));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "high", "value")) {
      checkResetEffectiveTimeInterval();
      effectiveTimeInterval.setStopTime(new CdaDate(xmlElement.getAttributeValue("value")));
    }

  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase("observation")) {
      cpdHealthConcernObservationList.add(new CPDHealthConcernObservationBuilder()
          .setId(id)
          .setCode(code)
          .setEffectiveTime(effectiveTime)
          .setEffectiveTimeInterval(effectiveTimeInterval)
          .setValueCode(valueHandlerObservation.getCode())
          .build());
      this.localClear();
    }
  }

  public void clear() {
    this.cpdHealthConcernObservationList = new ArrayList<CPDHealthConcernObservation>();
    this.localClear();
  }

  public void localClear() {
    id = null;
    code = null;
    effectiveTime = null;
    effectiveTimeInterval = null;
    valueHandlerObservation.clear();
  }

  private void checkResetEffectiveTimeInterval() {
    if (effectiveTimeInterval == null) {
      effectiveTimeInterval = new CdaDateInterval();
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    valueHandlerObservation.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    valueHandlerObservation.removeHandlerFromMap(xmlMapping);
  }

}
