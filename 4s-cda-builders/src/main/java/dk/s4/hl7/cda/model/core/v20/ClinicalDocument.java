package dk.s4.hl7.cda.model.core.v20;

import java.util.List;

import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.RelatedDocument;

/**
 * Base interface for all GreenCDA documents version 2.0.
 **/
public interface ClinicalDocument extends dk.s4.hl7.cda.model.core.ClinicalDocument {
  public String getCdaProfileAndVersion();

  public void setCdaProfileAndVersion(String cdaProfileAndVersion);

  public String getEpisodeOfCareLabel();

  public void setEpisodeOfCareLabel(String EpisodeOfCareLabel);

  public String getEpisodeOfCareLabelDisplayName();

  public void setEpisodeOfCareLabelDisplayName(String EpisodeOfCareLabelDisplayName);

  public List<ID> getEpisodeOfCareIdentifierList();

  public void setEpisodeOfCareIdentifierList(List<ID> episodeOfCareIdentifierList);

  public void addEpisodeOfCareIdentifier(ID episodeOfCareIdentifier);

  public void setProviderOrganization(OrganizationIdentity providerOrganization);

  public OrganizationIdentity getProviderOrganization();

  public void setAuthorReferrer(Participant authorReferrer);

  public Participant getAuthorReferrer();

  public void setRelateDocument(RelatedDocument relatedDocument);

  public RelatedDocument getRelateDocument();

}