package dk.s4.hl7.cda.model;

/** Data structure for a single telecom instance.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 * @author Lars Duus Christian Hausmann, Silverbullet A/S
 */
public final class Telecom extends AbstractTelecom<AddressData.Use> {

  /** Telecom constructor.
   * @param use The address use of the telecom.
   * @param value The telecom value.
   */
  public Telecom(AddressData.Use use, String value) {
    super(use, value);
  }

  /** Telecom constructor.
   * @param use The address use of the telecom.
   * @param value The telecom value.
   * @param protocol The protocol for the telecom - ie. mailto, tel etc
   */
  public Telecom(AddressData.Use use, String protocol, String value) {
    super(use, protocol, value);

  }

  /**
   * Get the address use.
   * 
   * @return the address use.
   */
  public AddressData.Use getAddressUse() {
    return getUse();
  }

}
