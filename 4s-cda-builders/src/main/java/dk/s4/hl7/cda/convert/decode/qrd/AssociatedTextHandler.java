package dk.s4.hl7.cda.convert.decode.qrd;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.IdHandler;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse.QRDTextResponseBuilder;
import dk.s4.hl7.util.xml.ElementAttributeFinder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class AssociatedTextHandler extends BaseXmlHandler {
  public static final String ASSOCIATED_TEXT_BASE = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/entryRelationship/observation";
  private QRDTextResponse qrdTextResponse;

  private IdHandler idHandler;
  private MediaHandler mediaHandler;

  private ElementAttributeFinder elementAttributeFinderObservationMedia;

  private CodedValue questionCode;
  private String question;
  private String answer;

  public AssociatedTextHandler() {
    this.idHandler = new IdHandler(ASSOCIATED_TEXT_BASE);
    this.mediaHandler = new MediaHandler(ASSOCIATED_TEXT_BASE + "/entryRelationship/observationMedia");
    this.elementAttributeFinderObservationMedia = createEntryRelationshipFinderObservationMedia();

    addPath(ASSOCIATED_TEXT_BASE);
    addPath(ASSOCIATED_TEXT_BASE + "/value");
    addPath(ASSOCIATED_TEXT_BASE + "/code");
    addPath(ASSOCIATED_TEXT_BASE + "/code/originalText");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code", "codeSystem")) {
      questionCode = new CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "originalText")) {
      question = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "value")) {
      answer = xmlElement.getElementValue();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "observation")) {
      createTextQuestion();
    }
  }

  public QRDTextResponse getAssociatedTextResponse() {
    if (hasValues()) {
      return qrdTextResponse;
    }
    return null;
  }

  private boolean hasValues() {
    return question != null;
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    idHandler.addHandlerToMap(xmlMapping);
    elementAttributeFinderObservationMedia.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    idHandler.removeHandlerFromMap(xmlMapping);
    elementAttributeFinderObservationMedia.removeHandlerFromMap(xmlMapping);
  }

  private ElementAttributeFinder createEntryRelationshipFinderObservationMedia() {
    ElementAttributeFinder elementAttributeFinder = new ElementAttributeFinder(
        ASSOCIATED_TEXT_BASE + "/entryRelationship", "entryRelationship");
    elementAttributeFinder.addAttributeMatchers("typeCode", "REFR", mediaHandler);
    return elementAttributeFinder;
  }

  public void clear() {
    idHandler.clear();
    mediaHandler.clear();

    questionCode = null;
    question = null;

  }

  private void createTextQuestion() {
    QRDTextResponseBuilder builder = new QRDTextResponseBuilder();
    builder.setCodeValue(questionCode).setId(idHandler.getId()).setQuestion(question);
    builder.setText(answer);
    builder.setQuestionnaireMedia(mediaHandler.getQuestionnaireMedia());
    qrdTextResponse = builder.build();

  }

}
