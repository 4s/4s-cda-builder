package dk.s4.hl7.cda.model.pdc.v30;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;

/**
 * Manually entered information about the citizen’s No Resuscitation.
 */

public class NoResuscitationRegistration {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;
  private Registered registered;
  boolean hasRegisteredANoResuscitationWish;

  public enum Registered {
    True("has registered a no resuscitation wish"),
    False("has NOT registered a no resuscitation wish"),
    Nav("temporarily unavailable");

    private final String label;

    private Registered(String label) {
      this.label = label;
    }

    public String getLabel() {
      return label;
    }
  }

  public static class NoResuscitationRegistrationBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;
    private Registered registered;

    public NoResuscitationRegistrationBuilder(ID id) {
      this.id = id;
    }

    public NoResuscitationRegistrationBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public NoResuscitationRegistrationBuilder setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public NoResuscitationRegistrationBuilder setRegisteret(Registered registered) {
      this.registered = registered;
      return this;
    }

    public NoResuscitationRegistration build() {

      return new NoResuscitationRegistration(this);
    }

  }

  private NoResuscitationRegistration(NoResuscitationRegistrationBuilder builder) {
    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.registered = builder.registered;
    this.hasRegisteredANoResuscitationWish = (Registered.True.equals(this.registered) ? true : false);
  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * @return Registered Returns how the citizen has registered a a NoResuscitationWish
   */
  public Registered getRegistered() {
    return registered;
  }

  /**
   * @return boolean Returns if the citizen has registered a a NoResuscitationWish or not. In case the information is not available the value returned is false.
   */
  public boolean hasRegisteredANoResuscitationWish() {
    return hasRegisteredANoResuscitationWish;
  }

}
