package dk.s4.hl7.cda.convert.decode.header.v20;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.core.v20.ClinicalDocument;

public class ParticipantHandler<C extends ClinicalDocument>
    extends dk.s4.hl7.cda.convert.decode.header.ParticipantHandler<C> implements CDAXmlHandler<C> {

  public ParticipantHandler(ParticipantType participantType, String completeBaseXPath, String completeTimeXPath,
      String personElementName, String organizationXPath) {
    super(participantType, completeBaseXPath, completeTimeXPath, personElementName, organizationXPath);
  }

  @Override
  public void addDataToDocument(C clinicalDocument) {
    if (participantType == ParticipantType.AUTHOR) {
      Participant author0 = getSingleParticipant(0);
      Participant author1 = getSingleParticipant(1);
      if (author0 != null) {
        clinicalDocument.setAuthor(author0);
      }

      if (author1 != null) {
        clinicalDocument.setAuthorReferrer(author1);
      }

    } else {
      super.addDataToDocument(clinicalDocument);
    }

  }

  protected Participant getSingleParticipant(int i) {
    if (participants.isEmpty()) {
      return null;
    }
    if (participants.size() <= i) {
      return null;
    }
    return (Participant) participants.get(i);
  }

}
