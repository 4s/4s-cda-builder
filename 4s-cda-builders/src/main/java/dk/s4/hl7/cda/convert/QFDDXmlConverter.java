package dk.s4.hl7.cda.convert;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.convert.encode.ClinicalDocumentConverter;
import dk.s4.hl7.cda.convert.encode.narrative.text.NarrativeTextConverter;
import dk.s4.hl7.cda.convert.encode.narrative.text.NarrativeTextCreator;
import dk.s4.hl7.cda.convert.encode.pattern.PreconditionPattern;
import dk.s4.hl7.cda.convert.encode.pattern.ReferenceRangePattern;
import dk.s4.hl7.cda.model.*;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDAnalogSliderQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDDiscreteSliderQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDFeedback;
import dk.s4.hl7.cda.model.qfdd.QFDDHelpText;
import dk.s4.hl7.cda.model.qfdd.QFDDMultipleChoiceQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDNumericQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

/**
 * An implementation of the Converter that can build an XML Document object
 * following the Danish QFDD standard.
 * 
 * @author Frank Jacobsen Systematic A/S
 */
public class QFDDXmlConverter extends ClinicalDocumentConverter<QFDDDocument, Void> {
  private static final PreconditionPattern preconditionPattern = new PreconditionPattern();
  private static final String STATUS_CODE_COMPLETED = "completed";

  public QFDDXmlConverter(XmlPrettyPrinter xmlPrettyPrinter) {
    super(xmlPrettyPrinter);
  }

  public QFDDXmlConverter() {
    this(new XmlPrettyPrinter());
  }

  protected void buildContext(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // CONF-PHMR-15
    xmlBuilder.element("title").value(document.getTitle()).elementEnd();
    xmlBuilder.element("sdtc:statusCode").attribute("code", "NEW").elementShortEnd();

    // CONF-PHMR-16 / CONF-DK PHMR-17
    xmlBuilder
        .element("effectiveTime")
        .attribute("value", dateTimeformatter.format(document.getEffectiveTime()))
        .elementShortEnd();

    xmlBuilder
        .element("confidentialityCode")
        .attribute("code", "N")
        .attribute("codeSystem", HL7.CONFIDENTIALITY_OID)
        .elementShortEnd();

    // CONF-PHMR-17-20
    xmlBuilder.element("languageCode").attribute("code", document.getLanguageCode()).elementShortEnd();

    // CONF-DK PHMR-22
    BuildUtil.buildSetIdExtensionOptional(document.getSetId(), xmlBuilder);
    if (document.getVersionNumber() != null) {
      xmlBuilder.element("versionNumber").attribute("value", document.getVersionNumber().toString()).elementShortEnd();
    }
  }

  private static void generateTemplateIds(XmlStreamBuilder xmlBuilder, List<String> templateIds) throws IOException {
    for (String templateId : templateIds) {
      xmlBuilder.element("templateId").attribute("root", templateId).elementShortEnd();
    }
  }

  private void buildQFDDSections(List<Section<QFDDOrganizer>> qfddSectionsList, XmlStreamBuilder xmlBuilder)
      throws IOException {
    for (Section<QFDDOrganizer> qfddSection : qfddSectionsList) {

      List<QFDDOrganizer> qfddOrganizerList = qfddSection.getOrganizers();

      if (!qfddOrganizerList.isEmpty()) { // we have a list of questions
        xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
        xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");
        BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_SECTION_ROOT_OID);

        BuildUtil.buildCode(Loinc.QFD_CODE, Loinc.OID, null, Loinc.DISPLAYNAME, xmlBuilder);

        if (qfddSection.getTitle() != null) {
          xmlBuilder.element("title").value(qfddSection.getTitle()).elementEnd();
        }
        // Text may be embedded HTML at this point
        xmlBuilder.element("text").valueNoEscaping(qfddSection.getText()).elementEnd();

        for (QFDDOrganizer organizer : qfddOrganizerList) {

          List<QFDDQuestion> qfddQuestionList = organizer.getQFDDQuestions();
          xmlBuilder.element("entry").attribute("typeCode", "DRIV").attribute("contextConductionInd", "true");
          xmlBuilder.element("organizer").attribute("classCode", "BATTERY").attribute("moodCode", "EVN");

          generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_QUESTION_ORGANIZER));
          BuildUtil.buildIds(organizer.getIds(), xmlBuilder);
          BuildUtil.buildCodeOptionalDisplayNameOptionalOriginalText(organizer.getCode(), organizer.getIntroduction(),
              xmlBuilder);
          xmlBuilder.element("statusCode").attribute("code", STATUS_CODE_COMPLETED).elementShortEnd();
          preconditionPattern.build(xmlBuilder, organizer.getPreconditions());
          buildQuestions(xmlBuilder, qfddQuestionList);
          xmlBuilder.elementEnd(); // End organizer
          xmlBuilder.elementEnd(); // End entry
        }
        xmlBuilder.elementEnd(); // End section
        xmlBuilder.elementEnd(); // End component

      } else if (qfddSection.getSectionInformation() != null) { // we have a copyright section or an information only section

        if (qfddSection.isCopyRightSection()) { // its copyright section
          buildCopyRight(qfddSection.getSectionInformation(), qfddSection.getCopyrightTexts(), xmlBuilder);

        } else { // its an information only section

          xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
          xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");
          BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_SECTION_ROOT_OID);

          if (qfddSection.getTitle() != null) {
            xmlBuilder.element("title").value(qfddSection.getTitle()).elementEnd();
          }

          // Text may be embedded HTML at this point
          xmlBuilder.element("text").valueNoEscaping(qfddSection.getText()).elementEnd();
          if (qfddSection.getLanguage() != null) {
            xmlBuilder.element("languageCode").attribute("code", qfddSection.getLanguage()).elementShortEnd();
          }
          xmlBuilder.elementEnd(); // End section
          xmlBuilder.elementEnd(); // End component

        }
      }
    }
  }

  private void buildQuestions(XmlStreamBuilder xmlBuilder, List<QFDDQuestion> qfddQuestionList) throws IOException {
    int sequenceNumber = 0;
    for (QFDDQuestion qfddQuestion : qfddQuestionList) {
      buildOneQuestion(qfddQuestion, xmlBuilder, ++sequenceNumber);
    }
  }

  private void buildCopyRight(SectionInformation copyRight, List<String> copyrightTexts, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (copyRight != null) {
      xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
      xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_COPYRIGHT_SECTION_TEMPLATEID);
      if (copyRight.getTitle() != null) {
        xmlBuilder.element("title").value(copyRight.getTitle()).elementEnd();
      }

      if (copyRight.getText() == null) {
        xmlBuilder
            .element("text")
            .valueNoEscaping(NarrativeTextCreator.createTextFromListBrSeparated(copyrightTexts))
            .elementEnd();
      } else {
        xmlBuilder.element("text").valueNoEscaping(copyRight.getText()).elementEnd();
      }

      if (copyRight.getLanguage() != null) {
        xmlBuilder.element("languageCode").attribute("code", copyRight.getLanguage()).elementShortEnd();
      }

      if (copyrightTexts != null && !copyrightTexts.isEmpty()) {
        for (String text : copyrightTexts) {
          buildCopyRightEntry(text, xmlBuilder);
        }
      } else {
        buildCopyRightEntry(copyRight.getText(), xmlBuilder);
      }

      xmlBuilder.elementEnd(); // end component
      xmlBuilder.elementEnd(); // end section
    }

  }

  private void buildCopyRightEntry(String text, XmlStreamBuilder xmlBuilder) throws IOException {

    xmlBuilder.element("entry").attribute("typeCode", "DRIV").attribute("contextConductionInd", "true");
    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");
    BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_COPYRIGHT_PATTERN_TEMPLATEID);

    BuildUtil.buildCode("COPY", Loinc.OID, "Code for Copyright", Loinc.DISPLAYNAME, xmlBuilder);

    xmlBuilder.element("value").attribute("xsi:type", "ST").value(text).elementEnd();

    xmlBuilder.elementEnd(); // end entry
    xmlBuilder.elementEnd(); // end observation
  }

  private static void buildOneQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder, int sequenceNumber)
      throws IOException {

    xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("sequenceNumber").attribute("value", String.valueOf(sequenceNumber)).elementShortEnd();
    buildObservation(qfddQuestion, xmlBuilder);

    xmlBuilder.elementEnd(); // end component

  }

  private static void buildObservation(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder) throws IOException {

    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "DEF");

    if (qfddQuestion instanceof QFDDNumericQuestion) {
      buildNumericQuestion(qfddQuestion, xmlBuilder);
    } else if (qfddQuestion instanceof QFDDDiscreteSliderQuestion) {
      buildDiscreteSliderQuestion(qfddQuestion, xmlBuilder);
    } else if (qfddQuestion instanceof QFDDAnalogSliderQuestion) {
      buildAnalogSliderQuestion(qfddQuestion, xmlBuilder);
    } else if (qfddQuestion instanceof QFDDTextQuestion) {
      buildTextQuestion(qfddQuestion, xmlBuilder);
    } else if (qfddQuestion instanceof QFDDMultipleChoiceQuestion) {
      buildMultipleChoiceQuestion(qfddQuestion, xmlBuilder);
    }

    //    externalReferencePattern.build(qfddQuestion.getReferences(), xmlBuilder);

    xmlBuilder.elementEnd(); // end observation

  }

  protected static void buildFeedback(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qfddQuestion.getFeedback() != null) {
      xmlBuilder.element("entryRelationship").attribute("typeCode", "REFR").attribute("contextConductionInd", "true");
      buildQuestionFeedback(qfddQuestion.getFeedback(), xmlBuilder);
      xmlBuilder.elementEnd();
    }
  }

  protected static void buildMedia(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qfddQuestion.getQuestionnaireMedia() != null) {
      xmlBuilder.element("entryRelationship").attribute("typeCode", "REFR");
      buildQuestionMedia(qfddQuestion.getQuestionnaireMedia(), xmlBuilder);
      xmlBuilder.elementEnd();
    }
  }

  private static void buildQuestionHelpText(QFDDHelpText qfddHelpText, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qfddHelpText == null)
      return;

    xmlBuilder.element("entryRelationship").attribute("typeCode", "SUBJ").attribute("contextConductionInd", "true");
    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");

    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_QUESTION_HELP_TEXT_PATTERN_OID));
    BuildUtil.buildCode(Loinc.HELPTEXT_CODE, Loinc.OID, Loinc.HELPTEXT_DISPLAYNAME, Loinc.DISPLAYNAME, xmlBuilder);

    xmlBuilder.element("value").attribute("xsi:type", "ST");
    if (qfddHelpText.getLanguage() != null) {
      xmlBuilder.attribute("language", qfddHelpText.getLanguage());
    }
    xmlBuilder.value(qfddHelpText.getHelpText()).elementEnd();

    xmlBuilder.elementEnd();// end Help text
    xmlBuilder.elementEnd();// end entryRelationship
  }

  private static void buildQuestionFeedback(QFDDFeedback qfddFeedback, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qfddFeedback == null)
      return;

    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "DEF");
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_QUESTION_FEEDBACK_PATTERN_OID));
    BuildUtil.buildCode(Loinc.FEEDBACK_CODE, Loinc.OID, Loinc.FEEDBACK_DISPLAYNAME, Loinc.DISPLAYNAME, xmlBuilder);
    xmlBuilder.element("value").attribute("xsi:type", "ST");
    if (qfddFeedback.getLanguage() != null) {
      xmlBuilder.attribute("language", qfddFeedback.getLanguage());
    }
    xmlBuilder.value(qfddFeedback.getFeedBackText()).elementEnd();
    preconditionPattern.build(xmlBuilder, qfddFeedback.getQFDDPreconditionList());
    xmlBuilder.elementEnd();

  }

  private static void buildQuestionMedia(QuestionnaireMedia questionnaireMedia, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (questionnaireMedia == null)
      return;

    xmlBuilder.element("entryRelationship").attribute("typeCode", "REFR");
    if (questionnaireMedia.getId() != null) {
      xmlBuilder.element("observationMedia").attribute("classCode", "OBS").attribute("moodCode", "DEF").attribute("ID",
          questionnaireMedia.getId());
    } else {
      xmlBuilder.element("observationMedia").attribute("classCode", "OBS").attribute("moodCode", "DEF");
    }

    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_QUESTION_MEDIA_PATTERN_OID));

    if (questionnaireMedia.getValue() != null) {
      xmlBuilder
          .element("value")
          .attribute("mediaType", questionnaireMedia.getMediaType())
          .attribute("representation", questionnaireMedia.getRepresentation())
          .value(questionnaireMedia.getValue());
    } else {
      xmlBuilder.element("value").attribute("mediaType", questionnaireMedia.getMediaType()).attribute("representation",
          questionnaireMedia.getRepresentation());
    }

    xmlBuilder.elementEnd();// end value
    xmlBuilder.elementEnd();// end observationMedia text
    xmlBuilder.elementEnd();// end entryRelationship
  }

  private static void buildAssociatedTextQuestion(QFDDTextQuestion associatedTextQuestion, XmlStreamBuilder xmlBuilder)
      throws IOException {

    if (associatedTextQuestion == null) {
      return;
    }
    xmlBuilder.element("entryRelationship").attribute("typeCode", "REFR");
    buildObservation(associatedTextQuestion, xmlBuilder);
    xmlBuilder.elementEnd();// end entryRelationship

  }

  private static void buildMultipleChoiceQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder)
      throws IOException {

    QFDDMultipleChoiceQuestion qfddMultipleChoiceQuestion = (QFDDMultipleChoiceQuestion) qfddQuestion;
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID));

    BuildUtil.buildId(qfddQuestion.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCodeOptionalDisplayName(qfddQuestion, xmlBuilder);

    for (CodedValue codeValue : qfddMultipleChoiceQuestion.getAnswerOptionList()) {
      xmlBuilder
          .element("value")
          .attribute("xsi:type", "CE")
          .attribute("code", codeValue.getCode())
          .attribute("codeSystem", codeValue.getCodeSystem())
          .attribute("displayName", codeValue.getDisplayName())
          .attribute("codeSystemName", codeValue.getCodeSystemName())
          .elementShortEnd();
    }

    questionOptionsPattern.build(qfddMultipleChoiceQuestion.getMinimum(), qfddMultipleChoiceQuestion.getMaximum(),
        xmlBuilder);
    buildFeedback(qfddQuestion, xmlBuilder);
    buildQuestionHelpText(qfddQuestion.getHelpText(), xmlBuilder);
    buildQuestionMedia(qfddQuestion.getQuestionnaireMedia(), xmlBuilder);
    buildAssociatedTextQuestion(qfddMultipleChoiceQuestion.getAssociatedTextQuestion(), xmlBuilder);
    preconditionPattern.build(xmlBuilder, qfddQuestion.getPreconditions());
  }

  private static void buildTextQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder) throws IOException {
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_TEXT_QUESTION_PATTERN_OID));
    BuildUtil.buildId(qfddQuestion.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCodeOptionalDisplayName(qfddQuestion, xmlBuilder);
    buildFeedback(qfddQuestion, xmlBuilder);
    buildQuestionHelpText(qfddQuestion.getHelpText(), xmlBuilder);
    buildQuestionMedia(qfddQuestion.getQuestionnaireMedia(), xmlBuilder);
    preconditionPattern.build(xmlBuilder, qfddQuestion.getPreconditions());
  }

  private static void buildAnalogSliderQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder)
      throws IOException {
    QFDDAnalogSliderQuestion qfddAnalogSliderQuestion = (QFDDAnalogSliderQuestion) qfddQuestion;
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_NUMERIC_PATTERN_OID, HL7.QFDD_ANALOG_SLIDER_PATTERN_OID));

    BuildUtil.buildId(qfddQuestion.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCodeOptionalDisplayName(qfddQuestion, xmlBuilder);

    buildFeedback(qfddQuestion, xmlBuilder);
    buildQuestionHelpText(qfddQuestion.getHelpText(), xmlBuilder);
    preconditionPattern.build(xmlBuilder, qfddQuestion.getPreconditions());
    // Specific referenceRange for analogSlider which is different from ReferenceRangePattern class
    xmlBuilder.element("referenceRange").attribute("typeCode", "REFV");
    xmlBuilder.element("observationRange");

    xmlBuilder.element("value").attribute("xsi:type", IntervalType.GLIST_PQ.name()).attribute("denominator",
        qfddAnalogSliderQuestion.getMaximum());

    xmlBuilder.element("head").attribute("value", qfddAnalogSliderQuestion.getMinimum()).elementShortEnd();
    xmlBuilder.element("increment").attribute("value", qfddAnalogSliderQuestion.getIncrement()).elementShortEnd();
    xmlBuilder.elementEnd(); // end value
    xmlBuilder.elementEnd(); // end observationRange
    xmlBuilder.elementEnd(); // end referenceRange
  }

  private static void buildDiscreteSliderQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder)
      throws IOException {
    QFDDDiscreteSliderQuestion qfddDiscreteSliderQuestion = (QFDDDiscreteSliderQuestion) qfddQuestion;
    generateTemplateIds(xmlBuilder,
        Arrays.asList(HL7.QFDD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID, HL7.QFDD_DISCRETE_SLIDER_PATTERN_OID));

    BuildUtil.buildId(qfddQuestion.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCodeOptionalDisplayName(qfddQuestion, xmlBuilder);

    for (CodedValue codeValue : qfddDiscreteSliderQuestion.getAnswerOptionList()) {
      xmlBuilder
          .element("value")
          .attribute("xsi:type", "CE")
          .attribute("code", codeValue.getCode())
          .attribute("codeSystem", codeValue.getCodeSystem())
          .attribute("displayName", codeValue.getDisplayName())
          .attribute("codeSystemName", codeValue.getCodeSystemName())
          .elementShortEnd();
    }

    questionOptionsPattern.build(qfddDiscreteSliderQuestion.getMinimum(), qfddDiscreteSliderQuestion.getMaximum(),
        xmlBuilder);
    buildFeedback(qfddQuestion, xmlBuilder);
    buildQuestionHelpText(qfddQuestion.getHelpText(), xmlBuilder);
    buildAssociatedTextQuestion(qfddDiscreteSliderQuestion.getAssociatedTextQuestion(), xmlBuilder);
    preconditionPattern.build(xmlBuilder, qfddQuestion.getPreconditions());
  }

  private static void buildNumericQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder) throws IOException {
    QFDDNumericQuestion qfddNumericQuestion = (QFDDNumericQuestion) qfddQuestion;
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_NUMERIC_PATTERN_OID));
    BuildUtil.buildId(qfddQuestion.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCodeOptionalDisplayName(qfddQuestion, xmlBuilder);
    buildFeedback(qfddQuestion, xmlBuilder);
    buildQuestionHelpText(qfddQuestion.getHelpText(), xmlBuilder);
    buildQuestionMedia(qfddQuestion.getQuestionnaireMedia(), xmlBuilder);
    preconditionPattern.build(xmlBuilder, qfddQuestion.getPreconditions());
    new ReferenceRangePattern().build(qfddNumericQuestion, xmlBuilder);
  }

  public String convert(QFDDDocument source) {
    int nofObservations = source.getInformationRecipients().size() + source.getSections().size();
    for (Section<QFDDOrganizer> section : source.getSections()) {
      for (QFDDOrganizer organizer : section.getOrganizers()) {
        nofObservations += organizer.getQFDDQuestions().size();
      }
    }
    StringBuilder builder = new StringBuilder(8000 + (nofObservations * 1500));
    serialize(source, builder);
    return builder.toString();
  }

  @Override
  protected void buildPatientSection(QFDDDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("recordTarget").attribute("typeCode", "RCT").attribute("contextControlCode", "OP");
    xmlBuilder.element("patientRole").attribute("nullFlavor", "NI");
    xmlBuilder.element("id").attribute("nullFlavor", "NI").elementShortEnd();
    xmlBuilder.elementEnd(); // end recordTarget
    xmlBuilder.elementEnd(); // end patientRole
  }

  @Override
  protected void buildDocumentationOf(QFDDDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // NOT used.
  }

  @Override
  protected void buildDataEnterer(ClinicalDocument document, boolean includeNullFlavorAdr, XmlStreamBuilder xmlBuilder)
      throws IOException {
    // NOT USED
  }

  @Override
  protected void buildInformationRecipient(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // NOT USED
  }

  @Override
  protected void buildParticipants(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // NOT USED
  }

  @Override
  protected void buildLegalAuthenticatorSection(ClinicalDocument document, XmlStreamBuilder xmlBuilder)
      throws IOException {
    // Not used
  }

  @Override
  protected void buildBody(QFDDDocument document, XmlStreamBuilder xmlBuilder,
      NarrativeTextConverter<Void> narrativeTextConverter) throws IOException {
    this.buildStructuredBodySectionStart(xmlBuilder);
    this.buildQFDDSections(document.getSections(), xmlBuilder);
    this.buildStructuredBodySectionEnd(xmlBuilder);
  }
}
