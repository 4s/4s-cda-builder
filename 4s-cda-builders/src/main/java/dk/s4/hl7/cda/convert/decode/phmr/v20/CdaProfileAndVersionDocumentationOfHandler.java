package dk.s4.hl7.cda.convert.decode.phmr.v20;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.phmr.v20.PHMRDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class CdaProfileAndVersionDocumentationOfHandler implements CDAXmlHandler<PHMRDocument> {

  private String currentTemplateId;
  private String cdaProfileAndVersion;

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      currentTemplateId = xmlElement.getAttributeValue("root");
    }
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "root", "extension")) {
      if (currentTemplateId != null && currentTemplateId.equals(MedCom.VERSION_TEMPLATEID_ROOT)) {
        cdaProfileAndVersion = xmlElement.getAttributeValue("extension");
      }
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent/id", this);
    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent/templateId", this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/id");
    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/templateId");
  }

  @Override
  public void addDataToDocument(PHMRDocument pHMRDocument) {
    pHMRDocument.setCdaProfileAndVersion(cdaProfileAndVersion);
  }

}
