package dk.s4.hl7.cda.convert.decode.qfdd;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.HelpTextHandler;
import dk.s4.hl7.cda.convert.decode.general.IdHandler;
import dk.s4.hl7.cda.convert.decode.qfdd.precondition.PreconditionHandler;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion.BaseQFDDQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion.QFDDTextQuestionBuilder;
import dk.s4.hl7.util.xml.ElementAttributeFinder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class AssociatedTextHandler extends BaseXmlHandler {
  public static final String ASSOCIATED_TEXT_BASE = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/entryRelationship/observation";
  private QFDDTextQuestion qFDDTextQuestion;

  private IdHandler idHandler;
  private HelpTextHandler helpTextHandler;
  private MediaHandler mediaHandler;
  private PreconditionHandler preconditionHandler;

  private ElementAttributeFinder elementAttributeFinderObservation;
  private ElementAttributeFinder elementAttributeFinderObservationMedia;

  private CodedValue questionCode;
  private String question;

  public AssociatedTextHandler() {
    this.idHandler = new IdHandler(ASSOCIATED_TEXT_BASE);
    this.helpTextHandler = new HelpTextHandler(ASSOCIATED_TEXT_BASE + "/entryRelationship/observation");
    this.mediaHandler = new MediaHandler(ASSOCIATED_TEXT_BASE + "/entryRelationship/observationMedia");
    this.elementAttributeFinderObservation = createEntryRelationshipFinderObservation();
    this.elementAttributeFinderObservationMedia = createEntryRelationshipFinderObservationMedia();
    this.preconditionHandler = new PreconditionHandler(ASSOCIATED_TEXT_BASE + "/precondition");

    addPath(ASSOCIATED_TEXT_BASE);
    addPath(ASSOCIATED_TEXT_BASE + "/value");
    addPath(ASSOCIATED_TEXT_BASE + "/code");
    addPath(ASSOCIATED_TEXT_BASE + "/code/originalText");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {

    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code", "codeSystem")) {
      questionCode = new CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "originalText")) {
      question = xmlElement.getElementValue();
    }

  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "observation")) {
      createTextQuestion();
    }
  }

  public QFDDTextQuestion getAssociatedTextQuestion() {
    if (hasValues()) {
      return qFDDTextQuestion;
    }
    return null;
  }

  private boolean hasValues() {
    return question != null;
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    idHandler.addHandlerToMap(xmlMapping);
    elementAttributeFinderObservation.addHandlerToMap(xmlMapping);
    elementAttributeFinderObservationMedia.addHandlerToMap(xmlMapping);
    preconditionHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    idHandler.removeHandlerFromMap(xmlMapping);
    elementAttributeFinderObservation.removeHandlerFromMap(xmlMapping);
    elementAttributeFinderObservationMedia.removeHandlerFromMap(xmlMapping);
    preconditionHandler.removeHandlerFromMap(xmlMapping);
  }

  private ElementAttributeFinder createEntryRelationshipFinderObservation() {
    ElementAttributeFinder elementAttributeFinder = new ElementAttributeFinder(
        ASSOCIATED_TEXT_BASE + "/entryRelationship/observation/templateId", "templateId");
    elementAttributeFinder.addAttributeMatchers("root", HL7.QFDD_QUESTION_HELP_TEXT_PATTERN_OID, helpTextHandler);
    return elementAttributeFinder;
  }

  private ElementAttributeFinder createEntryRelationshipFinderObservationMedia() {
    ElementAttributeFinder elementAttributeFinder = new ElementAttributeFinder(
        ASSOCIATED_TEXT_BASE + "/entryRelationship", "entryRelationship");
    elementAttributeFinder.addAttributeMatchers("typeCode", "REFR", mediaHandler);
    return elementAttributeFinder;
  }

  public void clear() {
    idHandler.clear();
    helpTextHandler.clear();
    mediaHandler.clear();
    preconditionHandler.clear();

    questionCode = null;
    question = null;

  }

  private void createTextQuestion() {
    QFDDTextQuestionBuilder builder = new QFDDTextQuestionBuilder();
    builder.setCodeValue(questionCode).setId(idHandler.getId()).setQuestion(question);
    builder.setHelpText(helpTextHandler.getHelpTextQFDD());
    builder.setQuestionnaireMedia(mediaHandler.getQuestionnaireMedia());
    addPreconditions(builder);

    qFDDTextQuestion = builder.build();
  }

  private void addPreconditions(BaseQFDDQuestionBuilder<?, ?> builder) {
    for (QFDDPrecondition precondition : preconditionHandler.getPreconditions()) {
      builder.addPrecondition(precondition);
    }
  }

}
