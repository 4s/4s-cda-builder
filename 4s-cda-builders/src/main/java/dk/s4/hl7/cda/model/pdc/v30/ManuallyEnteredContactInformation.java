package dk.s4.hl7.cda.model.pdc.v30;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.TelecomPhone;

/**
 * Manually entered information about the citizen’s phone numbers
 */

public class ManuallyEnteredContactInformation {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;
  private TelecomPhone[] telecomPhone;

  public static class ManuallyEnteredContactInformationBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;
    private List<TelecomPhone> telecomPhoneTemporary;
    private TelecomPhone[] telecomPhone;

    public ManuallyEnteredContactInformationBuilder(ID id) {
      telecomPhoneTemporary = new ArrayList<TelecomPhone>();
      this.id = id;
    }

    public ManuallyEnteredContactInformationBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public ManuallyEnteredContactInformationBuilder setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public ManuallyEnteredContactInformationBuilder addTelecom(TelecomPhone telecom) {
      telecomPhoneTemporary.add(telecom);
      return this;
    }

    public ManuallyEnteredContactInformationBuilder setTelecomPhone(TelecomPhone[] telecom) {
      this.telecomPhone = telecom;
      return this;
    }

    public ManuallyEnteredContactInformation build() {
      for (int i = 0; i < telecomPhone.length; i++) {
        telecomPhoneTemporary.add(telecomPhone[i]);
      }
      if (telecomPhoneTemporary.size() > 0) {
        telecomPhone = new TelecomPhone[telecomPhoneTemporary.size()];
        telecomPhoneTemporary.toArray(telecomPhone);
      } else {
        telecomPhone = new TelecomPhone[0];
      }

      return new ManuallyEnteredContactInformation(this);
    }

  }

  private ManuallyEnteredContactInformation(ManuallyEnteredContactInformationBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.telecomPhone = builder.telecomPhone;

  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * @return TelecomPhone Returns an array of citizens phonenumbers
   */
  public TelecomPhone[] getTelecomPhone() {
    return telecomPhone;
  }

}
