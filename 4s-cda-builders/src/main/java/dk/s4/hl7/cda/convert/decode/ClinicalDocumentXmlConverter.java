package dk.s4.hl7.cda.convert.decode;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

import dk.s4.hl7.cda.convert.base.Converter;
import dk.s4.hl7.cda.convert.base.ReaderSerializer;
import dk.s4.hl7.cda.convert.decode.header.CDAHeaderHandler;
import dk.s4.hl7.cda.convert.decode.header.ParticipantHandler;
import dk.s4.hl7.cda.convert.decode.header.ParticipantHandler.ParticipantType;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.util.xml.XmlElementFinder;
import dk.s4.hl7.util.xml.XmlHandler;
import dk.s4.hl7.util.xml.XmlMapping;

/**
 * Abstract base class for all CDA converters that parses xml to the GreenCDA
 * model
 * 
 * Contains overall process of converting.
 * 
 * @param <E>
 */
public abstract class ClinicalDocumentXmlConverter<E extends ClinicalDocument>
    implements Converter<String, E>, ReaderSerializer<E> {

  protected void parseDocument(Reader source, XmlElementFinder finder, XmlMapping xmlMapping) {
    try {
      finder.findElements(source, xmlMapping);
    } catch (Exception e) {
      throw new CdaBuilderException("Cant parse xml document " + e.getMessage(), e);
    } finally {
      if (source != null) {
        try {
          source.close();
        } catch (IOException e) {
          // Ignore
        }
      }
    }
  }

  protected abstract List<CDAXmlHandler<E>> createParticipantHandlers();

  protected abstract List<CDAXmlHandler<E>> createSectionHandlers();

  protected abstract E createNewDocument(CDAHeaderHandler<E> headerHandler);

  protected CDAHeaderHandler<E> createCDAHeaderHandler() {
    return new CDAHeaderHandler<E>();
  }

  @Override
  public E deserialize(Reader source) {
    try {
      XmlElementFinder finder = new XmlElementFinder();
      XmlMapping xmlMapping = new XmlMapping();
      CDAHeaderHandler<E> cdaHeaderHandler = createCDAHeaderHandler();
      List<CDAXmlHandler<E>> xmlHeaderHandlers = addHeaderHandlersToMapping(xmlMapping, cdaHeaderHandler);
      List<CDAXmlHandler<E>> xmlSectionHandlers = addSectionHandlersToMapping(xmlMapping);
      parseDocument(source, finder, xmlMapping);
      E cdaDocument = createNewDocument(cdaHeaderHandler);
      addHandlerDataToDocument(xmlHeaderHandlers, cdaDocument);
      addHandlerDataToDocument(xmlSectionHandlers, cdaDocument);
      return cdaDocument;
    } catch (CdaBuilderException cx) {
      throw cx;
    } catch (Exception ex) {
      throw new CdaBuilderException(ex);
    }
  }

  private List<CDAXmlHandler<E>> addSectionHandlersToMapping(XmlMapping xmlMapping) {
    List<CDAXmlHandler<E>> xmlSectionHandlers = createSectionHandlers();
    for (XmlHandler xmlHandler : xmlSectionHandlers) {
      xmlHandler.addHandlerToMap(xmlMapping);
    }
    return xmlSectionHandlers;
  }

  private List<CDAXmlHandler<E>> addHeaderHandlersToMapping(XmlMapping xmlMapping,
      CDAHeaderHandler<E> cdaHeaderHandler) {
    List<CDAXmlHandler<E>> xmlHeaderHandlers = createParticipantHandlers();
    xmlHeaderHandlers.add(cdaHeaderHandler);
    for (XmlHandler xmlHandler : xmlHeaderHandlers) {
      xmlHandler.addHandlerToMap(xmlMapping);
    }
    return xmlHeaderHandlers;
  }

  private <T extends ClinicalDocument> void addHandlerDataToDocument(List<CDAXmlHandler<T>> xmlhandlers,
      T cdaDocument) {
    for (CDAXmlHandler<T> xmlHandler : xmlhandlers) {
      xmlHandler.addDataToDocument(cdaDocument);
    }
  }

  protected ParticipantHandler<E> createLegalAuthenticatorHandler() {
    return new ParticipantHandler<E>(ParticipantType.LEGAL_AUTHENTICATOR,
        "/ClinicalDocument/legalAuthenticator/assignedEntity", "/ClinicalDocument/legalAuthenticator/time",
        "assignedPerson", "/representedOrganization");
  }

  protected ParticipantHandler<E> createAuthorHandler() {
    return new ParticipantHandler<E>(ParticipantType.AUTHOR, "/ClinicalDocument/author/assignedAuthor",
        "/ClinicalDocument/author/time", "assignedPerson", "/representedOrganization");
  }

  protected ParticipantHandler<E> createParticipantHandler() {
    return new ParticipantHandler<E>(ParticipantType.PARTICIPANT, "/ClinicalDocument/participant/associatedEntity",
        "/ClinicalDocument/participant/time", "associatedPerson", "/scopingOrganization");
  }

  protected ParticipantHandler<E> createInformationRecipientHandler() {
    return new ParticipantHandler<E>(ParticipantType.INFORMATION_RECIPIENT,
        "/ClinicalDocument/informationRecipient/intendedRecipient", "/ClinicalDocument/informationRecipient/time",
        "informationRecipient", "/receivedOrganization");
  }

  protected ParticipantHandler<E> createDataEntererHandler() {
    return new ParticipantHandler<E>(ParticipantType.DATAENTERER, "/ClinicalDocument/dataEnterer/assignedEntity",
        "/ClinicalDocument/dataEnterer/time", "assignedPerson", "/representedOrganization");
  }
}
