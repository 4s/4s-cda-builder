package dk.s4.hl7.cda.convert.encode.narrative.text;

import java.util.List;
import java.util.regex.Matcher;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse;

/**
 * <p>This converter takes a Section, and using that information, it creates the narrative text by search and replace
 * <p>The logic for each type of answer is as follows: </p>
 * <p> - QRDNumericResponse: search for response.code.code, replace by response.value.value </p>
 * <p> - QRDDiscreteSliderResponse: search for response.answer.code, replace by "X"  </p>
 * <p> - QRDAnalogSliderResponse: search for response.code.code, replace by response.value </p>
 * <p> - QRDTextResponse: search for response.code.code, replace by response.text </p>
 * <p> - QRDMultipleChoiceResponse: search for response.answer.code, replace by "X" </p>
 * 
 */
public class QRDXMLNarrativeTextReplaceResponseConverter implements NarrativeTextConverter<Section<QRDOrganizer>> {

  private String answerCodeStart = "<!--";
  private String answerCodeEnd = "-->";
  private String answerXMark = "X";

  public String createText(Section<QRDOrganizer> documentSection) {
    String narrativeText = documentSection.getText();

    List<QRDOrganizer> qrdOrganizersList = documentSection.getOrganizers();
    for (QRDOrganizer organizer : qrdOrganizersList) {

      List<QRDResponse> qrdResponseList = organizer.getQRDResponses();

      for (QRDResponse qRDResponse : qrdResponseList) {
        narrativeText = replaceResponse(qRDResponse, narrativeText);
      }
    }

    return narrativeText;
  }

  private String replaceResponse(QRDResponse qRDResponse, String narrativeText) {
    if (qRDResponse instanceof QRDNumericResponse) {
      return replaceNumericResponse(qRDResponse, narrativeText);
    } else if (qRDResponse instanceof QRDDiscreteSliderResponse) {
      return replaceDiscreteSliderResponse(qRDResponse, narrativeText);
    } else if (qRDResponse instanceof QRDAnalogSliderResponse) {
      return replaceAnalogSliderResponse(qRDResponse, narrativeText);
    } else if (qRDResponse instanceof QRDTextResponse) {
      return replaceTextResponse(qRDResponse, narrativeText);
    } else if (qRDResponse instanceof QRDMultipleChoiceResponse) {
      return replaceMultipleChoiceResponse(qRDResponse, narrativeText);
    } else {
      return narrativeText;
    }
  }

  private String replaceNumericResponse(QRDResponse qRDResponse, String narrativeText) {
    QRDNumericResponse qRDNumericResponse = (QRDNumericResponse) qRDResponse;

    if (qRDNumericResponse.getCode() != null && qRDNumericResponse.getCode().getCode() != null
        && !qRDNumericResponse.getCode().getCode().isEmpty()) {
      if (qRDNumericResponse.getValue() != null) {
        String answerToSearch = qRDNumericResponse.getCode().getCode();
        String pattern = answerCodeStart + "\\s*" + answerToSearch + "\\s*" + answerCodeEnd;
        narrativeText = narrativeText.replaceAll(pattern, qRDNumericResponse.getValue());
      }
    }
    return narrativeText;
  }

  private String replaceDiscreteSliderResponse(QRDResponse qRDResponse, String narrativeText) {
    QRDDiscreteSliderResponse qRDDiscreteSliderResponse = (QRDDiscreteSliderResponse) qRDResponse;

    if (qRDDiscreteSliderResponse.getAnswer() != null && qRDDiscreteSliderResponse.getAnswer().getCode() != null
        && !qRDDiscreteSliderResponse.getAnswer().getCode().isEmpty()) {
      String answerToSearch = qRDDiscreteSliderResponse.getAnswer().getCode();
      String pattern = answerCodeStart + "\\s*" + answerToSearch + "\\s*" + answerCodeEnd;
      narrativeText = narrativeText.replaceAll(pattern, answerXMark);
    }
    if (qRDDiscreteSliderResponse.getAssociatedTextResponse() != null) {
      narrativeText = replaceTextResponse(qRDDiscreteSliderResponse.getAssociatedTextResponse(), narrativeText);
    }
    return narrativeText;
  }

  private String replaceAnalogSliderResponse(QRDResponse qRDResponse, String narrativeText) {

    QRDAnalogSliderResponse qRDAnalogSliderResponse = (QRDAnalogSliderResponse) qRDResponse;

    if (qRDAnalogSliderResponse.getCode() != null && qRDAnalogSliderResponse.getCode().getCode() != null
        && !qRDAnalogSliderResponse.getCode().getCode().isEmpty()) {
      if (qRDAnalogSliderResponse.getValue() != null) {
        String answerToSearch = qRDAnalogSliderResponse.getCode().getCode();
        String pattern = answerCodeStart + "\\s*" + answerToSearch + "\\s*" + answerCodeEnd;
        narrativeText = narrativeText.replaceAll(pattern, qRDAnalogSliderResponse.getValue());
      }
    }
    return narrativeText;
  }

  private String replaceTextResponse(QRDResponse qRDResponse, String narrativeText) {
    QRDTextResponse qRDTextResponse = (QRDTextResponse) qRDResponse;

    if (qRDTextResponse.getCode() != null && qRDTextResponse.getCode().getCode() != null
        && !qRDTextResponse.getCode().getCode().isEmpty()) {
      if (qRDTextResponse.getText() != null && !qRDTextResponse.getText().isEmpty()) {
        String answerToSearch = qRDTextResponse.getCode().getCode();
        String pattern = answerCodeStart + "\\s*" + answerToSearch + "\\s*" + answerCodeEnd;
        String textToUse = Matcher.quoteReplacement(qRDTextResponse.getText());
        narrativeText = narrativeText.replaceAll(pattern, textToUse);
      }
    }
    return narrativeText;
  }

  private String replaceMultipleChoiceResponse(QRDResponse qRDResponse, String narrativeText) {
    QRDMultipleChoiceResponse qRDMultipleChoiceResponse = (QRDMultipleChoiceResponse) qRDResponse;

    for (CodedValue answerCode : qRDMultipleChoiceResponse.getAnswers()) {
      if (answerCode != null & answerCode.getCode() != null && !answerCode.getCode().isEmpty()) {
        String answerToSearch = answerCode.getCode();
        String pattern = answerCodeStart + "\\s*" + answerToSearch + "\\s*" + answerCodeEnd;
        narrativeText = narrativeText.replaceAll(pattern, answerXMark);
      }
    }

    if (qRDMultipleChoiceResponse.getAssociatedTextResponse() != null) {
      narrativeText = replaceTextResponse(qRDMultipleChoiceResponse.getAssociatedTextResponse(), narrativeText);
    }
    return narrativeText;
  }

}
