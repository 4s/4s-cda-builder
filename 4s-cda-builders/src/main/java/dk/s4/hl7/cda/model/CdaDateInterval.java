package dk.s4.hl7.cda.model;

import java.util.Date;

public class CdaDateInterval {

  private CdaDate startTime;
  private CdaDate stopTime;

  public CdaDateInterval() {
  }

  public CdaDateInterval(CdaDate startTime, CdaDate stopTime) {
    this.startTime = startTime;
    this.stopTime = stopTime;
  }

  public CdaDateInterval(Date startTime, Date stopTime, boolean hasTime) {
    this.startTime = new CdaDate(startTime, hasTime);
    this.stopTime = new CdaDate(stopTime, hasTime);
  }

  public CdaDate getStartTimeCdaDate() {
    return startTime;
  }

  public void setStartTime(CdaDate startTime) {
    this.startTime = startTime;
  }

  public Date getStartTime() {
    return startTime == null ? null : startTime.getDate();
  }

  public boolean startTimeHasTime() {
    return startTime == null ? false : startTime.dateHasTime();
  }

  public void setStartTime(Date startTime) {
    this.startTime = new CdaDate(startTime, true);
  }

  public void setStartDate(Date startTime) {
    this.startTime = new CdaDate(startTime, false);
  }

  public CdaDate getStopTimeCdaDate() {
    return stopTime;
  }

  public void setStopTime(CdaDate stopTime) {
    this.stopTime = stopTime;
  }

  public Date getStopTime() {
    return stopTime == null ? null : stopTime.getDate();
  }

  public boolean stopTimeHasTime() {
    return stopTime == null ? false : stopTime.dateHasTime();
  }

  public void setStopTime(Date stopTime) {
    this.stopTime = new CdaDate(stopTime, true);
  }

  public void setStopDate(Date stopTime) {
    this.stopTime = new CdaDate(stopTime, false);
  }

}
