package dk.s4.hl7.cda.convert;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.header.CDAHeaderHandler;
import dk.s4.hl7.cda.convert.decode.header.CustodianHandler;
import dk.s4.hl7.cda.convert.decode.header.v20.DocumentationOfHandler;
import dk.s4.hl7.cda.convert.decode.header.v20.PatientHandler;
import dk.s4.hl7.cda.convert.decode.pdc.v20.SectionHandler;
import dk.s4.hl7.cda.convert.decode.v20.ClinicalDocumentXmlConverter;
import dk.s4.hl7.cda.model.pdc.v20.PDCDocument;

/**
 * Parse PDC xml to GreenCDA model
 */
public class XmlPDCV20Converter extends ClinicalDocumentXmlConverter<PDCDocument> {

  @Override
  public PDCDocument convert(String source) {
    return deserialize(new StringReader(source));
  }

  @Override
  protected List<CDAXmlHandler<PDCDocument>> createParticipantHandlers() {
    List<CDAXmlHandler<PDCDocument>> list = new ArrayList<CDAXmlHandler<PDCDocument>>();
    list.add(new PatientHandler<PDCDocument>());
    list.add(createAuthorHandler());
    list.add(new CustodianHandler<PDCDocument>());
    list.add(new DocumentationOfHandler<PDCDocument>());
    return list;
  }

  @Override
  protected List<CDAXmlHandler<PDCDocument>> createSectionHandlers() {
    List<CDAXmlHandler<PDCDocument>> list = new ArrayList<CDAXmlHandler<PDCDocument>>();
    list.add(new SectionHandler());
    return list;
  }

  @Override
  protected PDCDocument createNewDocument(CDAHeaderHandler<PDCDocument> headerHandler) {
    return new PDCDocument(headerHandler.getDocumentId());
  }

}
