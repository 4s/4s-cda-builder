package dk.s4.hl7.cda.convert;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ClinicalDocumentXmlConverter;
import dk.s4.hl7.cda.convert.decode.cdametadata.MetaDataServiceEventCodeHandler;
import dk.s4.hl7.cda.convert.decode.cdametadata.SectionHandler;
import dk.s4.hl7.cda.convert.decode.header.CDAHeaderHandler;
import dk.s4.hl7.cda.convert.decode.header.DocumentationOfHandler;
import dk.s4.hl7.cda.convert.decode.header.PatientHandler;
import dk.s4.hl7.cda.model.cdametadata.CDAMetadata;

/**
 * Parse CDA metadata xml to GreenCDA model
 */
public class XmlCDAMetadataConverter extends ClinicalDocumentXmlConverter<CDAMetadata> {
  public CDAMetadata convert(String source) {
    return deserialize(new StringReader(source));
  }

  @Override
  protected List<CDAXmlHandler<CDAMetadata>> createParticipantHandlers() {
    List<CDAXmlHandler<CDAMetadata>> list = new ArrayList<CDAXmlHandler<CDAMetadata>>();
    list.add(createAuthorHandler());
    list.add(createLegalAuthenticatorHandler());
    list.add(new PatientHandler<CDAMetadata>());
    list.add(new DocumentationOfHandler<CDAMetadata>());
    return list;
  }

  @Override
  protected CDAMetadata createNewDocument(CDAHeaderHandler<CDAMetadata> headerHandler) {
    return new CDAMetadata(headerHandler.getDocumentId());
  }

  @Override
  protected List<CDAXmlHandler<CDAMetadata>> createSectionHandlers() {
    List<CDAXmlHandler<CDAMetadata>> list = new ArrayList<CDAXmlHandler<CDAMetadata>>();
    list.add(new SectionHandler());
    list.add(new MetaDataServiceEventCodeHandler());
    return list;
  }

}