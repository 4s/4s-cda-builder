package dk.s4.hl7.cda.convert.decode.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.IdHandler;
import dk.s4.hl7.cda.convert.decode.qfdd.precondition.PreconditionHandler;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer.QFDDOrganizerBuilder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class OrganizerHandler extends BaseXmlHandler {
  public static final String ORGANIZER_SECTION = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer";
  private ObservationHandler observationHandler;
  private IdHandler idHandler;
  private List<QFDDOrganizer> organizers;
  private PreconditionHandler preconditionHandler;
  private CodedValue code;
  private String introduction;

  public OrganizerHandler() {
    observationHandler = new ObservationHandler();
    idHandler = new IdHandler(ORGANIZER_SECTION);
    organizers = new ArrayList<QFDDOrganizer>();
    this.preconditionHandler = new PreconditionHandler(ORGANIZER_SECTION + "/precondition");
    addPath(ORGANIZER_SECTION);
    addPath(ORGANIZER_SECTION + "/code");
    addPath(ORGANIZER_SECTION + "/code/originalText");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code", "codeSystem")) {
      code = new CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "originalText")) {
      introduction = xmlElement.getElementValue();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "organizer")) {
      QFDDOrganizerBuilder qfddOrganizerBuilder = new QFDDOrganizerBuilder();
      qfddOrganizerBuilder.setIds(idHandler.getIds());
      for (QFDDPrecondition precondition : preconditionHandler.getPreconditions()) {
        qfddOrganizerBuilder.addPrecondition(precondition);
      }
      for (QFDDQuestion question : observationHandler.getQuestions()) {
        qfddOrganizerBuilder.addQFDDQuestion(question);
      }
      qfddOrganizerBuilder.setCode(code);
      qfddOrganizerBuilder.setIntroduction(introduction);
      organizers.add(qfddOrganizerBuilder.build());
      localClear();
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    observationHandler.addHandlerToMap(xmlMapping);
    idHandler.addHandlerToMap(xmlMapping);
    preconditionHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    observationHandler.removeHandlerFromMap(xmlMapping);
    idHandler.removeHandlerFromMap(xmlMapping);
    preconditionHandler.removeHandlerFromMap(xmlMapping);
  }

  private void localClear() {
    observationHandler.clear();
    idHandler.clear();
    preconditionHandler.clear();
    code = null;
    introduction = null;
  }

  public List<QFDDOrganizer> getOrganizers() {
    return organizers;
  }

  public void clear() {
    localClear();
    organizers = new ArrayList<QFDDOrganizer>();
  }
}
