package dk.s4.hl7.cda.model.pdc.v20;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.PersonIdentity;

/**
 * Custody information returned from the CPR register. This information can be
 * repeated as many times as needed.
 * <p>
 * This covers two scenarios, determined by the code element:
 * <p>
 * If code @code = “ChildCustody” this contains information about the child over
 * whom the citizen has custody.
 * <p>
 * If code @code = “CustodyBy” this contains information about the adults who
 * have custody over the citizen.
 *
 */
public class CustodyInformation {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;

  private CodedValue relationCode;
  private String cpr;
  private PersonIdentity personIdentity;

  public static class CustodyInformationBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;
    private CodedValue relationCode;
    private String cpr;
    private PersonIdentity personIdentity;

    public CustodyInformationBuilder(ID id) {
      this.id = id;
    }

    public CustodyInformationBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public CustodyInformationBuilder setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public CustodyInformationBuilder setRelationCode(CodedValue relationCode) {
      this.relationCode = relationCode;
      return this;
    }

    public CustodyInformationBuilder setCpr(String cpr) {
      this.cpr = cpr;
      return this;
    }

    public CustodyInformationBuilder setPersonIdentity(PersonIdentity personIdentity) {
      this.personIdentity = personIdentity;
      return this;
    }

    public CustodyInformation build() {

      return new CustodyInformation(this);
    }

  }

  private CustodyInformation(CustodyInformationBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.relationCode = builder.relationCode;
    this.cpr = builder.cpr;
    this.personIdentity = builder.personIdentity;
  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * The value of the code @code represents the nature of the custody
   * <p> 
   * If code @code = “ChildCustody” this contains information about the child over
   * whom the citizen has custody.
   * <p>
   * If code @code = “CustodyBy” this contains information about the adults who
   * have custody over the citizen.
   * 
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * The value of the relationCode represents the relation of the custody
   * <p>
   * The @code can be "mor", "far", "anden" and the @codeDisplayName the corresponding description
   * 
   * @return CodedValue Returns the relation Code of the custody.
   */
  public CodedValue getRelationCode() {
    return relationCode;
  }

  /**
   * The value of the cpr depends on the code @code value
   * <p>
   * If code @code = “ChildCustody” this contains cpr of the child
   * <p>
   * If code @code = “CustodyBy” this contains cpr of the adult
   * <p>
   * 
   * @return String Returns the cpr number
   */
  public String getCpr() {
    return cpr;
  }

  /**
   * The value of the name depends on the code @code value
   * <p>
   * If code @code = “ChildCustody” this contains name of the child
   * <p>
   * If code @code = “CustodyBy” this contains name of the adult
   * <p>
   * 
   * @return PersonIdentity Returns the name
   */
  public PersonIdentity getPersonIdentity() {
    return personIdentity;
  }

}
