package dk.s4.hl7.cda.convert.decode.general;

import dk.s4.hl7.cda.model.Patient.PatientBuilder;
import dk.s4.hl7.util.xml.XmlMapping;

public interface IPersonHandler {

  void addHandlerToMap(XmlMapping xmlMapping);

  void removeHandlerFromMap(XmlMapping xmlMapping);

  void addPatientInformation(PatientBuilder patientBuilder);

}
