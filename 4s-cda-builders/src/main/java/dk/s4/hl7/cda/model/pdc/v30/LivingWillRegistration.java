package dk.s4.hl7.cda.model.pdc.v30;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;

/**
 * Indicates whether the Citizen has registered a living will or not.
 */

public class LivingWillRegistration {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;
  private boolean isRegistered;

  public static class LivingWillRegistrationBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;
    private boolean isRegistered;

    public LivingWillRegistrationBuilder(ID id) {
      this.id = id;
    }

    public LivingWillRegistrationBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public LivingWillRegistrationBuilder setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public LivingWillRegistrationBuilder setRegisteret(boolean isRegistered) {
      this.isRegistered = isRegistered;
      return this;
    }

    public LivingWillRegistration build() {

      return new LivingWillRegistration(this);
    }

  }

  private LivingWillRegistration(LivingWillRegistrationBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.isRegistered = builder.isRegistered;

  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * @return boolean Returns if the citizen has a treatment will or not
   */
  public boolean isRegistered() {
    return isRegistered;
  }

}
