package dk.s4.hl7.cda.model.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.core.BaseClinicalDocument;

/**
 * Represents a QFDD document including sections, title, text and language code
 */
public class QFDDDocument extends BaseClinicalDocument {
  private List<Section<QFDDOrganizer>> sections;

  public QFDDDocument(ID id) {
    super(HL7.CDA_TYPEID_ROOT, HL7.CDA_TYPEID_EXTENSION, id, Loinc.QFD_CODE, Loinc.QFD_DISPLAYNAME,
        new String[] { MedCom.DK_QFD_ROOT_OID, MedCom.DK_QFD_ROOT_OID_LEVEL }, HL7.REALM_CODE_DK);
    this.sections = new ArrayList<Section<QFDDOrganizer>>();
  }

  public List<Section<QFDDOrganizer>> getSections() {
    return sections;
  }

  public void addSection(Section<QFDDOrganizer> section) {
    sections.add(section);
  }

}