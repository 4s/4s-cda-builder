package dk.s4.hl7.cda.convert.decode.cpd.v20;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.CodedValueHandler;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument;
import dk.s4.hl7.cda.model.cpd.v20.CPDGoalObservationComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDInterventionActComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDOutcomeObservationComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDSection;
import dk.s4.hl7.cda.model.cpd.v20.CPDSection.CPDSectionBuilder;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class SectionHandler implements CDAXmlHandler<CPDDocument> {

  public enum DocumentPart {
    None,
    HealthStatus,
    HealthStatusAct,
    Goal,
    Intervention,
    Outcome
  }

  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section";

  private DocumentPart currentDocumentPart;

  private String title;
  private RawTextHandler rawTextHandler;
  private CodedValueHandler codedValueHandler;
  private ComponentHandler componentHandler;

  private CPDSection<CPDHealthStatusComponent> healthConcernSection;
  private CPDSection<CPDGoalObservationComponent> goalSection;
  private CPDSection<CPDInterventionActComponent> interventionActSection;
  private CPDSection<CPDOutcomeObservationComponent> outcomeObservationSection;

  public SectionHandler() {
    rawTextHandler = new RawTextHandler(COMPONENT_SECTION + "/text", "text");
    codedValueHandler = new CodedValueHandler(COMPONENT_SECTION, false);
    componentHandler = new ComponentHandler();
  }

  public void clear() {
    clearLocal();
    healthConcernSection = null;
    goalSection = null;
    interventionActSection = null;
    outcomeObservationSection = null;
  }

  private void clearLocal() {
    rawTextHandler.clear();
    codedValueHandler.clear();
    title = null;
    currentDocumentPart = DocumentPart.None;
    componentHandler.clear();
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      this.setDocumentType(xmlElement);
    } else

    if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "title")) {
      title = xmlElement.getElementValue();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase("section")) {
      if (currentDocumentPart != null) {
        if (currentDocumentPart.equals(DocumentPart.HealthStatus)) {
          healthConcernSection = new CPDSectionBuilder<CPDHealthStatusComponent>()
              .setTitle(title)
              .setText(rawTextHandler.getRawText())
              .setComponent(componentHandler.getCPDHealthStatusObservationComponent())
              .build();
        } else if (currentDocumentPart.equals(DocumentPart.Goal)) {
          goalSection = new CPDSectionBuilder<CPDGoalObservationComponent>()
              .setTitle(title)
              .setText(rawTextHandler.getRawText())
              .setComponent(componentHandler.getCPDGoalObservationComponent())
              .build();
        } else if (currentDocumentPart.equals(DocumentPart.Intervention)) {
          interventionActSection = new CPDSectionBuilder<CPDInterventionActComponent>()
              .setTitle(title)
              .setText(rawTextHandler.getRawText())
              .setComponent(componentHandler.getCPDInterventionActComponent())
              .build();
        } else if (currentDocumentPart.equals(DocumentPart.Outcome)) {
          outcomeObservationSection = new CPDSectionBuilder<CPDOutcomeObservationComponent>()
              .setTitle(title)
              .setText(rawTextHandler.getRawText())
              .setComponent(componentHandler.getCPDOutcomeObservationComponent())
              .build();
        }
      }
      clearLocal();
    }
  }

  private void setDocumentType(XMLElement xmlElement) {
    if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_HEALTH_CONCERN_OID)) {
      currentDocumentPart = DocumentPart.HealthStatus;
    } else if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_GOAL_OID)) {
      currentDocumentPart = DocumentPart.Goal;
    } else if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_INTERVENTION_OID)) {
      currentDocumentPart = DocumentPart.Intervention;
    } else if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_OUTCOME_OID)) {
      currentDocumentPart = DocumentPart.Outcome;
    } else {
      currentDocumentPart = DocumentPart.None;
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add(COMPONENT_SECTION, this);
    xmlMapping.add(COMPONENT_SECTION + "/title", this);
    xmlMapping.add(COMPONENT_SECTION + "/templateId", this);
    rawTextHandler.addHandlerToMap(xmlMapping);
    codedValueHandler.addHandlerToMap(xmlMapping);
    componentHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove(COMPONENT_SECTION);
    xmlMapping.remove(COMPONENT_SECTION + "/title");
    xmlMapping.remove(COMPONENT_SECTION + "/templateId");
    rawTextHandler.removeHandlerFromMap(xmlMapping);
    codedValueHandler.removeHandlerFromMap(xmlMapping);
    componentHandler.removeHandlerFromMap(xmlMapping);
  }

  @Override
  public void addDataToDocument(CPDDocument clinicalDocument) {
    clinicalDocument.setHealthConcernSection(healthConcernSection);
    clinicalDocument.setGoalSection(goalSection);
    clinicalDocument.setInterventionSection(interventionActSection);
    clinicalDocument.setOutcomeSection(outcomeObservationSection);
    clear();
  }
}
