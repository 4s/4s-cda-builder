package dk.s4.hl7.cda.convert.encode.narrative.text;

import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;

public class QRDXMLNarrativeTextConverter implements NarrativeTextConverter<Section<QRDOrganizer>> {

  public String createText(Section<QRDOrganizer> documentSection) {
    return documentSection.getText();
  }

}
