package dk.s4.hl7.cda.convert;

import java.io.Reader;

import dk.s4.hl7.cda.convert.base.AppendableSerializer;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.convert.base.ReaderSerializer;
import dk.s4.hl7.cda.convert.decode.CdaBuilderException;
import dk.s4.hl7.cda.model.pdc.v30.PDCDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;

/**
 * The implementation of Codec to decode PDC XML to objects. After construction the codec is considered thread-safe.
 * 
 * @see <a href="https://www.dartlang.org/articles/libraries/converters-and-codecs">https://www.dartlang.org/articles/libraries/converters-and-codecs</a>
 * 
 */
public class PDCV30XmlCodec
    implements Codec<PDCDocument, String>, AppendableSerializer<PDCDocument>, ReaderSerializer<PDCDocument> {
  private XmlPDCV30Converter xmlPdcConverter;

  public PDCV30XmlCodec() {
    this(new XmlPrettyPrinter());
  }

  public PDCV30XmlCodec(XmlPrettyPrinter xmlPrettyPrinter) {
    this.xmlPdcConverter = new XmlPDCV30Converter();
  }

  public String encode(PDCDocument source) {
    throw new CdaBuilderException("encode is not available for PDC documents");
  }

  public PDCDocument decode(String source) {
    return xmlPdcConverter.convert(source);
  }

  @Override
  public void serialize(PDCDocument source, Appendable appendable) {
    throw new CdaBuilderException("Serialize is not available for PDC documents");

  }

  @Override
  public PDCDocument deserialize(Reader source) {
    return xmlPdcConverter.deserialize(source);
  }
}
