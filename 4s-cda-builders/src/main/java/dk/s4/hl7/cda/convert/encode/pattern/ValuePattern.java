package dk.s4.hl7.cda.convert.encode.pattern;

import java.io.IOException;

import dk.s4.hl7.cda.model.CdaMeasurement;
import dk.s4.hl7.cda.model.CdaMeasurementInterval;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

public class ValuePattern {

  public void buildValueIVL_PQ(CdaMeasurementInterval cdaMeasurementInterval, XmlStreamBuilder xmlBuilder)
      throws IOException {
    xmlBuilder.element("value").attribute("xsi:type", "IVL_PQ");
    if (cdaMeasurementInterval.getMeasurementLow() != null
        && cdaMeasurementInterval.getMeasurementLow().getValue() != null) {
      if (cdaMeasurementInterval.getMeasurementLow().getUnit() == null) {
        buildValueInterval("low", cdaMeasurementInterval.getMeasurementLow().getValue(),
            cdaMeasurementInterval.isInclusiveLow(), xmlBuilder);
      } else {
        buildValueInterval("low", cdaMeasurementInterval.getMeasurementLow().getValue(),
            cdaMeasurementInterval.isInclusiveLow(), cdaMeasurementInterval.getMeasurementLow().getUnit(), xmlBuilder);
      }
    } else {
      xmlBuilder.element("low").attribute("nullFlavor", "NA").elementShortEnd();
    }
    if (cdaMeasurementInterval.getMeasurementHigh() != null
        && cdaMeasurementInterval.getMeasurementHigh().getValue() != null) {
      if (cdaMeasurementInterval.getMeasurementHigh().getUnit() == null) {
        buildValueInterval("high", cdaMeasurementInterval.getMeasurementHigh().getValue(),
            cdaMeasurementInterval.isInclusiveHigh(), xmlBuilder);
      } else {
        buildValueInterval("high", cdaMeasurementInterval.getMeasurementHigh().getValue(),
            cdaMeasurementInterval.isInclusiveHigh(), cdaMeasurementInterval.getMeasurementHigh().getUnit(),
            xmlBuilder);
      }
    } else {
      xmlBuilder.element("high").attribute("nullFlavor", "NA").elementShortEnd();
    }
    xmlBuilder.elementEnd(); // end value
  }

  private void buildValueInterval(String elementName, String value, boolean isInclusive, XmlStreamBuilder xmlBuilder)
      throws IOException {
    xmlBuilder
        .element(elementName)
        .attribute("value", value)
        .attribute("inclusive", isInclusive ? "true" : "false")
        .elementShortEnd();
  }

  private void buildValueInterval(String elementName, String value, boolean isInclusive, String unit,
      XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder
        .element(elementName)
        .attribute("value", value)
        .attribute("inclusive", isInclusive ? "true" : "false")
        .attribute("unit", unit)
        .elementShortEnd();
  }

  public void buildValuePQ(CdaMeasurement cdaMeasurement, XmlStreamBuilder xmlBuilder) throws IOException {
    if (cdaMeasurement.getValue() != null && cdaMeasurement.getUnit() != null) {
      xmlBuilder
          .element("value")
          .attribute("xsi:type", "PQ")
          .attribute("value", cdaMeasurement.getValue())
          .attribute("unit", cdaMeasurement.getUnit())
          .elementShortEnd();
    } else {
      xmlBuilder.element("value").attribute("xsi:type", "PQ").attribute("nullFlavor", "NA").elementShortEnd();
    }
  }

  public void buildValueST(String value, XmlStreamBuilder xmlBuilder) throws IOException {
    if (value != null) {
      xmlBuilder.element("value").attribute("xsi:type", "ST").value(value).elementEnd();
    } else {
      xmlBuilder.element("value").attribute("xsi:type", "ST").attribute("nullFlavor", "NA").elementShortEnd();
    }
  }

  public void buildValueCD(CodedValue codedValue, XmlStreamBuilder xmlBuilder) throws IOException {
    if (codedValue != null) {
      xmlBuilder
          .element("value")
          .attribute("code", codedValue.getCode())
          .attribute("codeSystem", codedValue.getCodeSystem())
          .attribute("codeSystemName", codedValue.getCodeSystemName())
          .attribute("displayName", codedValue.getDisplayName())
          .attribute("xsi:type", "CD")
          .elementShortEnd();
      ;
    }
  }

}
