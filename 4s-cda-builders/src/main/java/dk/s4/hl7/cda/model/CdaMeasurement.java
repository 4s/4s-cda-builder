package dk.s4.hl7.cda.model;

public class CdaMeasurement {

  private String value;
  private String unit;

  public CdaMeasurement(String value, String unit) {
    this.value = value;
    this.unit = unit;
  }

  public String getValue() {
    return value;
  }

  public String getUnit() {
    return unit;
  }

}
