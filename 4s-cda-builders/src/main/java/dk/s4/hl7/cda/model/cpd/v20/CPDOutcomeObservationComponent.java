package dk.s4.hl7.cda.model.cpd.v20;

import java.util.ArrayList;
import java.util.List;

public class CPDOutcomeObservationComponent {

  private String title;
  private String text;
  private List<CPDOutcomeObservationEntry> entryList;

  private CPDOutcomeObservationComponent(CPDOutcomeObservationComponentBuilder builder) {
    this.entryList = builder.entryList;
    this.title = builder.title;
    this.text = builder.text;
  }

  public static class CPDOutcomeObservationComponentBuilder {

    private String title;
    private String text;
    private List<CPDOutcomeObservationEntry> entryList;

    public CPDOutcomeObservationComponentBuilder() {
      this.entryList = new ArrayList<CPDOutcomeObservationEntry>();
    }

    public CPDOutcomeObservationComponentBuilder setTitle(String title) {
      this.title = title;
      return this;
    }

    public CPDOutcomeObservationComponentBuilder setText(String text) {
      this.text = text;
      return this;
    }

    public CPDOutcomeObservationComponentBuilder setEntryList(List<CPDOutcomeObservationEntry> entryList) {
      this.entryList = entryList;
      return this;
    }

    public CPDOutcomeObservationComponentBuilder addEntry(CPDOutcomeObservationEntry entry) {
      this.entryList.add(entry);
      return this;
    }

    public CPDOutcomeObservationComponent build() {
      return new CPDOutcomeObservationComponent(this);
    }
  }

  public List<CPDOutcomeObservationEntry> getEntryList() {
    return entryList;
  }

  public String getTitle() {
    return title;
  }

  public String getText() {
    return text;
  }
}
