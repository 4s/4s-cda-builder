package dk.s4.hl7.cda.model.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CdaBuilderException;
import dk.s4.hl7.cda.model.ID;

public class QFDDPreconditionGroup {
  private QFDDPrecondition[] preconditions;
  private ID id;
  private GroupType groupType;

  // The names of these enums must be exactly as specified in the QFDD
  // specification with the first letter in upper case.
  // Otherwise the toXmlRepresentation() will generate wrong xml element names
  public enum GroupType {
    AllTrue("2.16.840.1.113883.10.20.32.4.13"),
    AllFalse("2.16.840.1.113883.10.20.32.4.14"),
    AtLeastOneTrue("2.16.840.1.113883.10.20.32.4.15"),
    AtLeastOneFalse("2.16.840.1.113883.10.20.32.4.16"),
    OnlyOneTrue("2.16.840.1.113883.10.20.32.4.17"),
    OnlyOneFalse("2.16.840.1.113883.10.20.32.4.18");

    private String templateId;

    private GroupType(String templateId) {
      this.templateId = templateId;
    }

    public String getTemplateId() {
      return templateId;
    }

    public String toXmlElementName() {
      StringBuilder builder = new StringBuilder(this.name());
      builder.setCharAt(0, Character.toLowerCase(builder.charAt(0)));
      return builder.toString();
    }
  }

  public QFDDPreconditionGroup(QFDDPreconditionGroupBuilder preconditionGroupBuilder) {
    this.preconditions = new QFDDPrecondition[preconditionGroupBuilder.preconditions.size()];
    preconditionGroupBuilder.preconditions.toArray(this.preconditions);
    this.id = preconditionGroupBuilder.id;
    this.groupType = preconditionGroupBuilder.groupType;
  }

  public QFDDPrecondition[] getPreconditions() {
    return preconditions;
  }

  public ID getId() {
    return id;
  }

  public GroupType getGroupType() {
    return groupType;
  }

  public static class QFDDPreconditionGroupBuilder {
    private List<QFDDPrecondition> preconditions;
    private ID id;
    private GroupType groupType;

    public QFDDPreconditionGroupBuilder() {
      this.preconditions = new ArrayList<QFDDPrecondition>();
    }

    public QFDDPreconditionGroupBuilder setGroupType(GroupType groupType) {
      this.groupType = groupType;
      return this;
    }

    public QFDDPreconditionGroupBuilder setId(ID id) {
      this.id = id;
      return this;
    }

    public QFDDPreconditionGroupBuilder addPrecondition(QFDDPrecondition precondition) {
      preconditions.add(precondition);
      return this;
    }

    public QFDDPreconditionGroup build() {
      if (groupType == null) {
        throw new CdaBuilderException("The group type must be specified");
      }
      if (id == null) {
        throw new CdaBuilderException("The id type must be specified");
      }
      if (preconditions.isEmpty()) {
        throw new CdaBuilderException("At least one precondition must be specified");
      }
      return new QFDDPreconditionGroup(this);
    }
  }
}
