package dk.s4.hl7.cda.convert.decode.pdc.v30;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.model.pdc.v30.PDCDocument;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlHandler;
import dk.s4.hl7.util.xml.XmlMapping;

public class SectionHandler implements CDAXmlHandler<PDCDocument> {

  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section";

  private RawTextHandler textHandler;
  private ObservationHandler observationHandler;

  private XmlHandler currentHandler;

  public SectionHandler() {
    textHandler = new RawTextHandler(COMPONENT_SECTION + "/text", "text");
    observationHandler = new ObservationHandler();
    currentHandler = null;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {

  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase("encounter")) {
      currentHandler.removeHandlerFromMap(xmlMapping);
      currentHandler = null;
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    textHandler.addHandlerToMap(xmlMapping);
    observationHandler.addHandlerToMap(xmlMapping);

    xmlMapping.add(COMPONENT_SECTION, this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    textHandler.removeHandlerFromMap(xmlMapping);
    observationHandler.removeHandlerFromMap(xmlMapping);
    xmlMapping.remove(COMPONENT_SECTION);
  }

  @Override
  public void addDataToDocument(PDCDocument clinicalDocument) {

    clinicalDocument.setText(textHandler.getRawText());
    clinicalDocument.getCustodianIdentity();
    clinicalDocument.setCustodyInformationList(observationHandler.getCustodyInformationList());
    clinicalDocument.setNameAndAddressInformation(observationHandler.getNameAndAddressInformation());
    clinicalDocument.setCoverageGroup(observationHandler.getCoverageGroup());
    clinicalDocument.setOrganDonorRegistration(observationHandler.getOrganDonorRegistration());
    clinicalDocument.setTreatmentWillRegistration(observationHandler.getTreatmentWillRegistration());
    clinicalDocument.setLivingWillRegistration(observationHandler.getLivingWillRegistration());
    clinicalDocument.setNoResuscitationRegistration(observationHandler.getNoResuscitationRegistration());
    clinicalDocument.setManuallyEnteredSpokenLanguage(observationHandler.getManuallyEnteredSpokenLanguage());
    clinicalDocument.setManuallyEnteredTemporaryAddress(observationHandler.getManuallyEnteredTemporaryAddress());
    clinicalDocument.setManuallyEnteredDentistInformation(observationHandler.getManuallyEnteredDentistInformation());
    clinicalDocument.setManuallyEnteredContactInformation(observationHandler.getManuallyEnteredContactInformation());
    clinicalDocument.setManuallyEnteredInformationAboutRelativesList(
        observationHandler.getManuallyEnteredInformationAboutRelativesList());

  }

}
