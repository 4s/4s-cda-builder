package dk.s4.hl7.cda.model.pdc.v30;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.TelecomPhone;

/**
 * Manually entered information about the citizen’s dentist.
 */

public class ManuallyEnteredDentistInformation {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;
  private TelecomPhone[] telecomPhone;

  private ID identification;
  private PersonIdentity personIdentity;
  private String practiceName;
  private AddressData addressData;

  public static class ManuallyEnteredDentistInformationBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;

    private ID identification;
    private PersonIdentity personIdentity;
    private String practiceName;
    private AddressData addressData;

    private TelecomPhone[] telecomPhone;
    private List<TelecomPhone> telecomPhoneTemporary;

    public ManuallyEnteredDentistInformationBuilder(ID id) {
      telecomPhoneTemporary = new ArrayList<TelecomPhone>();
      this.id = id;
    }

    public ManuallyEnteredDentistInformationBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public ManuallyEnteredDentistInformationBuilder setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public ManuallyEnteredDentistInformationBuilder setIdentification(ID identification) {
      this.identification = identification;
      return this;
    }

    public ManuallyEnteredDentistInformationBuilder setPersonIdentity(PersonIdentity personIdentity) {
      this.personIdentity = personIdentity;
      return this;
    }

    public ManuallyEnteredDentistInformationBuilder setPracticeName(String practiceName) {
      this.practiceName = practiceName;
      return this;
    }

    public ManuallyEnteredDentistInformationBuilder setAddressData(AddressData addressData) {
      this.addressData = addressData;
      return this;
    }

    public ManuallyEnteredDentistInformationBuilder setTelecomPhone(TelecomPhone[] telecom) {
      this.telecomPhone = telecom;
      return this;
    }

    public ManuallyEnteredDentistInformation build() {
      for (int i = 0; i < telecomPhone.length; i++) {
        telecomPhoneTemporary.add(telecomPhone[i]);
      }
      if (telecomPhoneTemporary.size() > 0) {
        telecomPhone = new TelecomPhone[telecomPhoneTemporary.size()];
        telecomPhoneTemporary.toArray(telecomPhone);
      } else {
        telecomPhone = new TelecomPhone[0];
      }

      return new ManuallyEnteredDentistInformation(this);
    }

  }

  private ManuallyEnteredDentistInformation(ManuallyEnteredDentistInformationBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.identification = builder.identification;
    this.personIdentity = builder.personIdentity;
    this.practiceName = builder.practiceName;
    this.addressData = builder.addressData;
    this.telecomPhone = builder.telecomPhone;

  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * The identification of the Dentist
   * <p>
   * The @root indicates the type of identification. If value is "1.2.208.176.1.4"
   * the id is based on ydernummer. If value is "1.2.208.176.1.1" the id is based
   * on SOR-kode
   * 
   * @return ID Returns the identification of the Dentist
   */
  public ID getIdentification() {
    return identification;
  }

  /**
   * @return PersonIdentity Returns the Dentists name
   */
  public PersonIdentity getPersonIdentity() {
    return personIdentity;
  }

  /**
   * @return OrgName Returns the Dentist
   */
  public String getPracticeName() {
    return practiceName;
  }

  /**
   * @return AddressData Returns the Dentists address
   */
  public AddressData getAddressData() {
    return addressData;
  }

  /**
   * @return TelecomPhone Returns an array of Dentists phonenumbers
   */
  public TelecomPhone[] getTelecomPhone() {
    return telecomPhone;
  }

}
