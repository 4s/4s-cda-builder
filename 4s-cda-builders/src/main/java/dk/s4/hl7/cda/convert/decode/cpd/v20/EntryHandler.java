package dk.s4.hl7.cda.convert.decode.cpd.v20;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.cpd.v20.SectionHandler.DocumentPart;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.CodedValueHandler;
import dk.s4.hl7.cda.convert.decode.general.ValueHandler;
import dk.s4.hl7.cda.convert.decode.header.ParticipantHandler;
import dk.s4.hl7.cda.convert.decode.header.ParticipantHandler.ParticipantType;
import dk.s4.hl7.cda.model.CdaDate;
import dk.s4.hl7.cda.model.CdaDateInterval;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument.Status;
import dk.s4.hl7.cda.model.cpd.v20.CPDGoalObservationEntry;
import dk.s4.hl7.cda.model.cpd.v20.CPDGoalObservationEntry.CPDGoalObservationEntryBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusActEntry;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusActEntry.CPDHealthStatusActEntryBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDInterventionActEntry;
import dk.s4.hl7.cda.model.cpd.v20.CPDInterventionActEntry.CPDInterventionActEntryBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDOutcomeObservationEntry;
import dk.s4.hl7.cda.model.cpd.v20.CPDOutcomeObservationEntry.CPDOutcomeObservationEntryBuilder;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class EntryHandler extends BaseXmlHandler {

  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section/component/section/entry";
  public static final String COMPONENT_SECTION_OBSERVATION = "/ClinicalDocument/component/structuredBody/component/section/component/section/entry/observation";
  public static final String COMPONENT_SECTION_ACT = "/ClinicalDocument/component/structuredBody/component/section/component/section/entry/act";
  public static final String COMPONENT_SECTION_ENCOUNTER = "/ClinicalDocument/component/structuredBody/component/section/component/section/entry/encounter";

  private RawTextHandler rawTextHandlerObservation;
  private RawTextHandler rawTextHandlerEncounter;
  private CodedValueHandler codedValueHandlerAct;
  private CodedValueHandler codedValueHandlerObservation;
  private CodedValueHandler codedValueHandlerEncounter;
  private EntryRelationshipHandler entryRelationshipHandler;
  private ValueHandler valueHandlerObservation;

  private ParticipantHandler<CPDDocument> authorActHandler;
  private ParticipantHandler<CPDDocument> authorObservationHandler;
  private ParticipantHandler<CPDDocument> performerEncounterHandler;

  private DocumentPart currentDocumentPart;
  private ID id;
  private Status statusCode;
  private CdaDate effectiveTime;
  private CdaDateInterval effectiveTimeInterval;

  private List<CPDHealthStatusActEntry> cpdHealthStatusActEntryList;
  private List<CPDGoalObservationEntry> cpdGoalObservationEntryList;
  private List<CPDInterventionActEntry> cpdInterventionActEntryList;
  private List<CPDOutcomeObservationEntry> cpdOutcomeObservationEntryList;

  public EntryHandler() {
    codedValueHandlerAct = new CodedValueHandler(COMPONENT_SECTION_ACT, true);
    codedValueHandlerObservation = new CodedValueHandler(COMPONENT_SECTION_OBSERVATION, true);
    codedValueHandlerEncounter = new CodedValueHandler(COMPONENT_SECTION_ENCOUNTER, true);
    rawTextHandlerObservation = new RawTextHandler(COMPONENT_SECTION_OBSERVATION + "/text", "text");
    rawTextHandlerEncounter = new RawTextHandler(COMPONENT_SECTION_ENCOUNTER + "/text", "text");
    entryRelationshipHandler = new EntryRelationshipHandler(COMPONENT_SECTION_ACT);
    valueHandlerObservation = new ValueHandler(COMPONENT_SECTION_OBSERVATION, true);

    authorActHandler = new ParticipantHandler<CPDDocument>(ParticipantType.PARTICIPANT,
        COMPONENT_SECTION_ACT + "/author/assignedAuthor", COMPONENT_SECTION_ACT + "/author/time", "assignedPerson",
        "/representedOrganization");
    authorObservationHandler = new ParticipantHandler<CPDDocument>(ParticipantType.PARTICIPANT,
        COMPONENT_SECTION_OBSERVATION + "/author/assignedAuthor", COMPONENT_SECTION_OBSERVATION + "/author/time",
        "assignedPerson", "/representedOrganization");
    performerEncounterHandler = new ParticipantHandler<CPDDocument>(ParticipantType.PARTICIPANT,
        COMPONENT_SECTION_ENCOUNTER + "/performer/assignedEntity", COMPONENT_SECTION_ENCOUNTER + "/performer/time",
        "assignedPerson", "/representedOrganization");
    this.initLists();
  }

  public void clear() {
    clearLocal();
    this.initLists();
  }

  private void clearLocal() {
    this.currentDocumentPart = DocumentPart.None;
    this.id = null;
    this.statusCode = null;
    this.effectiveTime = null;
    this.effectiveTimeInterval = null;
    rawTextHandlerObservation.clear();
    rawTextHandlerEncounter.clear();
    codedValueHandlerAct.clear();
    codedValueHandlerObservation.clear();
    codedValueHandlerEncounter.clear();
    entryRelationshipHandler.clear();
    valueHandlerObservation.clear();
    authorActHandler.clear();
    authorObservationHandler.clear();
    performerEncounterHandler.clear();
  }

  private void initLists() {
    this.cpdHealthStatusActEntryList = new ArrayList<CPDHealthStatusActEntry>();
    this.cpdGoalObservationEntryList = new ArrayList<CPDGoalObservationEntry>();
    this.cpdInterventionActEntryList = new ArrayList<CPDInterventionActEntry>();
    this.cpdOutcomeObservationEntryList = new ArrayList<CPDOutcomeObservationEntry>();
  }

  private void checkResetEffectiveTimeInterval() {
    if (effectiveTimeInterval == null) {
      effectiveTimeInterval = new CdaDateInterval();
    }
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      this.setDocumentType(xmlElement);
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "root", "extension")) {
      this.id = new IDBuilder()
          .setRoot(xmlElement.getAttributeValue("root"))
          .setExtension(xmlElement.getAttributeValue("extension"))
          .build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "root")) {
      this.id = new IDBuilder().setRoot(xmlElement.getAttributeValue("root")).build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "statusCode", "code")) {
      this.statusCode = getStatus(xmlElement.getAttributeValue("code"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "effectiveTime", "value")) {
      this.effectiveTime = new CdaDate(xmlElement.getAttributeValue("value"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "low", "value")) {
      checkResetEffectiveTimeInterval();
      effectiveTimeInterval.setStartTime(new CdaDate(xmlElement.getAttributeValue("value")));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "high", "value")) {
      checkResetEffectiveTimeInterval();
      effectiveTimeInterval.setStopTime(new CdaDate(xmlElement.getAttributeValue("value")));
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase("entry")) {
      if (currentDocumentPart != null) {
        if (currentDocumentPart.equals(DocumentPart.HealthStatusAct)) {
          cpdHealthStatusActEntryList.add(createCPDHealthStatusActEntry());
        } else if (currentDocumentPart.equals(DocumentPart.Goal)) {
          cpdGoalObservationEntryList.add(createCPDGoalObservationEntry());
        } else if (currentDocumentPart.equals(DocumentPart.Intervention)) {
          cpdInterventionActEntryList.add(createCPDInterventionActEntry());
        } else if (currentDocumentPart.equals(DocumentPart.Outcome)) {
          cpdOutcomeObservationEntryList.add(createCPDOutcomeObservationEntry());
        }
      }
      clearLocal();
    }
  }

  private void setDocumentType(XMLElement xmlElement) {
    if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_HEALTH_CONCERN_ACT_OID)) {
      currentDocumentPart = DocumentPart.HealthStatusAct;
    } else if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_GOAL_OBSERVATION_OID)) {
      currentDocumentPart = DocumentPart.Goal;
    } else if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_INTERVENTION_ACT_OID)) {
      currentDocumentPart = DocumentPart.Intervention;
    } else if (xmlElement.getAttributeValue("root").equals(HL7.CPD_SECTION_OUTCOME_OBSERVATION_OID)) {
      currentDocumentPart = DocumentPart.Outcome;
    } else {
      currentDocumentPart = DocumentPart.None;
    }
  }

  private Status getStatus(String statusCode) {
    if (Status.COMPLETED.toString().equalsIgnoreCase(statusCode)) {
      return Status.COMPLETED;
    } else if (Status.ACTIVE.toString().equalsIgnoreCase(statusCode)) {
      return Status.ACTIVE;
    } else {
      return null;
    }
  }

  private CPDHealthStatusActEntry createCPDHealthStatusActEntry() {
    return new CPDHealthStatusActEntryBuilder()
        .setId(this.id)
        .setCode(this.codedValueHandlerAct.getCodedValue())
        .setStatusCode(statusCode)
        .setEffectiveTime(effectiveTime)
        .setEffectiveTimeInterval(effectiveTimeInterval)
        .setAuthorList(authorActHandler.getParticipants())
        .setObservationList(entryRelationshipHandler.getObservationList())
        .build();
  }

  private CPDGoalObservationEntry createCPDGoalObservationEntry() {
    return new CPDGoalObservationEntryBuilder()
        .setId(this.id)
        .setCode(this.codedValueHandlerObservation.getCodedValue())
        .setEffectiveTime(effectiveTime)
        .setEffectiveTimeInterval(effectiveTimeInterval)
        .setText(this.rawTextHandlerObservation.getRawText())
        .setStatusCode(statusCode)
        .setCdaMeasurement(valueHandlerObservation.getCdaMeasurement())
        .setCdaMeasurementInterval(valueHandlerObservation.getCdaMeasurementInterval())
        .setComment(valueHandlerObservation.getString())
        .setAuthorList(authorObservationHandler.getParticipants())
        .build();
  }

  private CPDInterventionActEntry createCPDInterventionActEntry() {
    return new CPDInterventionActEntryBuilder()
        .setId(this.id)
        .setCode(this.codedValueHandlerEncounter.getCodedValue())
        .setText(this.rawTextHandlerEncounter.getRawText())
        .setStatusCode(statusCode)
        .setEffectiveTime(effectiveTime)
        .setEffectiveTimeInterval(effectiveTimeInterval)
        .setPerformerList(performerEncounterHandler.getParticipants())
        .build();
  }

  private CPDOutcomeObservationEntry createCPDOutcomeObservationEntry() {
    return new CPDOutcomeObservationEntryBuilder()
        .setId(this.id)
        .setCode(this.codedValueHandlerObservation.getCodedValue())
        .setStatusCode(statusCode)
        .setEffectiveTime(effectiveTime)
        .setEffectiveTimeInterval(effectiveTimeInterval)
        .setCdaMeasurement(valueHandlerObservation.getCdaMeasurement())
        .setCdaMeasurementInterval(valueHandlerObservation.getCdaMeasurementInterval())
        .setComment(valueHandlerObservation.getString())
        .setAuthorList(authorObservationHandler.getParticipants())
        .build();
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add(COMPONENT_SECTION, this);

    this.addSubHandlers(xmlMapping, "");
    this.addSubHandlers(xmlMapping, "/templateId");
    this.addSubHandlers(xmlMapping, "/id");
    this.addSubHandlers(xmlMapping, "/statusCode");
    this.addSubHandlers(xmlMapping, "/effectiveTime");
    this.addSubHandlers(xmlMapping, "/effectiveTime/low");
    this.addSubHandlers(xmlMapping, "/effectiveTime/high");
    rawTextHandlerObservation.addHandlerToMap(xmlMapping);
    rawTextHandlerEncounter.addHandlerToMap(xmlMapping);
    codedValueHandlerAct.addHandlerToMap(xmlMapping);
    codedValueHandlerObservation.addHandlerToMap(xmlMapping);
    codedValueHandlerEncounter.addHandlerToMap(xmlMapping);
    entryRelationshipHandler.addHandlerToMap(xmlMapping);
    valueHandlerObservation.addHandlerToMap(xmlMapping);
    authorActHandler.addHandlerToMap(xmlMapping);
    authorObservationHandler.addHandlerToMap(xmlMapping);
    performerEncounterHandler.addHandlerToMap(xmlMapping);
  }

  private void addSubHandlers(XmlMapping xmlMapping, String suffix) {
    xmlMapping.add(COMPONENT_SECTION_OBSERVATION + suffix, this);
    xmlMapping.add(COMPONENT_SECTION_ACT + suffix, this);
    xmlMapping.add(COMPONENT_SECTION_ENCOUNTER + suffix, this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove(COMPONENT_SECTION);
    this.removeSubHandlers(xmlMapping, "");
    this.removeSubHandlers(xmlMapping, "/templateId");
    this.removeSubHandlers(xmlMapping, "/id");
    this.removeSubHandlers(xmlMapping, "/statusCode");
    this.removeSubHandlers(xmlMapping, "/effectiveTime");
    this.removeSubHandlers(xmlMapping, "/effectiveTime/low");
    this.removeSubHandlers(xmlMapping, "/effectiveTime/high");
    rawTextHandlerObservation.removeHandlerFromMap(xmlMapping);
    rawTextHandlerEncounter.removeHandlerFromMap(xmlMapping);
    codedValueHandlerAct.removeHandlerFromMap(xmlMapping);
    codedValueHandlerObservation.removeHandlerFromMap(xmlMapping);
    codedValueHandlerEncounter.removeHandlerFromMap(xmlMapping);
    entryRelationshipHandler.removeHandlerFromMap(xmlMapping);
    valueHandlerObservation.removeHandlerFromMap(xmlMapping);
    authorActHandler.removeHandlerFromMap(xmlMapping);
    authorObservationHandler.removeHandlerFromMap(xmlMapping);
    performerEncounterHandler.removeHandlerFromMap(xmlMapping);
  }

  private void removeSubHandlers(XmlMapping xmlMapping, String suffix) {
    xmlMapping.remove(COMPONENT_SECTION_OBSERVATION + suffix);
    xmlMapping.remove(COMPONENT_SECTION_ACT + suffix);
    xmlMapping.remove(COMPONENT_SECTION_ENCOUNTER + suffix);
  }

  public List<CPDHealthStatusActEntry> getCPDHealthStatusActEntryList() {
    return cpdHealthStatusActEntryList;
  }

  public List<CPDGoalObservationEntry> getCPDGoalObservationEntryList() {
    return cpdGoalObservationEntryList;
  }

  public List<CPDInterventionActEntry> getCPDInterventionActEntryList() {
    return cpdInterventionActEntryList;
  }

  public List<CPDOutcomeObservationEntry> getCPDOutcomeObservationEntryList() {
    return cpdOutcomeObservationEntryList;
  }
}
