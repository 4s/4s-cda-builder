package dk.s4.hl7.cda.convert.decode.header;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.OrganizationHandler;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class CustodianHandler<C extends ClinicalDocument> implements CDAXmlHandler<C> {
  private static final String CUSTODIAN_PATH = "/ClinicalDocument/custodian/assignedCustodian/representedCustodianOrganization";

  private OrganizationHandler organizationHandler;
  private boolean hasCustodian;

  public CustodianHandler() {
    this.organizationHandler = new OrganizationHandler(CUSTODIAN_PATH);
    hasCustodian = false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "representedCustodianOrganization")) {
      hasCustodian = true;
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add(CUSTODIAN_PATH, this);
    organizationHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void addDataToDocument(ClinicalDocument clinicalDocument) {
    if (hasCustodian) {
      clinicalDocument.setCustodian(organizationHandler.getOrganization());
    }
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove(CUSTODIAN_PATH);
    organizationHandler.removeHandlerFromMap(xmlMapping);
  }
}