package dk.s4.hl7.cda.model.cpd.v20;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.model.CdaDate;
import dk.s4.hl7.cda.model.CdaDateInterval;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument.Status;
import dk.s4.hl7.cda.model.util.ModelUtil;

public class CPDInterventionActEntry {

  private ID id;
  private CodedValue code;
  private Status statusCode;
  private String text;
  private CdaDate effectiveTime;
  private CdaDateInterval effectiveTimeInterval;
  private List<Participant> performerList;

  private CPDInterventionActEntry(CPDInterventionActEntryBuilder builder) {
    this.id = builder.id;
    this.code = builder.code;
    this.text = builder.text;
    this.statusCode = builder.statusCode;
    this.effectiveTime = builder.effectiveTime;
    this.effectiveTimeInterval = builder.effectiveTimeInterval;
    this.performerList = builder.performerList;
  }

  /**
   * Builder for constructing CPDInterventionActEntry.
   */
  public static class CPDInterventionActEntryBuilder {
    private ID id;
    private CodedValue code;
    private Status statusCode;
    private String text;
    private CdaDate effectiveTime;
    private CdaDateInterval effectiveTimeInterval;
    private List<Participant> performerList;

    public CPDInterventionActEntryBuilder() {
      this.performerList = new ArrayList<Participant>();
    }

    /**
     * @param id the unique id of the encounter. The value is mandatory.
     */
    public CPDInterventionActEntryBuilder setId(ID id) {
      this.id = id;
      return this;
    }

    /**
     * @param code the intervention code
     * 
     */
    public CPDInterventionActEntryBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    /**
     * @param statusCode the status of the encounter. The value is mandatory and must be completed or active.
     * 
     * <p>Has the encounter occured use completed, has the encounter not occured use active</p>
     */
    public CPDInterventionActEntryBuilder setStatusCode(Status statusCode) {
      this.statusCode = statusCode;
      return this;
    }

    /**
     * @param text. If the intervention code is not given, then the text shall contain intervention description.
     * 
     */
    public CPDInterventionActEntryBuilder setText(String text) {
      this.text = text;
      return this;
    }

    /**
     * @param effectiveTime the time of the intervention (date + time). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDInterventionActEntryBuilder setEffectiveTime(Date effectiveTime) {
      this.effectiveTime = effectiveTime == null ? null : new CdaDate(effectiveTime, true);
      return this;
    }

    /**
     * @param effectiveTime the time of the intervention (date only). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDInterventionActEntryBuilder setEffectiveDate(Date effectiveTime) {
      this.effectiveTime = effectiveTime == null ? null : new CdaDate(effectiveTime, false);
      return this;
    }

    /**
     * @param effectiveTime the time of the intervention. 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDInterventionActEntryBuilder setEffectiveTime(CdaDate effectiveTime) {
      this.effectiveTime = effectiveTime;
      return this;
    }

    /**
     * @param effectiveStartTime the start time interval of the concern (date + time). 
     * @param effectiveStopTime the end time interval of the concern (date + time). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDInterventionActEntryBuilder setEffectiveTimeInterval(Date effectiveStartTime, Date effectiveStopTime) {
      this.effectiveTimeInterval = new CdaDateInterval(effectiveStartTime, effectiveStopTime, true);
      return this;
    }

    /**
     * @param effectiveStartTime the start time interval of the concern (date only). 
     * @param effectiveStopTime the end time interval of the concern (date only). 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDInterventionActEntryBuilder setEffectiveDateInterval(Date effectiveStartTime, Date effectiveStopTime) {
      this.effectiveTimeInterval = new CdaDateInterval(effectiveStartTime, effectiveStopTime, false);
      return this;
    }

    /**
     * @param effectiveTime the time interval of the intervention. 
     * <p>
     * Use either effectiveTime/Date or effectiveTime/DateInterval, not both.
     */
    public CPDInterventionActEntryBuilder setEffectiveTimeInterval(CdaDateInterval effectiveTimeInterval) {
      this.effectiveTimeInterval = effectiveTimeInterval;
      return this;
    }

    /**
     * @param performerList the list of performer participants
     */
    public CPDInterventionActEntryBuilder setPerformerList(List<Participant> performerList) {
      this.performerList = performerList;
      return this;
    }

    /**
     * @param performer one performer of the intervention
     */
    public CPDInterventionActEntryBuilder addPerformer(Participant performer) {
      this.performerList.add(performer);
      return this;
    }

    /**
     * validate and build CPDInterventionActEntry 
     */
    public CPDInterventionActEntry build() {
      ModelUtil.checkNull(id, "id value is mandatory");
      ModelUtil.checkNull(statusCode, "statusCode value is mandatory");
      if (effectiveTimeInterval != null) {
        ModelUtil.checkNullAtLeastOne(effectiveTimeInterval.getStartTime(), effectiveTimeInterval.getStopTime(),
            "effectiveStartTime value is mandatory when interval");
      }
      return new CPDInterventionActEntry(this);
    }

  }

  /**
   * @return the unique id of the encounter
   */
  public ID getId() {
    return id;
  }

  /**
   * @return the intervention code
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return the status code of the encounter
   */
  public Status getStatusCode() {
    return statusCode;
  }

  /**
   * @return the intervention description
   */
  public String getText() {
    return text;
  }

  /**
   * @return the date and time of the intervention. Use effectiveTimeHasTime to check if time part has to be considered
   */
  public Date getEffectiveTime() {
    return (effectiveTime == null) ? null : effectiveTime.getDate();
  }

  /**
   * @return if the time part of the effectiveTime has to be considered 
   */
  public boolean effectiveTimeHasTime() {
    return (effectiveTime == null) ? false : effectiveTime.dateHasTime();
  }

  /**
   * @return the start date and time of the intervention as CdaDate format
   */
  public CdaDate getEffectiveTimeCdaDate() {
    return effectiveTime;
  }

  /**
   * @return the start date and time interval of the intervention
   */
  public CdaDateInterval getEffectiveTimeInterval() {
    return effectiveTimeInterval;
  }

  /**
   * @param performerList the list of performers of the intervention
   */
  public List<Participant> getPerformerList() {
    return performerList;
  }

}
