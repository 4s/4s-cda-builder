package dk.s4.hl7.cda.convert.decode.qrd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.IdHandler;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer.QRDOrganizerBuilder;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class OrganizerHandler extends BaseXmlHandler {
  public static final String ORGANIZER_SECTION = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer";
  private ObservationHandler observationHandler;
  private IdHandler idHandler;
  private List<QRDOrganizer> organizers;
  private CodedValue code;
  private String introduction;

  public OrganizerHandler() {
    observationHandler = new ObservationHandler();
    idHandler = new IdHandler(ORGANIZER_SECTION);
    organizers = new ArrayList<QRDOrganizer>();
    addPath(ORGANIZER_SECTION);
    addPath(ORGANIZER_SECTION + "/code");
    addPath(ORGANIZER_SECTION + "/code/originalText");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code", "codeSystem")) {
      code = new CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "originalText")) {
      introduction = xmlElement.getElementValue();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "organizer")) {
      QRDOrganizerBuilder qrdOrganizerBuilder = new QRDOrganizerBuilder();
      qrdOrganizerBuilder.setIds(idHandler.getIds());
      for (QRDResponse response : observationHandler.getResponses()) {
        qrdOrganizerBuilder.addQRDResponse(response);
      }
      qrdOrganizerBuilder.setCode(code);
      qrdOrganizerBuilder.setIntroduction(introduction);
      organizers.add(qrdOrganizerBuilder.build());
      localClear();
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    observationHandler.addHandlerToMap(xmlMapping);
    idHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    observationHandler.removeHandlerFromMap(xmlMapping);
    idHandler.removeHandlerFromMap(xmlMapping);
  }

  private void localClear() {
    observationHandler.clear();
    idHandler.clear();
    code = null;
    introduction = null;
  }

  public List<QRDOrganizer> getOrganizers() {
    return organizers;
  }

  public void clear() {
    localClear();
    organizers = new ArrayList<QRDOrganizer>();
  }
}
