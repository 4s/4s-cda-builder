package dk.s4.hl7.cda.model.phmr.v20;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.core.BaseClinicalDocument;
import dk.s4.hl7.cda.model.phmr.Measurement;

import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of SimpleClinicalDocument that only supports the data of
 * the Danish PHMR.
  */
public final class PHMRDocument extends BaseClinicalDocument {
  private List<MeasurementGroup> vitalSignGroupList;
  private List<MeasurementGroup> resultGroupList;
  private String vitalSignText;
  private String resultText;
  private String cdaProfileAndVersion;

  public PHMRDocument(ID id) {
    super(HL7.CDA_TYPEID_ROOT, HL7.CDA_TYPEID_EXTENSION, id, Loinc.PHMR_CODE, Loinc.PMHR_DISPLAYNAME,
        new String[] { MedCom.PHMR_ROOT_POD, MedCom.DK_PHMR_ROOT_POD }, HL7.REALM_CODE_DK);
    this.vitalSignGroupList = new ArrayList<MeasurementGroup>();
    this.resultGroupList = new ArrayList<>();
    this.vitalSignText = "Vital Signs";
    this.resultText = "Results";
  }

  public void addResultGroups(MeasurementGroup measurementGroup) {
    resultGroupList.add(measurementGroup);
  }

  public void addVitalSignGroups(MeasurementGroup measurementGroup) {
    vitalSignGroupList.add(measurementGroup);
  }

  public List<MeasurementGroup> getResultGroups() {
    return resultGroupList;
  }

  public List<MeasurementGroup> getVitalSignGroups() {
    return vitalSignGroupList;
  }

  public void setResultsText(String resultText) {
    this.resultText = resultText;
  }

  public void setVitalSignsText(String vitalSignText) {
    this.vitalSignText = vitalSignText;
  }

  public String getResultsText() {
    return resultText;
  }

  public String getVitalSignsText() {
    return vitalSignText;
  }

  public String getCdaProfileAndVersion() {
    return cdaProfileAndVersion;
  }

  public void setCdaProfileAndVersion(String cdaProfileAndVersion) {
    this.cdaProfileAndVersion = cdaProfileAndVersion;
  }
}
