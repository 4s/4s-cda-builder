package dk.s4.hl7.cda.convert;

import java.io.Reader;

import dk.s4.hl7.cda.convert.base.AppendableSerializer;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.convert.base.ReaderSerializer;
import dk.s4.hl7.cda.model.cdametadata.CDAMetadata;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;

/**
 * The implementation of Codec to decode CDA meta data from XML to objects
 * After construction the codec is considered thread-safe.
 * 
 * @see https://www.dartlang.org/articles/libraries/converters-and-codecs
 * 
 * This class only handles decoding/parsing of the metadata. It makes not sense to build the metadata. This must be handled by the proper document implementation
 */
public class CDAMetadataXmlCodec
    implements Codec<CDAMetadata, String>, AppendableSerializer<CDAMetadata>, ReaderSerializer<CDAMetadata> {
  private XmlCDAMetadataConverter xmlCdaMetadataConverter;

  public CDAMetadataXmlCodec() {
    this(new XmlPrettyPrinter());
  }

  /**
   * Create CDAMetadataXmlCodec
   * 
   * @param xmlPrettyPrinter May be null
   */
  public CDAMetadataXmlCodec(XmlPrettyPrinter xmlPrettyPrinter) {
    this.xmlCdaMetadataConverter = new XmlCDAMetadataConverter();
  }

  @Override
  public String encode(CDAMetadata source) {
    return null; //not implemented for metadata which only parses data
  }

  @Override
  public CDAMetadata decode(String source) {
    return xmlCdaMetadataConverter.convert(source);
  }

  @Override
  public void serialize(CDAMetadata source, Appendable appendable) {
    //not implemented for metadata which only parses data
  }

  @Override
  public CDAMetadata deserialize(Reader source) {
    return xmlCdaMetadataConverter.deserialize(source);
  }

}