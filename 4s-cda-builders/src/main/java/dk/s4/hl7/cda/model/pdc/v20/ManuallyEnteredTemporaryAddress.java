package dk.s4.hl7.cda.model.pdc.v20;

import java.util.Date;

import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;

/**
 * Manually entered information about the citizen’s temporary address.
 */

public class ManuallyEnteredTemporaryAddress {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;
  private Date effectiveFrom;
  private Date effectiveTo;
  private AddressData addressData;

  public static class ManuallyEnteredTemporaryAddressBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;

    private Date effectiveFrom;
    private Date effectiveTo;
    private AddressData addressData;

    public ManuallyEnteredTemporaryAddressBuilder(ID id) {
      this.id = id;
    }

    public ManuallyEnteredTemporaryAddressBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public ManuallyEnteredTemporaryAddressBuilder setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public ManuallyEnteredTemporaryAddressBuilder setEffectiveFrom(Date effectiveFrom) {
      this.effectiveFrom = effectiveFrom;
      return this;
    }

    public ManuallyEnteredTemporaryAddressBuilder setEffectiveTo(Date effectiveTo) {
      this.effectiveTo = effectiveTo;
      return this;
    }

    public ManuallyEnteredTemporaryAddressBuilder setAddressData(AddressData addressData) {
      this.addressData = addressData;
      return this;
    }

    public ManuallyEnteredTemporaryAddress build() {

      return new ManuallyEnteredTemporaryAddress(this);
    }

  }

  private ManuallyEnteredTemporaryAddress(ManuallyEnteredTemporaryAddressBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.effectiveFrom = builder.effectiveFrom;
    this.effectiveTo = builder.effectiveTo;
    this.addressData = builder.addressData;

  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * @return Date Returns the start date/time of the temporary address
   */
  public Date getEffectiveFrom() {
    return effectiveFrom;
  }

  /**
   * @return Date Returns the end date/time of the temporary address
   */
  public Date getEffectiveTo() {
    return effectiveTo;
  }

  /**
   * @return AddressData Returns the citizens temporary address
   */
  public AddressData getAddressData() {
    return addressData;
  }

}
