package dk.s4.hl7.cda.codes;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.convert.PHMRXmlConverter;

public class EquipmentTypes {
  private static final Map<String, String> equipmentTypeToOidMap = createMap();

  public static final String TYPE_EUI_64 = "EUI-64";

  private static synchronized Map<String, String> createMap() {
    Map<String, String> equipmentTypeToOidMap = new HashMap<String, String>();
    equipmentTypeToOidMap.put(TYPE_EUI_64, "1.2.840.10004.1.1.1.0.0.1.0.0.1.2680");
    equipmentTypeToOidMap.put(MedCom.ROOT_AUTHORITYNAME, MedCom.MEDICAL_DEVICE_OID);
    return equipmentTypeToOidMap;
  }

  public static String getEquipmentTypeOID(String equipmentType) {
    String oid = equipmentTypeToOidMap.get(equipmentType);
    if (oid == null) {
      LoggerFactory.getLogger(PHMRXmlConverter.class).debug("Please add OID for equipmentType: " + equipmentType);
      return MedCom.ROOT_OID;
    }
    return oid;
  }
}
