package dk.s4.hl7.cda.convert.decode.header.v20;

import java.util.Date;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;

public class PersonHandler extends dk.s4.hl7.cda.convert.decode.general.PersonHandler {

  public PersonHandler(String personParentXPath, String personElementName) {
    super(personParentXPath, personElementName);

  }

  @Override
  protected Date getBirthTime(String birthTime) {
    Date date = ConvertXmlUtil.getPersonBirthdayIncludingTime(birthTime);

    if (date != null) {
      isBirthTimeOnlyDate = false;
      return date;
    } else {
      isBirthTimeOnlyDate = true;
      return ConvertXmlUtil.getPersonBirthdayExcludingTime(birthTime);
    }

  }

}
