package dk.s4.hl7.cda.model.cpd.v20;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.util.ModelUtil;

public class CPDInterventionActComponent {

  private String title;
  private String text;
  private CodedValue code;
  private List<Participant> authorList;
  private List<CPDInterventionActEntry> entryList;

  private CPDInterventionActComponent(CPDInterventionActComponentBuilder builder) {
    this.entryList = builder.entryList;
    this.authorList = builder.authorList;
    this.title = builder.title;
    this.text = builder.text;
    this.code = builder.code;
  }

  public static class CPDInterventionActComponentBuilder {

    private String title;
    private String text;
    private CodedValue code;
    private List<Participant> authorList;
    private List<CPDInterventionActEntry> entryList;

    public CPDInterventionActComponentBuilder() {
      this.entryList = new ArrayList<CPDInterventionActEntry>();
      this.authorList = new ArrayList<Participant>();
    }

    public CPDInterventionActComponentBuilder setTitle(String title) {
      this.title = title;
      return this;
    }

    public CPDInterventionActComponentBuilder setText(String text) {
      this.text = text;
      return this;
    }

    public CPDInterventionActComponentBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public CPDInterventionActComponentBuilder setEntryList(List<CPDInterventionActEntry> entryList) {
      this.entryList = entryList;
      return this;
    }

    public CPDInterventionActComponentBuilder addEntry(CPDInterventionActEntry entry) {
      this.entryList.add(entry);
      return this;
    }

    public CPDInterventionActComponentBuilder setAuthorList(List<Participant> authorList) {
      this.authorList = authorList;
      return this;
    }

    public CPDInterventionActComponentBuilder addAuthor(Participant author) {
      this.authorList.add(author);
      return this;
    }

    public CPDInterventionActComponent build() {
      ModelUtil.checkNull(code, "code value is mandatory");
      return new CPDInterventionActComponent(this);
    }
  }

  public List<CPDInterventionActEntry> getEntryList() {
    return entryList;
  }

  public List<Participant> getAuthorList() {
    return authorList;
  }

  public String getTitle() {
    return title;
  }

  public String getText() {
    return text;
  }

  public CodedValue getCode() {
    return code;
  }
}
