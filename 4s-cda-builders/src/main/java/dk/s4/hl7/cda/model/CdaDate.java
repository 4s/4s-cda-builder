package dk.s4.hl7.cda.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

public class CdaDate {

  private static final int lengthDate = 8;
  private static final int lengthFullDateTime = 19;

  private static final String dateTimeTimeZonePattern = "yyyyMMddHHmmssZ";
  private static final String datePattern = "yyyyMMdd";
  private static final DateTimeFormatter dateTimeTimeZoneFormatter = DateTimeFormatter
      .ofPattern(dateTimeTimeZonePattern);
  private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(datePattern);

  Date date;
  int lengthToConsider; //implemented like this because the IHE in principle allow for date/time only to be the year. For future enhancements
  //https://www.ihe.net/uploadedFiles/Documents/ITI/IHE_ITI_TF_Vol3.pdf page 59

  public CdaDate(Date date, boolean hasTime) {
    this.date = date;
    determineDateLength(hasTime);
  }

  public CdaDate(String time) {
    if (time != null && !time.trim().isEmpty()) {
      try {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(time, dateTimeTimeZoneFormatter);
        date = convertToDate(zonedDateTime);
        determineDateLength(true);
      } catch (DateTimeParseException e) {
        LocalDate localDate = LocalDate.parse(time, dateFormatter);
        date = convertToDate(localDate);
        determineDateLength(false);
      }
    }
  }

  private void determineDateLength(boolean hasTime) {
    if (hasTime) {
      lengthToConsider = lengthFullDateTime;
    } else {
      lengthToConsider = lengthDate;
    }
  }

  private Date convertToDate(ZonedDateTime zonedDateTime) {
    return Date.from(zonedDateTime.toInstant());
  }

  private Date convertToDate(LocalDate localDate) {
    return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
  }

  private ZonedDateTime convertToZonedDateTime(Date date) {
    return ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
  }

  public Date getDate() {
    return this.date;
  }

  public boolean dateHasTime() {
    return (this.lengthToConsider == lengthFullDateTime);
  }

  public String dateToFormattedString() {
    if (dateHasTime()) {
      return convertToZonedDateTime(this.date).format(dateTimeTimeZoneFormatter);
    } else {
      return convertToZonedDateTime(this.date).format(dateFormatter);
    }
  }
}
