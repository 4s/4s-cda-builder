package dk.s4.hl7.cda.model.qrd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.QuestionnaireEntity;
import dk.s4.hl7.cda.model.Reference;

/**
 * Base QRD response for all types of response types
 */
public abstract class QRDResponse extends QuestionnaireEntity {
  private List<Reference> references;
  private QRDHelpText helpText;

  protected QRDResponse(BaseQRDResponseBuilder<?, ?> builder) {
    super(builder);
    this.references = builder.references;
    helpText = builder.helpText;
  }

  public void addReference(Reference reference) {
    if (reference != null) {
      references.add(reference);
    }
  }

  public List<Reference> getReferences() {
    return references;
  }

  public QRDHelpText getHelpText() {
    return helpText;
  }

  public static abstract class BaseQRDResponseBuilder<R extends QRDResponse, T extends QuestionnaireEntityBuilder<R, T>>
      extends QuestionnaireEntityBuilder<R, T> {
    protected List<Reference> references;
    private QRDHelpText helpText;

    public BaseQRDResponseBuilder() {
      references = new ArrayList<Reference>();
    }

    public T addReference(Reference reference) {
      if (reference != null) {
        references.add(reference);
      }
      return getThis();
    }

    public T setHelpText(QRDHelpText qrdHelpText) {
      this.helpText = qrdHelpText;
      return getThis();
    }

    public abstract T getThis();

    public abstract R build();
  }
}
