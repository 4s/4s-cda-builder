package dk.s4.hl7.cda.model.pdc.v30;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;

/**
 * Indicates whether the Citizen has registered a treatment will or not.
 */
public class TreatmentWillRegistration {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;
  private boolean isRegistered;

  public static class TreatmentWillRegistrationBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;
    private boolean isRegistered;

    public TreatmentWillRegistrationBuilder(ID id) {
      this.id = id;
    }

    public TreatmentWillRegistrationBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public TreatmentWillRegistrationBuilder setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public TreatmentWillRegistrationBuilder setRegisteret(boolean isRegistered) {
      this.isRegistered = isRegistered;
      return this;
    }

    public TreatmentWillRegistration build() {

      return new TreatmentWillRegistration(this);
    }

  }

  private TreatmentWillRegistration(TreatmentWillRegistrationBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.isRegistered = builder.isRegistered;

  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * @return boolean Returns if the citizen has a treatment will or not
   */
  public boolean isRegistered() {
    return isRegistered;
  }

}
