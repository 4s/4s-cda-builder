package dk.s4.hl7.cda.model.pdc.v20;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;

/**
 * Manually entered information about the citizen’s spoken language.
 */

public class ManuallyEnteredSpokenLanguage {

  private ID id;
  private CodedValue code;
  private AuthorOfInformation authorOfInformation;
  private CodedValue languageCode;

  public static class ManuallyEnteredSpokenLanguageBuilder {
    private ID id;
    private CodedValue code;
    private AuthorOfInformation authorOfInformation;
    private CodedValue languageCode;

    public ManuallyEnteredSpokenLanguageBuilder(ID id) {
      this.id = id;
    }

    public ManuallyEnteredSpokenLanguageBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public ManuallyEnteredSpokenLanguageBuilder setAuthorOfInformation(AuthorOfInformation authorOfInformation) {
      this.authorOfInformation = authorOfInformation;
      return this;
    }

    public ManuallyEnteredSpokenLanguageBuilder setLanguageCode(CodedValue languageCode) {
      this.languageCode = languageCode;
      return this;
    }

    public ManuallyEnteredSpokenLanguage build() {

      return new ManuallyEnteredSpokenLanguage(this);
    }

  }

  private ManuallyEnteredSpokenLanguage(ManuallyEnteredSpokenLanguageBuilder builder) {

    this.id = builder.id;
    this.code = builder.code;
    this.authorOfInformation = builder.authorOfInformation;
    this.languageCode = builder.languageCode;

  }

  /**
   * @return ID Returns the identification of the information
   */
  public ID getId() {
    return id;
  }

  /**
   * @return CodedValue Returns the code for the information
   */
  public CodedValue getCode() {
    return code;
  }

  /**
   * @return AuthorOfInformation Returns the author of the information
   */
  public AuthorOfInformation getAuthorOfInformation() {
    return authorOfInformation;
  }

  /**
   * The value of the languageCode represents the spoken language of the citizen
   * <p>
   * The @code is the language code, e.g. "de" and the @codeDisplayName the
   * corresponding description e.g. "Tysk"
   * 
   * @return CodedValue Returns the language code
   */
  public CodedValue getLanguageCode() {
    return languageCode;
  }

}
