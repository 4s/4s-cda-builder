package dk.s4.hl7.util.xml.hashing;

/**
 * Mimics some of the functionality of a StringBuilder but with two crucial
 * differences:
 * 1) The builder continuously calculates the hashcode when appending strings
 * and setting a new length. This avoids the following code
 * 'builder.toString().hashCode()' which iterates the inner char array twice!
 * The new 'builder.hashCode()' is O(1). So using this class as a lookup key in
 * a HashMap is very efficient.
 * 2) The builder is able to compare itself with CharSequences (Strings). This
 * avoids the following code 'builder.toString().equalsIgnoreCase(otherString)'
 * which is now equal to 'builder.equalsIgnoreCase(otherString)'
 * 
 */
public class HashableStringBuilder extends HashableCharSequence implements Appendable {
  private StringBuilder builder;

  public HashableStringBuilder() {
    this(1024);
  }

  public HashableStringBuilder(int intialSize) {
    this.builder = new StringBuilder(intialSize);
  }

  @Override
  public int length() {
    return builder.length();
  }

  @Override
  public char charAt(int index) {
    return builder.charAt(index);
  }

  @Override
  public CharSequence subSequence(int start, int end) {
    return builder.subSequence(start, end);
  }

  @Override
  public HashableStringBuilder append(CharSequence csq) {
    int currentLength = builder.length();
    builder.append(csq);
    hasher.appendHash(currentLength, builder);
    return this;
  }

  @Override
  public HashableStringBuilder append(CharSequence csq, int start, int end) {
    int currentLength = builder.length();
    builder.append(csq, start, end);
    hasher.appendHash(currentLength - 1, builder);
    return this;
  }

  @Override
  public HashableStringBuilder append(char c) {
    builder.append(c);
    hasher.appendHash(builder.length() - 1, builder);
    return this;
  }

  public void setLength(int newLength) {
    if (newLength > -1) {
      if (builder.length() > newLength) {
        hasher.decreaseHash(newLength, builder);
      }
      builder.setLength(newLength);
    }
  }

  public boolean equalsIgnoreCase(CharSequence text) {
    return super.equalsIgnoreCase(this.builder, text);
  }

  @Override
  public boolean equals(Object obj) {
    return super.equals(builder, obj);
  }

  @Override
  public String toString() {
    return builder.toString();
  }
}