package dk.s4.hl7.util.xml;

public class AttributeMatcher {
  private String attributeName;
  private String value;
  private XmlHandler[] handlers;

  public AttributeMatcher(String attributeName, String value, XmlHandler... handlers) {
    this.handlers = handlers;
    this.attributeName = attributeName;
    this.value = value;
  }

  public boolean matchAttribute(XMLElement xmlElement) {
    String actualValue = xmlElement.getAttributeValue(attributeName);
    if (actualValue != null) {
      return actualValue.equalsIgnoreCase(value);
    }
    return false;
  }

  public XmlHandler[] getHandlers() {
    return handlers;
  }

  public void addHandlers(XmlMapping xmlMapping) {
    for (XmlHandler xmlHandler : handlers) {
      xmlHandler.addHandlerToMap(xmlMapping);
    }
  }

  public void removeHandlers(XmlMapping xmlMapping) {
    for (XmlHandler xmlHandler : handlers) {
      xmlHandler.removeHandlerFromMap(xmlMapping);
    }
  }
}
