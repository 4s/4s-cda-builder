package dk.s4.hl7.util.xml;

public interface XmlHandler {
  boolean includeChildren();

  void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement);

  void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement);

  void addHandlerToMap(XmlMapping xmlMapping);

  void removeHandlerFromMap(XmlMapping xmlMapping);
}