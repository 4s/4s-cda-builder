<?xml version="1.0" encoding="UTF-8" standalone="yes"?>

<!--
 Title:        	Oxygen saturation. Measured by the patient. Entered by the patient.
 Filename:     	PHMR-example-01
 PHMR-version:	 2.1.0
 
 Created by:   	Morten Bruun-Rasmussen
 Quality assured:  Søren Gammelgaard
 
 $LastChangedDate: 2025-02-27 $
  
 ********************************************************
 Disclaimer: This sample file contains representative data elements to represent an Personal Health Monitoring Record (PHMR). 
 The file depicts a fictional character's health data. Any resemblance to a real person is coincidental.
 The data in this sample file is not intended to represent real patients, people or clinical events. 
 ********************************************************
 -->

<?xml-stylesheet type="text/xsl" href="https://svn.medcom.dk/svn/drafts/Standarder/HL7/CDA-Stylesheet/HL7_CDA_stylesheet.xsl"?>
<ClinicalDocument xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="urn:hl7-org:v3" xmlns:voc="urn:hl7-org:v3/voc" xsi:schemaLocation="urn:hl7-org:v3 https://svn.medcom.dk/svn/drafts/Standarder/HL7/PRO/QRD/Schema/CDA_SDTC.xsd" classCode="DOCCLIN" moodCode="EVN">
	<realmCode code="DK"/>
	<typeId root="2.16.840.1.113883.1.3" extension="POCD_HD000040"/>
	
	<!-- Clinical Observation Template -->
	<templateId root="2.16.840.1.113883.10.20.9"/>
	
	<!-- Danish PHMR Template -->
	<templateId root="1.2.208.184.11.1"/>
	
	<!-- id for the organisation building the PHMR  -->
	<id extension="d68f4e35-6576-414e-9db4-0a0962dabf66" root="1.2.3.4.5" assigningAuthorityName="Some organisation"/>
	
	<code code="53576-5" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"  displayName="Personal Health Monitoring Report"/>
	<title>Hjemmemålinger</title>
	<effectiveTime value="20250227101010+0100"/>
	<confidentialityCode code="N" codeSystem="2.16.840.1.113883.5.25"/>
	<languageCode code="da-DK"/>
	
	<!-- Information about the patient -->
	<recordTarget typeCode="RCT" contextControlCode="OP">
		<patientRole classCode="PAT">
			<id root="1.2.208.176.1.2" extension="0103660140" assigningAuthorityName="CPR"/>
			<addr use="H">
				<streetAddressLine>Boltonvej 27</streetAddressLine>
				<postalCode>2300</postalCode>
				<city>København S</city>
			</addr>
			<patient classCode="PSN" determinerCode="INSTANCE">
				<name>
					<given>Frederikke</given>
					<family>Februar</family>
				</name>
				<administrativeGenderCode code="F" codeSystem="2.16.840.1.113883.5.1"/>
				<birthTime value="19660301000000+0000"/>
			</patient>
		</patientRole>
	</recordTarget>
	
	<!-- Information about the author and the responsible healthcare organization -->
	<author typeCode="AUT" contextControlCode="OP">
		<!-- Time when the author completes and approves the measurement results  -->
		<time value="20250130120000+0100"/>
		<assignedAuthor classCode="ASSIGNED">
			<!-- id, as a Danish CPR-number for the patient. The patient is author -->
			<id root="1.2.208.176.1.2" extension="0103660140" assigningAuthorityName="CPR"/>
			<!-- The patient is self-reporting the measurements -->
			<code code="SELF" displayName="Self" codeSystem="2.16.840.1.113883.5.111" codeSystemName="HL7 Role code"/>
			<addr use="H">
				<streetAddressLine>Boltonvej 27</streetAddressLine>
				<postalCode>2300</postalCode>
				<city>København S</city>
			</addr>
			<assignedPerson classCode="PSN" determinerCode="INSTANCE">
				<name>
					<given>Frederikke</given>
					<family>Februar</family>
				</name>
			</assignedPerson>
			
			<!-- The representedOrganization is the healthcare organization that has responsibility for the patient's treatment -->
			<representedOrganization classCode="ORG" determinerCode="INSTANCE">
				<id root="1.2.208.176.1.1" extension="1118261000016001" assigningAuthorityName="SOR"/>
				<name>Sundhedsteamet, Københavns Kommune</name>
				<telecom value="tel:12345678" use="WP"/>
				<telecom value="mailto:sundhed@testkommune.dk" use="WP"/>
				<telecom value="http://www.testkommune.dk" use="WP"/>
				<addr use="WP">
					<streetAddressLine>Sundholmsvej 18</streetAddressLine>
					<postalCode>2300</postalCode>
					<city>København S</city>
					<country>Danmark</country>
				</addr>
			</representedOrganization>
		</assignedAuthor>
	</author>
	
	<!-- Information about the organization technically responsible for the document  -->
	<custodian typeCode="CST">
		<assignedCustodian classCode="ASSIGNED">
			<representedCustodianOrganization classCode="ORG" determinerCode="INSTANCE">
				<id extension="21000016006" root="1.2.208.176.1.1" assigningAuthorityName="SOR"/>
				<name>Københavns Kommune</name>
				<telecom value="tel:76810000" use="WP"/>
				<addr use="WP">
					<streetAddressLine>Rådhuspladsen 1</streetAddressLine>
					<postalCode>1550</postalCode>
					<city>København V</city>
					<country>Danmark</country>
				</addr>
			</representedCustodianOrganization>
		</assignedCustodian>
	</custodian>
	
	<!-- Information about the organisation that have approved the document -->
	<legalAuthenticator typeCode="LA" contextControlCode="OP">
		<time value="20250130130000+0100"/>
		<signatureCode nullFlavor="NI"/>
		<assignedEntity classCode="ASSIGNED">
			<id root="1.2.208.176.1.1" extension="1118261000016001" assigningAuthorityName="SOR"/>
			<addr use="WP">
				<streetAddressLine>Sundholmsvej 18</streetAddressLine>
				<postalCode>2300</postalCode>
				<city>København S</city>
				<country>Danmark</country>
			</addr>
			<telecom value="tel:12345678" use="WP"/>
			<telecom value="mailto:sundhed@testkommune.dk" use="WP"/>
			<telecom value="http://www.testkommune.dk" use="WP"/>
			<assignedPerson classCode="PSN" determinerCode="INSTANCE">
				<name>
					<prefix>Læge</prefix>
					<given>Birthe</given>
					<family>Jensen</family>
				</name>
			</assignedPerson>
		</assignedEntity>
	</legalAuthenticator>
	
	<!-- Date/time (start and stop) for the measurements -->
	<documentationOf typeCode="DOC">
		<serviceEvent classCode="MPROT" moodCode="EVN">
			<effectiveTime>
				<low value="20250130100000+0100"/>
				<high value="20250130110000+0100"/>
			</effectiveTime>
		</serviceEvent>
	</documentationOf>
		
	<!-- PHMR DK version -->
	<documentationOf typeCode="DOC">
		<serviceEvent classCode="MPROT" moodCode="EVN">
			<templateId root="1.2.208.184.10.1.10"/>
			<id root="1.2.208.184.100.10" extension="phmr-v2.0" assigningAuthorityName="MedCom"/>
		</serviceEvent>
	</documentationOf>
	
	<!-- Code(s) for the used measurement in the CDA-body -->
	<documentationOf typeCode="DOC">
		<serviceEvent classCode="MPROT" moodCode="EVN">
			<code code="NPU03011" codeSystem="1.2.208.176.2.1" displayName="O2 sat; Hb(aB)"/>
		</serviceEvent>
	</documentationOf>
	
	<!-- Code(s) for the used measurement in the CDA-body -->
	<documentationOf typeCode="DOC">
		<serviceEvent classCode="MPROT" moodCode="EVN">
			<code code="DNK05472" codeSystem="1.2.208.176.2.1" displayName="Blodtryk systolisk;Arm"/>
		</serviceEvent>
	</documentationOf>
	
	<!-- Code(s) for the used measurement in the CDA-body -->
	<documentationOf typeCode="DOC">
		<serviceEvent classCode="MPROT" moodCode="EVN">
			<code code="DNK05473" codeSystem="1.2.208.176.2.1" displayName="Blodtryk diastolisk;Arm"/>
		</serviceEvent>
	</documentationOf>
			
	<!-- Code(s) for the used measurement in the CDA-body -->
	<documentationOf typeCode="DOC">
		<serviceEvent classCode="MPROT" moodCode="EVN">
			<code code="MCS88050" codeSystem="1.2.208.184.100.1" displayName="Rejse sætte sig testen; Pt"/>
		</serviceEvent>
	</documentationOf>
	
		
	<!--
	*******************************************************************************************************************************************************
	CDA Body
	*******************************************************************************************************************************************************
	-->
	<component typeCode="COMP" contextConductionInd="true">
		<structuredBody classCode="DOCBODY" moodCode="EVN">
			<component contextConductionInd="true" typeCode="COMP">
				<section classCode="DOCSECT" moodCode="EVN">
					<!-- HL7 PHMR Vital Signs Section template ID-->
					<templateId root="2.16.840.1.113883.10.20.1.16"/>
					
					<code code="8716-3" codeSystem="2.16.840.1.113883.6.1" 
						displayName="Vital signs" codeSystemName="LOINC"/>
					<title>Vital Signs</title>
					<text>
						<paragraph styleCode="Bold">O2 sat; Hb(aB)</paragraph>
						<table width="100%">
							<tbody>
								<tr>
									<th>Date/Time</th>
									<th>Value</th>
									<th>Målt af</th>
									<th>Indtastet af</th>
									<th>Status</th>
								</tr>
								<tr>
									<td>30-01-2025 10:00:00</td>
									<td>95 %</td>
									<td>Borgeren</td>
									<td>Borgeren</td>
									<td>completed</td>
								</tr>
								<tr>
									<td>30-01-2025 11:00:00</td>
									<td>96 %</td>
									<td>Borgeren</td>
									<td>Borgeren</td>
									<td>completed</td>
								</tr>
								<tr>
									<td>27-02-2025 09:30:00</td>
									<td>140 mmHg</td>
									<td>Borgeren</td>
									<td>Borgeren</td>
									<td>completed</td>
								</tr>
								<tr>
									<td>27-02-2025 09:30:30</td>
									<td>90 mmHg</td>
									<td>Borgeren</td>
									<td>Borgeren</td>
									<td>completed</td>
								</tr>				
								<tr>
									<td>27-02-2025 10:00:00</td>
									<td>120 mmHg</td>
									<td>Borgeren</td>
									<td>Borgeren</td>
									<td>completed</td>
								</tr>
								<tr>
									<td>27-02-2025 10:00:30</td>
									<td>80 mmHg</td>
									<td>Borgeren</td>
									<td>Borgeren</td>
									<td>completed</td>
								</tr>
							</tbody>
						</table>
					</text>					
					
					<!-- Measurement(s) -->
					<entry contextConductionInd="true" typeCode="COMP">			
						<organizer classCode="CLUSTER" moodCode="EVN"> 
							<!-- PHMR Result Vital Signs Organizer template ID -->
							<templateId root="2.16.840.1.113883.10.20.1.35"/>
							<statusCode code="completed"/>
							<!-- First oxygen saturation measurements -->
							<component contextConductionInd="true" typeCode="COMP">
								<observation classCode="OBS" moodCode="EVN">
									<!-- HL7 PHMR Numeric Observation template ID -->
									<templateId root="2.16.840.1.113883.10.20.9.8"/>
									<!-- id for the healthcare organization and local id for the measurement -->
									<id assigningAuthorityName="Some organization" 
										extension="organization internal unique id" root="1.2.4.5"/>
									<code code="NPU03011" codeSystem="1.2.208.176.2.1" 
										codeSystemName="NPU terminologien" displayName="O2 sat; Hb(aB)"/>
									<statusCode code="completed"/>
									<effectiveTime value="20250130100000+0100"/>
									<value xsi:type="PQ" value="0.95" unit="ratio"/>
									<methodCode code="POT" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Målt af borger"/>
									<methodCode code="TPD" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Indtastet af borger"/>	
								</observation>
								
							</component>
							<!-- Second oxygen saturation measurements -->
							<component contextConductionInd="true" typeCode="COMP">
								<observation classCode="OBS" moodCode="EVN">
									<templateId root="2.16.840.1.113883.10.20.9.8"/>
									<!-- id for the healthcare organization and local id for the measurement -->
									<id assigningAuthorityName="Some organization" 
										extension="organization internal id" root="1.2.4.5"/>
									<code code="NPU03011" codeSystem="1.2.208.176.2.1" 
										codeSystemName="NPU terminologien" displayName="O2 sat; Hb(aB)"/>
									<statusCode code="completed"/>
									<effectiveTime value="20250130110000+0100"/>
									<value xsi:type="PQ" value="0.96" unit="ratio"/>
									<methodCode code="POT" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Målt af borger"/>
									<methodCode code="TPD" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Indtastet af borger"/>	
								</observation>
							</component>					
						</organizer>		
					</entry>
					
					<!-- Measurement(s) -->
					<entry contextConductionInd="true" typeCode="COMP">			
						<organizer classCode="CLUSTER" moodCode="EVN"> 
							<!-- PHMR Result Vital Signs Organizer template ID -->
							<templateId root="2.16.840.1.113883.10.20.1.35"/>
							<statusCode code="completed"/>
							<!-- First bloodplessure measurements -->
							<component contextConductionInd="true" typeCode="COMP">
								<observation classCode="OBS" moodCode="EVN">
									<!-- HL7 PHMR Numeric Observation template ID -->
									<templateId root="2.16.840.1.113883.10.20.9.8"/>
									<!-- id for the healthcare organization and local id for the measurement -->
									<id assigningAuthorityName="Some organization" 
										extension="organization internal unique id" root="1.2.4.5"/>
									<code code="DNK05472" codeSystem="1.2.208.176.2.1" 
										codeSystemName="NPU terminologien" displayName="Blodtryk systolisk;Arm"/>
									<statusCode code="completed"/>
									<effectiveTime value="20250227093000+0100"/>
									<value xsi:type="PQ" value="140" unit="mmHg"/>
									<methodCode code="POT" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Målt af borger"/>
									<methodCode code="TPD" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Indtastet af borger"/>	
								</observation>
							</component>
							<component contextConductionInd="true" typeCode="COMP">
								<observation classCode="OBS" moodCode="EVN">
									<!-- HL7 PHMR Numeric Observation template ID -->
									<templateId root="2.16.840.1.113883.10.20.9.8"/>
									<!-- id for the healthcare organization and local id for the measurement -->
									<id assigningAuthorityName="Some organization" 
										extension="organization internal unique id" root="1.2.4.5"/>
									<code code="DNK05473" codeSystem="1.2.208.176.2.1" 
										codeSystemName="NPU terminologien" displayName="Blodtryk diastolisk;Arm"/>
									<statusCode code="completed"/>
									<effectiveTime value="20250227093030+0100"/>
									<value xsi:type="PQ" value="90" unit="mmHg"/>
									<methodCode code="POT" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Målt af borger"/>
									<methodCode code="TPD" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Indtastet af borger"/>	
								</observation>
							</component>
						</organizer>					
					</entry>
					
					
					
					<!-- Measurement(s) -->
					<entry contextConductionInd="true" typeCode="COMP">			
						<organizer classCode="CLUSTER" moodCode="EVN"> 
							<!-- PHMR Result Vital Signs Organizer template ID -->
							<templateId root="2.16.840.1.113883.10.20.1.35"/>
							<statusCode code="completed"/>
							<!-- First bloodplessure measurements -->
							<component contextConductionInd="true" typeCode="COMP">
								<observation classCode="OBS" moodCode="EVN">
									<!-- HL7 PHMR Numeric Observation template ID -->
									<templateId root="2.16.840.1.113883.10.20.9.8"/>
									<!-- id for the healthcare organization and local id for the measurement -->
									<id assigningAuthorityName="Some organization" 
										extension="organization internal unique id" root="1.2.4.5"/>
									<code code="DNK05472" codeSystem="1.2.208.176.2.1" 
										codeSystemName="NPU terminologien" displayName="Blodtryk systolisk;Arm"/>
									<statusCode code="completed"/>
									<effectiveTime value="20250227100000+0100"/>
									<value xsi:type="PQ" value="120" unit="mmHg"/>
									<methodCode code="POT" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Målt af borger"/>
									<methodCode code="TPD" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Indtastet af borger"/>	
								</observation>
							</component>
							<component contextConductionInd="true" typeCode="COMP">
								<observation classCode="OBS" moodCode="EVN">
									<!-- HL7 PHMR Numeric Observation template ID -->
									<templateId root="2.16.840.1.113883.10.20.9.8"/>
									<!-- id for the healthcare organization and local id for the measurement -->
									<id assigningAuthorityName="Some organization" 
										extension="organization internal unique id" root="1.2.4.5"/>
									<code code="DNK05473" codeSystem="1.2.208.176.2.1" 
										codeSystemName="NPU terminologien" displayName="Blodtryk diastolisk;Arm"/>
									<statusCode code="completed"/>
									<effectiveTime value="20250227100030+0100"/>
									<value xsi:type="PQ" value="80" unit="mmHg"/>
									<methodCode code="POT" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Målt af borger"/>
									<methodCode code="TPD" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Indtastet af borger"/>	
								</observation>
							</component>
						</organizer>					
					</entry>
				</section>
			</component>

			<component contextConductionInd="true" typeCode="COMP">	
				<section classCode="DOCSECT" moodCode="EVN">							
					<!-- HL7 PHMR Result Observation template ID -->
					<templateId root="2.16.840.1.113883.10.20.1.14"/>
					
					<code code="30954-2" codeSystem="2.16.840.1.113883.6.1" 
						displayName="Results" codeSystemName="LOINC"/>
					<title>Results</title>							
					<text>
						<paragraph styleCode="Bold">Rejse sætte sig testen; Pt</paragraph>
						<table width="100%">
							<tbody>
								<tr>
									<th>Date/Time</th>
									<th>Value</th>
									<th>Målt af</th>
									<th>Indtastet af</th>
									<th>Status</th>
								</tr>
								<tr>
									<td>30-01-2025 10:00:00</td>
									<td>22 /30s</td>
									<td>Borgeren</td>
									<td>Borgeren</td>
									<td>Completed</td>
								</tr>						
							</tbody>
						</table>
					</text>
					
					<!-- Measurement(s) -->
					<entry contextConductionInd="true" typeCode="COMP">
						<organizer classCode="CLUSTER" moodCode="EVN">
							<!-- PHMR Result Observation Organizer template ID -->
							<templateId root="2.16.840.1.113883.10.20.1.35"/>
							<statusCode code="completed"/>
							<effectiveTime value="20250130100000+0100"/>
							<!-- Result for Sit-to-Stand Test (STS) -->
							<component contextConductionInd="true" typeCode="COMP">
								<observation classCode="OBS" moodCode="EVN">
									<!-- HL7 PHMR Numeric Observation template ID -->
									<templateId root="2.16.840.1.113883.10.20.9.8"/>
									<!-- id for the healthcare organisation that stores the measurement -->
									<id assigningAuthorityName="Some organization" 
										extension="organization internal unique id" root="1.2.4.5"/>
									<code code="MCS88050" codeSystem="1.2.208.184.100.1" codeSystemName="MCS" 
										displayName="Rejse sætte sig testen; Pt"/>						
									<statusCode code="completed"/>
									<value xsi:type="PQ" value="22" unit="1/30sek"/>
									<methodCode code="POT" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Målt af borger"/>
									<methodCode code="TPD" codeSystem="1.2.208.184.100.1"  
										codeSystemName="MedCom Message Codes" displayName="Indtastet af borger"/>								
								</observation>
							</component>
						</organizer>	
					</entry>						
				</section>
			</component>
		</structuredBody>
	</component>
</ClinicalDocument>
