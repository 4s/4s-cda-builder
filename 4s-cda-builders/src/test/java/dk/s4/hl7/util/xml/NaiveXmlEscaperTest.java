package dk.s4.hl7.util.xml;

import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.Assert;
import org.junit.Test;

import dk.s4.hl7.util.xml.NaiveXmlEscaper;

public class NaiveXmlEscaperTest {

  @Test
  public void sunshine() throws Exception {
    StringBuilder builder = new StringBuilder();
    String text = "This is a normal text string \n";
    NaiveXmlEscaper.escapeAndInsert(builder, text);
    Assert.assertEquals(text, builder.toString());
  }

  @Test
  public void testLowerThan() throws Exception {
    StringBuilder builder = new StringBuilder();
    String text = "This is a normal text string < \n";
    NaiveXmlEscaper.escapeAndInsert(builder, text);
    Assert.assertEquals(StringEscapeUtils.escapeXml10(text), builder.toString());
  }

  @Test
  public void testGreaterThan() throws Exception {
    StringBuilder builder = new StringBuilder();
    String text = "This is a normal text string > \n";
    NaiveXmlEscaper.escapeAndInsert(builder, text);
    Assert.assertEquals(StringEscapeUtils.escapeXml10(text), builder.toString());
  }

  @Test
  public void testQuote() throws Exception {
    StringBuilder builder = new StringBuilder();
    String text = "This is a normal text string \" \n";
    NaiveXmlEscaper.escapeAndInsert(builder, text);
    Assert.assertEquals(StringEscapeUtils.escapeXml10(text), builder.toString());
  }

  @Test
  public void testAnd() throws Exception {
    StringBuilder builder = new StringBuilder();
    String text = "This is a normal text string & \n";
    NaiveXmlEscaper.escapeAndInsert(builder, text);
    Assert.assertEquals(StringEscapeUtils.escapeXml10(text), builder.toString());
  }

  @Test
  public void testSingleQuote() throws Exception {
    StringBuilder builder = new StringBuilder();
    String text = "This is a normal text string \' \n";
    NaiveXmlEscaper.escapeAndInsert(builder, text);
    Assert.assertEquals(StringEscapeUtils.escapeXml10(text), builder.toString());
  }

  @Test
  public void testContinousSpecialChar() throws Exception {
    StringBuilder builder = new StringBuilder();
    String text = "This is a normal text string <>\" test1 &\' works\n";
    NaiveXmlEscaper.escapeAndInsert(builder, text);
    Assert.assertEquals(StringEscapeUtils.escapeXml10(text), builder.toString());
  }
}
