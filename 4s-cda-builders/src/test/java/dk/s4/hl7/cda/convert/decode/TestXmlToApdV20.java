package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.APDV20XmlCodec;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.encode.apd.v20.SetupMedcomV20Example1;
import dk.s4.hl7.cda.convert.encode.apd.v20.SetupMedcomV20Example2;
import dk.s4.hl7.cda.convert.encode.apd.v20.SetupMedcomV20ExampleV10UpgradeToV20;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.AppointmentEncounterCode;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public final class TestXmlToApdV20 extends BaseDecodeTest implements ConcurrencyTestCase {

  private APDV20XmlCodec codec;

  @Before
  public void before() {
    setCodec(new APDV20XmlCodec());
  }

  public void setCodec(APDV20XmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testApdV20XMLConverter() {
    try {
      String XML = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_apd-2-0.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertEquals(true, appointmentDocument.isRepeatingDocument());
      assertNotNull(appointmentDocument.getRepeatingDocumentGroupValue());
      assertEquals("06b2b3bb-dac5-446f-aa19-ed5c46d8b0b7", appointmentDocument.getRepeatingDocumentGroupValue());

      assertEquals(true, appointmentDocument.isGuidedInterval());
      assertNotNull(appointmentDocument.getGuidedIntervalText());
      assertEquals("Tidspunktet er vejledende", appointmentDocument.getGuidedIntervalText());

      assertEquals(AppointmentEncounterCode.MunicipalityAppointment, appointmentDocument.getAppointmentEncounterCode());
      assertNotNull(appointmentDocument.getAppointmentEncounterCode());

      assertNotNull(appointmentDocument.getCdaProfileAndVersion());
      assertEquals("apd-v2.0.1", appointmentDocument.getCdaProfileAndVersion());

      assertNotNull(appointmentDocument.getEpisodeOfCareLabel());
      assertEquals("DiabetesPackage", appointmentDocument.getEpisodeOfCareLabel());

      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList());
      assertEquals(2, appointmentDocument.getEpisodeOfCareIdentifierList().size());

      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0));
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0));
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getAuthorityName());
      assertEquals("MedCom", appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getAuthorityName());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getExtension());
      assertEquals("39d615cd-5d62-4a54-9762-d33197c63aba",
          appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getExtension());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getRoot());
      assertEquals("1.2.208.184", appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getRoot());

      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1));
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1));
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getAuthorityName());
      assertEquals("MedCom", appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getAuthorityName());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getExtension());
      assertEquals("e7532c08-729b-4413-83d7-bd2cdf147ef7",
          appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getExtension());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getRoot());
      assertEquals("1.2.208.184", appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getRoot());

      assertNotNull(appointmentDocument.getAuthor());
      assertNotNull(appointmentDocument.getAuthor().getTime());
      assertEquals(new GregorianCalendar(2019, 07, 16, 11, 00, 00).getTime(),
          appointmentDocument.getAuthor().getTime());

      assertNotNull(appointmentDocument.getIndicationDisplayName());
      assertEquals("Hjemmehjælp", appointmentDocument.getIndicationDisplayName());

      assertNotNull(appointmentDocument.getIndicationCode());
      assertNull(appointmentDocument.getIndicationCode().getCode());
      assertNotNull(appointmentDocument.getIndicationCode().getDisplayName());
      assertEquals("Hjemmehjælp", appointmentDocument.getIndicationCode().getDisplayName());
      assertNull(appointmentDocument.getIndicationCode().getCodeSystem());
      assertNull(appointmentDocument.getIndicationCode().getCodeSystemName());

      assertEquals(AppointmentDocument.AppointmentLocationTypeCode.HOME,
          appointmentDocument.getAppointmentLocationTypeCode());
      assertNull(appointmentDocument.getAppointmentLocation());
      assertEquals("Borgers Hjemmeadresse", appointmentDocument.getAppointmentLocationHomeAddress().getOrgName());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExample2() {

    try {
      String XML = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_apd-2-0_example2.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertEquals(false, appointmentDocument.isRepeatingDocument());
      assertNull(appointmentDocument.getRepeatingDocumentGroupValue());

      assertEquals(false, appointmentDocument.isGuidedInterval());
      assertNull(appointmentDocument.getGuidedIntervalText());

      assertEquals(AppointmentEncounterCode.MunicipalityAppointment, appointmentDocument.getAppointmentEncounterCode());
      assertNotNull(appointmentDocument.getAppointmentEncounterCode());

      assertNotNull(appointmentDocument.getCdaProfileAndVersion());
      assertEquals("apd-v2.0.1", appointmentDocument.getCdaProfileAndVersion());

      assertNotNull(appointmentDocument.getEpisodeOfCareLabel());
      assertEquals("DiabetesPackage", appointmentDocument.getEpisodeOfCareLabel());
      assertNull(appointmentDocument.getEpisodeOfCareLabelDisplayName());

      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList());
      assertEquals(2, appointmentDocument.getEpisodeOfCareIdentifierList().size());

      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0));
      assertNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getAuthorityName());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getExtension());
      assertEquals("39d615cd-5d62-4a54-9762-d33197c63aba",
          appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getExtension());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getRoot());
      assertEquals("1.2.208.184", appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getRoot());

      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1));
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getAuthorityName());
      assertEquals("MedCom", appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getAuthorityName());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getExtension());
      assertEquals("e7532c08-729b-4413-83d7-bd2cdf147ef7",
          appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getExtension());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getRoot());
      assertEquals("1.2.208.184", appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getRoot());

      assertNotNull(appointmentDocument.getAuthor());
      assertNotNull(appointmentDocument.getAuthor().getTime());
      assertEquals(new GregorianCalendar(2019, 07, 16, 10, 00, 00).getTime(),
          appointmentDocument.getAuthor().getTime());

      assertNotNull(appointmentDocument.getIndicationDisplayName());
      assertEquals("lægekonsultation med ambulant patient", appointmentDocument.getIndicationDisplayName());

      assertNotNull(appointmentDocument.getIndicationCode());
      assertNotNull(appointmentDocument.getIndicationCode().getCode());
      assertEquals("17436001", appointmentDocument.getIndicationCode().getCode());
      assertNotNull(appointmentDocument.getIndicationCode().getDisplayName());
      assertEquals("lægekonsultation med ambulant patient", appointmentDocument.getIndicationCode().getDisplayName());
      assertNotNull(appointmentDocument.getIndicationCode().getCodeSystem());
      assertEquals("2.16.840.1.113883.6.96", appointmentDocument.getIndicationCode().getCodeSystem());
      assertNull(appointmentDocument.getIndicationCode().getCodeSystemName());

      assertEquals(AppointmentDocument.AppointmentLocationTypeCode.HEALTH_ORGANIZATION,
          appointmentDocument.getAppointmentLocationTypeCode());
      assertNull(appointmentDocument.getAppointmentLocationHomeAddress());
      assertEquals("OUH Radiologisk Ambulatorium (Nyborg)", appointmentDocument.getAppointmentLocation().getOrgName());
      assertEquals("320161000016005", appointmentDocument.getAppointmentLocation().getId().getExtension());
      assertEquals("1.2.208.176.1.1", appointmentDocument.getAppointmentLocation().getId().getRoot());
      assertEquals("SOR", appointmentDocument.getAppointmentLocation().getId().getAuthorityName());
      assertEquals("Address: 5800 Nyborg Street: [Vestergade 17,] Danmark (Use: WorkPlace)",
          appointmentDocument.getAppointmentLocation().getAddress().toString());
      assertEquals("tel", appointmentDocument.getAppointmentLocation().getTelecomList()[0].getProtocol());
      assertEquals("66113333-4", appointmentDocument.getAppointmentLocation().getTelecomList()[0].getValue());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExample3() {

    try {
      String XML = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_apd-2-0_example3.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertEquals(AppointmentDocument.AppointmentLocationTypeCode.NON_SOR,
          appointmentDocument.getAppointmentLocationTypeCode());
      assertNull(appointmentDocument.getAppointmentLocationHomeAddress());
      assertEquals("http", appointmentDocument.getAppointmentLocation().getTelecomList()[0].getProtocol());
      assertEquals("//minlaegeapp.dk", appointmentDocument.getAppointmentLocation().getTelecomList()[0].getValue());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExampleV10UpgradedTOV20() {

    try {
      String XML = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_1 _V10_upgraded_to_V20.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertEquals(false, appointmentDocument.isRepeatingDocument());
      assertNull(appointmentDocument.getRepeatingDocumentGroupValue());

      assertEquals(false, appointmentDocument.isGuidedInterval());
      assertNull(appointmentDocument.getGuidedIntervalText());

      assertEquals(AppointmentEncounterCode.RegionalAppointment, appointmentDocument.getAppointmentEncounterCode());
      assertNotNull(appointmentDocument.getAppointmentEncounterCode());

      assertNotNull(appointmentDocument.getCdaProfileAndVersion());
      assertEquals("apd-v2.0.1", appointmentDocument.getCdaProfileAndVersion());

      assertNull(appointmentDocument.getEpisodeOfCareLabel());
      assertNull(appointmentDocument.getEpisodeOfCareLabelDisplayName());

      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList());
      assertEquals(0, appointmentDocument.getEpisodeOfCareIdentifierList().size());

      assertNotNull(appointmentDocument.getAuthor());
      assertNotNull(appointmentDocument.getAuthor().getTime());
      assertEquals(new GregorianCalendar(2017, 01, 16, 10, 00, 00).getTime(),
          appointmentDocument.getAuthor().getTime());

      assertNotNull(appointmentDocument.getIndicationDisplayName());
      assertEquals("Ekkokardiografi (Ultralydsundersøgelse af hjertet)",
          appointmentDocument.getIndicationDisplayName());

      assertNotNull(appointmentDocument.getIndicationCode());
      assertNull(appointmentDocument.getIndicationCode().getCode());
      assertNotNull(appointmentDocument.getIndicationCode().getDisplayName());
      assertEquals("Ekkokardiografi (Ultralydsundersøgelse af hjertet)",
          appointmentDocument.getIndicationCode().getDisplayName());
      assertNull(appointmentDocument.getIndicationCode().getCodeSystem());
      assertNull(appointmentDocument.getIndicationCode().getCodeSystemName());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExample_1_6_apd_gp() {
    //Test specific problems found while testing the 1.6 example

    try {
      String XML = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_1_6_apd_gp_adj.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertNotNull(appointmentDocument);
      assertNotNull(appointmentDocument.getAppointmentPerformer());
      assertNull(appointmentDocument.getAppointmentPerformer().getPersonIdentity());
      assertEquals(0, appointmentDocument.getAppointmentPerformer().getAddress().getStreet().length);
      assertEquals(0, appointmentDocument.getAppointmentPerformer().getTelecomList().length);
      assertEquals("Lægehuset Valdemarsgade",
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getOrgName());
      assertEquals("Valdemarsgade 10",
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getAddress().getStreet()[0]);
      assertEquals("Svendborg",
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getAddress().getCity());
      assertEquals("5700",
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getAddress().getPostalCode());

      assertNotNull(appointmentDocument.getProviderOrganization());
      assertEquals("393421000016009", appointmentDocument.getProviderOrganization().getIdValue());
      assertEquals("Lægehuset Valdemarsgade", appointmentDocument.getProviderOrganization().getOrgName());
      assertEquals("Valdemarsgade 10", appointmentDocument.getProviderOrganization().getAddress().getStreet()[0]);
      assertEquals("Lægehuset", appointmentDocument.getProviderOrganization().getAddress().getStreet()[1]);
      assertEquals("Svendborg", appointmentDocument.getProviderOrganization().getAddress().getCity());
      assertEquals("5700", appointmentDocument.getProviderOrganization().getAddress().getPostalCode());
      assertEquals("Danmark", appointmentDocument.getProviderOrganization().getAddress().getCountry());
      assertEquals(Use.WorkPlace, appointmentDocument.getProviderOrganization().getAddress().getAddressUse());
      assertEquals(3, appointmentDocument.getProviderOrganization().getTelecomList().length);
      assertEquals("65119999", appointmentDocument.getProviderOrganization().getTelecomList()[0].getValue());
      assertEquals(Use.WorkPlace, appointmentDocument.getProviderOrganization().getTelecomList()[0].getAddressUse());
      assertEquals("tel", appointmentDocument.getProviderOrganization().getTelecomList()[0].getProtocol());
      assertEquals("laege@valdemar.dk", appointmentDocument.getProviderOrganization().getTelecomList()[1].getValue());
      assertEquals(Use.HomeAddress, appointmentDocument.getProviderOrganization().getTelecomList()[1].getAddressUse());
      assertEquals("mailto", appointmentDocument.getProviderOrganization().getTelecomList()[1].getProtocol());
      assertEquals("//www.LeagehusetValdemar.dk",
          appointmentDocument.getProviderOrganization().getTelecomList()[2].getValue());
      assertEquals(Use.HomeAddress, appointmentDocument.getProviderOrganization().getTelecomList()[2].getAddressUse());
      assertEquals("http", appointmentDocument.getProviderOrganization().getTelecomList()[2].getProtocol());

      assertNull(appointmentDocument.getAuthorReferrer());
      assertNotNull(appointmentDocument.getIndicationDisplayName());
      assertEquals("Lægekonsultation", appointmentDocument.getIndicationDisplayName());

      assertNotNull(appointmentDocument.getIndicationCode());
      assertNull(appointmentDocument.getIndicationCode().getCode());
      assertNotNull(appointmentDocument.getIndicationCode().getDisplayName());
      assertEquals("Lægekonsultation", appointmentDocument.getIndicationCode().getDisplayName());
      assertNull(appointmentDocument.getIndicationCode().getCodeSystem());
      assertNull(appointmentDocument.getIndicationCode().getCodeSystemName());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExample_1_6_apd_gp_v2() {
    //Test specific problems found while testing the 1.6 example

    try {
      String XML = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_1_6_apd_gp_v2_adj.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertNotNull(appointmentDocument);

      assertNotNull(appointmentDocument.getCdaProfileAndVersion());
      assertEquals("apd-v2.0.1", appointmentDocument.getCdaProfileAndVersion());

      assertNotNull(appointmentDocument.getAuthor());
      assertNotNull(appointmentDocument.getAuthor().getPersonIdentity());
      assertNotNull(appointmentDocument.getAuthor().getPersonIdentity().getFamilyName());
      assertEquals("Jensen", appointmentDocument.getAuthor().getPersonIdentity().getFamilyName());

      assertNotNull(appointmentDocument.getAuthorReferrer());
      assertNotNull(appointmentDocument.getAuthorReferrer().getPersonIdentity());
      assertNotNull(appointmentDocument.getAuthorReferrer().getPersonIdentity().getFamilyName());
      assertEquals("Strøm", appointmentDocument.getAuthorReferrer().getPersonIdentity().getFamilyName());

      assertNotNull(appointmentDocument.getIndicationDisplayName());
      assertEquals("Speciallægekonsultation", appointmentDocument.getIndicationDisplayName());

      assertNotNull(appointmentDocument.getIndicationCode());
      assertNull(appointmentDocument.getIndicationCode().getCode());
      assertNotNull(appointmentDocument.getIndicationCode().getDisplayName());
      assertEquals("Speciallægekonsultation", appointmentDocument.getIndicationCode().getDisplayName());
      assertNull(appointmentDocument.getIndicationCode().getCodeSystem());
      assertNull(appointmentDocument.getIndicationCode().getCodeSystemName());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExample_1_4_apd_municipality_a() {
    //Test specific problems found while testing the 1.6 example

    try {
      String XML = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_1_4_apd_municipality_a_adj.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertNotNull(appointmentDocument);
      assertNotNull(appointmentDocument.getConfidentialityCode());

      assertNotNull(appointmentDocument.getAppointmentPerformer());
      assertNull(appointmentDocument.getAppointmentPerformer().getPersonIdentity());
      assertNotNull(appointmentDocument.getAppointmentPerformer().getAddress());
      assertNotNull(appointmentDocument.getAppointmentPerformer().getAddress().getStreet());
      assertEquals(2, appointmentDocument.getAppointmentPerformer().getAddress().getStreet().length);
      assertEquals("Odense Kommune,\n" + "                      Sundhedsenhedsforvaltningen",
          appointmentDocument.getAppointmentPerformer().getAddress().getStreet()[0]);
      assertEquals("Ørbækvej 100", appointmentDocument.getAppointmentPerformer().getAddress().getStreet()[1]);
      assertNotNull(appointmentDocument.getAppointmentPerformer().getTelecomList());
      assertEquals(1, appointmentDocument.getAppointmentPerformer().getTelecomList().length);
      assertNotNull(appointmentDocument.getAppointmentPerformer().getTelecomList()[0].getValue());
      assertEquals("66113333-3", appointmentDocument.getAppointmentPerformer().getTelecomList()[0].getValue());
      assertNotNull(appointmentDocument.getAppointmentPerformer().getTelecomList()[0].getProtocol());
      assertEquals("tel", appointmentDocument.getAppointmentPerformer().getTelecomList()[0].getProtocol());
      assertNotNull(appointmentDocument.getAppointmentPerformer().getTelecomList()[0].getAddressUse());
      assertEquals(Use.WorkPlace, appointmentDocument.getAppointmentPerformer().getTelecomList()[0].getAddressUse());

      assertEquals("Hjemmepleje-enhed Midtbyen",
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getOrgName());

      assertNotNull(appointmentDocument.getProviderOrganization());
      assertEquals("393421000016009", appointmentDocument.getProviderOrganization().getIdValue());
      assertEquals("Lægehuset Valdemarsgade", appointmentDocument.getProviderOrganization().getOrgName());
      assertEquals("Munkerisvej 10", appointmentDocument.getProviderOrganization().getAddress().getStreet()[0]);
      assertEquals("Lægehuset", appointmentDocument.getProviderOrganization().getAddress().getStreet()[1]);
      assertEquals("Odense M", appointmentDocument.getProviderOrganization().getAddress().getCity());
      assertEquals("5230", appointmentDocument.getProviderOrganization().getAddress().getPostalCode());
      assertEquals("Danmark", appointmentDocument.getProviderOrganization().getAddress().getCountry());
      assertEquals(Use.WorkPlace, appointmentDocument.getProviderOrganization().getAddress().getAddressUse());
      assertEquals(3, appointmentDocument.getProviderOrganization().getTelecomList().length);
      assertEquals("65119999", appointmentDocument.getProviderOrganization().getTelecomList()[0].getValue());
      assertEquals(Use.WorkPlace, appointmentDocument.getProviderOrganization().getTelecomList()[0].getAddressUse());
      assertEquals("tel", appointmentDocument.getProviderOrganization().getTelecomList()[0].getProtocol());
      assertEquals("laege@valdemar.dk", appointmentDocument.getProviderOrganization().getTelecomList()[1].getValue());
      assertEquals(Use.HomeAddress, appointmentDocument.getProviderOrganization().getTelecomList()[1].getAddressUse());
      assertEquals("mailto", appointmentDocument.getProviderOrganization().getTelecomList()[1].getProtocol());
      assertEquals("//www.LeagehusetValdemar.dk",
          appointmentDocument.getProviderOrganization().getTelecomList()[2].getValue());
      assertEquals(Use.HomeAddress, appointmentDocument.getProviderOrganization().getTelecomList()[2].getAddressUse());
      assertEquals("http", appointmentDocument.getProviderOrganization().getTelecomList()[2].getProtocol());

      assertNull(appointmentDocument.getAppointmentLocation());
      assertNotNull(appointmentDocument.getAppointmentLocationHomeAddress());
      assertNotNull(appointmentDocument.getAppointmentLocationHomeAddress().getAddress());
      assertNotNull(appointmentDocument.getAppointmentLocationHomeAddress().getAddress().getStreet());
      assertEquals(2, appointmentDocument.getAppointmentLocationHomeAddress().getAddress().getStreet().length);
      assertEquals("Forskerparken 10",
          appointmentDocument.getAppointmentLocationHomeAddress().getAddress().getStreet()[0]);
      assertEquals("MedCom", appointmentDocument.getAppointmentLocationHomeAddress().getAddress().getStreet()[1]);

      assertNotNull(appointmentDocument.getAppointmentLocationHomeAddress().getAddress().getCity());
      assertEquals("Odense M", appointmentDocument.getAppointmentLocationHomeAddress().getAddress().getCity());
      assertNotNull(appointmentDocument.getAppointmentLocationHomeAddress().getAddress().getPostalCode());
      assertEquals("5230", appointmentDocument.getAppointmentLocationHomeAddress().getAddress().getPostalCode());
      assertNotNull(appointmentDocument.getAppointmentLocationHomeAddress().getAddress().getAddressUse());
      assertEquals(Use.HomeAddress,
          appointmentDocument.getAppointmentLocationHomeAddress().getAddress().getAddressUse());

      assertNotNull(appointmentDocument.getAppointmentLocationHomeAddress().getOrgName());
      assertEquals("Borgers Hjemmeadresse", appointmentDocument.getAppointmentLocationHomeAddress().getOrgName());
      assertNotNull(appointmentDocument.getAppointmentLocationHomeAddress().getTelecomList());
      assertEquals(1, appointmentDocument.getAppointmentLocationHomeAddress().getTelecomList().length);
      assertEquals("69894534", appointmentDocument.getAppointmentLocationHomeAddress().getTelecomList()[0].getValue());
      assertEquals("tel", appointmentDocument.getAppointmentLocationHomeAddress().getTelecomList()[0].getProtocol());

      assertTrue(appointmentDocument.isRepeatingDocument());
      assertNotNull(appointmentDocument.getRepeatingDocumentGroupValue());
      assertEquals("718a7c0d-bf7d-4162-ba28-e3dd720798eb", appointmentDocument.getRepeatingDocumentGroupValue());

      assertTrue(appointmentDocument.isGuidedInterval());
      assertNotNull(appointmentDocument.getGuidedIntervalText());
      assertEquals("Tidspunktet er vejledende", appointmentDocument.getGuidedIntervalText());

      assertNotNull(appointmentDocument.getIndicationDisplayName());
      assertEquals("Hjemmehjælp", appointmentDocument.getIndicationDisplayName());

      assertNotNull(appointmentDocument.getIndicationCode());
      assertNull(appointmentDocument.getIndicationCode().getCode());
      assertNotNull(appointmentDocument.getIndicationCode().getDisplayName());
      assertEquals("Hjemmehjælp", appointmentDocument.getIndicationCode().getDisplayName());
      assertNull(appointmentDocument.getIndicationCode().getCodeSystem());
      assertNull(appointmentDocument.getIndicationCode().getCodeSystemName());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExample_1_5_apd_region() {
    //Test specific problems found while testing the 1.6 example

    try {
      String XML = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_1_5_apd_region_adj.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertNotNull(appointmentDocument);

      assertNotNull(appointmentDocument.getCdaProfileAndVersion());
      assertEquals("apd-v2.0.1", appointmentDocument.getCdaProfileAndVersion());

      assertNotNull(appointmentDocument.getEpisodeOfCareLabel());
      assertEquals("ALAL21", appointmentDocument.getEpisodeOfCareLabel());

      assertNotNull(appointmentDocument.getEpisodeOfCareLabelDisplayName());
      assertEquals("Kronisk obstruktiv lungesygdom (KOL)  ", appointmentDocument.getEpisodeOfCareLabelDisplayName());

      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList());
      assertEquals(2, appointmentDocument.getEpisodeOfCareIdentifierList().size());

      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0));
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getAuthorityName());
      assertEquals("Sundhedsvæsenets Klassifikations System",
          appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getAuthorityName());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getExtension());
      assertEquals("cf0b2a79-dff1-4cdc-87d3-4e0c1fa7e991",
          appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getExtension());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getRoot());
      assertEquals("1.2.208.176.7.1.10.83", appointmentDocument.getEpisodeOfCareIdentifierList().get(0).getRoot());

      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1));
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getAuthorityName());
      assertEquals("Sundhedsvæsenets Klassifikations System",
          appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getAuthorityName());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getExtension());
      assertEquals("11ea56fe-307f-4f54-8c37-ed09fcf73179",
          appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getExtension());
      assertNotNull(appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getRoot());
      assertEquals("1.2.208.176.7.1.10.83", appointmentDocument.getEpisodeOfCareIdentifierList().get(1).getRoot());

      assertNotNull(appointmentDocument.getAppointmentPerformer());
      assertNull(appointmentDocument.getAppointmentPerformer().getPersonIdentity());
      assertNotNull(appointmentDocument.getAppointmentPerformer().getAddress());
      assertNotNull(appointmentDocument.getAppointmentPerformer().getAddress().getStreet());
      assertEquals(0, appointmentDocument.getAppointmentPerformer().getAddress().getStreet().length);
      assertNotNull(appointmentDocument.getAppointmentPerformer().getAddress().getAddressUse());
      assertNotNull(appointmentDocument.getAppointmentPerformer().getTelecomList());
      assertEquals(0, appointmentDocument.getAppointmentPerformer().getTelecomList().length);
      assertNotNull(appointmentDocument.getAppointmentPerformer().getId());
      assertNotNull(appointmentDocument.getAppointmentPerformer().getId().getExtension());
      assertEquals("325441000016006", appointmentDocument.getAppointmentPerformer().getId().getExtension());

      assertNotNull(appointmentDocument.getAppointmentPerformer().getOrganizationIdentity());
      assertNotNull(appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getOrgName());
      assertEquals("Regionshospitalet",
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getOrgName());

      assertNotNull(appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getAddress());
      assertNotNull(appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getAddress().getStreet());
      assertEquals(1,
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getAddress().getStreet().length);
      assertEquals("Albanigade 12",
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getAddress().getStreet()[0]);
      assertNotNull(appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getTelecomList());
      assertEquals(1, appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getTelecomList().length);
      assertNotNull(
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getTelecomList()[0].getValue());
      assertEquals("66113322",
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getTelecomList()[0].getValue());
      assertNotNull(
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getTelecomList()[0].getProtocol());
      assertEquals("tel",
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getTelecomList()[0].getProtocol());
      assertNotNull(
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getTelecomList()[0].getAddressUse());
      assertEquals(Use.WorkPlace,
          appointmentDocument.getAppointmentPerformer().getOrganizationIdentity().getTelecomList()[0].getAddressUse());

      assertNotNull(appointmentDocument.getProviderOrganization());
      assertEquals("393421000016009", appointmentDocument.getProviderOrganization().getIdValue());
      assertEquals("Lægehuset Valdemarsgade", appointmentDocument.getProviderOrganization().getOrgName());
      assertEquals("Munkerisvej 10", appointmentDocument.getProviderOrganization().getAddress().getStreet()[0]);
      assertEquals("Lægehuset", appointmentDocument.getProviderOrganization().getAddress().getStreet()[1]);
      assertEquals("Odense M", appointmentDocument.getProviderOrganization().getAddress().getCity());
      assertEquals("5230", appointmentDocument.getProviderOrganization().getAddress().getPostalCode());
      assertEquals("Danmark", appointmentDocument.getProviderOrganization().getAddress().getCountry());
      assertEquals(Use.WorkPlace, appointmentDocument.getProviderOrganization().getAddress().getAddressUse());
      assertEquals(3, appointmentDocument.getProviderOrganization().getTelecomList().length);
      assertEquals("65119999", appointmentDocument.getProviderOrganization().getTelecomList()[0].getValue());
      assertEquals(Use.WorkPlace, appointmentDocument.getProviderOrganization().getTelecomList()[0].getAddressUse());
      assertEquals("tel", appointmentDocument.getProviderOrganization().getTelecomList()[0].getProtocol());
      assertEquals("laege@valdemar.dk", appointmentDocument.getProviderOrganization().getTelecomList()[1].getValue());
      assertEquals(Use.HomeAddress, appointmentDocument.getProviderOrganization().getTelecomList()[1].getAddressUse());
      assertEquals("mailto", appointmentDocument.getProviderOrganization().getTelecomList()[1].getProtocol());
      assertEquals("//www.LeagehusetValdemar.dk",
          appointmentDocument.getProviderOrganization().getTelecomList()[2].getValue());
      assertEquals(Use.HomeAddress, appointmentDocument.getProviderOrganization().getTelecomList()[2].getAddressUse());
      assertEquals("http", appointmentDocument.getProviderOrganization().getTelecomList()[2].getProtocol());

      assertNotNull(appointmentDocument.getAppointmentLocation());
      assertNull(appointmentDocument.getAppointmentLocationHomeAddress());
      assertNotNull(appointmentDocument.getAppointmentLocation().getAddress());
      assertNotNull(appointmentDocument.getAppointmentLocation().getAddress().getStreet());
      assertEquals(2, appointmentDocument.getAppointmentLocation().getAddress().getStreet().length);
      assertEquals("Regionshospitalet", appointmentDocument.getAppointmentLocation().getAddress().getStreet()[0]);
      assertEquals("Albanigade 10", appointmentDocument.getAppointmentLocation().getAddress().getStreet()[1]);

      assertNotNull(appointmentDocument.getAppointmentLocation().getAddress().getCity());
      assertEquals("Odense", appointmentDocument.getAppointmentLocation().getAddress().getCity());
      assertNotNull(appointmentDocument.getAppointmentLocation().getAddress().getPostalCode());
      assertEquals("5000", appointmentDocument.getAppointmentLocation().getAddress().getPostalCode());
      assertNotNull(appointmentDocument.getAppointmentLocation().getAddress().getAddressUse());
      assertEquals(Use.WorkPlace, appointmentDocument.getAppointmentLocation().getAddress().getAddressUse());

      assertNotNull(appointmentDocument.getAppointmentLocation().getOrgName());
      assertEquals("opgang 4, 3 sal rum 101", appointmentDocument.getAppointmentLocation().getOrgName());
      assertNotNull(appointmentDocument.getAppointmentLocation().getTelecomList());
      assertEquals(1, appointmentDocument.getAppointmentLocation().getTelecomList().length);
      assertEquals("63221313", appointmentDocument.getAppointmentLocation().getTelecomList()[0].getValue());
      assertEquals("tel", appointmentDocument.getAppointmentLocation().getTelecomList()[0].getProtocol());

      assertEquals("lægekonsultation med ambulant patient", appointmentDocument.getIndicationDisplayName());

      assertFalse(appointmentDocument.isRepeatingDocument());
      assertNull(appointmentDocument.getRepeatingDocumentGroupValue());

      assertFalse(appointmentDocument.isGuidedInterval());
      assertNull(appointmentDocument.getGuidedIntervalText());

      assertNotNull(appointmentDocument.getIndicationDisplayName());
      assertEquals("lægekonsultation med ambulant patient", appointmentDocument.getIndicationDisplayName());

      assertNotNull(appointmentDocument.getIndicationCode());
      assertNotNull(appointmentDocument.getIndicationCode().getCode());
      assertEquals("17436001", appointmentDocument.getIndicationCode().getCode());
      assertNotNull(appointmentDocument.getIndicationCode().getDisplayName());
      assertEquals("lægekonsultation med ambulant patient", appointmentDocument.getIndicationCode().getDisplayName());
      assertNotNull(appointmentDocument.getIndicationCode().getCodeSystem());
      assertEquals("2.16.840.1.113883.6.96", appointmentDocument.getIndicationCode().getCodeSystem());
      assertNotNull(appointmentDocument.getIndicationCode().getCodeSystemName());
      assertEquals("SNOMED CT", appointmentDocument.getIndicationCode().getCodeSystemName());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XmlConverterMedcomExample1() throws Exception {
    encodeDecodeAndCompare(new APDV20XmlCodec(), SetupMedcomV20Example1.createBaseAppointmentDocument());
  }

  @Test
  public void testApdV20XmlConverterMedcomExample2() throws Exception {
    encodeDecodeAndCompare(new APDV20XmlCodec(), SetupMedcomV20Example2.createBaseAppointmentDocument());
  }

  @Test
  public void testApdV20XmlConverterMedcomExampleV10UpgradedTOV20() throws Exception {
    encodeDecodeAndCompare(new APDV20XmlCodec(), SetupMedcomV20ExampleV10UpgradeToV20.createBaseAppointmentDocument());
  }

  @Override
  public void runTest() throws Exception {
    testApdV20XmlConverterMedcomExample1();
  }

}
