package dk.s4.hl7.cda.convert.encode.apd;

import org.junit.Test;

import dk.s4.hl7.cda.convert.APDXmlCodec;
import dk.s4.hl7.cda.convert.APDXmlCodec.Version;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.StatusV10;
import dk.s4.hl7.cda.model.apd.StatusV11;

/**
 * Validate the output from our own appointment builder with the example from
 * MedCom.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public final class TestAgainstMedComApdExamples extends BaseEncodeTest<AppointmentDocument>
    implements ConcurrencyTestCase {

  private static final APDXmlCodec APD_XML_CODEC_V10 = new APDXmlCodec(Version.V10);
  private static final APDXmlCodec APD_XML_CODEC_V11 = new APDXmlCodec(Version.V11);

  public TestAgainstMedComApdExamples() {
    super("src/test/resources/apd/");
  }

  @Test
  public void shouldMatchExpectedValueV10() throws Exception {
    performTest(APD_XML_CODEC_V10, "DK-APD_Example_1.xml",
        SetupMedcomExample.createBaseAppointmentDocument(StatusV10.ACTIVE));
  }

  @Test
  public void shouldMatchExpectedValueV11() throws Exception {
    performTest(APD_XML_CODEC_V11, "DK-APD_Example_1.xml",
        SetupMedcomExample.createBaseAppointmentDocument(StatusV11.ACTIVE));
  }

  @Override
  public void runTest() throws Exception {
    shouldMatchExpectedValueV10();
    shouldMatchExpectedValueV11();
  }
}