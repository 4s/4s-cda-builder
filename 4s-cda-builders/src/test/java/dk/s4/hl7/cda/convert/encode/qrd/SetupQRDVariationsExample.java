package dk.s4.hl7.cda.convert.encode.qrd;

import java.io.IOException;
import java.net.URISyntaxException;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.*;
import dk.s4.hl7.cda.model.Reference.DocumentIdReferencesUse;
import dk.s4.hl7.cda.model.Reference.ReferenceBuilder;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse.QRDAnalogSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse.QRDMultipleChoiceResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse.QRDNumericResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer.QRDOrganizerBuilder;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse.QRDTextResponseBuilder;

public final class SetupQRDVariationsExample extends QRDExampleBase {
  private static final String QUESTION = "question";
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";

  private QRDResponse responseWithReference() {
    return responseWithReference(QUESTION, "simpleId", "Blahh", simpleDocumentReference());
  }

  private Reference simpleDocumentReference() {
    return new ReferenceBuilder(DocumentIdReferencesUse.CDA_DOCUMENT_ID_REFERENCE.getReferencesUse(),
        MedCom.createId("referenceId"), Loinc.createPHMRTypeCode()).build();
  }

  private QRDResponse responseNoReference() {
    return new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "2", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setText("Blahh1234")
        .build();
  }

  private QRDResponse responseTextNoDisplayName() {
    return new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setText("Blahh1234")
        .build();
  }

  protected QRDResponse responseTextNoResponseValue() {
    return responseTextNoResponseValue(QUESTION);
  }

  protected QRDResponse responseTextNoResponseValue(String question) {
    return new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(question)
        .setText(null)
        .build();
  }

  protected QRDAnalogSliderResponse responseAnalogSlider(boolean includeInterval, boolean includeDisplayName) {
    QRDAnalogSliderResponseBuilder qResponse = new QRDAnalogSliderResponseBuilder()
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setValue("2", BasicType.INT.name());
    if (includeDisplayName) {
      qResponse.setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "2", "CodeSystemName"));
    } else {
      qResponse.setCodeValue(new CodedValue("code", "codeSystem", "CodeSystemName"));
    }

    if (includeInterval) {
      qResponse.setInterval("1", "100", "1");
    }
    return qResponse.build();
  }

  protected QRDResponse responseNumeric(boolean includeInterval, boolean includeDisplayName) {
    QRDNumericResponseBuilder qResponse = new QRDNumericResponseBuilder()
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setValue("1.60", BasicType.REAL);
    if (includeDisplayName) {
      qResponse.setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "2", "CodeSystemName"));
    } else {
      qResponse.setCodeValue(new CodedValue("code", "codeSystem", "CodeSystemName"));
    }
    if (includeInterval) {
      qResponse.setInterval("1,00", "2,00", IntervalType.IVL_REAL);
    }
    return qResponse.build();
  }

  protected QRDResponse responseMultipleChoice(boolean includeDisplayName, QRDTextResponse associatedTextResponse) {
    QRDMultipleChoiceResponseBuilder qResponse = new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "1", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setInterval(2, 5)
        .addAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .addAnswer("answerCode2", "answerCodeSystem2", "answerDisplayName2", "answerCodeSystemName2")
        .addAnswer("answerCode3", "answerCodeSystem3", "answerDisplayName3", "answerCodeSystemName3")
        .setAssociatedTextResponse(associatedTextResponse);
    if (includeDisplayName) {
      qResponse.setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "2", "CodeSystemName"));
    } else {
      qResponse.setCodeValue(new CodedValue("code", "codeSystem", "CodeSystemName"));
    }
    return qResponse.build();
  }

  protected QRDResponse responseDiscreteSlider(boolean includeDisplayName, QRDTextResponse associatedTextResponse) {
    QRDDiscreteSliderResponseBuilder qResponse = new QRDDiscreteSliderResponseBuilder()
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setMinimum(0)
        .setAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .setAssociatedTextResponse(associatedTextResponse);
    if (includeDisplayName) {
      qResponse.setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "2", "CodeSystemName"));
    } else {
      qResponse.setCodeValue(new CodedValue("code", "codeSystem", "CodeSystemName"));
    }
    return qResponse.build();
  }

  private Section<QRDOrganizer> createSectionWithResponses(String title) throws IOException, URISyntaxException {
    Section<QRDOrganizer> section = new Section<QRDOrganizer>(title, "Questions");
    section.addOrganizer(new QRDOrganizerBuilder()
        .addQRDResponse(responseWithReference())
        .addQRDResponse(responseNoReference())
        .build());
    return section;
  }

  private Section<QRDOrganizer> createSectionWithTextResponses(String title, boolean includeDisplayName)
      throws IOException, URISyntaxException {
    return createSectionWithTextResponses(title, includeDisplayName, "Questions");
  }

  private Section<QRDOrganizer> createSectionWithTextResponses(String title, boolean includeDisplayName, String text)
      throws IOException, URISyntaxException {
    Section<QRDOrganizer> section = new Section<QRDOrganizer>(title, text);

    if (includeDisplayName) {
      section.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(responseNoReference()).build());
    } else {
      section.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(responseTextNoDisplayName()).build());
    }

    return section;
  }

  private Section<QRDOrganizer> createSectionWithNumericResponses(String title, boolean includeInterval,
      boolean includeDisplayName) throws IOException, URISyntaxException {
    Section<QRDOrganizer> section = new Section<QRDOrganizer>(title, "Questions");
    section.addOrganizer(
        new QRDOrganizerBuilder().addQRDResponse(responseNumeric(includeInterval, includeDisplayName)).build());
    return section;
  }

  private Section<QRDOrganizer> createSectionWithMultipleChoiceResponses(String title, boolean includeDisplayName)
      throws IOException, URISyntaxException {
    Section<QRDOrganizer> section = new Section<QRDOrganizer>(title, "Questions");
    section.addOrganizer(
        new QRDOrganizerBuilder().addQRDResponse(responseMultipleChoice(includeDisplayName, null)).build());
    return section;
  }

  private Section<QRDOrganizer> createSectionWithDiscreteSliderResponses(String title, boolean includeDisplayName)
      throws IOException, URISyntaxException {
    Section<QRDOrganizer> section = new Section<QRDOrganizer>(title, "Questions");
    section.addOrganizer(
        new QRDOrganizerBuilder().addQRDResponse(responseDiscreteSlider(includeDisplayName, null)).build());
    return section;
  }

  private Section<QRDOrganizer> createSectionWithAnalogSliderResponses(String title, boolean includeInterval,
      boolean includeDisplayName) throws IOException, URISyntaxException {
    Section<QRDOrganizer> section = new Section<QRDOrganizer>(title, "Questions");
    section.addOrganizer(
        new QRDOrganizerBuilder().addQRDResponse(responseAnalogSlider(includeInterval, includeDisplayName)).build());
    return section;
  }

  protected QRDResponse responseWithReference(String question, String id, String text, Reference reference) {
    return new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", question, "CodeSystemName"))
        .setId(MedCom.createId(id))
        .setQuestion(question)
        .setText(text)
        .addReference(reference)
        .build();
  }

  private Section createCopyrightSection(String title, String text) {
    Section section = new Section(title, text, "da-DK");
    section.addCopyrightText(text);
    return section;
  }

  @Override
  public QRDDocument createDocument() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    return cda;
  }

  public QRDDocument createDocumentTitleNull() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    cda.addSection(createSectionWithResponses(null));
    cda.addSection(createSectionWithResponses(null));

    return cda;
  }

  public QRDDocument createDocumentNumericResponseNoInterval() throws Exception {

    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    cda.addSection(createSectionWithNumericResponses("Questions", false, true));
    return cda;
  }

  public QRDDocument createDocumentAnalogSliderResponseNoInterval() throws Exception {

    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    cda.addSection(createSectionWithAnalogSliderResponses("Questions", false, true));
    return cda;

  }

  public QRDDocument createDocumentResponseNoDisplayName() throws Exception {

    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    cda.addSection(createSectionWithTextResponses("Questions", false));
    cda.addSection(createSectionWithAnalogSliderResponses("Questions", true, false));
    cda.addSection(createSectionWithNumericResponses("Questions", false, false));
    cda.addSection(createSectionWithMultipleChoiceResponses("Questions", false));
    cda.addSection(createSectionWithDiscreteSliderResponses("Questions", false));
    return cda;
  }

  public QRDDocument createDocumentWithCopyRightAndQuestionInTwoSections() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    cda.addSection(createSectionWithTextResponses("Questions1", false));
    cda.addSection(createSectionWithTextResponses("Questions2", false));
    cda.addSection(createCopyrightSection("Copyright title", "Copyright tekst"));
    return cda;
  }

  public QRDDocument createDocumentWithMultipleCopyRightAndQuestions() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);

    Section<QRDOrganizer> section1copyright = new Section("Copyright title 1",
        "Copyright tekst1.1 + Copyright tekst1.2", "da-DK");
    section1copyright.addCopyrightText("Copyright tekst1.1");
    section1copyright.addCopyrightText("Copyright tekst1.2");
    cda.addSection(section1copyright);

    cda.addSection(createSectionWithTextResponses("Questions1", false));

    Section section2copyright = new Section("Copyright title 2", "Copyright tekst2.1", "da-DK");
    section2copyright.addCopyrightText("Copyright tekst2.1");
    cda.addSection(section2copyright);

    Section section3copyright = new Section("Copyright title 3", "Copyright tekst3.1", "da-DK");
    section3copyright.addCopyrightText("Copyright tekst3.1");
    cda.addSection(section3copyright);

    cda.addSection(createSectionWithTextResponses("Questions2", false));

    Section sectionLastcopyright = new Section("Copyright title sidste", "Copyright tekst sidste", "da-DK");
    sectionLastcopyright.addCopyrightText("Copyright tekst sidste");
    cda.addSection(sectionLastcopyright);

    return cda;
  }

  public QRDDocument createDocumentWithOneCopyRightNoTextTreeTextAndOneQuestion() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    String narrativText = "Question1<br/>Question2";
    cda.addSection(createSectionWithTextResponses("Questions1", false, narrativText));

    Section<QRDOrganizer> section1copyright = new Section("Copyright title 1", null, "da-DK");
    section1copyright.addCopyrightText("Copyright tekst1.1");
    section1copyright.addCopyrightText("Copyright tekst1.2");
    section1copyright.addCopyrightText("Copyright tekst1.3");
    cda.addSection(section1copyright);

    return cda;
  }
}
