package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.QFDDXmlCodec;
import dk.s4.hl7.cda.model.QuestionnaireMedia;
import dk.s4.hl7.cda.model.qfdd.QFDDAnalogSliderQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDMultipleChoiceQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDNumericQuestion;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public class TestXmlToQfdd extends BaseDecodeTest implements ConcurrencyTestCase {
  private QFDDXmlCodec codec;

  @Before
  public void before() {
    setCodec(new QFDDXmlCodec());
  }

  public void setCodec(QFDDXmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testQFDDXMLConverterAnalogSliderFollowedByNumericReferenceRangeIsNotInherited() {
    try {

      String XML = FileUtil.getData(this.getClass(), "qfdd/analogSliderAndNumericTest.xml");

      QFDDDocument qfddDocument = decode(codec, XML);

      assertNotNull(qfddDocument);
      assertNotNull(qfddDocument.getSections());
      assertEquals(2, qfddDocument.getSections().size());
      assertNotNull(qfddDocument.getSections().get(0));

      assertNotNull(qfddDocument.getSections().get(0).getOrganizers());
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions());
      assertEquals(4, qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().size());
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(0));
      assertNotNull(((QFDDAnalogSliderQuestion) qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(0)).getIncrement());
      assertEquals("1", ((QFDDAnalogSliderQuestion) qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(0)).getIncrement());
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(1));
      assertNotNull(((QFDDAnalogSliderQuestion) qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(1)).getIncrement());
      assertEquals("1", ((QFDDAnalogSliderQuestion) qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(1)).getIncrement());
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(1));
      assertNotNull(((QFDDAnalogSliderQuestion) qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(2)).getIncrement());
      assertEquals("1", ((QFDDAnalogSliderQuestion) qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(2)).getIncrement());
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(1));
      assertNotNull(((QFDDAnalogSliderQuestion) qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(3)).getIncrement());
      assertEquals("1", ((QFDDAnalogSliderQuestion) qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(3)).getIncrement());

      assertNotNull(qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions());
      assertEquals(3, qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().size());
      assertNotNull(qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(0));
      assertNull(
          ((QFDDNumericQuestion) qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(0))
              .getMaximum());
      assertNull(
          ((QFDDNumericQuestion) qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(0))
              .getMinimum());
      assertNotNull(qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(1));
      assertNull(
          ((QFDDNumericQuestion) qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(1))
              .getMaximum());
      assertNull(
          ((QFDDNumericQuestion) qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(1))
              .getMinimum());
      assertNotNull(qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(2));
      assertNull(
          ((QFDDNumericQuestion) qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(2))
              .getMaximum());
      assertNull(
          ((QFDDNumericQuestion) qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(2))
              .getMinimum());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testQFDDXMLConverterTextExample() {
    try {
      String XML = FileUtil.getData(this.getClass(), "qfdd/generatedTextTest.xml");

      QFDDDocument qfddDocument = decode(codec, XML);

      assertNotNull(qfddDocument);
      assertNotNull(qfddDocument.getSections());
      assertEquals(1, qfddDocument.getSections().size());
      assertNotNull(qfddDocument.getSections().get(0));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers());
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions());
      assertEquals(2, qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().size());
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(0));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(1));

      assertQFDDMediaNull(
          qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(0).getQuestionnaireMedia());
      assertQFDDMediaValue(
          qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(1).getQuestionnaireMedia());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testQFDDXMLConverterNumericExample() {
    try {
      String XML = FileUtil.getData(this.getClass(), "qfdd/generatedNumericTest.xml");

      QFDDDocument qfddDocument = decode(codec, XML);

      assertNotNull(qfddDocument);
      assertNotNull(qfddDocument.getSections());
      assertEquals(1, qfddDocument.getSections().size());
      assertNotNull(qfddDocument.getSections().get(0));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers());
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions());
      assertEquals(3, qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().size());
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(0));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(1));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(2));

      assertQFDDMediaNull(
          qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(0).getQuestionnaireMedia());
      assertQFDDMediaNull(
          qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(1).getQuestionnaireMedia());
      assertQFDDMediaValue(
          qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(2).getQuestionnaireMedia());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testQFDDXMLConverterMultipleChoiceExample() {
    try {
      String XML = FileUtil.getData(this.getClass(), "qfdd/generatedMultipleChoiceTest.xml");

      QFDDDocument qfddDocument = decode(codec, XML);

      assertNotNull(qfddDocument);
      assertNotNull(qfddDocument.getSections());
      assertEquals(1, qfddDocument.getSections().size());
      assertNotNull(qfddDocument.getSections().get(0));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers());
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions());
      assertEquals(3, qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().size());
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(0));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(1));
      assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(2));

      assertQFDDMediaNull(
          qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(0).getQuestionnaireMedia());
      assertQFDDMediaNull(
          qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(1).getQuestionnaireMedia());
      assertQFDDMediaValue(
          qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(2).getQuestionnaireMedia());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testQFDDXMLConverterKitExample() {
    try {
      String XML = FileUtil.getData(this.getClass(), "qfdd/QFDD_Example_KIT_20200903.xml");

      QFDDDocument qfddDocument = decode(codec, XML);

      // den kan ikke parse det hele - pt skal der ikke gøres noget ved det

      assertNotNull(qfddDocument);

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testQFDDXMLConverterMultipleChoiceTestWithAssociatedText() {
    try {
      String XML = FileUtil.getData(this.getClass(), "qfdd/MultipleChoiceTestWithAssociatedText.xml");
      QFDDDocument qfddDocument = decode(codec, XML);

      assertNotNull(qfddDocument);

      assertTrue(qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(0) instanceof QFDDMultipleChoiceQuestion);
      QFDDMultipleChoiceQuestion qFDDMultipleChoiceQuestion = (QFDDMultipleChoiceQuestion) qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(0);
      assertNotNull(qFDDMultipleChoiceQuestion);
      assertNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion());

      assertTrue(qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(1) instanceof QFDDMultipleChoiceQuestion);
      qFDDMultipleChoiceQuestion = (QFDDMultipleChoiceQuestion) qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(1);
      assertNotNull(qFDDMultipleChoiceQuestion);
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion());

      assertEquals(1, qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getPreconditions().size());
      assertFalse(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getPreconditions().get(0).isNegationInd());
      assertNotNull(
          qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getPreconditions().get(0).getPreconditionGroup());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions());

      assertEquals(2, qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions().length);
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[0].getCriterion());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[0].getCriterion().getAnswer());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[0].getCriterion().getAnswer().getCode());
      assertEquals("173.A2",
          qFDDMultipleChoiceQuestion
              .getAssociatedTextQuestion()
              .getPreconditions()
              .get(0)
              .getPreconditionGroup()
              .getPreconditions()[0].getCriterion().getAnswer().getCode());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[0].getCriterion().getCodeValue());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[0].getCriterion().getCodeValue().getCode());
      assertEquals("173.Q",
          qFDDMultipleChoiceQuestion
              .getAssociatedTextQuestion()
              .getPreconditions()
              .get(0)
              .getPreconditionGroup()
              .getPreconditions()[0].getCriterion().getCodeValue().getCode());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[0].getCriterion().getCodeValue().getCodeSystem());
      assertEquals("1.2.208.176.1.5",
          qFDDMultipleChoiceQuestion
              .getAssociatedTextQuestion()
              .getPreconditions()
              .get(0)
              .getPreconditionGroup()
              .getPreconditions()[0].getCriterion().getCodeValue().getCodeSystem());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[0].getCriterion().getValueType());
      assertEquals("CE",
          qFDDMultipleChoiceQuestion
              .getAssociatedTextQuestion()
              .getPreconditions()
              .get(0)
              .getPreconditionGroup()
              .getPreconditions()[0].getCriterion().getValueType());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[1].getCriterion());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[1].getCriterion().getAnswer());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[1].getCriterion().getAnswer().getCode());
      assertEquals("173.A3",
          qFDDMultipleChoiceQuestion
              .getAssociatedTextQuestion()
              .getPreconditions()
              .get(0)
              .getPreconditionGroup()
              .getPreconditions()[1].getCriterion().getAnswer().getCode());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[1].getCriterion().getCodeValue());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[1].getCriterion().getCodeValue().getCode());
      assertEquals("173.Q",
          qFDDMultipleChoiceQuestion
              .getAssociatedTextQuestion()
              .getPreconditions()
              .get(0)
              .getPreconditionGroup()
              .getPreconditions()[1].getCriterion().getCodeValue().getCode());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[1].getCriterion().getCodeValue().getCodeSystem());
      assertEquals("1.2.208.176.1.5",
          qFDDMultipleChoiceQuestion
              .getAssociatedTextQuestion()
              .getPreconditions()
              .get(0)
              .getPreconditionGroup()
              .getPreconditions()[1].getCriterion().getCodeValue().getCodeSystem());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getPreconditionGroup()
          .getPreconditions()[1].getCriterion().getValueType());
      assertEquals("CE",
          qFDDMultipleChoiceQuestion
              .getAssociatedTextQuestion()
              .getPreconditions()
              .get(0)
              .getPreconditionGroup()
              .getPreconditions()[1].getCriterion().getValueType());

      assertTrue(qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(2) instanceof QFDDMultipleChoiceQuestion);
      qFDDMultipleChoiceQuestion = (QFDDMultipleChoiceQuestion) qfddDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(2);
      assertNotNull(qFDDMultipleChoiceQuestion);
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion());

      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getHelpText());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getHelpText().getHelpText());
      assertEquals("Associated Question helptext",
          qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getHelpText().getHelpText());

      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getPreconditions());
      assertEquals(1, qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getPreconditions().size());

      assertFalse(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getPreconditions().get(0).isNegationInd());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getPreconditions().get(0).getCriterion());
      assertNotNull(
          qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getPreconditions().get(0).getCriterion().getAnswer());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getCriterion()
          .getAnswer()
          .getCode());
      assertEquals("2272.AI", qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getCriterion()
          .getAnswer()
          .getCode());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getCriterion()
          .getCodeValue());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getCriterion()
          .getCodeValue()
          .getCode());
      assertEquals("2272.Q", qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getCriterion()
          .getCodeValue()
          .getCode());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getCriterion()
          .getCodeValue()
          .getCodeSystem());
      assertEquals("1.2.208.176.1.5", qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getCriterion()
          .getCodeValue()
          .getCodeSystem());
      assertNotNull(qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getCriterion()
          .getValueType());
      assertEquals("CE", qFDDMultipleChoiceQuestion
          .getAssociatedTextQuestion()
          .getPreconditions()
          .get(0)
          .getCriterion()
          .getValueType());

      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestionnaireMedia());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestionnaireMedia().getId());
      assertEquals("fememotes-400_dxt",
          qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestionnaireMedia().getId());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestionnaireMedia().getMediaType());
      assertEquals("image/png",
          qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestionnaireMedia().getMediaType());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestionnaireMedia().getRepresentation());
      assertEquals("B64",
          qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestionnaireMedia().getRepresentation());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestionnaireMedia().getValue());

      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestion());
      assertEquals("Associated Question Text", qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestion());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testQFDDXMLConverterDiabetesExample() {
    try {
      String XML = FileUtil.getData(this.getClass(), "qfdd/DiabetesSample.xml");

      QFDDDocument qfddDocument = decode(codec, XML);

      assertNotNull(qfddDocument);
      assertNotNull(qfddDocument.getSections());
      assertEquals(15, qfddDocument.getSections().size());
      assertNotNull(qfddDocument.getSections().get(0));

      assertEquals(0, qfddDocument.getSections().get(0).getOrganizers().size());
      assertEquals(1, qfddDocument.getSections().get(1).getOrganizers().size());
      assertEquals(5, qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().size());
      assertEquals(1, qfddDocument.getSections().get(2).getOrganizers().size());
      assertEquals(1, qfddDocument.getSections().get(2).getOrganizers().get(0).getQFDDQuestions().size());
      assertEquals(2, qfddDocument.getSections().get(3).getOrganizers().size());
      assertEquals(5, qfddDocument.getSections().get(3).getOrganizers().get(0).getQFDDQuestions().size());
      assertEquals(2, qfddDocument.getSections().get(3).getOrganizers().get(1).getQFDDQuestions().size());
      assertEquals(1, qfddDocument.getSections().get(4).getOrganizers().size());
      assertEquals(3, qfddDocument.getSections().get(4).getOrganizers().get(0).getQFDDQuestions().size());
      assertEquals(4, qfddDocument.getSections().get(5).getOrganizers().size());
      assertEquals(1, qfddDocument.getSections().get(5).getOrganizers().get(0).getQFDDQuestions().size());
      assertEquals(1, qfddDocument.getSections().get(5).getOrganizers().get(1).getQFDDQuestions().size());
      assertEquals(3, qfddDocument.getSections().get(5).getOrganizers().get(2).getQFDDQuestions().size());
      assertEquals(1, qfddDocument.getSections().get(5).getOrganizers().get(3).getQFDDQuestions().size());
      assertEquals(1, qfddDocument.getSections().get(6).getOrganizers().size());
      assertEquals(2, qfddDocument.getSections().get(6).getOrganizers().get(0).getQFDDQuestions().size());
      assertEquals(5, qfddDocument.getSections().get(7).getOrganizers().size());
      assertEquals(1, qfddDocument.getSections().get(7).getOrganizers().get(0).getQFDDQuestions().size());
      assertEquals(9, qfddDocument.getSections().get(7).getOrganizers().get(1).getQFDDQuestions().size());
      assertEquals(2, qfddDocument.getSections().get(7).getOrganizers().get(2).getQFDDQuestions().size());
      assertEquals(1, qfddDocument.getSections().get(7).getOrganizers().get(3).getQFDDQuestions().size());
      assertEquals(1, qfddDocument.getSections().get(7).getOrganizers().get(4).getQFDDQuestions().size());

      assertEquals(1, qfddDocument.getSections().get(8).getOrganizers().size());
      assertEquals(1, qfddDocument.getSections().get(8).getOrganizers().get(0).getQFDDQuestions().size());
      assertTrue(qfddDocument
          .getSections()
          .get(8)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(0) instanceof QFDDMultipleChoiceQuestion);
      QFDDMultipleChoiceQuestion qFDDMultipleChoiceQuestion = (QFDDMultipleChoiceQuestion) qfddDocument
          .getSections()
          .get(8)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(0);
      assertNotNull(qFDDMultipleChoiceQuestion);
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion());
      assertNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getHelpText());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getPreconditions());
      assertNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestionnaireMedia());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestion());

      assertEquals(8, qfddDocument.getSections().get(9).getOrganizers().size());
      assertEquals(5, qfddDocument.getSections().get(9).getOrganizers().get(0).getQFDDQuestions().size());
      assertEquals(3, qfddDocument.getSections().get(9).getOrganizers().get(1).getQFDDQuestions().size());
      assertEquals(4, qfddDocument.getSections().get(9).getOrganizers().get(2).getQFDDQuestions().size());
      assertEquals(5, qfddDocument.getSections().get(9).getOrganizers().get(3).getQFDDQuestions().size());
      assertEquals(3, qfddDocument.getSections().get(9).getOrganizers().get(4).getQFDDQuestions().size());
      assertEquals(3, qfddDocument.getSections().get(9).getOrganizers().get(5).getQFDDQuestions().size());
      assertEquals(5, qfddDocument.getSections().get(9).getOrganizers().get(6).getQFDDQuestions().size());
      assertEquals(3, qfddDocument.getSections().get(9).getOrganizers().get(7).getQFDDQuestions().size());
      assertEquals(1, qfddDocument.getSections().get(10).getOrganizers().size());
      assertEquals(1, qfddDocument.getSections().get(10).getOrganizers().get(0).getQFDDQuestions().size());
      assertEquals(1, qfddDocument.getSections().get(11).getOrganizers().size());
      assertEquals(4, qfddDocument.getSections().get(11).getOrganizers().get(0).getQFDDQuestions().size());
      assertEquals(3, qfddDocument.getSections().get(12).getOrganizers().size());
      assertEquals(1, qfddDocument.getSections().get(12).getOrganizers().get(0).getQFDDQuestions().size());
      assertEquals(3, qfddDocument.getSections().get(12).getOrganizers().get(1).getQFDDQuestions().size());
      assertEquals(2, qfddDocument.getSections().get(12).getOrganizers().get(2).getQFDDQuestions().size());

      assertEquals(1, qfddDocument.getSections().get(13).getOrganizers().size());
      assertEquals(2, qfddDocument.getSections().get(13).getOrganizers().get(0).getQFDDQuestions().size());
      assertTrue(qfddDocument.getSections().get(13).getOrganizers().get(0).getQFDDQuestions().get(
          0) instanceof QFDDMultipleChoiceQuestion);
      qFDDMultipleChoiceQuestion = (QFDDMultipleChoiceQuestion) qfddDocument
          .getSections()
          .get(13)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(0);
      assertNotNull(qFDDMultipleChoiceQuestion);
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion());
      assertNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getHelpText());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getPreconditions());
      assertNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestionnaireMedia());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestion());

      assertTrue(qfddDocument.getSections().get(13).getOrganizers().get(0).getQFDDQuestions().get(
          1) instanceof QFDDMultipleChoiceQuestion);
      qFDDMultipleChoiceQuestion = (QFDDMultipleChoiceQuestion) qfddDocument
          .getSections()
          .get(13)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(1);
      assertNotNull(qFDDMultipleChoiceQuestion);
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion());
      assertNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getHelpText());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getPreconditions());
      assertNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestionnaireMedia());
      assertNotNull(qFDDMultipleChoiceQuestion.getAssociatedTextQuestion().getQuestion());

      assertEquals(0, qfddDocument.getSections().get(14).getOrganizers().size());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testQFDDXMLConverterSlidgigtExample() {
    try {

      String XML = FileUtil.getData(this.getClass(), "qfdd/QFDD_Example_slidtgigt_20000903.xml");
      QFDDDocument qfddDocument = decode(codec, XML);

      assertNotNull(qfddDocument);
      assertNotNull(qfddDocument.getSections());
      assertEquals(8, qfddDocument.getSections().size());
      assertNotNull(qfddDocument.getSections().get(0));
      assertNotNull(qfddDocument.getSections().get(3));
      assertNotNull(qfddDocument.getSections().get(3).getOrganizers().get(0).getQFDDQuestions());
      assertEquals(7, qfddDocument.getSections().get(3).getOrganizers().get(0).getQFDDQuestions().size());
      assertNotNull(qfddDocument.getSections().get(3).getOrganizers().get(0).getQFDDQuestions().get(6));
      assertNotNull(
          qfddDocument.getSections().get(3).getOrganizers().get(0).getQFDDQuestions().get(6).getQuestionnaireMedia());
      assertNotNull(qfddDocument
          .getSections()
          .get(3)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(6)
          .getQuestionnaireMedia()
          .getMediaType());
      assertNotNull(qfddDocument
          .getSections()
          .get(3)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(6)
          .getQuestionnaireMedia()
          .getRepresentation());
      assertNotNull(qfddDocument
          .getSections()
          .get(3)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(6)
          .getQuestionnaireMedia()
          .getValue());
      assertNotNull(qfddDocument
          .getSections()
          .get(3)
          .getOrganizers()
          .get(0)
          .getQFDDQuestions()
          .get(6)
          .getQuestionnaireMedia()
          .getId());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testQFDDXMLConverterOrganizerPreconditionSDTC() {
    //doing a xml>model>xml will generate the precondition as simple once without SDTC prefix. But will the parser understand it?
    try {

      String XML = FileUtil.getData(this.getClass(), "qfdd/Precondition_organizer_sdtc.xml");
      QFDDDocument qfddDocument = decode(codec, XML);

      assertNotNull(qfddDocument);
      assertNotNull(qfddDocument.getSections().get(1).getOrganizers().get(0).getPreconditions());
      assertEquals(1, qfddDocument.getSections().get(1).getOrganizers().get(1).getPreconditions().size());
      assertNotNull(qfddDocument.getSections().get(1).getOrganizers().get(1).getPreconditions().get(0).getCriterion());
      assertEquals("Q2230",
          qfddDocument
              .getSections()
              .get(1)
              .getOrganizers()
              .get(1)
              .getPreconditions()
              .get(0)
              .getCriterion()
              .getCodeValue()
              .getCode());
      assertEquals("1.2.208.176.1.5",
          qfddDocument
              .getSections()
              .get(1)
              .getOrganizers()
              .get(1)
              .getPreconditions()
              .get(0)
              .getCriterion()
              .getCodeValue()
              .getCodeSystem());
      assertEquals("Sundhedsdatastyrelsen", qfddDocument
          .getSections()
          .get(1)
          .getOrganizers()
          .get(1)
          .getPreconditions()
          .get(0)
          .getCriterion()
          .getCodeValue()
          .getCodeSystemName());
      assertEquals("CE", qfddDocument
          .getSections()
          .get(1)
          .getOrganizers()
          .get(1)
          .getPreconditions()
          .get(0)
          .getCriterion()
          .getValueType());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  private void assertQFDDMediaNull(QuestionnaireMedia questionnaireMedia) {
    assertNull(questionnaireMedia);
  }

  private void assertQFDDMediaValue(QuestionnaireMedia questionnaireMedia) {
    assertNotNull(questionnaireMedia);
    assertNotNull(questionnaireMedia.getMediaType());
    assertEquals("image/png", questionnaireMedia.getMediaType());
    assertNotNull(questionnaireMedia.getRepresentation());
    assertEquals("B64", questionnaireMedia.getRepresentation());
    assertNotNull(questionnaireMedia.getValue());
    assertNotNull(questionnaireMedia.getId());
    assertEquals("fememotes-400_dxt", questionnaireMedia.getId());
  }

  @Test
  public void testQFDDComplete() throws Exception {
    encodeDecodeAndCompare(codec, SetupQFDDDocumentForTest.defineAsCDA());
  }

  @Override
  public void runTest() throws Exception {
    testQFDDComplete();
  }
}
