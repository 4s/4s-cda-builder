package dk.s4.hl7.cda.convert.encode.qfdd;

import java.io.IOException;
import java.net.URISyntaxException;

import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qfdd.QFDDCriterion.QFDDCriterionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDFeedback.QFDDFeedbackBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDHelpText.QFDDHelpTextBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDMultipleChoiceQuestion.QFDDMultipleChoiceQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer.QFDDOrganizerBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition.QFDDPreconditionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup.GroupType;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup.QFDDPreconditionGroupBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion.QFDDTextQuestionBuilder;

public final class SetupQFDDMultipleChoiceExample extends QFDDExampleBase {
  private static final String QUESTION = "question";
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";

  protected QFDDQuestion simpleQuestion() throws IOException, URISyntaxException {
    return new QFDDMultipleChoiceQuestionBuilder()
        .setInterval(1, 2)
        .setCodeValue(new CodedValue("value1", "value2", "value3", "value4"))
        .setId(MedCom.createId("idExtension1"))
        .addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited", "Some-CodeSystem-Name")
        .setQuestion(QUESTION)
        .build();
  }

  protected QFDDQuestion simpleQuestionWithReferenceAndPrecondition(boolean includeAssociatedText,
      QFDDPrecondition qfddPrecondition) throws IOException, URISyntaxException {
    CodedValue questionCodedValue = new CodedValue("value1", "value2", "value3", "value4");
    return new QFDDMultipleChoiceQuestionBuilder()
        .setInterval(0, 2)
        .setCodeValue(questionCodedValue)
        .setId(MedCom.createId("idExtension1"))
        .addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited", "Some-CodeSystem-Name")
        .setQuestion(QUESTION)
        .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
            .setMaximum("20")
            .setMinimum("0")
            .setValueType(IntervalType.IVL_INT)
            .build()).build())
        .setAssociatedTextQuestion(createAssociatedTextQuestion(includeAssociatedText, qfddPrecondition))
        .build();
  }

  protected QFDDQuestion fullQuestion(boolean includeAssociatedText, QFDDPrecondition qfddPrecondition)
      throws IOException, URISyntaxException {
    CodedValue questionCodedValue = new CodedValue("value1", "value2", "value3", "value4");
    return new QFDDMultipleChoiceQuestionBuilder()
        .setInterval(1, 2)
        .setCodeValue(questionCodedValue)
        .setId(MedCom.createId("idExtension1"))
        .addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited", "Some-CodeSystem-Name")
        .setQuestion(QUESTION)
        .setFeedback(new QFDDFeedbackBuilder()
            .feedBackText("feedbacktext1")
            .language("da-DK")
            .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
                .setMaximum("20")
                .setMinimum("1")
                .setValueType(IntervalType.IVL_INT)
                .build()).build())
            .build())
        .setHelpText(new QFDDHelpTextBuilder().helpText("helptext").language("da-DK").build())
        .setQuestionnaireMedia(getMediaShort())
        .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
            .setMaximum("20")
            .setMinimum("1")
            .setValueType(IntervalType.IVL_INT)
            .build()).build())
        .setAssociatedTextQuestion(createAssociatedTextQuestion(includeAssociatedText, qfddPrecondition))
        .build();
  }

  private QFDDTextQuestion createAssociatedTextQuestion(boolean includeAssociatedText,
      QFDDPrecondition qfddPrecondition) throws IOException, URISyntaxException {

    if (!includeAssociatedText) {
      return null;
    }

    QFDDTextQuestionBuilder builder = new QFDDTextQuestionBuilder();
    builder.setCodeValue(new CodedValue("172.Q", "1.2.208.176.1.5", "172.Q", "Sundhedsdatastyrelsen"));
    builder.setId(new IDBuilder()
        .setRoot("1.2.208.176.1.5")
        .setExtension("172")
        .setAuthorityName("Sundhedsdatastyrelsen")
        .build());
    builder.setQuestion("Associated Question Text");
    builder.setHelpText(new QFDDHelpTextBuilder().helpText("Associated Question helptext").language("da-DK").build());
    builder.setQuestionnaireMedia(getMediaShort());
    if (qfddPrecondition != null) {
      builder.addPrecondition(qfddPrecondition);
    }
    return builder.build();

  }

  protected QFDDPrecondition groupedPrecondition() {
    CodedValue questionCodedValue1 = new CodedValue("173.Q", "1.2.208.176.1.5", "173.Q", "Sundhedsdatastyrelsen");
    CodedValue questionCodedValueAnswer1 = new CodedValue("173.A2", "173.A2");
    CodedValue questionCodedValue2 = new CodedValue("173.Q", "1.2.208.176.1.5", "173.Q", "Sundhedsdatastyrelsen");
    CodedValue questionCodedValueAnswer2 = new CodedValue("173.A3", "173.A3");
    return new QFDDPreconditionBuilder(new QFDDPreconditionGroupBuilder()
        .setGroupType(GroupType.AtLeastOneTrue)
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.7.3.2")
            .setExtension("2062.C2")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .addPrecondition(new QFDDPreconditionBuilder(
            new QFDDCriterionBuilder(questionCodedValue1).setAnswer(questionCodedValueAnswer1).build()).build())
        .addPrecondition(new QFDDPreconditionBuilder(
            new QFDDCriterionBuilder(questionCodedValue2).setAnswer(questionCodedValueAnswer2).build()).build())
        .build()).build();
  }

  protected QFDDPrecondition simplePrecondition() {
    CodedValue questionCodedValue = new CodedValue("2272.Q", "1.2.208.176.1.5", "2272.Q", "Sundhedsdatastyrelsen");
    CodedValue questionCodedValueAnswer = new CodedValue("2272.AI", "2272.AI");
    return new QFDDPreconditionBuilder(
        new QFDDCriterionBuilder(questionCodedValue).setAnswer(questionCodedValueAnswer).build()).build();
  }

  public QFDDDocument createDocument() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);

    String text = "OM DETTE SKEMA: "
        + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
        + "Hvornår havde du dit seneste anfald?";

    Section<QFDDOrganizer> section = new Section<QFDDOrganizer>("Indledning", text);
    section.addOrganizer(new QFDDOrganizerBuilder()
        .addId(MedCom.createId("idExtensionOrganizer"))
        .addQFDDQuestion(simpleQuestion())
        .addQFDDQuestion(simpleQuestionWithReferenceAndPrecondition(false, null))
        .addQFDDQuestion(fullQuestion(false, null))
        .build());
    cda.addSection(section);
    return cda;
  }

  public QFDDDocument createDocumentWithAssociatedText() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);

    String text = "OM DETTE SKEMA: "
        + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
        + "Hvornår havde du dit seneste anfald?";

    Section<QFDDOrganizer> section = new Section<QFDDOrganizer>("Indledning", text);
    section.addOrganizer(new QFDDOrganizerBuilder()
        .addId(MedCom.createId("idExtensionOrganizer"))
        .addQFDDQuestion(simpleQuestion())
        .addQFDDQuestion(simpleQuestionWithReferenceAndPrecondition(true, groupedPrecondition()))
        .addQFDDQuestion(fullQuestion(true, simplePrecondition()))
        .build());
    cda.addSection(section);
    return cda;
  }
}
