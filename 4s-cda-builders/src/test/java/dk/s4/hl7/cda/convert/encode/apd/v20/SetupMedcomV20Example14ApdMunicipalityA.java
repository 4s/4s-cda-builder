package dk.s4.hl7.cda.convert.encode.apd.v20;

import java.util.Calendar;
import java.util.Date;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.OrganizationIdentity.OrganizationBuilder;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.AppointmentEncounterCode;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.Status;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public class SetupMedcomV20Example14ApdMunicipalityA {

  private static final String DOCUMENT_ID = "e9437901-6f1a-416e-a257-081af339f1c9";

  public static AppointmentDocument createBaseAppointmentDocument() {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTimeWithTimeZone(2020, 00, 30, 9, 57, 0);

    Date authorTime = DateUtil.makeDanishDateTimeWithTimeZone(2021, 00, 29, 9, 57, 0);
    Date authorReferrerTime = DateUtil.makeDanishDateTimeWithTimeZone(2020, 10, 29, 12, 20, 0);

    // Create document
    AppointmentDocument appointment = new AppointmentDocument(MedCom.createId(DOCUMENT_ID));
    appointment.setLanguageCode("da-DK");
    appointment.setTitle("Aftale for 2512489996");
    appointment.setEffectiveTime(documentCreationTime);
    // Create Patient

    AddressData nancyAddress = new AddressData.AddressBuilder("5230", "Odense M")
        .addAddressLine("Forskerparken 10")
        .addAddressLine("MedCom")
        .setUse(AddressData.Use.HomeAddress)
        .build();

    Patient nancy = new Patient.PatientBuilder("Berggren")
        .setGender(Patient.Gender.Female)
        .setBirthTime(1948, Calendar.DECEMBER, 25)
        .addGivenName("Nancy")
        .addGivenName("Ann")
        .setSSN("2512489996")
        .setAddress(nancyAddress)
        .addTelecom(AddressData.Use.HomeAddress, "tel", "69894534")
        .build();

    appointment.setPatient(nancy);

    OrganizationIdentity providerOrganization = new OrganizationBuilder()
        .setSOR("393421000016009")
        .setName("Lægehuset Valdemarsgade")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Munkerisvej 10")
            .addAddressLine("Lægehuset")
            .setCity("Odense M")
            .setPostalCode("5230")
            .setCountry("Danmark")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .addTelecom(Use.HomeAddress, "mailto", "laege@valdemar.dk")
        .addTelecom(Use.HomeAddress, "http", "//www.LeagehusetValdemar.dk")
        .build();
    appointment.setProviderOrganization(providerOrganization);

    // Create Custodian organization
    OrganizationIdentity custodianOrganization = new OrganizationBuilder()
        .setSOR("359001000016000")
        .setName("Odense kommunes Data-ansvarlige organisation")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Odense kommunes dataansvarlige enhed")
            .addAddressLine("Ørbækvej 100")
            .setCity("Odense SØ")
            .setPostalCode("5220")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "mailto", "dataansvarlige@ok.dk")
        .build();
    appointment.setCustodian(custodianOrganization);

    PersonIdentity dortheJensen = new PersonBuilder("Jensen").addGivenName("Dorte").build();

    OrganizationIdentity authorOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setName("Odense Kommune, Sundhedsenhedsforvaltningen")
        .build();

    appointment.setAuthor(new ParticipantBuilder()
        .setAddress(Setup.defineOdenseKommuneSundhedsenhedsforvaltningenAddress())
        .setSOR("378631000016009")
        .addTelecom(Use.WorkPlace, "tel", "65129999")
        .addTelecom(Use.WorkPlace, "mailto", "sundhed@ok.dk")
        .setTime(authorTime)
        .setPersonIdentity(dortheJensen)
        .setOrganizationIdentity(authorOrganization)
        .build());

    //    //Author - appointment requester (v. 2.0.1)
    PersonIdentity elinorStrom = new PersonBuilder("Strøm").addGivenName("Elinor").setPrefix("Læge").build();
    AddressData elinorAddress = new AddressData.AddressBuilder("5000", "Odense C")
        .addAddressLine("Lægeklinikken")
        .addAddressLine("Åvej 29")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    appointment.setAuthorReferrer(new ParticipantBuilder()
        .setSOR("191901000016999")
        .setTime(authorReferrerTime)
        .setPersonIdentity(elinorStrom)
        .setAddress(elinorAddress)
        .addTelecom(Use.WorkPlace, "tel", "52659852")
        .build());

    // 1.4 Define the service period       
    Date from = DateUtil.makeDanishDateTimeWithTimeZone(2020, 10, 30, 9, 00, 0);
    Date to = DateUtil.makeDanishDateTimeWithTimeZone(2020, 10, 30, 12, 00, 0);

    appointment.setDocumentationTimeInterval(from, to);

    appointment.setCdaProfileAndVersion("apd-v2.0.1");

    OrganizationIdentity appointmentLocation = new OrganizationIdentity.OrganizationBuilder()
        //        .setSOR("393421000016009")
        .setName("Borgers Hjemmeadresse")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Forskerparken 10")
            .addAddressLine("MedCom")
            .setCity("Odense M")
            .setPostalCode("5230")
            .setUse(Use.HomeAddress)
            .build())
        .addTelecom(Use.HomeAddress, "tel", "69894534")
        .build();

    appointment.setAppointmentTitle("Aftale");
    appointment.setAppointmentText("<paragraph>Aftale:</paragraph>" + "<table width=\"100%\">" + "<tbody>" + "<tr>"
        + "<th>Aftale dato</th>" + "<th>Vedrørende</th>" + "<th>Udførende organisation</th>" + "<th>Mødested</th>"
        + "<th>Kommentar</th>" + "</tr>" + "<tr>"
        + "<td>2020-11-30 09:00 - 2020-11-30 12:00. Tidspunktet er vejledende</td>" + "<td>Hjemmehjælpsbesøg</td>"
        + "<td>Hjemmepleje-enhed Midtbyen, Odense Kommune,  Sundhedsenhedsforvaltningen, Ørbækvej 100, 5220 Odense SØ, Tlf. 66113333-3</td>"
        + "<td>Forskerparken 10, 5230 Odense M, Borgers Hjemmeadresse</td>"
        + "<td>Aftalen er en del af et repeterende mønster</td>" + "</tr>" + "</tbody>" + "</table>");
    appointment.setAppointmentId(new IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension("a83a1762-ecd4-4cca-aa90-7cf04d754f03")
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build());
    appointment.setAppointmentStatus(Status.ACTIVE);

    appointment.setIndicationDisplayName("Hjemmehjælp");

    appointment.setAppointmentLocationHomeAddress(appointmentLocation);

    AddressData performerAddress = new AddressData.AddressBuilder("5220", "Odense SØ")
        .addAddressLine("Odense Kommune,\n" + "                      Sundhedsenhedsforvaltningen")
        .addAddressLine("Ørbækvej 100")
        .setUse(AddressData.Use.WorkPlace)
        .build();

    OrganizationIdentity organizationIdentity = new OrganizationIdentity.OrganizationBuilder()
        .setName("Hjemmepleje-enhed Midtbyen")
        .build();

    Participant appointmentPerformer = new ParticipantBuilder()
        .setSOR("325441000016006")
        .setTime(authorTime)
        .setOrganizationIdentity(organizationIdentity)
        .setAddress(performerAddress)
        .addTelecom(Use.WorkPlace, "tel", "66113333-3")
        .build();

    appointment.setAppointmentPerformer(appointmentPerformer);

    appointment.setRepeatingDocument(true);
    appointment.setRepeatingDocumentGroupValue("718a7c0d-bf7d-4162-ba28-e3dd720798eb");

    appointment.setGuidedInterval(true);
    appointment.setGuidedIntervalText("Tidspunktet er vejledende");

    appointment.setAppointmentEncounterCode(AppointmentEncounterCode.MunicipalityAppointment);

    return appointment;
  }

}
