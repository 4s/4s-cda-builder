package dk.s4.hl7.cda.convert.encode.phmr.v20;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.codes.NPU;
import dk.s4.hl7.cda.codes.UCUM;
import dk.s4.hl7.cda.convert.PHMRV20XmlCodec;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.*;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.v20.MeasurementGroup;
import dk.s4.hl7.cda.model.phmr.v20.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class TestPHMRVariationsExamplesBuildAndParse extends BaseEncodeTest<PHMRDocument> {

  public TestPHMRVariationsExamplesBuildAndParse() {
    super("src/test/resources/phmr");
  }

  @Before
  public void before() {
    setCodec(new PHMRV20XmlCodec());
  }

  @Test
  public void testBuildAndParseAuthorRepresentedOrganization() throws Exception {

    // given
    PHMRV20XmlCodec codec = new PHMRV20XmlCodec();
    PHMRDocument phmrDocument = SetupExample1.defineAsCDA();
    phmrDocument.setAuthor(new Participant.ParticipantBuilder()
        .setSOR("241301000016007")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(AddressData.Use.WorkPlace, "tel", "65112233")
        .setPersonIdentity(new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build())
        .setOrganizationIdentity(new OrganizationIdentity.OrganizationBuilder()
            .setName("Odense Universitetshospital - Svendborg Sygehus")
            .setSOR("241301000016007")
            .addTelecom(AddressData.Use.WorkPlace, "tel", "65112233")
            .setAddress(new AddressData.AddressBuilder()
                .setPostalCode("5700")
                .setCity("Svendborg")
                .setCountry("Danmark")
                .addAddressLine("Skovvejen 12")
                .addAddressLine("Landet")
                .setUse(AddressData.Use.HomeAddress)
                .build())
            .build())
        .setTime(DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 13, 10, 0, 0))
        .build());

    // when
    String xmlDocument = encodeDocument(phmrDocument);
    PHMRDocument phmrDocumentEncoded = codec.decode(xmlDocument);

    // then
    assertNotNull(phmrDocumentEncoded);
    assertEquals("Odense Universitetshospital - Svendborg Sygehus",
        phmrDocumentEncoded.getAuthor().getOrganizationIdentity().getOrgName());
    assertEquals("241301000016007", phmrDocumentEncoded.getAuthor().getOrganizationIdentity().getId().getExtension());
    assertEquals("65112233", phmrDocumentEncoded.getAuthor().getOrganizationIdentity().getTelecomList()[0].getValue());
    assertEquals(AddressData.Use.WorkPlace,
        phmrDocumentEncoded.getAuthor().getOrganizationIdentity().getTelecomList()[0].getAddressUse());
    assertEquals("tel", phmrDocumentEncoded.getAuthor().getOrganizationIdentity().getTelecomList()[0].getProtocol());
    assertEquals("5700", phmrDocumentEncoded.getAuthor().getOrganizationIdentity().getAddress().getPostalCode());
    assertEquals("Svendborg", phmrDocumentEncoded.getAuthor().getOrganizationIdentity().getAddress().getCity());
    assertEquals("Danmark", phmrDocumentEncoded.getAuthor().getOrganizationIdentity().getAddress().getCountry());
    assertEquals("Skovvejen 12", phmrDocumentEncoded.getAuthor().getOrganizationIdentity().getAddress().getStreet()[0]);
    assertEquals("Landet", phmrDocumentEncoded.getAuthor().getOrganizationIdentity().getAddress().getStreet()[1]);
    assertEquals(AddressData.Use.HomeAddress,
        phmrDocumentEncoded.getAuthor().getOrganizationIdentity().getAddress().getAddressUse());

  }

  @Test
  public void testBuildAndParseLegalAuthenticatorRepresentedOrganization() throws Exception {

    // given
    PHMRV20XmlCodec codec = new PHMRV20XmlCodec();
    PHMRDocument phmrDocument = SetupExample1.defineAsCDA();

    phmrDocument.setLegalAuthenticator(new Participant.ParticipantBuilder()
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(AddressData.Use.WorkPlace, "tel", "65112233")
        .setSOR("241301000016007")
        .setTime(DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 13, 10, 0, 0))
        .setPersonIdentity(new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build())
        .setOrganizationIdentity(new OrganizationIdentity.OrganizationBuilder()
            .setName("Odense Universitetshospital - Svendborg Sygehus - kælderen")
            .setSOR("241301000016007")
            .addTelecom(AddressData.Use.WorkPlace, "tel", "65112233")
            .setAddress(new AddressData.AddressBuilder()
                .setPostalCode("5700")
                .setCity("Svendborg")
                .setCountry("Danmark")
                .addAddressLine("Skovvejen 122")
                .addAddressLine("Landet udenfor")
                .setUse(AddressData.Use.HomeAddress)
                .build())
            .build())
        .build());

    // when
    String xmlDocument = encodeDocument(phmrDocument);
    PHMRDocument phmrDocumentEncoded = codec.decode(xmlDocument);

    // then
    assertEquals("Odense Universitetshospital - Svendborg Sygehus - kælderen",
        phmrDocumentEncoded.getLegalAuthenticator().getOrganizationIdentity().getOrgName());
    assertEquals("241301000016007",
        phmrDocumentEncoded.getLegalAuthenticator().getOrganizationIdentity().getId().getExtension());
    assertEquals("65112233",
        phmrDocumentEncoded.getLegalAuthenticator().getOrganizationIdentity().getTelecomList()[0].getValue());
    assertEquals(AddressData.Use.WorkPlace,
        phmrDocumentEncoded.getLegalAuthenticator().getOrganizationIdentity().getTelecomList()[0].getAddressUse());
    assertEquals("tel",
        phmrDocumentEncoded.getLegalAuthenticator().getOrganizationIdentity().getTelecomList()[0].getProtocol());
    assertEquals("5700",
        phmrDocumentEncoded.getLegalAuthenticator().getOrganizationIdentity().getAddress().getPostalCode());
    assertEquals("Svendborg",
        phmrDocumentEncoded.getLegalAuthenticator().getOrganizationIdentity().getAddress().getCity());
    assertEquals("Danmark",
        phmrDocumentEncoded.getLegalAuthenticator().getOrganizationIdentity().getAddress().getCountry());
    assertEquals("Skovvejen 122",
        phmrDocumentEncoded.getLegalAuthenticator().getOrganizationIdentity().getAddress().getStreet()[0]);
    assertEquals("Landet udenfor",
        phmrDocumentEncoded.getLegalAuthenticator().getOrganizationIdentity().getAddress().getStreet()[1]);
    assertEquals(AddressData.Use.HomeAddress,
        phmrDocumentEncoded.getLegalAuthenticator().getOrganizationIdentity().getAddress().getAddressUse());

  }

  @Test
  public void testBuildAndParseMedicalEquipmentDoesNotExist() throws Exception {

    // given
    PHMRV20XmlCodec codec = new PHMRV20XmlCodec();
    PHMRDocument phmrDocument = SetupExample1.defineAsCDAWitoutMedicalEquipment();

    // when
    String xmlDocument = encodeDocument(phmrDocument);
    PHMRDocument phmrDocumentEncoded = codec.decode(xmlDocument);

    // then
    assertNotNull(phmrDocumentEncoded);
    assertFalse(xmlDocument.contains("<code code=\"46264-8\" codeSystem=\"2.16.840.1.113883.6.1\"/>"));
    assertFalse(xmlDocument.contains("2.16.840.1.113883.10.20.1.7"));

  }

  @Test
  public void testParseMedicalEquipmentExistInXml() throws Exception {

    // given
    String xmlOrig = FileUtil.getData(this.getClass(), "phmr/v20/PHMR-example-with-medicalEquipment_phmr_v1" + ".xml");
    PHMRV20XmlCodec codec = new PHMRV20XmlCodec();

    // when
    PHMRDocument phmrDocumentEncoded = codec.decode(xmlOrig);

    // then
    assertNotNull(phmrDocumentEncoded);
    assertTrue(xmlOrig.contains("<code code=\"46264-8\" codeSystem=\"2.16.840.1.113883.6.1\"/>"));
    assertTrue(xmlOrig.contains("2.16.840.1.113883.10.20.1.7"));
    // medical equipment is ignored

  }

  @Test
  public void testParseGetCdaProfileAndVersion() throws Exception {

    // given
    String xmlOrig = FileUtil.getData(this.getClass(), "phmr/v20/PHMR-example-01.01-27022025v2.xml");
    PHMRV20XmlCodec codec = new PHMRV20XmlCodec();

    // when
    PHMRDocument phmrDocumentEncoded = codec.decode(xmlOrig);

    // then
    assertNotNull(phmrDocumentEncoded);
    assertEquals("phmr-v2.0", phmrDocumentEncoded.getCdaProfileAndVersion());
  }

  @Test
  public void testBuildAndParseTranslated() throws Exception {

    // given
    PHMRV20XmlCodec codec = new PHMRV20XmlCodec();
    PHMRDocument phmrDocument = SetupExample2.defineAsCDA();
    Date fromTime = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 20, 7, 53, 0);

    ID id = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("b6a079b0-89ab-11e3-baa8-0800200c9a66")
        .setRoot(MedCom.ROOT_OID)
        .build();

    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.Electronically,
        DataInputContext.PerformerType.Citizen);
    phmrDocument
        .addResultGroups(new MeasurementGroup(new Measurement.MeasurementBuilder(fromTime, Measurement.Status.COMPLETED)
            .setPhysicalQuantity("50.1", UCUM.kg, NPU.DRY_BODY_WEIGHT_CODE, NPU.DRY_BODY_WEIGHT_DISPLAYNAME)
            .setTranlated("codeT", "displaynameT", "codeSystemOIDT", "codeSystemDisplayNameT")
            .setContext(context)
            .setId(id)
            .build(), fromTime, Measurement.Status.COMPLETED));

    // when
    String xmlDocument = encodeDocument(phmrDocument);
    PHMRDocument phmrDocumentEncoded = codec.decode(xmlDocument);

    // then
    assertEquals(1, phmrDocument.getVitalSignGroups().size());
    assertEquals("DNK05472", phmrDocument.getVitalSignGroups().get(0).getMeasurementList().get(0).getCode());
    assertEquals("Blodtryk systolisk; Arm",
        phmrDocument.getVitalSignGroups().get(0).getMeasurementList().get(0).getDisplayName());
    assertEquals("1.2.208.176.2.1",
        phmrDocument.getVitalSignGroups().get(0).getMeasurementList().get(0).getCodeSystem());
    assertEquals("DNK05473", phmrDocument.getVitalSignGroups().get(0).getMeasurementList().get(1).getCode());
    assertEquals("Blodtryk diastolisk; Arm",
        phmrDocument.getVitalSignGroups().get(0).getMeasurementList().get(1).getDisplayName());
    assertEquals("1.2.208.176.2.1",
        phmrDocument.getVitalSignGroups().get(0).getMeasurementList().get(1).getCodeSystem());

    assertEquals(2, phmrDocument.getResultGroups().size());
    assertEquals("NPU03804", phmrDocument.getResultGroups().get(0).getMeasurementList().get(0).getCode());
    assertEquals("Legeme vægt;Pt", phmrDocument.getResultGroups().get(0).getMeasurementList().get(0).getDisplayName());
    assertEquals("1.2.208.176.2.1", phmrDocument.getResultGroups().get(0).getMeasurementList().get(0).getCodeSystem());
    assertEquals("NPU03804", phmrDocument.getResultGroups().get(1).getMeasurementList().get(0).getCode());
    assertEquals("Legeme vægt;Pt", phmrDocument.getResultGroups().get(1).getMeasurementList().get(0).getDisplayName());
    assertEquals("1.2.208.176.2.1", phmrDocument.getResultGroups().get(1).getMeasurementList().get(0).getCodeSystem());

    assertNotNull(phmrDocumentEncoded);
    assertEquals(1, phmrDocumentEncoded.getVitalSignGroups().size());
    assertEquals("DNK05472", phmrDocumentEncoded.getVitalSignGroups().get(0).getMeasurementList().get(0).getCode());
    assertEquals("Blodtryk systolisk; Arm",
        phmrDocumentEncoded.getVitalSignGroups().get(0).getMeasurementList().get(0).getDisplayName());
    assertEquals("1.2.208.176.2.1",
        phmrDocumentEncoded.getVitalSignGroups().get(0).getMeasurementList().get(0).getCodeSystem());
    assertEquals("DNK05473", phmrDocumentEncoded.getVitalSignGroups().get(0).getMeasurementList().get(1).getCode());
    assertEquals("Blodtryk diastolisk; Arm",
        phmrDocumentEncoded.getVitalSignGroups().get(0).getMeasurementList().get(1).getDisplayName());
    assertEquals("1.2.208.176.2.1",
        phmrDocumentEncoded.getVitalSignGroups().get(0).getMeasurementList().get(1).getCodeSystem());

    assertEquals(2, phmrDocumentEncoded.getResultGroups().size());
    assertEquals("NPU03804", phmrDocumentEncoded.getResultGroups().get(0).getMeasurementList().get(0).getCode());
    assertEquals("Legeme vægt;Pt",
        phmrDocumentEncoded.getResultGroups().get(0).getMeasurementList().get(0).getDisplayName());
    assertEquals("1.2.208.176.2.1",
        phmrDocumentEncoded.getResultGroups().get(0).getMeasurementList().get(0).getCodeSystem());
    assertEquals("NPU03804", phmrDocumentEncoded.getResultGroups().get(1).getMeasurementList().get(0).getCode());
    assertEquals("Legeme vægt;Pt",
        phmrDocumentEncoded.getResultGroups().get(1).getMeasurementList().get(0).getDisplayName());
    assertEquals("1.2.208.176.2.1",
        phmrDocumentEncoded.getResultGroups().get(1).getMeasurementList().get(0).getCodeSystem());

    assertFalse(
        xmlDocument.contains("<code code=\"codeT\" codeSystem=\"codeSystemOIDT\" displayName=\"displaynameT\"/>"));
    assertFalse(xmlDocument.contains("<translation code="));

  }

  @Test
  public void testBuildAndParseNoLegalAuthenticator() throws Exception {

    // given
    PHMRV20XmlCodec codec = new PHMRV20XmlCodec();
    PHMRDocument phmrDocument = SetupExample1.defineAsCDAWitoutMedicalEquipment();
    phmrDocument.setLegalAuthenticator(null);

    // when
    String xmlDocument = encodeDocument(phmrDocument);
    PHMRDocument phmrDocumentEncoded = codec.decode(xmlDocument);

    // then
    assertNotNull(phmrDocumentEncoded);
    assertFalse(xmlDocument.contains("legalAuthenticator"));
    assertNull(phmrDocument.getLegalAuthenticator());
    assertNull(phmrDocumentEncoded.getLegalAuthenticator());

  }

  @Test
  public void testBuildAndParseDataEnterer() throws Exception {

    // given
    PHMRV20XmlCodec codec = new PHMRV20XmlCodec();
    PHMRDocument phmrDocument = SetupExample1.defineAsCDAWitoutMedicalEquipment();
    phmrDocument.setDataEnterer(new Participant.ParticipantBuilder()
        .setAddress(phmrDocument.getCustodianIdentity().getAddress())
        .setSOR(phmrDocument.getCustodianIdentity().getIdValue())
        .setTelecomList(phmrDocument.getCustodianIdentity().getTelecomList())
        .setPersonIdentity(new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build())
        .setOrganizationIdentity(phmrDocument.getCustodianIdentity())
        .build());

    // when
    String xmlDocument = encodeDocument(phmrDocument);
    PHMRDocument phmrDocumentEncoded = codec.decode(xmlDocument);

    // then
    assertNotNull(phmrDocumentEncoded);
    assertTrue(xmlDocument.contains("dataEnterer"));
    assertNotNull(phmrDocument.getDataEnterer());
    assertNotNull(phmrDocumentEncoded.getDataEnterer());
    assertEquals("Hjertemedicinsk afdeling B", phmrDocumentEncoded.getDataEnterer().getAddress().getStreet()[0]);
    assertEquals("Valdemarsgade 53", phmrDocumentEncoded.getDataEnterer().getAddress().getStreet()[1]);
    assertEquals("5700", phmrDocumentEncoded.getDataEnterer().getAddress().getPostalCode());
    assertEquals("Svendborg", phmrDocumentEncoded.getDataEnterer().getAddress().getCity());
    assertEquals("Danmark", phmrDocumentEncoded.getDataEnterer().getAddress().getCountry());
    assertEquals(AddressData.Use.WorkPlace, phmrDocumentEncoded.getDataEnterer().getAddress().getAddressUse());

    assertEquals("1.2.208.176.1.1", phmrDocumentEncoded.getDataEnterer().getId().getRoot());
    assertEquals("241301000016007", phmrDocumentEncoded.getDataEnterer().getId().getExtension());
    assertEquals("SOR", phmrDocumentEncoded.getDataEnterer().getId().getAuthorityName());

    assertEquals("tel", phmrDocumentEncoded.getDataEnterer().getTelecomList()[0].getProtocol());
    assertEquals(AddressData.Use.WorkPlace, phmrDocumentEncoded.getDataEnterer().getTelecomList()[0].getAddressUse());
    assertEquals("65112233", phmrDocumentEncoded.getDataEnterer().getTelecomList()[0].getValue());

    assertEquals("Anders", phmrDocumentEncoded.getDataEnterer().getPersonIdentity().getGivenNames()[0]);
    assertEquals("Andersen", phmrDocumentEncoded.getDataEnterer().getPersonIdentity().getFamilyName());

    assertEquals("Hjertemedicinsk afdeling B",
        phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getAddress().getStreet()[0]);
    assertEquals("Valdemarsgade 53",
        phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getAddress().getStreet()[1]);
    assertEquals("5700", phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getAddress().getPostalCode());
    assertEquals("Svendborg", phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getAddress().getCity());
    assertEquals("Danmark", phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getAddress().getCountry());
    assertEquals(AddressData.Use.WorkPlace,
        phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getAddress().getAddressUse());

    assertEquals("1.2.208.176.1.1", phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getId().getRoot());
    assertEquals("241301000016007",
        phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getId().getExtension());
    assertEquals("SOR", phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getId().getAuthorityName());

    assertEquals("Odense Universitetshospital - Svendborg Sygehus",
        phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getOrgName());

    assertEquals("tel",
        phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getTelecomList()[0].getProtocol());
    assertEquals(AddressData.Use.WorkPlace,
        phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getTelecomList()[0].getAddressUse());
    assertEquals("65112233",
        phmrDocumentEncoded.getDataEnterer().getOrganizationIdentity().getTelecomList()[0].getValue());

  }

  @Test
  public void testBuildAndParseTelecomMobileContact() throws Exception {

    // given
    PHMRV20XmlCodec codec = new PHMRV20XmlCodec();
    PHMRDocument phmrDocument = SetupExample3.defineAsCDAWitoutMedicalEquipment();
    assertMobileContact(phmrDocument);

    assertEquals(AddressData.Use.MobileContact,
        phmrDocument.getDataEnterer().getOrganizationIdentity().getTelecomList()[0].getAddressUse());

    // when
    String xmlDocument = encodeDocument(phmrDocument);
    PHMRDocument phmrDocumentEncoded = codec.decode(xmlDocument);

    // then
    assertNotNull(phmrDocumentEncoded);
    int countMobileContact = StringUtils.countMatches(xmlDocument, " use=\"MC\"/>");
    assertEquals(15, countMobileContact);
    assertMobileContact(phmrDocumentEncoded);

  }

  private void assertMobileContact(PHMRDocument phmrDocument) {
    assertEquals(AddressData.Use.MobileContact, phmrDocument.getPatient().getTelecomList()[0].getAddressUse());
    assertEquals(AddressData.Use.MobileContact, phmrDocument.getPatient().getTelecomList()[1].getAddressUse());

    assertEquals(AddressData.Use.MobileContact, phmrDocument.getAuthor().getTelecomList()[0].getAddressUse());
    assertEquals(AddressData.Use.MobileContact, phmrDocument.getAuthor().getTelecomList()[1].getAddressUse());

    assertEquals(AddressData.Use.MobileContact,
        phmrDocument.getAuthor().getOrganizationIdentity().getTelecomList()[0].getAddressUse());
    assertEquals(AddressData.Use.MobileContact,
        phmrDocument.getAuthor().getOrganizationIdentity().getTelecomList()[1].getAddressUse());

    assertEquals(AddressData.Use.MobileContact, phmrDocument.getDataEnterer().getTelecomList()[0].getAddressUse());
    assertEquals(AddressData.Use.MobileContact, phmrDocument.getDataEnterer().getTelecomList()[1].getAddressUse());

    assertEquals(AddressData.Use.MobileContact,
        phmrDocument.getDataEnterer().getOrganizationIdentity().getTelecomList()[0].getAddressUse());
    assertEquals(AddressData.Use.MobileContact,
        phmrDocument.getDataEnterer().getOrganizationIdentity().getTelecomList()[1].getAddressUse());

    assertEquals(AddressData.Use.MobileContact,
        phmrDocument.getLegalAuthenticator().getTelecomList()[0].getAddressUse());
    assertEquals(AddressData.Use.MobileContact,
        phmrDocument.getLegalAuthenticator().getTelecomList()[1].getAddressUse());

    assertEquals(AddressData.Use.MobileContact,
        phmrDocument.getLegalAuthenticator().getOrganizationIdentity().getTelecomList()[0].getAddressUse());
    assertEquals(AddressData.Use.MobileContact,
        phmrDocument.getLegalAuthenticator().getOrganizationIdentity().getTelecomList()[1].getAddressUse());

    assertEquals(AddressData.Use.MobileContact,
        phmrDocument.getCustodianIdentity().getTelecomList()[0].getAddressUse());
    //    assertEquals(AddressData.Use.MobileContact, phmrDocument.getCustodianIdentity().getTelecomList()[1].getAddressUse());
  }

}
