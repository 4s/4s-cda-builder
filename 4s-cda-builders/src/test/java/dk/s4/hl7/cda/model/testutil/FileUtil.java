package dk.s4.hl7.cda.model.testutil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;

import dk.s4.hl7.util.PerfTest;

public class FileUtil {
  public static final String getData(final String resource) throws IOException, URISyntaxException {
    return getData(PerfTest.class, resource);
  }

  public static final String getData(final Class<?> clazz, final String resource)
      throws IOException, URISyntaxException {
    ClassLoader cl = clazz.getClassLoader();
    return getData(cl, resource);
  }

  public static final String getData(final ClassLoader cl, final String resource)
      throws IOException, URISyntaxException {
    URL url = cl.getResource(resource);
    return getData(url);
  }

  public static final String getData(final URL url) throws IOException, URISyntaxException {
    return getData(url.toURI());
  }

  public static final String getData(final URI uri) throws IOException {
    File file = new File(uri);
    return getData(file);
  }

  public static final String getData(final File file) throws IOException {
    return getData(file, Charset.forName("UTF-8"));
  }

  public static final String getData(final File file, final Charset cs) throws IOException {
    byte[] encoded = new byte[(int) file.length()];

    InputStream is = null;
    try {
      is = new FileInputStream(file);
      is.read(encoded, 0, (int) file.length());
    } finally {
      if (is != null) {
        is.close();
      }
    }

    return new String(encoded, cs);
  }

  public static Reader getReader(String data) throws IOException {
    return new StringReader(data);
  }

  public static Reader getReader(File file) throws IOException {
    return getReader(file, Charset.forName("UTF-8"));
  }

  public static Reader getReader(File file, Charset cs) throws IOException {
    FileInputStream fileInputStream = new FileInputStream(file);
    InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, cs);
    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
    return bufferedReader;
  }

  public static Writer getWriter(File file, Charset cs) throws IOException {
    FileOutputStream fileOutputStream = new FileOutputStream(file);
    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, cs);
    BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
    return bufferedWriter;
  }

  public static File getFile(String parentFolderPath) {
    return new File(parentFolderPath);
  }

  public static File getFile(final File parent, final String filename) {
    return new File(parent, filename);
  }

  public static File getFile(final File parent, final String filename, boolean deleteIfExists, boolean forceCreate)
      throws IOException {
    File result = new File(parent, filename);

    if (deleteIfExists && result.exists()) {
      result.delete();
    }

    if (forceCreate) {
      result.createNewFile();
    }

    return result;
  }

  public static void setData(File file, Charset cs, String documentContent) throws IOException {
    Writer writer = null;
    try {
      writer = getWriter(file, cs);
      writer.write(documentContent);
    } finally {
      close(writer);
    }
  }

  public static void close(Closeable closeMe) {
    if (closeMe != null) {
      try {
        closeMe.close();
      } catch (IOException e) {
        //Ignore errors while closing!
      }
    }
  }
}
