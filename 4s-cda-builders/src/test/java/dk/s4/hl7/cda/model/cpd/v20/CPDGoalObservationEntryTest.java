package dk.s4.hl7.cda.model.cpd.v20;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.model.CdaMeasurement;
import dk.s4.hl7.cda.model.CdaMeasurementInterval;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument.Status;
import dk.s4.hl7.cda.model.cpd.v20.CPDGoalObservationEntry.CPDGoalObservationEntryBuilder;
import dk.s4.hl7.cda.model.util.DateUtil;

public class CPDGoalObservationEntryTest {

  CPDGoalObservationEntryBuilder subject;

  @Before
  public void setUpValidBuilder() throws Exception {

    subject = new CPDGoalObservationEntryBuilder()
        .setId(new IDBuilder().setRoot("1.2.208.184.100.1").build())
        .setStatusCode(Status.ACTIVE)
        .setCode(new CodedValue("ZZ0172Y", "1.2.208.176.2.4",
            "Vurdering af behov for genoptræning, specialiseret genoptræning",
            "Sundhedsstyrelsens Klassifikations System"))
        .setText("<reference value=\"#RehabilitationNeedAndPotential\"/>")
        .setComment(null)
        .setEffectiveDate(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 0, 0, 0))
        .setCdaMeasurement(new CdaMeasurement("48", "%"));

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingId() {

    //given
    subject.setId(null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test
  public void shouldNotFailOnMissingStatusCode() {

    //given
    subject.setStatusCode(null);

    //when
    CPDGoalObservationEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNull(result.getStatusCode());

  }

  @Test
  public void shouldBuild_EffectiveDate() {

    //given

    //when
    CPDGoalObservationEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNotNull(result.getEffectiveTime());
    assertFalse(result.effectiveTimeHasTime());
    assertNull(result.getEffectiveTimeInterval());

  }

  @Test
  public void shouldBuild_EffectTime() {

    //given
    subject.setEffectiveTimeInterval(null).setEffectiveTime(
        DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 12, 40, 0));

    //when
    CPDGoalObservationEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNotNull(result.getEffectiveTime());
    assertTrue(result.effectiveTimeHasTime());
    assertNull(result.getEffectiveTimeInterval());

  }

  @Test
  public void shouldBuild_EffectTimeInterval() {

    //given
    subject
        .setEffectiveTimeInterval(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 11, 21, 35), null)
        .setEffectiveDate(null);

    //when
    CPDGoalObservationEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNull(result.getEffectiveTime());
    assertFalse(result.effectiveTimeHasTime());
    assertNotNull(result.getEffectiveTimeInterval());
    assertNotNull(result.getEffectiveTimeInterval().getStartTime());
    assertTrue(result.getEffectiveTimeInterval().startTimeHasTime());
    assertNull(result.getEffectiveTimeInterval().getStopTime());
    assertTrue(result.getEffectiveTimeInterval().stopTimeHasTime());

  }

  @Test
  public void shouldBuild_EffectDateInterval() {

    //given
    subject
        .setEffectiveDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 0, 0, 0), null)
        .setEffectiveDate(null);

    //when
    CPDGoalObservationEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNull(result.getEffectiveTime());
    assertFalse(result.effectiveTimeHasTime());
    assertNotNull(result.getEffectiveTimeInterval());
    assertNotNull(result.getEffectiveTimeInterval().getStartTime());
    assertFalse(result.getEffectiveTimeInterval().startTimeHasTime());
    assertNull(result.getEffectiveTimeInterval().getStopTime());
    assertFalse(result.getEffectiveTimeInterval().stopTimeHasTime());
  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingEffectiveTimes() {

    //given
    Date dateNull = null;
    subject.setEffectiveTime(dateNull).setEffectiveTimeInterval(null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingEffectiveDates() {

    //given
    Date dateNull = null;
    subject.setEffectiveDate(dateNull).setEffectiveTimeInterval(null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingEffectiveTimesInInterval() {

    //given
    subject.setEffectiveTimeInterval(null, null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingEffectiveDatesInInterval() {

    //given
    subject.setEffectiveDateInterval(null, null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test
  public void shouldBuild_Measurement() {

    //given

    //when
    CPDGoalObservationEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNotNull(result.getCdaMeasurement());
    assertNull(result.getCdaMeasurementInterval());

  }

  @Test
  public void shouldBuild_MeasurementInterval() {

    //given
    subject
        .setCdaMeasurementInterval(
            new CdaMeasurementInterval(new CdaMeasurement("48", "%"), false, new CdaMeasurement("50", "%"), false))
        .setCdaMeasurement(null);

    //when
    CPDGoalObservationEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNull(result.getCdaMeasurement());
    assertNotNull(result.getCdaMeasurementInterval());

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingMeasurementUnit() {

    //given
    subject.setCdaMeasurement(new CdaMeasurement("48", null));

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test
  public void shouldNotFailOnMissingMeasurementIntervalUnit() {

    //given
    subject
        .setCdaMeasurementInterval(
            new CdaMeasurementInterval(new CdaMeasurement("48", null), false, new CdaMeasurement("50", "%"), false))
        .setCdaMeasurement(null);

    //when
    CPDGoalObservationEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNull(result.getCdaMeasurement());
    assertNotNull(result.getCdaMeasurementInterval());
    assertNotNull(result.getCdaMeasurementInterval().getMeasurementLow());
    assertNotNull(result.getCdaMeasurementInterval().getMeasurementLow().getValue());
    assertNull(result.getCdaMeasurementInterval().getMeasurementLow().getUnit());

  }

}
