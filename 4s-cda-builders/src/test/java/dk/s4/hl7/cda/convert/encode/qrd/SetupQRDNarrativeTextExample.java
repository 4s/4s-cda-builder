package dk.s4.hl7.cda.convert.encode.qrd;

import java.util.Date;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity.OrganizationBuilder;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Reference.DocumentIdReferencesUse;
import dk.s4.hl7.cda.model.Reference.ReferenceBuilder;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.Telecom;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse.QRDAnalogSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse.QRDMultipleChoiceResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse.QRDNumericResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer.QRDOrganizerBuilder;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse.QRDTextResponseBuilder;
import dk.s4.hl7.cda.model.util.DateUtil;

public final class SetupQRDNarrativeTextExample {
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea613";

  private String narrativeTextInfo = "						<paragraph>Du har modtaget dette spørgeskema, fordi vi gerne vil vide, hvordan du har det. Dine svar vil være et udgangspunkt for vores videre samtale, når vi mødes næste gang.</paragraph>\n"
      + "						<paragraph>Inden du svarer, skal du være opmærksom på at:</paragraph>\n"
      + "						<list listType=\"unordered\">\n"
      + "							<item>Oplysningerne betragtes som en del af din journal, og det er de sundhedsfaglige personer, der tager del i din behandling, der har adgang til den.</item>\n"
      + "							<item>Dine svar vil først blive set af en sundhedsfaglig person lige op til dit næste besøg. Hvis din tilstand forværres inden da, eller hvis din tilstand kræver akut behandling, skal du tage kontakt til sundhedsvæsenet på samme måde, som du ville gøre, hvis du har valgt ikke at svare på spørgeskemaet.</item>\n"
      + "							<item>Dine svar vil kunne blive brugt til kvalitetssikring og –udvikling i sundhedsvæsenet.</item>\n"
      + "						</list>\n"
      + "						<paragraph>Det kan være at der er nogle af spørgsmålene, som du synes er svære at svare på, men prøv at svare så godt du kan ud fra hvordan du har det i øjeblikket. Bemærk, der er ikke nogen rigtige eller forkerte svar.</paragraph>\n"
      + "";

  private String narrativeTextMultipleChoice2081 = "						<table>\n"
      + "							<colgroup><!-- Tabelformat er midlertidigt. Afventer PRO-sekretariatets godkendelse. -->\n"
      + "								<col width=\"70%\"/>\n" + "								<col width=\"30%\"/>\n" + "							</colgroup>\n"
      + "							<tbody>\n" + "								<tr>\n"
      + "									<td colspan=\"2\">For at målrette spørgeskemaet til dig, kommer først et spørgsmål om dit køn.</td>\n"
      + "								</tr>\n" + "								<tr>\n"
      + "									<th colspan=\"2\" align=\"left\">Hvad er dit køn?</th>\n"
      + "								</tr><!-- Den nedenstående anvendelse af maskinlæsbare data skjult i kommentarer er et midlertidigt tiltag foranlediget af Medcom. -->\n"
      + "								<tr>\n" + "									<td>Mand</td>\n" + "									<td><!-- A2200_1 --></td>\n"
      + "								</tr>\n" + "								<tr>\n" + "									<td>Kvinde</td>\n"
      + "									<td><!--A2200_2 --></td>\n" + "								</tr>\n" + "							</tbody>\n"
      + "						</table>";

  private String narrativeTextMultipleChoice2070 = "						<table>\n"
      + "							<colgroup><!-- Tabelformat er midlertidigt. Afventer PRO-sekretariatets godkendelse. -->\n"
      + "								<col width=\"70%\"/>\n" + "								<col width=\"30%\"/>\n" + "							</colgroup>\n"
      + "							<tbody>\n" + "								<tr>\n"
      + "									<th colspan=\"2\" align=\"left\">Hvordan synes du dit helbred er alt i alt?</th>\n"
      + "								</tr><!-- Den nedenstående anvendelse af maskinlæsbare data skjult i kommentarer er et midlertidigt tiltag foranlediget af Medcom. -->\n"
      + "								<tr>\n" + "									<td>Fremragende</td>\n"
      + "									<td><!-- A30_1 --></td>\n" +

      "								</tr>\n" + "								<tr>\n" + "									<td>Vældig godt</td>\n"
      + "									<td><!-- A30_2 --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td>Godt</td>\n" + "									<td><!-- A30_3 --></td>\n" + "								</tr>\n"
      + "								<tr>\n" + "									<td>Mindre godt</td>\n"
      + "									<td><!-- A30_4 --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td>Dårligt</td>\n" + "									<td><!-- A30_5 --></td>\n" + "								</tr>\n"
      + "							</tbody>\n" + "						</table>";

  private String narrativeTextMultipleChoice2074 = "						<table>\n"
      + "							<colgroup><!-- Tabelformat er midlertidigt. Afventer PRO-sekretariatets godkendelse. -->\n"
      + "								<col width=\"70%\"/>\n" + "								<col width=\"30%\"/>\n" + "							</colgroup>\n"
      + "							<tbody>\n" + "								<tr>\n"
      + "									<th colspan=\"2\" align=\"left\">Sker det nogensinde, at du er alene, selvom du mest har lyst til at være sammen med andre?</th>\n"
      + "								</tr><!-- Den nedenstående anvendelse af maskinlæsbare data skjult i kommentarer er et midlertidigt tiltag foranlediget af Medcom. -->\n"
      + "								<tr>\n" + "									<td>Ja, ofte</td>\n"
      + "									<td><!-- A2203_1 --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td>Ja, en gang imellem</td>\n" + "									<td><!-- A2203_2 --></td>\n"
      + "								</tr>\n" + "								<tr>\n" + "									<td>Ja, men sjældent</td>\n"
      + "									<td><!-- A2203_3 --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td>Nej</td>\n" + "									<td><!-- A2203_4 --></td>\n" + "								</tr>\n"
      + "								<tr>\n"
      + "									<th colspan=\"2\" align=\"left\">Har du nogen at tale med, hvis du har problemer eller brug for støtte?</th>\n"
      + "								</tr><!-- Den nedenstående anvendelse af maskinlæsbare data skjult i kommentarer er et midlertidigt tiltag foranlediget af Medcom. -->\n"
      + "								<tr>\n" + "									<td>Ja, altid</td>\n"
      + "									<td><!-- A2204_1 --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td>Ja, for det meste</td>\n" + "									<td><!-- A2204_2 --></td>\n"
      + "								</tr>\n" + "								<tr>\n" + "									<td>Ja, nogle gange</td>\n"
      + "									<td><!-- A2204_3 --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td>Nej, aldrig eller næsten aldrig</td>\n" + "									<td><!-- A2204_4 --></td>\n"
      + "								</tr>\n" + "							</tbody>\n" + "						</table>";

  private String narrativeTextAnalogSlider = "			<paragraph>DIT HELBRED I DAG</paragraph>\n"
      + "			<list listType=\"unordered\">\n"
      + "				<item>Vi vil gerne vide, hvor godt eller dårligt dit helbred er I DAG.</item>\n"
      + "				<item>Denne skala er nummereret fra 0 til 100.</item>\n"
      + "				<item>100 svarer til det bedste helbred, du kan forestille dig.</item>\n"
      + "				<item>0 svarer til det dårligste helbred, du kan forestille dig.</item>\n"
      + "				<item>Klik med musen på det sted på skalaen, der viser, hvordan dit helbred er I DAG.</item>\n"
      + "			</list>\n" + "			<table>\n" + "				<colgroup>\n"
      + "					<!-- Tabelformat er midlertidigt. Afventer PRO-sekretariatets godkendelse. -->\n"
      + "					<col width=\"70%\"/>\n" + "					<col width=\"30%\"/>\n" + "				</colgroup>\n"
      + "				<tbody>\n" + "					<tr>\n"
      + "						<th colspan=\"2\" align=\"left\">Du bedes klikke med musen i DEN kasse, der bedst beskriver dit helbred I DAG.\n"
      + "						</th>\n" + "					</tr>\n"
      + "					<!-- Den nedenstående anvendelse af maskinlæsbare data skjult i kommentarer er et midlertidigt tiltag foranlediget af Medcom. -->\n"
      + "					<tr>\n" + "						<td>Maksimum</td>\n" + "						<td>100</td>\n" + "					</tr>\n"
      + "					<tr>\n" + "						<td>Besvarelse</td>\n" + "						<td>\n"
      + "							<content styleCode=\"Bold\"><!-- Q2595 --></content>\n" + "						</td>\n"
      + "					</tr>\n" + "					<tr>\n" + "						<td>Minimum</td>\n" + "						<td>0</td>\n"
      + "					</tr>\n" + "				</tbody>\n" + "			</table>\n"
      + "			<sub>© EuroQol Research Foundation. EQ-5D™ is a trade mark of the EuroQol Research Foundation</sub>";

  private String narrativeTextNumericInfo = "						<table>\n" + "							<colgroup>\n"
      + "								<!-- Tabelformat er midlertidigt. Afventer PRO-sekretariatets godkendelse. -->\n"
      + "								<col width=\"70%\"/>\n" + "								<col width=\"30%\"/>\n" + "							</colgroup>\n"
      + "							<tbody>\n" + "								<tr>\n" + "									<td>\n"
      + "										<!--<td ID=\"haandflade\">-->\n"
      + "										<paragraph>De næste spørgsmål omhandler udbredelsen af din psoriasis <content styleCode=\"Underline\">i dag</content>. Hvor stor en del af din krop, der er påvirket af psoriasis, kan bestemmes ved hjælp af <content styleCode=\"Italics\">størrelsen på din egen håndflade</content>.</paragraph>\n"
      + "										<paragraph>Du bedes angive, hvor mange håndflader i alt, der er påvirket af psoriasis. Har du fx psoriasis, der fylder to halve håndflader på dit hoved og hals, skal du angive én håndflade. Dine fingre skal være samlede, når du angiver antal håndflader som vist på billedet.</paragraph>\n"
      + "									</td>\n" + "									<td>\n" + "									</td>\n" + "								</tr>\n"
      + "							</tbody>\n" + "						</table>";

  private String narrativeTextNumeric = "						<table>\n" + "							<colgroup>\n"
      + "								<!-- Tabelformat er midlertidigt. Afventer PRO-sekretariatets godkendelse. -->\n"
      + "								<col width=\"70%\"/>\n" + "								<col width=\"30%\"/>\n" + "							</colgroup>\n"
      + "							<tbody>\n"
      + "								<!-- Den nedenstående anvendelse af maskinlæsbare data skjult i kommentarer er et midlertidigt tiltag foranlediget af Medcom. -->\n"
      + "								<tr>\n"
      + "									<td>Hvor mange håndflader fylder psoriasis på din hals og dit hoved (inklusiv hårbund) til sammen?</td>\n"
      + "									<td>\n" + "										<content styleCode=\"Bold\"><!-- Q2552 --></content>\n"
      + "									</td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<th colspan=\"2\" align=\"left\">\n" + "									</th>\n" + "								</tr>\n"
      + "								<tr>\n"
      + "									<td>Hvor mange håndflader fylder psoriasis på din overkrop (minus arme) foran og bagpå (inklusiv armhuler)?</td>\n"
      + "									<td>\n" + "										<content styleCode=\"Bold\"><!-- Q2553 --></content>\n"
      + "									</td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<th colspan=\"2\" align=\"left\">\n" + "									</th>\n" + "								</tr>\n"
      + "							</tbody>\n" + "						</table>\n" + "";

  private String narrativeTextText = "						<table>\n" + "							<colgroup>\n"
      + "								<!-- Tabelformat er midlertidigt. Afventer PRO-sekretariatets godkendelse. -->\n"
      + "								<col width=\"70%\"/>\n" + "								<col width=\"30%\"/>\n" + "							</colgroup>\n"
      + "							<tbody>\n" + "								<tr>\n"
      + "									<th colspan=\"2\" align=\"left\">Hvem har udfyldt dette spørgeskema?</th>\n"
      + "								</tr>\n"
      + "								<!-- Den nedenstående anvendelse af maskinlæsbare data skjult i kommentarer er et midlertidigt tiltag foranlediget af Medcom. -->\n"
      + "								<tr>\n" + "									<td>Jeg har selv udfyldt skemaet</td>\n"
      + "									<td><!--A173_1--></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td>Jeg har fået hjælp til at udfylde skemaet</td>\n"
      + "									<td><!-- A173_2 --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td>En anden har udfyldt skemaet for mig</td>\n"
      + "									<td><!-- A173_3 --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td/>\n" + "									<td>\n"
      + "									Skriv hvem der har udfyldt skemaet (fx ægtefælle eller sygeplejerske):\n"
      + "									</td>\n" + "								</tr>\n" + "								<tr>\n" + "									<td/>\n"
      + "									<td><!-- Q174 --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<th colspan=\"2\" align=\"left\">Hvad er vigtigst for dig at få ud af behandlingen?</th>\n"
      + "								</tr>\n" + "								<tr>\n"
      + "									<tr colspan=\"2\" align=\"left\"><!-- Q243 --></tr>\n" + "								</tr>\n"
      + "							</tbody>\n" + "						</table>";

  private String narrativeTextText_a = "						<table>\n" + "							<colgroup>\n"
      + "								<!-- Tabelformat er midlertidigt. Afventer PRO-sekretariatets godkendelse. -->\n"
      + "								<col width=\"70%\"/>\n" + "								<col width=\"30%\"/>\n" + "							</colgroup>\n"
      + "							<tbody>\n" + "								<tr>\n"
      + "									<th colspan=\"2\" align=\"left\">Hvem har udfyldt dette spørgeskema?</th>\n"
      + "								</tr>\n"
      + "								<!-- Den nedenstående anvendelse af maskinlæsbare data skjult i kommentarer er et midlertidigt tiltag foranlediget af Medcom. -->\n"
      + "								<tr>\n" + "									<td>Jeg har selv udfyldt skemaet</td>\n"
      + "									<td><!--A173_1a--></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td>Jeg har fået hjælp til at udfylde skemaet</td>\n"
      + "									<td><!-- A173_2a --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td>En anden har udfyldt skemaet for mig</td>\n"
      + "									<td><!-- A173_3a --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<td/>\n" + "									<td>\n"
      + "									Skriv hvem der har udfyldt skemaet (fx ægtefælle eller sygeplejerske):\n"
      + "									</td>\n" + "								</tr>\n" + "								<tr>\n" + "									<td/>\n"
      + "									<td><!-- Q174a --></td>\n" + "								</tr>\n" + "								<tr>\n"
      + "									<th colspan=\"2\" align=\"left\">Hvad er vigtigst for dig at få ud af behandlingen?</th>\n"
      + "								</tr>\n" + "								<tr>\n"
      + "									<tr colspan=\"2\" align=\"left\"><!-- Q243a --></tr>\n" + "								</tr>\n"
      + "							</tbody>\n" + "						</table>";

  private String narrativeTextDiscreteSlider = "						<table>\n" + "							<colgroup>\n"
      + "								<!-- Tabelformat er midlertidigt. Afventer PRO-sekretariatets godkendelse. -->\n"
      + "								<col width=\"70%\"/>\n" + "								<col width=\"30%\"/>\n" + "							</colgroup>\n"
      + "							<tbody>\n" + "								<tr>\n"
      + "									<th colspan=\"2\" align=\"left\">Hvordan synes du dit helbred er alt i alt?</th>\n"
      + "								</tr>\n"
      + "								<!-- Den nedenstående anvendelse af maskinlæsbare data skjult i kommentarer er et midlertidigt tiltag foranlediget af Medcom. -->\n"
      + "								<tr>\n" + "									<td>Fremragende</td>\n"
      + "									<td styleCode=\"Bold\"><!-- A30_1 --></td>\n" + "								</tr>\n"
      + "								<tr>\n" + "									<td>Vældig godt</td>\n"
      + "									<td styleCode=\"Bold\"><!-- A30_2 --></td>\n" + "								</tr>\n"
      + "								<tr>\n" + "									<td>Godt</td>\n"
      + "									<td styleCode=\"Bold\"><!-- A30_3 --></td>\n" + "								</tr>\n"
      + "								<tr>\n" + "									<td>Mindre godt</td>\n"
      + "									<td styleCode=\"Bold\"><!-- A30_4 --></td>\n" + "								</tr>\n"
      + "								<tr>\n" + "									<td>Dårligt</td>\n"
      + "									<td styleCode=\"Bold\"><!-- A30_5 --></td>\n" + "								</tr>\n"
      + "							</tbody>\n" + "						</table>\n" + "";

  private String narrativeTextDiscreteSlider2 = "			<table>\n" + "				<colgroup>\n"
      + "					<col width=\"90\"/>\n" + "					<col width=\"10%\"/>\n" + "				</colgroup>\n"
      + "				<tbody>\n" + "					<tr>\n" + "						<th align=\"left\" colspan=\"2\">Har du et svar?</th>\n"
      + "					</tr>\n" + "					<tr>\n" + "						<td>Ja</td>\n"
      + "						<td align=\"right\" styleCode=\"Bold\"><!-- A.DISCRETE.01.01 --></td>\n" + "					</tr>\n"
      + "					<tr>\n" + "						<td>Nej</td>\n"
      + "						<td align=\"right\" styleCode=\"Bold\"><!-- A.DISCRETE.01.02 --></td>\n" + "					</tr>\n"
      + "					<tr>\n" + "						<td>Andet</td>\n"
      + "						<td align=\"right\" styleCode=\"Bold\"><!-- A.DISCRETE.01.03 --></td>\n" + "					</tr>\n"
      + "				</tbody>\n" + "			</table>\n" + "			<br/>\n" + "			<table>\n" + "				<colgroup>\n"
      + "					<col width=\"90%\"/>\n" + "					<col width=\"10%\"/>\n" + "				</colgroup>\n"
      + "				<tbody>\n" + "					<tr>\n"
      + "						<th align=\"left\" colspan=\"2\">Hvad vil du svare?</th>\n" + "					</tr>\n"
      + "					<tr>\n"
      + "						<td align=\"right\" colspan=\"2\" styleCode=\"Bold\"><!-- Q.DISCRETE.02.01 --></td>\n"
      + "					</tr>\n" + "				</tbody>\n" + "			</table>";

  public QRDDocument createDocument() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);

    QRDMultipleChoiceResponse qrdMultipleChoiceResponse1;
    QRDMultipleChoiceResponse qrdMultipleChoiceResponse2;
    QRDAnalogSliderResponse qrdAnalogSliderResponse1;
    QRDNumericResponse qrdNumericResponse1;
    QRDNumericResponse qrdNumericResponse2;
    QRDTextResponse qrdTextResponse1;
    QRDDiscreteSliderResponse qrdDiscreteSliderResponse1;

    Reference ref = new ReferenceBuilder(DocumentIdReferencesUse.HTTP_REFERENCE.getReferencesUse(),
        new IDBuilder()
            .setRoot("1.2.208.176.1")
            .setExtension(
                "http://svn.medcom.dk/svn/drafts/QFDD/Hjerterehabilitering/Hjerterehab-uddrag-start-pakke_v1_20201221/Hjerterehab-uddrag-start_v1__QFD.xml")
            .build(),
        new CodedValue(Loinc.QFD_CODE, Loinc.OID, Loinc.QFD_DISPLAYNAME, Loinc.DISPLAYNAME)).build();

    // * info section *
    Section<QRDOrganizer> sectionInfo = new Section<QRDOrganizer>("Hvordan har du det?", narrativeTextInfo);
    cda.addSection(sectionInfo);

    // * multiple choice with 1 question, 1 answer *
    Section<QRDOrganizer> sectionMultipleChoice2081 = new Section<QRDOrganizer>("Indledende oplysninger om dig",
        narrativeTextMultipleChoice2081);

    qrdMultipleChoiceResponse1 = new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("Q2200", "1.2.208.176.1.5", "Hvad er dit køn?", "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5")
            .setExtension("2200")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Hvad er dit køn?")
        .setInterval(0, 1)
        .addAnswer("A2200_2", "1.2.208.176.1.5", "Kvinde", "Sundhedsdatastyrelsen")
        .addReference(ref)
        .build();

    sectionMultipleChoice2081.addOrganizer(new QRDOrganizerBuilder()
        .addId(new IDBuilder()
            .setRoot("1.2.208.176.7.3.3")
            .setExtension("2174")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .addQRDResponse(qrdMultipleChoiceResponse1)
        .build());
    cda.addSection(sectionMultipleChoice2081);

    // * multiple choice with 1 question, 2 answers, mandatory answer*
    Section<QRDOrganizer> sectionMultipleChoice2070 = new Section<QRDOrganizer>("Generelt om dit helbred",
        narrativeTextMultipleChoice2070);
    qrdMultipleChoiceResponse1 = new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("Q30", "1.2.208.176.1.5", "Hvordan synes du dit helbred er alt i alt?",
            "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5")
            .setExtension("30")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Hvordan synes du dit helbred er alt i alt?")
        .setInterval(1, 3)
        .addAnswer("A30_2", "1.2.208.176.1.5", "Vældig godt", "Sundhedsdatastyrelsen")
        .addAnswer("A30_4", "1.2.208.176.1.5", "Mindre godt", "Sundhedsdatastyrelsen")
        .addReference(ref)
        .build();
    sectionMultipleChoice2070.addOrganizer(new QRDOrganizerBuilder()
        .addId(new IDBuilder()
            .setRoot("1.2.208.176.7.3.3")
            .setExtension("2140")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .addQRDResponse(qrdMultipleChoiceResponse1)
        .build());
    cda.addSection(sectionMultipleChoice2070);

    // * multiple choice with 2 questions, 1 answer each *
    Section<QRDOrganizer> sectionMultipleChoice2074 = new Section<QRDOrganizer>("Samvær og netværk",
        narrativeTextMultipleChoice2074);
    qrdMultipleChoiceResponse1 = new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("Q2203", "1.2.208.176.1.5",
            "Sker det nogensinde, at du er alene, selvom du mest har lyst til at være sammen med andre?",
            "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5")
            .setExtension("2203")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Sker det nogensinde, at du er alene, selvom du mest har lyst til at være sammen med andre?")
        .setInterval(0, 1)
        .addAnswer("A2203_2", "1.2.208.176.1.5", "Ja, en gang imellem", "Sundhedsdatastyrelsen")
        .addReference(ref)
        .build();

    qrdMultipleChoiceResponse2 = new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("Q2204", "1.2.208.176.1.5",
            "Har du nogen at tale med, hvis du har problemer eller brug for støtte?", "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5")
            .setExtension("2204")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Har du nogen at tale med, hvis du har problemer eller brug for støtte?")
        .setInterval(0, 1)
        .addAnswer("A2204_3", "1.2.208.176.1.5", "Ja, nogle gange", "Sundhedsdatastyrelsen")
        .addReference(ref)
        .build();

    sectionMultipleChoice2074.addOrganizer(new QRDOrganizerBuilder()
        .addId(new IDBuilder()
            .setRoot("1.2.208.176.7.3.3")
            .setExtension("2149")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .addQRDResponse(qrdMultipleChoiceResponse1)
        .addQRDResponse(qrdMultipleChoiceResponse2)
        .build());
    cda.addSection(sectionMultipleChoice2074);

    // * analog slider with 1 question, 1 answer *
    Section<QRDOrganizer> sectionAnalogSlider = new Section<QRDOrganizer>("Livskvalitet", narrativeTextAnalogSlider);

    qrdAnalogSliderResponse1 = new QRDAnalogSliderResponseBuilder()
        .setCodeValue(new CodedValue("Q2595", "1.2.208.176.1.5", "DIT HELBRED I DAG", "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5")
            .setExtension("2595")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("DIT HELBRED I DAG")
        .setValue("4", BasicType.INT.name())
        .setInterval("0", "100", "1")
        .addReference(ref)
        .build();

    sectionAnalogSlider.addOrganizer(new QRDOrganizerBuilder()
        .addId(new IDBuilder()
            .setRoot("1.2.208.176.7.3.3")
            .setExtension("2230")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .addQRDResponse(qrdAnalogSliderResponse1)
        .build());
    cda.addSection(sectionAnalogSlider);

    // * numeric with info section *
    Section<QRDOrganizer> sectionInfoNumeric = new Section<QRDOrganizer>("Hudmanifestationer",
        narrativeTextNumericInfo);
    cda.addSection(sectionInfoNumeric);

    // * numeric with 2 question, each 1 answer *
    Section<QRDOrganizer> sectionNumeric = new Section<QRDOrganizer>("Hudmanifestationer", narrativeTextNumeric);

    qrdNumericResponse1 = new QRDNumericResponseBuilder()
        .setCodeValue(new CodedValue("Q2552", "1.2.208.176.1.5",
            "Hvor mange håndflader fylder psoriasis på din hals og dit hoved (inklusiv hårbund) til sammen?",
            "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5")
            .setExtension("2552")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Hvor mange håndflader fylder psoriasis på din hals og dit hoved (inklusiv hårbund) til sammen?")
        .setValue("1", BasicType.INT)
        .setInterval("0", "9", IntervalType.IVL_INT)
        .addReference(ref)
        .build();
    qrdNumericResponse2 = new QRDNumericResponseBuilder()
        .setCodeValue(new CodedValue("Q2553", "1.2.208.176.1.5",
            "Hvor mange håndflader fylder psoriasis på din overkrop (minus arme) foran og bagpå (inklusiv armhuler)?",
            "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5")
            .setExtension("2553")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion(
            "Hvor mange håndflader fylder psoriasis på din overkrop (minus arme) foran og bagpå (inklusiv armhuler)?")
        .setValue("2", BasicType.INT)
        .setInterval("0", "36", IntervalType.IVL_INT)
        .addReference(ref)
        .build();

    sectionNumeric.addOrganizer(new QRDOrganizerBuilder()
        .addId(new IDBuilder()
            .setRoot("1.2.208.176.7.3.3")
            .setExtension("2222")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .addQRDResponse(qrdNumericResponse1)
        .addQRDResponse(qrdNumericResponse2)
        .build());
    cda.addSection(sectionNumeric);

    // * text with 1 multiple choice question without associated text question 
    // and 1 text question *
    Section<QRDOrganizer> sectionText_1 = new Section<QRDOrganizer>("Text Question Patterm Observation",
        narrativeTextText);

    qrdMultipleChoiceResponse1 = new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(
            new CodedValue("Q173", "1.2.208.176.1.5.1", "Hvem har udfyldt dette spørgeskema?", "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5.1")
            .setExtension("173")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Hvem har udfyldt dette spørgeskema?")
        .setInterval(1, 1)
        .addAnswer("A173_1", "1.2.208.176.1.5.1", "Jeg har selv udfyldt skemaet", "Sundhedsdatastyrelsen")
        .addReference(ref)
        .build();

    qrdTextResponse1 = new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("Q243", "1.2.208.176.1.5.1", "Hvad er vigtigst for dig at få ud af behandlingen?",
            "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5.1")
            .setExtension("243")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Hvad er vigtigst for dig at få ud af behandlingen?")
        .setText("At blive rask")
        .addReference(ref)
        .build();

    sectionText_1.addOrganizer(new QRDOrganizerBuilder()
        .addId(new IDBuilder()
            .setRoot("1.2.208.176.7.3.3")
            .setExtension("12345678")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .addQRDResponse(qrdMultipleChoiceResponse1)
        .addQRDResponse(qrdTextResponse1)
        .build());
    cda.addSection(sectionText_1);

    // * text with 1 multiple choice question and associated text question 
    // and 1 text question *
    Section<QRDOrganizer> sectionText_a = new Section<QRDOrganizer>("Text Question Patterm Observation",
        narrativeTextText_a);

    QRDTextResponse associatedTextQuestion173_a = new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("Q174a", "1.2.208.176.1.5.1",
            "Skriv hvem der har udfyldt skemaet (fx ægtefælle eller sygeplejerske):", "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5.1")
            .setExtension("174")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Skriv hvem der har udfyldt skemaet (fx ægtefælle eller sygeplejerske):")
        .setText("Ægtefælle")
        .build();

    qrdMultipleChoiceResponse1 = new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("Q173a", "1.2.208.176.1.5.1", "Hvem har udfyldt dette spørgeskema?",
            "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5.1")
            .setExtension("173")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Hvem har udfyldt dette spørgeskema?")
        .setInterval(1, 1)
        .addAnswer("A173_3a", "1.2.208.176.1.5.1", "En anden har udfyldt skemaet for mig", "Sundhedsdatastyrelsen")
        .addReference(ref)
        .setAssociatedTextResponse(associatedTextQuestion173_a)
        .build();

    qrdTextResponse1 = new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("Q243a", "1.2.208.176.1.5.1", "Hvad er vigtigst for dig at få ud af behandlingen?",
            "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5.1")
            .setExtension("243")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Hvad er vigtigst for dig at få ud af behandlingen?")
        .setText("At blive rask")
        .addReference(ref)
        .build();

    sectionText_a.addOrganizer(new QRDOrganizerBuilder()
        .addId(new IDBuilder()
            .setRoot("1.2.208.176.7.3.3")
            .setExtension("123456789")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .addQRDResponse(qrdMultipleChoiceResponse1)
        .addQRDResponse(qrdTextResponse1)
        .build());
    cda.addSection(sectionText_a);

    // * discrete slider with 1 question, 1 answer *
    Section<QRDOrganizer> sectionDiscreteSlider = new Section<QRDOrganizer>("Generelt om dit helbred",
        narrativeTextDiscreteSlider);

    qrdDiscreteSliderResponse1 = new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("Q30", "1.2.208.176.1.5", "Hvordan synes du dit helbred er alt i alt?",
            "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5")
            .setExtension("30")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Hvordan synes du dit helbred er alt i alt?")
        .setMinimum(0)
        .setAnswer("A30_2", "1.2.208.176.1.5", "Vældig godt", "Sundhedsdatastyrelsen")
        .addReference(ref)
        .build();

    sectionDiscreteSlider.addOrganizer(new QRDOrganizerBuilder()
        .addId(new IDBuilder()
            .setRoot("1.2.208.176.7.3.3")
            .setExtension("2140")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .addQRDResponse(qrdDiscreteSliderResponse1)
        .build());
    cda.addSection(sectionDiscreteSlider);

    return cda;
  }

  private QRDDocument createBaseQRDDocument(String documentId) {
    Date documentCreationTime = DateUtil.makeDanishDateTimeWithTimeZone(2020, 11, 21, 20, 48, 0);
    Date documentAuthorTime = DateUtil.makeDanishDateTimeWithTimeZone(2020, 11, 07, 0, 0, 0);

    // Create document
    QRDDocument qrdDocument = new QRDDocument(MedCom.createId(documentId));
    qrdDocument.setLanguageCode("da-DK");
    qrdDocument.setTitle("Spørgeskema vedr. hjerterehabilitering, start");
    qrdDocument.setDocumentVersion(new IDBuilder().setRoot(MedCom.MESSAGECODE_OID).setExtension("2358344").build(), 1);
    qrdDocument.setEffectiveTime(documentCreationTime);

    AddressData authorAddress = new AddressData.AddressBuilder("2300", "København")
        .addAddressLine("PRO-sekretariatet")
        .addAddressLine("Ørestads Boulevard 5")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();

    qrdDocument.setPatient(new Patient.PatientBuilder().build());

    qrdDocument.setCustodian(new OrganizationBuilder()
        .setSOR("634491000016008")
        .setName("Sundhedsdatastyrelsen")
        .setAddress(authorAddress)
        .addTelecom(Use.WorkPlace, "tel", "+4591334808")
        .build());

    Telecom[] telecoms = new Telecom[1];
    telecoms[0] = new Telecom(Use.WorkPlace, "tel", "+4591334808");

    qrdDocument.setAuthor(new ParticipantBuilder()
        .setAddress(authorAddress)
        .setId(null)
        .setTelecomList(telecoms)
        .setTime(documentAuthorTime)
        .setOrganizationIdentity(new OrganizationBuilder().setName("Sundhedsdatastyrelsen").build())
        .build());

    return qrdDocument;

  }

  public Section<QRDOrganizer> setupQRDOrganizerWithDiscreteSliderAndAssociatedTextResponse() {

    Section<QRDOrganizer> sectionDiscreteSlider = new Section<QRDOrganizer>("Test af Discrete Slider",
        narrativeTextDiscreteSlider2);
    QRDTextResponse associatedTextQuestionQ_DISCRETE_02_01 = new QRDTextResponse.QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("Q.DISCRETE.02.01", "1.2.208.176.1.5.1", "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5.1")
            .setExtension("DISCRETE.02")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Hvad vil du svare?")
        .setText("Mit svar svæver i vinden")
        .build();

    QRDDiscreteSliderResponse qrdDiscreteSliderResponse2 = new QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("Q.DISCRETE.01.01", "1.2.208.176.1.5", "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5")
            .setExtension("DISCRETE.01")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Har du et svar?")
        .setMinimum(1)
        .setAnswer("A.DISCRETE.01.03", "1.2.208.176.1.5", "Andet", "Sundhedsdatastyrelsen")
        .setAssociatedTextResponse(associatedTextQuestionQ_DISCRETE_02_01)
        .build();

    sectionDiscreteSlider.addOrganizer(new QRDOrganizerBuilder()
        .addId(new IDBuilder()
            .setRoot("1.2.208.176.7.3.3")
            .setExtension("DISCRETE.01")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .addQRDResponse(qrdDiscreteSliderResponse2)
        .build());
    return sectionDiscreteSlider;

  }

}
