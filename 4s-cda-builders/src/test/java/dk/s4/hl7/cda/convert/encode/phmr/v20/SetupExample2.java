package dk.s4.hl7.cda.convert.encode.phmr.v20;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.codes.NPU;
import dk.s4.hl7.cda.model.*;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.Measurement.Status;
import dk.s4.hl7.cda.model.phmr.v20.MeasurementGroup;
import dk.s4.hl7.cda.model.phmr.v20.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

import java.util.Date;

public class SetupExample2 {

  /** Define a CDA for the Medcom example 2. */
  public static PHMRDocument defineAsCDA() {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 21, 10, 30, 0);

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data of a CDA.
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("b6a079b0-89ab-11e3-baa8-0800200c9a66")
        .setRoot(MedCom.ROOT_OID)
        .build();
    PHMRDocument cda = new PHMRDocument(idHeader);
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    cda.setPatient(nancy);

    cda.setTitle("   Hjemmemonitorering for " + nancy.getIdValue());
    cda.setLanguageCode("da-DK");

    // 1.1 Populate with time and version info
    cda.setEffectiveTime(documentCreationTime);

    // 1.3 Populate with Author, Custodian, and Authenticator
    PersonIdentity mathildeChristesen = new PersonIdentity.PersonBuilder("Christensen")
        .addGivenName("Mathilde")
        .setPrefix("Hjemmesygeplejerske")
        .build();

    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen")
        .addGivenName("Anders")
        .setPrefix("Hjertelæge")
        .build();

    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity svendborgHjerteMedicinskAfdeling = new OrganizationIdentity.OrganizationBuilder()
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    OrganizationIdentity hjemmeplejen = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("328151000016009")
        .setName("Hjemmesygeplejen, Svendborg kommune")
        .build();

    Date timeOfAuthor = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 24, 7, 53, 0);
    cda.setAuthor(createHjemmePlejeParticipant(mathildeChristesen, hjemmeplejen, timeOfAuthor));
    cda.setCustodian(new OrganizationIdentity.OrganizationBuilder()
        .setSOR("310541000016007")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build());
    Date timeOfAuthentication = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 21, 10, 30, 0);
    cda.setLegalAuthenticator(
        createAndersParticipant(andersAndersen, svendborgHjerteMedicinskAfdeling, timeOfAuthentication));

    // 1.4 Define the service period
    Date fromTime = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 20, 7, 53, 0);
    Date toTime = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 20, 14, 25, 0);
    cda.setDocumentationTimeInterval(fromTime, toTime);

    // 1.6 Add measurements (observations)

    // Example 2: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.

    ID id = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("b6a079b0-89ab-11e3-baa8-0800200c9a66")
        .setRoot(MedCom.ROOT_OID)
        .build();

    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.TypedByHealthcareProfessional,
        DataInputContext.PerformerType.HealthcareProfessional,
        new OrganizationIdentity.OrganizationBuilder().setSOR("328151000016009").build(),
        DataInputContext.CAREGIVER_USER_TYPE);
    Comment comment = new Comment(
        new ParticipantBuilder()
            .setSOR("310541000016007")
            .setPersonIdentity(andersAndersen)
            .setTime(DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 21, 10, 40, 0))
            .build(),
        "Jeg kan se at denne måling er tastet ind af Hjemmesygeplejerske Mathilde Christensen. Har mistanke om, at den høje værdi skyldes en indtastningsfejl. Målingen kan ikke godkendes. AA");
    Measurement systolic = NPU.createBloodPresureSystolic("253", toTime, context, id);
    systolic.setStatus(Status.NULLIFIED);
    systolic.setComment(comment);

    Measurement diastolic = NPU.createBloodPresureDiastolic("86", toTime, context, id);
    diastolic.setStatus(Status.NULLIFIED);

    MeasurementGroup measurements = new MeasurementGroup(toTime, Measurement.Status.COMPLETED);
    measurements.addMeasurement(systolic);
    measurements.addMeasurement(diastolic);
    cda.addVitalSignGroups(measurements);

    context = new DataInputContext(DataInputContext.ProvisionMethod.Electronically,
        DataInputContext.PerformerType.Citizen);
    Measurement weight = NPU.createWeight("77.3", fromTime, context, id);
    cda.addResultGroups(new MeasurementGroup(weight, fromTime, Measurement.Status.COMPLETED));

    return cda;
  }

  protected static Participant createAndersParticipant(PersonIdentity andersAndersen,
      OrganizationIdentity svendborgHjerteMedicinskAfdeling, Date timeOfAuthor) {
    Participant author = new ParticipantBuilder()
        .setSOR("310541000016007")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(svendborgHjerteMedicinskAfdeling)
        .setTime(timeOfAuthor)
        .build();
    return author;
  }

  protected static Participant createHjemmePlejeParticipant(PersonIdentity person,
      OrganizationIdentity svendborgHjerteMedicinskAfdeling, Date timeOfAuthor) {
    Participant author = new ParticipantBuilder()
        .setSOR("328151000016009")
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .addAddressLine("Svinget 14")
            .setCountry("Danmark")
            .setUse(Use.WorkPlace)
            .build())
        .setPersonIdentity(person)
        .setOrganizationIdentity(
            new OrganizationIdentity.OrganizationBuilder().setName("Hjemmesygeplejen, Svendborg kommune").build())
        .setTime(timeOfAuthor)
        .build();
    return author;
  }
}
