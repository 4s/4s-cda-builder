package dk.s4.hl7.cda.convert.decode;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.convert.CPDV20XmlCodec;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public class TestXmlToCpdV20ToXmlCompare extends BaseDecodeTest {

  private CPDV20XmlCodec codec = new CPDV20XmlCodec();

  @Before
  public void before() {
    codec = new CPDV20XmlCodec();
  }

  //CPD-DK_Care_Plan_example1.xml is created from various sources, to include many parts of the format
  @Test
  public void testXmlToCpdToXml_Example1() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "cpd/CPD-DK_Care_Plan_example1.xml");
    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToCpdToXml_KOL20190311Example() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "cpd/CPD-DK_Care_Plan_KOL_Example_2_Version_1.0.1_2019-03-11_adj.xml");

    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToCpdToXml_CPD_DK_Care_Plan_SAMBLIK_Example_02_MIN_v01()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "cpd/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_adj.xml");

    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  // xml fil tilsendt 7 juni 2021
  @Test
  public void testXmlToCpdToXml_CPD_DK_Care_Plan_SAMBLIK_Example_valid()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "cpd/CPD-DK_Care_Plan_SAMBLIK_Example_valid_adj.xml");

    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToCpdToXml_Careplan_doc_appointment_OK_Example()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "cpd/CPD-DK_Careplan_doc_appointment_OK_1.2.208.184_d021047e-9c15-451a-885a-bff76e8d8d96_adj.xml");

    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToCpdToXml_Careplan_TestNoTime() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "cpd/dateTimeTest/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_noTime.xml");

    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToCpdToXml_Careplan_TestSummer() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "cpd/dateTimeTest/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_summer.xml");

    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToCpdToXml_Careplan_TestWinter() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "cpd/dateTimeTest/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_winter.xml");

    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToCpdToXml_Careplan_ExamplesWithValueFormats()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "cpd/Examples_with_value_formats.xml");

    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToCpdToXml_Careplan_ExamplesWithDateOrDateInterval()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "cpd/Examples_with_date_or_dateInterval.xml");

    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToCpdToXml_Careplan_Example_represented_organization_adr_blank()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "cpd/CPD-DK_Care_Plan_Example1_adjusted_represented_organization_addr_blank.xml");

    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToCpdToXml_Careplan_Example_represented_organization_telecom_blank()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "cpd/CPD-DK_Care_Plan_Example1_adjusted_represented_organization_telecom_blank.xml");

    CPDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  // Some of the dateTimeTest files cannot be compared this way, since xml is always generated in local timezone and that is not UTC og minus five.

}
