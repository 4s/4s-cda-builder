package dk.s4.hl7.cda.convert.encode.apd.v20;

import java.util.Calendar;
import java.util.Date;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.OrganizationIdentity.OrganizationBuilder;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.AppointmentEncounterCode;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.Status;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public class SetupMedcomV20Example15ApdRegion {

  private static final String DOCUMENT_ID = "c50d18d1-c1e6-4c22-9f1c-575bbc5ce3c2";

  public static AppointmentDocument createBaseAppointmentDocument() {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTimeWithTimeZone(2020, 10, 30, 12, 20, 0);

    Date authorTime = DateUtil.makeDanishDateTimeWithTimeZone(2020, 10, 30, 11, 20, 0);
    Date authorReferrerTime = DateUtil.makeDanishDateTimeWithTimeZone(2020, 10, 29, 12, 20, 0);

    // Create document
    AppointmentDocument appointment = new AppointmentDocument(MedCom.createId(DOCUMENT_ID));
    appointment.setLanguageCode("da-DK");
    appointment.setTitle("Aftale for 2512489996");
    appointment.setEffectiveTime(documentCreationTime);
    // Create Patient

    AddressData nancyAddress = new AddressData.AddressBuilder("5000", "Odense C")
        .addAddressLine("Åvej 29")
        .setUse(AddressData.Use.WorkPlace)
        .build();

    Patient nancy = new Patient.PatientBuilder("Berggren")
        .setGender(Patient.Gender.Female)
        .setBirthTime(1948, Calendar.DECEMBER, 25)
        .addGivenName("Nancy")
        .addGivenName("Ann")
        .setSSN("2512489996")
        .setAddress(nancyAddress)
        .addTelecom(AddressData.Use.WorkPlace, "tel", "52659852")
        .build();

    appointment.setPatient(nancy);

    OrganizationIdentity providerOrganization = new OrganizationBuilder()
        .setSOR("393421000016009")
        .setName("Lægehuset Valdemarsgade")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Munkerisvej 10")
            .addAddressLine("Lægehuset")
            .setCity("Odense M")
            .setPostalCode("5230")
            .setCountry("Danmark")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .addTelecom(Use.HomeAddress, "mailto", "laege@valdemar.dk")
        .addTelecom(Use.HomeAddress, "http", "//www.LeagehusetValdemar.dk")
        .build();
    appointment.setProviderOrganization(providerOrganization);

    // Create Custodian organization
    OrganizationIdentity custodianOrganization = new OrganizationBuilder()
        .setSOR("378631000016009")
        .setName("Region Syds Data-ansvarlige organisation")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Datavej 2")
            .setCity("Odense S")
            .setPostalCode("5260")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "mailto", "dataansvarlige@rs.dk")
        .build();
    appointment.setCustodian(custodianOrganization);

    PersonIdentity dortheJensen = new PersonBuilder("Jensen").addGivenName("Dorte").build();

    OrganizationIdentity authorOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setName("OUH, Odense Universitetshospital")
        .build();

    appointment.setAuthor(new ParticipantBuilder()
        .setAddress(Setup.defineOdenseUniversitetshospitalAddress())
        .setSOR("378631000016009")
        .addTelecom(Use.WorkPlace, "tel", "65129999")
        .addTelecom(Use.WorkPlace, "mailto", "sundhed@ok.dk")
        .setTime(authorTime)
        .setPersonIdentity(dortheJensen)
        .setOrganizationIdentity(authorOrganization)
        .build());

    //    //Author - appointment requester (v. 2.0.1)
    PersonIdentity elinorStrom = new PersonBuilder("Strøm").addGivenName("Elinor").setPrefix("Læge").build();
    AddressData elinorAddress = new AddressData.AddressBuilder("5230", "Odense M")
        .addAddressLine("Munkerisvej 10")
        .addAddressLine("Lægehuset")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    appointment.setAuthorReferrer(new ParticipantBuilder()
        .setSOR("393421000016009")
        .setTime(authorReferrerTime)
        .setPersonIdentity(elinorStrom)
        .setAddress(elinorAddress)
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .addTelecom(Use.HomeAddress, "mailto", "laege@valdemar.dk")
        .addTelecom(Use.HomeAddress, "http", "//www.LeagehusetValdemar.dk")
        .build());

    // 1.4 Define the service period       
    Date from = DateUtil.makeDanishDateTimeWithTimeZone(2020, 11, 07, 11, 15, 0);
    Date to = DateUtil.makeDanishDateTimeWithTimeZone(2020, 11, 07, 12, 30, 0);

    appointment.setDocumentationTimeInterval(from, to);

    appointment.setCdaProfileAndVersion("apd-v2.0.1");

    appointment.setEpisodeOfCareLabel("ALAL21");
    appointment.setEpisodeOfCareLabelDisplayName("Kronisk obstruktiv lungesygdom (KOL)  ");

    appointment.addEpisodeOfCareIdentifier(new ID.IDBuilder()
        .setAuthorityName("Sundhedsvæsenets Klassifikations System")
        .setExtension("cf0b2a79-dff1-4cdc-87d3-4e0c1fa7e991")
        .setRoot("1.2.208.176.7.1.10.83")
        .build());
    appointment.addEpisodeOfCareIdentifier(new ID.IDBuilder()
        .setAuthorityName("Sundhedsvæsenets Klassifikations System")
        .setExtension("11ea56fe-307f-4f54-8c37-ed09fcf73179")
        .setRoot("1.2.208.176.7.1.10.83")
        .build());

    OrganizationIdentity appointmentLocation = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("325441000016006")
        .setName("opgang 4, 3 sal rum 101")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Regionshospitalet")
            .addAddressLine("Albanigade 10")
            .setCity("Odense")
            .setPostalCode("5000")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "63221313")
        .build();

    appointment.setAppointmentTitle("Aftale");
    appointment.setAppointmentText("<paragraph>Aftale:</paragraph>" + "<table width=\"100%\">" + "<tbody>" + "<tr>"
        + "<th>Aftale dato</th>" + "<th>Vedrørende</th>" + "<th>Udførende organisation</th>" + "<th>Mødested</th>"
        + "<th>Kommentar</th>" + "</tr>" + "<tr>" + "<td>2020-12-07 11:15 - 2020-12-07 12:30</td>"
        + "<td>lægekonsultation med ambulant patient</td>"
        + "<td>Regionshospitalet, Albanigade 10, 5000 Odense, Tlf. 66113322</td>"
        + "<td>Regionshospitalet, Albanigade 10, 5000 Odense, opgang 4, 3 sal rum 101</td>" + "<td></td>" + "</tr>"
        + "</tbody>" + "</table>");
    appointment.setAppointmentId(new IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension("0b3cfb2f-f76f-4a42-8098-2455ac7ece83")
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build());

    appointment.setAppointmentStatus(Status.ACTIVE);

    appointment.setIndicationCode(new CodedValue.CodedValueBuilder()
        .setCode("17436001")
        .setDisplayName("lægekonsultation med ambulant patient")
        .setCodeSystem("2.16.840.1.113883.6.96")
        .setCodeSystemName("SNOMED CT")
        .build());

    appointment.setAppointmentLocation(appointmentLocation);

    AddressData performerAddress = new AddressData.AddressBuilder("5000", "Odense")
        .addAddressLine("Albanigade 12")
        .setUse(AddressData.Use.WorkPlace)
        .build();

    OrganizationIdentity organizationIdentity = new OrganizationIdentity.OrganizationBuilder()
        .setName("Regionshospitalet")
        .setAddress(performerAddress)
        .addTelecom(Use.WorkPlace, "tel", "66113322")
        .build();

    Participant appointmentPerformer = new ParticipantBuilder()
        .setSOR("325441000016006")
        .setTime(authorTime)
        .setOrganizationIdentity(organizationIdentity)
        .build();

    appointment.setAppointmentPerformer(appointmentPerformer);

    appointment.setAppointmentEncounterCode(AppointmentEncounterCode.RegionalAppointment);

    return appointment;
  }

}
