package dk.s4.hl7.cda.model.testutil;

import java.util.Calendar;

import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.Patient.PatientBuilder;
import dk.s4.hl7.cda.model.util.DateUtil;

/**
 * This class contains a set of static create methods that is widely reused
 * throughout all the setup code for the individual examples.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class Setup {

  public static AddressData defineNancyAddress() {
    AddressData nancyAddress = new AddressData.AddressBuilder("5700", "Svendborg")
        .setCountry("Danmark")
        .addAddressLine("Skovvejen 12")
        .addAddressLine("Landet")
        .setUse(AddressData.Use.HomeAddress)
        .build();
    return nancyAddress;
  }

  public static AddressData defineHjerteMedicinskAfdAddress() {
    AddressData hjertemedicinskAddress = new AddressData.AddressBuilder("5700", "Svendborg")
        .addAddressLine("Hjertemedicinsk afdeling B")
        .addAddressLine("Valdemarsgade 53")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    return hjertemedicinskAddress;
  }

  public static AddressData defineValdemarsGade53Address() {
    AddressData valdemarsGade53 = new AddressData.AddressBuilder("5700", "Svendborg")
        .addAddressLine("Valdemarsgade 53")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    return valdemarsGade53;
  }

  public static AddressData defineValdemarsGade10Address() {
    AddressData valdemarsGade10 = new AddressData.AddressBuilder("5700", "Svendborg")
        .addAddressLine("Valdemarsgade 10")
        .addAddressLine("Lægehuset")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    return valdemarsGade10;
  }

  public static AddressData defineVestergade5Address() {
    AddressData vestergade5 = new AddressData.AddressBuilder("3000", "Odense")
        .addAddressLine("Vestergade 5")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    return vestergade5;
  }

  public static AddressData defineNeurologiskAfdAddress() {
    AddressData hjertemedicinskAddress = new AddressData.AddressBuilder("5700", "Svendborg")
        .addAddressLine("Neurologisk afdeling C")
        .addAddressLine("Valdemarsgade 53")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    return hjertemedicinskAddress;
  }

  public static AddressData defineOdenseKommuneSundhedsenhedsforvaltningenAddress() {
    AddressData addressData = new AddressData.AddressBuilder("5220", "Odense SØ")
        .addAddressLine("Odense Kommune, Sundhedsenhedsforvaltningen")
        .addAddressLine("Ørbækvej 100")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    return addressData;
  }

  public static AddressData defineOdenseUniversitetshospitalAddress() {
    AddressData addressData = new AddressData.AddressBuilder("5000", "Odense")
        .addAddressLine("Odense Universitetshospital")
        .addAddressLine("J. B. Winsløws Vej 4")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    return addressData;
  }

  public static Patient defineNancyAsFullPersonIdentity() {
    return defineNancyAsFullPersonIdentity("2512484916", false, false, 0, 0, 0);
  }

  public static Patient defineNancyAsFullPersonIdentity(String cpr, boolean includeBirthTime, boolean isSummer,
      int hour, int minute, int second) {

    PatientBuilder nancyBuilder = new Patient.PatientBuilder("Berggren")
        .setGender(Patient.Gender.Female)
        .addGivenName("Nancy")
        .addGivenName("Ann")
        .setSSN(cpr)
        .setAddress(defineNancyAddress())
        .addTelecom(AddressData.Use.HomeAddress, "tel", "65123456")
        .addTelecom(AddressData.Use.WorkPlace, "mailto", "nab@udkantsdanmark.dk");
    if (includeBirthTime) {
      if (!isSummer) {
        nancyBuilder.setBirthDateAndTime(DateUtil.makeDanishDateTimeWithTimeZone(1948, 11, 25, hour, minute, second));
      } else {
        nancyBuilder.setBirthDateAndTime(DateUtil.makeDanishDateTimeWithTimeZone(1948, 05, 25, hour, minute, second));
      }

    } else {
      nancyBuilder.setBirthTime(1948, Calendar.DECEMBER, 25);
    }

    return nancyBuilder.build();
  }

  public static Patient defineNancyAsFullPersonIdentity(String cpr) {
    return defineNancyAsFullPersonIdentity(cpr, false, false, 0, 0, 0);
  }

}
