package dk.s4.hl7.cda.convert.encode.apd.v20;

import java.util.Calendar;
import java.util.Date;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.OrganizationIdentity.OrganizationBuilder;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.AppointmentEncounterCode;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.Status;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public class SetupMedcomV20Example16ApdGpV2 {

  private static final String DOCUMENT_ID = "c6559092-5256-4edd-9090-911bab728be0";

  public static AppointmentDocument createBaseAppointmentDocument() {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTimeWithTimeZone(2020, 10, 30, 12, 20, 0);

    Date authorTime = DateUtil.makeDanishDateTimeWithTimeZone(2020, 10, 30, 11, 20, 0);
    Date authorReferrerTime = DateUtil.makeDanishDateTimeWithTimeZone(2020, 10, 29, 12, 20, 0);

    // Create document
    AppointmentDocument appointment = new AppointmentDocument(MedCom.createId(DOCUMENT_ID));
    appointment.setLanguageCode("da-DK");
    appointment.setTitle("Aftale for 2512489996");
    appointment.setEffectiveTime(documentCreationTime);
    // Create Patient

    AddressData nancyAddress = new AddressData.AddressBuilder("5260", "Odense S")
        .addAddressLine("Hindbærvej 23")
        .setUse(AddressData.Use.HomeAddress)
        .build();

    Patient nancy = new Patient.PatientBuilder("Berggren")
        .setGender(Patient.Gender.Female)
        .setBirthTime(1948, Calendar.DECEMBER, 25)
        .addGivenName("Nancy")
        .addGivenName("Ann")
        .setSSN("2512489996")
        .setAddress(nancyAddress)
        .build();

    appointment.setPatient(nancy);

    OrganizationIdentity providerOrganization = new OrganizationBuilder()
        .setSOR("393421000016009")
        .setName("Lægehuset Valdemarsgade")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Valdemarsgade 10")
            .addAddressLine("Lægehuset")
            .setCity("Svendborg")
            .setPostalCode("5700")
            .setCountry("Danmark")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .addTelecom(Use.HomeAddress, "mailto", "laege@valdemar.dk")
        .addTelecom(Use.HomeAddress, "http", "//www.LeagehusetValdemar.dk")
        .build();
    appointment.setProviderOrganization(providerOrganization);

    // Create Custodian organization
    OrganizationIdentity custodianOrganization = new OrganizationBuilder()
        .setSOR("378631000016009")
        .setName("Lægehusets Data-ansvarlige organisation")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Lægesystem A/S")
            .addAddressLine("Datavej 1")
            .setCity("Odense S")
            .setPostalCode("5260")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "mailto", "dataansvarlige@rs.dk")
        .build();
    appointment.setCustodian(custodianOrganization);

    //Author - appointment responsible
    OrganizationIdentity authorOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setName("Lægehuset Valdemarsgade")
        .setAddress(Setup.defineValdemarsGade10Address())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .build();
    PersonIdentity jensJensen = new PersonBuilder("Jensen")
        .addGivenName("Jens")
        .addGivenName("Bo")
        .addGivenName("Henrik")
        .setPrefix("Speciallæge")
        .build();
    appointment.setAuthor(new ParticipantBuilder()
        .setSOR("393421000016009")
        .setTime(authorTime)
        .setPersonIdentity(jensJensen)
        .setOrganizationIdentity(authorOrganization)
        .build());

    //    //Author - appointment requester (v. 2.0.1)
    PersonIdentity elinorStrom = new PersonBuilder("Strøm").addGivenName("Elinor").setPrefix("Læge").build();
    AddressData elinorAddress = new AddressData.AddressBuilder("5000", "Odense C")
        .addAddressLine("Lægeklinikken")
        .addAddressLine("Åvej 29")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    appointment.setAuthorReferrer(new ParticipantBuilder()
        .setSOR("191901000016999")
        .setTime(authorReferrerTime)
        .setPersonIdentity(elinorStrom)
        .setAddress(elinorAddress)
        .addTelecom(Use.WorkPlace, "tel", "52659852")
        .build());

    // 1.4 Define the service period       
    Date from = DateUtil.makeDanishDateTimeWithTimeZone(2020, 11, 01, 13, 15, 0);
    Date to = DateUtil.makeDanishDateTimeWithTimeZone(2020, 11, 01, 13, 30, 0);

    appointment.setDocumentationTimeInterval(from, to);

    appointment.setCdaProfileAndVersion("apd-v2.0.1");

    OrganizationIdentity appointmentLocation = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("393421000016009")
        .setName("Lægehuset Valdemarsgade")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Valdemarsgade 10")
            .setCity("Svendborg")
            .setPostalCode("5700")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "66113333")
        .build();

    appointment.setAppointmentTitle("Aftale");
    appointment.setAppointmentText("<paragraph>Aftale:</paragraph>" + "<table width=\"100%\">" + "<tbody>" + "<tr>"
        + "<th>Aftale dato</th>" + "<th>Vedrørende</th>" + "<th>Mødested</th>" + "<th>Kommentar</th>" + "</tr>" + "<tr>"
        + "<td>2020-12-01 13:15 - 2020-12-01 13:30</td>" + "<td>Speciallægekonsultation</td>"
        + "<td>Lægehuset Valdemarsgade</td>" + "<td></td>" + "</tr>" + "</tbody>" + "</table>");
    appointment.setAppointmentId(new IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension("36e2952f-0b6d-419a-b630-1fc515af4021")
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build());
    appointment.setAppointmentStatus(Status.ACTIVE);

    appointment.setIndicationDisplayName("Speciallægekonsultation");
    appointment.setAppointmentLocation(appointmentLocation);

    AddressData performerAddress = new AddressData.AddressBuilder("5700", "Svendborg")
        .addAddressLine("Valdemarsgade 10")
        .setUse(AddressData.Use.WorkPlace)
        .build();

    OrganizationIdentity organizationIdentity = new OrganizationIdentity.OrganizationBuilder()
        .setName("Lægehuset Valdemarsgade")
        .setAddress(performerAddress)
        .addTelecom(Use.WorkPlace, "tel", "66113333")
        .build();

    //    ParticipantBuilder participantBuilder = new ParticipantBuilder();
    //    participantBuilder
    //    .setAddress(vestergade5WithoutCountry)
    //    .setSOR("378631000016009")
    //    .addTelecom(Use.WorkPlace, "tel", "66113333-3")
    //    .setTime(authorTime)
    //    .setOrganizationIdentity(organizationIdentity);
    //    Participant appointmentPerformer = participantBuilder.build();

    Participant appointmentPerformer = new ParticipantBuilder()
        .setSOR("393421000016009")
        .setTime(authorTime)
        .setOrganizationIdentity(organizationIdentity)
        .build();

    appointment.setAppointmentPerformer(appointmentPerformer);

    appointment.setAppointmentEncounterCode(AppointmentEncounterCode.PractitionerAppointment);

    return appointment;
  }

}
