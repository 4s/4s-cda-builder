package dk.s4.hl7.cda.convert.decode;

import static dk.s4.hl7.cda.convert.APDXmlCodec.Version.V11;
import static dk.s4.hl7.cda.convert.decode.TestXmlToApdUtil.validateStatusCode;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.APDXmlCodec;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.decode.TestXmlToApdUtil.TestXmlToApd;
import dk.s4.hl7.cda.convert.encode.apd.SetupMedcomExample;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.StatusV11;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public final class TestXmlToApdV11 extends BaseDecodeTest implements ConcurrencyTestCase, TestXmlToApd {

  private APDXmlCodec codec;

  @Before
  public void before() {
    setCodec(new APDXmlCodec(V11));
  }

  public void setCodec(APDXmlCodec codec) {
    this.codec = codec;
  }

  @Override
  public AppointmentDocument decode(String xml) {
    return super.decode(codec, xml);
  }

  @Override
  public String getStatusVersion() {
    return "StatusV11";
  }

  @Test
  public void testApdDefaultXMLConverter() {
    String markup = null;
    try {
      markup = FileUtil.getData(this.getClass(), "apd/DK-APD_V11_Eksempel_default.markup.xml");
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }

    assertNotNull(markup);

    setCodec(new APDXmlCodec(/* APDXmlCodec.Version.V11 */)); // Expect default
                                                              // Codex to be
                                                              // same as version
                                                              // 1.1

    validateStatusCode(this, markup, "active");
    validateStatusCode(this, markup, "suspended");
    validateStatusCode(this, markup, "completed");
    validateStatusCode(this, markup, "aborted");

    validateStatusCode(this, markup, "cancelled", true); // Expecting exception
  }

  @Test
  public void testApd11XMLConverter() {
    String markup = null;
    try {
      markup = FileUtil.getData(this.getClass(), "apd/DK-APD_V11_Eksempel_default.markup.xml");
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }

    assertNotNull(markup);

    validateStatusCode(this, markup, "active");
    validateStatusCode(this, markup, "suspended");
    validateStatusCode(this, markup, "completed");
    validateStatusCode(this, markup, "aborted");

    validateStatusCode(this, markup, "cancelled", true); // Expecting exception
  }

  @Test
  public void testApdXMLConverter() {
    try {
      String XML = FileUtil.getData(this.getClass(), "apd/DK-APD_Example_1.xml");

      AppointmentDocument a = decode(codec, XML);
      a.getAuthor();
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testPHMRXmlConverterMedcomExampleV11() throws Exception {
    encodeDecodeAndCompare(new APDXmlCodec(V11), SetupMedcomExample.createBaseAppointmentDocument(StatusV11.ACTIVE));
  }

  @Override
  public void runTest() throws Exception {
    testPHMRXmlConverterMedcomExampleV11();
  }

}
