package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.PDCV20XmlCodec;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Patient.Gender;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.pdc.v20.AuthorOfInformation;
import dk.s4.hl7.cda.model.pdc.v20.PDCDocument;
import dk.s4.hl7.cda.model.pdc.v20.AuthorOfInformation.SourceOfInformation;
import dk.s4.hl7.cda.model.TelecomPhone;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public final class TestXmlToPdcV20 extends BaseDecodeTest implements ConcurrencyTestCase {

  private PDCV20XmlCodec codec;

  @Before
  public void before() {
    setCodec(new PDCV20XmlCodec());
  }

  public void setCodec(PDCV20XmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testPdcXMLConverterMedcomExampleMinimum() {
    try {
      String XML = FileUtil.getData(this.getClass(), "pdc/PDC20_Example-minimum_data.xml");

      PDCDocument pdcDocument = decode(codec, XML);

      // # HEADER #

      assertNotNull(pdcDocument);
      assertNotNull(pdcDocument.getRealmCode());
      assertEquals("DK", pdcDocument.getRealmCode());
      assertNotNull(pdcDocument.getTypeIdExtension());
      assertEquals("POCD_HD000040", pdcDocument.getTypeIdExtension());
      assertNotNull(pdcDocument.getTypeIdRoot());
      assertEquals("2.16.840.1.113883.1.3", pdcDocument.getTypeIdRoot());
      assertNotNull(pdcDocument.getTemplateIds());
      assertNotNull(pdcDocument.getTemplateIds()[0]);
      assertEquals("1.2.208.184.16.1", pdcDocument.getTemplateIds()[0]);
      assertNotNull(pdcDocument.getId().getExtension());
      assertEquals("94327cc4-491a-4b48-880a-e1b9dec20136", pdcDocument.getId().getExtension());
      assertNotNull(pdcDocument.getId().getRoot());
      assertEquals("1.2.208.184", pdcDocument.getId().getRoot());
      assertNotNull(pdcDocument.getId().getAuthorityName());
      assertEquals("MedCom", pdcDocument.getId().getAuthorityName());
      assertNotNull(pdcDocument.getCodeCodedValue());
      assertNotNull(pdcDocument.getCodeCodedValue().getCode());
      assertEquals("PDC", pdcDocument.getCodeCodedValue().getCode());
      assertNotNull(pdcDocument.getCodeCodedValue().getDisplayName());
      assertEquals("Stamkort", pdcDocument.getCodeCodedValue().getDisplayName());
      assertNotNull(pdcDocument.getCodeCodedValue().getCodeSystem());
      assertEquals("1.2.208.184.100.1", pdcDocument.getCodeCodedValue().getCodeSystem());
      // assertNotNull(pdcDocument.getCodeCodedValue().getCodeSystemName()); //is not
      // parsed: MedCom Message Codes
      assertNotNull(pdcDocument.getTitle());
      assertEquals("Personal Data Card for 2512489996", pdcDocument.getTitle());
      assertNotNull(pdcDocument.getEffectiveTime());
      assertEquals(new GregorianCalendar(2021, 0, 1, 0, 0, 1).getTime(), pdcDocument.getEffectiveTime());

      // assertNotNull(pdcDocument.getConfidentialityCode()); //is not parsed
      assertNotNull(pdcDocument.getLanguageCode());
      assertEquals("da-DK", pdcDocument.getLanguageCode());

      assertNotNull(pdcDocument.getPatient().getId());
      assertNotNull(pdcDocument.getPatient().getId().getExtension());
      assertEquals("2512489996", pdcDocument.getPatient().getId().getExtension());
      assertNotNull(pdcDocument.getPatient().getId().getAuthorityName());
      assertEquals("CPR", pdcDocument.getPatient().getId().getAuthorityName());
      assertNotNull(pdcDocument.getPatient().getId().getRoot());
      assertEquals("1.2.208.176.1.2", pdcDocument.getPatient().getId().getRoot());
      assertNotNull(pdcDocument.getPatient().getAddress());
      assertNotNull(pdcDocument.getPatient().getAddress().getAddressUse());
      assertEquals(AddressData.Use.HomeAddress, pdcDocument.getPatient().getAddress().getAddressUse());
      assertNotNull(pdcDocument.getPatient().getAddress().getStreet());
      assertNotNull(pdcDocument.getPatient().getAddress().getStreet()[0]);
      assertEquals("Hans Jensens Stræde 45", pdcDocument.getPatient().getAddress().getStreet()[0]);
      assertNotNull(pdcDocument.getPatient().getAddress().getPostalCode());
      assertEquals("5000", pdcDocument.getPatient().getAddress().getPostalCode());
      assertNotNull(pdcDocument.getPatient().getAddress().getCity());
      assertEquals("Odense", pdcDocument.getPatient().getAddress().getCity());
      assertNotNull(pdcDocument.getPatient().getAddress().getCountry());
      assertEquals("Danmark", pdcDocument.getPatient().getAddress().getCountry());

      assertNotNull(pdcDocument.getPatient().getGivenNames());
      assertNotNull(pdcDocument.getPatient().getGivenNames()[0]);
      assertEquals("Nancy", pdcDocument.getPatient().getGivenNames()[0]);
      assertNotNull(pdcDocument.getPatient().getGivenNames()[1]);
      assertEquals("Ann", pdcDocument.getPatient().getGivenNames()[1]);
      assertNotNull(pdcDocument.getPatient().getFamilyName());
      assertEquals("Berggren", pdcDocument.getPatient().getFamilyName());
      assertNotNull(pdcDocument.getPatient().getGender());
      assertEquals(Gender.Female, pdcDocument.getPatient().getGender());
      assertNotNull(pdcDocument.getPatient().getBirthDate());
      assertEquals(new GregorianCalendar(1948, 11, 25, 0, 0, 0).getTime(), pdcDocument.getPatient().getBirthDate());

      assertNotNull(pdcDocument.getProviderOrganization());
      assertNotNull(pdcDocument.getProviderOrganization().getId());
      assertNotNull(pdcDocument.getProviderOrganization().getId().getExtension());
      assertEquals("123456", pdcDocument.getProviderOrganization().getId().getExtension());
      assertNotNull(pdcDocument.getProviderOrganization().getId().getAuthorityName());
      assertEquals("Yderregisteret", pdcDocument.getProviderOrganization().getId().getAuthorityName());
      assertNotNull(pdcDocument.getProviderOrganization().getId().getRoot());
      assertEquals("1.2.208.176.1.4", pdcDocument.getProviderOrganization().getId().getRoot());
      assertNotNull(pdcDocument.getProviderOrganization().getOrgName());
      assertEquals("Testyder", pdcDocument.getProviderOrganization().getOrgName());
      assertNotNull(pdcDocument.getProviderOrganization().getTelecomList());
      assertNotNull(pdcDocument.getProviderOrganization().getTelecomList()[0]);
      assertNotNull(pdcDocument.getProviderOrganization().getTelecomList()[0].getAddressUse());
      assertEquals(AddressData.Use.WorkPlace,
          pdcDocument.getProviderOrganization().getTelecomList()[0].getAddressUse());
      assertNotNull(pdcDocument.getProviderOrganization().getTelecomList()[0].getProtocol());
      assertEquals("tel", pdcDocument.getProviderOrganization().getTelecomList()[0].getProtocol());
      assertNotNull(pdcDocument.getProviderOrganization().getTelecomList()[0].getValue());
      assertEquals("+4512345678", pdcDocument.getProviderOrganization().getTelecomList()[0].getValue());
      assertNotNull(pdcDocument.getProviderOrganization().getAddress());
      assertNotNull(pdcDocument.getProviderOrganization().getAddress().getAddressUse());
      assertEquals(AddressData.Use.WorkPlace, pdcDocument.getProviderOrganization().getAddress().getAddressUse());
      assertNotNull(pdcDocument.getProviderOrganization().getAddress().getStreet());
      assertNotNull(pdcDocument.getProviderOrganization().getAddress().getStreet()[0]);
      assertEquals("Ydervej 42", pdcDocument.getProviderOrganization().getAddress().getStreet()[0]);
      assertNotNull(pdcDocument.getProviderOrganization().getAddress().getPostalCode());
      assertEquals("1234", pdcDocument.getProviderOrganization().getAddress().getPostalCode());
      assertNotNull(pdcDocument.getProviderOrganization().getAddress().getCity());
      assertEquals("Yderby", pdcDocument.getProviderOrganization().getAddress().getCity());

      assertNotNull(pdcDocument.getAuthor());
      assertNotNull(pdcDocument.getAuthor().getTime());
      assertEquals(new GregorianCalendar(2021, 0, 1, 0, 0, 1).getTime(), pdcDocument.getAuthor().getTime());
      assertNull(pdcDocument.getAuthor().getId());
      assertNotNull(pdcDocument.getAuthor().getOrganizationIdentity());
      assertNull(pdcDocument.getAuthor().getOrganizationIdentity().getId());
      assertNotNull(pdcDocument.getAuthor().getOrganizationIdentity().getOrgName());
      assertEquals("Sundhedsdatastyrelsen", pdcDocument.getAuthor().getOrganizationIdentity().getOrgName());

      assertNotNull(pdcDocument.getCustodianIdentity());
      assertNull(pdcDocument.getCustodianIdentity().getId());
      assertNotNull(pdcDocument.getCustodianIdentity().getOrgName());
      assertEquals("Sundhedsdatastyrelsen", pdcDocument.getCustodianIdentity().getOrgName());

      assertNotNull(pdcDocument.getServiceStartTime());
      assertEquals(new GregorianCalendar(2021, 0, 1, 0, 0, 1).getTime(), pdcDocument.getServiceStartTime());
      assertNull(pdcDocument.getServiceStopTime());
      assertNotNull(pdcDocument.getCdaProfileAndVersion());
      assertEquals("pdc-v2.0", pdcDocument.getCdaProfileAndVersion());

      // # CONTENT #

      assertNotNull(pdcDocument.getText());
      assertTrue(pdcDocument.getText().contains("<td>Borgerens navn og adresse</td>"));

      assertNull(pdcDocument.getCustodyInformationList());

      // NameAndAddressInformation

      assertId(pdcDocument.getNameAndAddressInformation().getId(), "4ed01b10-ae6c-4e08-ae04-e0fe464f6a5f",
          "1.2.208.184", null);

      assertCode(pdcDocument.getNameAndAddressInformation().getCode(), "CitizenNameAddr", "Borgerens navn og adresse",
          "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData());
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet());
      assertEquals(4, pdcDocument.getNameAndAddressInformation().getAddressData().getStreet().length);
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[0]);
      assertEquals("H C Andersens Hus", pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[0]);
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[1]);
      assertEquals("Den gamle bydel", pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[1]);
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[2]);
      assertEquals("Museum", pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[2]);
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[3]);
      assertEquals("Hans Jensens Stræde 45",
          pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[3]);

      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getCity());
      assertEquals("Odense", pdcDocument.getNameAndAddressInformation().getAddressData().getCity());
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getPostalCode());
      assertEquals("5000", pdcDocument.getNameAndAddressInformation().getAddressData().getPostalCode());
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getCountry());
      assertEquals("Danmark", pdcDocument.getNameAndAddressInformation().getAddressData().getCountry());
      assertEquals(AddressData.Use.HomeAddress,
          pdcDocument.getNameAndAddressInformation().getAddressData().getAddressUse());

      assertPersonIdentity(pdcDocument.getNameAndAddressInformation().getPersonIdentity(),
          new PersonBuilder("Berggren").addGivenName("Nancy").addGivenName("Ann").build());

      assertNull(pdcDocument.getNameAndAddressInformation().getConfidentialId());
      assertFalse(pdcDocument.getNameAndAddressInformation().isAddressConfidential());

      assertAuthorRegisterEntered(pdcDocument.getNameAndAddressInformation().getAuthorOfInformation(), "CPR",
          "1.2.208.176.1.2", "CPR");

      // CoverageGroup

      assertId(pdcDocument.getCoverageGroup().getId(), "51a22378-fd6e-40f7-b429-db0fa927de11", "1.2.208.184", null);

      assertCode(pdcDocument.getCoverageGroup().getCode(), "CoverageGroup", "Sygesikringsgruppe", "1.2.208.184.100.1",
          "MedCom Message Codes");

      assertNotNull(pdcDocument.getCoverageGroup().getCoverageGroup());
      assertEquals("2", pdcDocument.getCoverageGroup().getCoverageGroup());

      assertAuthorRegisterEntered(pdcDocument.getCoverageGroup().getAuthorOfInformation(), "Sygesikringen",
          "1.2.208.176.2.7", "Sygesikringen");

      // OrganDonorRegistration

      assertId(pdcDocument.getOrganDonorRegistration().getId(), "3231b4ea-b73d-480a-9715-f6290ffcda8e", "1.2.208.184",
          null);
      assertCode(pdcDocument.getOrganDonorRegistration().getCode(), "OrganDonorRegistration", "Registreret organdonor",
          "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getOrganDonorRegistration().isRegistered());
      assertEquals(false, pdcDocument.getOrganDonorRegistration().isRegistered());

      assertAuthorRegisterEntered(pdcDocument.getOrganDonorRegistration().getAuthorOfInformation(),
          "Dansk Center For Organdonation", "1.2.208.176.1.10", "Dansk Center For Organdonation");

      // TreatmentWillRegistration

      assertId(pdcDocument.getTreatmentWillRegistration().getId(), "e16160e7-e00c-4f33-abaa-faa012bccda7",
          "1.2.208.184", null);

      assertCode(pdcDocument.getTreatmentWillRegistration().getCode(), "TreatmentWillRegistration",
          "Registreret behandlingstestamente", "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getTreatmentWillRegistration().isRegistered());
      assertEquals(false, pdcDocument.getTreatmentWillRegistration().isRegistered());

      assertAuthorRegisterEntered(pdcDocument.getTreatmentWillRegistration().getAuthorOfInformation(),
          "Sundhedsdatastyrelsen", "1.2.208.176.1.9", "Sundhedsdatastyrelsen");

      // LivingWillRegistration

      assertId(pdcDocument.getLivingWillRegistration().getId(), "fdb6d2ce-ecee-4e89-a7e0-b86e76085f09", "1.2.208.184",
          null);
      assertCode(pdcDocument.getLivingWillRegistration().getCode(), "LivingWillRegistration",
          "Registreret livstestamente", "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getLivingWillRegistration().isRegistered());
      assertEquals(false, pdcDocument.getLivingWillRegistration().isRegistered());

      assertAuthorRegisterEntered(pdcDocument.getLivingWillRegistration().getAuthorOfInformation(),
          "Sundhedsdatastyrelsen", "1.2.208.176.1.8", "Sundhedsdatastyrelsen");

      assertNull(pdcDocument.getManuallyEnteredSpokenLanguage());
      assertNull(pdcDocument.getManuallyEnteredTemporaryAddress());
      assertNull(pdcDocument.getManuallyEnteredDentistInformation());
      assertNull(pdcDocument.getManuallyEnteredContactInformation());
      assertNull(pdcDocument.getManuallyEnteredInformationAboutRelativesList());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testPdcXMLConverterMedcomExampleMaximum() {
    try {
      String XML = FileUtil.getData(this.getClass(), "pdc/PDC20_Example-maximum_data.xml");

      PDCDocument pdcDocument = decode(codec, XML);

      // # HEADER #

      assertNotNull(pdcDocument);
      assertNotNull(pdcDocument.getRealmCode());
      assertEquals("DK", pdcDocument.getRealmCode());
      assertNotNull(pdcDocument.getTypeIdExtension());
      assertEquals("POCD_HD000040", pdcDocument.getTypeIdExtension());
      assertNotNull(pdcDocument.getTypeIdRoot());
      assertEquals("2.16.840.1.113883.1.3", pdcDocument.getTypeIdRoot());
      assertNotNull(pdcDocument.getTemplateIds());
      assertNotNull(pdcDocument.getTemplateIds()[0]);
      assertEquals("1.2.208.184.16.1", pdcDocument.getTemplateIds()[0]);
      assertNotNull(pdcDocument.getId().getExtension());
      assertEquals("6caa5b52-fa0f-43fa-9f63-2620aed17869", pdcDocument.getId().getExtension());
      assertNotNull(pdcDocument.getId().getRoot());
      assertEquals("1.2.208.184", pdcDocument.getId().getRoot());
      assertNotNull(pdcDocument.getId().getAuthorityName());
      assertEquals("MedCom", pdcDocument.getId().getAuthorityName());
      assertNotNull(pdcDocument.getCodeCodedValue());
      assertNotNull(pdcDocument.getCodeCodedValue().getCode());
      assertEquals("PDC", pdcDocument.getCodeCodedValue().getCode());
      assertNotNull(pdcDocument.getCodeCodedValue().getDisplayName());
      assertEquals("Stamkort", pdcDocument.getCodeCodedValue().getDisplayName());
      assertNotNull(pdcDocument.getCodeCodedValue().getCodeSystem());
      assertEquals("1.2.208.184.100.1", pdcDocument.getCodeCodedValue().getCodeSystem());
      // assertNotNull(pdcDocument.getCodeCodedValue().getCodeSystemName()); //is not
      // parsed: MedCom Message Codes
      assertNotNull(pdcDocument.getTitle());
      assertEquals("Personal Data Card for 2512489996", pdcDocument.getTitle());
      assertNotNull(pdcDocument.getEffectiveTime());
      assertEquals(new GregorianCalendar(2021, 0, 1, 0, 0, 1).getTime(), pdcDocument.getEffectiveTime());
      // assertNotNull(pdcDocument.getConfidentialityCode()); //is not parsed
      assertNotNull(pdcDocument.getLanguageCode());
      assertEquals("da-DK", pdcDocument.getLanguageCode());

      assertNotNull(pdcDocument.getPatient().getId());
      assertNotNull(pdcDocument.getPatient().getId().getExtension());
      assertEquals("2512489996", pdcDocument.getPatient().getId().getExtension());
      assertNotNull(pdcDocument.getPatient().getId().getAuthorityName());
      assertEquals("CPR", pdcDocument.getPatient().getId().getAuthorityName());
      assertNotNull(pdcDocument.getPatient().getId().getRoot());
      assertEquals("1.2.208.176.1.2", pdcDocument.getPatient().getId().getRoot());
      assertNotNull(pdcDocument.getPatient().getAddress());
      assertNotNull(pdcDocument.getPatient().getAddress().getAddressUse());
      assertEquals(AddressData.Use.HomeAddress, pdcDocument.getPatient().getAddress().getAddressUse());
      assertNotNull(pdcDocument.getPatient().getAddress().getStreet());
      assertNotNull(pdcDocument.getPatient().getAddress().getStreet()[0]);
      assertEquals("H C Andersens Hus", pdcDocument.getPatient().getAddress().getStreet()[0]);
      assertNotNull(pdcDocument.getPatient().getAddress().getStreet()[1]);
      assertEquals("Den gamle bydel", pdcDocument.getPatient().getAddress().getStreet()[1]);
      assertNotNull(pdcDocument.getPatient().getAddress().getStreet()[2]);
      assertEquals("Museum", pdcDocument.getPatient().getAddress().getStreet()[2]);
      assertNotNull(pdcDocument.getPatient().getAddress().getStreet()[3]);
      assertEquals("Hans Jensens Stræde 45", pdcDocument.getPatient().getAddress().getStreet()[3]);
      assertNotNull(pdcDocument.getPatient().getAddress().getPostalCode());
      assertEquals("5000", pdcDocument.getPatient().getAddress().getPostalCode());
      assertNotNull(pdcDocument.getPatient().getAddress().getCity());
      assertEquals("Odense", pdcDocument.getPatient().getAddress().getCity());
      assertNotNull(pdcDocument.getPatient().getAddress().getCountry());
      assertEquals("Danmark", pdcDocument.getPatient().getAddress().getCountry());

      assertNotNull(pdcDocument.getPatient().getGivenNames());
      assertNotNull(pdcDocument.getPatient().getGivenNames()[0]);
      assertEquals("Nancy", pdcDocument.getPatient().getGivenNames()[0]);
      assertNotNull(pdcDocument.getPatient().getGivenNames()[1]);
      assertEquals("Ann", pdcDocument.getPatient().getGivenNames()[1]);
      assertNotNull(pdcDocument.getPatient().getFamilyName());
      assertEquals("Berggren", pdcDocument.getPatient().getFamilyName());
      assertNotNull(pdcDocument.getPatient().getGender());
      assertEquals(Gender.Female, pdcDocument.getPatient().getGender());
      assertNotNull(pdcDocument.getPatient().getBirthDate());
      assertEquals(new GregorianCalendar(1948, 11, 25, 0, 0, 0).getTime(), pdcDocument.getPatient().getBirthDate());

      assertNotNull(pdcDocument.getProviderOrganization());
      assertNotNull(pdcDocument.getProviderOrganization().getId());
      assertNotNull(pdcDocument.getProviderOrganization().getId().getExtension());
      assertEquals("123456", pdcDocument.getProviderOrganization().getId().getExtension());
      assertNotNull(pdcDocument.getProviderOrganization().getId().getAuthorityName());
      assertEquals("Yderregisteret", pdcDocument.getProviderOrganization().getId().getAuthorityName());
      assertNotNull(pdcDocument.getProviderOrganization().getId().getRoot());
      assertEquals("1.2.208.176.1.4", pdcDocument.getProviderOrganization().getId().getRoot());
      assertNotNull(pdcDocument.getProviderOrganization().getOrgName());
      assertEquals("Testyder", pdcDocument.getProviderOrganization().getOrgName());
      assertNotNull(pdcDocument.getProviderOrganization().getTelecomList());
      assertNotNull(pdcDocument.getProviderOrganization().getTelecomList()[0]);
      assertNotNull(pdcDocument.getProviderOrganization().getTelecomList()[0].getAddressUse());
      assertEquals(AddressData.Use.WorkPlace,
          pdcDocument.getProviderOrganization().getTelecomList()[0].getAddressUse());
      assertNotNull(pdcDocument.getProviderOrganization().getTelecomList()[0].getProtocol());
      assertEquals("tel", pdcDocument.getProviderOrganization().getTelecomList()[0].getProtocol());
      assertNotNull(pdcDocument.getProviderOrganization().getTelecomList()[0].getValue());
      assertEquals("+4512345678", pdcDocument.getProviderOrganization().getTelecomList()[0].getValue());
      assertNotNull(pdcDocument.getProviderOrganization().getAddress());
      assertNotNull(pdcDocument.getProviderOrganization().getAddress().getAddressUse());
      assertEquals(AddressData.Use.WorkPlace, pdcDocument.getProviderOrganization().getAddress().getAddressUse());
      assertNotNull(pdcDocument.getProviderOrganization().getAddress().getStreet());
      assertNotNull(pdcDocument.getProviderOrganization().getAddress().getStreet()[0]);
      assertEquals("Ydervej 42", pdcDocument.getProviderOrganization().getAddress().getStreet()[0]);
      assertNotNull(pdcDocument.getProviderOrganization().getAddress().getPostalCode());
      assertEquals("1234", pdcDocument.getProviderOrganization().getAddress().getPostalCode());
      assertNotNull(pdcDocument.getProviderOrganization().getAddress().getCity());
      assertEquals("Yderby", pdcDocument.getProviderOrganization().getAddress().getCity());

      assertNotNull(pdcDocument.getAuthor());
      assertNotNull(pdcDocument.getAuthor().getTime());
      assertEquals(new GregorianCalendar(2021, 0, 1, 0, 0, 1).getTime(), pdcDocument.getAuthor().getTime());
      assertNull(pdcDocument.getAuthor().getId());
      assertNotNull(pdcDocument.getAuthor().getOrganizationIdentity());
      assertNull(pdcDocument.getAuthor().getOrganizationIdentity().getId());
      assertNotNull(pdcDocument.getAuthor().getOrganizationIdentity().getOrgName());
      assertEquals("Sundhedsdatastyrelsen", pdcDocument.getAuthor().getOrganizationIdentity().getOrgName());

      assertNotNull(pdcDocument.getCustodianIdentity());
      assertNull(pdcDocument.getCustodianIdentity().getId());
      assertNotNull(pdcDocument.getCustodianIdentity().getOrgName());
      assertEquals("Sundhedsdatastyrelsen", pdcDocument.getCustodianIdentity().getOrgName());

      assertNotNull(pdcDocument.getServiceStartTime());
      assertEquals(new GregorianCalendar(2021, 0, 1, 0, 0, 1).getTime(), pdcDocument.getServiceStartTime());
      assertNull(pdcDocument.getServiceStopTime());
      assertNotNull(pdcDocument.getCdaProfileAndVersion());
      assertEquals("pdc-v2.0", pdcDocument.getCdaProfileAndVersion());

      // # CONTENT #

      assertNotNull(pdcDocument.getText());
      assertTrue(pdcDocument.getText().contains("<td>Borgerens navn og adresse</td>"));
      assertTrue(pdcDocument.getText().contains("<td>Pårørende, indtastet</td>"));

      int idx;

      // CustodyInformation

      assertNotNull(pdcDocument.getCustodyInformationList());
      assertEquals(5, pdcDocument.getCustodyInformationList().size());

      // CustodyInformation 1

      idx = 0;
      assertId(pdcDocument.getCustodyInformationList().get(idx).getId(), "6f5709c6-3fc2-40b5-a964-43355da64152",
          "1.2.208.184", null);

      assertCode(pdcDocument.getCustodyInformationList().get(idx).getCode(), "ChildCustody", "Forældremyndighed over",
          "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getCustodyInformationList().get(idx).getCpr());
      assertEquals("9999999999", pdcDocument.getCustodyInformationList().get(idx).getCpr());

      assertPersonIdentity(pdcDocument.getCustodyInformationList().get(idx).getPersonIdentity(),
          new PersonBuilder("Knudsen").addGivenName("Peter").addGivenName("Severin").build());

      assertCode(pdcDocument.getCustodyInformationList().get(idx).getRelationCode(), "mor", "Mor", "1.2.208.184.100.2",
          "MedCom Relation Codes");

      assertAuthorRegisterEntered(pdcDocument.getCustodyInformationList().get(idx).getAuthorOfInformation(), "CPR",
          "1.2.208.176.1.2", "CPR");

      // CustodyInformation 2

      idx = 1;
      assertId(pdcDocument.getCustodyInformationList().get(idx).getId(), "77d72a27-b3b0-453f-8b20-2633ab719e1b",
          "1.2.208.184", null);

      assertCode(pdcDocument.getCustodyInformationList().get(idx).getCode(), "ChildCustody", "Forældremyndighed over",
          "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getCustodyInformationList().get(idx).getCpr());
      assertEquals("9999999998", pdcDocument.getCustodyInformationList().get(idx).getCpr());

      assertPersonIdentity(pdcDocument.getCustodyInformationList().get(idx).getPersonIdentity(),
          new PersonBuilder("Larsen").addGivenName("Elsebeth").build());

      assertCode(pdcDocument.getCustodyInformationList().get(idx).getRelationCode(), "mor", "Mor", "1.2.208.184.100.2",
          "MedCom Relation Codes");

      assertAuthorRegisterEntered(pdcDocument.getCustodyInformationList().get(idx).getAuthorOfInformation(), "CPR",
          "1.2.208.176.1.2", "CPR");

      // CustodyInformation 3

      // CustodyInformation 4

      // CustodyInformation 5

      // NameAndAddressInformation

      assertNotNull(pdcDocument.getNameAndAddressInformation());
      assertId(pdcDocument.getNameAndAddressInformation().getId(), "7ce03c5e-f2b8-4db9-b077-6ae8ad4a8fde",
          "1.2.208.184", null);

      assertCode(pdcDocument.getNameAndAddressInformation().getCode(), "CitizenNameAddr", "Borgerens navn og adresse",
          "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData());
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet());
      assertEquals(4, pdcDocument.getNameAndAddressInformation().getAddressData().getStreet().length);
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[0]);
      assertEquals("H C Andersens Hus", pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[0]);
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[1]);
      assertEquals("Den gamle bydel", pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[1]);
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[2]);
      assertEquals("Museum", pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[2]);
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[3]);
      assertEquals("Hans Jensens Stræde 45",
          pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[3]);

      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getCity());
      assertEquals("Odense", pdcDocument.getNameAndAddressInformation().getAddressData().getCity());
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getPostalCode());
      assertEquals("5000", pdcDocument.getNameAndAddressInformation().getAddressData().getPostalCode());
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getCountry());
      assertEquals("Danmark", pdcDocument.getNameAndAddressInformation().getAddressData().getCountry());
      assertEquals(AddressData.Use.HomeAddress,
          pdcDocument.getNameAndAddressInformation().getAddressData().getAddressUse());

      assertPersonIdentity(pdcDocument.getNameAndAddressInformation().getPersonIdentity(),
          new PersonBuilder("Berggren").addGivenName("Nancy").addGivenName("Ann").build());

      assertNull(pdcDocument.getNameAndAddressInformation().getConfidentialId());
      assertFalse(pdcDocument.getNameAndAddressInformation().isAddressConfidential());

      assertAuthorRegisterEntered(pdcDocument.getNameAndAddressInformation().getAuthorOfInformation(), "CPR",
          "1.2.208.176.1.2", "CPR");

      // CoverageGroup

      assertNotNull(pdcDocument.getCoverageGroup());
      assertId(pdcDocument.getCoverageGroup().getId(), "48608f46-f2e4-4220-a768-bf90c01d0e77", "1.2.208.184", null);

      assertCode(pdcDocument.getCoverageGroup().getCode(), "CoverageGroup", "Sygesikringsgruppe", "1.2.208.184.100.1",
          "MedCom Message Codes");

      assertNotNull(pdcDocument.getCoverageGroup().getCoverageGroup());
      assertEquals("1", pdcDocument.getCoverageGroup().getCoverageGroup());

      assertAuthorRegisterEntered(pdcDocument.getCoverageGroup().getAuthorOfInformation(), "Sygesikringen",
          "1.2.208.176.2.7", "Sygesikringen");

      // OrganDonorRegistration

      assertNotNull(pdcDocument.getOrganDonorRegistration());
      assertId(pdcDocument.getOrganDonorRegistration().getId(), "2c4a5b4d-7366-4ff9-83b4-d94fec55a52e", "1.2.208.184",
          null);

      assertCode(pdcDocument.getOrganDonorRegistration().getCode(), "OrganDonorRegistration", "Registreret organdonor",
          "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getOrganDonorRegistration().isRegistered());
      assertEquals(true, pdcDocument.getOrganDonorRegistration().isRegistered());

      assertAuthorRegisterEntered(pdcDocument.getOrganDonorRegistration().getAuthorOfInformation(),
          "Dansk Center For Organdonation", "1.2.208.176.1.10", "Dansk Center For Organdonation");

      // TreatmentWillRegistration

      assertNotNull(pdcDocument.getTreatmentWillRegistration());
      assertId(pdcDocument.getTreatmentWillRegistration().getId(), "417ae1ef-df4c-4c93-b579-6036d99c963e",
          "1.2.208.184", null);

      assertCode(pdcDocument.getTreatmentWillRegistration().getCode(), "TreatmentWillRegistration",
          "Registreret behandlingstestamente", "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getTreatmentWillRegistration().isRegistered());
      assertEquals(true, pdcDocument.getTreatmentWillRegistration().isRegistered());

      assertAuthorRegisterEntered(pdcDocument.getTreatmentWillRegistration().getAuthorOfInformation(),
          "Sundhedsdatastyrelsen", "1.2.208.176.1.9", "Sundhedsdatastyrelsen");

      // LivingWillRegistration

      assertNotNull(pdcDocument.getLivingWillRegistration());
      assertId(pdcDocument.getLivingWillRegistration().getId(), "e951d149-5805-48eb-b1d5-fe42104c8777", "1.2.208.184",
          null);

      assertCode(pdcDocument.getLivingWillRegistration().getCode(), "LivingWillRegistration",
          "Registreret livstestamente", "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getLivingWillRegistration().isRegistered());
      assertEquals(true, pdcDocument.getLivingWillRegistration().isRegistered());

      assertAuthorRegisterEntered(pdcDocument.getLivingWillRegistration().getAuthorOfInformation(),
          "Sundhedsdatastyrelsen", "1.2.208.176.1.8", "Sundhedsdatastyrelsen");

      // ManuallyEnteredSpokenLanguage

      assertNotNull(pdcDocument.getManuallyEnteredSpokenLanguage());
      assertId(pdcDocument.getManuallyEnteredSpokenLanguage().getId(), "1e009601-89c5-49bf-b1ea-53889c9dca09",
          "1.2.208.184", null);

      assertCode(pdcDocument.getManuallyEnteredSpokenLanguage().getCode(), "LanguageTypedIn", "Talt sprog, indtastet",
          "1.2.208.184.100.1", "MedCom Message Codes");

      assertCode(pdcDocument.getManuallyEnteredSpokenLanguage().getLanguageCode(), "de", "Tysk", "1.0.639.1",
          "ISO-639-1");

      assertAuthorManualEntered(pdcDocument.getManuallyEnteredSpokenLanguage().getAuthorOfInformation(), "2512484916",
          new GregorianCalendar(2020, 11, 31, 23, 59, 59).getTime(),
          new PersonBuilder("Berggren").addGivenName("Nancy").addGivenName("Ann").build());

      // ManuallyEnteredTemporaryAddress

      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress());
      assertId(pdcDocument.getManuallyEnteredTemporaryAddress().getId(), "8a46fd63-8854-4f7c-a2ff-f4d95f9b38ba",
          "1.2.208.184", null);

      assertCode(pdcDocument.getManuallyEnteredTemporaryAddress().getCode(), "TempAddrTypedIn",
          "Midlertidig adresse, indtastet", "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getEffectiveFrom());
      assertEquals(new GregorianCalendar(2021, 0, 1, 0, 0, 0).getTime(),
          pdcDocument.getManuallyEnteredTemporaryAddress().getEffectiveFrom());
      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getEffectiveTo());
      assertEquals(new GregorianCalendar(2021, 1, 1, 0, 0, 0).getTime(),
          pdcDocument.getManuallyEnteredTemporaryAddress().getEffectiveTo());
      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData());
      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet());
      assertEquals(4, pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet().length);
      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet()[0]);
      assertEquals("Jernbane museet", pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet()[0]);
      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet()[1]);
      assertEquals("Ved stationen", pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet()[1]);
      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet()[2]);
      assertEquals("Museum", pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet()[2]);
      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet()[3]);
      assertEquals("Dannebrogsgade 24",
          pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet()[3]);
      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getCity());
      assertEquals("Odense", pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getCity());
      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getPostalCode());
      assertEquals("5000", pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getPostalCode());
      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getCountry());
      assertEquals("Danmark", pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getCountry());
      assertEquals(AddressData.Use.HomeAddress,
          pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getAddressUse());

      assertAuthorManualEntered(pdcDocument.getManuallyEnteredTemporaryAddress().getAuthorOfInformation(), "2512484916",
          new GregorianCalendar(2020, 11, 31, 23, 59, 59).getTime(),
          new PersonBuilder("Berggren").addGivenName("Nancy").addGivenName("Ann").build());

      // ManuallyEnteredDentistInformation

      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation());
      assertId(pdcDocument.getManuallyEnteredDentistInformation().getId(), "090ede7b-6f95-4597-bf7d-f4f31f5ebbd0",
          "1.2.208.184", null);

      assertCode(pdcDocument.getManuallyEnteredDentistInformation().getCode(), "DentistTypedIn", "Tandlæge indtastet",
          "1.2.208.184.100.1", "MedCom Message Codes");

      assertId(pdcDocument.getManuallyEnteredDentistInformation().getIdentification(), "654321", "1.2.208.176.1.4",
          "Yderregisteret");

      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation().getAddressData());
      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet());
      assertEquals(4, pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet().length);
      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet()[0]);
      assertEquals("Tandlæge museum",
          pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet()[0]);
      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet()[1]);
      assertEquals("Medicinsk Museum",
          pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet()[1]);
      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet()[2]);
      assertEquals("Bredgade 62", pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet()[2]);
      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet()[3]);
      assertEquals("OBS åbningstider",
          pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet()[3]);

      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getCity());
      assertEquals("København", pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getCity());
      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getPostalCode());
      assertEquals("1260", pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getPostalCode());
      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getCountry());
      assertEquals("Danmark", pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getCountry());
      assertNull(pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getAddressUse());

      assertPersonIdentity(pdcDocument.getManuallyEnteredDentistInformation().getPersonIdentity(),
          new PersonBuilder("Testesen").addGivenName("Jette").setPrefix("Tandlæge").build());

      assertAuthorManualEntered(pdcDocument.getManuallyEnteredDentistInformation().getAuthorOfInformation(),
          "0102632222", new GregorianCalendar(2020, 11, 31, 23, 59, 59).getTime(),
          new PersonBuilder("Knudsen").addGivenName("Bente").addGivenName("Kirkegård").build());
      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation().getTelecomPhone());
      assertEquals(0, pdcDocument.getManuallyEnteredDentistInformation().getTelecomPhone().length);

      // ManuallyEnteredContactInformation

      assertNotNull(pdcDocument.getManuallyEnteredContactInformation());
      assertId(pdcDocument.getManuallyEnteredContactInformation().getId(), "6491b0e3-f9ee-4ab6-afcd-e3524e1e7321",
          "1.2.208.184", null);

      assertCode(pdcDocument.getManuallyEnteredContactInformation().getCode(), "PatientContactTypedIn",
          "Kontaktoplysninger, indtastet", "1.2.208.184.100.1", "MedCom Message Codes");

      TelecomPhone[] telecomExpected = new TelecomPhone[3];
      telecomExpected[0] = new TelecomPhone(TelecomPhone.Use.Home, "tel", "+4511223344");
      telecomExpected[1] = new TelecomPhone(TelecomPhone.Use.Work, "tel", "(0045)33112244-33112255-3010");
      telecomExpected[2] = new TelecomPhone(TelecomPhone.Use.Mobile, "tel", "+4566774433");
      assertTelecom(pdcDocument.getManuallyEnteredContactInformation().getTelecomPhone(), telecomExpected);

      assertAuthorManualEntered(pdcDocument.getManuallyEnteredContactInformation().getAuthorOfInformation(),
          "0108629996", new GregorianCalendar(2020, 11, 31, 23, 59, 58).getTime(),
          new PersonBuilder("Berggren").addGivenName("Maxine").build());

      // ManuallyEnteredInformationAboutRelatives

      assertNotNull(pdcDocument.getManuallyEnteredInformationAboutRelativesList());
      assertEquals(6, pdcDocument.getManuallyEnteredInformationAboutRelativesList().size());

      // ManuallyEnteredInformationAboutRelatives 1

      idx = 0;
      assertId(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getId(),
          "8c474e69-e7b1-43e7-8489-8d951ab5de41", "1.2.208.184", null);

      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getCode(), "RelativeTypedIn",
          "Pårørende, indtastet", "1.2.208.184.100.1", "MedCom Message Codes");

      assertPersonIdentity(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getPersonIdentity(),
          new PersonBuilder("Hansen").addGivenName("Anne").addGivenName("Kathrine").addGivenName("Louise").build());

      telecomExpected[0] = new TelecomPhone(TelecomPhone.Use.Home, "tel", "11223344");
      telecomExpected[1] = new TelecomPhone(TelecomPhone.Use.Work, "tel", "(46)-55667788-1234");
      telecomExpected[2] = new TelecomPhone(TelecomPhone.Use.Mobile, "tel", "99001122");
      assertTelecom(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getTelecomPhone(),
          telecomExpected);

      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getRelationCode(), "nabo",
          "Nabo", "1.2.208.184.100.2", "MedCom Relation Codes");

      assertNotNull(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getNote());
      assertEquals(
          "Naboen arbejder hos TDC i Sverige og kan træffes på\n"
              + "                                arbejdstelefon i dagtimerne ml. 8 og 16.",
          pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getNote());

      assertAuthorManualEntered(
          pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getAuthorOfInformation(), "0107729995",
          new GregorianCalendar(2020, 11, 31, 23, 59, 59).getTime(),
          new PersonBuilder("Berggren").addGivenName("Max").addGivenName("Bo").build());

      // ManuallyEnteredInformationAboutRelatives 2

      idx = 1;
      assertId(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getId(),
          "fe14dce2-1ab7-4b3d-ab58-7d0f14b49818", "1.2.208.184", null);

      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getCode(), "RelativeTypedIn",
          "Pårørende, indtastet", "1.2.208.184.100.1", "MedCom Message Codes");

      assertPersonIdentity(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getPersonIdentity(),
          new PersonBuilder("Larsen").addGivenName("Tim").build());

      telecomExpected[0] = new TelecomPhone(TelecomPhone.Use.Home, "tel", "44232332");
      telecomExpected[1] = new TelecomPhone(TelecomPhone.Use.Work, "tel", "(46)-44667788-1234");
      telecomExpected[2] = new TelecomPhone(TelecomPhone.Use.Mobile, "tel", "69001222");
      assertTelecom(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getTelecomPhone(),
          telecomExpected);

      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getRelationCode(), "søskende",
          "Søskende", "1.2.208.184.100.2", "MedCom Relation Codes");

      assertNotNull(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getNote());
      assertEquals("Er svagt hørende, tal tydeligt!",
          pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getNote());

      assertAuthorManualEntered(
          pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getAuthorOfInformation(), "2512484916",
          new GregorianCalendar(2020, 11, 31, 23, 59, 59).getTime(),
          new PersonBuilder("Berggren").addGivenName("Nancy").addGivenName("Ann").build());

      // ManuallyEnteredInformationAboutRelatives 3

      idx = 2;
      assertId(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getId(),
          "f33ac06d-6ccd-4be3-ac0b-ff2366c90093", "1.2.208.184", null);

      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getCode(), "RelativeTypedIn",
          "Pårørende, indtastet", "1.2.208.184.100.1", "MedCom Message Codes");

      assertPersonIdentity(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getPersonIdentity(),
          new PersonBuilder("Hansen").addGivenName("Buller").build());

      telecomExpected[0] = new TelecomPhone(TelecomPhone.Use.Home, "tel", "22255448");
      telecomExpected[1] = new TelecomPhone(TelecomPhone.Use.Work, "tel", "(46)-22667788-1234");
      telecomExpected[2] = new TelecomPhone(TelecomPhone.Use.Mobile, "tel", "31001199");
      assertTelecom(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getTelecomPhone(),
          telecomExpected);

      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getRelationCode(),
          "uspec_paaroerende", "Uspecificeret pårørende", "1.2.208.184.100.2", "MedCom Relation Codes");

      assertNotNull(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getNote());
      assertEquals("Er en god ven af familien",
          pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getNote());

      assertAuthorManualEntered(
          pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getAuthorOfInformation(), "2512484916",
          new GregorianCalendar(2020, 11, 31, 23, 59, 59).getTime(),
          new PersonBuilder("Berggren").addGivenName("Nancy").addGivenName("Ann").build());

      // ManuallyEnteredInformationAboutRelatives 4

      idx = 3;
      assertId(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getId(),
          "43a8fa69-9190-452b-b115-f7e890dde887", "1.2.208.184", null);

      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getCode(), "RelativeTypedIn",
          "Pårørende, indtastet", "1.2.208.184.100.1", "MedCom Message Codes");

      assertPersonIdentity(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getPersonIdentity(),
          new PersonBuilder("RelaiveFammilyNameTypedIn").addGivenName("RelativeGivenNameTypedIn").build());

      telecomExpected[0] = new TelecomPhone(TelecomPhone.Use.Home, "tel", "11223344");
      telecomExpected[1] = new TelecomPhone(TelecomPhone.Use.Work, "tel", "(46)-55667788-1234");
      telecomExpected[2] = new TelecomPhone(TelecomPhone.Use.Mobile, "tel", "99001122");
      assertTelecom(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getTelecomPhone(),
          telecomExpected);

      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getRelationCode(),
          "ingen_relationer", "Ingen relationer", "1.2.208.184.100.2", "MedCom Relation Codes");

      assertNotNull(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getNote());
      assertTrue(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getNote().contains(
          "Praesent sagittis, erat vel auctor pretium,"));

      assertAuthorManualEntered(
          pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getAuthorOfInformation(), "2512484916",
          new GregorianCalendar(2020, 11, 31, 23, 59, 59).getTime(),
          new PersonBuilder("Knudsen").addGivenName("Bente").addGivenName("Kirkegård").build());

      // ManuallyEnteredInformationAboutRelatives 5

      // ManuallyEnteredInformationAboutRelatives 6

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testPdcXMLConverterMedcomExampleConfidentialAddress() {
    try {

      String XML = FileUtil.getData(this.getClass(),
          "pdc/PDC20_Example-5_2020-04-15_with_narrative_text_and_confidential_address_2.xml");

      PDCDocument pdcDocument = decode(codec, XML);

      assertNotNull(pdcDocument.getNameAndAddressInformation());
      assertId(pdcDocument.getNameAndAddressInformation().getId(), "2d537144-57f9-46a2-b323-c4bd6f6c1a60",
          "1.2.208.184", null);

      assertCode(pdcDocument.getNameAndAddressInformation().getCode(), "CitizenNameAddr", "Borgerens navn og adresse",
          "1.2.208.184.100.1", "MedCom Message Codes");

      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData());
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet());
      assertEquals(1, pdcDocument.getNameAndAddressInformation().getAddressData().getStreet().length);
      assertNotNull(pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[0]);
      assertEquals("Adressebeskyttelse", pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[0]);

      assertNull(pdcDocument.getNameAndAddressInformation().getAddressData().getCity());
      assertNull(pdcDocument.getNameAndAddressInformation().getAddressData().getPostalCode());
      assertNull(pdcDocument.getNameAndAddressInformation().getAddressData().getCountry());
      assertEquals(AddressData.Use.HomeAddress,
          pdcDocument.getNameAndAddressInformation().getAddressData().getAddressUse());

      assertPersonIdentity(pdcDocument.getNameAndAddressInformation().getPersonIdentity(),
          new PersonBuilder("Knudsen").addGivenName("Bente").addGivenName("Kirkegård").build());

      assertId(pdcDocument.getNameAndAddressInformation().getConfidentialId(), "ConfAddr", "1.2.208.184.100.1",
          "MedCom");
      assertTrue(pdcDocument.getNameAndAddressInformation().isAddressConfidential());

      assertAuthorRegisterEntered(pdcDocument.getNameAndAddressInformation().getAuthorOfInformation(), "CPR",
          "1.2.208.176.1.2", "CPR");

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testPdcXMLConverterMedcomExampleTemporaryAddressNoHighEffectiveTime() {
    try {

      String XML = FileUtil.getData(this.getClass(),
          "pdc/PDC20_Example-5_2020-04-15_with_narrative_text_and_confidential_address_2.xml");

      PDCDocument pdcDocument = decode(codec, XML);

      assertNotNull(pdcDocument.getManuallyEnteredTemporaryAddress().getEffectiveFrom());
      assertEquals(new GregorianCalendar(2019, 0, 1, 0, 0, 0).getTime(),
          pdcDocument.getManuallyEnteredTemporaryAddress().getEffectiveFrom());
      assertNull(pdcDocument.getManuallyEnteredTemporaryAddress().getEffectiveTo());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testPdcXMLConverterMedcomExampleValueWithTel() {
    try {
      String XML = FileUtil.getData(this.getClass(), "pdc/PDC20_Example-maximum_data_adj.xml");

      PDCDocument pdcDocument = decode(codec, XML);

      assertNotNull(pdcDocument);
      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation());
      assertNotNull(pdcDocument.getManuallyEnteredDentistInformation().getTelecomPhone());
      assertEquals(2, pdcDocument.getManuallyEnteredDentistInformation().getTelecomPhone().length);
      assertEquals("12345678", pdcDocument.getManuallyEnteredDentistInformation().getTelecomPhone()[0].getValue());
      assertEquals("87654321", pdcDocument.getManuallyEnteredDentistInformation().getTelecomPhone()[1].getValue());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  private void assertId(ID id, String extensionExpected, String rootExpected, String authorityNameExpected) {
    assertNotNull(id);
    assertNotNull(id.getExtension());
    assertEquals(extensionExpected, id.getExtension());
    assertNotNull(id.getRoot());
    assertEquals(rootExpected, id.getRoot());
    assertEquals(authorityNameExpected, id.getAuthorityName());

  }

  private void assertCode(CodedValue code, String codeExpected, String displayNameExpected, String codeSystemExpected,
      String codeSystemNameExpected) {
    assertNotNull(code);
    assertNotNull(code.getCode());
    assertEquals(codeExpected, code.getCode());
    assertNotNull(code.getDisplayName());
    assertEquals(displayNameExpected, code.getDisplayName());
    assertNotNull(code.getCodeSystem());
    assertEquals(codeSystemExpected, code.getCodeSystem());
    assertNotNull(code.getCodeSystemName());
    assertEquals(codeSystemNameExpected, code.getCodeSystemName());
  }

  private void assertAuthorManualEntered(AuthorOfInformation authorOfInformation, String idExtensionExpected,
      Date dateExpected, PersonIdentity personExpected) {

    assertNotNull(authorOfInformation);
    assertNotNull(authorOfInformation.getSourceOfInformation());
    assertEquals(SourceOfInformation.ManuallyEntered, authorOfInformation.getSourceOfInformation());

    assertNotNull(authorOfInformation.getTime());
    assertEquals(dateExpected, authorOfInformation.getTime());

    assertNotNull(authorOfInformation.getId());
    assertNotNull(authorOfInformation.getId().getExtension());
    assertEquals(idExtensionExpected, authorOfInformation.getId().getExtension());
    assertNotNull(authorOfInformation.getId().getRoot());
    assertEquals("1.2.208.176.1.2", authorOfInformation.getId().getRoot());
    assertNotNull(authorOfInformation.getId().getAuthorityName());
    assertEquals("CPR", authorOfInformation.getId().getAuthorityName());

    assertNull(authorOfInformation.getRepresentedOrganizationName());

    assertPersonIdentity(authorOfInformation.getAssignedPerson(), personExpected);

  }

  private void assertAuthorRegisterEntered(AuthorOfInformation authorOfInformation,
      String representedOrganizationNameExpected, String rootExpected, String assigningAuthorityNameExpected) {

    assertNotNull(authorOfInformation);
    assertNotNull(authorOfInformation.getSourceOfInformation());
    assertEquals(SourceOfInformation.FromRegister, authorOfInformation.getSourceOfInformation());

    assertNull(authorOfInformation.getTime());

    assertNotNull(authorOfInformation.getId());
    assertNull(authorOfInformation.getId().getExtension());
    assertNotNull(authorOfInformation.getId().getRoot());
    assertEquals(rootExpected, authorOfInformation.getId().getRoot());
    assertNotNull(authorOfInformation.getId().getAuthorityName());
    assertEquals(assigningAuthorityNameExpected, authorOfInformation.getId().getAuthorityName());

    assertNotNull(authorOfInformation.getRepresentedOrganizationName());
    assertEquals(representedOrganizationNameExpected, authorOfInformation.getRepresentedOrganizationName());

    assertNull(authorOfInformation.getAssignedPerson());

  }

  private void assertPersonIdentity(PersonIdentity personIdentity, PersonIdentity personIdentityExpected) {
    assertNotNull(personIdentity);
    assertNotNull(personIdentity.getGivenNames());
    assertEquals(personIdentityExpected.getGivenNames().length, personIdentity.getGivenNames().length);
    assertNotNull(personIdentity.getGivenNames()[0]);
    assertEquals(personIdentityExpected.getGivenNames()[0], personIdentity.getGivenNames()[0]);
    if (personIdentityExpected.getGivenNames().length > 1) {
      assertNotNull(personIdentityExpected.getGivenNames()[1]);
      assertEquals(personIdentityExpected.getGivenNames()[1], personIdentityExpected.getGivenNames()[1]);
    }
    if (personIdentityExpected.getGivenNames().length > 2) {
      assertNotNull(personIdentityExpected.getGivenNames()[2]);
      assertEquals(personIdentityExpected.getGivenNames()[2], personIdentityExpected.getGivenNames()[2]);
    }

    assertNotNull(personIdentity.getFamilyName());
    assertEquals(personIdentityExpected.getFamilyName(), personIdentity.getFamilyName());

    assertEquals(personIdentityExpected.getPrefix(), personIdentity.getPrefix());

  }

  private void assertTelecom(TelecomPhone[] telecoms, TelecomPhone[] telecomExpected) {
    assertNotNull(telecoms);
    assertEquals(telecomExpected.length, telecoms.length);
    for (int i = 0; i < telecomExpected.length; i++) {
      assertNotNull(telecoms[i]);
      assertNotNull(telecoms[i].getUse());
      assertEquals(telecomExpected[i].getUse(), telecoms[i].getUse());
      assertNotNull(telecoms[i].getProtocol());
      assertEquals(telecomExpected[i].getProtocol(), telecoms[i].getProtocol());
      assertNotNull(telecoms[i].getValue());
      assertEquals(telecomExpected[i].getValue(), telecoms[i].getValue());
    }

  }

  @Override
  public void runTest() throws Exception {
    testPdcXMLConverterMedcomExampleMinimum();
  }

}
