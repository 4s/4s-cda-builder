package dk.s4.hl7.cda.examples.medcom;

import java.util.Date;
import java.util.UUID;

import org.junit.Test;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qfdd.QFDDCriterion.QFDDCriterionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDMultipleChoiceQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer.QFDDOrganizerBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition.QFDDPreconditionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public class QFDDLungInformationNeedsQuestionaire {
  @Test
  public void createQfdd() {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as author
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a QFDD document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID id = new ID.IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension("5f971389-13cd-42b6-bef1-fcd46543e93a")
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    QFDDDocument document = new QFDDDocument(id);
    document.setTitle("Spørgeskema til patienter med kronisk lungesygdom");

    // 1.1 Populate with time and version info
    document.setEffectiveTime(documentCreationTime);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    document.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    document.setAuthor(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Section<QFDDOrganizer> qfddSection = new Section<QFDDOrganizer>("Spørgeskema", "Spørgeskema");

    QFDDOrganizerBuilder qfddOrganizerBuilder = new QFDDOrganizerBuilder();
    qfddOrganizerBuilder
        .addQFDDQuestion(createMultipleChoiceQuestion("1", "Kender du navnet på din lungesygdom?", "ja", "nej"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("2",
        "Har en læge eller sygeplejerske fortalt dig, hvordan denne sygdom påvirker dine lunger?", "ja", "nej"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("3",
        "Har en læge eller sygeplejerske fortalt dig, hvad der sandsynligvis vil ske i fremtiden?", "ja", "nej"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("4",
        "Hvilke af de følgende udsagn beskriver bedst, hvad der vil ske med dig i løbet af de nærmeste år?",
        "Nu, da min sygdom bliver behandlet, får jeg det nok bedre",
        "Nu, da min sygdom bliver behandlet, bliver jeg nok ved med at have det, som jeg har det nu",
        "Jeg får det dårligere", "Jeg ved det ikke"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("5",
        "Har en læge eller sygeplejerske forklaret dig grunden til, at du skal bruge inhalator eller tage medicin?",
        "ja", "nej"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("6",
        "Bestræber du dig på at bruge din inhalator eller tage din medicin, nøjagtigt som en læge eller sygeplejerske har lært dig?",
        "ja", "nej"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("7",
        "Er du tilfreds med den information, læger og sygeplejersker har givet dig om dine inhalatorer og din medicin?",
        "Jeg føler mig velinformeret om det, jeg skal vide",
        "Jeg forstår alt, hvad jeg har fået at vide, men ville gerne vide mere", "Jeg er lidt usikker mht. min medicin",
        "Jeg er meget usikker mht. min medicin"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("8",
        "Hvilket udsagn beskriver bedst, hvad du har fået at vide, at du skal gøre, hvis din vejrtrækning bliver mere besværet (fx tage to pust i stedet for et)?",
        "Jeg har fået at vide, hvad jeg skal gøre, og lægen/sygeplejersken har givet mig en skriftlig vejledning",
        "Jeg har fået det at vide, men det er ikke skrevet ned",
        "Jeg har ikke fået det at vide, men jeg ved, hvad jeg skal gøre",
        "Jeg har ikke fået det at vide, og jeg ved ikke, hvad jeg skal gøre"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("9",
        "Har du fået at vide, hvornår du skal ringe efter en ambulance, hvis din vejrtrækning bliver mere besværet?",
        "Jeg har fået at vide, hvad jeg skal gøre, og lægen/sygeplejersken har givet mig en skriftlig vejledning",
        "Jeg har fået det at vide, men det er ikke skrevet ned",
        "Jeg har ikke fået det at vide, men jeg ved, hvad jeg skal gøre",
        "Jeg har ikke fået det at vide, og jeg er usikker på, hvornår jeg skal ringe efter en ambulance"));
    qfddOrganizerBuilder.addQFDDQuestion(
        createMultipleChoiceQuestion("10", "Hvilket udsagn beskriver dig bedst?", "Aldrig røget (gå til spørgsmål 13)",
            "Tidligere røget, men gør det ikke nu (gå til spørgsmål 13)", "Ryger stadig (gå til spørgsmål 11)"));
    QFDDMultipleChoiceQuestion question = createMultipleChoiceQuestion("11",
        "Har en læge eller sygeplejerske rådet dig til at holde op med at ryge?", "ja", "nej");
    question.addPrecondition(createPrecondition("10", "A3", "Ryger stadig (gå til spørgsmål 11)"));
    qfddOrganizerBuilder.addQFDDQuestion(question);
    question = createMultipleChoiceQuestion("12",
        "Har en læge eller sygeplejerske tilbudt dig hjælp til at holde op med at ryge? (fx givet dig nikotintyggegummi, nikotinplaster eller en henvisning til en rygestopklinik)",
        "ja", "nej");
    question.addPrecondition(createPrecondition("10", "A3", "Ryger stadig (gå til spørgsmål 11)"));
    qfddOrganizerBuilder.addQFDDQuestion(question);
    question = createMultipleChoiceQuestion("13",
        "Har en læge eller sygeplejerske fortalt dig, at du skal forsøge at dyrke motion (fx almindelig gang, rask gang og andre former for motion)?",
        "ja", "nej");
    question.addPrecondition(createPrecondition("10", "A1", "Aldrig røget (gå til spørgsmål 13)"));
    question
        .addPrecondition(createPrecondition("10", "A2", "Tidligere røget, men gør det ikke nu (gå til spørgsmål 13)"));
    qfddOrganizerBuilder.addQFDDQuestion(question);
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("14",
        "Har en læge eller sygeplejerske fortalt dig, hvor meget motion (fx almindelig gang, rask gang og andre former for motion), du skal dyrke?",
        "Ja, og jeg ved, hvad jeg skal gøre", "Ja, men jeg er usikker på, hvad jeg skal gøre",
        "Ja, men jeg er ikke i stand til at gøre det", "Nej"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("15", "Hvor meget motion dyrker du?",
        "Så lidt som muligt", "Jeg gør en indsats", "Jeg anstrenger mig, så meget jeg kan"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("16",
        "Hvad har læger eller sygeplejersker fortalt dig om dine kost- og spisevaner? (Sæt kryds for hvert svar der passer for dig)",
        4, "Ingenting", "Tab dig eller tag på", "Spis sundt",
        "Spise mange små måltider dagligt (fx 6 små måltider i stedet for 3 store)"));
    qfddOrganizerBuilder.addQFDDQuestion(createTextQuestion("17",
        "Har du spørgsmål eller kommentarer ang. din lungesygdom? Hvis du har så skriv dem herunder:"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("18", "Bor du alene?", "ja", "nej"));
    qfddOrganizerBuilder.addQFDDQuestion(createMultipleChoiceQuestion("19", "Hvad er dit køn?", "mand", "kvinde"));
    qfddOrganizerBuilder.addQFDDQuestion(createTextQuestion("20", "Hvilket år er du født?"));

    qfddSection.addOrganizer(qfddOrganizerBuilder.build());
    document.addSection(qfddSection);
  }

  private QFDDQuestion createTextQuestion(String questionCode, String question) {
    CodedValue codeValue = new CodedValue(questionCode, MedCom.MEDCOM_PROMPT_OID, "", MedCom.MEDCOM_PROMPT_TABLE);
    ID id = new ID.IDBuilder()
        .setRoot(MedCom.MEDCOM_PROMPT_OID)
        .setExtension(uuid())
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    return new QFDDTextQuestion.QFDDTextQuestionBuilder()
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion(question)
        .build();
  }

  private QFDDMultipleChoiceQuestion createMultipleChoiceQuestion(String questionCode, String question, int maximum,
      String... options) {
    CodedValue codeValue = new CodedValue(questionCode, MedCom.MEDCOM_PROMPT_OID, "", MedCom.MEDCOM_PROMPT_TABLE);
    ID id = new ID.IDBuilder()
        .setRoot(MedCom.MEDCOM_PROMPT_OID)
        .setExtension(uuid())
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    QFDDMultipleChoiceQuestion qfddMultipleChoiceQuestion = new QFDDMultipleChoiceQuestion.QFDDMultipleChoiceQuestionBuilder()
        .setInterval(1, maximum)
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion(question)
        .build();

    for (int index = 0; index < options.length; index++) {
      qfddMultipleChoiceQuestion.addAnswerOption("A" + (index + 1), MedCom.MEDCOM_PROMPT_OID, options[index],
          MedCom.MEDCOM_PROMPT_TABLE);
    }
    return qfddMultipleChoiceQuestion;
  }

  private QFDDMultipleChoiceQuestion createMultipleChoiceQuestion(String questionCode, String question,
      String... options) {
    return createMultipleChoiceQuestion(questionCode, question, 1, options);
  }

  private QFDDPrecondition createPrecondition(String questionCode, String answerId, String answer) {
    CodedValue codeValue = new CodedValue(questionCode, MedCom.MEDCOM_PROMPT_OID, MedCom.MEDCOM_PROMPT_TABLE);
    return new QFDDPreconditionBuilder(
        new QFDDCriterionBuilder(codeValue).setAnswer(new CodedValue(answerId, answer)).build()).build();
  }

  private String uuid() {
    return UUID.randomUUID().toString();
  }

}
