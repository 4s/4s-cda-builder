package dk.s4.hl7.cda.convert.decode;

import dk.s4.hl7.cda.model.Patient;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static dk.s4.hl7.cda.model.Patient.PatientBuilder;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Every.everyItem;
import static org.junit.Assert.assertThat;

/**
 * Test for the XML conversion utility {@link ConvertXmlUtil}
 */
public class ConvertXmlUtilTest {

  // Values used for the Date in assertions
  private static final int BIRTHDAY_YEAR = 2019;
  private static final int BIRTHDAY_MONTH = 1;
  private static final int BIRTHDAY_DAY = 31;
  private static final int BIRTHDAY_HOUR = 16;
  private static final int BIRTHDAY_MINUTE = 14;
  private static final int BIRTHDAY_SECOND = 13;

  /**
   * Test the method setting a birthday in the {@link PatientBuilder} using a format containing a valid date.
   */
  @Test
  public void setPersonBirthdayWithValidDate() {
    // Given
    PatientBuilder patientBuilder = new PatientBuilder();
    ConvertXmlUtil.setPersonBirthday("20190131000000+0000", patientBuilder);

    // When
    Patient patient = patientBuilder.build();

    // Then
    assertThat(patient.getBirthTime().getTime(), is(generateTestDateOfBirth().getTime()));
  }

  /**
   * Test the method setting a birthday in the {@link PatientBuilder} using a invalid format for date.
   */
  @Test
  public void setPersonBirthdayWithInvalidDate() {
    // Given
    PatientBuilder patientBuilder = new PatientBuilder();
    ConvertXmlUtil.setPersonBirthday("2019-01-31-000000+0000", patientBuilder);

    // When
    Patient patient = patientBuilder.build();

    // Then
    assertThat(patient.getBirthTime(), is(nullValue()));
  }

  /**
   * Test the parsing of strings containing a valid date of birth.
   * Only values for year, month and day from the date of birth string should be evaluated.
   */
  @Test
  public void getPersonBirthdayWithValidDates() {
    // When
    List<Date> birthDays = new ArrayList<Date>();
    birthDays.add(ConvertXmlUtil.getPersonBirthday("20190131000000+0000"));
    birthDays.add(ConvertXmlUtil.getPersonBirthday("20190131000000+0100"));
    birthDays.add(ConvertXmlUtil.getPersonBirthday("20190131000000-02:00"));
    birthDays.add(ConvertXmlUtil.getPersonBirthday("2019013100:00:00Z"));
    birthDays.add(ConvertXmlUtil.getPersonBirthday("20190131"));

    // Then
    assertThat(birthDays, everyItem(is(generateTestDateOfBirth())));
  }

  /**
   * Test the parsing of strings containing an invalid value as date of birth.
   * Only values for year, month and day from the date of birth string should be evaluated.
   */
  @Test
  public void getPersonBirthdayWithInvalidDates() {
    // When
    List<Date> birthDays = new ArrayList<Date>();
    birthDays.add(ConvertXmlUtil.getPersonBirthday("NOT_A_DATE"));
    birthDays.add(ConvertXmlUtil.getPersonBirthday("2019-01-31"));
    birthDays.add(ConvertXmlUtil.getPersonBirthday("2019-01-31T00:00:00+01:00"));

    // Then
    assertThat(birthDays, everyItem(nullValue(Date.class)));
  }

  /**
   * Test the parsing of strings containing a valid date of birth.
   */
  @Test
  public void getPersonBirthdayWithoutTimeWithValidDates() {
    // When
    List<Date> birthDays = new ArrayList<Date>();
    birthDays.add(ConvertXmlUtil.getPersonBirthdayExcludingTime("20190131"));

    // Then
    assertThat(birthDays, everyItem(is(generateTestDateOfBirth())));
  }

  /**
   * Test the parsing of strings containing an invalid value as date of birth.
   */
  @Test
  public void getPersonBirthdayWithoutTimeWithInvalidDates() {
    // When
    List<Date> birthDays = new ArrayList<Date>();
    birthDays.add(ConvertXmlUtil.getPersonBirthdayExcludingTime("NOT_A_DATE"));
    birthDays.add(ConvertXmlUtil.getPersonBirthdayExcludingTime("2019-01-31T00:00:00+01:00"));
    birthDays.add(ConvertXmlUtil.getPersonBirthdayExcludingTime("20190131000000+0000"));
    birthDays.add(ConvertXmlUtil.getPersonBirthdayExcludingTime("20190131000000+0100"));
    birthDays.add(ConvertXmlUtil.getPersonBirthdayExcludingTime("20190131000000-02:00"));
    birthDays.add(ConvertXmlUtil.getPersonBirthdayExcludingTime("2019013100:00:00Z"));

    // Then
    assertThat(birthDays, everyItem(nullValue(Date.class)));
  }

  /**
   * Test the parsing of strings containing a valid date of birth.
   */
  @Test
  public void getPersonBirthdayWithValidDatesAndTime() {
    // When
    List<Date> birthDays = new ArrayList<Date>();
    birthDays.add(ConvertXmlUtil.getPersonBirthdayIncludingTime("20190131151413+0000"));
    birthDays.add(ConvertXmlUtil.getPersonBirthdayIncludingTime("20190131161413+0100"));
    birthDays.add(ConvertXmlUtil.getPersonBirthdayIncludingTime("20190131131413-0200"));

    // Then
    assertThat(birthDays, everyItem(is(generateTestDateAndTimeOfBirth())));
  }

  /**
   * Test the parsing of strings containing an invalid value as date of birth.
   */
  @Test
  public void getPersonBirthdayWithInvalidDatesAndTime() {
    // When
    List<Date> birthDays = new ArrayList<Date>();
    birthDays.add(ConvertXmlUtil.getPersonBirthdayIncludingTime("NOT_A_DATE"));
    birthDays.add(ConvertXmlUtil.getPersonBirthdayIncludingTime("2019-01-31"));
    birthDays.add(ConvertXmlUtil.getPersonBirthdayIncludingTime("2019-01-31T00:00:00+01:00"));

    // Then
    assertThat(birthDays, everyItem(nullValue(Date.class)));
  }

  private Date generateTestDateOfBirth() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.YEAR, BIRTHDAY_YEAR);
    calendar.set(Calendar.MONTH, BIRTHDAY_MONTH - 1);
    calendar.set(Calendar.DAY_OF_MONTH, BIRTHDAY_DAY);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  private Date generateTestDateAndTimeOfBirth() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.YEAR, BIRTHDAY_YEAR);
    calendar.set(Calendar.MONTH, BIRTHDAY_MONTH - 1);
    calendar.set(Calendar.DAY_OF_MONTH, BIRTHDAY_DAY);
    calendar.set(Calendar.HOUR_OF_DAY, BIRTHDAY_HOUR);
    calendar.set(Calendar.MINUTE, BIRTHDAY_MINUTE);
    calendar.set(Calendar.SECOND, BIRTHDAY_SECOND);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }
}