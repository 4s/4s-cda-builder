package dk.s4.hl7.cda.convert.decode;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.convert.APDXmlCodec;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public class TestXmlToApdToXmlCompare extends BaseDecodeTest {

  private APDXmlCodec codec = new APDXmlCodec();

  @Before
  public void before() {
    codec = new APDXmlCodec();
  }

  @Test
  public void testXmlToApdToXml_Example_1() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "apd/DK-APD_Example_1.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToApdToXml_Example_v11_default() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "apd/DK-APD_V11_Eksempel_default.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }
}
