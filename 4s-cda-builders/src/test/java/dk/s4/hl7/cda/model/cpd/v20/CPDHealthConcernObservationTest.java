package dk.s4.hl7.cda.model.cpd.v20;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthConcernObservation.CPDHealthConcernObservationBuilder;
import dk.s4.hl7.cda.model.util.DateUtil;

public class CPDHealthConcernObservationTest {

  CPDHealthConcernObservationBuilder subject;

  @Before
  public void setUpValidBuilder() throws Exception {

    subject = new CPDHealthConcernObservationBuilder()
        .setId(new IDBuilder().setRoot("1.2.208.184").setExtension("b1b0b657-1121-4607-995d-f20b549d767f").build())
        .setCode(
            new CodedValue("ALGA01", "1.2.208.176.2.4", "aktionsdiagnose", "Sundhedsvæsenets Klassifikations System"))
        .setEffectiveDate(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 0, 0, 0))
        .setValueCode(new CodedValue("T90", "1.2.208.176.99.99.99", "Diabetes", "ICPC-2-DK"));

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingId() {

    //given
    subject.setId(null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingCode() {

    //given
    subject.setCode(null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test
  public void shouldBuild_EffectiveDate() {

    //given

    //when
    CPDHealthConcernObservation result = subject.build();

    //then
    assertNotNull(result);
    assertNotNull(result.getEffectiveTime());
    assertFalse(result.effectiveTimeHasTime());
    assertNull(result.getEffectiveTimeInterval());
  }

  @Test
  public void shouldBuild_EffectTime() {

    //given
    subject.setEffectiveTimeInterval(null).setEffectiveTime(
        DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 12, 40, 0));

    //when
    CPDHealthConcernObservation result = subject.build();

    //then
    assertNotNull(result);
    assertNotNull(result.getEffectiveTime());
    assertTrue(result.effectiveTimeHasTime());
    assertNull(result.getEffectiveTimeInterval());
  }

  @Test
  public void shouldBuild_EffectTimeInterval() {

    //given
    subject
        .setEffectiveTimeInterval(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 11, 21, 35), null)
        .setEffectiveDate(null);

    //when
    CPDHealthConcernObservation result = subject.build();

    //then
    assertNotNull(result);
    assertNull(result.getEffectiveTime());
    assertFalse(result.effectiveTimeHasTime());
    assertNotNull(result.getEffectiveTimeInterval());
    assertNotNull(result.getEffectiveTimeInterval().getStartTime());
    assertTrue(result.getEffectiveTimeInterval().startTimeHasTime());
    assertNull(result.getEffectiveTimeInterval().getStopTime());
    assertTrue(result.getEffectiveTimeInterval().stopTimeHasTime());
  }

  @Test
  public void shouldBuild_EffectDateInterval() {

    //given
    subject
        .setEffectiveDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 0, 0, 0), null)
        .setEffectiveDate(null);

    //when
    CPDHealthConcernObservation result = subject.build();

    //then
    assertNotNull(result);
    assertNull(result.getEffectiveTime());
    assertFalse(result.effectiveTimeHasTime());
    assertNotNull(result.getEffectiveTimeInterval());
    assertNotNull(result.getEffectiveTimeInterval().getStartTime());
    assertFalse(result.getEffectiveTimeInterval().startTimeHasTime());
    assertNull(result.getEffectiveTimeInterval().getStopTime());
    assertFalse(result.getEffectiveTimeInterval().stopTimeHasTime());
  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingEffectiveTimes() {

    //given
    Date dateNull = null;
    subject.setEffectiveTime(dateNull).setEffectiveTimeInterval(null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingEffectiveDates() {

    //given
    Date dateNull = null;
    subject.setEffectiveDate(dateNull).setEffectiveTimeInterval(null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingEffectiveTimesInInterval() {

    //given
    subject.setEffectiveTimeInterval(null, null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingEffectiveDatesInInterval() {

    //given
    subject.setEffectiveDateInterval(null, null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingValueCode() {

    //given
    subject.setValueCode(null);

    //when
    subject.build();

    //then
    //excpetion

  }

}
