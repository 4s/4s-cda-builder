package dk.s4.hl7.cda.convert.decode;

import dk.s4.hl7.cda.convert.PHMRV20XmlCodec;
import dk.s4.hl7.cda.model.phmr.v20.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URISyntaxException;

public class TestXmlToPhmrV20ToXmlCompare extends BaseDecodeTest {

  private PHMRV20XmlCodec codec = new PHMRV20XmlCodec();

  @Before
  public void before() {
    codec = new PHMRV20XmlCodec();
  }

  @Test
  public void testXmlToPhmrToXml_Example_PHMR_example_0101_27022025()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "phmr/v20/PHMR-example-01.01-27022025v2.xml");

    PHMRDocument phmr = codec.decode(xmlOrig);
    String xmlNew = codec.encode(phmr);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToPhmrToXml_Example_PHMR_example_createdInTestOfMobileContact()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "phmr/v20/PHMR-example-created-in-test-of-mobilecontact.xml");

    PHMRDocument phmr = codec.decode(xmlOrig);
    String xmlNew = codec.encode(phmr);
    compare(xmlOrig, xmlNew);
  }

}
