package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.CPDV20XmlCodec;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.encode.cpd.v20.SetupMedcomExample1;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusActEntry;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public class TestXmlToCpdV20 extends BaseDecodeTest implements ConcurrencyTestCase {

  private CPDV20XmlCodec codec = new CPDV20XmlCodec();

  @Before
  public void before() {
    setCodec(new CPDV20XmlCodec());
  }

  public void setCodec(CPDV20XmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testCpdXMLConverterSampleDifferentLevels() {
    try {
      String cpdAsXML = FileUtil.getData(this.getClass(), "cpd/CPD-DK_Care_Plan_example1.xml");

      CPDDocument cpd = decode(codec, cpdAsXML);

      assertNotNull(cpd);
      assertNotNull(cpd.getRealmCode());
      assertEquals("DK", cpd.getRealmCode());
      assertNotNull(cpd.getTypeIdExtension());

      // # HEALTH #
      assertNotNull(cpd.getHealthConcernSection());

      assertNotNull(cpd.getHealthConcernSection().getComponent());
      assertEquals(2, cpd.getHealthConcernSection().getComponent().getEntryList().size());

      CPDHealthStatusActEntry cpdHealthStatusActEntry = cpd
          .getHealthConcernSection()
          .getComponent()
          .getEntryList()
          .get(0);
      assertEquals("7b889ee9-2877-4824-94c2-b1dbe30324e8", cpdHealthStatusActEntry.getId().getExtension());
      assertEquals(false, cpdHealthStatusActEntry.getEffectiveTimeCdaDate().dateHasTime());

      assertEquals(2, cpdHealthStatusActEntry.getAuthorList().size());
      assertEquals("Sørensen", cpdHealthStatusActEntry.getAuthorList().get(0).getPersonIdentity().getFamilyName());
      assertEquals("Pedersen", cpdHealthStatusActEntry.getAuthorList().get(1).getPersonIdentity().getFamilyName());

      assertEquals(2, cpdHealthStatusActEntry.getObservationList().size());
      assertEquals("b1b0b657-1121-4607-995d-f20b549d767f",
          cpdHealthStatusActEntry.getObservationList().get(0).getId().getExtension());
      assertEquals("1.2.208.184", cpdHealthStatusActEntry.getObservationList().get(0).getId().getRoot());
      assertEquals("1.2.208.184.100.1", cpdHealthStatusActEntry.getObservationList().get(1).getId().getRoot());

      CPDHealthStatusActEntry cpdHealthStatusActEntry2 = cpd
          .getHealthConcernSection()
          .getComponent()
          .getEntryList()
          .get(1);
      assertEquals("7b889ee9-2877-4824-94c2-b1dbe3032402", cpdHealthStatusActEntry2.getId().getExtension());
      assertEquals(true, cpdHealthStatusActEntry2.getEffectiveTimeCdaDate().dateHasTime());

      assertNotNull(cpdHealthStatusActEntry2.getObservationList().get(0).getId());
      assertEquals("b1b0b657-1121-4607-995d-f20b549d027f",
          cpdHealthStatusActEntry2.getObservationList().get(0).getId().getExtension());
      assertEquals("1.2.208.184", cpdHealthStatusActEntry2.getObservationList().get(0).getId().getRoot());
      assertEquals("b1b0b657-1121-4607-995d-f20b549d0202",
          cpdHealthStatusActEntry2.getObservationList().get(1).getId().getExtension());
      assertEquals("1.2.208.184", cpdHealthStatusActEntry2.getObservationList().get(1).getId().getRoot());

      // # GOAL #
      assertNotNull(cpd.getGoalSection());
      assertNotNull(cpd.getGoalSection().getComponent());

      assertEquals(3, cpd.getGoalSection().getComponent().getEntryList().size());
      assertEquals("1.2.208.184.100.1", cpd.getGoalSection().getComponent().getEntryList().get(0).getId().getRoot());
      assertEquals("ce7cfb78-bd16-467e-8bcf-859a30341002",
          cpd.getGoalSection().getComponent().getEntryList().get(1).getId().getExtension());

      assertEquals(2, cpd.getGoalSection().getComponent().getEntryList().get(0).getAuthorList().size());
      assertEquals("Hansen", cpd
          .getGoalSection()
          .getComponent()
          .getEntryList()
          .get(0)
          .getAuthorList()
          .get(0)
          .getPersonIdentity()
          .getFamilyName());
      assertEquals("Larsen", cpd
          .getGoalSection()
          .getComponent()
          .getEntryList()
          .get(0)
          .getAuthorList()
          .get(1)
          .getPersonIdentity()
          .getFamilyName());

      assertEquals("ce7cfb78-bd16-467e-8bcf-859a30340202",
          cpd.getGoalSection().getComponent().getEntryList().get(2).getId().getExtension());

      // # INTERVENTION #
      assertNotNull(cpd.getInterventionSection());
      assertNotNull(cpd.getInterventionSection().getComponent());

      assertEquals(2, cpd.getInterventionSection().getComponent().getAuthorList().size());
      assertEquals("Johnsen",
          cpd.getInterventionSection().getComponent().getAuthorList().get(0).getPersonIdentity().getFamilyName());
      assertEquals("Kurtsen",
          cpd.getInterventionSection().getComponent().getAuthorList().get(1).getPersonIdentity().getFamilyName());

      assertEquals(3, cpd.getInterventionSection().getComponent().getEntryList().size());
      assertEquals("9a6d1bac-17d3-4195-89a4-1121bc809b4d",
          cpd.getInterventionSection().getComponent().getEntryList().get(0).getId().getExtension());
      assertEquals("9a6d1bac-17d3-4195-89a4-1121bc809b02",
          cpd.getInterventionSection().getComponent().getEntryList().get(1).getId().getExtension());

      assertEquals("9a6d1bac-17d3-4195-89a4-1121bc809b4e",
          cpd.getInterventionSection().getComponent().getEntryList().get(2).getId().getExtension());

      assertEquals(2, cpd.getInterventionSection().getComponent().getEntryList().get(2).getPerformerList().size());

      // # OUTCOME #
      assertNotNull(cpd.getOutcomeSection());
      assertNotNull(cpd.getOutcomeSection().getComponent());

      assertEquals(3, cpd.getOutcomeSection().getComponent().getEntryList().size());
      assertEquals("ca9dd558-40ac-4dd9-b028-0de1654fc630",
          cpd.getOutcomeSection().getComponent().getEntryList().get(0).getId().getExtension());
      assertEquals(true,
          cpd.getOutcomeSection().getComponent().getEntryList().get(0).getEffectiveTimeCdaDate().dateHasTime());
      assertEquals("90064452-79cd-4305-81bb-8a70df72a710",
          cpd.getOutcomeSection().getComponent().getEntryList().get(1).getId().getExtension());
      assertEquals(false,
          cpd.getOutcomeSection().getComponent().getEntryList().get(1).getEffectiveTimeCdaDate().dateHasTime());

      assertEquals(2, cpd.getOutcomeSection().getComponent().getEntryList().get(1).getAuthorList().size());
      assertEquals("Poulsen", cpd
          .getOutcomeSection()
          .getComponent()
          .getEntryList()
          .get(1)
          .getAuthorList()
          .get(0)
          .getPersonIdentity()
          .getFamilyName());
      assertEquals("Runesen", cpd
          .getOutcomeSection()
          .getComponent()
          .getEntryList()
          .get(1)
          .getAuthorList()
          .get(1)
          .getPersonIdentity()
          .getFamilyName());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testCpdXMLConverterCheckEntryCodeAndStatusCodeNullOrNotNull() {
    try {
      String cpdAsXML = FileUtil.getData(this.getClass(), "cpd/CPD-DK_Care_Plan_example1.xml");

      CPDDocument cpd = decode(codec, cpdAsXML);

      assertNotNull(cpd);

      // # HEALTH #
      CPDHealthStatusActEntry cpdHealthStatusActEntry = cpd
          .getHealthConcernSection()
          .getComponent()
          .getEntryList()
          .get(0);
      assertNotNull(cpdHealthStatusActEntry.getCode());
      assertNotNull(cpdHealthStatusActEntry.getStatusCode());

      CPDHealthStatusActEntry cpdHealthStatusActEntry2 = cpd
          .getHealthConcernSection()
          .getComponent()
          .getEntryList()
          .get(1);
      assertNotNull(cpdHealthStatusActEntry2.getCode());
      assertNotNull(cpdHealthStatusActEntry2.getStatusCode());

      // # GOAL #
      assertNotNull(cpd.getGoalSection());
      assertNotNull(cpd.getGoalSection().getComponent().getEntryList().get(0).getCode());
      assertNull(cpd.getGoalSection().getComponent().getEntryList().get(1).getCode());
      assertNotNull(cpd.getGoalSection().getComponent().getEntryList().get(2).getCode());
      assertNotNull(cpd.getGoalSection().getComponent().getEntryList().get(0).getStatusCode());
      assertNotNull(cpd.getGoalSection().getComponent().getEntryList().get(1).getStatusCode());
      assertNotNull(cpd.getGoalSection().getComponent().getEntryList().get(2).getStatusCode());

      // # INTERVENTION #
      assertNotNull(cpd.getInterventionSection());
      assertNotNull(cpd.getInterventionSection().getComponent().getEntryList().get(0).getCode());
      assertNull(cpd.getInterventionSection().getComponent().getEntryList().get(1).getCode());
      assertNotNull(cpd.getInterventionSection().getComponent().getEntryList().get(2).getCode());
      assertNotNull(cpd.getInterventionSection().getComponent().getEntryList().get(0).getStatusCode());
      assertNotNull(cpd.getInterventionSection().getComponent().getEntryList().get(1).getStatusCode());
      assertNotNull(cpd.getInterventionSection().getComponent().getEntryList().get(2).getStatusCode());

      // # OUTCOME #
      assertNotNull(cpd.getOutcomeSection());
      assertNotNull(cpd.getOutcomeSection().getComponent().getEntryList().get(0).getId());
      assertNotNull(cpd.getOutcomeSection().getComponent().getEntryList().get(1).getId());
      assertNotNull(cpd.getOutcomeSection().getComponent().getEntryList().get(2).getId());
      assertNotNull(cpd.getOutcomeSection().getComponent().getEntryList().get(0).getStatusCode());
      assertNotNull(cpd.getOutcomeSection().getComponent().getEntryList().get(1).getStatusCode());
      assertNotNull(cpd.getOutcomeSection().getComponent().getEntryList().get(2).getStatusCode());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testCpdXmlConvertermedcomKOLExample() {
    encodeDecodeAndCompare(codec, SetupMedcomExample1.defineAsCDA());
  }

  @Override
  public void runTest() throws Exception {
    testCpdXmlConvertermedcomKOLExample();

  }

}
