package dk.s4.hl7.cda.convert.encode.narrative.text;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse.QRDAnalogSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse.QRDMultipleChoiceResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse.QRDNumericResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer.QRDOrganizerBuilder;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse.QRDTextResponseBuilder;

public class QRDXMLNarrativeTextReplaceResponseConverterTest {

  @Test
  public void checkNoCodeValueIsTheSame() {

    String narrativeTextInput = "narrativeText";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();
    Section<QRDOrganizer> sectionNumeric = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse2553 = new QRDNumericResponseBuilder().setValue("2", BasicType.INT).build();
    sectionNumeric.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse2553).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionNumeric);

    assertEquals(narrativeTextInput, narrativeTextResult);

  }

  @Test
  public void checkNumericReplaceStandardSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!-- Q1234 --> more narrativeText ";
    String narrativeTextExpected = "narrativeText 2 more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();
    Section<QRDOrganizer> sectionNumeric = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDNumericResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setValue("2", BasicType.INT)
        .build();
    sectionNumeric.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionNumeric);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkNumericReplaceMoreThanOneCode() {

    String narrativeTextInput = "narrativeText <!-- Q1234 --> more narrativeText <!-- Q1234 -->";
    String narrativeTextExpected = "narrativeText 2 more narrativeText 2";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();
    Section<QRDOrganizer> sectionNumeric = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDNumericResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setValue("2", BasicType.INT)
        .build();
    sectionNumeric.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionNumeric);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkNumericReplaceNoSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!--Q1234--> more narrativeText ";
    String narrativeTextExpected = "narrativeText 2 more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();
    Section<QRDOrganizer> sectionNumeric = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDNumericResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setValue("2", BasicType.INT)
        .build();
    sectionNumeric.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionNumeric);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkNumericReplaceTwoSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!--  Q1234  --> more narrativeText ";
    String narrativeTextExpected = "narrativeText 2 more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();
    Section<QRDOrganizer> sectionNumeric = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDNumericResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setValue("2", BasicType.INT)
        .build();
    sectionNumeric.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionNumeric);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkAnalogSliderResponseReplaceStandardSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!-- Q1234 --> more narrativeText ";
    String narrativeTextExpected = "narrativeText 2 more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionAnalogSlider = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDAnalogSliderResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setValue("2", BasicType.INT.name())
        .setInterval("0", "100", "1")
        .build();
    sectionAnalogSlider.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionAnalogSlider);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkAnalogSliderResponseReplaceMoreThanOneCode() {

    String narrativeTextInput = "narrativeText <!-- Q1234 --> more narrativeText <!-- Q1234 -->";
    String narrativeTextExpected = "narrativeText 2 more narrativeText 2";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionAnalogSlider = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDAnalogSliderResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setValue("2", BasicType.INT.name())
        .setInterval("0", "100", "1")
        .build();
    sectionAnalogSlider.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionAnalogSlider);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkAnalogSliderResponseReplaceNoSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!--Q1234--> more narrativeText ";
    String narrativeTextExpected = "narrativeText 2 more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionAnalogSlider = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDAnalogSliderResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setValue("2", BasicType.INT.name())
        .setInterval("0", "100", "1")
        .build();
    sectionAnalogSlider.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionAnalogSlider);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkAnalogSliderResponseReplaceTwoSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!--  Q1234  --> more narrativeText ";
    String narrativeTextExpected = "narrativeText 2 more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionAnalogSlider = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDAnalogSliderResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setValue("2", BasicType.INT.name())
        .setInterval("0", "100", "1")
        .build();
    sectionAnalogSlider.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionAnalogSlider);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkTextResponseReplaceStandardSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!-- Q1234 --> more narrativeText ";
    String narrativeTextExpected = "narrativeText Besvarelse more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setText("Besvarelse")
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkTextResponseReplaceMoreThanOneCode() {

    String narrativeTextInput = "narrativeText <!-- Q1234 --> more narrativeText <!-- Q1234 -->";
    String narrativeTextExpected = "narrativeText Besvarelse more narrativeText Besvarelse";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setText("Besvarelse")
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkTextResponseReplaceNoSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!--Q1234--> more narrativeText ";
    String narrativeTextExpected = "narrativeText Besvarelse more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setText("Besvarelse")
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkTextResponseReplaceTwoSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!--  Q1234  --> more narrativeText ";
    String narrativeTextExpected = "narrativeText Besvarelse more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setText("Besvarelse")
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkTextResponseReplaceSpecialCharInSearchString() {

    String narrativeTextInput = "narrativeText <!--  Q1234  --> more narrativeText ";
    String narrativeTextExpected = "narrativeText æøå&amp;@£$ more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setText("æøå&amp;@£$")
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkMultipleChoiceResponseReplaceStandardSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!-- A1234_2 --> more narrativeText ";
    String narrativeTextExpected = "narrativeText X more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .addAnswer("A1234_2", "1.2.208.176.1.5", "A1234_2 description", "Sundhedsdatastyrelsen")
        .setInterval(0, 1)
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkMultipleChoiceResponseReplaceMoreThanOneCode() {

    String narrativeTextInput = "narrativeText <!-- A1234_2 --> more narrativeText <!-- A1234_2 -->";
    String narrativeTextExpected = "narrativeText X more narrativeText X";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .addAnswer("A1234_2", "1.2.208.176.1.5", "A1234_2 description", "Sundhedsdatastyrelsen")
        .setInterval(0, 1)
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkMultipleChoiceResponseReplaceNoSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!--A1234_2--> more narrativeText ";
    String narrativeTextExpected = "narrativeText X more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .addAnswer("A1234_2", "1.2.208.176.1.5", "A1234_2 description", "Sundhedsdatastyrelsen")
        .setInterval(0, 1)
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkMultipleChoiceResponseReplaceTwoSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!--  A1234_2  --> more narrativeText ";
    String narrativeTextExpected = "narrativeText X more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .addAnswer("A1234_2", "1.2.208.176.1.5", "A1234_2 description", "Sundhedsdatastyrelsen")
        .setInterval(0, 1)
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);
    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  //*
  @Test
  public void checkDiscreteSliderResponseReplaceStandardSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!-- A1234_2 --> more narrativeText ";
    String narrativeTextExpected = "narrativeText X more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setAnswer("A1234_2", "1.2.208.176.1.5", "A1234_2 description", "Sundhedsdatastyrelsen")
        .setMinimum(0)
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkDiscreteSliderResponseReplaceMoreThanOneCode() {

    String narrativeTextInput = "narrativeText <!-- A1234_2 --> more narrativeText <!-- A1234_2 -->";
    String narrativeTextExpected = "narrativeText X more narrativeText X";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setAnswer("A1234_2", "1.2.208.176.1.5", "A1234_2 description", "Sundhedsdatastyrelsen")
        .setMinimum(0)
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkDiscreteSliderResponseReplaceNoSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!--A1234_2--> more narrativeText ";
    String narrativeTextExpected = "narrativeText X more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setAnswer("A1234_2", "1.2.208.176.1.5", "A1234_2 description", "Sundhedsdatastyrelsen")
        .setMinimum(0)
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);

    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

  @Test
  public void checkDiscreteSliderResponseReplaceTwoSpaceInSearchString() {

    String narrativeTextInput = "narrativeText <!--  A1234_2  --> more narrativeText ";
    String narrativeTextExpected = "narrativeText X more narrativeText ";

    QRDXMLNarrativeTextReplaceResponseConverter qrdXMLNarrativeTextReplaceResponseConverter = new QRDXMLNarrativeTextReplaceResponseConverter();

    Section<QRDOrganizer> sectionText = new Section<QRDOrganizer>("Title", narrativeTextInput);
    QRDResponse qRDResponse = new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("Q1234", "1.2.208.176.1.5", "Q1234 description", "Sundhedsdatastyrelsen"))
        .setAnswer("A1234_2", "1.2.208.176.1.5", "A1234_2 description", "Sundhedsdatastyrelsen")
        .setMinimum(0)
        .build();
    sectionText.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qRDResponse).build());

    String narrativeTextResult = qrdXMLNarrativeTextReplaceResponseConverter.createText(sectionText);
    assertEquals(narrativeTextExpected, narrativeTextResult);

  }

}
