package dk.s4.hl7.cda.model.cpd.v20;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument.Status;
import dk.s4.hl7.cda.model.cpd.v20.CPDInterventionActEntry.CPDInterventionActEntryBuilder;
import dk.s4.hl7.cda.model.util.DateUtil;

public class CPDInterventionActEntryTest {

  CPDInterventionActEntryBuilder subject;

  @Before
  public void setUpValidBuilder() throws Exception {

    subject = new CPDInterventionActEntryBuilder()
        .setId(new IDBuilder().setRoot("1.2.208.184").setExtension("9a6d1bac-17d3-4195-89a4-1121bc809b4d").build())
        .setCode(new CodedValue("AAF1", "1.2.208.176.2.4", "Indlæggelse", "Sundhedsstyrelsens Klassifikations System"))
        .setText("<reference value=\"#testing1\"/>")
        .setStatusCode(Status.COMPLETED)
        .setEffectiveDate(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 0, 0, 0));
  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingId() {

    //given
    subject.setId(null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingStatusCode() {

    //given
    subject.setStatusCode(null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test
  public void shouldBuild_EffectiveDate() {

    //given

    //when
    CPDInterventionActEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNotNull(result.getEffectiveTime());
    assertFalse(result.effectiveTimeHasTime());
    assertNull(result.getEffectiveTimeInterval());
  }

  @Test
  public void shouldBuild_EffectTime() {

    //given
    subject.setEffectiveTimeInterval(null).setEffectiveTime(
        DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 12, 40, 0));

    //when
    CPDInterventionActEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNotNull(result.getEffectiveTime());
    assertTrue(result.effectiveTimeHasTime());
    assertNull(result.getEffectiveTimeInterval());
  }

  @Test
  public void shouldBuild_EffectTimeInterval() {

    //given
    subject
        .setEffectiveTimeInterval(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 11, 21, 35), null)
        .setEffectiveDate(null);

    //when
    CPDInterventionActEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNull(result.getEffectiveTime());
    assertFalse(result.effectiveTimeHasTime());
    assertNotNull(result.getEffectiveTimeInterval());
    assertNotNull(result.getEffectiveTimeInterval().getStartTime());
    assertTrue(result.getEffectiveTimeInterval().startTimeHasTime());
    assertNull(result.getEffectiveTimeInterval().getStopTime());
    assertTrue(result.getEffectiveTimeInterval().stopTimeHasTime());
  }

  @Test
  public void shouldBuild_EffectDateInterval() {

    //given
    subject
        .setEffectiveDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 0, 0, 0), null)
        .setEffectiveDate(null);

    //when
    CPDInterventionActEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNull(result.getEffectiveTime());
    assertFalse(result.effectiveTimeHasTime());
    assertNotNull(result.getEffectiveTimeInterval());
    assertNotNull(result.getEffectiveTimeInterval().getStartTime());
    assertFalse(result.getEffectiveTimeInterval().startTimeHasTime());
    assertNull(result.getEffectiveTimeInterval().getStopTime());
    assertFalse(result.getEffectiveTimeInterval().stopTimeHasTime());
  }

  @Test
  public void shouldNotFailOnMissingEffectiveTimes() {

    //given
    Date dateNull = null;
    subject.setEffectiveTime(dateNull).setEffectiveTimeInterval(null);

    //when
    CPDInterventionActEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNull(result.getEffectiveTime());
    assertNull(result.getEffectiveTimeInterval());

  }

  @Test
  public void shouldNotFailOnMissingEffectiveDates() {

    //given
    Date dateNull = null;
    subject.setEffectiveDate(dateNull).setEffectiveTimeInterval(null);

    //when
    CPDInterventionActEntry result = subject.build();

    //then
    assertNotNull(result);
    assertNull(result.getEffectiveTime());
    assertNull(result.getEffectiveTimeInterval());

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingEffectiveTimesInInterval() {

    //given
    subject.setEffectiveTimeInterval(null, null);

    //when
    subject.build();

    //then
    //excpetion

  }

  @Test(expected = RuntimeException.class)
  public void shouldFailOnMissingEffectiveDatesInInterval() {

    //given
    subject.setEffectiveDateInterval(null, null);

    //when
    subject.build();

    //then
    //excpetion

  }

}
