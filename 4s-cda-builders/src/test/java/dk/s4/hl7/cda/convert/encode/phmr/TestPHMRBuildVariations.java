package dk.s4.hl7.cda.convert.encode.phmr;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;

/**
 * Validate the output from our own PHMR builder with the example from MedCom.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public final class TestPHMRBuildVariations extends BaseEncodeTest<PHMRDocument> {

  public TestPHMRBuildVariations() {
    super("src/test/resources/phmr/");
  }

  @Before
  public void before() {
    setCodec(new PHMRXmlCodec());
  }

  @Test
  public void testServiceEventEffectTimeHighNotNull() throws Exception {
    PHMRDocument phmrDocument = SetupMedcomKOLExample1.defineAsCDA();
    String phmrXml = encodeDocument(phmrDocument);

    assertNotNull(phmrXml);
    assertTrue(phmrXml.contains("<low value=\"20140106080200+0100\"/>"));
    assertTrue(phmrXml.contains("<high value=\"20140110081500+0100\"/>"));

  }

  @Test
  public void testServiceEventEffectTimeHighNull() throws Exception {
    PHMRDocument phmrDocument = SetupMedcomKOLExample1.defineAsCDA();
    phmrDocument.setDocumentationTimeInterval(phmrDocument.getServiceStartTime(), null);
    String phmrXml = encodeDocument(phmrDocument);

    assertNotNull(phmrXml);
    assertTrue(phmrXml.contains("<low value=\"20140106080200+0100\"/>"));
    assertTrue(phmrXml.contains("<high nullFlavor=\"NI\"/>"));

  }

}