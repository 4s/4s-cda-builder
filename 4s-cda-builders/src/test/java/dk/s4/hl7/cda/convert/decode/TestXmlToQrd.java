package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.QRDXmlCodec;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.model.QuestionnaireMedia;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public final class TestXmlToQrd extends BaseDecodeTest implements ConcurrencyTestCase {
  private Codec<QRDDocument, String> codec;

  @Before
  public void before() {
    setCodec(new QRDXmlCodec());
  }

  public void setCodec(QRDXmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testQRDXMLConverterTextExample() {
    try {
      String XML = FileUtil.getData(this.getClass(), "qrd/generatedTextTest.xml");

      QRDDocument qrdDocument = decode(codec, XML);

      assertNotNull(qrdDocument);
      assertNotNull(qrdDocument.getConfidentialityCode());
      assertNotNull(qrdDocument.getSections());
      assertEquals(1, qrdDocument.getSections().size());
      assertNotNull(qrdDocument.getSections().get(0));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers());
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses());
      assertEquals(2, qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().size());
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(1));

      assertQRDMediaNull(
          qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0).getQuestionnaireMedia());
      assertQRDMediaValue(
          qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(1).getQuestionnaireMedia());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testQRDXMLConverterNumericExample() {
    try {
      String XML = FileUtil.getData(this.getClass(), "qrd/generatedNumericTest.xml");

      QRDDocument qrdDocument = decode(codec, XML);

      assertNotNull(qrdDocument);
      assertNotNull(qrdDocument.getSections());
      assertEquals(1, qrdDocument.getSections().size());
      assertNotNull(qrdDocument.getSections().get(0));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers());
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses());
      assertEquals(3, qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().size());
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(1));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(2));

      assertQRDMediaNull(
          qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0).getQuestionnaireMedia());
      assertQRDMediaNull(
          qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(1).getQuestionnaireMedia());
      assertQRDMediaValue(
          qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(2).getQuestionnaireMedia());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testQRDXMLConverterMultipleChoiceExample() {
    try {
      String XML = FileUtil.getData(this.getClass(), "qrd/generatedMultipleChoiceTest.xml");

      QRDDocument qrdDocument = decode(codec, XML);

      assertNotNull(qrdDocument);
      assertNotNull(qrdDocument.getSections());
      assertEquals(1, qrdDocument.getSections().size());
      assertNotNull(qrdDocument.getSections().get(0));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers());
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses());
      assertEquals(3, qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().size());
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(1));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(2));

      assertQRDMediaNull(
          qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0).getQuestionnaireMedia());
      assertQRDMediaNull(
          qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(1).getQuestionnaireMedia());
      assertQRDMediaValue(
          qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(2).getQuestionnaireMedia());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  private void assertQRDMediaNull(QuestionnaireMedia questionnaireMedia) {
    assertNull(questionnaireMedia);
  }

  private void assertQRDMediaValue(QuestionnaireMedia questionnaireMedia) {
    assertNotNull(questionnaireMedia);
    assertNotNull(questionnaireMedia.getMediaType());
    assertEquals("image/png", questionnaireMedia.getMediaType());
    assertNotNull(questionnaireMedia.getRepresentation());
    assertEquals("B64", questionnaireMedia.getRepresentation());
    assertNotNull(questionnaireMedia.getValue());
    assertNotNull(questionnaireMedia.getId());
    assertEquals("fememotes-400_dxt", questionnaireMedia.getId());
  }

  @Test
  public void testQRDXMLConverterMultipleChoiceTestWithAssociatedText() {
    try {

      String XML = FileUtil.getData(this.getClass(), "qrd/MultipleChoiceTestWithAssociatedText.xml");

      QRDDocument qrdDocument = decode(codec, XML);

      assertNotNull(qrdDocument);
      assertNotNull(qrdDocument.getSections());
      assertEquals(1, qrdDocument.getSections().size());
      assertNotNull(qrdDocument.getSections().get(0));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers());
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0));
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses());
      assertEquals(1, qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().size());
      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0));

      assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0).getQuestion());
      assertTrue(qrdDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQRDResponses()
          .get(0) instanceof QRDMultipleChoiceResponse);
      QRDMultipleChoiceResponse qrdMultipleChoiceResponse = (QRDMultipleChoiceResponse) qrdDocument
          .getSections()
          .get(0)
          .getOrganizers()
          .get(0)
          .getQRDResponses()
          .get(0);
      assertNotNull(qrdMultipleChoiceResponse.getAssociatedTextResponse());
      assertNotNull(qrdMultipleChoiceResponse.getAssociatedTextResponse().getId());
      assertNotNull(qrdMultipleChoiceResponse.getAssociatedTextResponse().getId().getExtension());
      assertEquals("2229", qrdMultipleChoiceResponse.getAssociatedTextResponse().getId().getExtension());
      assertNotNull(qrdMultipleChoiceResponse.getAssociatedTextResponse().getCode());
      assertEquals("Q2229", qrdMultipleChoiceResponse.getAssociatedTextResponse().getCode().getCode());
      assertNotNull(qrdMultipleChoiceResponse.getAssociatedTextResponse().getCode().getCode());
      assertNotNull(qrdMultipleChoiceResponse.getAssociatedTextResponse().getQuestion());
      assertEquals("Her kan du skrive, hvad du ønsker støtte til",
          qrdMultipleChoiceResponse.getAssociatedTextResponse().getQuestion());
      assertNotNull(qrdMultipleChoiceResponse.getAssociatedTextResponse().getText());
      assertEquals("Jeg ønsker støtte til 123 og ABC", qrdMultipleChoiceResponse.getAssociatedTextResponse().getText());
      assertNotNull(qrdMultipleChoiceResponse.getAssociatedTextResponse().getQuestionnaireMedia());
      assertNotNull(qrdMultipleChoiceResponse.getAssociatedTextResponse().getQuestionnaireMedia().getValue());
      assertEquals("megetKortOgIkkeValid",
          qrdMultipleChoiceResponse.getAssociatedTextResponse().getQuestionnaireMedia().getValue());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }

  }

  @Test
  public void testXmlToQRD() throws Exception {
    encodeDecodeAndCompare(codec, SetupQRDDocumentForTest.defineAsCDA());
  }

  @Override
  public void runTest() throws Exception {
    testXmlToQRD();
  }
}
