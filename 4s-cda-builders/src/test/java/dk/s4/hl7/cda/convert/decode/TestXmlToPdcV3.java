package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.PDCV30XmlCodec;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Patient.Gender;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.TelecomPhone;
import dk.s4.hl7.cda.model.pdc.v30.AuthorOfInformation;
import dk.s4.hl7.cda.model.pdc.v30.AuthorOfInformation.SourceOfInformation;
import dk.s4.hl7.cda.model.pdc.v30.NoResuscitationRegistration.Registered;
import dk.s4.hl7.cda.model.pdc.v30.PDCDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public final class TestXmlToPdcV3 extends BaseDecodeTest implements ConcurrencyTestCase {

  private PDCV30XmlCodec codec;

  @Before
  public void before() {
    setCodec(new PDCV30XmlCodec());
  }

  public void setCodec(PDCV30XmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testPdcXMLConverterMedcomExampleMinimum() {

    try {
      String XML = FileUtil.getData(this.getClass(), "pdc/PDC30_Eks_minimalt-eksempel_adj.xml");

      PDCDocument pdcDocument = decode(codec, XML);

      // # HEADER #

      assertNotNull(pdcDocument);
      assertEquals("DK", pdcDocument.getRealmCode());
      assertEquals("POCD_HD000040", pdcDocument.getTypeIdExtension());
      assertEquals("2.16.840.1.113883.1.3", pdcDocument.getTypeIdRoot());
      assertEquals("1.2.208.184.16.1", pdcDocument.getTemplateIds()[0]);
      assertEquals("e361c3a7-6d99-47bb-9503-2628d7ed72ba", pdcDocument.getId().getExtension());
      assertEquals("1.2.208.184", pdcDocument.getId().getRoot());
      assertEquals("MedCom", pdcDocument.getId().getAuthorityName());
      assertEquals("PDC", pdcDocument.getCodeCodedValue().getCode());
      assertEquals("Stamkort", pdcDocument.getCodeCodedValue().getDisplayName());
      assertEquals("1.2.208.184.100.1", pdcDocument.getCodeCodedValue().getCodeSystem());
      // assertNotNull(pdcDocument.getCodeCodedValue().getCodeSystemName()); //is not
      // parsed: MedCom Message Codes
      assertEquals("Personal Data Card for 0107729995", pdcDocument.getTitle());
      assertEquals(new GregorianCalendar(2023, 10, 8, 13, 16, 36).getTime(), pdcDocument.getEffectiveTime());
      assertEquals("N", pdcDocument.getConfidentialityCode());
      assertEquals("da-DK", pdcDocument.getLanguageCode());

      assertEquals("0107729995", pdcDocument.getPatient().getId().getExtension());
      assertEquals("CPR", pdcDocument.getPatient().getId().getAuthorityName());
      assertEquals("1.2.208.176.1.2", pdcDocument.getPatient().getId().getRoot());
      assertEquals(AddressData.Use.HomeAddress, pdcDocument.getPatient().getAddress().getAddressUse());
      assertEquals("Testervej 18", pdcDocument.getPatient().getAddress().getStreet()[0]);
      assertEquals("6200", pdcDocument.getPatient().getAddress().getPostalCode());
      assertEquals("København V", pdcDocument.getPatient().getAddress().getCity());
      assertEquals("DK", pdcDocument.getPatient().getAddress().getCountry());

      assertEquals("Max", pdcDocument.getPatient().getGivenNames()[0]);
      assertEquals("Test", pdcDocument.getPatient().getGivenNames()[1]);
      assertEquals("Berggren", pdcDocument.getPatient().getFamilyName());
      assertEquals(Gender.Male, pdcDocument.getPatient().getGender());
      assertEquals(new GregorianCalendar(1972, 6, 1, 0, 0, 0).getTime(), pdcDocument.getPatient().getBirthDate());

      assertNull(pdcDocument.getProviderOrganization().getId());
      assertNull(pdcDocument.getProviderOrganization().getOrgName());
      assertEquals(0, pdcDocument.getProviderOrganization().getTelecomList().length);
      assertNull(pdcDocument.getProviderOrganization().getAddress());

      assertEquals(new GregorianCalendar(2023, 10, 8, 13, 16, 36).getTime(), pdcDocument.getAuthor().getTime());
      assertEquals("1126211000016009", pdcDocument.getAuthor().getId().getExtension());
      assertEquals("SOR", pdcDocument.getAuthor().getId().getAuthorityName());
      assertEquals("1.2.208.176.1.1", pdcDocument.getAuthor().getId().getRoot());
      assertEquals("Sundhedsdatastyrelsen", pdcDocument.getAuthor().getOrganizationIdentity().getOrgName());
      assertEquals("1126211000016009", pdcDocument.getCustodianIdentity().getId().getExtension());
      assertEquals("SOR", pdcDocument.getCustodianIdentity().getId().getAuthorityName());
      assertEquals("1.2.208.176.1.1", pdcDocument.getCustodianIdentity().getId().getRoot());
      assertEquals("Sundhedsdatastyrelsen", pdcDocument.getCustodianIdentity().getOrgName());

      assertEquals(new GregorianCalendar(2023, 10, 8, 13, 16, 36).getTime(), pdcDocument.getServiceStartTime());
      assertNull(pdcDocument.getServiceStopTime());
      assertEquals("pdc-v3.0", pdcDocument.getCdaProfileAndVersion());

      // # CONTENT #

      assertTrue(pdcDocument.getText().contains("<td>Borgerens navn og adresse</td>"));
      assertTrue(pdcDocument.getText().contains("<td>Sygesikringsgruppe</td>"));

      // CustodyInformation
      assertNull(pdcDocument.getCustodyInformationList());

      // NameAndAddressInformation
      assertId(pdcDocument.getNameAndAddressInformation().getId(), "6f45a54d-3338-4e63-9056-866919a7aff5",
          "1.2.208.184", null);
      assertCode(pdcDocument.getNameAndAddressInformation().getCode(), "CitizenNameAddr", "Borgerens navn og adresse",
          "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(1, pdcDocument.getNameAndAddressInformation().getAddressData().getStreet().length);
      assertEquals("Testervej 18", pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[0]);

      assertEquals("København V", pdcDocument.getNameAndAddressInformation().getAddressData().getCity());
      assertEquals("6200", pdcDocument.getNameAndAddressInformation().getAddressData().getPostalCode());
      assertEquals("DK", pdcDocument.getNameAndAddressInformation().getAddressData().getCountry());
      assertEquals(AddressData.Use.HomeAddress,
          pdcDocument.getNameAndAddressInformation().getAddressData().getAddressUse());

      assertPersonIdentity(pdcDocument.getNameAndAddressInformation().getPersonIdentity(),
          new PersonBuilder("Berggren").addGivenName("Max").addGivenName("Test").build());

      assertNull(pdcDocument.getNameAndAddressInformation().getConfidentialId());
      assertFalse(pdcDocument.getNameAndAddressInformation().isAddressConfidential());

      assertAuthorRegisterEntered(pdcDocument.getNameAndAddressInformation().getAuthorOfInformation(), "CPR", null,
          "1.2.208.176.1.2", "CPR", null, null);

      // CoverageGroup
      assertId(pdcDocument.getCoverageGroup().getId(), "261f4b73-ab44-4c5d-bf8a-7476a11bdbca", "1.2.208.184", null);
      assertCode(pdcDocument.getCoverageGroup().getCode(), "CoverageGroup", "Sygesikringsgruppe", "1.2.208.184.100.1",
          "MedCom Message Codes");
      assertEquals("1", pdcDocument.getCoverageGroup().getCoverageGroup());
      assertAuthorRegisterEntered(pdcDocument.getCoverageGroup().getAuthorOfInformation(), "Sygesikringen", null,
          "1.2.208.176.2.7", "Sygesikringen", null, null);

      // OrganDonorRegistration
      assertId(pdcDocument.getOrganDonorRegistration().getId(), "2ffdee02-d8d3-4d53-8b13-e542affa96f6", "1.2.208.184",
          null);
      assertCode(pdcDocument.getOrganDonorRegistration().getCode(), "OrganDonorRegistration", "Registreret organdonor",
          "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(true, pdcDocument.getOrganDonorRegistration().isRegistered());

      assertAuthorRegisterEntered(pdcDocument.getOrganDonorRegistration().getAuthorOfInformation(),
          "Dansk Center For Organdonation", null, "1.2.208.176.1.10", "CPR", null, null);

      // TreatmentWillRegistration
      assertId(pdcDocument.getTreatmentWillRegistration().getId(), "1feac1cb-ad27-4ecd-a36b-35d37036a644",
          "1.2.208.184", null);
      assertCode(pdcDocument.getTreatmentWillRegistration().getCode(), "TreatmentWillRegistration",
          "Registreret behandlingstestamente", "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(true, pdcDocument.getTreatmentWillRegistration().isRegistered());
      assertAuthorRegisterEntered(pdcDocument.getTreatmentWillRegistration().getAuthorOfInformation(),
          "Sundhedsdatastyrelsen", null, "1.2.208.176.1.9", "Sundhedsdatastyrelsen", null, null);

      // LivingWillRegistration
      assertId(pdcDocument.getLivingWillRegistration().getId(), "5b6e9f76-818f-4c2b-ac11-521eca6136e5", "1.2.208.184",
          null);
      assertCode(pdcDocument.getLivingWillRegistration().getCode(), "LivingWillRegistration",
          "Registreret livstestamente", "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(false, pdcDocument.getLivingWillRegistration().isRegistered());
      assertAuthorRegisterEntered(pdcDocument.getLivingWillRegistration().getAuthorOfInformation(),
          "Sundhedsdatastyrelsen", null, "1.2.208.176.1.8", "Sundhedsdatastyrelsen", null, null);

      // NoResuscitation 
      assertId(pdcDocument.getNoResuscitationRegistration().getId(), "d90df5cb-602c-44d5-8cc6-9fb7ed9b8df9",
          "1.2.208.184", null);
      assertCode(pdcDocument.getNoResuscitationRegistration().getCode(), "NoResuscitationRegistration",
          "Registreret fravalg af genoplivningsforsøg v. hjertestop", "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(Registered.False, pdcDocument.getNoResuscitationRegistration().getRegistered());
      assertAuthorRegisterEntered(pdcDocument.getNoResuscitationRegistration().getAuthorOfInformation(),
          "Sundhedsdatastyrelsen", null, "1.2.208.176.1.8", "Sundhedsdatastyrelsen", null, null);

      // ManuallyEnteredSpokenLanguage
      assertNull(pdcDocument.getManuallyEnteredSpokenLanguage());

      // ManuallyEnteredTemporaryAddress
      assertNull(pdcDocument.getManuallyEnteredTemporaryAddress());

      // ManuallyEnteredDentistInformation
      assertNull(pdcDocument.getManuallyEnteredDentistInformation());

      // ManuallyEnteredContactInformation
      assertNull(pdcDocument.getManuallyEnteredContactInformation());

      // ManuallyEnteredInformationAboutRelatives
      assertNull(pdcDocument.getManuallyEnteredInformationAboutRelativesList());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }

  }

  @Test
  public void testPdcXMLConverterMedcomExampleMinimum_NoResuscitationUnAvailable() {

    try {
      String XML = FileUtil.getData(this.getClass(), "pdc/PDC30_Eks_minimalt-eksempel_adj_varianter.xml");

      PDCDocument pdcDocument = decode(codec, XML);

      // # HEADER #

      assertNotNull(pdcDocument);

      // # CONTENT #

      // CustodyInformation
      assertNull(pdcDocument.getCustodyInformationList());

      // NameAndAddressInformation
      assertNotNull(pdcDocument.getNameAndAddressInformation());

      // CoverageGroup
      assertNotNull(pdcDocument.getCoverageGroup());

      // OrganDonorRegistration
      assertNotNull(pdcDocument.getOrganDonorRegistration());

      // TreatmentWillRegistration
      assertNotNull(pdcDocument.getTreatmentWillRegistration());

      // LivingWillRegistration
      assertNotNull(pdcDocument.getLivingWillRegistration());

      // NoResuscitation 
      assertId(pdcDocument.getNoResuscitationRegistration().getId(), "d90df5cb-602c-44d5-8cc6-9fb7ed9b8df9",
          "1.2.208.184", null);
      assertCode(pdcDocument.getNoResuscitationRegistration().getCode(), "NoResuscitationRegistration",
          "Registreret fravalg af genoplivningsforsøg v. hjertestop", "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(Registered.Nav, pdcDocument.getNoResuscitationRegistration().getRegistered());
      assertAuthorRegisterEntered(pdcDocument.getNoResuscitationRegistration().getAuthorOfInformation(),
          "Sundhedsdatastyrelsen", null, "1.2.208.176.1.8", "Sundhedsdatastyrelsen", null, null);

      // ManuallyEnteredSpokenLanguage
      assertNull(pdcDocument.getManuallyEnteredSpokenLanguage());

      // ManuallyEnteredTemporaryAddress
      assertNull(pdcDocument.getManuallyEnteredTemporaryAddress());

      // ManuallyEnteredDentistInformation
      assertNull(pdcDocument.getManuallyEnteredDentistInformation());

      // ManuallyEnteredContactInformation
      assertNull(pdcDocument.getManuallyEnteredContactInformation());

      // ManuallyEnteredInformationAboutRelatives
      assertNull(pdcDocument.getManuallyEnteredInformationAboutRelativesList());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }

  }

  @Test
  public void testPdcXMLConverterMedcomExampleMaximum() {
    try {
      String XML = FileUtil.getData(this.getClass(), "pdc/PDC30_Eks_maksimalt-eksempel_adj.xml");

      PDCDocument pdcDocument = decode(codec, XML);

      // # HEADER #

      assertNotNull(pdcDocument);
      assertEquals("DK", pdcDocument.getRealmCode());
      assertEquals("POCD_HD000040", pdcDocument.getTypeIdExtension());
      assertEquals("2.16.840.1.113883.1.3", pdcDocument.getTypeIdRoot());
      assertEquals("1.2.208.184.16.1", pdcDocument.getTemplateIds()[0]);
      assertEquals("89edc116-82ae-4a33-aead-28a1278d6129", pdcDocument.getId().getExtension());
      assertEquals("1.2.208.184", pdcDocument.getId().getRoot());
      assertEquals("MedCom", pdcDocument.getId().getAuthorityName());
      assertEquals("PDC", pdcDocument.getCodeCodedValue().getCode());
      assertEquals("Stamkort", pdcDocument.getCodeCodedValue().getDisplayName());
      assertEquals("1.2.208.184.100.1", pdcDocument.getCodeCodedValue().getCodeSystem());
      // assertNotNull(pdcDocument.getCodeCodedValue().getCodeSystemName()); //is not
      // parsed: MedCom Message Codes
      assertEquals("Personal Data Card for 0107729995", pdcDocument.getTitle());
      assertEquals(new GregorianCalendar(2023, 10, 8, 14, 14, 11).getTime(), pdcDocument.getEffectiveTime());
      assertEquals("N", pdcDocument.getConfidentialityCode());
      assertEquals("da-DK", pdcDocument.getLanguageCode());

      assertEquals("0107729995", pdcDocument.getPatient().getId().getExtension());
      assertEquals("CPR", pdcDocument.getPatient().getId().getAuthorityName());
      assertEquals("1.2.208.176.1.2", pdcDocument.getPatient().getId().getRoot());
      assertEquals(AddressData.Use.HomeAddress, pdcDocument.getPatient().getAddress().getAddressUse());
      assertEquals("Testervej 18", pdcDocument.getPatient().getAddress().getStreet()[0]);
      assertEquals("6200", pdcDocument.getPatient().getAddress().getPostalCode());
      assertEquals("København V", pdcDocument.getPatient().getAddress().getCity());
      assertEquals("DK", pdcDocument.getPatient().getAddress().getCountry());

      assertEquals("Max", pdcDocument.getPatient().getGivenNames()[0]);
      assertEquals("Test", pdcDocument.getPatient().getGivenNames()[1]);
      assertEquals("Berggren", pdcDocument.getPatient().getFamilyName());
      assertEquals(Gender.Male, pdcDocument.getPatient().getGender());
      assertEquals(new GregorianCalendar(1972, 6, 1, 0, 0, 0).getTime(), pdcDocument.getPatient().getBirthDate());

      assertNull(pdcDocument.getProviderOrganization().getId());
      assertNull(pdcDocument.getProviderOrganization().getOrgName());
      assertEquals(0, pdcDocument.getProviderOrganization().getTelecomList().length);
      assertNull(pdcDocument.getProviderOrganization().getAddress());

      assertEquals(new GregorianCalendar(2023, 10, 8, 14, 14, 11).getTime(), pdcDocument.getAuthor().getTime());
      assertEquals("1126211000016009", pdcDocument.getAuthor().getId().getExtension());
      assertEquals("SOR", pdcDocument.getAuthor().getId().getAuthorityName());
      assertEquals("1.2.208.176.1.1", pdcDocument.getAuthor().getId().getRoot());
      assertEquals("Sundhedsdatastyrelsen", pdcDocument.getAuthor().getOrganizationIdentity().getOrgName());
      assertEquals("1126211000016009", pdcDocument.getCustodianIdentity().getId().getExtension());
      assertEquals("SOR", pdcDocument.getCustodianIdentity().getId().getAuthorityName());
      assertEquals("1.2.208.176.1.1", pdcDocument.getCustodianIdentity().getId().getRoot());
      assertEquals("Sundhedsdatastyrelsen", pdcDocument.getCustodianIdentity().getOrgName());

      assertEquals(new GregorianCalendar(2023, 10, 8, 14, 14, 11).getTime(), pdcDocument.getServiceStartTime());
      assertNull(pdcDocument.getServiceStopTime());
      assertEquals("pdc-v3.0", pdcDocument.getCdaProfileAndVersion());

      // # CONTENT #

      assertTrue(pdcDocument.getText().contains("<td>Borgerens navn og adresse</td>"));
      assertTrue(pdcDocument.getText().contains("<td>Pårørende, indtastet</td>"));

      int idx;

      // CustodyInformation
      assertEquals(1, pdcDocument.getCustodyInformationList().size());

      // CustodyInformation 1
      idx = 0;
      assertId(pdcDocument.getCustodyInformationList().get(idx).getId(), "db52d28b-e825-35ff-bf96-3cafaad9bbd0",
          "1.2.208.184", null);
      assertCode(pdcDocument.getCustodyInformationList().get(idx).getCode(), "ChildCustody", "Forældremyndighed over",
          "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals("0505109990", pdcDocument.getCustodyInformationList().get(idx).getCpr());
      assertPersonIdentity(pdcDocument.getCustodyInformationList().get(idx).getPersonIdentity(),
          new PersonBuilder("Berggren").addGivenName("Gitte").addGivenName("Nancy").addGivenName("Test").build());
      assertCode(pdcDocument.getCustodyInformationList().get(idx).getRelationCode(), "far", "Far", "1.2.208.184.100.2",
          "MedCom Relation Codes");
      assertAuthorRegisterEntered(pdcDocument.getCustodyInformationList().get(idx).getAuthorOfInformation(), null,
          "MSK", "1.2.208.176.1.2", "CPR", null,
          new PersonBuilder("Berggren").addGivenName("Max").addGivenName("Test").build());

      // NameAndAddressInformation
      assertId(pdcDocument.getNameAndAddressInformation().getId(), "ef690fd7-0fb4-4c6a-8ad2-883fe48efd8f",
          "1.2.208.184", null);
      assertCode(pdcDocument.getNameAndAddressInformation().getCode(), "CitizenNameAddr", "Borgerens navn og adresse",
          "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(1, pdcDocument.getNameAndAddressInformation().getAddressData().getStreet().length);
      assertEquals("Testervej 18", pdcDocument.getNameAndAddressInformation().getAddressData().getStreet()[0]);

      assertEquals("København V", pdcDocument.getNameAndAddressInformation().getAddressData().getCity());
      assertEquals("6200", pdcDocument.getNameAndAddressInformation().getAddressData().getPostalCode());
      assertEquals("DK", pdcDocument.getNameAndAddressInformation().getAddressData().getCountry());
      assertEquals(AddressData.Use.HomeAddress,
          pdcDocument.getNameAndAddressInformation().getAddressData().getAddressUse());

      assertPersonIdentity(pdcDocument.getNameAndAddressInformation().getPersonIdentity(),
          new PersonBuilder("Berggren").addGivenName("Max").addGivenName("Test").build());

      assertNull(pdcDocument.getNameAndAddressInformation().getConfidentialId());
      assertFalse(pdcDocument.getNameAndAddressInformation().isAddressConfidential());

      assertAuthorRegisterEntered(pdcDocument.getNameAndAddressInformation().getAuthorOfInformation(), "CPR", "MSK",
          "1.2.208.176.1.2", "CPR", null, null);

      // CoverageGroup
      assertId(pdcDocument.getCoverageGroup().getId(), "d54eeb91-726c-482d-a1c5-9abc47da1c93", "1.2.208.184", null);
      assertCode(pdcDocument.getCoverageGroup().getCode(), "CoverageGroup", "Sygesikringsgruppe", "1.2.208.184.100.1",
          "MedCom Message Codes");
      assertEquals("1", pdcDocument.getCoverageGroup().getCoverageGroup());
      assertAuthorRegisterEntered(pdcDocument.getCoverageGroup().getAuthorOfInformation(), "Sygesikringen", null,
          "1.2.208.176.2.7", "Sygesikringen", null, null);

      // OrganDonorRegistration
      assertId(pdcDocument.getOrganDonorRegistration().getId(), "6a4f70af-011e-48e8-9b53-949e4be40e3b", "1.2.208.184",
          null);
      assertCode(pdcDocument.getOrganDonorRegistration().getCode(), "OrganDonorRegistration", "Registreret organdonor",
          "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(true, pdcDocument.getOrganDonorRegistration().isRegistered());

      assertAuthorRegisterEntered(pdcDocument.getOrganDonorRegistration().getAuthorOfInformation(),
          "Dansk Center For Organdonation", null, "1.2.208.176.1.10", "CPR", null, null);

      // TreatmentWillRegistration
      assertId(pdcDocument.getTreatmentWillRegistration().getId(), "74d782a1-ac64-411a-8160-68fbf16dd2f8",
          "1.2.208.184", null);
      assertCode(pdcDocument.getTreatmentWillRegistration().getCode(), "TreatmentWillRegistration",
          "Registreret behandlingstestamente", "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(true, pdcDocument.getTreatmentWillRegistration().isRegistered());
      assertAuthorRegisterEntered(pdcDocument.getTreatmentWillRegistration().getAuthorOfInformation(),
          "Sundhedsdatastyrelsen", null, "1.2.208.176.1.9", "Sundhedsdatastyrelsen", null, null);

      // LivingWillRegistration
      assertId(pdcDocument.getLivingWillRegistration().getId(), "7a4d9bd3-f9d3-4e99-a02e-c59f6f3691da", "1.2.208.184",
          null);
      assertCode(pdcDocument.getLivingWillRegistration().getCode(), "LivingWillRegistration",
          "Registreret livstestamente", "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(false, pdcDocument.getLivingWillRegistration().isRegistered());
      assertAuthorRegisterEntered(pdcDocument.getLivingWillRegistration().getAuthorOfInformation(),
          "Sundhedsdatastyrelsen", null, "1.2.208.176.1.8", "Sundhedsdatastyrelsen", null, null);

      // NoResuscitation 
      assertId(pdcDocument.getNoResuscitationRegistration().getId(), "d90df5cb-602c-44d5-8cc6-9fb7ed9b8df9",
          "1.2.208.184", null);
      assertCode(pdcDocument.getNoResuscitationRegistration().getCode(), "NoResuscitationRegistration",
          "Registreret fravalg af genoplivningsforsøg v. hjertestop", "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(Registered.True, pdcDocument.getNoResuscitationRegistration().getRegistered());
      assertAuthorRegisterEntered(pdcDocument.getNoResuscitationRegistration().getAuthorOfInformation(),
          "Sundhedsdatastyrelsen", null, "1.2.208.176.1.8", "Sundhedsdatastyrelsen",
          new GregorianCalendar(2023, 10, 2, 10, 22, 56).getTime(), null);

      // ManuallyEnteredSpokenLanguage
      assertId(pdcDocument.getManuallyEnteredSpokenLanguage().getId(), "985deaf5-bc3a-4d7a-a513-ba70ed45dc6b",
          "1.2.208.184.15.7", "FSK");
      assertCode(pdcDocument.getManuallyEnteredSpokenLanguage().getCode(), "LanguageTypedIn", "Talt sprog, indtastet",
          "1.2.208.184.100.1", "MedCom Message Codes");
      assertCode(pdcDocument.getManuallyEnteredSpokenLanguage().getLanguageCode(), "de", "Deutsch", "1.0.639.1",
          "ISO-639-1");
      assertAuthorManualEntered(pdcDocument.getManuallyEnteredSpokenLanguage().getAuthorOfInformation(), null, "MSK",
          "1.2.208.176.1.2", "CPR", null,
          new PersonBuilder("Berggren").addGivenName("Max").addGivenName("Test").build(),
          new CodedValueBuilder()
              .setCode("EnteredCitizen")
              .setCodeSystem("1.2.208.184.100.1")
              .setDisplayName("Indtastet af borger")
              .setCodeSystemName("MedCom Message Codes")
              .build());
      // ManuallyEnteredTemporaryAddress
      assertId(pdcDocument.getManuallyEnteredTemporaryAddress().getId(), "690f19b0-661f-4048-ac55-0d7b48395a27",
          "1.2.208.184.15.2", null);
      assertCode(pdcDocument.getManuallyEnteredTemporaryAddress().getCode(), "TempAddrTypedIn",
          "Midlertidig adresse, indtastet", "1.2.208.184.100.1", "MedCom Message Codes");
      assertEquals(new GregorianCalendar(2023, 7, 8, 0, 0, 0).getTime(),
          pdcDocument.getManuallyEnteredTemporaryAddress().getEffectiveFrom());
      assertEquals(new GregorianCalendar(2025, 3, 19, 0, 0, 0).getTime(),
          pdcDocument.getManuallyEnteredTemporaryAddress().getEffectiveTo());
      assertEquals(1, pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet().length);
      assertEquals("Forskerparken 10",
          pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getStreet()[0]);
      assertEquals("Odense", pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getCity());
      assertEquals("5230", pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getPostalCode());
      assertEquals("Danmark", pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getCountry());
      assertEquals(AddressData.Use.HomeAddress,
          pdcDocument.getManuallyEnteredTemporaryAddress().getAddressData().getAddressUse());
      assertAuthorManualEntered(pdcDocument.getManuallyEnteredTemporaryAddress().getAuthorOfInformation(), null, "MSK",
          "1.2.208.176.1.2", "CPR", new GregorianCalendar(2023, 10, 8, 14, 03, 06).getTime(),
          new PersonBuilder("Berggren").addGivenName("Max").addGivenName("Test").build(),
          new CodedValueBuilder()
              .setCode("EnteredCitizen")
              .setCodeSystem("1.2.208.184.100.1")
              .setDisplayName("Indtastet af borger")
              .setCodeSystemName("MedCom Message Codes")
              .build());

      // ManuallyEnteredDentistInformation
      assertId(pdcDocument.getManuallyEnteredDentistInformation().getId(), "753ccb9e-eaec-40c0-948e-5726d397400b",
          "1.2.208.184.15.13", null);
      assertCode(pdcDocument.getManuallyEnteredDentistInformation().getCode(), "DentistTypedIn", "Tandlæge indtastet",
          "1.2.208.184.100.1", "MedCom Message Codes");
      assertId(pdcDocument.getManuallyEnteredDentistInformation().getIdentification(), "410845", "1.2.208.176.1.4",
          "Yderregisteret");
      assertEquals(1, pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet().length);
      assertEquals("Nivå Center 40 b",
          pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getStreet()[0]);
      assertEquals("Nivå", pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getCity());
      assertEquals("2990", pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getPostalCode());
      assertEquals("Danmark", pdcDocument.getManuallyEnteredDentistInformation().getAddressData().getCountry());
      assertPersonIdentity(pdcDocument.getManuallyEnteredDentistInformation().getPersonIdentity(),
          new PersonBuilder().addGivenName("Tandklinikken smil. nu").build());
      assertEquals("laege praksis", pdcDocument.getManuallyEnteredDentistInformation().getPracticeName());
      assertAuthorManualEntered(pdcDocument.getManuallyEnteredDentistInformation().getAuthorOfInformation(), null,
          "MSK", "1.2.208.176.1.2", "CPR", new GregorianCalendar(2023, 10, 8, 14, 3, 6).getTime(),
          new PersonBuilder("Berggren").addGivenName("Max").addGivenName("Test").build(),
          new CodedValueBuilder()
              .setCode("EnteredCitizen")
              .setCodeSystem("1.2.208.184.100.1")
              .setDisplayName("Indtastet af borger")
              .setCodeSystemName("MedCom Message Codes")
              .build());
      TelecomPhone[] dentistTelecomExpected = new TelecomPhone[2];
      dentistTelecomExpected[0] = new TelecomPhone(TelecomPhone.Use.Work, "tel", "80706050");
      dentistTelecomExpected[1] = new TelecomPhone(TelecomPhone.Use.Work, "tel", "80756555");
      assertTelecom(pdcDocument.getManuallyEnteredDentistInformation().getTelecomPhone(), dentistTelecomExpected);

      // ManuallyEnteredContactInformation
      assertId(pdcDocument.getManuallyEnteredContactInformation().getId(), "b297745a-d2cd-4957-895d-beffa06396da",
          "1.2.208.184", null);
      assertCode(pdcDocument.getManuallyEnteredContactInformation().getCode(), "PatientContactTypedIn",
          "Kontaktoplysninger, indtastet", "1.2.208.184.100.1", "MedCom Message Codes");
      TelecomPhone[] contentTelecomExpected = new TelecomPhone[3];
      contentTelecomExpected[0] = new TelecomPhone(TelecomPhone.Use.Home, "tel", "+4589898989");
      contentTelecomExpected[1] = new TelecomPhone(TelecomPhone.Use.Mobile, "tel", "98765432");
      contentTelecomExpected[2] = new TelecomPhone(TelecomPhone.Use.Work, "tel", "004586006500");
      assertTelecom(pdcDocument.getManuallyEnteredContactInformation().getTelecomPhone(), contentTelecomExpected);
      assertAuthorManualEntered(pdcDocument.getManuallyEnteredContactInformation().getAuthorOfInformation(), null,
          "MSK", "1.2.208.176.1.2", "CPR", new GregorianCalendar(2023, 10, 8, 14, 0, 11).getTime(),
          new PersonBuilder("Berggren").addGivenName("Max").addGivenName("Test").build(),
          new CodedValueBuilder()
              .setCode("EnteredCitizen")
              .setCodeSystem("1.2.208.184.100.1")
              .setDisplayName("Indtastet af borger")
              .setCodeSystemName("MedCom Message Codes")
              .build());

      // ManuallyEnteredInformationAboutRelatives
      assertEquals(2, pdcDocument.getManuallyEnteredInformationAboutRelativesList().size());

      // ManuallyEnteredInformationAboutRelatives 1
      idx = 0;
      assertId(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getId(),
          "23d21334-a665-40d7-b6f2-157bdeabea79", "1.2.208.184.15.3", null);
      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getCode(), "RelativeTypedIn",
          "Pårørende, indtastet", "1.2.208.184.100.1", "MedCom Message Codes");
      assertPersonIdentity(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getPersonIdentity(),
          new PersonBuilder("Berggren").addGivenName("Lise").build());
      TelecomPhone[] relativesTelecomExpected1 = new TelecomPhone[2];
      relativesTelecomExpected1[0] = new TelecomPhone(TelecomPhone.Use.Home, "tel", "004523232323");
      relativesTelecomExpected1[1] = new TelecomPhone(TelecomPhone.Use.Work, "tel", "96856523");
      assertTelecom(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getTelecomPhone(),
          relativesTelecomExpected1);
      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getRelationCode(),
          "uspec_paaroerende", "Uspecificeret pårørende", "1.2.208.184.100.2", "MedCom Relation Codes");
      assertEquals("Kommentar til Lise",
          pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getNote());

      assertAuthorManualEntered(
          pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getAuthorOfInformation(),
          "Plejecenter Herlev", "953741000016009", "1.2.208.176.1.1", "SOR",
          new GregorianCalendar(2023, 7, 18, 11, 23, 14).getTime(), null,
          new CodedValueBuilder()
              .setCode("EnteredHealthcareprofessional")
              .setCodeSystem("1.2.208.184.100.1")
              .setDisplayName("Indtastet af sundhedsprofessionel")
              .setCodeSystemName("MedCom Message Codes")
              .build());

      // ManuallyEnteredInformationAboutRelatives 2
      idx = 1;
      assertId(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getId(),
          "657e7afa-6753-4625-ba2e-a032b48d21d7", "1.2.208.184.15.3", null);
      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getCode(), "RelativeTypedIn",
          "Pårørende, indtastet", "1.2.208.184.100.1", "MedCom Message Codes");
      assertPersonIdentity(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getPersonIdentity(),
          new PersonBuilder("Berggren").addGivenName("Bente Test").build());
      TelecomPhone[] relativesTelecomExpected2 = new TelecomPhone[2];
      relativesTelecomExpected2[0] = new TelecomPhone(TelecomPhone.Use.Home, "tel", "+4589898989");
      relativesTelecomExpected2[1] = new TelecomPhone(TelecomPhone.Use.Work, "tel", "96856523");
      assertTelecom(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getTelecomPhone(),
          relativesTelecomExpected2);
      assertCode(pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getRelationCode(),
          "uspec_paaroerende", "Uspecificeret pårørende", "1.2.208.184.100.2", "MedCom Relation Codes");
      assertEquals("Dette er en \"prøve\" på 'nogle' særlige <tegn> ; til test & certificering @ / \\ ^ * .”",
          pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getNote());

      assertAuthorManualEntered(
          pdcDocument.getManuallyEnteredInformationAboutRelativesList().get(idx).getAuthorOfInformation(), null, "MSK",
          "1.2.208.176.1.2", "CPR", new GregorianCalendar(2023, 7, 18, 11, 22, 1).getTime(),
          new PersonBuilder("Berggren").addGivenName("Bente").addGivenName("Test").build(),
          new CodedValueBuilder()
              .setCode("EnteredCitizenRelative")
              .setCodeSystem("1.2.208.184.100.1")
              .setDisplayName("Indtastet af borgers pårørende")
              .setCodeSystemName("MedCom Message Codes")
              .build());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  private void assertId(ID id, String extensionExpected, String rootExpected, String authorityNameExpected) {
    assertEquals(extensionExpected, id.getExtension());
    assertEquals(rootExpected, id.getRoot());
    assertEquals(authorityNameExpected, id.getAuthorityName());

  }

  private void assertCode(CodedValue code, String codeExpected, String displayNameExpected, String codeSystemExpected,
      String codeSystemNameExpected) {
    assertEquals(codeExpected, code.getCode());
    assertEquals(displayNameExpected, code.getDisplayName());
    assertEquals(codeSystemExpected, code.getCodeSystem());
    assertEquals(codeSystemNameExpected, code.getCodeSystemName());
  }

  private void assertAuthorManualEntered(AuthorOfInformation authorOfInformation,
      String representedOrganizationNameExpected, String extensionExpected, String rootExpected,
      String assigningAuthorityNameExpected, Date dateExpected, PersonIdentity personExpected,
      CodedValue codeExpected) {

    assertEquals(SourceOfInformation.ManuallyEntered, authorOfInformation.getSourceOfInformation());

    assertCode(authorOfInformation.getCode(), codeExpected.getCode(), codeExpected.getDisplayName(),
        codeExpected.getCodeSystem(), codeExpected.getCodeSystemName());
    assertEquals(dateExpected, authorOfInformation.getTime());

    assertEquals(extensionExpected, authorOfInformation.getId().getExtension());
    assertEquals(rootExpected, authorOfInformation.getId().getRoot());
    assertEquals(assigningAuthorityNameExpected, authorOfInformation.getId().getAuthorityName());

    assertEquals(representedOrganizationNameExpected, authorOfInformation.getRepresentedOrganizationName());

    if (personExpected != null) {
      assertPersonIdentity(authorOfInformation.getAssignedPerson(), personExpected);
    } else {
      assertNull(authorOfInformation.getAssignedPerson());
    }

  }

  private void assertAuthorRegisterEntered(AuthorOfInformation authorOfInformation,
      String representedOrganizationNameExpected, String extensionExpected, String rootExpected,
      String assigningAuthorityNameExpected, Date dateExpected, PersonIdentity personExpected) {

    assertEquals(SourceOfInformation.FromRegister, authorOfInformation.getSourceOfInformation());

    assertEquals(dateExpected, authorOfInformation.getTime());

    assertEquals(extensionExpected, authorOfInformation.getId().getExtension());
    assertEquals(rootExpected, authorOfInformation.getId().getRoot());
    assertEquals(assigningAuthorityNameExpected, authorOfInformation.getId().getAuthorityName());

    assertEquals(representedOrganizationNameExpected, authorOfInformation.getRepresentedOrganizationName());

    if (personExpected != null) {
      assertEquals(personExpected.getGivenNames()[0], authorOfInformation.getAssignedPerson().getGivenNames()[0]);
      assertEquals(personExpected.getGivenNames()[1], authorOfInformation.getAssignedPerson().getGivenNames()[1]);
      assertEquals(personExpected.getFamilyName(), authorOfInformation.getAssignedPerson().getFamilyName());
    } else {
      assertNull(authorOfInformation.getAssignedPerson());
    }

  }

  private void assertPersonIdentity(PersonIdentity personIdentity, PersonIdentity personIdentityExpected) {
    assertEquals(personIdentityExpected.getGivenNames().length, personIdentity.getGivenNames().length);
    assertEquals(personIdentityExpected.getGivenNames()[0], personIdentity.getGivenNames()[0]);
    if (personIdentityExpected.getGivenNames().length > 1) {
      assertEquals(personIdentityExpected.getGivenNames()[1], personIdentityExpected.getGivenNames()[1]);
    }
    if (personIdentityExpected.getGivenNames().length > 2) {
      assertEquals(personIdentityExpected.getGivenNames()[2], personIdentityExpected.getGivenNames()[2]);
    }

    assertEquals(personIdentityExpected.getFamilyName(), personIdentity.getFamilyName());

    assertEquals(personIdentityExpected.getPrefix(), personIdentity.getPrefix());

  }

  private void assertTelecom(TelecomPhone[] telecoms, TelecomPhone[] telecomExpected) {
    assertNotNull(telecoms);
    assertEquals(telecomExpected.length, telecoms.length);
    for (int i = 0; i < telecomExpected.length; i++) {
      assertEquals(telecomExpected[i].getUse(), telecoms[i].getUse());
      assertEquals(telecomExpected[i].getProtocol(), telecoms[i].getProtocol());
      assertEquals(telecomExpected[i].getValue(), telecoms[i].getValue());
    }

  }

  @Override
  public void runTest() throws Exception {
    testPdcXMLConverterMedcomExampleMinimum();
  }

}
