package dk.s4.hl7.cda.convert.encode.phmr.v20;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.codes.NPU;
import dk.s4.hl7.cda.codes.UCUM;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.*;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.v20.MeasurementGroup;
import dk.s4.hl7.cda.model.phmr.v20.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

import java.util.Calendar;
import java.util.Date;

/**
 * Helper methods to create SimpleClinicalDocuments that match those of MedCom's
 * KOL example 1.
 *
 */
public class SetupExample3 {

  // example with 2 mobilecontacts in every telecom list, except for custodian, who can only have one

  public static PHMRDocument defineAsCDA() {
    PHMRDocument cda = defineAsCDAWitoutMedicalEquipment();
    return cda;
  }

  /**
   * Define a CDA for the Medcom example 1 but without medical equipment
   * section.
   */
  public static PHMRDocument defineAsCDAWitoutMedicalEquipment() {

    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTimeWithTimeZone(2016, 5, 9, 14, 50, 10);

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("021ba3bd-6935-45ca-bdcd-ed77f8b2ee2a")
        .setRoot(MedCom.ROOT_OID)
        .build();
    PHMRDocument cda = new PHMRDocument(idHeader);

    Patient nancyMobile = new Patient.PatientBuilder("Berggren")
        .setGender(Patient.Gender.Female)
        .addGivenName("Nancy")
        .addGivenName("Ann")
        .setSSN("2512484916")
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .setCountry("Danmark")
            .addAddressLine("Skovvejen 12")
            .addAddressLine("Landet")
            .setUse(AddressData.Use.HomeAddress)
            .build())
        .addTelecom(Use.MobileContact, "tel", "65123456")
        .addTelecom(Use.MobileContact, "tel", "65123457")
        .addTelecom(AddressData.Use.WorkPlace, "mailto", "nab@udkantsdanmark.dk")
        .setBirthTime(1948, Calendar.DECEMBER, 25)
        .build();

    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    cda.setPatient(nancyMobile);

    cda.setTitle("Hjemmemonitorering for " + nancy.getIdValue());
    cda.setLanguageCode("da-DK");

    // 1.1 Populate with time and version info
    cda.setEffectiveTime(documentCreationTime);

    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    OrganizationIdentity svendborgHjerteMedicinskAfdeling = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("241301000016007")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.MobileContact, "tel", "65112233")
        .addTelecom(Use.MobileContact, "tel", "65112234")
        .build();

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .addTelecom(Use.MobileContact, "tel", "75123456")
        .addTelecom(Use.MobileContact, "tel", "75123457")
        .addTelecom(AddressData.Use.WorkPlace, "mailto", "ou@udkantsdanmark.dk")
        .build();

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization

    cda.setAuthor(new ParticipantBuilder()
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .setTelecomList(svendborgHjerteMedicinskAfdeling.getTelecomList())
        .setSOR(svendborgHjerteMedicinskAfdeling.getIdValue())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());
    cda.setCustodian(new OrganizationIdentity.OrganizationBuilder()
        .setSOR("241301000016007")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.MobileContact, "tel", "65112233")
        //            .addTelecom(Use.MobileContact, "tel", "65112234") // custodian can only have 1 telecom
        .build());
    Date at1000onJan13 = DateUtil.makeDanishDateTimeWithTimeZone(2016, 5, 9, 14, 50, 10);
    cda.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .setTelecomList(svendborgHjerteMedicinskAfdeling.getTelecomList())
        .setSOR(svendborgHjerteMedicinskAfdeling.getIdValue())
        .setTime(at1000onJan13)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    cda.setDataEnterer(new Participant.ParticipantBuilder()
        .setAddress(svendborgHjerteMedicinskAfdeling.getAddress())
        .setSOR(svendborgHjerteMedicinskAfdeling.getIdValue())
        .setTelecomList(svendborgHjerteMedicinskAfdeling.getTelecomList())
        .setPersonIdentity(new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build())
        .setOrganizationIdentity(svendborgHjerteMedicinskAfdeling)
        .build());

    // 1.4 Define the service period
    Date from = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 6, 8, 2, 0);
    Date to = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 10, 8, 15, 0);
    cda.setDocumentationTimeInterval(from, to);

    // 1.6 Add measurements (observations)

    // Example 1: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.
    ID id = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("021ba3bd-6935-45ca-bdcd-ed77f8b2ee2a")
        .setRoot(MedCom.ROOT_OID)
        .build();

    Date time1 = DateUtil.makeDanishDateTimeWithTimeZone(2016, 5, 9, 12, 10, 10);
    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.Electronically,
        DataInputContext.PerformerType.Citizen);
    Measurement sat1 = NPU.createSaturation("0.97", time1, context, id);
    cda.addVitalSignGroups(new MeasurementGroup(sat1, time1, Measurement.Status.COMPLETED));

    // Use the basic methods that allow any legal
    // code system to be used but requires all data to be
    // provided
    Date time2 = DateUtil.makeDanishDateTimeWithTimeZone(2016, 5, 9, 12, 15, 10);
    Measurement sat2 = new Measurement.MeasurementBuilder(time2, Measurement.Status.COMPLETED)
        .setPhysicalQuantity("0.92", UCUM.NA, NPU.SATURATION_CODE, NPU.SATURATION_DISPLAYNAME)
        .setContext(context)
        .setId(id)
        .addReferenceRange(new ReferenceRange.ReferenceRangeBuilder()
            .setCode(new CodedValueBuilder()
                .setCode(MedCom.DK_OBSERVATION_RANGE_RED_ALERT)
                .setCodeSystem(MedCom.MESSAGECODE_OID)
                .setDisplayName(MedCom.DK_OBSERVATION_RANGE_RED_ALERT_DISPLAYNAME)
                .setCodeSystemName(MedCom.MESSAGECODE_DISPLAYNAME)
                .build())
            .setLowValue("0.88", true)
            .setHighValue(null, false)
            .build())
        .addReferenceRange(new ReferenceRange.ReferenceRangeBuilder()
            .setCode(new CodedValueBuilder()
                .setCode(MedCom.DK_OBSERVATION_RANGE_YELLOW_ALERT)
                .setCodeSystem(MedCom.MESSAGECODE_OID)
                .setDisplayName(MedCom.DK_OBSERVATION_RANGE_YELLOW_ALERT_DISPLAYNAME)
                .setCodeSystemName(MedCom.MESSAGECODE_DISPLAYNAME)
                .build())
            .setLowValue("0.92", true)
            .setHighValue(null, false)
            .build())
        .build();
    cda.addVitalSignGroups(new MeasurementGroup(sat2, time2, Measurement.Status.COMPLETED));

    Date time3 = DateUtil.makeDanishDateTimeWithTimeZone(2016, 5, 9, 12, 30, 10);
    Measurement sat3 = new Measurement.MeasurementBuilder(time3, Measurement.Status.COMPLETED)
        .setPhysicalQuantity("0.95", UCUM.NA, NPU.SATURATION_CODE, NPU.SATURATION_DISPLAYNAME)
        .setContext(context)
        .setId(id)
        .build();
    cda.addVitalSignGroups(new MeasurementGroup(sat3, time3, Measurement.Status.COMPLETED));

    return cda;
  }
}
