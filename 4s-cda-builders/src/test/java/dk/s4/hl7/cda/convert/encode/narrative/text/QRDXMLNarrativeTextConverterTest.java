package dk.s4.hl7.cda.convert.encode.narrative.text;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse.QRDNumericResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer.QRDOrganizerBuilder;

public class QRDXMLNarrativeTextConverterTest {

  @Test
  public void checkValueIsTheSame() {

    String narrativeTextInput = "narrativeText";

    QRDXMLNarrativeTextConverter qrdXMLNarrativeTextConverter = new QRDXMLNarrativeTextConverter();

    Section<QRDOrganizer> sectionNumeric = new Section<QRDOrganizer>("Title", narrativeTextInput);
    sectionNumeric.addOrganizer(new QRDOrganizerBuilder()
        .addQRDResponse(new QRDNumericResponseBuilder().setValue("2", BasicType.INT).build())
        .build());

    String narrativeTextResult = qrdXMLNarrativeTextConverter.createText(sectionNumeric);

    assertEquals(narrativeTextInput, narrativeTextResult);

  }

}
