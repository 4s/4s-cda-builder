package dk.s4.hl7.cda.convert.encode.qrd;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.UUID;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.encode.CDAExample;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity.OrganizationBuilder;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.QuestionnaireMedia.QuestionnaireMediaBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.QuestionnaireMedia;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public abstract class QRDExampleBase implements CDAExample<QRDDocument> {
  protected QRDDocument createBaseQRDDocument() {
    return createBaseQRDDocument(UUID.randomUUID().toString());
  }

  protected QRDDocument createBaseQRDDocument(String documentId) {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 13, 10, 0, 0);
    // Create document
    QRDDocument qrdDocument = new QRDDocument(MedCom.createId(documentId));
    qrdDocument.setLanguageCode("da-DK");
    qrdDocument.setTitle("KOL spørgeskema");
    qrdDocument.setDocumentVersion(new IDBuilder().setRoot(MedCom.MESSAGECODE_OID).setExtension("2358344").build(), 1);
    qrdDocument.setEffectiveTime(documentCreationTime);
    // Create Patient
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    qrdDocument.setPatient(nancy);
    // Create Custodian organization
    qrdDocument.setCustodian(new OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build());

    // Author is the patient self
    qrdDocument.setAuthor(new ParticipantBuilder()
        .setAddress(nancy.getAddress())
        .setId(nancy.getId())
        .setTelecomList(nancy.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(nancy)
        .build());

    // 1.4 Define the service period
    Date from = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 6, 8, 2, 0);
    Date to = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 10, 8, 15, 0);
    qrdDocument.setDocumentationTimeInterval(from, to);
    return qrdDocument;
  }

  protected QuestionnaireMedia getMedia() throws IOException, URISyntaxException {
    String image = FileUtil.getData(this.getClass(), "qfdd/imageencoded.txt");
    return new QuestionnaireMediaBuilder()
        .representation("B64")
        .mediaType("image/png")
        .value(image)
        .id("fememotes-400_dxt")
        .build();
  }
}
