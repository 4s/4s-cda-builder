package dk.s4.hl7.cda.convert.encode.qrd;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Reference.DocumentIdReferencesUse;
import dk.s4.hl7.cda.model.Reference.ReferenceBuilder;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse.QRDAnalogSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer.QRDOrganizerBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;
import dk.s4.hl7.cda.model.qrd.QRDResponse;

/**
 * Test Analogslider
 * 
 * @author Frank Jacobsen, Systematic
 */
public final class SetupQRDAnalogSliderExample extends QRDExampleBase {
  private static final String QUESTION = "question";
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";

  private QRDResponse responseWithReference() {
    return new QRDAnalogSliderResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "3", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setValue("4", BasicType.INT.name())
        .setInterval("1", "100", "5")
        .addReference(simpleDocument())
        .build();
  }

  private Reference simpleDocument() {
    return new ReferenceBuilder(DocumentIdReferencesUse.CDA_DOCUMENT_ID_REFERENCE.getReferencesUse(),
        MedCom.createId("referenceId"), Loinc.createPHMRTypeCode()).build();
  }

  protected QRDAnalogSliderResponse simpleResponse() {
    return new QRDAnalogSliderResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "2", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setValue("2", BasicType.INT.name())
        .setInterval("1", "100", "1")
        .build();
  }

  @Override
  public QRDDocument createDocument() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);

    Section<QRDOrganizer> section = new Section<QRDOrganizer>("Questions", "Questions");
    section.addOrganizer(new QRDOrganizerBuilder()
        .addId(MedCom.createId("idExtensionOrganizer"))
        .addQRDResponse(simpleResponse())
        .addQRDResponse(responseWithReference())
        .build());
    cda.addSection(section);
    return cda;
  }
}