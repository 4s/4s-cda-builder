package dk.s4.hl7.cda.convert.encode.apd;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import dk.s4.hl7.cda.convert.APDXmlCodec;
import dk.s4.hl7.cda.convert.APDXmlCodec.Version;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.StatusV10;

/**
 * Validates various aspects of the XML generation 
 *
 * Will be extended when builder/parser is adjusted
 */
public final class TestApdBuildVariations extends BaseEncodeTest<AppointmentDocument> {

  private static final APDXmlCodec APD_XML_CODEC_V10 = new APDXmlCodec(Version.V10);
  private static final APDXmlCodec APD_XML_CODEC_V11 = new APDXmlCodec(Version.V11);

  public TestApdBuildVariations() {
    super("src/test/resources/apd/v20/");
  }

  @Test
  public void testServiceEventEffectTimeHighNotNullV10() throws Exception {
    AppointmentDocument appointmentDocument = SetupMedcomExample.createBaseAppointmentDocument(StatusV10.ACTIVE);
    String appointmentXml = encodeDocument(APD_XML_CODEC_V10, appointmentDocument);

    assertNotNull(appointmentXml);
    assertTrue(appointmentXml.contains("<low value=\"20170531110000+0200\"/>"));
    assertTrue(appointmentXml.contains("<high value=\"20170531120000+0200\"/>"));

  }

  @Test
  public void testServiceEventEffectTimeHighNullV10() throws Exception {
    AppointmentDocument appointmentDocument = SetupMedcomExample.createBaseAppointmentDocument(StatusV10.ACTIVE);
    appointmentDocument.setDocumentationTimeInterval(appointmentDocument.getServiceStartTime(), null);
    String appointmentXml = encodeDocument(APD_XML_CODEC_V10, appointmentDocument);

    assertNotNull(appointmentXml);
    assertTrue(appointmentXml.contains("<low value=\"20170531110000+0200\"/>"));
    assertTrue(appointmentXml.contains("<high nullFlavor=\"NI\"/>"));

  }

  @Test
  public void testServiceEventEffectTimeHighNotNullV11() throws Exception {
    AppointmentDocument appointmentDocument = SetupMedcomExample.createBaseAppointmentDocument(StatusV10.ACTIVE);
    String appointmentXml = encodeDocument(APD_XML_CODEC_V11, appointmentDocument);

    assertNotNull(appointmentXml);
    assertTrue(appointmentXml.contains("<low value=\"20170531110000+0200\"/>"));
    assertTrue(appointmentXml.contains("<high value=\"20170531120000+0200\"/>"));

  }

  @Test
  public void testServiceEventEffectTimeHighNullV11() throws Exception {
    AppointmentDocument appointmentDocument = SetupMedcomExample.createBaseAppointmentDocument(StatusV10.ACTIVE);
    appointmentDocument.setDocumentationTimeInterval(appointmentDocument.getServiceStartTime(), null);
    String appointmentXml = encodeDocument(APD_XML_CODEC_V11, appointmentDocument);

    assertNotNull(appointmentXml);
    assertTrue(appointmentXml.contains("<low value=\"20170531110000+0200\"/>"));
    assertTrue(appointmentXml.contains("<high nullFlavor=\"NI\"/>"));

  }

}