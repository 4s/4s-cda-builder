package dk.s4.hl7.cda.convert.encode.qrd;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dk.s4.hl7.cda.model.*;
import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.QRDXmlCodec;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.convert.encode.narrative.text.QRDXMLNarrativeTextReplaceResponseConverter;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.Reference.ReferenceBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer.QRDOrganizerBuilder;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public final class TestQRDVariationsExamplesBuildAndParse extends BaseEncodeTest<QRDDocument> {

  public TestQRDVariationsExamplesBuildAndParse() {
    super("src/test/resources/qrd");
  }

  @Before
  public void before() {
    setCodec(new QRDXmlCodec());
  }

  @Test
  public void testNullValueForTitleIsValidBuildAndParse() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    String xmlDocument = encodeDocument(new SetupQRDVariationsExample().createDocumentTitleNull());
    QRDDocument qrdDocument = codec.decode(xmlDocument);

    assertNotNull(qrdDocument);
    assertNotNull(qrdDocument.getSections());
    assertEquals(2, qrdDocument.getSections().size());
    assertNotNull(qrdDocument.getSections().get(0));
    assertNull(qrdDocument.getSections().get(0).getTitle());
    assertNotNull(qrdDocument.getSections().get(1));
    assertNull(qrdDocument.getSections().get(1).getTitle());

  }

  @Test
  public void testReferenceRangeIsNullForNumericQuestionIsValidBuildAndParse() throws Exception {

    QRDXmlCodec codec = new QRDXmlCodec();
    String xmlDocument = encodeDocument(new SetupQRDVariationsExample().createDocumentNumericResponseNoInterval());
    QRDDocument qrdDocument = codec.decode(xmlDocument);

    assertNotNull(qrdDocument);
    assertNotNull(qrdDocument.getSections());

    assertEquals(1, qrdDocument.getSections().size());
    assertNotNull(qrdDocument.getSections().get(0));
    assertNotNull(qrdDocument.getSections().get(0).getOrganizers());
    assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0));
    assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses());
    assertEquals(1, qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().size());
    assertTrue(
        qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0) instanceof QRDNumericResponse);
    QRDNumericResponse qrdNumericResponse = (QRDNumericResponse) qrdDocument
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0);
    assertNull(qrdNumericResponse.getMaximum());
    assertNull(qrdNumericResponse.getMinimum());

  }

  @Test
  public void testReferenceInludeWihtReferenesUseBuildAndParse() throws Exception {

    // given
    QRDXmlCodec codec = new QRDXmlCodec();
    String referenceUse = "3";
    String externalDocumentExtension1 = "http://svn.medcom.dk/svn/drafts/QFDD/Test_Certificering/QFDD_complete_1.0.xml";
    String externalDocumentExtension2 = "a4062532-93f8-4313-bfc6-ff6e58530f4a";
    String externalDocumentExtension3observation = "a4062532-93f8-4313-bfc6-ff6e58531111";
    String externalObservationId = "123";
    String externalDocumentTypeCode = "74468-0";

    QRDDocument cda = new SetupQRDVariationsExample().createDocument();
    Section<QRDOrganizer> section = new Section<QRDOrganizer>("title", "Questions");

    Reference reference1 = createReference(externalDocumentExtension1, externalDocumentTypeCode, referenceUse, null);
    QRDResponse qrdResponse1 = new SetupQRDVariationsExample().responseWithReference("question", "simpleId", "text",
        reference1);

    Reference reference2 = createReference(externalDocumentExtension2, externalDocumentTypeCode, null, null);
    QRDResponse qrdResponse2 = new SetupQRDVariationsExample().responseWithReference("question", "simpleId", "text",
        reference2);

    Reference reference3 = createReference(externalDocumentExtension3observation, externalDocumentTypeCode,
        referenceUse, MedCom.createId(externalObservationId));
    QRDResponse qrdResponse3 = new SetupQRDVariationsExample().responseWithReference("question", "simpleId", "text",
        reference3);

    QRDOrganizerBuilder qrdOrganizerBuilder = new QRDOrganizerBuilder();
    qrdOrganizerBuilder.addQRDResponse(qrdResponse1);
    qrdOrganizerBuilder.addQRDResponse(qrdResponse2);
    qrdOrganizerBuilder.addQRDResponse(qrdResponse3);
    section.addOrganizer(qrdOrganizerBuilder.build());
    cda.addSection(section);

    // when
    String xmlDocument = encodeDocument(cda);
    QRDDocument qrdDocument = codec.decode(xmlDocument);

    // then
    assertNotNull(xmlDocument);
    assertTrue(xmlDocument
        .contains("<id root=\"" + MedCom.DK_REFERENCE_EXTERNAL_DOCUMENT + "\" extension=\"" + referenceUse + "\"/>"));
    assertTrue(xmlDocument.contains("<id root=\"1.2.208.184.5.3\" extension=\"" + externalDocumentExtension1 + "\"/>"));
    assertTrue(xmlDocument.contains("<id root=\"1.2.208.184.5.3\" extension=\"" + externalDocumentExtension2 + "\"/>"));
    assertTrue(
        xmlDocument.contains("<id root=\"" + MedCom.ROOT_OID + "\" extension=\"" + externalObservationId + "\"/>"));
    assertTrue(xmlDocument
        .contains("<id root=\"1.2.208.184.5.3\" extension=\"" + externalDocumentExtension3observation + "\"/>"));

    assertNotNull(qrdDocument);

    Reference referenceDecoded1 = getReference(0, qrdDocument);
    assertEquals(referenceUse, referenceDecoded1.getReferencesUse());
    assertEquals(externalDocumentExtension1, referenceDecoded1.getExternalDocument().getExtension());
    assertEquals(externalDocumentTypeCode, referenceDecoded1.getExternalDocumentType().getCode());

    Reference referenceDecoded2 = getReference(1, qrdDocument);
    assertNull(referenceDecoded2.getReferencesUse());
    assertEquals(externalDocumentExtension2, referenceDecoded2.getExternalDocument().getExtension());
    assertEquals(externalDocumentTypeCode, referenceDecoded2.getExternalDocumentType().getCode());

    Reference referenceDecoded3 = getReference(2, qrdDocument);
    assertEquals(referenceUse, referenceDecoded3.getReferencesUse());
    assertEquals(externalDocumentExtension3observation, referenceDecoded3.getExternalDocument().getExtension());
    assertEquals(externalDocumentTypeCode, referenceDecoded3.getExternalDocumentType().getCode());
    assertEquals(externalObservationId, referenceDecoded3.getExternalObservation().getExtension());

  }

  private Reference createReference(String externalDocumentExtension, String code, String referenceUse,
      ID referenceObservationId) {
    ReferenceBuilder referenceBuilder = new ReferenceBuilder(referenceUse,
        new IDBuilder()
            .setRoot("1.2.208.184.5.3")
            .setExtension(externalDocumentExtension)
            .setAuthorityName("MedCom")
            .build(),
        new CodedValue(code, "2.16.840.1.113883.6.1", "Questionnaire Form Definition Document", "LOINC"));
    if (referenceObservationId != null) {
      referenceBuilder.externalObservation(referenceObservationId);
    }
    return referenceBuilder.build();
  }

  private Reference getReference(int responseIdx, QRDDocument qrdDocument) {
    return qrdDocument
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(responseIdx)
        .getReferences()
        .get(0);
  }

  @Test
  public void testReferenceInludeWihtoutReferenesUseBuildAndParse() throws Exception {

    // given
    QRDXmlCodec codec = new QRDXmlCodec();
    String externalDocumentRoot = "2.16.840.1.113883.4.873";
    String externalDocumentExtension = "a4062532-93f8-4313-bfc6-ff6e58530f4a";
    String externalDocumentTypeCode = "74468-0";

    QRDDocument cda = new SetupQRDVariationsExample().createDocument();
    Section<QRDOrganizer> section = new Section<QRDOrganizer>("title", "Questions");

    Reference reference = new ReferenceBuilder(null,
        new IDBuilder()
            .setRoot(externalDocumentRoot)
            .setExtension(externalDocumentExtension)
            .setAuthorityName("MedCom")
            .build(),
        new CodedValue(externalDocumentTypeCode, "2.16.840.1.113883.6.1", "Questionnaire Form Definition Document",
            "LOINC")).build();
    QRDResponse qrdResponse = new SetupQRDVariationsExample().responseWithReference("question", "simpleId", "text",
        reference);
    section.addOrganizer(new QRDOrganizerBuilder().addQRDResponse(qrdResponse).build());
    cda.addSection(section);

    // when
    String xmlDocument = encodeDocument(cda);
    QRDDocument qrdDocument = codec.decode(xmlDocument);

    // then
    assertNotNull(xmlDocument);
    assertFalse(xmlDocument.contains("<id root=\"" + MedCom.DK_REFERENCE_EXTERNAL_DOCUMENT + "\""));
    assertTrue(xmlDocument
        .contains("<id root=\"" + externalDocumentRoot + "\" extension=\"" + externalDocumentExtension + "\"/>"));

    assertNotNull(qrdDocument);

    Reference referenceDecoded = qrdDocument
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0)
        .getReferences()
        .get(0);
    assertNull(referenceDecoded.getReferencesUse());
    assertEquals(externalDocumentExtension, referenceDecoded.getExternalDocument().getExtension());
    assertEquals(externalDocumentTypeCode, referenceDecoded.getExternalDocumentType().getCode());

  }

  @Test(expected = IllegalArgumentException.class)
  public void testReferenceRangeIsNullForAnalogSliderIsInvalidBuildAndParse() throws Exception {

    encodeDocument(new SetupQRDVariationsExample().createDocumentAnalogSliderResponseNoInterval());

  }

  @Test
  public void testNoDisplayNameIsValidWhenBuilded() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    String xmlDocument = encodeDocument(new SetupQRDVariationsExample().createDocumentResponseNoDisplayName());
    QRDDocument qrdDocument = codec.decode(xmlDocument);

    assertNotNull(qrdDocument);
    assertNotNull(qrdDocument.getSections());

    assertEquals(5, qrdDocument.getSections().size());

    assertNotNull(qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0).getCode());
    assertEquals("code",
        qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCode());
    assertEquals("codeSystem",
        qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCodeSystem());
    assertEquals("CodeSystemName",
        qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCodeSystemName());
    assertNull(
        qrdDocument.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0).getCode().getDisplayName());

    assertNotNull(qrdDocument.getSections().get(1).getOrganizers().get(0).getQRDResponses().get(0).getCode());
    assertEquals("code",
        qrdDocument.getSections().get(1).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCode());
    assertEquals("codeSystem",
        qrdDocument.getSections().get(1).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCodeSystem());
    assertEquals("CodeSystemName",
        qrdDocument.getSections().get(1).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCodeSystemName());
    assertNull(
        qrdDocument.getSections().get(1).getOrganizers().get(0).getQRDResponses().get(0).getCode().getDisplayName());

    assertNotNull(qrdDocument.getSections().get(2).getOrganizers().get(0).getQRDResponses().get(0).getCode());
    assertEquals("code",
        qrdDocument.getSections().get(2).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCode());
    assertEquals("codeSystem",
        qrdDocument.getSections().get(2).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCodeSystem());
    assertEquals("CodeSystemName",
        qrdDocument.getSections().get(2).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCodeSystemName());
    assertNull(
        qrdDocument.getSections().get(2).getOrganizers().get(0).getQRDResponses().get(0).getCode().getDisplayName());

    assertNotNull(qrdDocument.getSections().get(3).getOrganizers().get(0).getQRDResponses().get(0).getCode());
    assertEquals("code",
        qrdDocument.getSections().get(3).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCode());
    assertEquals("codeSystem",
        qrdDocument.getSections().get(3).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCodeSystem());
    assertEquals("CodeSystemName",
        qrdDocument.getSections().get(3).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCodeSystemName());
    assertNull(
        qrdDocument.getSections().get(3).getOrganizers().get(0).getQRDResponses().get(0).getCode().getDisplayName());

    assertNotNull(qrdDocument.getSections().get(4).getOrganizers().get(0).getQRDResponses().get(0).getCode());
    assertEquals("code",
        qrdDocument.getSections().get(4).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCode());
    assertEquals("codeSystem",
        qrdDocument.getSections().get(4).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCodeSystem());
    assertEquals("CodeSystemName",
        qrdDocument.getSections().get(4).getOrganizers().get(0).getQRDResponses().get(0).getCode().getCodeSystemName());
    assertNull(
        qrdDocument.getSections().get(4).getOrganizers().get(0).getQRDResponses().get(0).getCode().getDisplayName());

  }

  @Test
  public void testNarrativeTextExampleToPrepareCorrectSetup() throws Exception {
    performGeneratedTest("NarrativeTextSample.xml", new SetupQRDNarrativeTextExample().createDocument());
  }

  @Test
  public void testNarrativeTextNoReplace() throws Exception {

    int idxInfo = 0;
    int idx2081 = 1;
    int idx2070 = 2;
    int idx2074 = 3;
    int idxAnalogSlider = 4;
    //    int idxNumericInfo = 5;
    int idxNumeric = 6;
    int idxText = 7;
    int idxTextA = 8; //associated text response
    int idxDiscreteSlider = 9;

    String xmlDocument = encodeDocument(new SetupQRDNarrativeTextExample().createDocument());
    QRDXmlCodec codec = new QRDXmlCodec();
    QRDDocument qrdDocument = codec.decode(xmlDocument);

    assertNotNull(qrdDocument);
    assertNotNull(qrdDocument.getSections());

    assertEquals(10, qrdDocument.getSections().size());
    assertNotNull(qrdDocument.getSections().get(idxInfo));
    assertNotNull(qrdDocument.getSections().get(idx2081));
    assertNotNull(qrdDocument.getSections().get(idx2074));
    assertNotNull(qrdDocument.getSections().get(idxAnalogSlider));
    assertNotNull(qrdDocument.getSections().get(idxNumeric));
    assertNotNull(qrdDocument.getSections().get(idxTextA));
    assertNotNull(qrdDocument.getSections().get(idxDiscreteSlider));

    assertNotNull(qrdDocument.getSections().get(idxInfo).getText());
    assertTrue(qrdDocument.getSections().get(idxInfo).getText().contains("<list listType=\"unordered\">"));
    assertTrue(xmlDocument.contains("<list listType=\"unordered\">"));

    assertNotNull(qrdDocument.getSections().get(idx2081).getText());
    assertTrue(qrdDocument.getSections().get(idx2081).getText().contains("<td><!-- A2200_1 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2081).getText().contains("<td><!--A2200_2 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2200_1 --></td>"));
    assertTrue(xmlDocument.contains("<td><!--A2200_2 --></td>"));

    assertNotNull(qrdDocument.getSections().get(idx2070).getText());
    assertTrue(qrdDocument.getSections().get(idx2070).getText().contains("<td><!-- A30_1 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2070).getText().contains("<td><!-- A30_2 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2070).getText().contains("<td><!-- A30_3 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2070).getText().contains("<td><!-- A30_4 --></td>"));

    assertTrue(xmlDocument.contains("<td><!-- A30_1 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A30_2 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A30_3 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A30_4 --></td>"));

    assertNotNull(qrdDocument.getSections().get(idx2074).getText());
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2203_1 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2203_2 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2203_3 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2203_4 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2204_1 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2204_2 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2204_3 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2204_4 --></td>"));

    assertTrue(xmlDocument.contains("<td><!-- A2203_1 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2203_2 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2203_3 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2203_4 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2204_1 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2204_2 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2204_3 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2204_4 --></td>"));

    assertNotNull(qrdDocument.getSections().get(idxAnalogSlider).getText());
    assertTrue(qrdDocument
        .getSections()
        .get(idxAnalogSlider)
        .getText()
        .contains("<content styleCode=\"Bold\"><!-- Q2595 --></content>"));
    assertTrue(xmlDocument.contains("<content styleCode=\"Bold\"><!-- Q2595 --></content>"));

    assertNotNull(qrdDocument.getSections().get(idxNumeric).getText());
    assertTrue(qrdDocument
        .getSections()
        .get(idxNumeric)
        .getText()
        .contains("<content styleCode=\"Bold\"><!-- Q2552 --></content>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxNumeric)
        .getText()
        .contains("<content styleCode=\"Bold\"><!-- Q2553 --></content>"));
    assertTrue(xmlDocument.contains("<content styleCode=\"Bold\"><!-- Q2552 --></content>"));
    assertTrue(xmlDocument.contains("<content styleCode=\"Bold\"><!-- Q2553 --></content>"));

    assertNotNull(qrdDocument.getSections().get(idxText).getText());
    assertTrue(qrdDocument.getSections().get(idxText).getText().contains("<td><!--A173_1--></td>"));
    assertTrue(qrdDocument.getSections().get(idxText).getText().contains("<td><!-- A173_2 --></td>"));
    assertTrue(qrdDocument.getSections().get(idxText).getText().contains("<td><!-- A173_3 --></td>"));
    assertTrue(qrdDocument.getSections().get(idxText).getText().contains("<td><!-- Q174 --></td>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxText)
        .getText()
        .contains("<tr colspan=\"2\" align=\"left\"><!-- Q243 --></tr>"));
    assertTrue(xmlDocument.contains("<td><!--A173_1--></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A173_2 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A173_3 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- Q174 --></td>"));
    assertTrue(xmlDocument.contains("<tr colspan=\"2\" align=\"left\"><!-- Q243 --></tr>"));

    assertNotNull(qrdDocument.getSections().get(idxTextA).getText());
    assertTrue(qrdDocument.getSections().get(idxTextA).getText().contains("<td><!--A173_1a--></td>"));
    assertTrue(qrdDocument.getSections().get(idxTextA).getText().contains("<td><!-- A173_2a --></td>"));
    assertTrue(qrdDocument.getSections().get(idxTextA).getText().contains("<td><!-- A173_3a --></td>"));
    assertTrue(qrdDocument.getSections().get(idxTextA).getText().contains("<td><!-- Q174a --></td>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxTextA)
        .getText()
        .contains("<tr colspan=\"2\" align=\"left\"><!-- Q243a --></tr>"));
    assertTrue(xmlDocument.contains("<td><!--A173_1a--></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A173_2a --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A173_3a --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- Q174a --></td>"));
    assertTrue(xmlDocument.contains("<tr colspan=\"2\" align=\"left\"><!-- Q243a --></tr>"));

    assertNotNull(qrdDocument.getSections().get(idxDiscreteSlider).getText());
    assertTrue(qrdDocument
        .getSections()
        .get(idxDiscreteSlider)
        .getText()
        .contains("<td styleCode=\"Bold\"><!-- A30_1 --></td>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxDiscreteSlider)
        .getText()
        .contains("<td styleCode=\"Bold\"><!-- A30_2 --></td>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxDiscreteSlider)
        .getText()
        .contains("<td styleCode=\"Bold\"><!-- A30_3 --></td>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxDiscreteSlider)
        .getText()
        .contains("<td styleCode=\"Bold\"><!-- A30_4 --></td>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxDiscreteSlider)
        .getText()
        .contains("<td styleCode=\"Bold\"><!-- A30_5 --></td>"));
    assertTrue(xmlDocument.contains("<td styleCode=\"Bold\"><!-- A30_1 --></td>"));
    assertTrue(xmlDocument.contains("<td styleCode=\"Bold\"><!-- A30_2 --></td>"));
    assertTrue(xmlDocument.contains("<td styleCode=\"Bold\"><!-- A30_3 --></td>"));
    assertTrue(xmlDocument.contains("<td styleCode=\"Bold\"><!-- A30_4 --></td>"));
    assertTrue(xmlDocument.contains("<td styleCode=\"Bold\"><!-- A30_5 --></td>"));

  }

  @Test
  public void testNarrativeTextReplaceBasedOnResponses() throws Exception {

    int idxInfo = 0;
    int idx2081 = 1;
    int idx2070 = 2;
    int idx2074 = 3;
    int idxAnalogSlider = 4;
    //    int idxNumericInfo = 5;
    int idxNumeric = 6;
    int idxText = 7;
    int idxTextA = 8; //associated text response
    int idxDiscreteSlider1 = 9;
    int idxDiscreteSlider2 = 10;

    setCodec(new QRDXmlCodec(new QRDXMLNarrativeTextReplaceResponseConverter()));
    QRDDocument newQRDDocument = new SetupQRDNarrativeTextExample().createDocument();
    newQRDDocument
        .addSection(new SetupQRDNarrativeTextExample().setupQRDOrganizerWithDiscreteSliderAndAssociatedTextResponse());

    String xmlDocument = encodeDocument(newQRDDocument);
    QRDXmlCodec codec = new QRDXmlCodec();
    QRDDocument qrdDocument = codec.decode(xmlDocument);

    assertNotNull(qrdDocument);
    assertNotNull(qrdDocument.getSections());

    assertEquals(11, qrdDocument.getSections().size());
    assertNotNull(qrdDocument.getSections().get(idxInfo));
    assertNotNull(qrdDocument.getSections().get(idx2081));
    assertNotNull(qrdDocument.getSections().get(idx2074));
    assertNotNull(qrdDocument.getSections().get(idxAnalogSlider));
    assertNotNull(qrdDocument.getSections().get(idxNumeric));
    assertNotNull(qrdDocument.getSections().get(idxTextA));
    assertNotNull(qrdDocument.getSections().get(idxDiscreteSlider1));

    assertNotNull(qrdDocument.getSections().get(idxInfo).getText());
    assertTrue(qrdDocument.getSections().get(idxInfo).getText().contains("<list listType=\"unordered\">"));
    assertTrue(xmlDocument.contains("<list listType=\"unordered\">"));

    //Multiple choice response is: code="A2200_2" displayName="Kvinde"
    assertNotNull(qrdDocument.getSections().get(idx2081).getText());
    assertTrue(qrdDocument.getSections().get(idx2081).getText().contains("<td><!-- A2200_1 --></td>"));
    assertFalse(qrdDocument.getSections().get(idx2081).getText().contains("<td><!-- A2200_2 --></td>"));

    assertEquals(1, countOccurences(qrdDocument.getSections().get(idx2081).getText(), "<td>X</td>"));

    assertTrue(xmlDocument.contains("<td><!-- A2200_1 --></td>"));
    assertFalse(xmlDocument.contains("<td><!-- A2200_2 --></td>"));

    //Multiple choice response is: code="A30_2" displayName="Vældig godt" and code="A30_4" displayName="Mindre godt"
    assertNotNull(qrdDocument.getSections().get(idx2070).getText());
    assertTrue(qrdDocument.getSections().get(idx2070).getText().contains("<td><!-- A30_1 --></td>"));
    assertFalse(qrdDocument.getSections().get(idx2070).getText().contains("<td><!-- A30_2 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2070).getText().contains("<td><!-- A30_3 --></td>"));
    assertFalse(qrdDocument.getSections().get(idx2070).getText().contains("<td><!-- A30_4 --></td>"));

    assertEquals(2, countOccurences(qrdDocument.getSections().get(idx2070).getText(), "<td>X</td>"));

    assertTrue(xmlDocument.contains("<td><!-- A30_1 --></td>"));
    assertFalse(xmlDocument.contains("<td><!-- A30_2 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A30_3 --></td>"));
    assertFalse(xmlDocument.contains("<td><!-- A30_4 --></td>"));

    //Multiple choice response 1 is: code="A2203_2" displayName="Ja, en gang imellem"
    //Multiple choice response 2 is: code="A2204_3" displayName="Ja, nogle gange"
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2203_1 --></td>"));
    assertFalse(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2203_2 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2203_3 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2203_4 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2204_1 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2204_2 --></td>"));
    assertFalse(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2204_3 --></td>"));
    assertTrue(qrdDocument.getSections().get(idx2074).getText().contains("<td><!-- A2204_4 --></td>"));

    assertEquals(2, countOccurences(qrdDocument.getSections().get(idx2074).getText(), "<td>X</td>"));

    assertTrue(xmlDocument.contains("<td><!-- A2203_1 --></td>"));
    assertFalse(xmlDocument.contains("<td><!-- A2203_2 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2203_3 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2203_4 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2204_1 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2204_2 --></td>"));
    assertFalse(xmlDocument.contains("<td><!-- A2204_3 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A2204_4 --></td>"));

    //Multiple choice check total replace with X
    assertEquals(7, countOccurences(xmlDocument, "<td>X</td>"));

    //Analog slider, response 4
    assertNotNull(qrdDocument.getSections().get(idxAnalogSlider).getText());
    assertFalse(qrdDocument
        .getSections()
        .get(idxAnalogSlider)
        .getText()
        .contains("<content styleCode=\"Bold\"><!-- Q2595 --></content>"));
    assertTrue(
        qrdDocument.getSections().get(idxAnalogSlider).getText().contains("<content styleCode=\"Bold\">4</content>"));
    assertFalse(xmlDocument.contains("<content styleCode=\"Bold\"><!-- Q2595 --></content>"));
    assertTrue(xmlDocument.contains("<content styleCode=\"Bold\">4</content>"));

    //Numeric, response 1 og 2
    assertNotNull(qrdDocument.getSections().get(idxNumeric).getText());
    assertFalse(qrdDocument
        .getSections()
        .get(idxNumeric)
        .getText()
        .contains("<content styleCode=\"Bold\"><!-- Q2552 --></content>"));
    assertTrue(qrdDocument.getSections().get(idxNumeric).getText().contains("<content styleCode=\"Bold\">1</content>"));
    assertFalse(qrdDocument
        .getSections()
        .get(idxNumeric)
        .getText()
        .contains("<content styleCode=\"Bold\"><!-- Q2553 --></content>"));
    assertTrue(qrdDocument.getSections().get(idxNumeric).getText().contains("<content styleCode=\"Bold\">2</content>"));
    assertFalse(xmlDocument.contains("<content styleCode=\"Bold\"><!-- Q2552 --></content>"));
    assertTrue(xmlDocument.contains("<content styleCode=\"Bold\">1</content>"));
    assertFalse(xmlDocument.contains("<content styleCode=\"Bold\"><!-- Q2552 --></content>"));
    assertTrue(xmlDocument.contains("<content styleCode=\"Bold\">2</content>"));

    //Text, response code A173_1 og text "At blive rask"
    assertNotNull(qrdDocument.getSections().get(idxText).getText());
    assertFalse(qrdDocument.getSections().get(idxText).getText().contains("<td><!--A173_1--></td>"));
    assertEquals(1, countOccurences(qrdDocument.getSections().get(idxText).getText(), "<td>X</td>"));
    //    assertTrue(qrdDocument.getSections().get(idxText).getText().contains("<td>X</td>"));
    assertTrue(qrdDocument.getSections().get(idxText).getText().contains("<td><!-- A173_2 --></td>"));
    assertTrue(qrdDocument.getSections().get(idxText).getText().contains("<td><!-- A173_3 --></td>"));
    assertTrue(qrdDocument.getSections().get(idxText).getText().contains("<td><!-- Q174 --></td>"));
    assertFalse(qrdDocument
        .getSections()
        .get(idxText)
        .getText()
        .contains("<tr colspan=\"2\" align=\"left\"><!-- Q243 --></tr>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxText)
        .getText()
        .contains("<tr colspan=\"2\" align=\"left\">At blive rask</tr>"));

    assertFalse(xmlDocument.contains("<td><!--A173_1--></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A173_2 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A173_3 --></td>"));
    assertTrue(xmlDocument.contains("<td><!-- Q174 --></td>"));
    assertFalse(xmlDocument.contains("<tr colspan=\"2\" align=\"left\"><!-- Q243 --></tr>"));
    assertTrue(xmlDocument.contains("<tr colspan=\"2\" align=\"left\">At blive rask</tr>"));

    //Text and associated text, response code A173_1 og text "At blive rask" and "Ægtefælle"
    assertNotNull(qrdDocument.getSections().get(idxTextA).getText());
    assertTrue(qrdDocument.getSections().get(idxTextA).getText().contains("<td><!--A173_1a--></td>"));
    assertTrue(qrdDocument.getSections().get(idxTextA).getText().contains("<td><!-- A173_2a --></td>"));
    assertFalse(qrdDocument.getSections().get(idxTextA).getText().contains("<td><!-- A173_3a --></td>"));
    assertEquals(1, countOccurences(qrdDocument.getSections().get(idxTextA).getText(), "<td>X</td>"));
    assertTrue(qrdDocument.getSections().get(idxTextA).getText().contains("<td>Ægtefælle</td>"));
    assertFalse(qrdDocument.getSections().get(idxTextA).getText().contains("<td><!-- Q174a --></td>"));
    assertFalse(qrdDocument
        .getSections()
        .get(idxTextA)
        .getText()
        .contains("<tr colspan=\"2\" align=\"left\"><!-- Q243a --></tr>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxTextA)
        .getText()
        .contains("<tr colspan=\"2\" align=\"left\">At blive rask</tr>"));

    assertTrue(xmlDocument.contains("<td><!--A173_1a--></td>"));
    assertTrue(xmlDocument.contains("<td><!-- A173_2a --></td>"));
    assertFalse(xmlDocument.contains("<td><!-- A173_3a --></td>"));
    assertTrue(xmlDocument.contains("<td>Ægtefælle</td>"));
    assertFalse(xmlDocument.contains("<td><!-- Q174a --></td>"));
    assertFalse(xmlDocument.contains("<tr colspan=\"2\" align=\"left\"><!-- Q243a --></tr>"));
    assertTrue(xmlDocument.contains("<tr colspan=\"2\" align=\"left\">At blive rask</tr>"));

    //Discrete slider response is: code="A30_2" displayName="Vældig godt"
    assertTrue(qrdDocument
        .getSections()
        .get(idxDiscreteSlider1)
        .getText()
        .contains("<td styleCode=\"Bold\"><!-- A30_1 --></td>"));
    assertFalse(qrdDocument
        .getSections()
        .get(idxDiscreteSlider1)
        .getText()
        .contains("<td styleCode=\"Bold\"><!-- A30_2 --></td>"));
    assertEquals(1,
        countOccurences(qrdDocument.getSections().get(idxDiscreteSlider1).getText(), "<td styleCode=\"Bold\">X</td>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxDiscreteSlider1)
        .getText()
        .contains("<td styleCode=\"Bold\"><!-- A30_3 --></td>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxDiscreteSlider1)
        .getText()
        .contains("<td styleCode=\"Bold\"><!-- A30_4 --></td>"));
    assertTrue(qrdDocument
        .getSections()
        .get(idxDiscreteSlider1)
        .getText()
        .contains("<td styleCode=\"Bold\"><!-- A30_5 --></td>"));
    assertTrue(xmlDocument.contains("<td styleCode=\"Bold\"><!-- A30_1 --></td>"));
    assertFalse(xmlDocument.contains("<td styleCode=\"Bold\"><!-- A30_2 --></td>"));
    assertTrue(xmlDocument.contains("<td styleCode=\"Bold\"><!-- A30_3 --></td>"));
    assertTrue(xmlDocument.contains("<td styleCode=\"Bold\"><!-- A30_4 --></td>"));
    assertTrue(xmlDocument.contains("<td styleCode=\"Bold\"><!-- A30_5 --></td>"));

    assertEquals(1, countOccurences(xmlDocument, "<td styleCode=\"Bold\">X</td>"));

    //Discrete slider response with associcated text question - replace narrative texts
    assertTrue(qrdDocument
        .getSections()
        .get(idxDiscreteSlider2)
        .getText()
        .contains("<td align=\"right\" styleCode=\"Bold\">X</td>"));
    assertFalse(qrdDocument.getSections().get(idxDiscreteSlider2).getText().contains("<!-- A.DISCRETE.01.03 -->"));
    assertTrue(qrdDocument.getSections().get(idxDiscreteSlider2).getText().contains(
        "<td align=\"right\" colspan=\"2\" styleCode=\"Bold\">Mit svar svæver i vinden</td>"));
    assertFalse(qrdDocument.getSections().get(idxDiscreteSlider2).getText().contains("<!-- Q.DISCRETE.02.01 -->"));

    //    System.out.println(xmlDocument);

  }

  @Test
  public void testTextResponseWithNoAnswer() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    QRDDocument qrdDocumentCreate = new SetupQRDVariationsExample().createDocument();

    QRDResponse qrdResponseCreate = new SetupQRDVariationsExample().responseTextNoResponseValue();
    Section<QRDOrganizer> section = new Section<QRDOrganizer>("title", "Questions");
    QRDOrganizerBuilder qrdOrganizerBuilder = new QRDOrganizerBuilder();
    qrdOrganizerBuilder.addQRDResponse(qrdResponseCreate);
    section.addOrganizer(qrdOrganizerBuilder.build());
    qrdDocumentCreate.addSection(section);

    assertTrue(qrdDocumentCreate
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0) instanceof QRDTextResponse);
    assertNull(
        ((QRDTextResponse) qrdDocumentCreate.getSections().get(0).getOrganizers().get(0).getQRDResponses().get(0))
            .getText());

    String xmlDocument = encodeDocument(qrdDocumentCreate);
    QRDDocument qrdDocumentDecoded = codec.decode(xmlDocument);

    assertTrue(qrdDocumentDecoded
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0) instanceof QRDTextResponse);
    QRDTextResponse qrdResponseDecoded = (QRDTextResponse) qrdDocumentDecoded
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0);
    assertNull(qrdResponseDecoded.getText());

  }

  @Test
  public void testMultipleChoiceResponseAssociatedTextWithNoAnswer() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    QRDDocument qrdDocumentCreate = new SetupQRDVariationsExample().createDocument();

    QRDTextResponse associatedTextResponseCreate = (QRDTextResponse) new SetupQRDVariationsExample()
        .responseTextNoResponseValue("questionAsAssociatedText");
    assertTrue(associatedTextResponseCreate instanceof QRDTextResponse);

    QRDResponse qrdResponse = new SetupQRDVariationsExample().responseMultipleChoice(false,
        associatedTextResponseCreate);
    Section<QRDOrganizer> section = new Section<QRDOrganizer>("title", "Questions");
    QRDOrganizerBuilder qrdOrganizerBuilder = new QRDOrganizerBuilder();
    qrdOrganizerBuilder.addQRDResponse(qrdResponse);
    section.addOrganizer(qrdOrganizerBuilder.build());
    qrdDocumentCreate.addSection(section);

    assertTrue(qrdDocumentCreate
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0) instanceof QRDMultipleChoiceResponse);
    assertNull(((QRDMultipleChoiceResponse) qrdDocumentCreate
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0)).getAssociatedTextResponse().getText());

    String xmlDocument = encodeDocument(qrdDocumentCreate);
    QRDDocument qrdDocumentDecoded = codec.decode(xmlDocument);

    assertTrue(qrdDocumentDecoded
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0) instanceof QRDMultipleChoiceResponse);
    QRDMultipleChoiceResponse qrdResponseDecoded = (QRDMultipleChoiceResponse) qrdDocumentDecoded
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0);
    assertNull(qrdResponseDecoded.getAssociatedTextResponse().getText());

  }

  @Test
  public void testDiscreteSliderResponseAssociatedTextWithNoAnswer() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    QRDDocument qrdDocumentCreate = new SetupQRDVariationsExample().createDocument();

    QRDTextResponse associatedTextResponseCreate = (QRDTextResponse) new SetupQRDVariationsExample()
        .responseTextNoResponseValue("questionAsAssociatedText");
    assertTrue(associatedTextResponseCreate instanceof QRDTextResponse);

    QRDResponse qrdResponse = new SetupQRDVariationsExample().responseDiscreteSlider(false,
        associatedTextResponseCreate);
    Section<QRDOrganizer> section = new Section<QRDOrganizer>("title", "Questions");
    QRDOrganizerBuilder qrdOrganizerBuilder = new QRDOrganizerBuilder();
    qrdOrganizerBuilder.addQRDResponse(qrdResponse);
    section.addOrganizer(qrdOrganizerBuilder.build());
    qrdDocumentCreate.addSection(section);

    assertTrue(qrdDocumentCreate
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0) instanceof QRDDiscreteSliderResponse);
    assertNull(((QRDDiscreteSliderResponse) qrdDocumentCreate
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0)).getAssociatedTextResponse().getText());

    String xmlDocument = encodeDocument(qrdDocumentCreate);
    QRDDocument qrdDocumentDecoded = codec.decode(xmlDocument);

    assertTrue(qrdDocumentDecoded
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0) instanceof QRDDiscreteSliderResponse);
    QRDDiscreteSliderResponse qrdResponseDecoded = (QRDDiscreteSliderResponse) qrdDocumentDecoded
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQRDResponses()
        .get(0);
    assertNull(qrdResponseDecoded.getAssociatedTextResponse().getText());

  }

  @Test
  public void testIdOnAuthor() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    QRDDocument qrdDocumentCreate = new SetupQRDVariationsExample().createDocument();

    String sorAuthor = "242621000016001";
    String sorAuthorOrg = "242621000016002";

    Date aTime = DateUtil.makeDanishDateTimeWithTimeZone(2017, 1, 16, 10, 0, 0);

    PersonIdentity jensJensen = new PersonBuilder("Jensen").addGivenName("Jens").setPrefix("Læge").build();

    OrganizationIdentity authorOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR(sorAuthorOrg)
        .setName("OUH Radiologisk Afdeling (Svendborg)")
        .setAddress(Setup.defineValdemarsGade53Address())
        .addTelecom(Use.WorkPlace, "tel", "65113333-1")
        .build();
    Participant author = new ParticipantBuilder()
        .setAddress(authorOrganization.getAddress())
        .setSOR(sorAuthor)
        .setTelecomList(authorOrganization.getTelecomList())
        .setTime(aTime)
        .setPersonIdentity(jensJensen)
        .setOrganizationIdentity(authorOrganization)
        .build();
    qrdDocumentCreate.setAuthor(author);

    String xmlDocument = encodeDocument(qrdDocumentCreate);
    QRDDocument qrdDocumentDecoded = codec.decode(xmlDocument);

    assertEquals(sorAuthor, qrdDocumentDecoded.getAuthor().getId().getExtension());
    assertEquals(sorAuthorOrg, qrdDocumentDecoded.getAuthor().getOrganizationIdentity().getId().getExtension());

  }

  @Test
  public void testAuthorRepresentedOrganizationAdrAndTelecomDetailsAvailable() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    QRDDocument qrdDocumentCreate = new SetupQRDVariationsExample().createDocument();

    boolean includeOrgAdrAndTelecom = true;
    OrganizationIdentity organizationIdentity = setupOrganizationIdentity(includeOrgAdrAndTelecom);

    Participant author = setupAuthor(organizationIdentity, null, qrdDocumentCreate.getEffectiveTime());
    qrdDocumentCreate.setAuthor(author);

    qrdDocumentCreate.setLegalAuthenticator(author);

    String xmlDocument = encodeDocument(qrdDocumentCreate);
    QRDDocument qrdDocumentDecoded = codec.decode(xmlDocument);

    assertTelecom(organizationIdentity.getTelecomList(), qrdDocumentDecoded.getAuthor().getTelecomList());
    assertTelecom(organizationIdentity.getTelecomList(),
        qrdDocumentDecoded.getAuthor().getOrganizationIdentity().getTelecomList());

    assertAddr(organizationIdentity.getAddress(), qrdDocumentDecoded.getAuthor().getAddress());
    assertAddr(organizationIdentity.getAddress(),
        qrdDocumentDecoded.getAuthor().getOrganizationIdentity().getAddress());

    assertNull(qrdDocumentDecoded.getLegalAuthenticator()); //LegalAuthenticator indgår ikke i byg af QRD xml

  }

  @Test
  public void testAuthorRepresentedOrganizationAdrAndTelecomDetailsNotAvailable() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    QRDDocument qrdDocumentCreate = new SetupQRDVariationsExample().createDocument();

    boolean includeOrgAdrAndTelecom = false;
    OrganizationIdentity organizationIdentity = setupOrganizationIdentity(includeOrgAdrAndTelecom);

    Participant author = setupAuthor(organizationIdentity, null, qrdDocumentCreate.getEffectiveTime());
    qrdDocumentCreate.setAuthor(author);
    qrdDocumentCreate.setLegalAuthenticator(author);

    String xmlDocument = encodeDocument(qrdDocumentCreate);
    QRDDocument qrdDocumentDecoded = codec.decode(xmlDocument);

    assertEquals(0, qrdDocumentDecoded.getAuthor().getTelecomList().length);
    assertEquals(0, qrdDocumentDecoded.getAuthor().getOrganizationIdentity().getTelecomList().length);

    assertAddr(organizationIdentity.getAddress(), qrdDocumentDecoded.getAuthor().getAddress());
    assertEquals(null, qrdDocumentDecoded.getAuthor().getOrganizationIdentity().getAddress());

    assertNull(qrdDocumentDecoded.getLegalAuthenticator()); //LegalAuthenticator indgår ikke i byg af QRD xml

  }

  @Test
  public void testAuthorRepresentedOrganizationWhenAuthorIsPatient() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    QRDDocument qrdDocumentCreate = new SetupQRDVariationsExample().createDocument();

    boolean includeOrgAdrAndTelecom = true;
    OrganizationIdentity organizationIdentity = setupOrganizationIdentity(includeOrgAdrAndTelecom);
    Patient patient = qrdDocumentCreate.getPatient();

    Participant author = setupAuthor(organizationIdentity, patient, qrdDocumentCreate.getEffectiveTime());
    qrdDocumentCreate.setAuthor(author);
    qrdDocumentCreate.setLegalAuthenticator(author);

    String xmlDocument = encodeDocument(qrdDocumentCreate);
    QRDDocument qrdDocumentDecoded = codec.decode(xmlDocument);

    assertTelecom(patient.getTelecomList(), qrdDocumentDecoded.getAuthor().getTelecomList());
    assertTelecom(organizationIdentity.getTelecomList(),
        qrdDocumentDecoded.getAuthor().getOrganizationIdentity().getTelecomList());

    assertAddr(patient.getAddress(), qrdDocumentDecoded.getAuthor().getAddress());
    assertAddr(organizationIdentity.getAddress(),
        qrdDocumentDecoded.getAuthor().getOrganizationIdentity().getAddress());

    assertNull(qrdDocumentDecoded.getLegalAuthenticator()); //LegalAuthenticator indgår ikke i byg af QRD xml

  }

  private Participant setupAuthor(OrganizationIdentity authorOrganization, Patient patient, Date time) {
    if (patient != null) {
      Participant author = new ParticipantBuilder()
          .setAddress(patient.getAddress())
          .setId(patient.getId())
          .setTelecomList(patient.getTelecomList())
          .setTime(time)
          .setPersonIdentity(patient)
          .setOrganizationIdentity(authorOrganization)
          .build();
      return author;

    } else {
      PersonIdentity jensJensen = new PersonBuilder("Jensen").addGivenName("Jens").setPrefix("Læge").build();
      Participant author = new ParticipantBuilder()
          .setAddress(authorOrganization.getAddress())
          .setSOR("242621000016001")
          .setTelecomList(authorOrganization.getTelecomList())
          .setTime(time)
          .setPersonIdentity(jensJensen)
          .setOrganizationIdentity(authorOrganization)
          .build();
      return author;
    }

  }

  private OrganizationIdentity setupOrganizationIdentity(boolean includeOrgAdrAndTelecom) {
    Telecom[] telecoms = { new Telecom(Use.WorkPlace, "tel", "65113333-3"),
        new Telecom(Use.HomeAddress, "tel", "65113333-4") };
    AddressData valdemarsGade53 = new AddressData.AddressBuilder("5700", "Svendborg")
        .addAddressLine("Valdemarsgade 53")
        .addAddressLine("Øverste lejlighed")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    List<Telecom> orgTelecoms = (includeOrgAdrAndTelecom) ? Arrays.asList(telecoms) : new ArrayList<Telecom>();
    AddressData orgAddr = (includeOrgAdrAndTelecom) ? valdemarsGade53 : null;
    OrganizationIdentity authorOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("242621000016002")
        .setName("OUH Radiologisk Afdeling (Svendborg)")
        .setAddress(orgAddr)
        .addTelecoms(orgTelecoms)
        .build();
    return authorOrganization;
  }

  private void assertTelecom(Telecom[] telecomsExpected, Telecom[] telecomsActual) {
    if (telecomsActual != null && telecomsActual != null) {
      assertEquals(telecomsExpected[0].getUse(), telecomsActual[0].getUse());
      assertEquals(telecomsExpected[0].getValue(), telecomsActual[0].getValue());
      assertEquals(telecomsExpected[0].getProtocol(), telecomsActual[0].getProtocol());
      assertEquals(telecomsExpected[1].getUse(), telecomsActual[1].getUse());
      assertEquals(telecomsExpected[1].getValue(), telecomsActual[1].getValue());
      assertEquals(telecomsExpected[1].getProtocol(), telecomsActual[1].getProtocol());
    }
  }

  private void assertAddr(AddressData addressDataExpected, AddressData addressDataActual) {
    if (addressDataExpected != null && addressDataActual != null) {
      assertEquals(addressDataExpected.getAddressUse(), addressDataActual.getAddressUse());
      assertEquals(addressDataExpected.getStreet()[0], addressDataActual.getStreet()[0]);
      assertEquals(addressDataExpected.getStreet()[1], addressDataActual.getStreet()[1]);
      assertEquals(addressDataExpected.getCountry(), addressDataActual.getCountry());
      assertEquals(addressDataExpected.getCity(), addressDataActual.getCity());
      assertEquals(addressDataExpected.getPostalCode(), addressDataActual.getPostalCode());
    }
  }

  @Test
  public void testCopyRightBuildAndParse() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    String xmlDocument = encodeDocument(
        new SetupQRDVariationsExample().createDocumentWithCopyRightAndQuestionInTwoSections());
    QRDDocument qrdDocument = codec.decode(xmlDocument);

    assertNotNull(qrdDocument);
    assertNotNull(qrdDocument.getSections());
    assertEquals(3, qrdDocument.getSections().size());

    Section<QRDOrganizer> sectioncopyright = qrdDocument.getSections().get(2);
    assertNotNull(sectioncopyright);
    assertTrue(sectioncopyright.isCopyRightSection());
    assertNotNull(sectioncopyright.getTitle());
    assertEquals("Copyright title", sectioncopyright.getTitle());
    assertNotNull(sectioncopyright.getText());
    assertEquals("Copyright tekst", sectioncopyright.getText());
    assertNotNull(sectioncopyright.getLanguage());
    assertEquals("da-DK", sectioncopyright.getLanguage());
    assertEquals("Copyright tekst", sectioncopyright.getCopyrightTexts().get(0));
  }

  @Test
  public void testMultipleCopyRightBuildAndParse() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    QRDDocument qrdDocumentNew = new SetupQRDVariationsExample().createDocumentWithMultipleCopyRightAndQuestions();

    assertNotNull(qrdDocumentNew);

    String xmlDocument = encodeDocument(qrdDocumentNew);
    assertNotNull(xmlDocument);
    QRDDocument qrdDocument = codec.decode(xmlDocument);

    assertNotNull(qrdDocument);
    assertNotNull(qrdDocument.getSections());
    assertEquals(6, qrdDocument.getSections().size());

    Section<QRDOrganizer> section0copyright = qrdDocument.getSections().get(0);
    assertEquals("Copyright title 1", section0copyright.getTitle());
    assertEquals("Copyright tekst1.1 + Copyright tekst1.2", section0copyright.getText());
    assertEquals("da-DK", section0copyright.getLanguage());
    assertTrue(section0copyright.isCopyRightSection());
    assertEquals(2, section0copyright.getCopyrightTexts().size());
    assertEquals(0, section0copyright.getOrganizers().size());

    Section<QRDOrganizer> section1 = qrdDocument.getSections().get(1);
    assertEquals("Questions1", section1.getTitle());
    assertEquals("Questions", section1.getText());
    assertEquals(null, section1.getLanguage());
    assertFalse(section1.isCopyRightSection());
    assertEquals(0, section1.getCopyrightTexts().size());
    assertEquals(1, section1.getOrganizers().size());

    Section<QRDOrganizer> section2copyright = qrdDocument.getSections().get(2);
    assertEquals("Copyright title 2", section2copyright.getTitle());
    assertEquals("Copyright tekst2.1", section2copyright.getText());
    assertEquals("da-DK", section2copyright.getLanguage());
    assertTrue(section2copyright.isCopyRightSection());
    assertEquals(1, section2copyright.getCopyrightTexts().size());
    assertEquals(0, section2copyright.getOrganizers().size());

    Section<QRDOrganizer> section3copyright = qrdDocument.getSections().get(3);
    assertEquals("Copyright title 3", section3copyright.getTitle());
    assertEquals("Copyright tekst3.1", section3copyright.getText());
    assertEquals("da-DK", section3copyright.getLanguage());
    assertTrue(section3copyright.isCopyRightSection());
    assertEquals(1, section3copyright.getCopyrightTexts().size());
    assertEquals(0, section3copyright.getOrganizers().size());

    Section<QRDOrganizer> section4 = qrdDocument.getSections().get(4);
    assertEquals("Questions2", section4.getTitle());
    assertEquals("Questions", section4.getText());
    assertEquals(null, section4.getLanguage());
    assertFalse(section4.isCopyRightSection());
    assertEquals(0, section4.getCopyrightTexts().size());
    assertEquals(1, section4.getOrganizers().size());

    Section<QRDOrganizer> section5copyright = qrdDocument.getSections().get(5);
    assertEquals("Copyright title sidste", section5copyright.getTitle());
    assertEquals("Copyright tekst sidste", section5copyright.getText());
    assertEquals("da-DK", section5copyright.getLanguage());
    assertTrue(section5copyright.isCopyRightSection());
    assertEquals(1, section5copyright.getCopyrightTexts().size());
    assertEquals(0, section5copyright.getOrganizers().size());

    String xmlDocument2 = encodeDocument(qrdDocumentNew);
    assertEquals(xmlDocument, xmlDocument2);

  }

  @Test
  public void testMultipleCopyRightWithTextReplaceBuildAndParse() throws Exception {
    QRDXmlCodec codec = new QRDXmlCodec();
    QRDDocument qrdDocumentNew = new SetupQRDVariationsExample()
        .createDocumentWithOneCopyRightNoTextTreeTextAndOneQuestion();

    assertNotNull(qrdDocumentNew);

    String xmlDocument = encodeDocument(qrdDocumentNew);
    assertNotNull(xmlDocument);
    QRDDocument qrdDocument = codec.decode(xmlDocument);

    assertNotNull(qrdDocument);
    assertNotNull(qrdDocument.getSections());
    assertEquals(2, qrdDocument.getSections().size());

    Section<QRDOrganizer> section = qrdDocument.getSections().get(0);
    assertEquals("Questions1", section.getTitle());
    assertEquals("Question1<br/>Question2", section.getText());
    assertEquals(null, section.getLanguage());
    assertFalse(section.isCopyRightSection());
    assertEquals(0, section.getCopyrightTexts().size());
    assertEquals(1, section.getOrganizers().size());

    Section<QRDOrganizer> sectionCopyright = qrdDocument.getSections().get(1);
    assertEquals("Copyright title 1", sectionCopyright.getTitle());
    assertEquals("Copyright tekst1.1<br/>Copyright tekst1.2<br/>Copyright tekst1.3", sectionCopyright.getText());
    assertEquals("da-DK", sectionCopyright.getLanguage());
    assertTrue(sectionCopyright.isCopyRightSection());
    assertEquals(3, sectionCopyright.getCopyrightTexts().size());
    assertEquals(0, sectionCopyright.getOrganizers().size());

    String xmlDocument2 = encodeDocument(qrdDocumentNew);
    assertEquals(xmlDocument, xmlDocument2);

  }

  private int countOccurences(String sourceString, String subString) {
    int i = 0;
    Pattern p = Pattern.compile(subString);
    Matcher m = p.matcher(sourceString);
    while (m.find()) {
      i++;
    }
    return i;
  }

}
