package dk.s4.hl7.cda.convert.decode;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public class TestXmlToPhmrToXmlCompare extends BaseDecodeTest {

  private PHMRXmlCodec codec = new PHMRXmlCodec();

  @Before
  public void before() {
    codec = new PHMRXmlCodec();
  }

  @Test
  public void testXmlToPhmrToXml_Example_Ex1_Weight_measurement_MODIFIED()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "phmr/Ex1-Weight_measurement-MODIFIED.xml");

    PHMRDocument phmr = codec.decode(xmlOrig);
    String xmlNew = codec.encode(phmr);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToPhmrToXml_Example_KOL_Example_1_MaTIS_MODIFIED()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "phmr/PHMR_KOL_Example_1_MaTIS-MODIFIED.xml");

    PHMRDocument phmr = codec.decode(xmlOrig);
    String xmlNew = codec.encode(phmr);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToPhmrToXml_Example_KOL_Example_1_MaTIS_MODIFIED_with_representeorganizationId()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "phmr/PHMR_KOL_Example_1_MaTIS-MODIFIED_with_representeorganizationId.xml");

    PHMRDocument phmr = codec.decode(xmlOrig);
    String xmlNew = codec.encode(phmr);
    compare(xmlOrig, xmlNew);
  }

}
