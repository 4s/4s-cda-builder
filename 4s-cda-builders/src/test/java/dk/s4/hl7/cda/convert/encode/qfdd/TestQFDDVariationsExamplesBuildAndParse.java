package dk.s4.hl7.cda.convert.encode.qfdd;

import org.junit.Test;

import dk.s4.hl7.cda.convert.QFDDXmlCodec;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qfdd.QFDDDiscreteSliderQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDMultipleChoiceQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDNumericQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer.QFDDOrganizerBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;

public final class TestQFDDVariationsExamplesBuildAndParse extends BaseEncodeTest<QFDDDocument> {
  public TestQFDDVariationsExamplesBuildAndParse() {
    super("src/test/resources/qfdd");
    setCodec(new QFDDXmlCodec());
  }

  @Test
  public void testNullValueForTitleIsValidBuildAndParse() throws Exception {
    QFDDXmlCodec codec = new QFDDXmlCodec();
    String xmlDocument = encodeDocument(new SetupQFDDVariationsExample().createDocumentTitleNull());
    QFDDDocument qfddDocument = codec.decode(xmlDocument);

    assertNotNull(qfddDocument);
    assertNotNull(qfddDocument.getSections());

    assertEquals(3, qfddDocument.getSections().size());
    assertNotNull(qfddDocument.getSections().get(0));
    assertNull(qfddDocument.getSections().get(0).getTitle());
    assertNotNull(qfddDocument.getSections().get(1));
    assertNull(qfddDocument.getSections().get(1).getTitle());
    assertNotNull(qfddDocument.getSections().get(2));
    assertNotNull(qfddDocument.getSections().get(2).getText());
    assertNotNull(qfddDocument.getSections().get(2).getCopyrightTexts().get(0));
    assertNull(qfddDocument.getSections().get(2).getTitle());
    assertTrue(qfddDocument.getSections().get(2).isCopyRightSection());
  }

  @Test
  public void testReferenceRangeIsNullForNumericQuestionIsValidBuildAndParse() throws Exception {

    QFDDXmlCodec codec = new QFDDXmlCodec();
    String xmlDocument = encodeDocument(new SetupQFDDVariationsExample().createDocumentNumericQuestionNoInterval());
    QFDDDocument qfddDocument = codec.decode(xmlDocument);

    assertNotNull(qfddDocument);
    assertNotNull(qfddDocument.getSections());

    assertEquals(1, qfddDocument.getSections().size());
    assertNotNull(qfddDocument.getSections().get(0));
    assertNotNull(qfddDocument.getSections().get(0).getOrganizers());
    assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0));
    assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions());
    assertEquals(1, qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().size());
    assertTrue(qfddDocument
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0) instanceof QFDDNumericQuestion);
    QFDDNumericQuestion qfddNumericQuestion = (QFDDNumericQuestion) qfddDocument
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0);
    assertNull(qfddNumericQuestion.getMaximum());
    assertNull(qfddNumericQuestion.getMinimum());

  }

  @Test
  public void testReferenceRangeIsNotNullForNumericQuestionIsValidBuildAndParse() throws Exception {

    QFDDXmlCodec codec = new QFDDXmlCodec();
    String xmlDocument = encodeDocument(new SetupQFDDVariationsExample().createDocumentNumericQuestion());
    QFDDDocument qfddDocument = codec.decode(xmlDocument);

    assertNotNull(qfddDocument);
    assertNotNull(qfddDocument.getSections());

    assertEquals(1, qfddDocument.getSections().size());
    assertNotNull(qfddDocument.getSections().get(0));
    assertNotNull(qfddDocument.getSections().get(0).getOrganizers());
    assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0));
    assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions());
    assertEquals(1, qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().size());
    assertTrue(qfddDocument
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0) instanceof QFDDNumericQuestion);
    QFDDNumericQuestion qfddNumericQuestion = (QFDDNumericQuestion) qfddDocument
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0);
    assertNotNull(qfddNumericQuestion.getMaximum());
    assertEquals("5", qfddNumericQuestion.getMaximum());
    assertNotNull(qfddNumericQuestion.getMinimum());
    assertEquals("1", qfddNumericQuestion.getMinimum());

  }

  @Test
  public void testNoDisplayNameIsValidWhenBuilded() throws Exception {

    QFDDXmlCodec codec = new QFDDXmlCodec();
    String xmlDocument = encodeDocument(new SetupQFDDVariationsExample().createDocumentQuestionNoDisplayName());
    QFDDDocument qfddDocument = codec.decode(xmlDocument);

    assertNotNull(qfddDocument);
    assertNotNull(qfddDocument.getSections());

    assertEquals(5, qfddDocument.getSections().size());

    assertNotNull(qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(0).getCode());
    assertEquals("value1",
        qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getCode());
    assertEquals("value2",
        qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getCodeSystem());
    assertEquals("value4", qfddDocument
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0)
        .getCode()
        .getCodeSystemName());
    assertNull(
        qfddDocument.getSections().get(0).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getDisplayName());

    assertNotNull(qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(0).getCode());
    assertEquals("value1",
        qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getCode());
    assertEquals("value2",
        qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getCodeSystem());
    assertEquals("value4", qfddDocument
        .getSections()
        .get(1)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0)
        .getCode()
        .getCodeSystemName());
    assertNull(
        qfddDocument.getSections().get(1).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getDisplayName());

    assertNotNull(qfddDocument.getSections().get(2).getOrganizers().get(0).getQFDDQuestions().get(0).getCode());
    assertEquals("value1",
        qfddDocument.getSections().get(2).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getCode());
    assertEquals("value2",
        qfddDocument.getSections().get(2).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getCodeSystem());
    assertEquals("value4", qfddDocument
        .getSections()
        .get(2)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0)
        .getCode()
        .getCodeSystemName());
    assertNull(
        qfddDocument.getSections().get(2).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getDisplayName());

    assertNotNull(qfddDocument.getSections().get(3).getOrganizers().get(0).getQFDDQuestions().get(0).getCode());
    assertEquals("value1",
        qfddDocument.getSections().get(3).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getCode());
    assertEquals("value2",
        qfddDocument.getSections().get(3).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getCodeSystem());
    assertEquals("value4", qfddDocument
        .getSections()
        .get(3)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0)
        .getCode()
        .getCodeSystemName());
    assertNull(
        qfddDocument.getSections().get(3).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getDisplayName());

    assertNotNull(qfddDocument.getSections().get(4).getOrganizers().get(0).getQFDDQuestions().get(0).getCode());
    assertEquals("value1",
        qfddDocument.getSections().get(4).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getCode());
    assertEquals("value2",
        qfddDocument.getSections().get(4).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getCodeSystem());
    assertEquals("value4", qfddDocument
        .getSections()
        .get(4)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0)
        .getCode()
        .getCodeSystemName());
    assertNull(
        qfddDocument.getSections().get(4).getOrganizers().get(0).getQFDDQuestions().get(0).getCode().getDisplayName());
  }

  @Test
  public void testMultiblechoiceResponseAssociatedText() throws Exception {
    QFDDXmlCodec codec = new QFDDXmlCodec();
    QFDDDocument qfddDocumentCreate = new SetupQFDDVariationsExample().createDocument();

    QFDDTextQuestion associatedTextQuestionCreate = (QFDDTextQuestion) new SetupQFDDVariationsExample()
        .simpleQuestion("questionAsAssociatedText");
    assertTrue(associatedTextQuestionCreate instanceof QFDDQuestion);

    QFDDMultipleChoiceQuestion qfddQuestion = (QFDDMultipleChoiceQuestion) new SetupQFDDVariationsExample()
        .multipleChoiceQuestion(false, associatedTextQuestionCreate);
    Section<QFDDOrganizer> section = new Section<QFDDOrganizer>("title", "Questions");
    QFDDOrganizerBuilder qfddOrganizerBuilder = new QFDDOrganizerBuilder();
    qfddOrganizerBuilder.addQFDDQuestion(qfddQuestion);
    section.addOrganizer(qfddOrganizerBuilder.build());
    qfddDocumentCreate.addSection(section);

    assertTrue(qfddDocumentCreate
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0) instanceof QFDDMultipleChoiceQuestion);
    assertNotNull(((QFDDMultipleChoiceQuestion) qfddDocumentCreate
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0)).getAssociatedTextQuestion());

    String xmlDocument = encodeDocument(qfddDocumentCreate);
    QFDDDocument qfddDocumentDecoded = codec.decode(xmlDocument);

    assertTrue(qfddDocumentDecoded
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0) instanceof QFDDMultipleChoiceQuestion);
    QFDDMultipleChoiceQuestion qfddQuestionDecoded = (QFDDMultipleChoiceQuestion) qfddDocumentDecoded
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0);
    assertNotNull(qfddQuestionDecoded.getAssociatedTextQuestion());

  }

  @Test
  public void testDiscreteSliderResponseAssociatedText() throws Exception {
    QFDDXmlCodec codec = new QFDDXmlCodec();
    QFDDDocument qfddDocumentCreate = new SetupQFDDVariationsExample().createDocument();

    QFDDTextQuestion associatedTextQuestionCreate = (QFDDTextQuestion) new SetupQFDDVariationsExample()
        .simpleQuestion("questionAsAssociatedText");
    assertTrue(associatedTextQuestionCreate instanceof QFDDQuestion);

    QFDDDiscreteSliderQuestion qfddQuestion = (QFDDDiscreteSliderQuestion) new SetupQFDDVariationsExample()
        .multipleDiscreteSlider(false, associatedTextQuestionCreate);
    Section<QFDDOrganizer> section = new Section<QFDDOrganizer>("title", "Questions");
    QFDDOrganizerBuilder qfddOrganizerBuilder = new QFDDOrganizerBuilder();
    qfddOrganizerBuilder.addQFDDQuestion(qfddQuestion);
    section.addOrganizer(qfddOrganizerBuilder.build());
    qfddDocumentCreate.addSection(section);

    assertTrue(qfddDocumentCreate
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0) instanceof QFDDDiscreteSliderQuestion);
    assertNotNull(((QFDDDiscreteSliderQuestion) qfddDocumentCreate
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0)).getAssociatedTextQuestion());

    String xmlDocument = encodeDocument(qfddDocumentCreate);
    QFDDDocument qfddDocumentDecoded = codec.decode(xmlDocument);

    assertTrue(qfddDocumentDecoded
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0) instanceof QFDDDiscreteSliderQuestion);
    QFDDDiscreteSliderQuestion qfddQuestionDecoded = (QFDDDiscreteSliderQuestion) qfddDocumentDecoded
        .getSections()
        .get(0)
        .getOrganizers()
        .get(0)
        .getQFDDQuestions()
        .get(0);
    assertNotNull(qfddQuestionDecoded.getAssociatedTextQuestion());

  }

  @Test
  public void testMultipleCopyRightBuildAndParse() throws Exception {
    QFDDXmlCodec codec = new QFDDXmlCodec();
    QFDDDocument qfddDocumentNew = new SetupQFDDVariationsExample().createDocumentWithMultipleCopyRightAndQuestions();

    assertNotNull(qfddDocumentNew);

    String xmlDocument = encodeDocument(qfddDocumentNew);
    assertNotNull(xmlDocument);

    QFDDDocument qfddDocument = codec.decode(xmlDocument);

    assertNotNull(qfddDocument);
    assertNotNull(qfddDocument.getSections());
    assertEquals(6, qfddDocument.getSections().size());

    Section<QFDDOrganizer> section0copyright = qfddDocument.getSections().get(0);
    assertEquals("Copyright title 1", section0copyright.getTitle());
    assertEquals("Copyright tekst1.1 + Copyright tekst1.2", section0copyright.getText());
    assertEquals("da-DK", section0copyright.getLanguage());
    assertTrue(section0copyright.isCopyRightSection());
    assertEquals(2, section0copyright.getCopyrightTexts().size());
    assertEquals(0, section0copyright.getOrganizers().size());

    Section<QFDDOrganizer> section1 = qfddDocument.getSections().get(1);
    assertEquals("Indledning1", section1.getTitle());
    assertEquals(
        "OM DETTE SKEMA: Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>Hvornår havde du dit seneste anfald?",
        section1.getText());
    assertEquals(null, section1.getLanguage());
    assertFalse(section1.isCopyRightSection());
    assertEquals(0, section1.getCopyrightTexts().size());
    assertEquals(1, section1.getOrganizers().size());

    Section<QFDDOrganizer> section2copyright = qfddDocument.getSections().get(2);
    assertEquals("Copyright title 2", section2copyright.getTitle());
    assertEquals("Copyright tekst2.1", section2copyright.getText());
    assertEquals("da-DK", section2copyright.getLanguage());
    assertTrue(section2copyright.isCopyRightSection());
    assertEquals(1, section2copyright.getCopyrightTexts().size());
    assertEquals(0, section2copyright.getOrganizers().size());

    Section<QFDDOrganizer> section3copyright = qfddDocument.getSections().get(3);
    assertEquals("Copyright title 3", section3copyright.getTitle());
    assertEquals("Copyright tekst3.1", section3copyright.getText());
    assertEquals("da-DK", section3copyright.getLanguage());
    assertTrue(section3copyright.isCopyRightSection());
    assertEquals(1, section3copyright.getCopyrightTexts().size());
    assertEquals(0, section2copyright.getOrganizers().size());

    Section<QFDDOrganizer> section4 = qfddDocument.getSections().get(4);
    assertEquals("Indledning2", section4.getTitle());
    assertEquals(
        "OM DETTE SKEMA: Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>Hvornår havde du dit seneste anfald?",
        section4.getText());
    assertEquals(null, section4.getLanguage());
    assertFalse(section4.isCopyRightSection());
    assertEquals(0, section4.getCopyrightTexts().size());
    assertEquals(1, section4.getOrganizers().size());

    Section<QFDDOrganizer> section5copyright = qfddDocument.getSections().get(5);
    assertEquals("Copyright title sidste", section5copyright.getTitle());
    assertEquals("Copyright tekst sidste", section5copyright.getText());
    assertEquals("da-DK", section5copyright.getLanguage());
    assertTrue(section5copyright.isCopyRightSection());
    assertEquals(1, section5copyright.getCopyrightTexts().size());
    assertEquals(0, section5copyright.getOrganizers().size());

  }

  @Test
  public void testMultipleCopyRightWithTextReplaceBuildAndParse() throws Exception {

    QFDDXmlCodec codec = new QFDDXmlCodec();
    QFDDDocument qfddDocumentNew = new SetupQFDDVariationsExample()
        .createDocumentWithOneCopyRightNoTextTreeTextAndOneQuestion();

    assertNotNull(qfddDocumentNew);

    String xmlDocument = encodeDocument(qfddDocumentNew);
    assertNotNull(xmlDocument);

    QFDDDocument qfddDocument = codec.decode(xmlDocument);

    assertNotNull(qfddDocument);
    assertNotNull(qfddDocument.getSections());
    assertEquals(2, qfddDocument.getSections().size());

    Section<QFDDOrganizer> section = qfddDocument.getSections().get(0);
    assertEquals("Indledning1", section.getTitle());
    assertEquals("Question1<br/>Question2", section.getText());
    assertEquals(null, section.getLanguage());
    assertFalse(section.isCopyRightSection());
    assertEquals(0, section.getCopyrightTexts().size());
    assertEquals(1, section.getOrganizers().size());

    Section<QFDDOrganizer> sectionCopyright = qfddDocument.getSections().get(1);
    assertEquals("Copyright title 1", sectionCopyright.getTitle());
    assertEquals("Copyright tekst1.1<br/>Copyright tekst1.2<br/>Copyright tekst1.3", sectionCopyright.getText());
    assertEquals("da-DK", sectionCopyright.getLanguage());
    assertTrue(sectionCopyright.isCopyRightSection());
    assertEquals(3, sectionCopyright.getCopyrightTexts().size());
    assertEquals(0, sectionCopyright.getOrganizers().size());

  }

}
