package dk.s4.hl7.cda.convert.decode;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.convert.QRDXmlCodec;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public class TestXmlToQRDToXmlCompare extends BaseDecodeTest {

  private QRDXmlCodec codec = new QRDXmlCodec();

  @Before
  public void before() {
    codec = new QRDXmlCodec();
  }

  @Test
  public void testXmlToQRDToXmlNumericExample() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "qrd/TestNumericResponse.xml");
    QRDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToQRDToXmlMultipleChoiceExample() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "qrd/TestMultipleChoiceResponse.xml");
    QRDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToQRDToXmlMoreOrganizersWithinSectionExample()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "qrd/MoreOrganizersWithinSectionTest.xml");
    QRDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToQRDToXmlTextExample() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "qrd/TestTextResponse.xml");
    QRDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToQRDToXmlAnalogSliderExample() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "qrd/TestAnalogSliderResponse.xml");
    QRDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

}
