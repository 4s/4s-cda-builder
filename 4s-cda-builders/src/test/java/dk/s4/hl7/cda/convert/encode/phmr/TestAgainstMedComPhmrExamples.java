package dk.s4.hl7.cda.convert.encode.phmr;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;

/**
 * Validate the output from our own PHMR builder with the example from MedCom.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public final class TestAgainstMedComPhmrExamples extends BaseEncodeTest<PHMRDocument> implements ConcurrencyTestCase {

  public TestAgainstMedComPhmrExamples() {
    super("src/test/resources/phmr/");
  }

  @Before
  public void before() {
    setCodec(new PHMRXmlCodec());
  }

  @Test
  public void shouldMatchExpectedValueEx1() throws Exception {
    performTest("Ex1-Weight_measurement-MODIFIED.xml", SetupMedcomExample1.defineAsCDA());
  }

  @Test
  public void shouldMatchExpectedValueEx2() throws Exception {
    performTest("Ex2-Typing_error-MODIFIED.xml", SetupMedcomExample2.defineAsCDA());
  }

  @Test
  public void shouldMatchExpectedValueEx4() throws Exception {
    performTest("Ex4-COPD-MODIFIED.xml", SetupMedcomExample4.defineAsCDA());
  }

  @Test
  public void shouldMatchExpectedValueExKOL1() throws Exception {
    performTest("PHMR_KOL_Example_1_MaTIS-MODIFIED.xml", SetupMedcomKOLExample1.defineAsCDA());
  }

  @Override
  public void runTest() throws Exception {
    shouldMatchExpectedValueExKOL1();
  }
}