package dk.s4.hl7.cda.convert.encode.cpd.v20;

import java.util.Date;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CdaDateInterval;
import dk.s4.hl7.cda.model.CdaMeasurement;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument.Status;
import dk.s4.hl7.cda.model.cpd.v20.CPDGoalObservationComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDGoalObservationComponent.CPDGoalObservationComponentBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDGoalObservationEntry.CPDGoalObservationEntryBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthConcernObservation.CPDHealthConcernObservationBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusActEntry.CPDHealthStatusActEntryBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusComponent.CPDHealthStatusComponentBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDInterventionActComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDInterventionActComponent.CPDInterventionActComponentBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDInterventionActEntry.CPDInterventionActEntryBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDOutcomeObservationComponent;
import dk.s4.hl7.cda.model.cpd.v20.CPDOutcomeObservationComponent.CPDOutcomeObservationComponentBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDOutcomeObservationEntry.CPDOutcomeObservationEntryBuilder;
import dk.s4.hl7.cda.model.cpd.v20.CPDSection.CPDSectionBuilder;
import dk.s4.hl7.cda.model.util.DateUtil;

public class SetupMedcomExample1 {

  public static CPDDocument defineAsCDA() {

    // ********************************************************
    // HEADER
    // ********************************************************

    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("9be3ed20-4402-11e9-b475-0800200c9a66")
        .setRoot(MedCom.ROOT_OID)
        .build();
    CPDDocument cda = new CPDDocument(idHeader);

    cda.setTitle("Min forløbsplan for KOL");
    cda.setLanguageCode("da-DK");

    Date documentCreationTime = DateUtil.makeDanishDateTimeWithTimeZone(2019, 2, 11, 0, 0, 0);

    cda.setEffectiveTime(documentCreationTime);

    cda.setDocumentVersion(new IDBuilder().setRoot("1.2.208.184.100.1").build(), 1);
    cda.setCdaProfileAndVersion("cpd-v1.0.1");

    Patient patient = new Patient.PatientBuilder("Svendsen")
        .setGender(Patient.Gender.Female)
        .addGivenName("Lene")
        .setSSN("1011534240")
        .setAddress(new AddressData.AddressBuilder("1652", "København V")
            .setCountry("Danmark")
            .addAddressLine("Mosegårdsvangen 295")
            .setUse(AddressData.Use.HomeAddress)
            .build())
        .addTelecom(AddressData.Use.HomeAddress, "tel", "12345678")
        .setBirthDate(DateUtil.makeDanishDateTimeWithTimeZone(1953, 10, 10, 0, 0, 0))
        .build();
    cda.setPatient(patient);

    PersonIdentity personIdentityAuthor = new PersonIdentity.PersonBuilder("Andersen")
        .setPrefix("Læge")
        .addGivenName("Anders")
        .build();

    Date authorDate = DateUtil.makeDanishDateTimeWithTimeZone(2018, 4, 15, 10, 19, 0);
    cda.setAuthor(new ParticipantBuilder()
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .addAddressLine("Valdemarsgade 10")
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .setSOR("191901000016999")
        .setTime(authorDate)
        .setPersonIdentity(personIdentityAuthor)
        .setOrganizationIdentity(
            new OrganizationIdentity.OrganizationBuilder().setName("Lægehuset Valdemarsgade").build())
        .build());

    cda.setCustodian(new OrganizationIdentity.OrganizationBuilder()
        .setSOR("191901000016999")
        .setName("Lægehuset Valdemarsgade")
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .addAddressLine("Valdemarsgade 10")
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build())
        .build());

    Date from = DateUtil.makeDanishDateTimeWithTimeZone(2019, 2, 11, 0, 0, 0);
    cda.setDocumentationTimeInterval(from, null);

    // ********************************************************
    // HEALTH STATUS
    // ********************************************************

    CPDSectionBuilder<CPDHealthStatusComponent> cpdHealthSectionBuilder = new CPDSectionBuilder<CPDHealthStatusComponent>()
        .setTitle("Tilstande, diagnoser mm.")
        .setText(null);

    Participant authorStatusAct1 = new ParticipantBuilder()
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .addAddressLine("Valdemarsgade 10")
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .setSOR("191901000016999")
        .setTime(DateUtil.makeDanishDateTimeWithTimeZone(2018, 8, 25, 10, 30, 0))
        .setPersonIdentity(
            new PersonIdentity.PersonBuilder("Sørensen").setPrefix("SpecialLæge").addGivenName("Søren").build())
        .setOrganizationIdentity(new OrganizationIdentity.OrganizationBuilder()
            .setName("Lægehuset Valdemarsgade")
            .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
                .addAddressLine("Valdemarsgade 10")
                .setCountry("Danmark")
                .setUse(AddressData.Use.WorkPlace)
                .build())
            .build())
        .build();

    Participant authorStatusAct2 = new ParticipantBuilder()
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .addAddressLine("Valdemarsgade 10")
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .setSOR("191901000016999")
        .setTime(DateUtil.makeDanishDateTimeWithTimeZone(2019, 8, 25, 10, 30, 0))
        .setPersonIdentity(
            new PersonIdentity.PersonBuilder("Pedersen").setPrefix("SpecialLæge").addGivenName("Per").build())
        .setOrganizationIdentity(new OrganizationIdentity.OrganizationBuilder()
            .setName("Lægehuset Valdemarsgade")
            .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
                .addAddressLine("Valdemarsgade 10")
                .setCountry("Danmark")
                .setUse(AddressData.Use.WorkPlace)
                .build())
            .build())
        .build();

    CPDHealthStatusComponent cpdHealthStatusObservationComponent1 = new CPDHealthStatusComponentBuilder()
        .setTitle("Praktiske oplysninger")
        .setText("text")
        .setCode(new CodedValue("PracticalInformation", "1.2.208.184.100.1", "Praktiske oplysninger",
            "MedCom Message Codes"))
        .addEntry(new CPDHealthStatusActEntryBuilder()
            .setId(new IDBuilder().setRoot("1.2.208.184").setExtension("7b889ee9-2877-4824-94c2-b1dbe30324e8").build())
            .setStatusCode(Status.ACTIVE)
            .setEffectiveDate(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 0, 0, 0))
            .addAuthor(authorStatusAct1)
            .addAuthor(authorStatusAct2)
            .addObservation(new CPDHealthConcernObservationBuilder()
                .setId(
                    new IDBuilder().setRoot("1.2.208.184").setExtension("b1b0b657-1121-4607-995d-f20b549d767f").build())
                .setCode(new CodedValue("ALGA01", "1.2.208.176.2.4", "aktionsdiagnose",
                    "Sundhedsvæsenets Klassifikations System"))
                .setEffectiveTimeInterval(
                    new CdaDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 11, 21, 35), null, true))
                .setValueCode(new CodedValue("T90", "1.2.208.176.99.99.99", "Diabetes", "ICPC-2-DK"))
                .build())
            .addObservation(new CPDHealthConcernObservationBuilder()
                .setId(new IDBuilder()
                    .setRoot("1.2.208.184.100.1")
                    //                        .setExtension("b1b0b657-1121-4607-995d-f20b549d7602")
                    .build())
                .setCode(new CodedValue("ALGA01", "1.2.208.176.2.4", "aktionsdiagnose",
                    "Sundhedsvæsenets Klassifikations System"))
                .setEffectiveTimeInterval(
                    new CdaDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 11, 21, 35), null, true))
                .setValueCode(new CodedValue("T90", "1.2.208.176.99.99.99", "Diabetes", "ICPC-2-DK"))
                .build())
            .build())
        .addEntry(new CPDHealthStatusActEntryBuilder()
            .setId(new IDBuilder().setRoot("1.2.208.184").setExtension("7b889ee9-2877-4824-94c2-b1dbe3032402").build())
            .setStatusCode(Status.ACTIVE)
            .setEffectiveTime(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 0, 0, 0))
            .addObservation(new CPDHealthConcernObservationBuilder()
                .setId(
                    new IDBuilder().setRoot("1.2.208.184").setExtension("b1b0b657-1121-4607-995d-f20b549d027f").build())
                .setCode(new CodedValue("ALGA01", "1.2.208.176.2.4", "aktionsdiagnose",
                    "Sundhedsvæsenets Klassifikations System"))
                .setEffectiveTimeInterval(
                    new CdaDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 11, 21, 35), null, true))
                .setValueCode(new CodedValue("T90", "1.2.208.176.99.99.99", "Diabetes", "ICPC-2-DK"))
                .build())
            .addObservation(new CPDHealthConcernObservationBuilder()
                .setId(
                    new IDBuilder().setRoot("1.2.208.184").setExtension("b1b0b657-1121-4607-995d-f20b549d0202").build())
                .setCode(new CodedValue("ALGA01", "1.2.208.176.2.4", "aktionsdiagnose",
                    "Sundhedsvæsenets Klassifikations System"))
                .setEffectiveTimeInterval(
                    new CdaDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2012, 9, 17, 11, 21, 35), null, true))
                .setValueCode(new CodedValue("T90", "1.2.208.176.99.99.99", "Diabetes", "ICPC-2-DK"))
                .build())
            .build())

        .build();
    cpdHealthSectionBuilder.setComponent(cpdHealthStatusObservationComponent1);

    cda.setHealthConcernSection(cpdHealthSectionBuilder.build());

    // ********************************************************
    // GOAL OBSERVATION
    // ********************************************************

    CPDSectionBuilder<CPDGoalObservationComponent> cpdGoalSectionBuilder = new CPDSectionBuilder<CPDGoalObservationComponent>()
        .setTitle("Mål")
        .setText(null);

    Participant authorGoal1 = new ParticipantBuilder()
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .addAddressLine("Valdemarsgade 10")
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .setSOR("191901000016999")
        .setTime(DateUtil.makeDanishDateTimeWithTimeZone(2018, 8, 25, 10, 30, 0))
        .setPersonIdentity(
            new PersonIdentity.PersonBuilder("Hansen").setPrefix("SpecialLæge").addGivenName("Hans").build())
        .setOrganizationIdentity(
            new OrganizationIdentity.OrganizationBuilder().setName("Lægehuset Valdemarsgade").build())
        .build();

    Participant authorGoal2 = new ParticipantBuilder()
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .addAddressLine("Valdemarsgade 10")
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .setSOR("191901000016999")
        .setTime(DateUtil.makeDanishDateTimeWithTimeZone(2018, 8, 25, 10, 30, 0))
        .setPersonIdentity(
            new PersonIdentity.PersonBuilder("Larsen").setPrefix("SpecialLæge").addGivenName("Lars").build())
        .setOrganizationIdentity(new OrganizationIdentity.OrganizationBuilder()
            .setName("Lægehuset Valdemarsgade")
            .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
                .addAddressLine("Valdemarsgade 10")
                .setCountry("Danmark")
                .setUse(AddressData.Use.WorkPlace)
                .build())
            .build())
        .build();

    CPDGoalObservationComponent cpdGoalObservationComponent1 = new CPDGoalObservationComponentBuilder()
        .setTitle("Patientens genoptræningsbehov og potentiale")
        .setText("text2")
        .setCode(new CodedValue("RehabilitationNeedAndPotential", "1.2.208.184.100.1",
            "Patientens genoptræningsbehov og potentiale", "MedCom Message Codes"))
        .addEntry(new CPDGoalObservationEntryBuilder()
            .setId(new IDBuilder().setRoot("1.2.208.184.100.1").build())
            .setCode(new CodedValue("ZZ0172Y", "1.2.208.176.2.4",
                "Vurdering af behov for genoptræning, specialiseret genoptræning",
                "Sundhedsstyrelsens Klassifikations System"))
            .setText("<reference value=\"#RehabilitationNeedAndPotential\"/>")
            .setComment(null)
            .setEffectiveTimeInterval(
                new CdaDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 21, 10, 40, 00), null, true))
            .addAuthor(authorGoal1)
            .addAuthor(authorGoal2)
            .build())
        .addEntry(
            new CPDGoalObservationEntryBuilder()
                .setId(
                    new IDBuilder().setRoot("1.2.208.184").setExtension("ce7cfb78-bd16-467e-8bcf-859a30341002").build())
                .setCode(null)
                .setText("<reference value=\"#RehabilitationNeedAndPotential\"/>")
                .setComment(null)
                .setEffectiveTimeInterval(
                    new CdaDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 21, 10, 40, 00), null, true))
                .build())
        .addEntry(new CPDGoalObservationEntryBuilder()
            .setId(new IDBuilder().setRoot("1.2.208.184").setExtension("ce7cfb78-bd16-467e-8bcf-859a30340202").build())
            .setCode(new CodedValue("ZZ0172Y", "1.2.208.176.2.4",
                "Vurdering af behov for genoptræning, specialiseret genoptræning",
                "Sundhedsstyrelsens Klassifikations System"))
            .setText("<reference value=\"#RehabilitationNeedAndPotential\"/>")
            .setComment(null)
            .setEffectiveTimeInterval(
                new CdaDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 21, 10, 40, 00), null, true))
            .build())
        .build();
    cpdGoalSectionBuilder.setComponent(cpdGoalObservationComponent1);

    cda.setGoalSection(cpdGoalSectionBuilder.build());

    // ********************************************************
    // INTERVENTION ACT
    // ********************************************************

    CPDSectionBuilder<CPDInterventionActComponent> cpdInterventionSectionBuilder = new CPDSectionBuilder<CPDInterventionActComponent>()
        .setTitle("Udførte eller planlagte Interventioner")
        .setText(null);

    Participant authorInterventionAct1 = new ParticipantBuilder()
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .addAddressLine("Valdemarsgade 10")
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .setSOR("191901000016999")
        .setTime(DateUtil.makeDanishDateTimeWithTimeZone(2018, 8, 25, 10, 30, 0))
        .setPersonIdentity(
            new PersonIdentity.PersonBuilder("Johnsen").setPrefix("SpecialLæge").addGivenName("John").build())
        .setOrganizationIdentity(new OrganizationIdentity.OrganizationBuilder()
            .setName("Lægehuset Valdemarsgade")
            .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
                .addAddressLine("Valdemarsgade 10")
                .setCountry("Danmark")
                .setUse(AddressData.Use.WorkPlace)
                .build())
            .build())
        .build();

    Participant authorInterventionAct2 = new ParticipantBuilder()
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .addAddressLine("Valdemarsgade 10")
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .setSOR("191901000016999")
        .setTime(DateUtil.makeDanishDateTimeWithTimeZone(2018, 8, 25, 10, 30, 0))
        .setPersonIdentity(
            new PersonIdentity.PersonBuilder("Kurtsen").setPrefix("SpecialLæge").addGivenName("Kurt").build())
        .setOrganizationIdentity(
            new OrganizationIdentity.OrganizationBuilder().setName("Lægehuset Valdemarsgade").build())
        .build();

    Participant cpdInterventionActPerformer1 = new ParticipantBuilder()
        .setOrganizationIdentity(new OrganizationIdentity.OrganizationBuilder()
            .setName("Odense Universitetshospital, Ortopædkirurgisk Afdeling, Q")
            .addTelecom(Use.HomeAddress, "ean", "5790002012272")
            .setAddress(new AddressData.AddressBuilder("5000", "Odense C")
                .addAddressLine("Sdr. Boulevard29")
                .setUse(AddressData.Use.WorkPlace)
                .build())
            .build())
        .build();
    Participant cpdInterventionActPerformer2 = new ParticipantBuilder()
        .setOrganizationIdentity(new OrganizationIdentity.OrganizationBuilder()
            .setName("Odense Universitetshospital, Ortopædkirurgisk Afdeling, Q")
            .build())
        .build();
    CPDInterventionActComponent cpdInterventionActComponent1 = new CPDInterventionActComponentBuilder()
        .setTitle("Aktuel sygehuskontakt")
        .setText("text")
        .setCode(new CodedValue("CurrentHospitalContact", "1.2.208.184.100.1", "Aktuel sygehuskontakt",
            "MedCom Message Codes"))
        .addEntry(new CPDInterventionActEntryBuilder()
            .setId(new IDBuilder().setRoot("1.2.208.184").setExtension("9a6d1bac-17d3-4195-89a4-1121bc809b4d").build())
            .setCode(
                new CodedValue("AAF1", "1.2.208.176.2.4", "Indlæggelse", "Sundhedsstyrelsens Klassifikations System"))
            .setText("<reference value=\"#testing1\"/>")
            .setStatusCode(Status.COMPLETED)
            .setEffectiveTimeInterval(
                new CdaDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2014, 11, 13, 9, 1, 0),
                    DateUtil.makeDanishDateTimeWithTimeZone(2014, 11, 20, 0, 0, 0), true))
            .build())
        .addEntry(
            new CPDInterventionActEntryBuilder()
                .setId(
                    new IDBuilder().setRoot("1.2.208.184").setExtension("9a6d1bac-17d3-4195-89a4-1121bc809b02").build())
                .setCode(null)
                .setText("<reference value=\"#testing1\"/>")
                .setStatusCode(Status.COMPLETED)
                .setEffectiveTimeInterval(
                    new CdaDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2014, 11, 13, 9, 1, 0),
                        DateUtil.makeDanishDateTimeWithTimeZone(2014, 11, 20, 0, 0, 0), true))
                .build())
        .addEntry(new CPDInterventionActEntryBuilder()
            .setId(new IDBuilder().setRoot("1.2.208.184").setExtension("9a6d1bac-17d3-4195-89a4-1121bc809b4e").build())
            .setCode(new CodedValue("AAF23", "1.2.208.176.2.4", "Ambulant Kontrolbesøg",
                "Sundhedsvæsenets Klassifikations System"))
            .setStatusCode(Status.ACTIVE)
            .setEffectiveTimeInterval(
                new CdaDateInterval(DateUtil.makeDanishDateTimeWithTimeZone(2015, 0, 25, 10, 30, 0), null, true))
            .addPerformer(cpdInterventionActPerformer1)
            .addPerformer(cpdInterventionActPerformer2)
            .build())
        .addAuthor(authorInterventionAct1)
        .addAuthor(authorInterventionAct2)
        .build();
    cpdInterventionSectionBuilder.setComponent(cpdInterventionActComponent1);

    cda.setInterventionSection(cpdInterventionSectionBuilder.build());

    // ********************************************************
    // OUTCOME
    // ********************************************************

    CPDSectionBuilder<CPDOutcomeObservationComponent> cpdOutcomeObservationSectionBuilder = new CPDSectionBuilder<CPDOutcomeObservationComponent>()
        .setTitle("Resultater")
        .setText("Seneste fem målinger");

    Participant authorOutcomeObservation1 = new ParticipantBuilder()
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .addAddressLine("Valdemarsgade 10")
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .setSOR("191901000016999")
        .setTime(DateUtil.makeDanishDateTimeWithTimeZone(2018, 8, 25, 10, 30, 0))
        .setPersonIdentity(
            new PersonIdentity.PersonBuilder("Poulsen").setPrefix("SpecialLæge").addGivenName("Poul").build())
        .setOrganizationIdentity(new OrganizationIdentity.OrganizationBuilder()
            .setName("Lægehuset Valdemarsgade")
            .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
                .addAddressLine("Valdemarsgade 10")
                .setCountry("Danmark")
                .setUse(AddressData.Use.WorkPlace)
                .build())
            .build())
        .build();

    Participant authorOutcomeObservation2 = new ParticipantBuilder()
        .setAddress(new AddressData.AddressBuilder("5700", "Svendborg")
            .addAddressLine("Valdemarsgade 10")
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "65119999")
        .setSOR("191901000016999")
        .setTime(DateUtil.makeDanishDateTimeWithTimeZone(2018, 8, 25, 10, 30, 0))
        .setPersonIdentity(
            new PersonIdentity.PersonBuilder("Runesen").setPrefix("SpecialLæge").addGivenName("Rune").build())
        .setOrganizationIdentity(
            new OrganizationIdentity.OrganizationBuilder().setName("Lægehuset Valdemarsgade").build())
        .build();

    CPDOutcomeObservationComponent cpdOutcomeObservationComponent1 = new CPDOutcomeObservationComponentBuilder()
        .setTitle("Langtidsblodsukker (mmol/mol)")
        .setText("text")
        .addEntry(new CPDOutcomeObservationEntryBuilder()
            .setId(new IDBuilder().setRoot("1.2.208.184").setExtension("ca9dd558-40ac-4dd9-b028-0de1654fc630").build())
            .setCode(
                new CodedValue("MCS88023", "1.2.208.176.2.1", "FEV1 i % af den forventede værdi", "NPU terminologien"))
            .setEffectiveTime(DateUtil.makeDanishDateTimeWithTimeZone(2017, 5, 22, 12, 15, 45))
            .setCdaMeasurement(new CdaMeasurement("48", "%"))
            .build())
        .addEntry(new CPDOutcomeObservationEntryBuilder()
            .setId(new IDBuilder().setRoot("1.2.208.184").setExtension("90064452-79cd-4305-81bb-8a70df72a710").build())
            .setCode(
                new CodedValue("MCS88023", "1.2.208.176.2.1", "FEV1 i % af den forventede værdi", "NPU terminologien"))
            .setEffectiveDate(DateUtil.makeDanishDateTimeWithTimeZone(2017, 10, 3, 11, 36, 34))
            .setCdaMeasurement(new CdaMeasurement("75", "%"))
            .addAuthor(authorOutcomeObservation1)
            .addAuthor(authorOutcomeObservation2)
            .build())
        .addEntry(new CPDOutcomeObservationEntryBuilder()
            .setId(new IDBuilder().setRoot("1.2.208.184").setExtension("ca9dd558-40ac-4dd9-b028-0de1654f0230").build())
            .setCode(
                new CodedValue("MCS88023", "1.2.208.176.2.1", "FEV1 i % af den forventede værdi", "NPU terminologien"))
            .setEffectiveTime(DateUtil.makeDanishDateTimeWithTimeZone(2017, 5, 22, 12, 15, 45))
            .setCdaMeasurement(new CdaMeasurement("48", "%"))
            .build())
        .build();
    cpdOutcomeObservationSectionBuilder.setComponent(cpdOutcomeObservationComponent1);

    cda.setOutcomeSection(cpdOutcomeObservationSectionBuilder.build());

    return cda;
  }
}
