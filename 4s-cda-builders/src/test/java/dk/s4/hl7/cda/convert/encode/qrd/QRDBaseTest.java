package dk.s4.hl7.cda.convert.encode.qrd;

import java.util.Date;
import java.util.UUID;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.QRDXmlCodec;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity.OrganizationBuilder;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

/**
 * Base class for testing QFDD
 *
 * @author Frank Jacobsen, Systematic
 */
public class QRDBaseTest extends BaseEncodeTest<QRDDocument> {

  public QRDBaseTest() {
    super("src/test/resources/qrd");
    setCodec(new QRDXmlCodec());
  }

  protected QRDDocument createBaseQRDDocument() {
    return createBaseQRDDocument(UUID.randomUUID().toString());
  }

  protected QRDDocument createBaseQRDDocument(String documentId) {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 13, 10, 0, 0);
    // Create document
    QRDDocument qrdDocument = new QRDDocument(MedCom.createId(documentId));
    qrdDocument.setLanguageCode("da-DK");
    qrdDocument.setTitle("KOL spørgeskema");
    qrdDocument.setDocumentVersion(new IDBuilder().setRoot(MedCom.MESSAGECODE_OID).setExtension("2358344").build(), 1);
    qrdDocument.setEffectiveTime(documentCreationTime);
    // Create Patient
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    qrdDocument.setPatient(nancy);
    // Create Custodian organization
    qrdDocument.setCustodian(new OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build());

    // Author is the patient self
    qrdDocument.setAuthor(new ParticipantBuilder()
        .setAddress(nancy.getAddress())
        .setId(nancy.getId())
        .setTelecomList(nancy.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(nancy)
        .build());

    // 1.4 Define the service period
    Date from = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 6, 8, 2, 0);
    Date to = DateUtil.makeDanishDateTimeWithTimeZone(2014, 0, 10, 8, 15, 0);
    qrdDocument.setDocumentationTimeInterval(from, to);
    return qrdDocument;
  }
}
