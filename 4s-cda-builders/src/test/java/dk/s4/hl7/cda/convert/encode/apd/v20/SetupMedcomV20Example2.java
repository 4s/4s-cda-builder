package dk.s4.hl7.cda.convert.encode.apd.v20;

import java.util.Date;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.OrganizationIdentity.OrganizationBuilder;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.AppointmentEncounterCode;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument.Status;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public class SetupMedcomV20Example2 {

  private static final String DOCUMENT_ID = "31af7e14-891c-48f0-a414-fd432289bf7d";

  public static AppointmentDocument createBaseAppointmentDocument() {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTimeWithTimeZone(2019, 11, 31, 10, 0, 0);

    Date authorTime = DateUtil.makeDanishDateTimeWithTimeZone(2019, 7, 16, 10, 0, 0);

    // Create document
    AppointmentDocument appointment = new AppointmentDocument(MedCom.createId(DOCUMENT_ID));
    appointment.setLanguageCode("da-DK");
    appointment.setTitle("Aftale for 2512489996");
    appointment.setEffectiveTime(documentCreationTime);
    // Create Patient
    Patient nancy = Setup.defineNancyAsFullPersonIdentity("2512489996");

    appointment.setPatient(nancy);
    // Create Custodian organization
    OrganizationIdentity custodianOrganization = new OrganizationBuilder()
        .setSOR("378631000016009")
        .setName("OUH Klinisk IT (Odense)")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("J. B. Winsløwsvej 4 1")
            .setCity("Odense C")
            .setPostalCode("5000")
            .setCountry("Danmark")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "66113333-2")
        .build();
    appointment.setCustodian(custodianOrganization);

    OrganizationIdentity authorOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setName("OUH Radiologisk Afdeling (Svendborg)")
        .setAddress(Setup.defineValdemarsGade53Address())
        .addTelecom(Use.WorkPlace, "tel", "65113333-1")
        .build();

    PersonIdentity jensJensen = new PersonBuilder("Jensen").addGivenName("Jens").setPrefix("Læge").build();
    PersonIdentity laegeAndersAndersen = new PersonBuilder("Andersen").addGivenName("Anders").setPrefix("Læge").build();

    appointment.setAuthor(new ParticipantBuilder()
        //        .setAddress(authorOrganization.getAddress())
        .setSOR("378631000016009")
        .setTelecomList(authorOrganization.getTelecomList())
        .setTime(authorTime)
        .setPersonIdentity(jensJensen)
        .setOrganizationIdentity(authorOrganization)
        .build());

    // 1.4 Define the service period       
    Date from = DateUtil.makeDanishDateTimeWithTimeZone(2019, 11, 31, 9, 0, 0);
    Date to = DateUtil.makeDanishDateTimeWithTimeZone(2019, 11, 31, 12, 0, 0);

    appointment.setDocumentationTimeInterval(from, to);

    appointment.setCdaProfileAndVersion("apd-v2.0.1");
    appointment.setEpisodeOfCareLabel("DiabetesPackage");

    appointment.addEpisodeOfCareIdentifier(new ID.IDBuilder()
        //            .setAuthorityName("MedCom")
        .setExtension("39d615cd-5d62-4a54-9762-d33197c63aba").setRoot("1.2.208.184").build());

    appointment.addEpisodeOfCareIdentifier(new ID.IDBuilder()
        .setAuthorityName("MedCom")
        .setExtension("e7532c08-729b-4413-83d7-bd2cdf147ef7")
        .setRoot("1.2.208.184")
        .build());

    OrganizationIdentity appointmentLocation = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("320161000016005")
        .setName("OUH Radiologisk Ambulatorium (Nyborg)")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Vestergade 17")
            .setCity("Nyborg")
            .setPostalCode("5800")
            .setCountry("Danmark")
            .setUse(Use.WorkPlace)
            .build())
        .addTelecom(Use.WorkPlace, "tel", "66113333-4")
        .build();

    appointment.setAppointmentTitle("Aftale");
    appointment.setAppointmentText("<paragraph>Aftale:</paragraph>" + "<table width=\"100%\">" + "<tbody>" + "<tr>"
        + "<th>Aftale dato</th>" + "<th>Vedrørende</th>" + "<th>Mødested</th>" + "<th>Kommentar</th>" + "</tr>" + "<tr>"
        + "<td>2019-12-31 09:00 - 2019-12-31 12:00. Tidspunktet er vejledende</td>" + "<td>Hjemmehjælpsbesøg</td>"
        + "<td>Borgers Hjemmeadresse</td>" + "<td>Aftalen er en del af et repeterende mønster</td>" + "</tr>"
        + "</tbody>" + "</table>");
    appointment.setAppointmentId(new IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension("9a6d1bac-17d3-4195-89a4-1121bc809b4d")
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build());
    appointment.setAppointmentStatus(Status.ACTIVE);

    appointment.setIndicationCode(new CodedValue.CodedValueBuilder()
        .setCode("17436001")
        .setDisplayName("lægekonsultation med ambulant patient")
        .setCodeSystem("2.16.840.1.113883.6.96")
        .build());

    appointment.setAppointmentLocation(appointmentLocation);

    PersonIdentity andersAndersen = new PersonBuilder("Andersen").addGivenName("Anders").build();
    Participant appointmentAuthor = new ParticipantBuilder()
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Toldbodvej 9")
            .setCity("Svendborg")
            .setPostalCode("5700")
            .setCountry("Danmark")
            .setUse(Use.WorkPlace)
            .build())
        .setSOR("48681000016007")
        .addTelecom(Use.WorkPlace, "tel", "62214518")
        .setTime(authorTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(new OrganizationIdentity.OrganizationBuilder().setName("Lægerne Toldbodvej").build())
        .build();
    appointment.setAppointmentAuthor(appointmentAuthor);

    OrganizationIdentity organizationIdentity = new OrganizationIdentity.OrganizationBuilder()
        .setName("Hjemmehjælp, afdeling City, Odense Kommune")
        .build();

    Participant appointmentPerformer = new ParticipantBuilder()
        .setAddress(Setup.defineVestergade5Address())
        .setSOR("378631000016009")
        .addTelecom(Use.WorkPlace, "tel", "66113333-3")
        .setTime(authorTime)
        .setPersonIdentity(laegeAndersAndersen)
        .setOrganizationIdentity(organizationIdentity) //Hjemmehjælp, afdeling City, Odense Kommune
        .build();

    appointment.setAppointmentPerformer(appointmentPerformer);

    appointment.setAppointmentEncounterCode(AppointmentEncounterCode.MunicipalityAppointment);

    return appointment;
  }

}
