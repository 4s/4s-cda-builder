package dk.s4.hl7.cda.convert.encode.qfdd;

import java.io.IOException;
import java.net.URISyntaxException;

import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qfdd.QFDDAnalogSliderQuestion.QFDDAnalogSliderQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDCriterion.QFDDCriterionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDDiscreteSliderQuestion.QFDDDiscreteSliderQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDFeedback.QFDDFeedbackBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDHelpText.QFDDHelpTextBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDMultipleChoiceQuestion.QFDDMultipleChoiceQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDNumericQuestion.QFDDNumericQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer.QFDDOrganizerBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDOrganizer;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition.QFDDPreconditionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion.QFDDTextQuestionBuilder;

public final class SetupQFDDVariationsExample extends QFDDExampleBase {
  private static final String QUESTION = "question";
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";
  private String text = "OM DETTE SKEMA: "
      + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
      + "Hvornår havde du dit seneste anfald?";

  protected QFDDQuestion simpleQuestion() {
    return simpleQuestion(QUESTION);
  }

  protected QFDDQuestion simpleQuestion(String question) {
    return new QFDDTextQuestionBuilder()
        .setCodeValue(new CodedValue("value1", "value2", "value3", "value4"))
        .setId(MedCom.createId("idExtension1"))
        .setQuestion(question)
        .build();
  }

  protected QFDDQuestion textQuestion(boolean includeMedia) throws IOException, URISyntaxException {
    QFDDTextQuestionBuilder qfddTextQuestionBuilder = new QFDDTextQuestionBuilder()
        .setCodeValue(new CodedValue("value1", "value2", "value3", "value4"))
        .setId(MedCom.createId("idExtension1"))
        .setQuestion(QUESTION)
        .setFeedback(new QFDDFeedbackBuilder().feedBackText("feedbacktext1").language("da-DK").build())
        .setHelpText(new QFDDHelpTextBuilder().helpText("helptext").language("da-DK").build());

    if (includeMedia) {
      qfddTextQuestionBuilder.setQuestionnaireMedia(getMedia());
    }

    return qfddTextQuestionBuilder.build();
  }

  protected QFDDQuestion textQuestionNoDisplayName(boolean includeMedia) throws IOException, URISyntaxException {
    QFDDTextQuestionBuilder qfddTextQuestionBuilder = new QFDDTextQuestionBuilder()
        .setCodeValue(new CodedValue("value1", "value2", "value4"))
        .setId(MedCom.createId("idExtension1"))
        .setQuestion(QUESTION)
        .setFeedback(new QFDDFeedbackBuilder().feedBackText("feedbacktext1").language("da-DK").build())
        .setHelpText(new QFDDHelpTextBuilder().helpText("helptext").language("da-DK").build());

    if (includeMedia) {
      qfddTextQuestionBuilder.setQuestionnaireMedia(getMedia());
    }

    return qfddTextQuestionBuilder.build();
  }

  private QFDDQuestion numericQuestion(boolean includeInterval, boolean includeDisplayName)
      throws IOException, URISyntaxException {
    CodedValue questionCodedValue = new CodedValue("value1", "value2", "value3", "value4");
    QFDDNumericQuestionBuilder qBuilder = new QFDDNumericQuestionBuilder()
        .setId(MedCom.createId("idExtension1"))
        .setQuestion(QUESTION)
        .setFeedback(new QFDDFeedbackBuilder()
            .feedBackText("feedbacktext1")
            .language("da-DK")
            .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
                .setMaximum("20")
                .setMinimum("1")
                .setValueType(IntervalType.IVL_INT)
                .build()).build())
            .build())
        .setHelpText(new QFDDHelpTextBuilder().helpText("helptext").language("da-DK").build())
        .setQuestionnaireMedia(getMedia())
        .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
            .setMaximum("20")
            .setMinimum("1")
            .setValueType(IntervalType.IVL_INT)
            .build()).build());
    if (includeDisplayName) {
      qBuilder.setCodeValue(new CodedValue("value1", "value2", "value3", "value4"));
    } else {
      qBuilder.setCodeValue(new CodedValue("value1", "value2", "value4"));
    }
    if (includeInterval) {
      qBuilder.setInterval("1", "5", IntervalType.IVL_INT);
    }
    return qBuilder.build();
  }

  protected QFDDQuestion multipleChoiceQuestion(boolean includeDisplayName, QFDDTextQuestion associatedTextQuestion)
      throws IOException, URISyntaxException {
    QFDDMultipleChoiceQuestionBuilder qBuilder = new QFDDMultipleChoiceQuestionBuilder()
        .setInterval(1, 2)
        .setId(MedCom.createId("idExtension1"))
        .addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited", "Some-CodeSystem-Name")
        .setQuestion(QUESTION)
        .setAssociatedTextQuestion(associatedTextQuestion);
    if (includeDisplayName) {
      qBuilder.setCodeValue(new CodedValue("value1", "value2", "value3", "value4"));
    } else {
      qBuilder.setCodeValue(new CodedValue("value1", "value2", "value4"));
    }
    return qBuilder.build();
  }

  protected QFDDQuestion multipleDiscreteSlider(boolean includeDisplayName, QFDDTextQuestion associatedTextQuestion) {
    QFDDDiscreteSliderQuestionBuilder qBuilder = new QFDDDiscreteSliderQuestionBuilder()
        .setMinimum(1)
        .setId(MedCom.createId("idExtension1"))
        .addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited", "Some-CodeSystem-Name")
        .addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited", "Some-CodeSystem-Name")
        .setQuestion(QUESTION)
        .setAssociatedTextQuestion(associatedTextQuestion);
    if (includeDisplayName) {
      qBuilder.setCodeValue(new CodedValue("value1", "value2", "value3", "value4"));
    } else {
      qBuilder.setCodeValue(new CodedValue("value1", "value2", "value4"));
    }
    return qBuilder.build();
  }

  protected QFDDQuestion analogSliderQuestion(boolean includeInterval, boolean includeDisplayName) {
    QFDDAnalogSliderQuestionBuilder qBuilder = new QFDDAnalogSliderQuestionBuilder()
        .setId(MedCom.createId("idExtension1"))
        .setQuestion(QUESTION)
        .setFeedback(new QFDDFeedbackBuilder().feedBackText("feedbacktext1").language("da-DK").build())
        .setHelpText(new QFDDHelpTextBuilder().helpText("helptext").language("da-DK").build())
        .addPrecondition(
            new QFDDPreconditionBuilder(new QFDDCriterionBuilder(new CodedValue("value1", "value2", "value3", "value4"))
                .setMaximum("20")
                .setMinimum("10")
                .setValueType(IntervalType.IVL_INT)
                .build()).build());
    if (includeDisplayName) {
      qBuilder.setCodeValue(new CodedValue("value1", "value2", "value3", "value4"));
    } else {
      qBuilder.setCodeValue(new CodedValue("value1", "value2", "value4"));
    }
    if (includeInterval) {
      qBuilder.setInterval("1", "100", "1");
    }
    return qBuilder.build();
  }

  private Section<QFDDOrganizer> createSectionWithQuestions(String title) throws IOException, URISyntaxException {

    Section<QFDDOrganizer> section = new Section<QFDDOrganizer>(title, text);
    section.addOrganizer(new QFDDOrganizerBuilder()
        .addQFDDQuestion(simpleQuestion())
        .addQFDDQuestion(textQuestion(true))
        .addQFDDQuestion(textQuestion(false))
        .build());
    return section;
  }

  private Section<QFDDOrganizer> createSectionWithNumericQuestion(String title, boolean includeInterval,
      boolean includeDisplayName) throws IOException, URISyntaxException {
    Section<QFDDOrganizer> section = new Section<QFDDOrganizer>(title, text);
    section.addOrganizer(
        new QFDDOrganizerBuilder().addQFDDQuestion(numericQuestion(includeInterval, includeDisplayName)).build());
    return section;
  }

  private Section<QFDDOrganizer> createSectionWithMultipleChoice(String title, boolean includeDisplayName)
      throws IOException, URISyntaxException {
    Section<QFDDOrganizer> section = new Section<QFDDOrganizer>(title, text);
    section.addOrganizer(
        new QFDDOrganizerBuilder().addQFDDQuestion(multipleChoiceQuestion(includeDisplayName, null)).build());
    return section;
  }

  private Section<QFDDOrganizer> createSectionWithDiscreteSlider(String title, boolean includeDisplayName)
      throws IOException, URISyntaxException {
    Section<QFDDOrganizer> section = new Section<QFDDOrganizer>(title, text);
    section.addOrganizer(
        new QFDDOrganizerBuilder().addQFDDQuestion(multipleDiscreteSlider(includeDisplayName, null)).build());
    return section;
  }

  private Section<QFDDOrganizer> createSectionWithAnalogSliderQuestion(String title, boolean includeInterval,
      boolean includeDisplayName) throws IOException, URISyntaxException {
    Section<QFDDOrganizer> section = new Section<QFDDOrganizer>(title, text);
    section.addOrganizer(
        new QFDDOrganizerBuilder().addQFDDQuestion(analogSliderQuestion(includeInterval, includeDisplayName)).build());
    return section;
  }

  private Section<QFDDOrganizer> createSectionWithoutQuestions(String title) throws IOException, URISyntaxException {
    Section<QFDDOrganizer> section = new Section<QFDDOrganizer>(title, text);
    return section;
  }

  private Section<QFDDOrganizer> createSectionWithTextQuestion(String title, boolean includeInterval,
      boolean includeDisplayName) throws IOException, URISyntaxException {
    return createSectionWithTextQuestion(title, includeInterval, includeDisplayName, this.text);
  }

  private Section<QFDDOrganizer> createSectionWithTextQuestion(String title, boolean includeInterval,
      boolean includeDisplayName, String text) throws IOException, URISyntaxException {
    Section<QFDDOrganizer> section = new Section<QFDDOrganizer>(title, text);
    if (includeDisplayName) {
      section.addOrganizer(new QFDDOrganizerBuilder().addQFDDQuestion(textQuestion(false)).build());

    } else {
      section.addOrganizer(new QFDDOrganizerBuilder().addQFDDQuestion(textQuestionNoDisplayName(false)).build());
    }
    return section;
  }

  private Section createCopyrightSection(String title, String text) {
    Section section = new Section(title, text, "da-DK");
    section.addCopyrightText(text);
    return section;
  }

  public QFDDDocument createDocument() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);
    return cda;
  }

  public QFDDDocument createDocumentTitleNull() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);
    cda.addSection(createSectionWithQuestions(null));
    cda.addSection(createSectionWithoutQuestions(null));
    cda.addSection(createCopyrightSection(null, "Copyright tekst skrives her"));
    return cda;
  }

  public QFDDDocument createDocumentNumericQuestionNoInterval() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);
    cda.addSection(createSectionWithNumericQuestion("Indledning", false, true));
    return cda;
  }

  public QFDDDocument createDocumentNumericQuestion() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);
    cda.addSection(createSectionWithNumericQuestion("Indledning", true, true));
    return cda;
  }

  public QFDDDocument createDocumentAnalogSliderQuestionNoInterval() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);
    cda.addSection(createSectionWithAnalogSliderQuestion("Indledning", false, true));
    return cda;
  }

  public QFDDDocument createDocumentQuestionNoDisplayName() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);
    cda.addSection(createSectionWithTextQuestion("Indledning", true, false));
    cda.addSection(createSectionWithAnalogSliderQuestion("Indledning", true, false));
    cda.addSection(createSectionWithNumericQuestion("Indledning", true, false));
    cda.addSection(createSectionWithMultipleChoice("Indledning", false));
    cda.addSection(createSectionWithDiscreteSlider("Indledning", false));
    return cda;
  }

  public QFDDDocument createDocumentWithMultipleCopyRightAndQuestions() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);

    Section<QFDDOrganizer> section0copyright = new Section("Copyright title 1",
        "Copyright tekst1.1 + Copyright tekst1.2", "da-DK");
    section0copyright.addCopyrightText("Copyright tekst1.1");
    section0copyright.addCopyrightText("Copyright tekst1.2");
    cda.addSection(section0copyright);

    cda.addSection(createSectionWithTextQuestion("Indledning1", true, false));

    Section section2copyright = new Section("Copyright title 2", "Copyright tekst2.1", "da-DK");
    section2copyright.addCopyrightText("Copyright tekst2.1");
    cda.addSection(section2copyright);

    Section section3copyright = new Section("Copyright title 3", "Copyright tekst3.1", "da-DK");
    section3copyright.addCopyrightText("Copyright tekst3.1");
    cda.addSection(section3copyright);

    cda.addSection(createSectionWithTextQuestion("Indledning2", true, false));

    Section sectionLastcopyright = new Section("Copyright title sidste", "Copyright tekst sidste", "da-DK");
    sectionLastcopyright.addCopyrightText("Copyright tekst sidste");
    cda.addSection(sectionLastcopyright);

    return cda;
  }

  public QFDDDocument createDocumentWithOneCopyRightNoTextTreeTextAndOneQuestion() throws Exception {
    QFDDDocument cda = createBaseQFDDDocument(DOCUMENT_ID);

    String narrativText = "Question1<br/>Question2";
    cda.addSection(createSectionWithTextQuestion("Indledning1", true, false, narrativText));

    Section<QFDDOrganizer> section0copyright = new Section("Copyright title 1", null, "da-DK");
    section0copyright.addCopyrightText("Copyright tekst1.1");
    section0copyright.addCopyrightText("Copyright tekst1.2");
    section0copyright.addCopyrightText("Copyright tekst1.3");
    cda.addSection(section0copyright);

    return cda;
  }

}
