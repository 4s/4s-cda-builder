package dk.s4.hl7.cda.convert.encode.apd.v20;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.APDV20XmlCodec;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;

/**
 * Validate the output from our own appointment builder with the example from
 * MedCom 2.0.
 *
 */
public final class TestAgainstMedComApdV20Examples extends BaseEncodeTest<AppointmentDocument>
    implements ConcurrencyTestCase {

  public TestAgainstMedComApdV20Examples() {
    super("src/test/resources/apd/v20/");
  }

  @Before
  public void before() {
    setCodec(new APDV20XmlCodec());
  }

  @Test
  public void shouldMatchExpectedValueExample1() throws Exception {
    performTest("DK-APD_Example_apd-2-0_example1.xml", SetupMedcomV20Example1.createBaseAppointmentDocument());
  }

  @Test
  public void shouldMatchExpectedValueExample2() throws Exception {
    performTest("DK-APD_Example_apd-2-0_example2.xml", SetupMedcomV20Example2.createBaseAppointmentDocument());
  }

  @Test
  public void shouldMatchExpectedValueExampleV10UpgradedTOV20() throws Exception {
    performTest("DK-APD_Example_1 _V10_upgraded_to_V20.xml",
        SetupMedcomV20ExampleV10UpgradeToV20.createBaseAppointmentDocument());
  }

  @Test
  public void shouldMatchExpectedValueExample_1_6_apd_gp() throws Exception {
    performTest("DK-APD_Example_1_6_apd_gp_adj.xml", SetupMedcomV20Example16ApdGp.createBaseAppointmentDocument());
  }

  @Test
  public void shouldMatchExpectedValueExample_1_6_apd_gp_v2() throws Exception { //Adjustment for 2.0.1 with version number and extra author (referer)
    performTest("DK-APD_Example_1_6_apd_gp_v2_adj.xml", SetupMedcomV20Example16ApdGpV2.createBaseAppointmentDocument());
  }

  @Test
  public void shouldMatchExpectedValueExample_1_4_apd_municipality_a() throws Exception {
    performTest("DK-APD_Example_1_4_apd_municipality_a_adj.xml",
        SetupMedcomV20Example14ApdMunicipalityA.createBaseAppointmentDocument());
  }

  @Test
  public void shouldMatchExpectedValueExample_1_5_apd_region() throws Exception {
    performTest("DK-APD_Example_1_5_apd_region_adj.xml",
        SetupMedcomV20Example15ApdRegion.createBaseAppointmentDocument());
  }

  @Override
  public void runTest() throws Exception {
    shouldMatchExpectedValueExample1();
  }
}