package dk.s4.hl7.cda.convert.encode.qrd;

import java.io.IOException;
import java.net.URISyntaxException;

import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Reference.DocumentIdReferencesUse;
import dk.s4.hl7.cda.model.Reference.ReferenceBuilder;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.QuestionnaireMedia.QuestionnaireMediaBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse.QRDMultipleChoiceResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer.QRDOrganizerBuilder;
import dk.s4.hl7.cda.model.qrd.QRDOrganizer;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse.QRDTextResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse;

/**
 * Test MultibleChoise response.
 *
 * @author Frank Jacobsen, Systematic
 * 
 */
public final class SetupQRDMultipleChoiceExample extends QRDExampleBase {
  private static final String QUESTION = "question";
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";

  private QRDResponse responseWithReference() throws IOException, URISyntaxException {
    return new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "1", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setInterval(2, 5)
        .addAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .addAnswer("answerCode2", "answerCodeSystem2", "answerDisplayName2", "answerCodeSystemName2")
        .addAnswer("answerCode3", "answerCodeSystem3", "answerDisplayName3", "answerCodeSystemName3")
        .addReference(simpleDocument())
        .setQuestionnaireMedia(getMedia())
        .build();
  }

  private Reference simpleDocument() {
    return new ReferenceBuilder(DocumentIdReferencesUse.CDA_DOCUMENT_ID_REFERENCE.getReferencesUse(),
        MedCom.createId("referenceId"), Loinc.createPHMRTypeCode()).build();
  }

  protected QRDResponse optionalResponseWithAnswer() {
    return new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "1", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setInterval(2, 5)
        .addAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .addAnswer("answerCode2", "answerCodeSystem2", "answerDisplayName2", "answerCodeSystemName2")
        .addAnswer("answerCode3", "answerCodeSystem3", "answerDisplayName3", "answerCodeSystemName3")
        .build();
  }

  protected QRDResponse optionalResponseNoAnswer() {
    return new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "1", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setInterval(0, 5)
        .build();
  }

  protected QRDResponse responseWithAssociatedTextResponse() {
    return new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("Q2228", "1.2.208.176.1.5", "Jeg ønsker støtte til at:", "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5")
            .setExtension("2228")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Jeg ønsker støtte til at:")
        .setInterval(1, 8)
        .addAnswer("A2228_7", "1.2.208.176.1.5", "... andet", "Sundhedsdatastyrelsen")
        .setAssociatedTextResponse(createAssociatedTextResponse())
        .build();
  }

  private QRDTextResponse createAssociatedTextResponse() {

    return new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("Q2229", "1.2.208.176.1.5", "Her kan du skrive, hvad du ønsker støtte til",
            "Sundhedsdatastyrelsen"))
        .setId(new IDBuilder()
            .setRoot("1.2.208.176.1.5")
            .setExtension("2229")
            .setAuthorityName("Sundhedsdatastyrelsen")
            .build())
        .setQuestion("Her kan du skrive, hvad du ønsker støtte til")
        .setText("Jeg ønsker støtte til 123 og ABC")
        .setQuestionnaireMedia(new QuestionnaireMediaBuilder()
            .representation("B64")
            .mediaType("image/png")
            .value("megetKortOgIkkeValid")
            .id("fememotes-400_dxt")
            .build())
        .build();
  }

  @Override
  public QRDDocument createDocument() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    Section<QRDOrganizer> section = new Section<QRDOrganizer>("Questions", "Questions");
    section.addOrganizer(new QRDOrganizerBuilder()
        .addId(MedCom.createId("idExtensionOrganizer"))
        .addQRDResponse(optionalResponseWithAnswer())
        .addQRDResponse(optionalResponseNoAnswer())
        .addQRDResponse(responseWithReference())
        .build());
    cda.addSection(section);
    return cda;
  }

  public QRDDocument createDocumentWithAssociatedText() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    Section<QRDOrganizer> section = new Section<QRDOrganizer>(
        "Ønsker til støtte eller opbakning til håndtering af diabetes", "text");
    section.addOrganizer(new QRDOrganizerBuilder()
        .addId(MedCom.createId("idExtensionOrganizer"))
        .addQRDResponse(responseWithAssociatedTextResponse())
        .build());
    cda.addSection(section);
    return cda;
  }

}
