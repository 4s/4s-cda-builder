package dk.s4.hl7.cda.convert.encode.apd.v20;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.APDV20XmlCodec;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;

/**
 * Validates various aspects of the XML generation 
 *
 * Will be extended when builder/parser is adjusted
 */
public final class TestApdV20BuildVariations extends BaseEncodeTest<AppointmentDocument> {

  public TestApdV20BuildVariations() {
    super("src/test/resources/apd/v20/");
  }

  @Before
  public void before() {
    setCodec(new APDV20XmlCodec());
  }

  @Test
  public void testServiceEventEffectTimeHighNotNull() throws Exception {
    AppointmentDocument appointmentDocument = SetupMedcomV20Example14ApdMunicipalityA.createBaseAppointmentDocument();
    String appointmentXml = encodeDocument(appointmentDocument);

    assertNotNull(appointmentXml);
    assertTrue(appointmentXml.contains("<low value=\"20201130090000+0100\"/>"));
    assertTrue(appointmentXml.contains("<high value=\"20201130120000+0100\"/>"));

  }

  @Test
  public void testServiceEventEffectTimeHighNull() throws Exception {
    AppointmentDocument appointmentDocument = SetupMedcomV20Example14ApdMunicipalityA.createBaseAppointmentDocument();
    appointmentDocument.setDocumentationTimeInterval(appointmentDocument.getServiceStartTime(), null);
    String appointmentXml = encodeDocument(appointmentDocument);

    assertNotNull(appointmentXml);
    assertTrue(appointmentXml.contains("<low value=\"20201130090000+0100\"/>"));
    assertTrue(appointmentXml.contains("<high nullFlavor=\"NI\"/>"));

  }

  @Test
  public void testAuthorReferrerNotNullGeneratesAuthorElement() throws Exception {
    AppointmentDocument appointmentDocument = SetupMedcomV20Example14ApdMunicipalityA.createBaseAppointmentDocument();
    String appointmentXml = encodeDocument(appointmentDocument);

    assertNotNull(appointmentXml);
    assertTrue(appointmentXml.contains(
        "<code code=\"REFB\" codeSystem=\"2.16.840.1.113883.5.90\" displayName=\"ParticipationReferredBy\" codeSystemName=\"ParticipationType (HL7) Code System\"/>"));
    assertFalse(appointmentXml.contains("<author nullFlavor=\"NI\">"));

  }

  @Test
  public void testAppointmentLocationSBJWithoutUsingAppointmentLocationHomeAddress() throws Exception {
    AppointmentDocument appointmentDocument = SetupMedcomV20Example14ApdMunicipalityA.createBaseAppointmentDocument();
    appointmentDocument.setAppointmentLocation(null);
    appointmentDocument.setAppointmentLocationHomeAddress(null);
    OrganizationIdentity appointmentLocation = new OrganizationIdentity.OrganizationBuilder()
        //                    .setSOR("393421000016009")
        .setName("Borgers Hjemmeadresse")
        .setAddress(new AddressData.AddressBuilder()
            .addAddressLine("Forskerparken 10")
            .addAddressLine("MedCom")
            .setCity("Odense M")
            .setPostalCode("5230")
            .setUse(Use.HomeAddress)
            .build())
        .addTelecom(Use.HomeAddress, "tel", "69894534")
        .build();
    appointmentDocument.setAppointmentLocation(appointmentLocation);
    String appointmentXml = encodeDocument(appointmentDocument);

    assertNotNull(appointmentXml);
    assertTrue(appointmentXml.contains("<participant typeCode=\"SBJ\">"));

  }

  @Test
  public void testAuthorReferrerNullMustNotGenerateNullFlavor() throws Exception {
    AppointmentDocument appointmentDocument = SetupMedcomV20Example14ApdMunicipalityA.createBaseAppointmentDocument();
    appointmentDocument.setAuthorReferrer(null);
    String appointmentXml = encodeDocument(appointmentDocument);

    assertNotNull(appointmentXml);
    assertFalse(appointmentXml.contains(
        "<code code=\"REFB\" codeSystem=\"2.16.840.1.113883.5.90\" displayName=\"ParticipationReferredBy\" codeSystemName=\"ParticipationType (HL7) Code System\"/>"));
    assertFalse(appointmentXml.contains("<author nullFlavor=\"NI\">"));

  }

}