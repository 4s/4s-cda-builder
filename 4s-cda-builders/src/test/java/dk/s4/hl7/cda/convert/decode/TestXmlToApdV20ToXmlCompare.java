package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.convert.APDV20XmlCodec;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public class TestXmlToApdV20ToXmlCompare extends BaseDecodeTest {

  private APDV20XmlCodec codec = new APDV20XmlCodec();

  @Before
  public void before() {
    codec = new APDV20XmlCodec();
  }

  @Test
  public void testXmlToApdToXml_Example_1_5_apd_region() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_1_5_apd_region_adj.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

  //Xml is a copy of DK-APD_Example_1_5_apd_region_adj.xml. The providerOrganization has changed, e.g. assigningAuthorityName is Yderregisteret
  @Test
  public void testXmlToApdToXml_Example_1_5_apd_region_with_practitioner()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_1_5_apd_region_with_practitioner.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToApdToXml_Example_1_5_apd_region_participant_null_values()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "apd/v20/DK-APD_Example_1_5_apd_region_participant_null_values.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToApdToXml_Example_1_4_apd_municipality_a() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_1_4_apd_municipality_a_adj.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToApdToXml_Example_1_4_apd_municipality_c() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_1_4_apd_municipality_c_adj.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

  // sample Example_apd_1.21 sent in mail 18/6-2021
  @Test
  public void testXmlToApdToXml_example_apd_1_21_idsAreRootAssignedAuthorityExtension()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "apd/v20/idTest/Example_apd_1.21_adj_id_setid_appointmentid_are_root_assignedAuthority_extension.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    assertId(true, true, true, apd.getId());
    assertId(true, true, true, apd.getSetId());
    assertId(true, true, true, apd.getAppointmentId());
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToApdToXml_example_apd_1_21_idsAreRootAssignedAuthority()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "apd/v20/idTest/Example_apd_1.21_adj_id_setid_appointmentid_are_root_assignedAuthority.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    assertId(true, false, true, apd.getId());
    assertId(true, false, true, apd.getSetId());
    assertId(true, false, true, apd.getAppointmentId());
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToApdToXml_example_apd_1_21_idsAreRootExtension()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "apd/v20/idTest/Example_apd_1.21_adj_id_setid_appointmentid_are_root_extension.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    assertId(true, true, false, apd.getId());
    assertId(true, true, false, apd.getSetId());
    assertId(true, true, false, apd.getAppointmentId());
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToApdToXml_example_apd_1_21_idsAreRoot() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "apd/v20/idTest/Example_apd_1.21_adj_id_setid_appointmentid_are_root.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    assertId(true, false, false, apd.getId());
    assertId(true, false, false, apd.getSetId());
    assertId(true, false, false, apd.getAppointmentId());
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

  private void assertId(boolean rootNotNull, boolean extensionNotNull, boolean authorityNameNotNull, ID id) {
    assertEquals(rootNotNull, id.getRoot() != null);
    assertEquals(extensionNotNull, id.getExtension() != null);
    assertEquals(authorityNameNotNull, id.getAuthorityName() != null);
  }

  @Test
  public void testXmlToApdToXml_Example_represented_organization_adr_blank()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "apd/v20/DK-APD_Example_apd-2-0_example4_adjusted_represented_organization_addr_blank.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToApdToXml_Example_represented_organization_telecom_blank()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(),
        "apd/v20/DK-APD_Example_apd-2-0_example4_adjusted_represented_organization_telecom_blank.xml");
    AppointmentDocument apd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(apd);
    compare(xmlOrig, xmlNew);
  }

}
