package dk.s4.hl7.cda.convert.decode;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.convert.QFDDXmlCodec;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public class TestXmlToQFDDToXmlCompare extends BaseDecodeTest {

  private QFDDXmlCodec codec = new QFDDXmlCodec();

  @Before
  public void before() {
    codec = new QFDDXmlCodec();
  }

  @Test
  public void testXmlToQFDDToXmlNumericExample() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "qfdd/TestNumericQuestion.xml");
    QFDDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToQFDDToXmlMultipleChoiceExample() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "qfdd/TestMultipleChoiceQuestion.xml");
    QFDDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToQRDToXmlMoreOrganizersWithinSectionExample()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "qfdd/MoreOrganizersWithinSectionTest.xml");
    QFDDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToQRDToXmlOrganizerConditionExample() throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "qfdd/Precondition_organizer.xml");
    QFDDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

  @Test
  public void testXmlToQRDToXmlOrganizerAndObservationConditionCombinationExample()
      throws IOException, URISyntaxException, SAXException {
    String xmlOrig = FileUtil.getData(this.getClass(), "qfdd/Precondition_various.xml");
    QFDDDocument cpd = codec.decode(xmlOrig);
    String xmlNew = codec.encode(cpd);
    compare(xmlOrig, xmlNew);
  }

}
