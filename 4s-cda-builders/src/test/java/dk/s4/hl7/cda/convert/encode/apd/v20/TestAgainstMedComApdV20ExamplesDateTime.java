package dk.s4.hl7.cda.convert.encode.apd.v20;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.APDV20XmlCodec;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;

/**
 * Validate the output from our own appointment builder with the example from
 * MedCom 2.0.
 *
 */
public final class TestAgainstMedComApdV20ExamplesDateTime extends BaseEncodeTest<AppointmentDocument>
    implements ConcurrencyTestCase {

  public TestAgainstMedComApdV20ExamplesDateTime() {
    super("src/test/resources/apd/v20/dateTimeTest");
  }

  @Before
  public void before() {
    setCodec(new APDV20XmlCodec());
  }

  @Test
  public void shouldMatchExpectedValueExampleBirthTimeNoTime() throws Exception {
    performTest("DK-APD_Example_apd-2-0_birthTimeNoTime.xml",
        SetupMedcomV20ExampleDateTime.createBaseAppointmentDocument(false, false, 0, 0, 0));
  }

  @Test
  public void shouldMatchExpectedValueExampleBirthTimeWinter() throws Exception {
    performTest("DK-APD_Example_apd-2-0_birthTimeWinter.xml",
        SetupMedcomV20ExampleDateTime.createBaseAppointmentDocument(true, false, 16, 14, 12));
  }

  @Test
  public void shouldMatchExpectedValueExampleBirthTimeWinterMidnight() throws Exception {
    performTest("DK-APD_Example_apd-2-0_birthTimeWinterMidnight.xml",
        SetupMedcomV20ExampleDateTime.createBaseAppointmentDocument(true, false, 0, 0, 0));
  }

  @Test
  public void shouldMatchExpectedValueExampleBirthTimeSummer() throws Exception {
    performTest("DK-APD_Example_apd-2-0_birthTimeSummer.xml",
        SetupMedcomV20ExampleDateTime.createBaseAppointmentDocument(true, true, 16, 14, 12));
  }

  @Override
  public void runTest() throws Exception {
    //none
  }
}