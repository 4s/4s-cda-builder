package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.APDV20XmlCodec;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.model.apd.v20.AppointmentDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;
import dk.s4.hl7.cda.model.util.DateUtil;

public final class TestXmlToApdV2DateTime extends BaseDecodeTest implements ConcurrencyTestCase {

  private APDV20XmlCodec codec;

  @Before
  public void before() {
    setCodec(new APDV20XmlCodec());
  }

  public void setCodec(APDV20XmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testApdV20XMLConverterBirthTimeNoTime() {
    try {
      String XML = FileUtil.getData(this.getClass(), "apd/v20/dateTimeTest/DK-APD_Example_apd-2-0_birthTimeNoTime.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertNotNull(appointmentDocument.getPatient());
      assertNotNull(appointmentDocument.getPatient().getBirthDate());
      assertTrue(appointmentDocument.getPatient().isBirthTimeOnlyDate());

      Calendar calendar = DateUtil
          .createDanishCalendarWithTimeZone(appointmentDocument.getPatient().getBirthDateAndTime());
      assertEquals(1948, calendar.get(Calendar.YEAR));
      assertEquals(11, calendar.get(Calendar.MONTH));
      assertEquals(25, calendar.get(Calendar.DAY_OF_MONTH));

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExampleBirthTimeWinter() {

    try {
      String XML = FileUtil.getData(this.getClass(), "apd/v20/dateTimeTest/DK-APD_Example_apd-2-0_birthTimeWinter.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertNotNull(appointmentDocument.getPatient());
      assertNotNull(appointmentDocument.getPatient().getBirthDateAndTime());
      assertFalse(appointmentDocument.getPatient().isBirthTimeOnlyDate());

      Calendar calendar = DateUtil
          .createDanishCalendarWithTimeZone(appointmentDocument.getPatient().getBirthDateAndTime());
      assertEquals(1948, calendar.get(Calendar.YEAR));
      assertEquals(11, calendar.get(Calendar.MONTH));
      assertEquals(25, calendar.get(Calendar.DAY_OF_MONTH));
      assertEquals(16, calendar.get(Calendar.HOUR_OF_DAY));
      assertEquals(14, calendar.get(Calendar.MINUTE));
      assertEquals(12, calendar.get(Calendar.SECOND));

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExampleBirthTimeWinterUtc() {

    try {
      String XML = FileUtil.getData(this.getClass(),
          "apd/v20/dateTimeTest/DK-APD_Example_apd-2-0_birthTimeWinterUtc.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertNotNull(appointmentDocument.getPatient());
      assertNotNull(appointmentDocument.getPatient().getBirthDateAndTime());
      assertFalse(appointmentDocument.getPatient().isBirthTimeOnlyDate());

      Calendar calendar = DateUtil
          .createDanishCalendarWithTimeZone(appointmentDocument.getPatient().getBirthDateAndTime());
      assertEquals(1948, calendar.get(Calendar.YEAR));
      assertEquals(11, calendar.get(Calendar.MONTH));
      assertEquals(25, calendar.get(Calendar.DAY_OF_MONTH));
      assertEquals(17, calendar.get(Calendar.HOUR_OF_DAY));
      assertEquals(14, calendar.get(Calendar.MINUTE));
      assertEquals(12, calendar.get(Calendar.SECOND));

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExampleBirthTimeSummer() {

    try {
      String XML = FileUtil.getData(this.getClass(), "apd/v20/dateTimeTest/DK-APD_Example_apd-2-0_birthTimeSummer.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertNotNull(appointmentDocument.getPatient());
      assertNotNull(appointmentDocument.getPatient().getBirthDateAndTime());
      assertFalse(appointmentDocument.getPatient().isBirthTimeOnlyDate());

      Calendar calendar = DateUtil
          .createDanishCalendarWithTimeZone(appointmentDocument.getPatient().getBirthDateAndTime());
      assertEquals(1948, calendar.get(Calendar.YEAR));
      assertEquals(05, calendar.get(Calendar.MONTH));
      assertEquals(25, calendar.get(Calendar.DAY_OF_MONTH));
      assertEquals(16, calendar.get(Calendar.HOUR_OF_DAY));
      assertEquals(14, calendar.get(Calendar.MINUTE));
      assertEquals(12, calendar.get(Calendar.SECOND));

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExampleBirthTimeSummerUtc() {

    try {
      String XML = FileUtil.getData(this.getClass(),
          "apd/v20/dateTimeTest/DK-APD_Example_apd-2-0_birthTimeSummerUtc.xml");

      AppointmentDocument appointmentDocument = decode(codec, XML);

      assertNotNull(appointmentDocument.getPatient());
      assertNotNull(appointmentDocument.getPatient().getBirthDateAndTime());
      assertFalse(appointmentDocument.getPatient().isBirthTimeOnlyDate());

      Calendar calendar = DateUtil
          .createDanishCalendarWithTimeZone(appointmentDocument.getPatient().getBirthDateAndTime());
      assertEquals(1948, calendar.get(Calendar.YEAR));
      assertEquals(05, calendar.get(Calendar.MONTH));
      assertEquals(25, calendar.get(Calendar.DAY_OF_MONTH));
      assertEquals(18, calendar.get(Calendar.HOUR_OF_DAY));
      assertEquals(14, calendar.get(Calendar.MINUTE));
      assertEquals(12, calendar.get(Calendar.SECOND));

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Override
  public void runTest() throws Exception {
    //nothing
  }

}
