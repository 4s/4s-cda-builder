package dk.s4.hl7.cda.convert.encode.qrd;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.QRDXmlCodec;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse.QRDNumericResponseBuilder;

public final class TestQRDBuildVariations extends BaseEncodeTest<QRDDocument> {

  public TestQRDBuildVariations() {
    super("src/test/resources/qrd");
  }

  @Before
  public void before() {
    setCodec(new QRDXmlCodec());
  }

  @Test
  public void testServiceEventEffectTimeHighNotNull() throws Exception {
    QRDDocument qrdDocument = new SetupQRDNumericExample().createDocument();
    String qrdXml = encodeDocument(qrdDocument);

    assertNotNull(qrdXml);
    assertTrue(qrdXml.contains("<low value=\"20140106080200+0100\"/>"));
    assertTrue(qrdXml.contains("<high value=\"20140110081500+0100\"/>"));

  }

  @Test
  public void testServiceEventEffectTimeHighNull() throws Exception {
    QRDDocument qrdDocument = new SetupQRDNumericExample().createDocument();
    qrdDocument.setDocumentationTimeInterval(qrdDocument.getServiceStartTime(), null);
    String qrdXml = encodeDocument(qrdDocument);

    assertNotNull(qrdXml);
    assertTrue(qrdXml.contains("<low value=\"20140106080200+0100\"/>"));
    assertTrue(qrdXml.contains("<high nullFlavor=\"NI\"/>"));

  }

  @Test(expected = IllegalArgumentException.class)
  public void testNumericResponseMutualDependentValueValue() throws Exception {
    QRDNumericResponse qrdNumericResponse = new QRDNumericResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", "question" + "1", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion("question")
        .setInterval("1,00", "2,00", IntervalType.IVL_REAL)
        .setValue(null, BasicType.REAL)
        .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNumericResponseMutualDependentValueType() throws Exception {
    QRDNumericResponse qrdNumericResponse = new QRDNumericResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", "question" + "1", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion("question")
        .setInterval("1,00", "2,00", IntervalType.IVL_REAL)
        .setValue("1.55", null)
        .build();
  }

}
