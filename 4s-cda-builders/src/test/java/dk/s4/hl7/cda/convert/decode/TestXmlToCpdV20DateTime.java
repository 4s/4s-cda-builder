package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.CPDV20XmlCodec;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.model.CdaDate;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument;
import dk.s4.hl7.cda.model.cpd.v20.CPDHealthStatusActEntry;
import dk.s4.hl7.cda.model.testutil.FileUtil;
import dk.s4.hl7.cda.model.util.DateUtil;

public class TestXmlToCpdV20DateTime extends BaseDecodeTest implements ConcurrencyTestCase {

  private CPDV20XmlCodec codec = new CPDV20XmlCodec();

  @Before
  public void before() {
    setCodec(new CPDV20XmlCodec());
  }

  public void setCodec(CPDV20XmlCodec codec) {
    this.codec = codec;
  }

  @Test(expected = CdaBuilderException.class)
  public void testCpdV20XMLConverterExampleInvalidDateMonthIs13() {

    try {
      String XML = FileUtil.getData(this.getClass(),
          "cpd/dateTimeTest/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_invalidMonthIs13.xml");

      decode(codec, XML);

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterEffectiveTimeNoTime() {
    try {
      String XML = FileUtil.getData(this.getClass(),
          "cpd/dateTimeTest/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_noTime.xml");

      CPDDocument cpdDocument = decode(codec, XML);
      assertNotNull(cpdDocument);

      CdaDate effectiveCdaTime = getObservationEffectiveStartDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDate(effectiveCdaTime, 2010, 5, 15);

      effectiveCdaTime = getObservationEffectiveStopDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDate(effectiveCdaTime, 2010, 5, 16);

      effectiveCdaTime = getOutcomeDate(cpdDocument, 0, 0);
      assertEffectiveCdaDate(effectiveCdaTime, 2020, 1, 15);

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExampleEffectiveTimeWinter() {

    try {
      String XML = FileUtil.getData(this.getClass(),
          "cpd/dateTimeTest/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_winter.xml");

      CPDDocument cpdDocument = decode(codec, XML);
      assertNotNull(cpdDocument);

      CdaDate effectiveCdaTime = getObservationEffectiveStartDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 1, 15, 9, 15, 0);

      effectiveCdaTime = getObservationEffectiveStopDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 1, 15, 9, 30, 0);

      effectiveCdaTime = getOutcomeDate(cpdDocument, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 1, 15, 9, 15, 0);

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExampleEffectiveTimeWinterMinusFive() {

    try {
      String XML = FileUtil.getData(this.getClass(),
          "cpd/dateTimeTest/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_winterMinusFive.xml");

      CPDDocument cpdDocument = decode(codec, XML);
      assertNotNull(cpdDocument);

      CdaDate effectiveCdaTime = getObservationEffectiveStartDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 1, 15, 15, 15, 0);

      effectiveCdaTime = getObservationEffectiveStopDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 1, 15, 15, 30, 0);

      effectiveCdaTime = getOutcomeDate(cpdDocument, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 1, 15, 15, 15, 0);

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExampleEffectiveTimeWinterUtc() {

    try {
      String XML = FileUtil.getData(this.getClass(),
          "cpd/dateTimeTest/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_winterUtc.xml");

      CPDDocument cpdDocument = decode(codec, XML);
      assertNotNull(cpdDocument);

      CdaDate effectiveCdaTime = getObservationEffectiveStartDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 1, 15, 10, 15, 0);

      effectiveCdaTime = getObservationEffectiveStopDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 1, 15, 10, 30, 0);

      effectiveCdaTime = getOutcomeDate(cpdDocument, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 1, 15, 10, 15, 0);

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExampleEffectiveTimeSummer() {

    try {
      String XML = FileUtil.getData(this.getClass(),
          "cpd/dateTimeTest/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_summer.xml");

      CPDDocument cpdDocument = decode(codec, XML);
      assertNotNull(cpdDocument);

      CdaDate effectiveCdaTime = getObservationEffectiveStartDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 8, 15, 9, 15, 0);

      effectiveCdaTime = getObservationEffectiveStopDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 8, 15, 9, 30, 0);

      effectiveCdaTime = getOutcomeDate(cpdDocument, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 8, 15, 9, 15, 0);

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExampleEffectiveTimeSummerMinusFive() {

    try {
      String XML = FileUtil.getData(this.getClass(),
          "cpd/dateTimeTest/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_summerMinusFive.xml");

      CPDDocument cpdDocument = decode(codec, XML);
      assertNotNull(cpdDocument);

      CdaDate effectiveCdaTime = getOutcomeDate(cpdDocument, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 8, 15, 16, 15, 0);

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testApdV20XMLConverterExampleEffectiveTimeSummerUtc() {

    try {
      String XML = FileUtil.getData(this.getClass(),
          "cpd/dateTimeTest/CPD-DK_Care_Plan_SAMBLIK_Example_02-MIN_v01_summerUtc.xml");

      CPDDocument cpdDocument = decode(codec, XML);
      assertNotNull(cpdDocument);

      CdaDate effectiveCdaTime = getObservationEffectiveStartDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 8, 15, 11, 15, 0);

      effectiveCdaTime = getObservationEffectiveStopDate(cpdDocument, 0, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 8, 15, 11, 30, 0);

      effectiveCdaTime = getOutcomeDate(cpdDocument, 0, 0);
      assertEffectiveCdaDateTime(effectiveCdaTime, 2020, 8, 15, 11, 15, 0);

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  private void assertEffectiveCdaDateTime(CdaDate effectiveCdaTime, int year, int month, int day, int hour, int minute,
      int second) {
    assertNotNull(effectiveCdaTime);
    assertNotNull(effectiveCdaTime.getDate());
    assertTrue(effectiveCdaTime.dateHasTime());

    Calendar calendar = DateUtil.createDanishCalendarWithTimeZone(effectiveCdaTime.getDate());
    assertEquals(year, calendar.get(Calendar.YEAR));
    assertEquals(month, calendar.get(Calendar.MONTH));
    assertEquals(day, calendar.get(Calendar.DAY_OF_MONTH));
    assertEquals(hour, calendar.get(Calendar.HOUR_OF_DAY));
    assertEquals(minute, calendar.get(Calendar.MINUTE));
    assertEquals(second, calendar.get(Calendar.SECOND));

  }

  private void assertEffectiveCdaDate(CdaDate effectiveCdaTime, int year, int month, int day) {
    assertNotNull(effectiveCdaTime);
    assertNotNull(effectiveCdaTime.getDate());
    assertFalse(effectiveCdaTime.dateHasTime());
    Calendar calendar = DateUtil.createDanishCalendarWithTimeZone(effectiveCdaTime.getDate());
    assertEquals(year, calendar.get(Calendar.YEAR));
    assertEquals(month, calendar.get(Calendar.MONTH));
    assertEquals(day, calendar.get(Calendar.DAY_OF_MONTH));

  }

  private CdaDate getObservationEffectiveStartDate(CPDDocument cpdDocument, int componentIdx, int entryIdx,
      int observationIdx) {
    CdaDate effectiveCdaTime = (cpdDocument.getHealthConcernSection().getComponent().getEntryList().get(entryIdx))
        .getObservationList()
        .get(observationIdx)
        .getEffectiveTimeInterval()
        .getStartTimeCdaDate();
    return effectiveCdaTime;
  }

  private CdaDate getObservationEffectiveStopDate(CPDDocument cpdDocument, int componentIdx, int entryIdx,
      int observationIdx) {
    CdaDate effectiveCdaTime = (cpdDocument.getHealthConcernSection().getComponent().getEntryList().get(entryIdx))
        .getObservationList()
        .get(observationIdx)
        .getEffectiveTimeInterval()
        .getStopTimeCdaDate();
    return effectiveCdaTime;
  }

  private CdaDate getOutcomeDate(CPDDocument cpdDocument, int componentIdx, int entryIdx) {
    CdaDate effectiveCdaTime = cpdDocument
        .getOutcomeSection()
        .getComponent()
        //        .get(componentIdx)
        .getEntryList()
        .get(entryIdx)
        .getEffectiveTimeCdaDate();
    return effectiveCdaTime;
  }

  @Override
  public void runTest() throws Exception {
    //nothing
  }

}
