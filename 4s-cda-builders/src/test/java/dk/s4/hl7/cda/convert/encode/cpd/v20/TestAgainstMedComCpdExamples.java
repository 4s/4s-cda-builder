package dk.s4.hl7.cda.convert.encode.cpd.v20;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.CPDV20XmlCodec;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.cpd.v20.CPDDocument;

public final class TestAgainstMedComCpdExamples extends BaseEncodeTest<CPDDocument> implements ConcurrencyTestCase {

  public TestAgainstMedComCpdExamples() {
    super("src/test/resources/cpd/");
  }

  @Before
  public void before() {
    setCodec(new CPDV20XmlCodec());
  }

  @Test
  public void shouldMatchExpectedValueExample1() throws Exception {
    performTest("CPD-DK_Care_Plan_example1.xml", SetupMedcomExample1.defineAsCDA());
  }

  @Override
  public void runTest() throws Exception {
    shouldMatchExpectedValueExample1();

  }
}
