package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.CDAMetadataXmlCodec;
import dk.s4.hl7.cda.model.cdametadata.CDAMetadata;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public final class TestXmlToCdaMetadata extends BaseDecodeTest {
  private CDAMetadataXmlCodec codec = new CDAMetadataXmlCodec();

  @Before
  public void before() {
    setCodec(new CDAMetadataXmlCodec());
  }

  public void setCodec(CDAMetadataXmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testCDAMetadataXMLConverterFindValuesFor_author_creationTime_confidentialityCode_languageCode() {
    try {

      //given
      String docAsXML = FileUtil.getData(this.getClass(), "apd/DK-APD_Example_1.xml");

      //when
      CDAMetadata CdaMetadata = decode(codec, docAsXML);

      //then

      //author.authorInstitution
      assertNotNull(CdaMetadata.getAuthorInstitution().getDisplayName());
      assertEquals("OUH Radiologisk Afdeling (Svendborg)", CdaMetadata.getAuthorInstitution().getDisplayName());
      assertNotNull(CdaMetadata.getAuthorInstitution().getCode());
      assertEquals("242621000016001", CdaMetadata.getAuthorInstitution().getCode());
      assertNotNull(CdaMetadata.getAuthorInstitution().getCodeSystem());
      assertEquals("1.2.208.176.1.1", CdaMetadata.getAuthorInstitution().getCodeSystem());

      //author.authorperson
      assertNotNull(CdaMetadata.getAuthorperson().getGivenNames());
      assertTrue(CdaMetadata.getAuthorperson().getGivenNames().length == 1);
      assertEquals("Jens", CdaMetadata.getAuthorperson().getGivenNames()[0]);
      assertNotNull(CdaMetadata.getAuthorperson().getFamilyName());
      assertEquals("Jensen", CdaMetadata.getAuthorperson().getFamilyName());

      //confidentialityCode
      assertNotNull(CdaMetadata.getConfidentialityCodeCodedValue().getCode());
      assertEquals("N", CdaMetadata.getConfidentialityCodeCodedValue().getCode());
      assertNotNull(CdaMetadata.getConfidentialityCodeCodedValue().getCodeSystem());
      assertEquals("2.16.840.1.113883.5.25", CdaMetadata.getConfidentialityCodeCodedValue().getCodeSystem());
      assertNull(CdaMetadata.getConfidentialityCodeCodedValue().getDisplayName());

      //creationTime
      assertNotNull(CdaMetadata.getCreationTime()); // value="2017 01 13  10:00:00+0100"/>
      assertEquals(new GregorianCalendar(2017, 00, 13, 10, 00, 00).getTime(), CdaMetadata.getCreationTime());

      //eventCodeList
      assertNotNull(CdaMetadata.getEventCodeList());
      assertTrue(CdaMetadata.getEventCodeList().size() == 0);

      //languageCode
      assertNotNull(CdaMetadata.getLanguageCode());
      assertEquals("da-DK", CdaMetadata.getLanguageCode());

      //legalAuthenticator
      assertNull(CdaMetadata.getLegalAuthenticator());

      //patientId
      assertNotNull(CdaMetadata.getPatientId().getCode());
      assertEquals("2512489996", CdaMetadata.getPatientId().getCode());
      assertNotNull(CdaMetadata.getPatientId().getCodeSystem());
      assertEquals("1.2.208.176.1.2", CdaMetadata.getPatientId().getCodeSystem());

      //serviceStartTime+serviceStopTime
      assertNotNull(CdaMetadata.getServiceStartTime());
      assertEquals(new GregorianCalendar(2017, 04, 31, 11, 00, 00).getTime(), CdaMetadata.getServiceStartTime());
      assertNotNull(CdaMetadata.getServiceStopTime());
      assertEquals(new GregorianCalendar(2017, 04, 31, 12, 00, 00).getTime(), CdaMetadata.getServiceStopTime());

      //sourcePatientId
      assertNotNull(CdaMetadata.getSourcePatientId().getCode());
      assertEquals("2512489996", CdaMetadata.getSourcePatientId().getCode());
      assertNotNull(CdaMetadata.getSourcePatientId().getCodeSystem());
      assertEquals("1.2.208.176.1.2", CdaMetadata.getSourcePatientId().getCodeSystem());

      //sourcePatientInfo
      assertNotNull(CdaMetadata.getPatient().getFamilyName());
      assertEquals("Berggren", CdaMetadata.getPatient().getFamilyName());
      assertNotNull(CdaMetadata.getPatient().getGivenNames());
      assertTrue(CdaMetadata.getPatient().getGivenNames().length > 0);
      assertEquals("Nancy", CdaMetadata.getPatient().getGivenNames()[0]);
      assertEquals("Ann", CdaMetadata.getPatient().getGivenNames()[1]);
      assertNotNull(CdaMetadata.getPatient().getBirthTime());
      assertEquals(new GregorianCalendar(1948, 11, 25, 00, 00, 00).getTime(), CdaMetadata.getPatient().getBirthTime());
      assertNotNull(CdaMetadata.getPatient().getGender());
      assertEquals("Female", CdaMetadata.getPatient().getGender().name());

      //title
      assertNotNull(CdaMetadata.getTitle());
      assertEquals("Aftale for 2512489996", CdaMetadata.getTitle());

      //typeCode
      assertNotNull(CdaMetadata.getCodeCodedValue().getCode());
      assertEquals("39289-4", CdaMetadata.getCodeCodedValue().getCode());
      assertNotNull(CdaMetadata.getCodeCodedValue().getDisplayName());
      assertEquals("Dato og tidspunkt for møde mellem patient og sundhedsperson",
          CdaMetadata.getCodeCodedValue().getDisplayName());
      assertNotNull(CdaMetadata.getCodeCodedValue().getCodeSystem());
      assertEquals("2.16.840.1.113883.6.1", CdaMetadata.getCodeCodedValue().getCodeSystem());
      assertNull(CdaMetadata.getCodeCodedValue().getCodeSystemName());

      //uniqueId
      assertNotNull(CdaMetadata.getId().getExtension());
      assertEquals("aa2386d0-79ea-11e3-981f-0800200c9a66", CdaMetadata.getId().getExtension());
      assertNotNull(CdaMetadata.getId().getRoot());
      assertEquals("1.2.208.184", CdaMetadata.getId().getRoot());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testCDAMetadataXMLConverterFindValuesFor_creationTime_starttime_stoptime_other_timezone() {
    try {

      //given
      String docAsXML = FileUtil.getData(this.getClass(), "apd/DK-APD_Example_1_anden_tidszone.xml");

      //when
      CDAMetadata CdaMetadata = decode(codec, docAsXML);
      DateFormat formatterUTCTimezone = new SimpleDateFormat("yyyyMMddHHmmss");
      formatterUTCTimezone.setTimeZone(TimeZone.getTimeZone("UTC"));

      DateFormat formatterDefaultTimezone = new SimpleDateFormat("yyyyMMddHHmmss");
      formatterDefaultTimezone.setTimeZone(TimeZone.getTimeZone("UTC"));

      //then
      //creationTime
      assertNotNull(CdaMetadata.getCreationTime()); //creationTime has timezone +4
      String dateUTCString = formatterUTCTimezone.format(CdaMetadata.getCreationTime());
      assertEquals("20170113060000", dateUTCString);

      //serviceStartTime+serviceStopTime
      assertNotNull(CdaMetadata.getServiceStartTime()); //startTime has timezone +4
      dateUTCString = formatterUTCTimezone.format(CdaMetadata.getServiceStartTime());
      assertEquals("20170530080000", dateUTCString);

      assertNotNull(CdaMetadata.getServiceStopTime()); //stopTime does not have a timezone. We dont know what timezone of the test server, hence only check not null at the moment

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testCDAMetadataXMLConverterFindValuesFor_eventCodeList_legalAuthenticator() {
    try {
      //given
      String docAsXML = FileUtil.getData(this.getClass(), "phmr/Ex4-COPD-MODIFIED.xml");

      //when
      CDAMetadata CdaMetadata = decode(codec, docAsXML);

      //then

      //eventCodeList
      assertNotNull(CdaMetadata.getEventCodeList());
      assertTrue(CdaMetadata.getEventCodeList().size() == 2);
      assertEquals("NPU03011", CdaMetadata.getEventCodeList().get(0).getCode());
      assertEquals("1.2.208.176.2.1", CdaMetadata.getEventCodeList().get(0).getCodeSystem());
      assertEquals("O2 sat.;Hb(aB)", CdaMetadata.getEventCodeList().get(0).getDisplayName());
      assertNull(CdaMetadata.getEventCodeList().get(0).getCodeSystemName());

      //legalAuthenticator
      assertNotNull(CdaMetadata.getLegalAuthenticator().getPersonIdentity().getGivenNames());
      assertTrue(CdaMetadata.getLegalAuthenticator().getPersonIdentity().getGivenNames().length > 0);
      assertNotNull(CdaMetadata.getLegalAuthenticator().getPersonIdentity().getGivenNames()[0]);
      assertEquals("Lars", CdaMetadata.getLegalAuthenticator().getPersonIdentity().getGivenNames()[0]);
      assertNotNull(CdaMetadata.getLegalAuthenticator().getPersonIdentity().getFamilyName());
      assertEquals("Olsen", CdaMetadata.getLegalAuthenticator().getPersonIdentity().getFamilyName());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testCDAMetadataXMLConverterFindValuesFor_formatCode() {
    try {
      //given
      String docAsXML = FileUtil.getData(this.getClass(), "apd/v20/DK-APD_Example_apd-2-0.xml");

      //when
      CDAMetadata cdaMetadata = decode(codec, docAsXML);

      //then

      //formatCode
      assertNotNull(cdaMetadata.getFormatCode());
      assertEquals("urn:ad:dk:medcom:apd-v2.0.1:full", cdaMetadata.getFormatCode().getCode());
      assertEquals("1.2.208.184.100.10", cdaMetadata.getFormatCode().getCodeSystem());
      assertEquals("APD DK schema", cdaMetadata.getFormatCode().getDisplayName());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testCDAMetadataXMLConverterFindValuesForCprEvenWhenInvalid() {
    try {
      //given
      String docAsXML = FileUtil.getData(this.getClass(), "metadata/DK-APD_Example_apd-2-0_invalid_cpr.xml");
      //      <id extension="1234567890" root="1.1.1.1.1.1" assigningAuthorityName="invalid"/>

      //when
      CDAMetadata cdaMetadata = decode(codec, docAsXML);

      //then
      assertNotNull(cdaMetadata.getPatient());
      assertNotNull(cdaMetadata.getPatient().getId());
      assertNotNull(cdaMetadata.getPatient().getId().getAuthorityName());
      assertEquals("invalid", cdaMetadata.getPatient().getId().getAuthorityName());
      assertNotNull(cdaMetadata.getPatient().getId().getExtension());
      assertEquals("1234567890", cdaMetadata.getPatient().getId().getExtension());
      assertNotNull(cdaMetadata.getPatient().getId().getRoot());
      assertEquals("1.1.1.1.1.1", cdaMetadata.getPatient().getId().getRoot());

    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testCDAMetadataXMLConverter_QRD1_3_FindValuesForFormatCode() {
    try {
      //given
      String docAsXML = FileUtil.getData(this.getClass(), "metadata/QRD_Example_Text_Question_formatcode1_3.xml");

      //when
      CDAMetadata cdaMetadata = decode(codec, docAsXML);

      //then
      assertNotNull(cdaMetadata);
      assertNotNull(cdaMetadata.getFormatCode());
      assertEquals("urn:ad:dk:medcom:qrd-v1.3:full", cdaMetadata.getFormatCode().getCode());
      assertEquals("1.2.208.184.100.10", cdaMetadata.getFormatCode().getCodeSystem());
      assertNull(cdaMetadata.getFormatCode().getCodeSystemName());
      assertEquals("QRD DK schema", cdaMetadata.getFormatCode().getDisplayName());
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testCDAMetadataXMLConverter_QFDD1_2_FindValuesForFormatCode() {
    try {
      //given
      String docAsXML = FileUtil.getData(this.getClass(), "metadata/QFD_Example_Text_Question_formatcode1_2.xml");

      //when
      CDAMetadata cdaMetadata = decode(codec, docAsXML);

      //then
      assertNotNull(cdaMetadata);
      assertNotNull(cdaMetadata.getFormatCode());
      assertEquals("urn:ad:dk:medcom:qfdd-v1.2:full", cdaMetadata.getFormatCode().getCode());
      assertEquals("1.2.208.184.100.10", cdaMetadata.getFormatCode().getCodeSystem());
      assertNull(cdaMetadata.getFormatCode().getCodeSystemName());
      assertEquals("QFDD DK schema", cdaMetadata.getFormatCode().getDisplayName());
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

}