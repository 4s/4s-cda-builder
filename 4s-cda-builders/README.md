# Builders
Notice: The CDABuilder is a tool for parsning and generating CDA documents. For vendors using the CDABuilder please be aware that the tool do not guarantee compliance to the international CDA standard or the national CDA standard
 
## Builder version 
(Pom file version, which is not the same as CDA version)

### Version 2.1.0
 
  - Apd version 2.0 is new
     - New operations:
        - getAppointmentLocationHomeAddress
        - setAppointmentLocationHomeAddress(OrganizationIdentity appointmentLocationHomeAddress)
        - getAppointmentEncounterCode()
        - setAppointmentEncounterCode(AppointmentEncounterCode appointmentEncounterCode)
        - isRepeatingDocument
        - setRepeatingDocument(boolean isRepeatingDocument)
        - getRepeatingDocumentGroupValue()
        - setRepeatingDocumentGroupValue(String repeatingDocumentGroupValue)
        - isGuidedInterval()
        - setGuidedInterval(boolean isGuidedInterval)
        - getGuidedIntervalText()
        - setGuidedIntervalText(String guidedIntervalText)
     - Adjusted operations
        - Appointment.getAppointmentPerformer.setPersonIdentity(OrganizationIdentity organizationIdentity): personIdentity is no longer required
        - Appointment.getAppointmentPerformer.setOrganizationIdentity(OrganizationIdentity organizationIdentity): address and Telecom are now used instead of always null flavor
 
  - Cda Header Version 2.0 is new 
     - New operations:
        - getCdaProfileAndVersion()
        - setCdaProfileAndVersion(String cdaProfileAndVersion)
        - getEpisodeOfCareLabel()
        - setEpisodeOfCareLabel(String EpisodeOfCareLabel)
        - getEpisodeOfCareIdentifierList()
        - setEpisodeOfCareIdentifierList(List<String> episodeOfCareIdentifierList)
        - addEpisodeOfCareIdentifier(String episodeOfCareIdentifier)
        - setProviderOrganization(OrganizationIdentity providerOrganization)
        - getProviderOrganization
     - Adjusted operations
        - Appointment.setAuthor(Participant author): address and Telecom are now used instead of always null flavor

### Version 2.1.1

  - Apd version 2.0.1 is new version (changes are made to cda header)
     - New operations:
        - getAuthorReferrer
        - setAuthorReferrer(Participant authorReferrer)
     - Adjusted operations
        - setCdaProfileAndVersion(String cdaProfileAndVersion): when xml is generated for this information, the layout has changed
   

### Version 2.1.2

  - Apd version 2.0.1 has been adjusted (changes are made to cda header)
     - New operations:
        - getEpisodeOfCareLabelDisplayName()
        - setEpisodeOfCareLabelDisplayName(String EpisodeOfCareLabelDisplayName)
        - getIndicationCode()
        - setIndicationCode(CodedValue codedValue)
     - Adjusted operations
        - setEffectiveTime(Date documentCreationTime): when xml is generated the values has changed
        - setIndicationDisplayName and getIndicationDisplayName: can be replaced by getIndicationCode and setIndicationCode for more detailed values
        - setEpisodeOfCareLabel(String EpisodeOfCareLabel): when xml is generated the values has changed
        - getEpisodeOfCareIdentifierList() now returns a list of IDs instead of Strings
        - setEpisodeOfCareIdentifierList(List<String> episodeOfCareIdentifierList) is now setEpisodeOfCareIdentifierList(List<ID> episodeOfCareIdentifierList)
        - addEpisodeOfCareIdentifier(String episodeOfCareIdentifier) is now addEpisodeOfCareIdentifier(ID episodeOfCareIdentifier)
 
### Version 2.1.3
 
  - QFDD has been adjusted
     - New operations, available to numeric, text and multiple choice questions:
  	    - setQuestionnaireMedia(QuestionnaireMedia questionnaireMedia)
  	    - getQuestionnaireMedia() //mediaType, representation, value, id
  	 - Title is no longer a required field at section level
  	 - Building copyright now uses value from text field in the <text> tag (before it used title)
  	 - referenceRange is no longer required for numeric questions
  	 - templateId in referenceRange for numeric questions is now correctly 2.16.840.1.113883.10.20.32.4.5 (before 33.4.3 was used)
  	 - New operations, available for multiple choice questions handling associated text
  	    - setAssociatedTextQuestion(QFDDTextQuestion associatedTextQuestion)
  	    - getAssociatedTextQuestion
  	 - When xml is generated for questions, the observation moodCode is now correctly DEF
  	 
  - QRD has been adjusted
     - New operations, available to numeric, text and multiple choice responses:
  	    - setQuestionnaireMedia(QuestionnaireMedia questionnaireMedia)
  	    - getQuestionnaireMedia() //mediaType, representation, value, id
     - Title is no longer a required field at section level
  - Apd version 2.0.1 has been adjusted
     - setCdaProfileAndVersion(String cdaProfileAndVersion): when xml is generated for this information, the layout has changed (id root is now 1.2.208.184.100.10)
     - New operations
     	- patientBuilder.setBirthDateAndTime(Date date): when xml is generated, time is included in the xml for birthTime
     	- patientBuilder.setBirthDate(Date date): when xml is generated, time is not included in the xml for birthTime
     	- (patientBuilder.setBirthTime is existing method and is not changed, but should be replaced by setBirthDate which has same behavior)
     	- patient.getBirthDateAndTime: time is actual time, so when 0 i return in time, it means midnight 
     	- patient.getBirthDate: time returned should be considered blank, since it was not available in the xml data
     	- (patient.getBirthTime is existing method and is not changed, but should be replaced by getBirthDate which has same behavior)

### Version 2.2.1

  - PDC (Person Date Card / Stamkort) has been added as document type. Only parse (decode) is available
     - PDC uses CDA header 2.0.1
     - New operations/entities are:
        - pdcXmlCodec.decode(PDCXmlCodec codec, String XML): generates datamodel from PDC xml 
        - pdcDocument.getText()
        - pdcDocument.getCustodyInformationList() 
        - pdcDocument.getNameAndAddressInformation()
        - pdcDocument.getCoverageGroup()
        - pdcDocument.getOrganDonorRegistration()
        - pdcDocument.getTreatmentWillRegistration()
        - pdcDocument.getLivingWillRegistration()
        - pdcDocument.getManuallyEnteredSpokenLanguage()
        - pdcDocument.getManuallyEnteredTemporaryAddress()
        - pdcDocument.getManuallyEnteredDentistInformation
        - pdcDocument.getManuallyEnteredContactInformation
        - pdcDocument.getManuallyEnteredInformationAboutRelativesList
     - For more detailed information about the different entities above including their methods, checkout the source code and build it. Then locate the java doc for the PDCDocument class (4s-cda-builders/target/apidocs/dk/s4/hl7/cda/model/pdc/v20/PDCDocument.html)
     - Example code can be found in TestXmlToPdcV20   

### Version 2.2.2
  - QFDD and QRD has been adjusted
       - displayName in the Code element is no longer required for text, analogSlider and Numeric questions/responses
     
### Version 3.0.1

  - QFDD and QRD has been adjusted
     - Organizer level has been added between Section and QuestionnaireEntity in the model. 
        - NB! This change in the model is not backwards compatible, and hence code using this document must be adjusted accordingly.
        - See test case TestQFDDExamples.testText() and TestQRDExamples.testText() where QFDDOrganizer and QRDOrganizer is in use and old and new code can be compared.
        - The changes was required in order for the builder/parser to comply with the QFDD and QRD standard with multiple <entry><organizer> per <component><section>
     - Id (one or more) has been introduced at organizer level. In case no id is available <id nullFlavor="NI"/> is added to the XML.
  - QFDD has been adjusted
     - When building PreConditionGroup the outer <sdtc:precondition> no longer includes the typeCode="PRCN" attribute (example in MultipleChoiceTestWithAssociatedText.xml)   
  - QRD has been adjusted
     - NarrativeTextConverter is now available which is a way to adjust the narrative text at section level when the xml is generated
        - By using the new QRDXMLNarrativeTextReplaceResponseConverter the encoder will look at the answers given and adjust the narrative text accordingly. It will do a search replace, for more details see the code.
        - Example of usage:
           - QRDXmlCodec codec = new QRDXmlCodec(new QRDXMLNarrativeTextReplaceResponseConverter());
           - QRDDocument qrdDocument = new SetupQRDNarrativeTextExample().createDocument();
           - String xmlDocument = codec.encode(qrdDocument);
        - If no adjustments should take place use the encoder as usual, without giving a NarrativeTextConverter
        - It is possible to create a new implementation of NarrativeTextConverter if needed, and give that as parameter to the encoder
     - AnalogSlider now correctly uses the templateId root for numeric response (instead of multiple choice)
     - Numeric, Text and Multiple choice answers now includes <statusCode code="COMPLETED"/>  
     - New operations, available for multiple choice responses handling associated text
        - setAssociatedTextResponse(QFDDTextResponse associatedTextResponse)
        - getAssociatedTextResponse
     - New operations, available for all answers
        - setHelpText(QRDHelpText qrdHelpText)
        - getHelpText
     - Adjusted operations
  	    - setQuestionnaireMedia(QuestionnaireMedia questionnaireMedia): when xml is generated moodCode for observationMedia is now set to EVN instead of DEF
     
### Version 3.0.2
  - QFDD and QRD has been adjusted
       - displayName in the Code element is no longer required for Multiple Choice and Discrete Slider questions/responses
       - code value in the statusCode element is now lowercase (completed) instead of uppercase (COMPLETED)
       
### Version 3.0.3
  - CDA header version 1 and 2 has been adjusted
     - Adjusted operations
        - setDocumentationTimeInterval(Date serviceStartTime, Date serviceStopTime): when header xml is generated, and serviceStopTime is null, nullFlavor is generated: <high nullFlavor="NI"/>
  - CDA header version 2 has been adjusted
     - Adjusted operations
        - setAuthorReferrer(Participant authorReferrer): when header xml is generated, null value will no longer generate nullFlavor referrer author (will leave out the element)
  - Apd version 2 has been adjusted
     - Adjusted operation
        - setDocumentationTimeInterval(Date serviceStartTime, Date serviceStopTime): when encounter xml is geneated, and serviceStopTime is null, nullFlaver is generated: <high nullFlavor="NI"/>
        
### Version 3.0.4
  - CDA header version 1 and 2 has been adjusted
     - patient.getId() now returns the actual values for AuthorityName and Root after parsing the XML


### Version 4.1.1
  - Added disclaimer in readme files
  - To allow for "adressebeskyttelse" All addr elements in the xml output now uses <postalCode nullFlavor="NI"/> and <city nullFlavor="NI"/> when postalCode and City is not given. (instead of skipping the addr element completely)
  - CPD (Careplan) has been added as document type. For parse and build.
     - The implementation is not final - adjustments will come later
     - PDC uses CDA header 2.0
     - New operations are:
        - cpdXmlCodec.decode: generates datamodel from CPD xml
        - cpdXmlCodec.encode: generates CPD xml from datamodel
        - cpdDocument.getHealthConcernSection
        - cpdDocument.setHealthConcernSection
        - cpdDocument.getGoalSection
        - cpdDocument.setGoalSection
        - cpdDocument.getInterventionSection
        - cpdDocument.setInterventionSection
        - cpdDocument.getOutcomeSection
        - cpdDocument.setOutcomeSection
     - For more detailed information about the different entities above including their methods, checkout the source code and build it. Then locate the java doc for the PDCDocument class (4s-cda-builders/target/apidocs/dk/s4/hl7/cda/model/cpd/v20/CPDDocument.html)
     - Example code can be found in TestXmlToCpdV20
  
### Version 4.2.1
  - When building XML, all documents now have a stylesheet tag
  - When building XML, all documents now have a new value for the schemaLocation attribute
  - Careplan ClinicalDocument tag now has the attributes classCode and moodCode
  - For Careplan the following is implemented for effectiveTime
  	  - in the XML time must be in format: value="20200215" or value="20200915091500+0200"
  	  - the time and date must not be invalid. E.g. it is no longer valid to use 13 as month 
  	  - when a date or time cannot be parsed, a CdaBuilderException is thrown (before you where given a warning and the value was set to null)
  	  
### Version 4.3.1
  - Changes to APD v2 and appointmentLocation
     - Use setAppointmentLocationTypeCode(AppointmentLocationTypeCode.HOME) or setAppointmentLocationHomeAddress() to force SBJ in XML participant
     - Use setAppointmentLocationTypeCode(AppointmentLocationTypeCode.HEALTH_ORGANIZATION) to force LOC in XML participant
     - Use setAppointmentLocationTypeCode(AppointmentLocationTypeCode.NON_SOR) to force DST in XML participant
     - In case no appointmentLocationTypeCode default code will be:
        - if setAppointmentLocation() and SOR code available LOC will be used
        - if setAppointmentLocation() and appointment address and telecom equals patient address and telecom SBJ will be used
        - otherwise DST is used
     - Use getAppointmentLocationTypeCode to determine what location type was found during parse of the XML        
  - APD tag "RepeatingAppointmentType" is replaced by "RepeatingDocumentType"

### Version 5.0.1
  - Document Id in CDA header no longer is required to have extension or assigningAuthorityName element
  - SetId now has the type ID instead of String
     - changed operations are:
        - getSetId now returns ID
        - setSetid(String setId) is deprecated
        - setSetId(ID setId) has been added
        - setDocumentVersion(String setId, Integer versionNumber) has been deprecated
        - setDocumentVersion(ID setId, Integer versionNumber) has been added 
        - getUniqueId() now returns ID 
     - the extension and assigningAuthorityName elements are not required
  - AppointmentId v2 now has the type ID instead of String
     - changes operations are 
        - getAppointmentId() now returns ID
        - setAppointmentId(String id) is deprecated
        - setAppointmentId(ID id) has been added
     - the extension and assigningAuthorityName elements are not required

### Version 6.0.1
  - Careplan adjustments (not all backward compatible)
     - CPDDiagnosis renamed to CPDHealthConcernObservation
     - diagnosisCode renamed to valueCode
     - Each section can only have one component instead of a list of components
     - Adjustments to how code is builded and parsed (code NI allowed on entry level)
     - Adjustments to what value combinations are allowed in XML
         - new operations are:
            - setCdaMeasurement(CdaMeasurement cdaMeasurement) and getCdaMeasurement for handling type PQ
            - setCdaMeasurementInterval(CdaMeasurementInterval cdaMeasurementInterval) and getCdaMeasurementInterval() for handling type IVL_PQ
            - setComment(String comment) and getComment() for handling type ST
     - Adjustments to effective time - can be either one time or interval of time
         - new/adjusted operations for are:
            - setEffectiveTime(Date effectiveTime) or setEffectiveDate(Date effectiveTime)
            - setEffectiveTimeInterval(Date effectiveStartTime, Date effectiveStopTime) or setEffectiveDateInterval(Date effectiveStartTime, Date effectiveStopTime)
            - getEffectiveTime() and effectiveTimeHasTime()
            - getEffectiveTimeInterval().getStartTime(), getEffectiveTimeInterval().getStopTime(), getEffectiveTimeInterval().startTimeHasTime and getEffectiveTimeInterval().stopTimeHasTime
     - CPDHealthStatusObservationEntry has been removed as entity       
            
### Version 6.1.1
  - CDA header version 2 has been adjusted
     - When building the XML elements representedOrganization and representedCustodianOrganization: when telecom and adr is empty they are no longer included with nullflavor.

### Version 6.1.2
  - QRD has been adjusted
     - QRDXMLNarrativeTextReplaceResponseConverter has been adjusted, so that dollar sign ($) now can be used in the text to be used as replacement

### Version 6.2.1
  - QRD has been adjusted
     - When building the XML references on a response, the id with root "1.2.208.184.5" is only included when corresponding extension is given (reference.setReferencesUse) or when external observation. 
     
### Version 6.2.2
  - confidentialityCode is now parsed. Use document.getConfidentialityCode()

### Version 6.2.3
  - qfdd-v1.2 and qrd-v1.3 is now available when parsing (decoding) metadata/formatcode

### Version 6.3.1
  - for text responses (incl associatedtext) value is allowed empty
  - when building XML for representedOrganization id is now included when available
  
### Version 6.3.2
  - QFDD has been adjusted to allow more details on organizer level
     - new operations at organizer level are:
         - addPrecondition(QFDDPrecondition precondition) and getPreconditions. As precondition element within organizer xml element.
         - setCode(CodedValue codedValue) and getCode(). As code element within organizer xml element.
         - setIntroduction(String introduction) and getIntroduction(). As originalText within code xml element.

### Version 6.3.3
  - QRD has been adjusted so value is not required for numeric and analog slider answers
  
### Version 6.3.4
  - In some specific situations the parsing failed with "Cant parse xml document String index out of range". This has been fixed
  
### Version 6.3.5
  - PDC parsing can now extract the telecom of an Dentist
  
### Version 6.3.6
  - QRD "Information Only Section" now uses templateid 2.16.840.1.113883.10.20.32.2.1.
  - QRD "Copy Right Section" added. New operations are:
     - setCopyRight(SectionInformation copyRight)
     - getCopyRight()
  - QRD Discrete Slider now has associated text available

### Version 6.3.7
  - QRD has been adjusted to allow more details on organizer level
     - new operations at organizer level are:
         - setCode(CodedValue codedValue) and getCode(). As code element within organizer xml element.
         - setIntroduction(String introduction) and getIntroduction(). As originalText within code xml element.
  - QFDD Discrete Slider now has associated text available

### Version 6.4.1
  - PDC version 3.0 is new

### Version 6.4.2
  - QRD has been adjusted in relation to author representedOrganization:
     - telecom and adr is included in XML when available
     - representedOrganization is included when author is based on patient

### Version 6.4.3
  - QRD has been adjusted
    - QRDXMLNarrativeTextReplaceResponseConverter has been adjusted, so that QRDDiscreteSliderResponse now also uses replace for associated text

### Version 6.5.0
  - QRD and QFDD has been adjusted in relation to copyright section. The change is not backward compatible!
    - Copyright section has been moved from the document level and into section level in the document model. This allows for multiple copyright sections.
    - The methods getCopyRight() and setCopyRight(...) has been removed from QFDDDocument and QRDDocument
    - To have the copyright section at the end of the document as before, simple add a copyright section as the last section
    - To find out if a section is a copyright section or a question/response section use the isCopyRightSection() method after parsning
    - Use the methods "new Section(...)" and ".addCopyrightText(...)" at Section to create a copyright
    - In case the text at Section level is not set, it will be filled in automatically during xml building with all copyright texts separated with <br/>
    - See example TestQRDVariationsExamplesBuildAndParse.testCopyRightBuildAndParse for an example of creating and getting the copyright

### Version 6.6.0
  - For all document build of address could not handle when city was null. This has been fixed, so that it will generate an nullflavor element
  - PHMR version 2.0 is new. The following adjustments have been made compared with previous version:
      - representedOrganization has been adjusted (author and LegalAuthenticator) when the xml is generated:
          - telecom and adr is included when available
          - representedOrganization is included when author is based on patient
      - Medical Equipment is no longer part of the new version of PHMR (Model, build or parse)
      - Vital Signs and Results sections is no longer included when the xml is generated when there are no data available
      - The code element in the entry element in Vital Signs and Results no longer uses the Translation element when the xml is generated
      - TemplateIds have been removed from Section and Observation level when the xml is generated
      - Code at section level now has displayName and CodeSystemName when the xml is generated
      - New documentationOf section with id element holding profile and version. Use operation getCdaProfileAndVersion() and setCdaProfileAndVersion(String cdaProfileAndVersion).
      - ObservationMedia removed when generating xml for Vital Signs and Results
      - It is now possible to have more component/observations pr entry/organizer in the xml
      - timestamp and status have moved from measurement to measurementGroup. They are put in the organizer element of the xml as statusCode and effectiveTime
      - Reference and ReferenceRange is no longer available in the xml
      - Look at TestPHMRVariationsExamplesBuildAndParse for examples on how to use the new version

### Version 6.6.1
  - PHMR version 2.0 has been adjusted:
      - The constructor for MeasurementGroup(Measurement measurement) has been added Date and Status as parameter.
      - dataEnterer has been added as participant
      - It is now possible to use MobileContact as value for "Use" in the Telecom list. In the xml the value is MC.