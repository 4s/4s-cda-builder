# Net4Care CDA Library

This component allows

   - A *GreenCDA* approach for constructing a data object representing
     PHMR, QRD, QFDD and APD documents: *ClinicalDocument*
   - A Builder which can produce a XML document representing a valid
     Danish PHMR, QRD, QFDD or APD (following the MedCom profile) document from the
     *ClinicalDocument* object.

## Usage

### Builders
Notice: The CDABuilder is a tool for parsning and generating CDA documents. For vendors using the CDABuilder please be aware that the tool do not guarantee compliance to the international CDA standard or the national CDA standard

For code examples please take a look at the following example tests. Also be sure to have a look at the DK CDA profiles at MedCom:

CDAMetadata:
 - the CDAMetadata is for fetching the CDA metadata information which is needed to the registry when documents are uploaded to a repository.
 - when an actual CDA document is present, e.g. an PHMR document, this can be sent to the CDAMetadata API which will return the Metadata.
 - For examples see the mentioned test. And in the CDAMetadata.java file, all fields are listed along with the relevant methods calls.
 - [dk.s4.hl7.cda.convert.decode.TestXmlToCdaMetadata](4s-cda-builders/src/test/java/dk/s4/hl7/cda/convert/decode/TestXmlToCdaMetadata.java)
 - http://svn.medcom.dk/svn/releases/Standarder/IHE/DK_profil_metadata/Metadata-v095.pdf
 - (The API only have parse data from document to model. It's up to the specific documents to do the buldings itself from model to document) 

PHMR:

 - [dk.s4.hl7.cda.convert.SetupMedcomKOLExample1](4s-cda-builders/src/test/java/dk/s4/hl7/cda/convert/SetupMedcomKOLExample1.java)
 - http://svn.medcom.dk/svn/drafts/Standarder/HL7/PHMR/

QRD:
 
 - [dk.s4.hl7.cda.model.qrd.SetupQrdKolExample1](4s-cda-builders/src/test/java/dk/s4/hl7/cda/model/qrd/SetupQrdKolExample1.java)
 - http://svn.medcom.dk/svn/drafts/Standarder/HL7/PRO/QRD/

QFDD: 

 - [dk.s4.hl7.cda.model.qfdd.SetupQFDDMultibleChoiseExample](4s-cda-builders/src/test/java/dk/s4/hl7/cda/model/qfdd/SetupQFDDMultibleChoiseExample.java)
 - http://svn.medcom.dk/svn/drafts/Standarder/HL7/PRO/QFDD/
 
APD: 

 - [dk.s4.hl7.cda.convert.encode.apd.SetupMedcomExample](4s-cda-builders/src/test/java/dk/s4/hl7/cda/convert/encode/apd/SetupMedcomExample.java)
 - http://svn.medcom.dk/svn/drafts/Standarder/HL7/Appointment/
 
 - If APD 1.0 documents needs to be handled, then initialize APDXmlCodec:
   `APDXmlCodec codec = new APDXmlCodec(Version.V10);` 
 - Use StatusV10 to get old values for APD 1.0 documents

APD 1.1:

 - Use StatusV11 to get new values for APD 1.1 documents.
 - Default APDXmlCodec is working with APD 1.1 xml and appointment documents.
   `APDXmlCodec codec = new APDXmlCodec();`
 - Or specifically call with Version:
   `APDXmlCodec codec = new APDXmlCodec(Version.V11);`

APD 2.0:

 - Is now absolete, instead use version 2.0.1.

APD 2.0.1:

 - To instanciate version 2.0.1 of the document, use AppointmentDocument from the dk.s4.hl7.cda.model.apd.v20 package
 - More details on version changes: See readme file in builder [project](4s-cda-builders/README.md)
 - [dk.s4.hl7.cda.convert.encode.apd.v20.SetupMedcomV20Example14ApdMunicipalityA.java](4s-cda-builders/src/test/java/dk/s4/hl7/cda/convert/encode/apd/v20/SetupMedcomV20Example14ApdMunicipalityA.java)
 - [dk.s4.hl7.cda.convert.encode.apd.v20.SetupMedcomV20Example15ApdRegion.java](4s-cda-builders/src/test/java/dk/s4/hl7/cda/convert/encode/apd/v20/SetupMedcomV20Example15ApdRegion.java)
 - [dk.s4.hl7.cda.convert.encode.apd.v20.SetupMedcomV20Example16ApdGpV2.java](4s-cda-builders/src/test/java/dk/s4/hl7/cda/convert/encode/apd/v20/SetupMedcomV20Example16ApdGpV2.java) 
 - http://svn.medcom.dk/svn/drafts/Standarder/HL7/Appointment/

PDC 2.0.1
 - Only parsing is available - XML to model (not building - model to XML)
 - Example code: [TestXmlToPdc](4s-cda-builders/src/test/java/dk/s4/hl7/cda/convert/decode/TestXmlToPdcV20.java);
 - More details on version changes: See readme file in builder [project](4s-cda-builders/README.md)
 - http://svn.medcom.dk/svn/releases/Standarder/HL7/PDC/


NB! The builder does not make an exact 1:1 conversion of what is given as input and what comes out as XML. An example of this is the address values. When not given, then the output will be <streetAddressLine nullFlavor="NI"/>

As of 28 February 2020 only DK-APD_Example_1_6_apd_gp.xml from the 2.0 sample set has been tested.
As of Juli 2020 DK-APD_Example_1_4_apd_municipality_a and DK-APD_Example_1_5_apd_region from the 2.0 sample set has been tested.

An known issue is the performer xml element. Whether to adopt to version 1 or version 2 of the APD standard. Currently it adopt to version 1.

### Data generators
This tool is used for generating PHMR and QRD documents based on template csv files as input.
Having the xml files the CDAUploader may be used to upload the documents to an XDS Respository by using the "Provide and Register" request.

Please have a look at the 'User guide for CDA tools.docx' document for thorough explanation and examples of this set of tools.

This package has dependencies to XDS Connector 0.3.0

## Prerequisites
 - Git 
 - Java version 1.6, or newer (data generators require 1.7 or newer)
 - Maven version 3.0, or newer

## Build
 - Clone project from Bitbucket:
   `git clone https://bitbucket.org/4s/4s-cda-builder.git `
 - Compile and run test for CDA builders: `<git-root-folder>/4s-cda-builders/mvn install`
 - Compile and run test for Data generators: `<git-root-folder>/4s-cda-data-generators/mvn install`

## Develop

### Eclipse

- Prepare eclipse project files:  
  `mvn eclipse:eclipse`
- If the M2_REPO variable is not set in eclipse then execute:  
  `mvn -Declipse.workspace=<path to eclipse workspace>; eclipse:configure-workspace`
- Open eclipse and import as existing project, see [eclipse help](http://help.eclipse.org/kepler/index.jsp?topic=/org.eclipse.platform.doc.user/tasks/tasks-importproject.htm)

## Deploy

Only maintainers of the repository should do this, provided here for reference

 - Add artifactory credentials to *$HOME/.m2/settings.xml*
 - Verify that the pom.xml has the proper version number (No SNAPSHOT) and
   that changes.md is updated
 - Execute: `mvn deploy -P {CS|4S}`

Use profile `CS` for [AU CS's Twiga repository](http://twiga.cs.au.dk:8081/artifactory/simple/libs-release-net4care/) 
and `4S` for [4S' repository](http://artifactory.4s-online.dk/artifactory/libs-snapshot-local)
