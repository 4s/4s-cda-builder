package dk.s4.hl7.cda.download;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.net4care.xdsconnector.Constants.XDSStatusValues.DocumentEntry;
import org.net4care.xdsconnector.Utilities.CodedValue;
import org.net4care.xdsconnector.Utilities.FindDocumentsQueryBuilder;
import org.net4care.xdsconnector.Utilities.QueryBuilder;

public class CDAFindDocumentsQueryBuilder {
  public final static String XDS_DOCUMENTENTRY_OBJ_TYPE_STABLE_DOCUMENT = "urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1";
  public final static String XDS_DOCUMENTENTRY_OBJ_TYPE_ONDEMAND_DOCUMENT = "urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248";
  public final static String XDS_DTM_FORMAT = "yyyyMMddHHmmss";

  private FindDocumentsQueryBuilder queryBuilder;

  public CDAFindDocumentsQueryBuilder() {
    this.queryBuilder = new FindDocumentsQueryBuilder();
    queryBuilder.setReturnType(FindDocumentsQueryBuilder.RETURN_TYPE_FULLMETADATA);
  }

  public CDAFindDocumentsQueryBuilder setPatientCpr(String patientCpr) {
    if (patientCpr != null) {
      queryBuilder.setPatientId(createPatientCprPattern(patientCpr));
    }
    return this;
  }

  public CDAFindDocumentsQueryBuilder setPatientId(String patientId, String authorityDomainId) {
    queryBuilder.setPatientId(createPatientIdPattern(patientId, authorityDomainId));
    return this;
  }

  public CDAFindDocumentsQueryBuilder setApproved() {
    queryBuilder.addDocumentStatus(DocumentEntry.Approved);
    return this;
  }

  public CDAFindDocumentsQueryBuilder setDeprecated() {
    queryBuilder.addDocumentStatus(DocumentEntry.Deprecated);
    return this;
  }

  public void setOnDemand() {
    queryBuilder.addObjectType(XDS_DOCUMENTENTRY_OBJ_TYPE_ONDEMAND_DOCUMENT);
  }

  public void setStable() {
    queryBuilder.addObjectType(XDS_DOCUMENTENTRY_OBJ_TYPE_STABLE_DOCUMENT);
  }

  public CDAFindDocumentsQueryBuilder setServiceStartTimeInterval(UnboundableInterval interval) {
    if (interval != null) {
      if (interval.getStart() != null) {
        queryBuilder.setServiceStartTimeFrom(createTimePattern(interval.getStart()));
      }
      if (interval.getEnd() != null) {
        queryBuilder.setServiceStartTimeTo(createTimePattern(interval.getEnd()));
      }
    }
    return this;
  }

  public CDAFindDocumentsQueryBuilder setServiceStopTimeInterval(UnboundableInterval interval) {
    if (interval != null) {
      if (interval.getStart() != null) {
        queryBuilder.setServiceStopTimeFrom(createTimePattern(interval.getStart()));
      }
      if (interval.getEnd() != null) {
        queryBuilder.setServiceStopTimeTo(createTimePattern(interval.getEnd()));
      }
    }
    return this;
  }

  public CDAFindDocumentsQueryBuilder setCreatenTimeInterval(UnboundableInterval interval) {
    if (interval != null) {
      if (interval.getStart() != null) {
        queryBuilder.setCreationTimeFrom(createTimePattern(interval.getStart()));
      }
      if (interval.getEnd() != null) {
        queryBuilder.setCreationTimeTo(createTimePattern(interval.getEnd()));
      }
    }
    return this;
  }

  public CDAFindDocumentsQueryBuilder addClassCodes(List<CodedValue> classCodes) {
    for (CodedValue codedValue : classCodes) {
      queryBuilder.addClassCode(formatCodedValues(codedValue));
    }
    return this;
  }

  public CDAFindDocumentsQueryBuilder addTypeCodes(List<CodedValue> typeCodes) {
    for (CodedValue codedValue : typeCodes) {
      queryBuilder.addTypeCode(formatCodedValues(codedValue));
    }
    return this;
  }

  public CDAFindDocumentsQueryBuilder addPracticeSettingCodes(List<CodedValue> practiceSettingCodes) {
    for (CodedValue codedValue : practiceSettingCodes) {
      queryBuilder.addPracticeSettingCode(formatCodedValues(codedValue));
    }
    return this;
  }

  public CDAFindDocumentsQueryBuilder addHealthcareFacilityCodes(List<CodedValue> healthcareFacilityCodes) {
    for (CodedValue codedValue : healthcareFacilityCodes) {
      queryBuilder.addHealthcareFacilityTypeCode(formatCodedValues(codedValue));
    }
    return this;
  }

  public CDAFindDocumentsQueryBuilder addEventCodes(List<CodedValue> eventCodes) {
    for (CodedValue codedValue : eventCodes) {
      queryBuilder.addEventCodeList(formatCodedValues(codedValue));
    }
    return this;
  }

  public CDAFindDocumentsQueryBuilder addConfidentialityCodes(List<CodedValue> confidentialityCodes) {
    for (CodedValue codedValue : confidentialityCodes) {
      queryBuilder.addConfidentialityCode(formatCodedValues(codedValue));
    }
    return this;
  }

  public CDAFindDocumentsQueryBuilder addFormatCodes(List<CodedValue> formatCodes) {
    for (CodedValue codedValue : formatCodes) {
      queryBuilder.addFormatCode(formatCodedValues(codedValue));
    }
    return this;
  }

  public CDAFindDocumentsQueryBuilder addAuthorPerson(String authorPerson) {
    if (authorPerson != null) {
      queryBuilder.addAuthorPerson(authorPerson);
    }
    return this;
  }

  public QueryBuilder build() {
    return queryBuilder;
  }

  /*
   * PRIVATE SECTION
   */
  private static String createTimePattern(DateTime dateTime) {
    if (dateTime == null) {
      throw new RuntimeException("Date time required");
    }
    return DateTimeFormat.forPattern(XDS_DTM_FORMAT).print(dateTime);
  }

  private static String createPatientCprPattern(String patientCpr) {
    return createPatientIdPattern(patientCpr, "1.2.208.176.1.2");
  }

  private static String createPatientIdPattern(String patientId, String authorityDomainId) {
    if (!isTextUseful(patientId)) {
      throw new RuntimeException("Patient id is required");
    }
    StringBuilder builder = new StringBuilder().append(patientId);
    if (isTextUseful(authorityDomainId)) {
      builder.append("^^^&").append(authorityDomainId).append("&ISO");
    }
    return builder.toString();
  }

  private String formatCodedValues(CodedValue codedValue) {
    // Description field intentionally left out
    if (!isTextUseful(codedValue.getCode())) {
      throw new RuntimeException("Code is required");
    }
    StringBuilder builder = new StringBuilder().append(codedValue.getCode()).append("^^");
    if (isTextUseful(codedValue.getCodeSystem())) {
      builder.append(codedValue.getCodeSystem());
    }
    return builder.toString();
  }

  private static boolean isTextUseful(String text) {
    if (text != null) {
      String trimmed = text.trim();
      return !trimmed.isEmpty() && !trimmed.equalsIgnoreCase("null");
    }
    return false;
  }
}
