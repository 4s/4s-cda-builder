package dk.s4.hl7.cda.datacreation;

import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.datacreation.model.apd.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.*;

public class ApdDataGenerator extends DataGeneratorBase {
  private String APD_BASE_INPUT_FILE = "02_apd_base.csv";
  private String APD_TIME_INPUT_FILE = "02_apd_time.csv";
  private String APD_REASON_INPUT_FILE = "02_apd_reason.csv";
  private String APD_REQUESTER_INPUT_FILE = "02_apd_requester.csv";
  private String OUTPUT_FILE_BASE_NAME = "ApdDataFileFor";

  public static void main(String[] args) {
    Logger logger = LoggerFactory.getLogger(ApdDataGenerator.class);
    ApdDataGenerator adp = new ApdDataGenerator();
    try {
      // Load all properties
      adp.readProperty(args);
      adp.parseCsvFile();
    } catch (IOException | URISyntaxException e) {
      logger.error("Parsing the input csv file(s) failed", e);
    }
  }

  @Override
  protected void parseCsvFile() throws IOException {
    List<CSVRecord> generatedCdaHeaderDataRecords = getHeaderData();
    getHeaderData(generatedCdaHeaderDataRecords);
    getSpecificData(generatedCdaHeaderDataRecords, apdFilesPath, generatedApdDataFilesPath + OUTPUT_FILE_BASE_NAME);
  }

  /**
   * Overrides the 'getSpecificData' method in the DataGeneratorBase class.
   * The name of the method is somewhat misleading, since nothing is returned from the method
   * and because the purpose of the method(s) actually is to:
   * a: Prepare the header data.
   * b: Construct the specific data/temporary csv data (in this case the Apd-data).
   * c: Persist the temporary CSV data to be used when creating the XML test files.
   */
  @Override
  protected void getSpecificData(List<CSVRecord> generatedCdaHeaderDataRecords, String filesPath, String outputFileName)
      throws IOException {
    List<CSVRecord> baseRecords = getApdBaseCsvRecordsWithHeader();
    List<CSVRecord> timeRecords = getApdTimeCsvRecordsWithHeader();
    List<CSVRecord> reasonRecords = getApdReasonCsvWithHeader();
    List<CSVRecord> authorRecords = getApdAuthorCsvWithHeader();

    List<String> allApdCsvLines = createAllApdCsvLines(baseRecords, timeRecords, reasonRecords, authorRecords);
    String allInputCsvColumnNames = createCSVColumnNames(Arrays.asList(baseRecords.get(0)),
        Arrays.asList(authorRecords.get(0)));
    persistGeneratedApdCsvLines(allApdCsvLines, allInputCsvColumnNames, outputFileName);
  }

  /**
   * This method will convert the input apd-csv files to lines in a new temporary csv file (which will later be used
   * to create the XML-data files).
   *
   * An appointment consists of the following elements, beyond the elements already covered by the header:
   *  1: Status, Title, Id, Description etc.
   *  2: A performer
   *  3: A requester/author (who has requested the appointment)
   *  4: A location of the appointment
   *  5: A start and end time for the appointment.
   *
   * These elements have been grouped into 4 'blocks':
   *   1: Base-data: Title, description, id etc.
   *   2: Time-data: specifying when the appointment will take place.
   *   3: Reason: Description, the performer and the location of the appointment.
   *   4: Request: Request/Author
   *
   * The application takes 4 input files (which can each have x number of lines):
   *   1: 02-apd-base.csv which contains base-data PLUS optional time-data and optional reason data.
   *   2: 02-apd-time.csv which contains Time-data.
   *   3: 02-apd-reason.csv which contains a description, a performer and a location.
   *   4: 02-apd-request.csv which contains the requester/author.
   *
   * When constructing a line for the resulting apd-csv file, the algorithm will for each base-record(line) in the apd-base file do:
   *   1: Take base-data from the base-record.
   *   2: Attempt to take time-data from the base-record.
   *     2.b If the base-record does not contain time-data,
   *         time-data will instead be taken from a random record from the apd-time file.
   *   3: Attempt to take reason-data from the base-record.
   *     3.b If the base-record does not contain reason-data,
   *         reason data will instead be taken from a random record from the apd-reason file.
   *   4: Take request-data from a random record from the apd-request file.
   *
   *   If the appointment id of the current base-record is the same as the appointment id from the previous
   *   base-record, the reason data will be reused from the previous base-record (or from the previously used
   *   reason-record).
   */
  private List<String> createAllApdCsvLines(List<CSVRecord> baseRecords, List<CSVRecord> timeRecords,
      List<CSVRecord> reasonRecords, List<CSVRecord> authorRecords) {

    String previousRecordAppointId = "";
    ApdReason apdReason = null;
    List<String> allCsvLines = new ArrayList<>();
    for (int patientRecordIndex = 1; patientRecordIndex < baseRecords.size(); patientRecordIndex++) {
      CSVRecord baseRecord = baseRecords.get(patientRecordIndex);

      ApdBase apdBase = ApdCsvInputParser.getApdBaseFromCsvRecord(baseRecord);
      ApdTime apdTime = getApdTimeFromInputRecords(timeRecords, baseRecord);
      ApdParticipant apdAuthor = getApdParticipantFromInputRecords(authorRecords);

      if (!apdBase.getAppointmentId().equals(previousRecordAppointId)) {
        apdReason = getApdReasonFromInputRecords(reasonRecords, baseRecord);
      }

      addNewCsvLines(apdBase, apdReason, apdTime, apdAuthor, allCsvLines);
      previousRecordAppointId = apdBase.getAppointmentId();
    }
    return allCsvLines;
  }

  private ApdParticipant getApdParticipantFromInputRecords(List<CSVRecord> authorRecords) {
    CSVRecord authorRecord = ApdCsvInputParser.getRandomCvsRecord(authorRecords);
    return ApdCsvInputParser.getApdAuthorFromCsvRecord(authorRecord);
  }

  private ApdReason getApdReasonFromInputRecords(List<CSVRecord> reasonRecords, CSVRecord baseRecord) {
    ApdReason reason = ApdCsvInputParser.getApdReasonFromCsvRecord(baseRecord);
    if (reason.isObjectEmpty()) {
      CSVRecord reasonRecord = ApdCsvInputParser.getRandomCvsRecord(reasonRecords);
      reason = ApdCsvInputParser.getApdReasonFromCsvRecord(reasonRecord);
    }
    return reason;
  }

  private ApdTime getApdTimeFromInputRecords(List<CSVRecord> timeRecords, CSVRecord baseRecord) {
    ApdTime apdTime = ApdCsvInputParser.getApdTimeFromCsvRecord(baseRecord);
    if (apdTime.isObjectEmpty()) {
      CSVRecord randomTimeRecord = ApdCsvInputParser.getRandomCvsRecord(timeRecords);
      apdTime = ApdCsvInputParser.getApdTimeFromCsvRecord(randomTimeRecord);
    }
    return apdTime;
  }

  /**
   * Will create new CsvLines, either one line if appointment start and end time is specified in the input records,
   * or multiple lines if appointment start time and duration is specified through cron-values in the input records.
   */
  private void addNewCsvLines(ApdBase apdBase, ApdReason apdReason, ApdTime apdTime, ApdParticipant apdAuthor,
      List<String> allCsvLines) {
    Logger logger = LoggerFactory.getLogger(ApdDataGenerator.class);

    ApdCsvBuilder appointmentBuilder = new ApdCsvBuilder(apdBase);
    appointmentBuilder.withReason(apdReason);
    appointmentBuilder.authoredBy(apdAuthor);

    if (apdTime.isStandardTimeUsed()) {
      appointmentBuilder.startingAt(apdTime.getAppointmentStart());
      appointmentBuilder.endingAt(apdTime.getAppointmentEnd());
      allCsvLines.add(appointmentBuilder.build());
    } else {
      try {
        CronDateTimeGenerator cron = new CronDateTimeGenerator(apdTime.getAppointmentCron());
        while (cron.hasNext()) {
          DateTime startDate = cron.next();
          DateTime endDate = startDate.plusMinutes(Integer.valueOf(apdTime.getAppointmentDuration()));
          appointmentBuilder.startingAt(startDate.toString());
          appointmentBuilder.endingAt(endDate.toString());
          allCsvLines.add(appointmentBuilder.build());
        }
      } catch (ParseException e) {
        logger.error("Exception thrown while parsing cron expression: " + apdTime.getAppointmentCron());
      }
    }
  }

  /**
   * Will create the output ApdCsv files containing both header data and specific apd data.
   * One file will be create pr. patient in the personDataMap.
   * All files will contain all the data found in allApdCsvLines.
   */
  private void persistGeneratedApdCsvLines(List<String> allApdCsvLines, String allCsvColumnNames, String outputFileName)
      throws IOException {
    Set<Map.Entry<String, String>> personDataEntrySet = personDataMap.entrySet();
    Iterator<Map.Entry<String, String>> itr = personDataEntrySet.iterator();
    while (itr.hasNext()) {
      Map.Entry<String, String> personData = itr.next();
      for (int counter = 0; counter < allApdCsvLines.size(); counter++) {
        writeCsvFile(allCsvColumnNames, personData.getValue(), Arrays.asList(allApdCsvLines.get(counter)),
            outputFileName + personData.getKey() + String.format("_%05d.csv", counter));
      }
    }
  }

  private List<CSVRecord> getApdBaseCsvRecordsWithHeader() throws IOException {
    List<String> apdBaseCsvHeaders = ApdCsvInputParser.getApdBaseCsvHeaders();
    return getCsvRecords(apdFilesPath + APD_BASE_INPUT_FILE, "UTF-8",
        apdBaseCsvHeaders.toArray(new String[apdBaseCsvHeaders.size()]));
  }

  private List<CSVRecord> getApdTimeCsvRecordsWithHeader() throws IOException {
    List<String> apdTimeCsvHeaders = ApdCsvInputParser.getApdTimeCsvHeaders();
    return getCsvRecords(apdFilesPath + APD_TIME_INPUT_FILE, "UTF-8",
        apdTimeCsvHeaders.toArray(new String[apdTimeCsvHeaders.size()]));
  }

  private List<CSVRecord> getApdReasonCsvWithHeader() throws IOException {
    List<String> apdReasonCsvHeaders = ApdCsvInputParser.getApdReasonCsvHeaders();
    return getCsvRecords(apdFilesPath + APD_REASON_INPUT_FILE, "UTF-8",
        apdReasonCsvHeaders.toArray(new String[apdReasonCsvHeaders.size()]));
  }

  private List<CSVRecord> getApdAuthorCsvWithHeader() throws IOException {
    List<String> apdAuthorCsvHeaders = ApdCsvInputParser.getApdAuthorCsvHeaders();
    return getCsvRecords(apdFilesPath + APD_REQUESTER_INPUT_FILE, "UTF-8",
        apdAuthorCsvHeaders.toArray(new String[apdAuthorCsvHeaders.size()]));
  }

  @Override
  protected List<String> manipulateData(CSVRecord csvRecord, String docId, String docTid) {
    return null;
  }
}
