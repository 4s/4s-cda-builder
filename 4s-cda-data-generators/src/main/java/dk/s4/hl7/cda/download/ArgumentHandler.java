package dk.s4.hl7.cda.download;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.net4care.xdsconnector.Utilities.CodedValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ArgumentHandler {
  private static final String DEFAULT_PROPERTIES_FILE = "./download/download.properties";

  private static final Logger logger = LoggerFactory.getLogger(ArgumentHandler.class);

  private static final Option OPTION_HELP = Option.builder("help").desc("Print help usage page").hasArg(false).build();

  private static final Option OPTION_PROPERTIES = Option
      .builder("properties")
      .desc("Path to the file download.properties")
      .hasArg()
      .argName("PATH")
      .build();

  private static final Option OPTION_PATIENT_CPR = Option
      .builder("patient")
      .hasArg()
      .desc("The social security number of the patient")
      .argName("PATIENT CPR")
      .required()
      .build();

  private static final Option OPTION_STATUS_APPROVED = Option
      .builder("approved")
      .hasArg(false)
      .desc(
          "Search for documents with approved status. At least one of 'approved' or 'deprecated' parameters must be specified")
      .build();

  private static final Option OPTION_STATUS_DEPRECATED = Option
      .builder("deprecated")
      .hasArg(false)
      .desc(
          "Search for documents with deprecated status. At least one of 'approved' or 'deprecated' parameters must be specified")
      .build();

  private static final Option OPTION_STABLE = Option
      .builder("stable")
      .hasArg(false)
      .desc("Search for stable document")
      .build();

  private static final Option OPTION_ON_DEMAND = Option
      .builder("ondemand")
      .hasArg(false)
      .desc("Search for on demand decuments")
      .build();

  private static final Option OPTION_CREATION_TIME = Option
      .builder("creationTime")
      .hasArg()
      .argName("FROM_TIME,TO_TIME")
      .desc("Date time interval of the document creation time")
      .build();

  private static final Option OPTION_SERVICE_START_TIME = Option
      .builder("serviceStart")
      .hasArg()
      .argName("FROM_TIME,TO_TIME")
      .desc("Date time interval of the document service start time")
      .build();

  private static final Option OPTION_SERVICE_STOP_TIME = Option
      .builder("serviceStop")
      .hasArg()
      .argName("FROM_TIME,TO_TIME")
      .desc("Date time interval of the document service stop time")
      .build();

  private static final Option OPTION_CLASS_CODE = Option
      .builder("classCode")
      .hasArg()
      .argName("CODE,CODE_SYSTEM")
      .desc(
          "Class code of the document. Format: <code>,<codeSystem>. For DK values see: https://svn.medcom.dk/svn/drafts/Standarder/IHE/OID/DK-IHE_Metadata-Common_Code_systems-Value_sets.xlsx")
      .build();

  private static final Option OPTION_TYPE_CODE = Option
      .builder("typeCode")
      .hasArg()
      .argName("CODE,CODE_SYSTEM")
      .desc(
          "Type code of the document. Format: <code>,<codeSystem>. For DK values see: https://svn.medcom.dk/svn/drafts/Standarder/IHE/OID/DK-IHE_Metadata-Common_Code_systems-Value_sets.xlsx")
      .build();

  private static final Option OPTION_PRACTICE_SETTING_CODE = Option
      .builder("practiceSettingCode")
      .hasArg()
      .argName("CODE,CODE_SYSTEM")
      .desc(
          "Practice settings code of the document. Format: <code>,<codeSystem>. For DK values see: https://svn.medcom.dk/svn/drafts/Standarder/IHE/OID/DK-IHE_Metadata-Common_Code_systems-Value_sets.xlsx")
      .build();

  private static final Option OPTION_HEALTHCARE_FACILITY_CODE = Option
      .builder("healthcareFacilityCode")
      .hasArg()
      .argName("CODE,CODE_SYSTEM")
      .desc(
          "Healthcare facility code of the document. Format: <code>,<codeSystem>. For DK values see: https://svn.medcom.dk/svn/drafts/Standarder/IHE/OID/DK-IHE_Metadata-Common_Code_systems-Value_sets.xlsx")
      .build();

  private static final Option OPTION_EVENT_CODE = Option
      .builder("eventCode")
      .hasArg()
      .argName("CODE,CODE_SYSTEM")
      .desc(
          "Event code of the document. Format: <code>,<codeSystem>. For DK values see: https://svn.medcom.dk/svn/drafts/Standarder/IHE/OID/DK-IHE_Metadata-Common_Code_systems-Value_sets.xlsx")
      .build();

  private static final Option OPTION_CONFIDENTIALITY_CODE = Option
      .builder("confidentialityCode")
      .hasArg()
      .argName("CODE,CODE_SYSTEM")
      .desc(
          "Confidentiality code of the document. Format: <code>,<codeSystem>. For DK values see: https://svn.medcom.dk/svn/drafts/Standarder/IHE/OID/DK-IHE_Metadata-Common_Code_systems-Value_sets.xlsx")
      .build();

  private static final Option OPTION_FORMAT_CODE = Option
      .builder("formatCode")
      .hasArg()
      .argName("CODE,CODE_SYSTEM")
      .desc(
          "Format code of the document. Format: <code>,<codeSystem>. For DK values see: https://svn.medcom.dk/svn/drafts/Standarder/IHE/OID/DK-IHE_Metadata-Common_Code_systems-Value_sets.xlsx")
      .build();

  private static final Option OPTION_AUTHOR = Option
      .builder("author")
      .hasArg()
      .argName("AUTHOR")
      .desc(
          "Search by the author of the document. The format must be XCN (http://www.healthintersections.com.au/?p=1175) as stated by the DK metadata profile: https://svn.medcom.dk/svn/drafts/Standarder/IHE/DK_profil_metadata/. eg. \"^Andersen^Anders^Frederik&Ingolf^^^^^&ISO\"")
      .build();

  private CommandLine commandLine;
  private static final Options helpOptions = createHelpOptions();
  private static final Options allOptions = createAllOptions();

  public boolean parseArguments(String[] args) throws ParseException {
    if (parseHelpOptions(args)) {
      this.commandLine = new DefaultParser().parse(allOptions, args);
      if (!hasOption(OPTION_STATUS_APPROVED) && !hasOption(OPTION_STATUS_DEPRECATED)) {
        throw new ParseException("The usage of -approved or -deprecated is required");
      }
      return true;
    }
    return false;
  }

  private static Options createHelpOptions() {
    Options options = new Options();
    options.addOption(OPTION_HELP);
    return options;
  }

  private static Options createAllOptions() {
    Options options = new Options();
    options.addOption(OPTION_PROPERTIES);
    options.addOption(OPTION_HELP);
    options.addOption(OPTION_PATIENT_CPR);
    options.addOption(OPTION_STATUS_DEPRECATED);
    options.addOption(OPTION_STATUS_APPROVED);
    options.addOption(OPTION_STABLE);
    options.addOption(OPTION_ON_DEMAND);
    options.addOption(OPTION_CREATION_TIME);
    options.addOption(OPTION_SERVICE_START_TIME);
    options.addOption(OPTION_SERVICE_STOP_TIME);
    options.addOption(OPTION_CLASS_CODE);
    options.addOption(OPTION_TYPE_CODE);
    options.addOption(OPTION_PRACTICE_SETTING_CODE);
    options.addOption(OPTION_HEALTHCARE_FACILITY_CODE);
    options.addOption(OPTION_EVENT_CODE);
    options.addOption(OPTION_CONFIDENTIALITY_CODE);
    options.addOption(OPTION_FORMAT_CODE);
    options.addOption(OPTION_AUTHOR);
    return options;
  }

  private boolean parseHelpOptions(String[] args) throws ParseException {
    CommandLine tempCommandLine = new DefaultParser().parse(helpOptions, args, true);
    if (tempCommandLine.hasOption(OPTION_HELP.getOpt())) {
      new HelpFormatter().printHelp("dk.s4.hl7.cda.download.CDADownloader", allOptions, true);
      return false;
    }
    return true;
  }

  public Properties getProperties() throws Exception {
    File propertiesFile = readPropertiesFile();
    return createProperties(propertiesFile);
  }

  private void checkPropertiesFile(File file) {
    if (!file.exists()) {
      throw new RuntimeException("Properties file does not exist: " + file.getAbsolutePath());
    }
    if (!file.isFile()) {
      throw new RuntimeException("Properties file is not a file: " + file.getAbsolutePath());
    }
    if (!file.getAbsolutePath().endsWith(".properties")) {
      throw new RuntimeException("Properties file does not end with '.properties': " + file.getAbsolutePath());
    }
  }

  private File readPropertiesFile() {
    String propertiesFileName = commandLine.getOptionValue(OPTION_PROPERTIES.getOpt());
    File file = null;
    if (propertiesFileName != null && !propertiesFileName.isEmpty()) {
      file = new File(propertiesFileName);
      checkPropertiesFile(file);
    } else {
      logger.warn("No properties file in arguments - Trying to read default properties: " + DEFAULT_PROPERTIES_FILE);
      file = new File(DEFAULT_PROPERTIES_FILE);
      checkPropertiesFile(file);
      logger.info("Successfully loaded default propeties");
    }
    return file;
  }

  private static Properties createProperties(File file) throws IOException {
    Properties properties = new Properties();
    Reader reader = null;
    try {
      reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
      properties.load(reader);
    } catch (Exception ex) {
      logger.error(ex.getMessage());
    } finally {
      if (reader != null) {
        reader.close();
      }
    }
    return properties;
  }

  public String getPatientCpr() {
    return readOption(OPTION_PATIENT_CPR);
  }

  public boolean isApproved() {
    return hasOption(OPTION_STATUS_APPROVED);
  }

  public boolean isDeprecated() {
    return hasOption(OPTION_STATUS_DEPRECATED);
  }

  public boolean isStable() {
    return hasOption(OPTION_STABLE);
  }

  public boolean isOnDemand() {
    return hasOption(OPTION_ON_DEMAND);
  }

  public UnboundableInterval getCreationTime() {
    return createInterval(readOption(OPTION_CREATION_TIME));
  }

  public UnboundableInterval getServiceStartTime() {
    return createInterval(readOption(OPTION_SERVICE_START_TIME));
  }

  public UnboundableInterval getServiceStopTime() {
    return createInterval(readOption(OPTION_SERVICE_STOP_TIME));
  }

  public List<CodedValue> getClassCodes() {
    return createCodedValue(readOptions(OPTION_CLASS_CODE), OPTION_CLASS_CODE.getArgName());
  }

  public List<CodedValue> getTypeCodes() {
    return createCodedValue(readOptions(OPTION_TYPE_CODE), OPTION_TYPE_CODE.getArgName());
  }

  public List<CodedValue> getPracticeSettingCodes() {
    return createCodedValue(readOptions(OPTION_PRACTICE_SETTING_CODE), OPTION_PRACTICE_SETTING_CODE.getArgName());
  }

  public List<CodedValue> getHealthcareFacilityCodes() {
    return createCodedValue(readOptions(OPTION_HEALTHCARE_FACILITY_CODE), OPTION_HEALTHCARE_FACILITY_CODE.getArgName());
  }

  public List<CodedValue> getEventCodes() {
    return createCodedValue(readOptions(OPTION_EVENT_CODE), OPTION_EVENT_CODE.getArgName());
  }

  public List<CodedValue> getConfidentialityCodes() {
    return createCodedValue(readOptions(OPTION_CONFIDENTIALITY_CODE), OPTION_CONFIDENTIALITY_CODE.getArgName());
  }

  public List<CodedValue> getFormatCodes() {
    return createCodedValue(readOptions(OPTION_FORMAT_CODE), OPTION_FORMAT_CODE.getArgName());
  }

  public String getAuthor() {
    return readOption(OPTION_AUTHOR);
  }

  protected List<CodedValue> createCodedValue(String[] codedValues, String argumentName) {
    List<CodedValue> result = new ArrayList<CodedValue>();
    if (codedValues != null) {
      for (String codedValueText : codedValues) {
        String[] values = codedValueText.split(",");
        if (values.length == 1) {
          result.add(new CodedValue(values[0], null, null, null));
        } else if (values.length == 2) {
          result.add(new CodedValue(values[0], values[1], null, null));
        } else {
          throw new RuntimeException("Unexpected number of values for argument: " + argumentName);
        }
      }
    }
    return result;
  }

  protected UnboundableInterval createInterval(String times) {
    UnboundableInterval interval = null;
    if (times != null) {
      String[] timeArray = times.split(",");
      if (timeArray.length == 2) {
        interval = new UnboundableInterval(createDateTime(timeArray[0]), createDateTime(timeArray[1]));
      } else if (timeArray.length == 1) {
        interval = new UnboundableInterval(createDateTime(timeArray[0]), null);
      } else {
        throw new RuntimeException("Invalid argument for an interval. Check creationTime, serviceStart or serviceStop");
      }
      if (interval.getEnd() == null && interval.getStart() == null) {
        interval = null;
      }
    }
    return interval;
  }

  private DateTime createDateTime(String text) {
    if (text != null && !text.isEmpty()) {
      return ISODateTimeFormat.dateOptionalTimeParser().parseDateTime(text);
    }
    return null;
  }

  private String readOption(Option option) {
    return commandLine.getOptionValue(option.getOpt());
  }

  private String[] readOptions(Option option) {
    return commandLine.getOptionValues(option.getOpt());
  }

  private boolean hasOption(Option option) {
    return commandLine.hasOption(option.getOpt());
  }
}