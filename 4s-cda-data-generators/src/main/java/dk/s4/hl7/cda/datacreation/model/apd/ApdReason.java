package dk.s4.hl7.cda.datacreation.model.apd;

public class ApdReason extends ApdCsvObject {
  private String indicationDisplayName;
  private ApdParticipant performer;
  private ApdOrganization location;

  public String getIndicationDisplayName() {
    return indicationDisplayName;
  }

  public void setIndicationDisplayName(String indicationDisplayName) {
    this.indicationDisplayName = indicationDisplayName;
  }

  public ApdParticipant getPerformer() {
    return performer;
  }

  public void setPerformer(ApdParticipant performer) {
    this.performer = performer;
  }

  public ApdOrganization getLocation() {
    return location;
  }

  public void setLocation(ApdOrganization location) {
    this.location = location;
  }

  @Override
  public boolean isObjectEmpty() {
    return isNullOrEmpty(this.indicationDisplayName) && this.performer.isObjectEmpty() && this.location.isObjectEmpty();
  }

  @Override
  public String toCsv() {
    return this.indicationDisplayName + ";" + this.location.toCsv() + this.performer.toCsv();
  }
}
