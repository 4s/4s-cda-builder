package dk.s4.hl7.cda.download;

import java.util.List;

import org.apache.commons.cli.ParseException;
import org.net4care.xdsconnector.RegistryConnector;
import org.net4care.xdsconnector.RepositoryConnector;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType.DocumentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = { "org.net4care" })
@EnableAutoConfiguration
public class CDADownloader {
  private static final Logger logger = LoggerFactory.getLogger(CDADownloader.class);

  public static void main(String[] args) throws Exception {
    try {
      ArgumentHandler argumentHandler = new ArgumentHandler();
      if (argumentHandler.parseArguments(args)) {
        DownloaderProperties properties = new DownloaderProperties(argumentHandler.getProperties());
        CDAFindDocumentsQueryBuilder queryBuilder = new ArgumentsToQueryMapper().map(argumentHandler);

        logger.info("Starting XDSConnector");
        ConfigurableApplicationContext run = new SpringApplicationBuilder()
            .main(CDADownloader.class)
            .sources(CDADownloader.class)
            .properties(properties.getProperties())
            .logStartupInfo(false)
            .showBanner(false)
            .run(args);
        Downloader downloader = new Downloader(run.getBean("xdsRegistryConnector", RegistryConnector.class),
            run.getBean("xdsRepositoryConnector", RepositoryConnector.class), properties);
        logger.info("XDSConnector started");

        List<DocumentResponse> documents = downloader.download(queryBuilder);
        new CDADocumentWriter(properties.getOutputFolder(), properties.getEncoding()).write(documents);
      }
    } catch (ParseException ex) {
      logger.error(ex.getMessage());
      logger.info("Please use -help to print application usage page");
    } catch (Exception ex) {
      logger.error(ex.getMessage(), ex);
    }
  }
}
