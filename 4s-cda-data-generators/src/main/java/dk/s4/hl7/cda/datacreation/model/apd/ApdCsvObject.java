package dk.s4.hl7.cda.datacreation.model.apd;

public abstract class ApdCsvObject {
  public abstract boolean isObjectEmpty();

  public abstract String toCsv();

  protected boolean isNullOrEmpty(String value) {
    return value == null || value.toLowerCase().equals("null") || value.equals("");
  }
}
