package dk.s4.hl7.cda.datacreation.model.apd;

import java.util.UUID;

public class ApdCsvBuilder {
  private ApdBase appointmentBaseData;
  private String appointmentStart;
  private String appointmentEnd;
  private ApdReason reason;
  private ApdParticipant author;

  public ApdCsvBuilder(ApdBase appointmentBaseData) {
    this.appointmentBaseData = appointmentBaseData;
  }

  public ApdCsvBuilder withReason(ApdReason apdReason) {
    this.reason = apdReason;
    return this;
  }

  public ApdCsvBuilder startingAt(String appointmentStart) {
    this.appointmentStart = appointmentStart;
    return this;
  }

  public ApdCsvBuilder endingAt(String appointmentEnd) {
    this.appointmentEnd = appointmentEnd;
    return this;
  }

  public ApdCsvBuilder authoredBy(ApdParticipant author) {
    this.author = author;
    return this;
  }

  public String build() {
    String csvLine = "";
    csvLine += this.appointmentBaseData.toCsv();
    csvLine += this.appointmentStart + ";";
    csvLine += this.appointmentEnd + ";";
    csvLine += "null;null;"; //Cron data are not set for this intermediate step!
    csvLine += this.reason.toCsv();
    csvLine += this.author.toCsv();

    String docId = UUID.randomUUID().toString();
    String docTid = this.appointmentStart;
    csvLine += docId + ";";
    csvLine += docTid;

    //Remove last ";" as the method createCSVColumnNames in dataGeneratorBase adds one
    csvLine = csvLine.substring(0, csvLine.length() - 2);
    return csvLine;
  }
}
