package dk.s4.hl7.validation;

import org.junit.Test;

import dk.s4.hl7.cda.convert.QRDXmlCodec;
import dk.s4.hl7.cda.convert.decode.SetupQRDDocumentForTest;
import dk.s4.hl7.cda.convert.encode.qrd.SetupQRDAnalogSliderExample;
import dk.s4.hl7.cda.convert.encode.qrd.SetupQRDDiscreteSliderExample;
import dk.s4.hl7.cda.convert.encode.qrd.SetupQRDMultipleChoiceExample;
import dk.s4.hl7.cda.convert.encode.qrd.SetupQRDNoResponseExample;
import dk.s4.hl7.cda.convert.encode.qrd.SetupQRDNumericExample;
import dk.s4.hl7.cda.convert.encode.qrd.SetupQRDTextExample;
import dk.s4.hl7.cda.convert.encode.qrd.SetupQrdKolExample1;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import generated.CdaType;

public class TestValidateQRDExamples extends BaseTest<QRDDocument> {

  public TestValidateQRDExamples() {
    super(new QRDXmlCodec());
  }

  @Test
  public void testAnalogSlider() throws Exception {
    validateDocument(new SetupQRDAnalogSliderExample().createDocument(), CdaType.QRDOC);
  }

  @Test
  public void testDiscreteSlider() throws Exception {
    validateDocument(new SetupQRDDiscreteSliderExample().createDocument(), CdaType.QRDOC);
  }

  @Test
  public void testMultipleChoice() throws Exception {
    validateDocument(new SetupQRDMultipleChoiceExample().createDocument(), CdaType.QRDOC);
  }

  @Test
  public void testNoResponse() throws Exception {
    validateDocument(new SetupQRDNoResponseExample().createDocument(), CdaType.QRDOC);
  }

  @Test
  public void testNumeric() throws Exception {
    validateDocument(new SetupQRDNumericExample().createDocument(), CdaType.QRDOC);
  }

  @Test
  public void testText() throws Exception {
    validateDocument(new SetupQRDTextExample().createDocument(), CdaType.QRDOC);
  }

  @Test
  public void testMatisKolEx1() throws Exception {
    validateDocument(SetupQrdKolExample1.defineAsCDA(), CdaType.QRDOC);
  }

  @Test
  public void testQRDForTest() throws Exception {
    validateDocument(SetupQRDDocumentForTest.defineAsCDA(), CdaType.QRDOC);
  }
}